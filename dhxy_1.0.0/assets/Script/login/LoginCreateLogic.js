let define = require('../common/define');
var MsgCode = require('../etc/msg_code');
let skillMgr = require('../game/Skill');
let GameDefine = require('../game/GameDefine');

let OpenType = {
    CREATE: 0,      //创建
    RELIVE: 1,      //重温
    CHANGERACE: 2,  //切换种族
}

cc.Class({
    extends: cc.Component,

    properties: {
        bgNode: cc.Node,
        infoNode: cc.Node,
        roleNode: cc.Node,
        menuNode: cc.Node,

        skillNodes: [cc.Node],
        roleInfoNode: cc.Node,
        roleNameNode: cc.Node,
        skillLineNode: cc.Node,
        skillTypeNode: cc.Node,
        skillNameNode: cc.Node,
        skillTitleLab: cc.Label,
        skillInfoLab: cc.Label,

        nameEditbox: cc.EditBox,

        TxtAtlas: cc.SpriteAtlas,
        RoleAtlas: cc.SpriteAtlas,
        SkillAtlas: cc.SpriteAtlas,

        relive3: cc.Node,
    },

    onLoad() {
        this.rolePosX = this.roleNode.x;
        this.rolePosY = this.roleNode.y;
        this.roleScale = this.roleNode.scale;
        this.canChangeRole = true;
        this.roleChangeDirection = 0;
        this.onTouched = false;
        this.roleDragon = this.roleNode.getComponent('dragonBones.ArmatureDisplay');
        this.infoNode.zIndex = 3;
        this.roleNode.zIndex = 2;
        this.menuNode.zIndex = 4;
        this.reliveIndex = 0;
        this.roleIndex = 0;
        this.resid = 1001;
        this.bgType = 1001;
        this.skillIndex = -1;
        this.opentype = OpenType.CREATE;
        this.resetInfoLayer();
        this.getRandomName();

        this.node.on(cc.Node.EventType.TOUCH_START, this.touchBegan.bind(this));
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this.touchMoved.bind(this));
        this.node.on(cc.Node.EventType.TOUCH_END, this.touchEnded.bind(this));
        this.node.on(cc.Node.EventType.TOUCH_CANCEL, this.touchCanceled.bind(this));

        this.roleInfoRun();
        this.bgNode.getComponent('LoginCreateBg').playDragon();//loadInfo(cc.ll.propData.createbg.data[this.bgType]);
    },

    start() {
        this.setOpenType(this.opentype);
        if ((this.opentype == OpenType.RELIVE && cc.ll.player.relive >= 2) ||
            (this.opentype == OpenType.CHANGERACE && cc.ll.player.relive == 3)) {
            this.relive3.active = true;
        } else {
            this.relive3.active = false;
        }
    },

    setOpenType(t) {
        if (t == OpenType.CREATE || t == OpenType.RELIVE || t == OpenType.CHANGERACE) {
            this.opentype = t;
        }

        let relivemenu = cc.find(`reliveToggleContainer`, this.menuNode);
        if (relivemenu) {
            relivemenu.active = this.opentype == OpenType.RELIVE || this.opentype == OpenType.CHANGERACE;
            if (this.opentype == OpenType.CHANGERACE) {
                if (cc.ll.player.relive < 3) cc.find('toggle1', relivemenu).active = false;
                if (cc.ll.player.relive < 1) cc.find('toggle2', relivemenu).active = false;
            }
        }

        let reliveBtn = cc.find(`reliveBtn`, this.menuNode);
        if (reliveBtn) {
            reliveBtn.active = this.opentype == OpenType.RELIVE;
        }

        let creatBtn = cc.find(`ui_common_btn_ngt0`, this.menuNode);
        if (creatBtn) {
            creatBtn.active = this.opentype == OpenType.CREATE;
        }

        let namenode = cc.find(`ui_login_role_bg_welcome_back1`, this.menuNode);
        if (namenode) {
            namenode.active = this.opentype == OpenType.CREATE;
        }

        let changeraceBtn = cc.find(`changeraceBtn`, this.menuNode);
        if (changeraceBtn) {
            changeraceBtn.active = this.opentype == OpenType.CHANGERACE;
        }
    },

    changeRelive(index) {
        if (this.opentype == OpenType.CREATE) {
            return;
        }
        if (this.reliveIndex == index) {
            return;
        }
        this.reliveIndex = index;
        this.changeRole();
    },

    onChangeRole(e, d) {
        if (!this.canChangeRole) {
            return;
        }
        if (d == 0) {
            this.roleIndex--;
            this.roleChangeDirection = -1;
        } else if (d == 1) {
            this.roleIndex++;
            this.roleChangeDirection = 1;
        }
        this.roleIndex = (this.roleIndex + 8) % 8;
        this.changeRole();
    },

    changeRole() {
        let datalist = cc.ll.propData.login.data;
        if (this.reliveIndex == 0) {
            datalist = cc.ll.propData.login.data;
        } else if (this.reliveIndex == 1) {
            datalist = cc.ll.propData.login.data1;
        } else if (this.reliveIndex == 3) {
            datalist = cc.ll.propData.login.data3;
        }

        this.resid = datalist[this.roleIndex];

        this.loadRoleInfo();
        let tempBg = cc.ll.propData.role.data[this.resid].bgid;
        if (tempBg != this.bgType) {
            let nextBg = cc.instantiate(this.bgNode);
            nextBg.parent = this.node;
            this.bgType = tempBg;
            this.bgNode.getComponent('LoginCreateBg').delSelf();
            this.bgNode = nextBg;
            this.bgNode.getComponent('LoginCreateBg').loadInfo(cc.ll.propData.createbg.data[this.bgType]);

            if (this.bgType == 1004) {
                cc.find('back_ske', this.bgNode).zIndex = 1;
                cc.find('front_ske', this.bgNode).zIndex = 2;
                cc.find('left_ske', this.bgNode).zIndex = 3;
                cc.find('right_ske', this.bgNode).zIndex = 4;
            }
            else {
                cc.find('back_ske', this.bgNode).zIndex = 1;
                cc.find('left_ske', this.bgNode).zIndex = 2;
                cc.find('right_ske', this.bgNode).zIndex = 3;
                cc.find('front_ske', this.bgNode).zIndex = 4;
            }
        }
    },

    skillBtnClicked(e, d) {
        if (this.skillIndex != -1) {
            this.skillNodes[this.skillIndex].runAction(cc.rotateTo(0.2, 0));
        }
        this.skillIndex = d;
        this.skillNodes[this.skillIndex].runAction(cc.rotateTo(0.2, 45));

        let skillId = cc.ll.propData.role.data[this.resid]['skills'][this.skillIndex];
        let skillInfo = skillMgr.GetSkillInfo(skillId);

        this.skillNameNode.getComponent(cc.Sprite).spriteFrame = this.TxtAtlas.getSpriteFrame(`skill_${skillInfo.nID}_name`);
        this.skillNameNode.opacity = 0;
        this.skillNameNode.runAction(cc.fadeIn(0.25));

        this.skillInfoLab.string = skillInfo.getIntro();
        this.skillTitleLab.string = '【法术系】' + skillInfo.getFaxiName();
    },

    loadRoleInfo() {
        this.canChangeRole = false;
        this.resetInfoLayer();
        let self = this;
        cc.loader.loadResDir(`dragonbone/${this.resid}`, function (err, assets) {
            if (self.roleChangeDirection != 0) {
                let curRole = cc.instantiate(self.roleNode);
                curRole.parent = self.node;
                curRole.scale = 0.4;
                curRole.x = self.rolePosX + 300 * self.roleChangeDirection;
                curRole.y = self.rolePosY;
                curRole.runAction(cc.sequence(cc.spawn(cc.moveTo(0.2, cc.v2(self.rolePosX, self.rolePosY)), cc.scaleTo(0.25, self.roleScale)), cc.callFunc(() => {
                    self.canChangeRole = true;
                    self.beganPos = self.movedPos;
                    if (!self.onTouched) {
                        self.onTouched = false;
                        self.roleNode.stopAllActions();
                        self.roleNode.runAction(cc.moveTo(0.1, cc.v2(self.rolePosX, self.rolePosY)));
                    }
                })));
                self.roleDragon = curRole.getComponent('dragonBones.ArmatureDisplay');
                self.roleNode.runAction(cc.sequence(cc.spawn(cc.moveTo(0.2, cc.v2(self.rolePosX - 300 * self.roleChangeDirection, self.rolePosY)), cc.scaleTo(0.25, 0.4), cc.fadeTo(0.2, 50)), cc.callFunc((node) => {
                    node.destroy()
                }, self.roleNode)));
                self.roleNode = curRole;
            }
            else {
                self.canChangeRole = true;
            }
            cc.sys.isNative && self.roleDragon._factory.clear(false);//在手机上加载相同的资源会出现闪退
            self.roleDragon.dragonAsset = null;//设置新的资源前必须清除原有的资源，在手机会出现程序闪退
            self.roleDragon.dragonAtlasAsset = null;
            for (let i = 0; i < assets.length; i++) {
                if (assets[i] instanceof dragonBones.DragonBonesAsset) {
                    self.roleDragon.dragonAsset = assets[i];
                }
                if (assets[i] instanceof dragonBones.DragonBonesAtlasAsset) {
                    self.roleDragon.dragonAtlasAsset = assets[i];
                }
            }
            self.roleDragon.armatureName = 'armatureName';
            if (!self.onTouched) {
                self.roleInfoRun();
            }
        });

        let type = cc.ll.propData.role.data[this.resid].roletype + 1;
        for (let index = 0; index < this.skillNodes.length; index++) {
            let skill = this.skillNodes[index];
            skill.getComponent(cc.Sprite).spriteFrame = this.RoleAtlas.getSpriteFrame(`ui_login_role_bg_schooll${type}`);

            let skillId = cc.ll.propData.role.data[this.resid]['skills'][index];
            let skillInfo = skillMgr.GetSkillInfo(skillId);
            skill.getChildByName('icon').getComponent(cc.Sprite).spriteFrame = this.SkillAtlas.getSpriteFrame(skillInfo.strIcon);
        }
    },

    roleDragonRun() {
        this.roleDragon.playAnimation('idle2');
        let self = this;
        this.roleDragon.addEventListener('complete', function () {
            self.roleDragon.playAnimation('idle', 0);
        });
    },

    onBtnClick(e, d) {
        this.changeRelive(parseInt(d));
    },

    onReliveClick(e, d) {
        let roleData = {
            race: define.RaceType.Demon + Math.floor(this.roleIndex / 2),
            sex: define.SexType.Male + this.roleIndex % 2,
            resid: this.resid,
        };

        cc.ll.net.send('c2s_relive', roleData);
    },

    onChangeRaceClick(e, d) {
        let roleData = {
            race: define.RaceType.Demon + Math.floor(this.roleIndex / 2),
            sex: define.SexType.Male + this.roleIndex % 2,
            resid: this.resid,
        };

        cc.ll.net.send('c2s_changerace', roleData);
    },

    resetInfoLayer() {
        this.unschedule(this.updateMask);
        this.roleInfoNode.height = 0;
        this.roleNameNode.height = 0;
        this.skillLineNode.width = 0;

        this.skillTypeNode.opacity = 0;
        this.skillNameNode.opacity = 0;
        this.skillTypeNode.stopAllActions();

        this.skillIndex = -1;
        for (let index = 0; index < this.skillNodes.length; index++) {
            let skill = this.skillNodes[index];
            skill.active = false;
            skill.angle = 0;
            skill.stopAllActions();
        }
    },

    changeRoleInfo() {
        this.skillTypeNode.runAction(cc.fadeIn(1.5));
        for (let index = 0; index < this.skillNodes.length; index++) {
            let skill = this.skillNodes[index];
            skill.opacity = 0;
            skill.active = true;
            skill.runAction(cc.sequence(cc.delayTime(index * 0.25 + 0.5), cc.fadeIn(0.25)));
        }
        this.skillTypeNode.getComponent(cc.Sprite).spriteFrame = this.TxtAtlas.getSpriteFrame(cc.ll.propData.role.data[this.resid].skilltype);
        cc.find('roleInfo', this.roleInfoNode).getComponent(cc.Sprite).spriteFrame = this.TxtAtlas.getSpriteFrame(cc.ll.propData.role.data[this.resid].info);
        cc.find('roleNamebg/roleName', this.roleNameNode).getComponent(cc.Sprite).spriteFrame = this.TxtAtlas.getSpriteFrame(cc.ll.propData.role.data[this.resid].name);
    },

    updateMask() {
        let hadChange = false;
        if (this.roleInfoNode.height < cc.find('roleInfo', this.roleInfoNode).height) {
            this.roleInfoNode.height += 3;
            hadChange = true;
        }
        if (this.roleNameNode.height < cc.find('roleNamebg', this.roleNameNode).height) {
            this.roleNameNode.height += 3;
            hadChange = true;
        }
        if (this.skillLineNode.width < cc.find('skillLine', this.skillLineNode).width) {
            this.skillLineNode.width += 3;
            hadChange = true;
        }
        if (!hadChange) {
            this.unschedule(this.updateMask);
        }
    },

    roleInfoRun() {
        this.roleDragonRun();
        this.changeRoleInfo();
        this.unschedule(this.updateMask);
        this.schedule(this.updateMask, 0.01, cc.macro.REPEAT_FOREVER);

        for (let index = 0; ; index++) {
            let toggle = cc.find(`sexToggleContainer/toggle${index + 1}`, this.menuNode);
            if (toggle == null) {
                break;
            }
            toggle.getComponent(cc.Toggle).isChecked = false;
        }
        for (let index = 0; ; index++) {
            let toggle = cc.find(`typeToggleContainer/toggle${index + 1}`, this.menuNode);
            if (toggle == null) {
                break;
            }
            toggle.getComponent(cc.Toggle).isChecked = false;
        }
        let roleSex = this.roleIndex % 2;
        let roleType = Math.floor(this.roleIndex / 2);
        cc.find(`sexToggleContainer/toggle${roleSex + 1}`, this.menuNode).getComponent(cc.Toggle).isChecked = true;
        cc.find(`typeToggleContainer/toggle${roleType + 1}`, this.menuNode).getComponent(cc.Toggle).isChecked = true;
    },

    typeToggleClicked(e, d) {
        if (!this.canChangeRole) {
            return;
        }
        let curIndex = d * 2;
        if (curIndex != this.roleIndex) {
            if (this.roleIndex > curIndex) {
                this.roleChangeDirection = -1;
            }
            else {
                this.roleChangeDirection = 1;
            }
            this.roleIndex = curIndex;
            this.changeRole();
        }
    },

    sexToggleClicked(e, d) {
        if (!this.canChangeRole) {
            return;
        }
        let curIndex = Math.floor(this.roleIndex / 2) * 2;
        if (d == 1) {
            curIndex += 1;
        }
        if (curIndex != this.roleIndex) {
            if (this.roleIndex > curIndex) {
                this.roleChangeDirection = -1;
            }
            else {
                this.roleChangeDirection = 1;
            }
            this.roleIndex = curIndex;
            this.changeRole();
        }
    },

    touchBegan(event) {
        this.beganPos = event.getLocation();
    },

    touchMoved(event) {
        if (!this.onTouched && cc.ll.pDistance(cc.v2(0, 0), event.getDelta())) {
            this.onTouched = true;
        }
        if (!this.onTouched) {
            return;
        }
        this.resetInfoLayer();
        this.movedPos = event.getLocation();
        if (this.movedPos.x - this.beganPos.x > 100) {
            this.onChangeRole(null, 0);
        }
        else if (this.beganPos.x - this.movedPos.x > 100) {
            this.onChangeRole(null, 1);
        }
        this.roleNode.x += event.getDelta().x;
    },

    touchEnded(event) {
        if (!this.onTouched) {
            return;
        }
        this.onTouched = false;
        this.roleNode.runAction(cc.moveTo(0.1, cc.v2(this.rolePosX, this.rolePosY)));
        this.roleInfoRun();
    },

    touchCanceled(event) {
        if (!this.onTouched) {
            return;
        }
        this.onTouched = false;
        this.roleNode.runAction(cc.moveTo(0.1, cc.v2(this.rolePosX, this.rolePosY)));
        this.roleInfoRun();
    },

    getRandomName(e, d) {
        let v1 = ["思", "烟", "冰", "琴", "夜", "蓝", "痴", "梦", "依", "丹", "小", "柳", "香", "冬", "绿", "萍", "向", "菱", "映", "寒", "阳", "含", "曼", "霜", "春", "白", "南", "醉", "丝", "之", "新", "真", "雨", "露", "天", "云", "寄", "芙", "如", "筠", "容", "若", "涵", "荷", "风", "亦", "儿", "采", "雪", "谷", "巧", "凌", "山", "安", "蕾", "从", "芷", "芹", "绮", "雅", "柔", "飞",
            "锦", "程", "修", "杰", "烨", "伟", "尔", "曼", "立", "辉", "致", "远", "天", "思", "友", "绿", "聪", "健", "洁", "访", "琴", "初", "彤", "谷", "雪", "平", "灵", "源", "智", "华", "振", "家", "越", "彬", "乞", "子", "轩", "宸", "晋", "鹏", "觅", "松", "海", "亦", "戾", "嵩", "邑", "瑛", "鸿", "卿", "裘", "契", "涛", "疾", "驳", "凛", "逊", "鹰", "威", "紊", "阁", "哈"];
        let name = v1[Math.floor(Math.random() * v1.length)];
        name += v1[Math.floor(Math.random() * v1.length)];
        if (Math.random() > 0.1) {
            name += v1[Math.floor(Math.random() * v1.length)];
        }
        if (Math.random() > 0.6) {
            name += v1[Math.floor(Math.random() * v1.length)];
        }
        this.nameEditbox.string = name;
    },




    GetNameLength(strName) {
        let nCnt = 0;

        let nLen = strName.length;

        for (let i = 0; i < nLen; i++) {
            if (/[a-zA-Z0-9]/.test(strName[i]))
                nCnt += 1;
            else
                nCnt += 2;
        }

        return nCnt;

    },




    creatRole(e, d) {
        let username = this.nameEditbox.string;
        let flitlist = GameDefine.limitWordList;
        for (let i = 0; i < flitlist.length; i++) {
            const fword = flitlist[i];
            if (username.indexOf(fword) != -1) {
                cc.ll.notice.addMsg(0, MsgCode.CREATE_NAME_INVALID, () => {
                });
                return;
            }
        }

        cc.ll.loading.showLoading(10);

        let nUserNameLen = this.GetNameLength(username);

        if (nUserNameLen > 12) {
            cc.ll.notice.addMsg(0, MsgCode.CREATE_NAME_INVALID, () => { });
            return;
        }

        let roleData = {
            name: username,
            race: define.RaceType.Demon + Math.floor(this.roleIndex / 2),
            sex: define.SexType.Male + this.roleIndex % 2,
            resid: this.resid,
            accountid: cc.ll.player.accountid,
            serverid: cc.ll.server.id,
        };

        cc.ll.http.send('/createRole', roleData, ret => {
            cc.ll.loading.unshowLoading();
            if (ret.result != MsgCode.SUCCESS) {
                cc.ll.msgbox.addMsg(ret.result);
                return;
            }

            let loginfunc = require('./LoginFunc');
            loginfunc.login(cc.ll.player.accountid, ret.roleid, cc.ll.server.id);
        });
    },

    backBtnClicked(e, d) {
        if (this.opentype == OpenType.CREATE) {
            cc.ll.sceneMgr.changeScene('LoginScene');
        }
        else if (this.opentype == OpenType.RELIVE) {
            this.node.destroy();
        }
        else if (this.opentype == OpenType.CHANGERACE) {
            this.node.destroy();
        }
    },
});