cc.Class({
    extends: cc.Component,

    properties: {
        serverLab: cc.Label,
        nameLab: cc.Label,
        lineNode: cc.Node,
        headFrame: cc.Node,
        headIcon: cc.Sprite,
        levelLab: cc.Label,
        headAtlas: cc.SpriteAtlas,
    },

    onLoad() {
        this.data = {};
    },

    showInfo(data){
        this.data = data;
        this.serverLab.string = data.servername;
        if (!data.registed) {
            this.nameLab.node.active = false;
            this.lineNode.active = false;
            this.headFrame.active = false;
        }else{
            this.nameLab.string = data.name;
            this.levelLab.string = data.level;
            this.headIcon.spriteFrame = this.headAtlas.getSpriteFrame('role_' + data.resid);
        }
    },

    start() {
    },
});