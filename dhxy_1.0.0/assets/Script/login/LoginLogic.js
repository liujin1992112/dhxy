var StrCommon = require('../etc/str_common');
let player = require('../player/player');
let define = require('../common/define');
var MsgCode = require('../etc/msg_code');

cc.Class({
    extends: cc.Component,

    properties: {
        serverItem: cc.Prefab,
        serverContent: cc.Node,
        loginNode: cc.Node,
        loginGameNode: cc.Node,
        changePassBtn: cc.Node,

        nameEditBox: cc.EditBox,
        psdEditBox: cc.EditBox,
        chooseNode: cc.Node,
        chooseLeft: cc.Node,
        chooseList: cc.Node,

        creatRoleNode: cc.Prefab,
        noticePre: cc.Prefab,

        curServerName: cc.Label,

        updateNode: cc.Node,
        Tips: cc.Label,
        operationLayer: cc.Node,
        VersionLabel: cc.Label,
        loadingProgress: cc.ProgressBar,

        preRegistUI: cc.Prefab,

        video: cc.VideoPlayer,
    },

    ctor() {
        this.curServerData = null;
        this.version = '0.9.1135';
    },

    onLoad() {
        this.account = cc.sys.localStorage.getItem('account');
        this.password = cc.sys.localStorage.getItem('password');
        if (this.account != null && this.account != '') {
            this.nameEditBox.string = this.account;
            this.psdEditBox.string = this.password;
        }
        this.loginGameNode.active = false;
        this.chooseNode.active = false;

        cc.find('titlebg/role', this.chooseList).active = false;
        cc.find('titlebg/server', this.chooseList).active = false;



        // cc.ll.AudioMgr.setMusicVolume(0);
        // cc.ll.AudioMgr.setVoiceVolume(0);
        // cc.ll.AudioMgr.setEffectVolume(0);
    },

    start() {
        // var Config = require('../etc/config');
        // cc.ll.net.connect(Config.ServerHttp, Config.ServerPort);
        if (cc.sys.isNative && cc.sys.os != cc.sys.OS_WINDOWS) {
            this.loginNode.active = false;
            this.changePassBtn.active = false;
            this.updateNode.active = true;
            this.operationLayer.active = false;

            let self = this;
            cc.loader.loadRes("project", function (err, manifest) {
                self.manifest = manifest;
                self.checkUpdate();
            });
        } else {
            this.checkNotice();
            this.loginNode.active = true;
            this.changePassBtn.active = true;
            this.updateNode.active = false;
        }

        if (cc.sys.isBrowser || cc.sys.os == cc.sys.OS_ANDROID) {
            cc.ll.AudioMgr.playBgm('login');
        }

        // let sharderMgr = require('../utils/ShaderMgr');
        // let spr = this.loginNode.getComponent(cc.Sprite);
        // sharderMgr.setShader(spr, 'Banish');
        this.setVersion(this.version);
    },

    /**
     * 登录玩家账号信息
     * @param {*} e 
     * @param {*} d 
     * @returns 
     */
    onLoginBtnClicked(e, d) {
        this.account = this.nameEditBox.string;
        this.password = this.psdEditBox.string;

        if (this.account == '') {
            cc.ll.msgbox.addMsg(StrCommon.AccountNotNull);
            return;
        }

        if (this.password == '') {
            cc.ll.msgbox.addMsg(StrCommon.PsdNotNull);
            return;
        }


        let mac = cc.sys.localStorage.getItem('mac');
        if (mac == null || mac == '') {
            let aimu = ['a', 'A', 'b', 'B', 'c', 'C', 'd', 'D', 'e', 'E', 'f', 'F', 'g', 'G', 'h', 'H', 'i', 'I', 'j', 'J', 'k', 'K', 'l', 'L', 'm', 'M', 'n', 'N', 'o', 'O', 'p', 'P', 'q', 'Q', 'r', 'R', 's', 'S', 't', 'T', 'w', 'W', 'v', 'V', 'u', 'U', 'x', 'X', 'y', 'Y', 'z', 'Z', 'Z'];
            let temp = '';
            for (let i = 0; i < 13; i++) {
                let index = Math.floor(Math.random() * aimu.length);
                if (index == aimu.length) {
                    index = aimu.length - 1;
                }
                temp += aimu[index];
            }
            temp += Math.ceil(Math.random() * 800000) + 100000;
            temp += Date.now();
            cc.sys.localStorage.setItem('mac', temp);
            mac = temp;
        }

        let loginData = {
            account: this.account,
            password: this.password,
            gametype: cc.ll.config.debug ? 'debug' : 'release',
            version: this.version,
            mac: mac,
        };

        cc.ll.loading.showLoading(40);
        cc.ll.http.send('/login', loginData, ret => {
            if (ret.result != MsgCode.SUCCESS) {
                cc.ll.notice.addMsg(0, ret.result, () => { });
                cc.ll.loading.unshowLoading();
                return;
            }

            cc.sys.localStorage.setItem('account', this.account);
            cc.sys.localStorage.setItem('password', this.password);

            let p = new player(ret.accountid);
            p.setInfo(ret);
            cc.ll.player = p;

            this.loginNode.active = false;
            this.changePassBtn.active = false;

            this.loginGameNode.active = true;
            // this.animationNode.active = true;

            let serverData = {
                accountid: ret.accountid,
            };
            cc.ll.http.send('/serList', serverData, ret => {
                cc.ll.loading.unshowLoading();
                if (ret.result != MsgCode.SUCCESS) {
                    cc.ll.msgbox.addMsg(ret.errcode);
                    return;
                }
                this.dealServerData(ret);
            });
        });
    },

    dealServerData(data) {
        this.roleData = [];
        this.serverData = [];
        this.tuijianData = [];

        //遍历玩家角色列表,将角色信息填充到服务器信息中
        for (const role of data.rolelist) {
            //判断角色所在游戏服是否存在
            if (data.serlist[role.serverid] == null) {
                continue;
            }
            data.serlist[role.serverid].roleid = role.roleid;
            data.serlist[role.serverid].name = role.name;
            data.serlist[role.serverid].race = role.race;
            data.serlist[role.serverid].sex = role.sex;
            data.serlist[role.serverid].resid = role.resid;
            data.serlist[role.serverid].level = role.level;
            data.serlist[role.serverid].registed = true;
            data.serlist[role.serverid].mapid = role.mapid;
        }

        //遍历玩家角色列表,将服务器信息充到角色信息中
        for (const role of data.rolelist) {
            //判断角色所在游戏服是否存在
            if (data.serlist[role.serverid] == null) {
                continue;
            }
            role.registed = true;
            role.id = data.serlist[role.serverid].id;
            role.servername = data.serlist[role.serverid].servername;

            this.roleData.push(role);
        }

        //推荐服
        for (const tuijian of data.guide) {
            if (data.serlist[tuijian] == null) {
                continue;
            }
            this.tuijianData.push(data.serlist[tuijian]);
        }
        for (const sid in data.serlist) {
            this.serverData.push(data.serlist[sid]);
        }
        if (this.roleData.length > 0) {
            let roleid = cc.sys.localStorage.getItem('LoginRole');
            if (roleid) {
                for (let item of this.roleData)
                    if (item.roleid == roleid)
                        this.curServerData = item;
            }
            if (!this.curServerData) {
                this.curServerData = this.roleData[0];
            }
            this.showServerList(this.roleData);
        } else if (this.tuijianData.length > 0) {
            this.curServerData = this.tuijianData[0];
            this.showServerList(this.tuijianData);
        } else {
            this.curServerData = this.serverData[0];
            this.showServerList(this.serverData);
        }
        this.curServerName.string = this.curServerData.servername;
    },

    onRegistBtnClicked(e, d) {
        let goRegistUI = cc.instantiate(this.preRegistUI);
        goRegistUI.parent = this.node;
        goRegistUI.setPosition(cc.v2(0, 0));


        /*
                this.account = this.nameEditBox.string;
                this.password = this.psdEditBox.string;

                if (this.account == '')
                {
                    cc.ll.msgbox.addMsg(StrCommon.AccountNotNull);
                    return;
                }

                if (this.password == '')
                {
                    cc.ll.msgbox.addMsg(StrCommon.PsdNotNull);
                    return;
                }

                let loginData = {
                    account: this.account,
                    password: this.password
                };
                cc.ll.http.send('/register', loginData, ret =>
                {
                    if (ret.result != MsgCode.SUCCESS)
                    {
                        cc.ll.msgbox.addMsg(ret.result);
                        // cc.ll.loading.unshowLoading();
                        return;
                    }
                    this.onLoginBtnClicked(0, 0, this.account, this.password);
                });

        */
    },

    showServerList(list) {
        this.serverContent.destroyAllChildren();
        // this.serverContent.height = Math.ceil(list.length / 2) * 100;
        // if (this.serverContent.height < 470) {
        //     this.serverContent.height = 470;
        // }
        let height = 470;
        for (let index = 0; index < list.length; index++) {
            let item = cc.instantiate(this.serverItem);
            let btn = item.getComponent(cc.Button);
            var clickEventHandler = new cc.Component.EventHandler();
            clickEventHandler.target = this.node;
            clickEventHandler.component = "LoginLogic";
            clickEventHandler.handler = "serverItemClicked";
            clickEventHandler.customEventData = list[index];
            btn.clickEvents.push(clickEventHandler);

            let itemlogic = item.getComponent('LoginServerItem');
            itemlogic.showInfo(list[index]);
            item.parent = this.serverContent;
            item.x = -180 + (index % 2) * 360;
            item.y = -Math.floor(index / 2) * 100;
            if (-item.y + 100 > height) {
                height = -item.y + 100;
            }
        }
        this.serverContent.height = height;
    },

    serverItemClicked(e, d) {
        this.curServerData = d;
        this.curServerName.string = d.servername;
        this.onBackBtnClicked();
    },

    /**
     * 选择完服务器,登录游戏服务器
     * @param {*} e 
     * @param {*} d 
     * @returns 
     */
    onLoginGameClicked(e, d) {
        /*
            id: 1000
            ip: 0
            port: 0
            servername: "测试1服"
            */
        if (this.curServerData == null) {
            return;
        }
        cc.ll.server = this.curServerData;

        if (this.curServerData) {
            let roleid = this.curServerData.roleid;
            cc.sys.localStorage.setItem('LoginRole', roleid);
        }

        //当前服务器是否注册过角色
        if (this.curServerData.registed) {
            //注册过
            cc.ll.loading.showLoading(30);
            let loginfunc = require('./LoginFunc');

            loginfunc.login(cc.ll.player.accountid, this.curServerData.roleid, cc.ll.server.id);
        } else {
            //未注册过
            let create = cc.instantiate(this.creatRoleNode);
            create.parent = this.node;
        }
    },

    onChooseBtnClicked(e, d) {
        // this.chooseLeft.stopAllActions();
        // this.chooseList.stopAllActions();
        this.loginGameNode.runAction(cc.fadeOut(0.8));
        this.chooseNode.active = true;
        // this.chooseLeft.runAction(cc.moveTo(0.2, cc.v2(-512, this.chooseLeft.y)));
        // this.chooseList.runAction(cc.moveTo(0.2, cc.v2(this.chooseList.x, -60)));
    },

    onVideoClicked() {
        this.video.node.active = false;
        cc.ll.videoPlayed = true;
        cc.ll.AudioMgr.playBgm('login');
    },

    videoOnEmitEvent(videoplayer, eventType, customEventData) {
        //这里 videoplayer 是一个 VideoPlayer 组件对象实例
        // 这里的 eventType === cc.VideoPlayer.EventType enum 里面的值
        //这里的 customEventData 参数就等于你之前设置的 "foobar"
        if (eventType == cc.VideoPlayer.EventType.COMPLETED) {
            this.video.node.active = false;
            cc.ll.videoPlayed = true;
            cc.ll.AudioMgr.playBgm('login');
        }
    },

    onServerListClicked(e, d) {
        if (d == 1) {
            this.showServerList(this.roleData);
        } else if (d == 2) {
            this.showServerList(this.tuijianData);
        } else if (d == 3) {
            this.showServerList(this.serverData);
        }
    },

    onBackBtnClicked(e, d) {
        this.loginGameNode.runAction(cc.fadeIn(0.8));
        this.chooseNode.active = false;
        // this.chooseLeft.runAction(cc.moveBy(0.2, cc.v2(-200, 0)));
        // this.chooseList.runAction(cc.moveBy(0.2, cc.v2(0, -700)));
    },

    onNoticeBtnClicked(e, d) {
        this.checkNotice();
    },


    //////////////////////////////////////////////////////////////////
    setVersion(ver) {
        // let yinying = cc.find('Version', this.VersionLabel.node);
        // let yinyinglabel = yinying.getComponent(cc.Label);
        // yinyinglabel.string = "Ver. " + ver;
        this.VersionLabel.string = "Ver. " + ver;
        this.version = ver;
    },

    checkNotice() {
        // 先播放动画
        if ((cc.ll.videoPlayed == null || cc.ll.videoPlayed == false)
            && (cc.sys.OS_IOS == cc.sys.os || cc.sys.OS_WINDOWS == cc.sys.os)) {
            this.video.node.active = true;
            this.video.play();
            this.video.node.on('clicked', this.onVideoClicked, this);
            this.video.node.on('stopped', this.onVideoClicked, this);
            this.video.node.on('completed', this.onVideoClicked, this);
        }

        let notice = cc.instantiate(this.noticePre);
        notice.parent = this.node;
    },

    showLogin() {
        this.loadingProgress.progress = 1;
        this.Tips.string = '正在进入...';
        this.scheduleOnce(() => {
            this.updateNode.active = false;
            this.loginNode.active = true;
            this.changePassBtn.active = true;
            this.checkNotice();
        }, 0.5);
    },

    checkUpdate() {
        if (!cc.sys.isNative) {
            this.showLogin();
            return;
        }

        this._storagePath = ((jsb.fileUtils ? jsb.fileUtils.getWritablePath() : '/') + 'xygame');
        this.versionCompareHandle = function (versionA, versionB) {
            var vA = versionA.split('.');
            var vB = versionB.split('.');
            for (var i = 0; i < vA.length; ++i) {
                var a = parseInt(vA[i]);
                var b = parseInt(vB[i] || 0);
                if (a === b) {
                    continue;
                } else {
                    return a - b;
                }
            }
            if (vB.length > vA.length) {
                return -1;
            } else {
                return 0;
            }
        };
        this._am = new jsb.AssetsManager('', this._storagePath, this.versionCompareHandle);
        let self = this;
        this._am.setVerifyCallback(function (path, asset) {
            var compressed = asset.compressed;
            var expectedMD5 = asset.md5;
            var relativePath = asset.path;
            var size = asset.size;
            if (compressed) {
                // self.Tips.string = "Verification passed : " + relativePath;
                return true;
            } else {
                // self.Tips.string = "Verification passed : " + relativePath + ' (' + expectedMD5 + ')';
                return true;
            }
        });

        if (cc.sys.os === cc.sys.OS_ANDROID) {
            this._am.setMaxConcurrentTask(5);
        }

        if (this._am.getState() === jsb.AssetsManager.State.UNINITED) {
            var url = this.manifest.nativeUrl;
            if (cc.loader.md5Pipe) {
                url = cc.loader.md5Pipe.transformURL(url);
            }
            this._am.loadLocalManifest(url);
        }

        if (!this._am.getLocalManifest() || !this._am.getLocalManifest().isLoaded()) {
            this.showLogin();
            return;
        }
        this._am.setEventCallback(this.checkCb.bind(this));
        this.setVersion(this._am.getLocalManifest().getVersion());
        this._am.checkUpdate();
        this._updating = true;
    },

    checkCb: function (event) {
        let toloading = false;
        switch (event.getEventCode()) {
            case jsb.EventAssetsManager.ERROR_NO_LOCAL_MANIFEST:
                cc.log("您的版本过低，请下载最新版本");
                this.Tips.string = "您的版本过低，请下载最新版本";
                break;
            case jsb.EventAssetsManager.ERROR_DOWNLOAD_MANIFEST:
            case jsb.EventAssetsManager.ERROR_PARSE_MANIFEST:
                // this.Tips.string = "";
                // this.showUpdateError();
                cc.log("Fail to download manifest file, hot update skipped.");

                toloading = true;
                break;
            case jsb.EventAssetsManager.ALREADY_UP_TO_DATE:
                // this.Tips.string = "Already up to date with the latest remote version.";
                cc.log("Already up to date with the latest remote version.");
                toloading = true;
                break;
            case jsb.EventAssetsManager.NEW_VERSION_FOUND:
                this.operationLayer.active = true;
                this.Tips.string = '发现新版本\n是否现在更新？';
                break;
            default:
                return;
        }

        this._am.setEventCallback(null);
        this._checkListener = null;
        this._updating = false;
        if (toloading) {
            this.showLogin();
        }
    },


    updateCb: function (event) {
        var needRestart = false;
        var failed = false;
        switch (event.getEventCode()) {
            case jsb.EventAssetsManager.ERROR_NO_LOCAL_MANIFEST:
                this.Tips.string = 'No local manifest file found, hot update skipped.';
                failed = true;
                break;
            case jsb.EventAssetsManager.UPDATE_PROGRESSION:
                let per = event.getPercent();
                cc.log(per * 100 + '%');
                if (typeof (per) == 'number') {
                    per = per * 100;
                    if (per > 100) {
                        per = 100;
                    }
                    if (isNaN(per)) {
                        pre = 0;
                    }
                    this.Tips.string = per.toFixed(2) + '%';
                    this.loadingProgress.progress = per / 100;
                } else {
                    if (this.update_n >= 90 && this.update_n < 100) {
                        this.update_n += 1;
                    } else if (this.update_n >= 100) {
                        this.update_n = 100;
                    } else {
                        this.update_n += 10;
                    }
                    this.Tips.string = this.update_n + '%';
                }
                break;
            case jsb.EventAssetsManager.ERROR_DOWNLOAD_MANIFEST:
            case jsb.EventAssetsManager.ERROR_PARSE_MANIFEST:
                // this.Tips.string = 'Fail to download manifest file, hot update skipped.';
                failed = true;
                break;
            case jsb.EventAssetsManager.ALREADY_UP_TO_DATE:
                // this.Tips.string = 'Already up to date with the latest remote version.';
                failed = true;
                break;
            case jsb.EventAssetsManager.UPDATE_FINISHED:
                // this.Tips.string = 'Update finished. ' + event.getMessage();
                this.Tips.string = '更新完成';
                needRestart = true;
                break;
            case jsb.EventAssetsManager.UPDATE_FAILED:
                // this.Tips.string = 'Update failed. ' + event.getMessage();
                // this.panel.retryBtn.active = true;
                this._updating = false;
                this._canRetry = true;
                break;
            case jsb.EventAssetsManager.ERROR_UPDATING:
                // this.Tips.string = 'Asset update error: ' + event.getAssetId() + ', ' + event.getMessage();
                failed = true;
                break;
            case jsb.EventAssetsManager.ERROR_DECOMPRESS:
                // this.Tips.string = event.getMessage();
                break;
            default:
                break;
        }

        if (failed) {
            this._am.setEventCallback(null);
            this.Tips.string = "更新失败，是否重试？";
            this.operationLayer.active = true;
            this._updateListener = null;
            this._updating = false;
        }

        if (needRestart) {
            this._am.setEventCallback(null);
            this._updateListener = null;
            // Prepend the manifest's search path
            var searchPaths = jsb.fileUtils.getSearchPaths();
            var newPaths = this._am.getLocalManifest().getSearchPaths();

            Array.prototype.unshift(searchPaths, newPaths);
            // This value will be retrieved and appended to the default search path during game startup,
            // please refer to samples/js-tests/main.js for detailed usage.
            // !!! Re-add the search paths in main.js is very important, otherwise, new scripts won't take effect.
            cc.sys.localStorage.setItem('HotUpdateSearchPaths', JSON.stringify(searchPaths));
            jsb.fileUtils.setSearchPaths(searchPaths);

            cc.audioEngine.stopAll();
            cc.game.restart();
        }
    },

    hotUpdate: function () {
        if (this._am && !this._updating) {
            this._am.setEventCallback(this.updateCb.bind(this));

            if (this._am.getState() === jsb.AssetsManager.State.UNINITED) {
                // Resolve md5 url
                var url = this.manifestUrl.nativeUrl;
                if (cc.loader.md5Pipe) {
                    url = cc.loader.md5Pipe.transformURL(url);
                }
                this._am.loadLocalManifest(url);
            }

            this._failCount = 0;
            this._am.update();
            // this.panel.updateBtn.active = false;
            this._updating = true;
        }
    },

    onExitClicked(e, d) {
        cc.game.end();
    },

    onUpdateClicked(e, d) {
        this.Tips.string = '正在更新';
        // let pbar = cc.find('progressBar', this.updateLayer);
        // pbar.active = true;
        this.operationLayer.active = false;
        this.hotUpdate();
    },
});