﻿
let CPubFunction = require('../game/PubFunction');
let MsgCode = require('../etc/msg_code');

cc.Class({

    extends: cc.Component,

    properties:
    {

    },

    onLoad() {
    },

    start() {
        cc.find('btnClose', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "RegistUI", "Close", 0));
        cc.find('btnOK', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "RegistUI", "OnOK", 0));
    },

    Close(e, d) {
        this.node.destroy();
    },

    OnOK(e, d) {

        let strName = cc.find('editName', this.node).getComponent(cc.EditBox).string;
        let strPwd = cc.find('editPwd', this.node).getComponent(cc.EditBox).string;
        let strRepeat = cc.find('editRepeat', this.node).getComponent(cc.EditBox).string;
        let strCode = cc.find('editCode', this.node).getComponent(cc.EditBox).string;

        if (strCode == '') {
            strCode = '';//'666666'
        }

        let strNameErr = this.Check(strName, /[a-zA-Z0-9]/, 6, 20);
        if (strNameErr != '') {
            cc.ll.msgbox.addMsg(`用户名${strNameErr}`);
            return;
        }

        if (/^[0-9]*$/.test(strName)) {
            cc.ll.msgbox.addMsg(`用户名不能纯数字`);
            return;
        }

        let strPwdErr = this.Check(strPwd, /[a-zA-Z0-9]/, 6, 20);
        if (strPwdErr != '') {
            cc.ll.msgbox.addMsg(`密码${strPwdErr}`);
            return;
        }


        if (strPwd != strRepeat) {
            cc.ll.msgbox.addMsg('密码不一致');
            return;
        }

        /*
        if (this.Check(strCode, /[a-zA-Z0-9]/, 6, 6) != '') {
            cc.ll.msgbox.addMsg('邀请码无效');
            return;
        }
        */

        cc.ll.loading.showLoading(10);
        cc.ll.http.send('/register', { account: strName, password: strPwd, invitecode: strCode }, ret => {
            cc.ll.loading.unshowLoading();

            if (ret.result != MsgCode.SUCCESS) {
                cc.ll.msgbox.addMsg(ret.result);
                return;
            }

            cc.ll.notice.addMsg(0, MsgCode.SUCCESS, () => { });

            this.DoCloseAndLogin(strName, strPwd);

        });

    },


    DoCloseAndLogin(strAccount, strPwd) {
        let login = this.node.parent.getComponent('LoginLogic');
        login.account = strAccount;
        login.password = strPwd;
        login.nameEditBox.string = login.account;
        login.psdEditBox.string = login.password;
        this.node.destroy();
    },


    Check(strData, strRule, nMin, nMax) {
        if (strData.length < nMin || strData.length > nMax)
            return `长度必须在${nMin}~${nMax}位之间`;

        if (strRule.test(strData) == false)
            return '包含非法字符'

        return '';
    }


});
