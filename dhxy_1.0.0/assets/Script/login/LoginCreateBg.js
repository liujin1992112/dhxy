cc.Class({
    extends: cc.Component,

    properties: {
        backDragon: dragonBones.ArmatureDisplay,
        frontDragon: dragonBones.ArmatureDisplay,
        leftDragon: dragonBones.ArmatureDisplay,
        rightDragon: dragonBones.ArmatureDisplay,
    },

    start() {
        // this.playDragon();
    },

    loadInfo(info) {
        this.loadDragon(info.back, info.left, info.right, info.front);
    },

    playDragon() {
        this.backDragon.playAnimation('in');
        this.frontDragon.playAnimation('in');
        this.leftDragon.playAnimation('in');
        this.rightDragon.playAnimation('in');
        this.scheduleOnce(() => {
            this.backDragon.playAnimation('idle', 0);
            this.frontDragon.playAnimation('idle', 0);
            this.leftDragon.playAnimation('idle', 0);
            this.rightDragon.playAnimation('idle', 0);
        }, 1);
    },

    loadDragon(bgname, leftname, rightname, frontname) {
        let self = this;
        cc.loader.loadResDir(`dragonbone/${bgname}`, function (err, assets) {
            cc.sys.isNative && self.backDragon._factory.clear(false);//在手机上加载相同的资源会出现闪退
            self.backDragon.dragonAsset = null;
            self.backDragon.dragonAtlasAsset = null;
            for (let i = 0; i < assets.length; i++) {
                if (assets[i] instanceof dragonBones.DragonBonesAsset) {
                    self.backDragon.dragonAsset = assets[i];
                }
                if (assets[i] instanceof dragonBones.DragonBonesAtlasAsset) {
                    self.backDragon.dragonAtlasAsset = assets[i];
                }
            }
            self.backDragon.armatureName = 'armatureName';
        });

        cc.loader.loadResDir(`dragonbone/${frontname}`, function (err, assets) {
            cc.sys.isNative && self.frontDragon._factory.clear(false);//在手机上加载相同的资源会出现闪退
            self.frontDragon.dragonAsset = null;
            self.frontDragon.dragonAtlasAsset = null;
            for (let i = 0; i < assets.length; i++) {
                if (assets[i] instanceof dragonBones.DragonBonesAsset) {
                    self.frontDragon.dragonAsset = assets[i];
                }
                if (assets[i] instanceof dragonBones.DragonBonesAtlasAsset) {
                    self.frontDragon.dragonAtlasAsset = assets[i];
                }
            }
            self.frontDragon.armatureName = 'armatureName';
        });

        cc.loader.loadResDir(`dragonbone/${leftname}`, function (err, assets) {
            cc.sys.isNative && self.leftDragon._factory.clear(false);//在手机上加载相同的资源会出现闪退
            self.leftDragon.dragonAsset = null;
            self.leftDragon.dragonAtlasAsset = null;
            for (let i = 0; i < assets.length; i++) {
                if (assets[i] instanceof dragonBones.DragonBonesAsset) {
                    self.leftDragon.dragonAsset = assets[i];
                }
                if (assets[i] instanceof dragonBones.DragonBonesAtlasAsset) {
                    self.leftDragon.dragonAtlasAsset = assets[i];
                }
            }
            self.leftDragon.armatureName = 'armatureName';
        });

        cc.loader.loadResDir(`dragonbone/${rightname}`, function (err, assets) {
            cc.sys.isNative && self.rightDragon._factory.clear(false);//在手机上加载相同的资源会出现闪退
            self.rightDragon.dragonAsset = null;
            self.rightDragon.dragonAtlasAsset = null;
            for (let i = 0; i < assets.length; i++) {
                if (assets[i] instanceof dragonBones.DragonBonesAsset) {
                    self.rightDragon.dragonAsset = assets[i];
                }
                if (assets[i] instanceof dragonBones.DragonBonesAtlasAsset) {
                    self.rightDragon.dragonAtlasAsset = assets[i];
                }
            }
            self.rightDragon.armatureName = 'armatureName';
        });
        this.playDragon();
    },

    delSelf() {
        let self = this;
        this.node.runAction(cc.sequence(cc.fadeTo(1, 50), cc.removeSelf()));
    }
});