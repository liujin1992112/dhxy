
cc.Class({
    extends: cc.Component,

    properties: {
        bg: cc.Node,
        text: cc.RichText,
    },

    start () {
        this.node.scale = 0.1;
        this.node.runAction(cc.scaleTo(0.3, 1).easing(cc.easeBackOut(3.0)));
        let self = this;
        cc.ll.http.send('/gonggao', {}, ret => {
            self.setText(ret.text);
        });
    },

    close(){
        this.node.runAction(cc.sequence(
            cc.scaleTo(0.3, 0.1).easing(cc.easeBackIn(3.0)),
            cc.removeSelf(),
        ));
    },

    setText(txt){
        this.text.string = txt;

        this.scheduleOnce(()=>{
            this.bg.height = this.text.node.height + 50;
        }, 0);
    }
});
