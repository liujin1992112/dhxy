let Net = require('../network/Net');
var MsgCode = require('../etc/msg_code');

let tryTimer = 0;

function clearTryTimer() {
	clearTimeout(tryTimer);
	tryTimer = 0;
}
exports.clearTryTimer = clearTryTimer;

/**
 * 登录游戏服务器
 * @param {*} accountid 账户ID
 * @param {*} roleid 	角色ID
 * @param {*} serverid 	服务器ID
 * @param {*} callback 
 */
exports.login = (accountid, roleid, serverid, callback) => {
	clearTryTimer();

	//向网关服务器请求当前服务器ID对应的ip和端口号信息
	cc.ll.http.send('/toServer', {
		accountid: accountid,
		serverid: serverid
	}, (ret) => {
		if (ret.result == null) {
			cc.ll.notice.addMsg(0, MsgCode.NEED_RELOGIN, () => {
				cc.ll.sceneMgr.changeScene('LoginScene');
			});
			return;
		}
		if (ret.result != MsgCode.SUCCESS) {
			cc.ll.msgbox.addMsg(ret.result);
			cc.ll.loading.unshowLoading();
			return;
		}
		let ip = ret.ip;
		let port = ret.port;
		let token = ret.token;
		let canRelogin = true;

		let connect = () => {
			let net = new Net();
			net.connect(ip, port, (Lnet) => {
				if (tryTimer == 0) {
					Lnet.send('c2s_login', {
						accountid: accountid,
						roleid: roleid,
						token: token,
					});
				} else {
					if (canRelogin == false) {
						return;
					}
					Lnet.send('c2s_relogin', {
						accountid: accountid,
						roleid: roleid,
						token: token,
					});
					clearTryTimer();
				}

				cc.ll.net = Lnet;

				if (callback) {
					callback();
				}
			}, () => {
				connect();
				tryTimer = setTimeout(() => {
					console.log('网络断开连接')
					canRelogin = false;
					cc.ll.loading.unshowLoading();
					cc.ll.notice.addMsg(0, MsgCode.NETWORK_ERROR, () => {
						let battlelayer = cc.find('Canvas/BattleLayer');
						if (battlelayer) {
							battlelayer.destroyAllChildren();
						}
						cc.ll.sceneMgr.changeScene('LoginScene');
					});
				}, 5 * 1000);
			});
		}

		//连接游戏服务器
		connect();
	})
}
