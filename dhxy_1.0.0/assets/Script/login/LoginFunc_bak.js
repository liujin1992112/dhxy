let Net = require('../network/Net');
var MsgCode = require('../etc/msg_code');

exports.login = (accountid, serverid, callback) => {
	cc.ll.http.send('/toServer', {
		accountid: accountid,
		serverid: serverid
	}, (ret) => {
		if (ret.result != MsgCode.SUCCESS) {
			cc.ll.msgbox.addMsg(ret.result);
			cc.ll.loading.unshowLoading();
			return;
		}
		let ip = ret.ip;
		let port = ret.port;
		let token = ret.token;


		let connect = () => {
			let trytime = 3;
			let net = new Net();
			let errorcallback = ()=>{
				trytime--
				if (trytime <= 0) {
					cc.ll.loading.unshowLoading();
					cc.ll.notice.addMsg(0, MsgCode.NETWORK_ERROR, () => {
						cc.ll.sceneMgr.changeScene('LoginScene');
					});
				} else {
					connect();
				}
			}
			try {
				net.connect(ip, port, (Lnet) => {
					cc.ll.net = Lnet;
					cc.ll.net.send('c2s_login', {
						accountid: accountid,
						token: token,
					});
					if (callback) {
						callback()
					}
				}, () => {
					errorcallback();
				});
			} catch (error) {
				errorcallback();
			}
			
		}

		connect();
		
		
	})
}