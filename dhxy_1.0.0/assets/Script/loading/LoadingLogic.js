var StrCommon = require('../etc/str_common');

cc.Class({
    extends: cc.Component,

    properties: {
        // loadingLayer: cc.Node,
        loadingText: cc.Label,
        // loadingProgressBar: cc.ProgressBar,

        _loadStr: "",
        _isLoading: false,

        _loadkey: 0,
        _loadingList: [cc.String],
        _nameList: [""],
        _progress: 0,
    },

    onLoad() {

        cc.game.setFrameRate(60);
    },

    start() {
        cc.macro.ENABLE_CULLING = true;

        if (cc.sys.isNative) {
            window.__errorHandler = function (errorMessage, file, line, message, error) {
                let exception = {};
                exception.errorMessage = errorMessage;
                exception.file = file;
                exception.line = line;
                exception.message = message;
                exception.error = error;
                if (window.exception != JSON.stringify(exception)) {
                    window.exception = JSON.stringify(exception);
                    cc.log("error----------------------");
                    cc.log(exception);
                    //TODO: 发送请求上报异常
                }
            };
        } else if (cc.sys.isBrowser) {
            window.onerror = function (errorMessage, file, line, message, error) {
                let exception = {};
                exception.errorMessage = errorMessage;
                exception.file = file;
                exception.line = line;
                exception.message = message;
                exception.error = error;
                if (window.exception != JSON.stringify(exception)) {
                    window.exception = JSON.stringify(exception);
                    cc.log(exception);
                    //TODO: 发送请求上报异常
                }


            };
        }

        cc.ll = {};
        cc.ll.config = {};

        cc.ll.pDistance = function (p1, p2) {
            return p1.sub(p2).mag();
        }

        cc.ll.pAdd = function (p1, p2) {
            return p1.add(p2);
        }

        //初始化网络模块服务
        cc.loader.loadRes(("Common/c2s"), function (err, tex) {
            let pr = protobuf.parse(tex.text);
            cc.pbroot = pr.root;
        });
        cc.ll.http = require('../network/HTTP');
        // let Net = require('../network/Net');
        // cc.ll.net = new Net();
        // 加载配置信息
        cc.loader.loadRes('config/config', (err, obj) => {
            cc.ll.config.ip = obj.json.ip;
            cc.ll.config.debug = obj.json.debug;
            if (obj.json.debug) {
                cc.ll.config.ip = obj.json.debugip;
            }
            cc.ll.config.port = obj.json.port;
            cc.ll.http.url = "http://" + cc.ll.config.ip + ":" + cc.ll.config.port;
        });
        //初始化通用界面
        cc.ll.msgbox = require('../common/Msgbox');
        cc.ll.msgbox.init();
        cc.ll.loading = require('../common/Loading');
        cc.ll.loading.init();
        cc.ll.notice = require('../common/Notice');
        cc.ll.notice.init();

        let VoiceMgr = require('../game/voice/voice_mgr');
        cc.ll.voicemgr = new VoiceMgr();

        //初始化本地存储
        let localStorage = require('../game/local_storage/local');
        cc.ll.local = new localStorage();

        // cc.ll.wechat = require('../common/WeChatMgr');
        //初始化场景管理器
        cc.ll.sceneMgr = require('../manager/SceneManager');
        //初始化音效管理器
        cc.ll.AudioMgr = require('../manager/AudioMgr');
        cc.ll.AudioMgr.init();

        let GoodsMgr = require('../game/GoodsMgr');
        GoodsMgr.Init();

        cc.ll.propData = {};
        cc.ll.propData.map = {};

        this._loadStr = StrCommon.StartLoading;

        this.loadingText.string = this._loadStr;

        //读取隐藏玩家设置
        let otherHide = cc.ll.local.get('setting_otherHide_checked');
        otherHide = otherHide == null ? false : otherHide;
        cc.ll.allowOtherHide = otherHide;
        // cc.ll.hideChanged = true;      //隐藏是否被修改

        // this.loadingProgressBar.progress = 0;

        // 加载资源
        this._loadingList = ['Login', 'prop_data', 'shape_mask'];
        this._nameList = ['必要', '重要', '游戏', '界面', 'UI', '通用', '其他'];

        this.onLoadNext();
    },

    onLoadComplete() {
        this._isLoading = false;
        this._loadStr = "正在登陆";
        cc.loader.onComplete = null;

        cc.ll.sceneMgr.changeScene('LoginScene');
    },

    onLoadNext() {
        if (this._loadkey >= this._loadingList.length) {
            this.onLoadComplete();
        } else {
            var name = this._nameList[this._loadkey];
            var dir = this._loadingList[this._loadkey];
            this.startPreloading(name, dir);
        }
        this._loadkey = this._loadkey + 1;
    },

    startPreloading(name, dir) {
        this._loadStr = "正在加载" + name + "资源，请稍候";
        this._isLoading = true;
        var self = this;


        if (dir == 'prop_data') {
            cc.loader.loadResDir(dir, function (completedCount, totalCount, item) {
                if (self._isLoading) {
                    self._progress = completedCount / totalCount;
                }
            }, function (err, jsons, urls) {
                for (let i = 0; i < jsons.length; i++) {
                    let jsondata = jsons[i];
                    if (jsondata.isValid) {
                        let dataname = jsondata.name;
                        let data = jsondata.json;
                        // console.log('url', url);
                        if (dataname.indexOf('skill') != -1) {
                            cc.ll.propData.skill = data;
                        } else if (dataname.indexOf('login_roleinfo') != -1) {
                            cc.ll.propData.login = data;
                        } else if (dataname.indexOf('prop_charge_reward') != -1) {
                            cc.ll.propData.chargereward = data;
                        } else if (dataname.indexOf('prop_xinshou_equip') != -1) {
                            cc.ll.propData.equip = data;
                        } else if (dataname.indexOf('prop_createbg') != -1) {
                            cc.ll.propData.createbg = data;
                        } else if (dataname.indexOf('prop_rolecolor') != -1) {
                            cc.ll.propData.rolecolor = data;
                        } else if (dataname.indexOf('prop_role') != -1) {
                            cc.ll.propData.role = data;
                        } else if (dataname.indexOf('prop_map') != -1) {
                            cc.ll.propData.mapdata = data;
                        } else if (dataname.indexOf('prop_item') != -1) {
                            cc.ll.propData.item = data;
                        } else if (dataname.indexOf('prop_charge') != -1) {
                            cc.ll.propData.charge = data;
                        } else if (dataname.indexOf('prop_level') != -1) {
                            cc.ll.propData.level = data;
                        } else if (dataname.indexOf('prop_npc') != -1) {
                            cc.ll.propData.npc = data;
                        } else if (dataname.indexOf('prop_paihang') != -1) {
                            cc.ll.propData.paihang = data;
                        } else if (dataname.indexOf('prop_pet') != -1) {
                            cc.ll.propData.pet = data;
                        } else if (dataname.indexOf('prop_resAnchor') != -1) {
                            cc.ll.propData.resanchor = data;
                        } else if (dataname.indexOf('prop_skill') != -1) {
                            cc.ll.propData.skill = data;
                        } else if (dataname.indexOf('prop_task') != -1) {
                            cc.ll.propData.task = data;
                        } else if (dataname.indexOf('prop_transfer_point') != -1) {
                            cc.ll.propData.transfer = data;
                        } else if (dataname.indexOf('map_') != -1) {
                            let mapid = dataname.substr(dataname.indexOf('map_') + 4);
                            cc.ll.propData.map[mapid] = data;
                        } else {
                            console.log('no catch file: ' + dataname);
                        }
                    }
                }
                self.onLoadNext();
            });
            return;
        }
        cc.loader.loadResDir(dir, cc.Texture2D, function (completedCount, totalCount, item) {
            if (self._isLoading) {
                self._progress = completedCount / totalCount;
                // self.loadingProgressBar.progress = self._progress;
            }
        }, function (err, assets) {
            self.onLoadNext();
        });
    },

    update(dt) {
        if (!this._isLoading || !this.loadingText) {
            return;
        }

        this.loadingText.string = this._loadStr + ' ';
        if (this._isLoading) {
            this.loadingText.string += Math.floor(this._progress * 100) + "%";
        } else {
            var t = Math.floor(Date.now() / 1000) % 4;
            for (var i = 0; i < t; ++i) {
                this.loadingText.string += '.';
            }
        }
    },
});