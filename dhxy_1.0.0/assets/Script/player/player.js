class player {
    constructor(id) {
        this.accountid = id;
        this.serverid = 0;
        this.onlyid = 0;
        this.roleid = 0;
        this.account = '';
        this.token = '';

        this.state = 0;

        this.name = 0;
        this.race = 0;
        this.sex = 0;
        this.mapid = 0;
        this.x = 0;
        this.y = 0;
        this.relive = 0;
        this.level = 0;
        this.level_reward = '';
        this.weapon = '';
        this.state = 0;
        this.resid = 0;
        this.bangid = 0;
        this.bangdata = null;
        this.chargesum = 0;
        this.rewardrecord = 0;

        this.exp = 0;
        this.maxexp = 0;

        this.node = null;

        this.maxChatItems = 50;
        this.chatItemList = {};
        this.unreadInfoId = {};
        this.teamInfo = {};
        this.teamid = 0;
        this.isleader = false;
        this.livingtype = 1;
        this.qianneng = 0;

        this.gmlevel = 0;
        // this.shane = 0;
        this.prisonTime = 0;

        this.itemList = {};
        //称谓
        this.titleid = 0;
        this.titletype = -1;
        this.titleval = '';
        this.bangname = '';
        this.color1 = 0;
        this.color2 = 0;
        this.schemename = '';
        this.safe_password = '';
        this.safe_lock = 0;
    }

    setTeam(info) {
        this.teamInfo = info;
        this.teamid = info.teamid;
        this.isleader = info.leader == this.roleid;
        if (this.teamid > 0 && this.isleader) {
            this.getLogic().addHeadStatus(info.teamcnt >= 5 ? 'zudui2' : 'zudui');
        }
        else {
            this.getLogic().delHeadStatus('zudui');
            this.getLogic().delHeadStatus('zudui2');
        }

        this.getMainUILogic().setTeamListInfo();
    }

    addFriendChatInfo(info) {
        if (this.chatItemList[info.toid + info.fromid] == null) {
            this.chatItemList[info.toid + info.fromid] = [];
        }
        this.chatItemList[info.toid + info.fromid].push(info);
        let infolength = this.chatItemList[info.toid + info.fromid].length;
        if (infolength > this.maxChatItems) {
            this.chatItemList.splice(0, infolength - this.maxChatItems);
        }
        this.unreadInfoId[info.fromid] = 1;
        let mainUI = this.getMainUILogic();
        if (mainUI) {
            mainUI.friendsChat(info);
        }
    }

    setInfo(info) {
        info.account && (this.account = info.account);
        info.state && (this.state = info.state);
        info.token && (this.token = info.token);
    }

    setUserInfo(data) {
        this.gameData = data;
        data.addattr1 && (this.gameData.addattr1 = JSON.parse(this.gameData.addattr1));
        data.addattr2 && (this.gameData.addattr2 = JSON.parse(this.gameData.addattr2));
        data.attr1 && (this.gameData.attr1 = JSON.parse(this.gameData.attr1));

        data.serverid && (this.serverid = data.serverid);
        data.onlyid && (this.onlyid = data.onlyid);
        data.accountid && (this.accountid = data.accountid);
        data.roleid && (this.roleid = data.roleid);
        data.name && (this.name = data.name);
        data.race && (this.race = data.race);
        data.sex && (this.sex = data.sex);
        data.mapid && (this.mapid = data.mapid);
        data.x && (this.x = data.x);
        data.y && (this.y = data.y);
        data.relive && (this.relive = data.relive);
        data.level && (this.level = data.level);
        data.levelreward && (this.level_reward = data.levelreward);
        data.weapon && (this.weapon = data.weapon);
        data.state && (this.state = data.state);
        data.resid && (this.resid = data.resid);
        data.bangid && (this.bangid = data.bangid);
        data.exp && (this.exp = data.exp);
        data.maxexp && (this.maxexp = data.maxexp);
        data.chargesum && (this.chargesum = data.chargesum);
        data.rewardrecord && (this.rewardrecord = data.rewardrecord);
        if (this.bangid && this.bangid > 0) {
            cc.ll.net.send('c2s_getbanginfo', {
                roleid: this.roleid,
                bangid: this.bangid
            });
        }
        data.gmlevel && (this.gmlevel = data.gmlevel);

        data.teamid && (this.teamid = data.teamid);
        data.isleader && (this.isleader = data.isleader);
        data.qianneng && (this.qianneng = data.qianneng);

        data.bangname && (this.bangname = data.bangname);
        data.titleid && (this.titleid = data.titleid);
        data.titleval && (this.titleval = data.titleval);
        data.titletype && (this.titletype = data.titletype);
        data.schemename && (this.schemename = data.schemename);
        this.safe_password = data.safepassword || this.safe_password;
        this.safe_lock = data.safelock || this.safe_lock;
        if (typeof (data.color1) == 'number') {
            this.color1 = data.color1;
        }
        if (typeof (data.color2) == 'number') {
            this.color2 = data.color2;
        }
        // data.shane && (this.shane = data.shane);

        // this.shanEChange(data.shane);
    }

    setNode(node) {
        this.node = node;
    }

    getNode() {
        return this.node;
    }

    getLogic() {
        if (this.node) {
            return this.node.getComponent('role');
        }
        return null;
    }

    getMainLogic() {
        if (this.mainGame == null) {
            this.mainGame = cc.find('Canvas');
        }
        let logic = this.mainGame.getComponent('GameLogic');
        return logic;
    }

    getMainUILogic() {
        if (this.mainUI == null) {
            this.mainUI = cc.find('Canvas/MainUI');
        }
        let logic = this.mainUI.getComponent('MainUI');
        return logic;
    }

    levelUp(level) {
        this.getLogic().showLevelUp();
        this.getMainUILogic().setRoleLevel(level)
    }

    changeExp(curexp, maxexp = 0) {
        this.exp = curexp;
        if (maxexp != 0) {
            this.maxexp = maxexp;
            if (cc.ll.player.gameData) {
                cc.ll.player.gameData.maxexp = maxexp;
            }
        }
        if (cc.ll.player.gameData) {
            cc.ll.player.gameData.exp = curexp;
        }
        this.getMainUILogic().setExp(curexp, maxexp);
    }

    shanEChange(n) {
        // this.shane = n;
        // if (this.shane > 0 && this.mapid == 1011) {
        //     let mainLogic = this.getMainLogic();
        //     if (mainLogic) {
        //         mainLogic.mapLogic.changeMap(1201, cc.v2(59, 4));
        //     }
        // }
        // if (this.shane <= 0 && this.mapid == 1201) {
        //     let mainLogic = this.getMainLogic();
        //     if (mainLogic) {
        //         mainLogic.mapLogic.changeMap(1011);
        //     }
        //     // this.getLogic().setObjPos(41, 19);
        // }
    }

    changePrisonTime(n) {
        this.prisonTime = n;
        this.getMainUILogic().changePrisonTime(n);
    }

    getItemList() {
        return this.itemList;
    }

    getItemNum() {
        let n = 0;
        for (const itemid in this.itemList) {
            const itemnum = this.itemList[itemid];
            if (itemnum > 0) {
                n++;
            }
        }
        return n;
    }

    send(event, data) {
        cc.ll.net.send(event, data);
    }
}

module.exports = player;
