let GoodsMgr = require('../game/GoodsMgr');
cc.Class({
	extends: cc.Component,
	properties: {
		item_prefab: cc.Prefab,
		item_layout: cc.Node,
	},

	start () {
		this.color_index1 = 0;
		this.color_index2 = 0;
		this.costs = {};
		let right = cc.find('Right', this.node);
		if (right) {
			right.runAction(cc.flipX(true));
		}
		this.setCurColor();
		this.onColorClick();
	},

	/*
	 * 当前染色方案
	 */
	setCurColor () {
		let color1 = cc.ll.player.color1;
		let color2 = cc.ll.player.color2;
		this.cur_color_index1 = this.color_index1;
		this.cur_color_index2 = this.color_index2;
		let data = cc.ll.propData.rolecolor[cc.ll.player.resid];
		for (let key in data) {
			if (key.indexOf('color1_') != -1) {
				if (parseInt(data[key].color, 16) == color1) {
					this.cur_color_index1 = parseInt(key.split('_')[1]);
				}
			}
			if (key.indexOf('color2_') != -1) {
				if (parseInt(data[key].color, 16) == color2) {
					this.cur_color_index2 = parseInt(key.split('_')[1]);
				}
			}
		}
		this.color_index1 = this.cur_color_index1;
		this.color_index2 = this.cur_color_index2;
		this.setColor();
		this.setTipLabel();
		this.setCost();
	},

	/*
	 * 自选
	 */
	onColorClick () {
		let node = cc.find('BtnColor', this.node);
		if (node) {
			node.setScale(1.2);
		}
	},

	/*
	 * 定制
	 */
	onSaveClick () {
		cc.ll.msgbox.addMsg('暂时未开放');
	},

	/*
	 * 左点击
	 */
	onLeftRoleClick () {
		let role = cc.find('role', this.node);
		if (role) {
			let logic = role.getComponent('ColoringRole');
			logic.playNextAction(-1);
		}
	},

	/*
	 * 右点击
	 */
	onRightRoleClick () {
		let role = cc.find('role', this.node);
		if (role) {
			let logic = role.getComponent('ColoringRole');
			logic.playNextAction(1);
		}
	},

	/*
	 * 部位一左点击
	 */
	onLeftColor1Click () {
		-- this.color_index1;
		if (this.color_index1 < 0) {
			this.color_index1 = 10;
		}
		this.setColor();
		this.setTipLabel();
		this.setCost();
	},

	/*
	 * 部位一右点击
	 */
	onRightColor1Click () {
		++ this.color_index1;
		if (this.color_index1 > 10) {
			this.color_index1 = 0;
		}
		this.setColor();
		this.setTipLabel();
		this.setCost();
	},

	/*
	 * 部位二左点击
	 */
	onLeftColor2Click () {
		-- this.color_index2;
		if (this.color_index2 < 0) {
			this.color_index2 = 10;
		}
		this.setColor();
		this.setTipLabel();
		this.setCost();
	},

	/*
	 * 部位二右点击
	 */
	onRightColor2Click () {
		++ this.color_index2;
		if (this.color_index2 > 10) {
			this.color_index2 = 0;
		}
		this.setColor();
		this.setTipLabel();
		this.setCost();
	},

	/*
	 * 部位颜色显示
	 */
	setColor () {
		let data = cc.ll.propData.rolecolor[cc.ll.player.resid];
		let color1 = this.hxcolorTransfrom('000000'), color2 = this.hxcolorTransfrom('000000');
		if (this.color_index1 > 0 && this.color_index1 <= 10) {
			color1 = this.hxcolorTransfrom(data['color1_'+(this.color_index1)].color);
		}
		if (this.color_index2 > 0 && this.color_index2 <= 10) {
			color2 = this.hxcolorTransfrom(data['color2_'+(this.color_index2)].color);
		}

		cc.find('ui_dye_dye_cloth1/color_bar', this.node).color = color1;
		cc.find('ui_dye_dye_cloth2/color_bar', this.node).color = color2;

		let logic = cc.find('role', this.node).getComponent('ColoringRole');
		if (this.color_index1 == 0 && this.color_index2 == 0) {
			logic.unshowColorMask();
		}
		else {
			logic.showColorMask(color1, color2);
		}
	},

	/*
	 * 部位颜色提示
	 */
	setTipLabel () {
		let tiplabel1 = cc.find('TipLabel1', this.node);
		let tiplabel2 = cc.find('TipLabel2', this.node);
		if (tiplabel1) {
			let str = "部\n位\n一";
			if (this.color_index1 != 0) {
				str += "\n" + (this.color_index1);
			}
			tiplabel1.getComponent(cc.Label).string = str;
		}
		if (tiplabel2) {
			let str = "部\n位\n二";
			if (this.color_index2 != 0) {
				str += "\n" + (this.color_index2);
			}
			tiplabel2.getComponent(cc.Label).string = str;
		}
	},

	/*
	 * 消耗显示
	 */
	setCost () {
		let cost_node = cc.find('Cost', this.node);
		if (!cost_node) { return; }
		if ((this.color_index1 == 0 && this.color_index2 == 0) || (this.color_index1 == this.cur_color_index1 && this.color_index2 == this.cur_color_index2)) {
			cost_node.active = false;
		}
		else {
			cost_node.active = true;
			let costs = {};
			if ((this.color_index1 != 0) && (this.color_index1 != this.cur_color_index1)) {
				let item = cc.ll.propData.rolecolor[cc.ll.player.resid]['color1_'+this.color_index1];
				costs[item.itemid] = item.count;
			}
			if ((this.color_index2 != 0) && (this.color_index2 != this.cur_color_index2)) {
				let item = cc.ll.propData.rolecolor[cc.ll.player.resid]['color2_'+this.color_index2];
				if (!costs[item.itemid]) {
					costs[item.itemid] = 0;
				}
				costs[item.itemid] += item.count;
			}

			for (let node of this.item_layout.children) {
				let logic = node.getComponent('BagItem');
				let count = costs[logic.getItemid()];
				if (count) {
					logic.setItemCount(count);
					delete costs[logic.getItemid()];
				}
				else {
					node.destroy();
				}
			}
			for (let key in costs) {
				let node = cc.instantiate(this.item_prefab);
				node.scaleX = node.scaleX * 0.75;
				node.scaleY = node.scaleY * 0.75;
				node.width = 75;
				node.height = 75;
				node.parent = this.item_layout;
				let logic = node.getComponent('BagItem');
				logic.rmSelectedNode();
				logic.loadInfo({
					itemid: key,
					count: parseInt(costs[key]),
				});
			}
		}
	},

	/*
	 * 本色
	 */
	onOriginClick () {
		this.color_index1 = 0;
		this.color_index2 = 0;
		this.setColor();
		this.setTipLabel();
		this.setCost();
	},

	/*
	 * 还原本色
	 */
	onUseOriginClick () {
		cc.ll.notice.addMsg(1, '确定要还原未原始的颜色吗！', () => {
			this.onOriginClick();
			if (this.cur_color_index1 != this.color_index1 || this.cur_color_index2 != this.color_index2) {
				cc.ll.net.send('c2s_change_role_color', {
					index1: this.color_index1,
					index2: this.color_index2,
				});
			}
		}, () => {});
	},

	/*
	 * 颜色转换
	 */
	hxcolorTransfrom (color) {
		let num = parseInt(color, 16);
		let r = Math.floor(num/256/256);
		let g = Math.floor(num/256%256);
		let b = Math.floor(num%256);
		return new cc.Color(r, g, b);
	},

	onCloseClick () {
		// console.log('close ColoringRolePanel');
		this.node.destroy();
	},

	/*
	 * 点击变色
	 */
	onChangeClick () {
		let rolecolor = cc.ll.propData.rolecolor[cc.ll.player.resid];
		let data1, data2;
		if (this.color_index1 != 0) {
			data1 = rolecolor['color1_'+(this.color_index1)];
			if (parseInt(data1.color, 16) != cc.ll.player.color) {
				let info = GoodsMgr.getItem(data1.itemid);
				if (info.count < data1.count) {
					cc.ll.msgbox.addMsg(`${info.name}数量不够`);
					return;
				}
			}
		}
		if (this.color_index2 != 0) {
			data2 = rolecolor['color2_'+(this.color_index2)];
			if (parseInt(data2.color, 16) != cc.ll.player.color) {
				let info = GoodsMgr.getItem(data2.itemid);
				if (info.count < data2.count) {
					cc.ll.msgbox.addMsg(`${info.name}数量不够`);
					return;
				}
			}
		}

		/* 不可以改会原色 */
		/* if (this.color_index1 == 0) {
			for (let key in rolecolor) {
				let item = rolecolor[key];
				if (parseInt(item.color, 16) == cc.ll.player.color1 && key.indexOf('color1') != -1) {
					this.color_index1 = parseInt(key.split('_')[1]);
				}
			}
		} */

		/* 不可以改会原色 */
		/* if (this.color_index2 == 0) {
			for (let key in rolecolor) {
				let item = rolecolor[key];
				if (parseInt(item.color, 16) == cc.ll.player.color2 && key.indexOf('color2') != -1) {
					this.color_index2 = parseInt(key.split('_')[1]);
				}
			}
		} */

		cc.ll.net.send('c2s_change_role_color', {
			index1: this.color_index1,
			index2: this.color_index2,
		});
	},
});
