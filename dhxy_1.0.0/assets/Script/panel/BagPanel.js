let GameRes = require('../game/GameRes');

/**
 * 背包面板
 */
cc.Class({
	extends: cc.Component,

	properties: {
		itemContent: cc.Node,
		equipNodes: [cc.Node],
		roleNode: cc.Node,

		// shigongLab: cc.Label,
		// yinliangLab: cc.Label,
		// xianyuLab: cc.Label,
		scoreLabel: cc.Label,

		/** 背包Item预制体 */
		bagItem: cc.Prefab,

		/** 装备Item预制体 */
		equipItem: cc.Prefab,
		schemeName: cc.Label,
		SchemePanel: cc.Prefab,
	},

	onLoad() {
		this.currentY = 0;
		this.currentSelected = -1;
		this.itemNodes = [];
		// cc.ll.net.send('c2s_get_bagitem', { roleid: cc.ll.player.roleid });

		this.roleNode.getComponent('UIRole').setInfo(cc.ll.player);
		this.setSchemeName();
	},

	setSchemeName() {
		if (cc.ll.player.schemename && cc.ll.player.schemename != '')
			this.schemeName.string = cc.ll.player.schemename;
	},

	start() {
		if (cc.ll.player.getItemList() == null || cc.ll.player.getItemNum() == 0) {
			cc.ll.net.send('c2s_get_bagitem', {
				roleid: cc.ll.player.roleid
			});
		} else {
			this.loadBagList();
		}
	},

	loadBagList() {
		this.itemContent.destroyAllChildren();
		this.itemNodes = [];

		let list = [];
		if (cc.ll.player.equipdata) {
			this.scoreLabel.string = cc.ll.player.equipdata.score;
			for (let index = 0; index < this.equipNodes.length; index++) {
				let equipid = cc.ll.player.equipdata.use[index + 1];
				this.equipNodes[index].getComponent('EquipItem').reset();
				if (equipid && cc.ll.player.equipdata.info[equipid]) {
					this.equipNodes[index].getChildByName('itembg').active = true;
					this.equipNodes[index].getComponent('EquipItem').loadInfo(cc.ll.player.equipdata.info[equipid]);
				}
			}

			for (const equip of cc.ll.player.equipdata.list) {
				if (cc.ll.player.equipdata.info[equip]) {
					list.push({
						itemid: equip,
						info: cc.ll.player.equipdata.info[equip],
						type: 1
					});
				}
			}
		}

		for (const itemid in cc.ll.player.itemList) {
			if (cc.ll.player.itemList.hasOwnProperty(itemid) && cc.ll.player.itemList[itemid] > 0) {
				list.push({
					itemid: itemid,
					count: cc.ll.player.itemList[itemid],
					type: 0
				});
			}
		}

		let maxLength = 50;
		if (list.length > maxLength) {
			maxLength = list.length;
		}
		this.itemContent.height = Math.ceil(maxLength / 5) * 75;

		for (let index = 0; index < maxLength; index++) {
			let item = null;
			if (index < list.length) {
				if (list[index].type == 1) {
					item = cc.instantiate(this.equipItem);
					item.parent = this.itemContent;
					item.getComponent('EquipItem').loadInfo(list[index].info);
				} else {
					item = cc.instantiate(this.bagItem);
					item.parent = this.itemContent;
					if (index < list.length) {
						item.getComponent('BagItem').loadInfo(list[index]);
					}
				}
			} else {
				item = cc.instantiate(this.bagItem);
				item.parent = this.itemContent;
			}
			item.x = -150 + (index % 5) * 75;
			item.y = -37.5 - Math.floor(index / 5) * 75;

			let btn = item.getComponent(cc.Button);
			var clickEventHandler = new cc.Component.EventHandler();
			clickEventHandler.target = this.node;
			clickEventHandler.component = "BagPanel";
			clickEventHandler.customEventData = index;
			clickEventHandler.handler = "bagItemClicked";
			btn.clickEvents.push(clickEventHandler);

			this.itemNodes.push(item);
		}


		this.SetMoneyInfo();

		// for (let index = 0; index < maxLength; index++) {
		// 	let item = cc.instantiate(this.bagItem);
		// 	item.x = -150 + (index % 5) * 75;
		// 	item.y = -37.5 - Math.floor(index / 5) * 75;
		// 	item.parent = this.itemContent;

		// 	let btn = item.getComponent(cc.Button);
		// 	var clickEventHandler = new cc.Component.EventHandler();
		// 	clickEventHandler.target = this.node;
		// 	clickEventHandler.component = "BagPanel";
		// 	clickEventHandler.handler = "bagItemClicked";
		// 	clickEventHandler.customEventData = index;
		// 	btn.clickEvents.push(clickEventHandler);

		// 	this.itemNodes.push(item);

		// 	if (index < baglist.length) {
		// 		item.getComponent('BagItem').loadInfo(baglist[index]);
		// 	}
		// }
	},

	useEquipClicked(e, d) {
		e.target.getComponent('EquipItem').selected();
	},

	bagItemClicked(e, d) {
		if (this.currentSelected == d) {
			return;
		}
		for (const item of this.itemNodes) {
			if (e.target == item) {
				continue;
			}
			item.getComponent('BagItem') && item.getComponent('BagItem').unSelected();
			item.getComponent('EquipItem') && item.getComponent('EquipItem').unSelected();
		}
	},

	onCloseBtnClicked(e, d) {
		cc.ll.AudioMgr.playCloseAudio();
		this.roleNode.getComponent('UIRole').clear();
		this.node.destroy();
	},

	SetMoneyInfo() {
		let topnode = cc.find('topInfo', this.node);
		let toplogic = topnode.getComponent('TopInfo');
		toplogic.updateGameMoney();
	},

	showSchemeListPanel() {
		let schemePanel = cc.instantiate(this.SchemePanel);
		schemePanel.parent = cc.find('Canvas/MainUI');
	}
});
