let GameDefine = require('../game/GameDefine');
let PubFun = require('../game/PubFunction');
let StrCommon = require('../etc/str_common')

cc.Class({
	extends: cc.Component,

	properties: {
		listContent: cc.Node,
		cntLab: cc.Label,
		ItemNode: cc.Node,
		EquipNode: cc.Node,
		equipListItem: cc.Prefab,
	},

	onLoad() {
		this.equipIndex = 0;
		this.listItems = [];
	},

	finishedCombine(info) {
		if (info) {
			this.showInfo();
			this.EquipNode.getComponent('EquipItem').loadInfo(info);
		}
	},

	start() {
	},

	getTypeList() {
		cc.ll.net.send('c2s_xianqi_list', { roleid: cc.ll.player.roleid });
	},

	loadTypeList(info) {
		if (!info || !info.list) {
			return;
		}
		let curListY = 0;
		this.listContent.destroyAllChildren();
		this.listItems = [];
		let infoArr = JSON.parse(info.list);
		for (const key in infoArr) {
			let itemInfo = infoArr[key];
			let item = cc.instantiate(this.equipListItem);
			item.x = 0;
			item.y = curListY;
			curListY -= 90;
			item.parent = this.listContent;
			item.getComponent('EquipListItem').loadInfo(itemInfo);

			let btn = item.getComponent(cc.Button);
			var clickEventHandler = new cc.Component.EventHandler();
			clickEventHandler.target = this.node;
			clickEventHandler.component = "XianQiCombinePanel";
			clickEventHandler.handler = "equipItemClicked";
			btn.clickEvents.push(clickEventHandler);
			this.listItems.push(item);
		}
		this.listContent.height = -curListY;
		if (this.listContent.height < this.listContent.parent.height) {
			this.listContent.height = this.listContent.parent.height;
		}
		this.equipIndex = 1;
		this.listItems[0].getComponent('EquipListItem').selected();
		this.showInfo();
	},

	equipItemClicked(e, d) {
		for (let index = 0; index < this.listItems.length; index++) {
			this.listItems[index].getComponent('EquipListItem').unSelected();
		}
		e.target.getComponent('EquipListItem').selected();
		this.equipIndex = e.target.getComponent('EquipListItem').iteminfo.EIndex;
	},

	showInfo() {
		this.ItemNode.getComponent('BagItem').loadInfo({ itemid: 10406, count: 1 });
		let curcnt = cc.ll.player.itemList[10406] == null ? 0 : cc.ll.player.itemList[10406];
		if (curcnt >= 40) {
			this.cntLab.node.color = cc.Color.GREEN;
		}
		else {
			this.cntLab.node.color = cc.Color.RED;
		}
		this.cntLab.string = curcnt;
	},

	onCombineBtnClicked(e, d) {
		let curcnt = cc.ll.player.itemList[10406] == null ? 0 : cc.ll.player.itemList[10406];
		if (curcnt < 40) {
			cc.ll.msgbox.addMsg(StrCommon.BaHuangNotEnough);
			return;
		}
		cc.ll.net.send('c2s_creat_equip',
			{
				type: 3,
				roleid: cc.ll.player.roleid,
				index: this.equipIndex
			});
	},

	onCloseBtnClicked(e, d) {
		this.node.destroy();
	}
});
