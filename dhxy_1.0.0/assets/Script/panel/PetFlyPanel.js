cc.Class({
	extends: cc.Component,

	properties: {
		pet_sprite:    cc.Sprite,
		pet_label:     cc.Label,
		fly_label:     cc.Label,
		fly_button:    cc.Node,
		property_node: cc.Node,
	},

	start () {
		this.petid_list = [];
		for (let item of cc.ll.player.petInfo.list) {
			if (item.grade >= 3) {
				this.petid_list.push(item.petid);
			}
		}
		if (this.petid_list.length == 0) {
			cc.ll.msgbox.addMsg('你没有符合条件的神兽！');
			this.onClose();
			return;
		}
		if (this.petid_list.length == 1) {
			cc.find('PetBg/Right', this.node).active = false;
			cc.find('PetBg/Left', this.node).active = false;
		}
		this.cur_index = this.petid_list.indexOf(cc.ll.player.petInfo.curid);
		if (this.cur_index == -1) {
			this.cur_index = 0;
		}
		this.refrush();
	},

	/*
	 * 切换前一个宠物
	 */
	onLeftClick () {
		-- this.cur_index;
		if (this.cur_index < 0) {
			this.cur_index = this.petid_list.length-1;
		}
		this.refrush();
	},

	/*
	 * 切换后一个宠物
	 */
	onRightClick () {
		++ this.cur_index;
		if (this.cur_index == this.petid_list.length) {
			this.cur_index = 0;
		}
		this.refrush();
	},

	getPetByPetid (petid) {
		for (let item of cc.ll.player.petInfo.list) {
			if (item.petid == petid) {
				return item;
			}
		}
		return null;
	},

	/*
	 * 刷新界面
	 */
	refrush () {
		this.petid = this.petid_list[this.cur_index];
		for (let item of cc.ll.player.petInfo.list) {
			if (item.petid == this.petid) {
				this.pet = item;
			}
		}
		if (this.pet.fly%10 == 0) {
			this.fly_label.string = '第一次飞升';
		}
		else if (this.pet.fly%10 == 1) {
			this.fly_label.string = '第二次飞升';
		}
		else if (this.pet.fly%10 == 2) {
			this.fly_label.string = '第三次飞升';
		}
		else if (this.pet.fly%10 == 3) {
			this.fly_label.string = '更换飞升属性';
		}
		this.fly_button.active = true;
		this.property_node.active = false;
		this.pet_label.string = this.pet.name;
		cc.loader.loadRes(`ui/photo/${this.pet.resid}`, cc.SpriteFrame, (err, spriteFrame) => {
			if (err) {
				console.log(err);
			}
			else {
				this.pet_sprite.spriteFrame = spriteFrame;
			}
		});
	},

	/*
	 * 点击飞升按钮
	 */
	onFlyClick () {
		if (this.pet.fly%10 == 0) {
			if (this.pet.level < 50) {
				cc.ll.msgbox.addMsg('宠物需要0转50级！');
				return;
			}
			cc.ll.net.send('c2s_petfly_msg', {
				petid: this.petid,
				type: 0,
			});
		}
		else if (this.pet.fly%10 == 1) {
			if (this.pet.level < 100 || this.pet.relive < 1) {
				cc.ll.msgbox.addMsg('宠物需要1转100级！');
				return;
			}
			cc.ll.net.send('c2s_petfly_msg', {
				petid: this.petid,
				type: 0,
			});
		}
		else if (this.pet.fly%10 == 2) {
			if (this.pet.level < 120 || this.pet.relive < 2) {
				cc.ll.msgbox.addMsg('宠物需要2转120级！');
				return;
			}
			this.property_node.active = true;
			this.fly_button.active = false;
		}
		else if (this.pet.fly%10 == 3) {
			this.property_node.active = true;
			this.fly_button.active = false;
		}
	},

	/*
	 * 点击选择初始值
	 */
	onPropertyClick (event, param) {
		if (cc.ll.player.gameData.money < 5000000) {
			cc.ll.msgbox.addMsg('需要消耗500万银两，银两不足！');
			return;
		}
		if (param == parseInt(this.pet.fly/10)) {
			cc.ll.msgbox.addMsg('当前是已选择此属性，请选择其它属性！');
			return;
		} 
		cc.ll.notice.addMsg(1, '需要消耗500万银两，是否继续？', () => {
			cc.ll.net.send('c2s_petfly_msg', {
				petid: this.petid,
				type: parseInt(param),
			});
		}, () => {});
	},

	onClose () {
		this.node.destroy();
	},
});
