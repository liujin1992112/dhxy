let PetLogic = require('../game/PetLogic')

cc.Class({
	extends: cc.Component,

	properties: {
		petNode1: cc.Node,
		petNode2: cc.Node,
		lightNode1: cc.Node,
		lightNode2: cc.Node,

	},

	onLoad() {
		this.petid = -1;
		this.lightNode1.active = false;
		this.lightNode2.active = false;

		this.petNode1.getComponent('UIRole').setInfo({resid:5011, name:''});
		this.petNode2.getComponent('UIRole').setInfo({resid:5021, name:''});
	},

	clear(){
		this.petNode1.getComponent('UIRole').clear();
		this.petNode2.getComponent('UIRole').clear();
	},

	pet1Clicked(e, d){
		this.petid = 1005;
		this.lightNode1.active = true;
		this.lightNode2.active = false;
	},

	pet2Clicked(e, d){
		this.petid = 1004;
		this.lightNode1.active = false;
		this.lightNode2.active = true;
	},

	onSureBtnClicked(e, d) {
		if (this.petid == -1) {
			return;
		}
		let pet = new PetLogic.PetLogic();
		pet.createPet(this.petid);

		this.clear();
		this.node.destroy();
	},
});
