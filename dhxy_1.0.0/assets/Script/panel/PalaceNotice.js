/**
 * 
 */
cc.Class({
    extends: cc.Component,
    properties: {
        background: cc.Node,
        richText: cc.RichText,
    },

    onLoad() {
        this.str_list = [];
        this.running = false;
    },

    start() {
        this.unshowRichText();
    },

    addStrToList(str) {
        this.str_list.push(str);
        if (!this.running) {
            this.showRichText(this.str_list.shift());
        }
    },

    showRichText(str) {
        this.running = true;
        this.background.getComponent(cc.Animation).play('palaceNotice');
        for (let item of this.node.children) {
            item.active = true;
        }
        this.richText.string = str;
        let maskWidth = cc.find('Mask', this.node).width;
        this.richText.node.x = maskWidth / 2;
        this.richText.node.runAction(cc.sequence(
            cc.moveBy(10, cc.v2(-maskWidth - this.richText.node.width, 0)),
            cc.callFunc(() => {
                if (this.str_list.length > 0) {
                    this.showRichText(this.str_list.shift());
                }
                else {
                    this.unshowRichText();
                    this.running = false;
                }
            })
        ));
    },

    unshowRichText() {
        this.background.getComponent(cc.Animation).stop('palaceNotice');
        for (let item of this.node.children) {
            item.active = false;
        }
    },
});
