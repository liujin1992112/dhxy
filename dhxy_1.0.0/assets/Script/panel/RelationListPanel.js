let GameDefine = require('../game/GameDefine');


cc.Class({
	extends: cc.Component,

	properties: {
        lblTitle:cc.Label,
        lblDesc:cc.Label,
        scrollViewContent: cc.Node,
        relationItem: cc.Prefab,

	},

	onLoad() {        
    },

    start(){
        let operationType = this.node.opType;
        cc.ll.player.send('c2s_relation_List',{
            roleId: cc.ll.player.roleid,
            relationType: GameDefine.RelationType.Brother,
            operationType: operationType
        })
    },
    
    //初始化关系列表，1，添加新人，2，退出
    initRelationList(data){


        let relationList = data.relationList;
        this.relationList = JSON.parse(relationList);
        this.scrollViewContent.destroyAllChildren();
        this.relationList.forEach((relation,index) => {
                
                let relationItem = cc.instantiate(this.relationItem);
                if(relationItem){
                    let relationItemLogic = relationItem.getComponent('RelationItem');
                    if(relationItemLogic){
                        relationItemLogic.initRelationData(relation,data.operationType);
                    }                
                    relationItem.parent = this.scrollViewContent;
                }

		});
		
		this.scrollViewContent.height = Math.max(this.scrollViewContent.height, this.relationList.length * 120);
    
	},
	

	leaveRelation(data){
		let children = this.scrollViewContent.getChildren();
		for(var i = 0;i < children.length;i++){
			let playerPanel = children[i];
			let membersPanel = playerPanel.getChildByName('MembersPanel');
		
			let lblRelationId = Number(playerPanel.getChildByName('LblRelationId').getComponent(cc.Label).string);
			if(lblRelationId == data.relationId){
				if(data.leaveRoleId != cc.ll.player.roleid){
					let playerHead = membersPanel.getChildren().find(e=>{
						let lblRoleId = Number(e.getChildByName('LblRoleId').getComponent(cc.Label).string);
						return lblRoleId == data.leaveRoleId;
					});

					if(playerHead){
						playerHead.destroy();
						break;
					}
				}else{
					playerPanel.destroy();
				}

			}
		}
	},
	
	deleteRelationItem(data){
		let children = this.scrollViewContent.getChildren();
		for(var i = 0;i < children.length;i++){
			let playerPanel = children[i];
		
			let lblRelationId = Number(playerPanel.getChildByName('LblRelationId').getComponent(cc.Label).string);

			if(lblRelationId == data.relationId){
				playerPanel.destroy();
			}
		}
	},

	onCloseBtnClicked(e, d) {
		cc.ll.AudioMgr.playCloseAudio();
		this.node.destroy();
	},
});
