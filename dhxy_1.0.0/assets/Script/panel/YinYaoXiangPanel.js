let GameRes = require('../game/GameRes');

/**
 * 引妖香 面板
 */
cc.Class({
	extends: cc.Component,

	properties: {
		timeLab: cc.Label,
	},

	onLoad() {
		this.time = 0;
		this.setTime(3600);
	},

	setTime(time) {
		this.time = time;
		// if (time > 3600) {
		// 	this.time = 3600
		// }
		this.updateTime();
		this.schedule(this.updateTime, 1, cc.macro.REPEAT_FOREVER, 0);
	},

	updateTime() {
		this.time--;
		let min = Math.floor(this.time / 60);
		if (min < 10) {
			min = '0' + min;
		}
		let sec = this.time % 60;
		if (sec < 10) {
			sec = '0' + sec;
		}
		this.timeLab.string = min + ':' + sec;
	},

	closePanel(e, d) {
		let gamelogic = (cc.find('Canvas')).getComponent('GameLogic');
		if (gamelogic.autoPatrolNode.active) {
			gamelogic.autoPatrolNode.active = false;
			gamelogic.mapLogic.selfHeroLogic.playStop();
		}
		cc.ll.net.send('c2s_stop_incense', { roleid: cc.ll.player.roleid });
		this.node.destroy();
	},

	xunluoBtnClicked(e, d) {
		let gamelogic = (cc.find('Canvas')).getComponent('GameLogic');
		if (!gamelogic.uiLogic.isBattle)
			gamelogic.mapLogic.autoPatrol();
	}
});
