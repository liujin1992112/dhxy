let GameDefine = require('../game/GameDefine');
let CPubFunction = require('../game/PubFunction');
let CNpcMgr = require('../game/NpcMgr');


cc.Class({
	extends: cc.Component,

	properties: {
        emojiAtlas: cc.SpriteAtlas,
	},

	onLoad() {        
    },

    start(){

    },
    
    //播放效果
    playBrotherCreateShow(data){

        let actionArrs = [];
        let gameLogic = this.getGameLogic();
        if(gameLogic){

            let sayArr = GameDefine.relationMsg.filter(e =>{
                return e.action == GameDefine.relationActionType.Bro_Create;
            });
            let members = JSON.parse(data.members);
            if(sayArr.length > 0){
                sayArr.forEach((e,index) =>{
                    if(e.target == 1){
                        //玩家说的话                        
                        members.forEach((i,index) => {
                            let curRole = null;
                            if (i.onlyid == cc.ll.player.onlyid) {
                                curRole = gameLogic.mapLogic.roleNode;
                            }
                            if (curRole == null) {
                                curRole = gameLogic.GetPlayerObjs(i.onlyid);
                            }

                            //拼接显示的话
                            
                            let sayStr = e.msg1;
                            

                            if(e.msg2 != ''){                                
                                let nameArrs = this.getOtherMembersName(members,i.name);
                                nameArrs.forEach((j,index)=>{
                                    if(index > 0)
                                        sayStr = sayStr + ',';
                                    sayStr = sayStr + j;
                                });
                                sayStr = sayStr + e.msg2;
                            }
                            let sayFunc = cc.callFunc(() => {
                                CPubFunction.CreateRoleEmojBroadCast(curRole, sayStr,this.emojiAtlas);
                            });
                            actionArrs.push(sayFunc);
                        });
                    }else if(e.target == 2){
                        //NPC说的话
                        let sayStr = e.msg1;
                            

                        if(e.msg2 != ''){                                
                            let memberCount = members.length;                            
                            sayStr = sayStr + memberCount + e.msg2;
                        }
                        let sayFunc = cc.callFunc(() => {
                            CPubFunction.CreateRoleEmojBroadCast(CNpcMgr.FindNpcByConfigID(30049), sayStr,this.emojiAtlas);                        
                        });
                        actionArrs.push(sayFunc);
                        
                    }
                    actionArrs.push(cc.delayTime(4));
                });
                            
            }

            actionArrs.push(cc.delayTime(0.2));

            actionArrs.push(cc.callFunc(() => {
                this.closeShow();
            }));
            
            let mainNode = cc.find('Canvas/MainUI');
            if(mainNode)
                mainNode.runAction(cc.sequence(actionArrs));

        }
    
    },

    getOtherMembersName(arr,name){
        let arr1 = arr.filter(e =>{
            return e.name != name;
        })


        let nameArrs = [];
        arr1.forEach(e=>{
            nameArrs.push(e.name);
        });

        return nameArrs;
    },
    
    getGameLogic() {
        if (cc.ll.runningSceneName == 'MainScene') {
            let logic = cc.find('Canvas').getComponent('GameLogic');
            if (logic != null) {
                return logic;
            }
        }
        return null;
    },

    closeShow(){
        this.node.destroy();
    }


});
