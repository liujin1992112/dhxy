let GameRes = require('../game/GameRes');
let PubFunc = require('../game/PubFunction');

cc.Class({
	extends: cc.Component,

	properties: {
		nameLab: cc.Label,
		levelLab: cc.Label,
		headIcon: cc.Sprite,
		raceNodes: [cc.Node],
		normalNode: cc.Node,
		selectedNode: cc.Node,
		detailNode: cc.Prefab,
	},

	onLoad() {},

	loadInfo(info) {
		this.friendInfo = info;
		this.nameLab.string = info.name;
		if (info.level == -1) {
			this.levelLab.string = '离线';
			this.levelLab.node.color = cc.color(255, 0, 0);
		} else {
			this.levelLab.string = info.level + '级';
			this.levelLab.node.color = PubFunc.getReliveColor(info.relive);
		}

		if (info.race != 0) {
			this.raceNodes[parseInt(info.race) - 1].active = true;
		}
		this.headIcon.spriteFrame = GameRes.getRoleHead(info.resid);
		// if (info.online == 1) {
		// 	this.onlineNode.active = true;
		// }
		// else{
		// 	this.offlineNode.active = true;
		// }
	},

	onDetailBtnClicked(e, d) {
		let detail = cc.instantiate(this.detailNode);
		detail.parent = this.node.parent.parent.parent.parent;
		detail.getComponent('FriendDetail').loadInfo(this.friendInfo, this.node);
	},

	selected() {
		this.selectedNode.active = true;
		this.normalNode.active = false;
	},

	unSelected() {
		this.selectedNode.active = false;
		this.normalNode.active = true;
	}
});