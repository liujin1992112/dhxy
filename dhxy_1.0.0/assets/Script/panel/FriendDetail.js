let GameRes = require('../game/GameRes');
let PubFunc = require('../game/PubFunction');

cc.Class({
	extends: cc.Component,

	properties: {
		nameLab: cc.Label,
		levelLab: cc.Label,
		headIcon: cc.Sprite,
		roleidLab: cc.Label,
		bgNode: cc.Node,
	},

	onLoad() {
		this.node.on(cc.Node.EventType.TOUCH_START, this.touchBegan.bind(this));
	},

	touchBegan(event) {
		if (this.bgNode.getBoundingBoxToWorld().contains(event.getLocation())) {
			return;
		}
		this.node.runAction(cc.sequence(cc.fadeOut(0.1), cc.removeSelf()));
	},

	loadInfo(info, node) {
		this.friendInfo = info;
		this.nameLab.string = info.name;
		if (info.level == -1) {
			this.nameLab.node.color = cc.color(255, 0, 0);
			this.levelLab.node.parent.active = false;
		} else {
			this.levelLab.node.parent.active = true;
			this.nameLab.node.color = PubFunc.getReliveColor(info.relive);
			this.levelLab.string = info.level;
		}
		this.roleidLab.string = info.roleid;
		this.itemNode = node;
		this.headIcon.spriteFrame = GameRes.getRoleHead(info.resid);
	},

	chatBtnClicked(e, d) {
		let panelLogic = this.node.parent.parent.getComponent('FriendPanel');
		panelLogic.selectedFriend(this.itemNode, this.friendInfo.roleid);
		this.node.destroy();
	},

	delBtnClicked(e, d) {
		cc.ll.net.send('c2s_update_friends', {
			roleid: this.friendInfo.roleid,
			operation: 0
		});
		this.node.destroy();
	},
});
