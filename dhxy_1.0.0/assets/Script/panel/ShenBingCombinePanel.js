let GameDefine = require('../game/GameDefine');
let PubFun = require('../game/PubFunction');
let StrCommon = require('../etc/str_common')

cc.Class({
	extends: cc.Component,

	properties: {
		cntLab: cc.Label,
		ItemNode: cc.Node,
		EquipNode: cc.Node,
	},

	onLoad() {
		this.node.name = 'ShenBingCombinePanel';
		this.showInfo();
	},

	finishedCombine(info){
		if (info) {
			this.EquipNode.getComponent('EquipItem').loadInfo(info);
		}
	},


	showInfo() {
		this.ItemNode.getComponent('BagItem').loadInfo({ itemid: 10408, count: 1 });
		let curcnt = cc.ll.player.itemList[10408] == null ? 0 : cc.ll.player.itemList[10408];
		if (curcnt >= 50) {
			this.cntLab.node.color = cc.Color.GREEN;
		}
		else{
			this.cntLab.node.color = cc.Color.RED;
		}
		this.cntLab.string = curcnt;
	
		
	},

	onCombineBtnClicked(e, d){
		let curcnt = cc.ll.player.itemList[10408] == null ? 0 : cc.ll.player.itemList[10408];
		if (curcnt < 50) {
			cc.ll.msgbox.addMsg(StrCommon.SuiPianNotEnough);
			return;
		}
		cc.ll.net.send('c2s_creat_equip',
		{
			 type:2,
			 roleid:cc.ll.player.roleid,
			 index:0
		});
	},

	onCloseBtnClicked(e, d) {
		this.node.destroy();
	}
});
