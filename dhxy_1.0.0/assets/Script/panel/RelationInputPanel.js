let GameDefine = require('../game/GameDefine');
let CPubFunction = require('../game/PubFunction');





cc.Class({
	extends: cc.Component,

	properties: {
        nameEdit: cc.EditBox,
        btnConfirm: cc.Button,
        btnCancel: cc.Button,
        btnClose: cc.Button
	},

	onLoad() {
	},
	
    GetNameLength(strName) {
        let nCnt = 0;

        let nLen = strName.length;

        for (let i = 0; i < nLen; i++) {
            if (/[a-zA-Z0-9]/.test(strName[i]))
                nCnt += 1;
            else
                nCnt += 2;
        }

        return nCnt;

	},
	
	createBtnClicked(e, d) {
		if (this.nameEdit.string == '' || this.nameEdit.string.length > 6){//this.GetNameLength(this.nameEdit.string) > 12) {
			CPubFunction.CreateNotice(cc.find('Canvas'), '名称不能为空，并且最多6个字', 2);
			return;
		}

		/*
		if (cc.ll.player.teamInfo == null || cc.ll.player.teamInfo.objlist == null) {
			return;
		}
		let objs = [];
		//获取结拜同伴roldID
		for(const teamObj in cc.ll.player.teamInfo.objlist){
			if(teamObj.livingtype == 1 && teamObj.roleid != cc.ll.player.roleid){				
				objs.push(teamObj.roleid);
			}
		}*/



		if (!cc.ll.player.teamInfo.objlist || !Array.isArray(cc.ll.player.teamInfo.objlist)) {
			CPubFunction.CreateNotice(cc.find('Canvas'), '先组队才能结拜', 2);
			return false;
		}

		//获取队伍成员列表
		let members = [];
        for (let index = 0; index < cc.ll.player.teamInfo.objlist.length; index++) {
			
			let obj = cc.ll.player.teamInfo.objlist[index];
			if(obj && obj.livingtype == 1){
				members.push(obj.roleid);
			}
		}
		
		cc.ll.net.send('c2s_relation_new', {
			roleId: cc.ll.player.roleid,
			relationType: GameDefine.RelationType.Brother,	
			relationName: this.nameEdit.string,
			members: members
		});
		
		this.btnConfirm.interactable = false;
		this.btnConfirm.node.runAction(cc.sequence(cc.delayTime(1),cc.callFunc(() =>{
			this.btnConfirm.interactable = true;
		})));

	},

	onCloseBtnClicked(e, d) {
		cc.ll.AudioMgr.playCloseAudio();
		this.node.destroy();
	},
});
