let GameDefine = require('../game/GameDefine');
let GameRes = require('../game/GameRes');
let pubFun = require('../game/PubFunction');
let StrCommon = require('../etc/str_common')

cc.Class({
	extends: cc.Component,

	properties: {
		listContent: cc.Node,
		listItem: cc.Node,
		typeNode: cc.Node,
		rateLab: cc.Label,
		hpLab: cc.Label,
		mpLab: cc.Label,
		atkLab: cc.Label,
		spdLab: cc.Label,
		nameLab: cc.Label,

		itemNode: cc.Node,
		needNode: cc.Node,
	},

	onLoad() {
		this.curPetInfo = null;
		this.canhecheng = false;
		this.loadPetList();
	},

	loadPetList() {
		let curListY = -40;
		let curListX = -80;
		this.listContent.destroyAllChildren();

		for (const key in cc.ll.propData.pet) {
			if (key == 'datatype') {
				continue;
			}
			let itemInfo = pubFun.deepClone(cc.ll.propData.pet[key]);
			if (itemInfo.gettype != 1) {
				continue;
			}

			let item = cc.instantiate(this.listItem);
			item.getChildByName('icon').getComponent(cc.Sprite).spriteFrame = GameRes.getPetHead(itemInfo.resid);
			item.getChildByName('level').getComponent(cc.Label).string = itemInfo.xiedailevel;
			item.getChildByName('level').active = false;

			item.active = true;
			item.x = curListX;
			item.y = curListY;
			curListX += 80;
			if (curListX > 80) {
				curListX = -80;
				curListY -= 80;
			}
			item.parent = this.listContent;
			item.petid = key;

			if (this.curPetInfo == null) {
				this.curPetInfo = itemInfo;
				item.getComponent(cc.Toggle).isChecked = true;
				this.showPetInfo();
			}
		}
		if (curListX > -80) {
			curListY -= 80;
		}
		this.listContent.height = -curListY;
		if (this.listContent.height < this.listContent.parent.height) {
			this.listContent.height = this.listContent.parent.height;
		}
	},

	showPetInfo() {
		this.curPetInfo.rate = JSON.parse(this.curPetInfo.rate);
		this.curPetInfo.hp = JSON.parse(this.curPetInfo.hp);
		this.curPetInfo.mp = JSON.parse(this.curPetInfo.mp);
		this.curPetInfo.atk = JSON.parse(this.curPetInfo.atk);
		this.curPetInfo.spd = JSON.parse(this.curPetInfo.spd);
		this.curPetInfo.needitem = JSON.parse(this.curPetInfo.needitem);
		for (let index = 0; index < 4; index++) {
			this.typeNode.getChildByName(index + '').active = false;
		}
		this.node.getChildByName('UIRole').getComponent('UIRole').setInfo({ resid: this.curPetInfo.resid, name: '' });

		this.typeNode.getChildByName(this.curPetInfo.grade + '').active = true;
		this.rateLab.string = this.curPetInfo.rate[0] + ' - ' + this.curPetInfo.rate[1];
		this.hpLab.string = this.curPetInfo.hp[0] + ' - ' + this.curPetInfo.hp[1];
		this.mpLab.string = this.curPetInfo.mp[0] + ' - ' + this.curPetInfo.mp[1];
		this.atkLab.string = this.curPetInfo.atk[0] + ' - ' + this.curPetInfo.atk[1];
		this.spdLab.string = this.curPetInfo.spd[0] + ' - ' + this.curPetInfo.spd[1];
		this.nameLab.string = this.curPetInfo.name;

		this.needNode.destroyAllChildren();
		let curbagList = pubFun.deepClone(cc.ll.player.itemList);
		let startX = -(this.curPetInfo.needitem.length - 1) * 40;
		this.canhecheng = true;
		for (const itemid of this.curPetInfo.needitem) {
			let item = cc.instantiate(this.itemNode);
			item.x = startX;
			item.y = 0;
			startX += 80;
			item.active = true;
			item.parent = this.needNode;
			if (curbagList[itemid]) {
				curbagList[itemid]--;
			} else {
				this.canhecheng = false;
				item.opacity = 150;
			}
			item.getComponent('BagItem').loadInfo({ itemid: itemid, count: 1 });
		}
	},

	petItemClicked(e, d) {
		if (e.target.getComponent(cc.Toggle).isChecked) {
			this.curPetInfo = pubFun.deepClone(cc.ll.propData.pet[e.target.petid]);
			this.showPetInfo();
		}
	},

	hechengBtnClicked(e, d) {
		if (this.canhecheng) {
			cc.ll.net.send('c2s_hecheng_pet', { petid: this.curPetInfo.id });
		} else {
			cc.ll.msgbox.addMsg(StrCommon.HeChengNotEnough);
		}
	},

	onCloseBtnClicked(e, d) {
		let uirolenode = this.node.getChildByName('UIRole');
		uirolenode.getComponent('UIRole').clear();
		this.node.destroy();
	}
});
