
cc.Class({
    extends: cc.Component,

    properties: {
        robBtn: cc.Node,
        fontTxt: cc.Node,
    },


    start () {
        this.fontTxt.runAction(cc.repeatForever(cc.sequence(
            cc.scaleTo(0.3, 1.2),
            cc.scaleTo(0.3, 1.0),
        )));
    },

    onRobClick(e, d){
        cc.ll.net.send('c2s_hongbao_open');
        this.robBtn.active = false;
    },

    onSelfDestroy(){
        this.node.destroy();
    },
});
