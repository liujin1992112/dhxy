let GameDefine = require('../game/GameDefine');
let GameRes = require('../game/GameRes');


cc.Class({
	extends: cc.Component,

	properties: {
		btnConfirm: cc.Button,
		btnLeave: cc.Button,
		lblName: cc.Label,
		lblCreateTime: cc.Label,
		lblRelationId: cc.Label,
		membersPanel: cc.Node,
		

	},

	onLoad() {
	},


	initRelationData(data,operateType){
		let relation = data;
		if(operateType == 1){
			this.btnConfirm.node.active = true;
		}else if(operateType == 2){
			this.btnLeave.node.active = true;
		}


		this.lblName.getComponent(cc.Label).string = relation.name;
		this.relationName = relation.name;
		
		let time = new Date(relation.createTime);
		let str_time = time.getFullYear() + '-' + (time.getMonth() + 1) + '-' + time.getDate();



		this.lblCreateTime.getComponent(cc.Label).string = str_time;

		this.lblRelationId.getComponent(cc.Label).string = relation.relationId;
		this.relationId = relation.relationId;
		this.relationType = relation.relationType;



		let members = JSON.parse(relation.members);
		this.membersId = [];
		for(var i = 0;i < members.length;i++){
            let member = members[i];
			
            let str = 'Player_' + i;
            let playerPanel = this.membersPanel.getChildByName(str);
            if(playerPanel){
                let headIcon = playerPanel.getChildByName('headicon');
				let sp = GameRes.getRoleHead(member.resid);
				if (sp) {
					headIcon.getComponent(cc.Sprite).spriteFrame = sp;
				}

				let lblRoleId = playerPanel.getChildByName('LblRoleId');
				if(lblRoleId){
					lblRoleId.getComponent(cc.Label).string = member.playerid;
				}
				playerPanel.active = true;
			}
			this.membersId.push(member.playerid);			
		}
		
		

	},

	onBtnConfirmClicked(e, d) {
		if (!cc.ll.player.teamInfo.objlist || !Array.isArray(cc.ll.player.teamInfo.objlist)) {
            return false;
		}
		let members = [];
        for (let index = 0; index < cc.ll.player.teamInfo.objlist.length; index++) {
			
			let obj = cc.ll.player.teamInfo.objlist[index];
			if(obj.livingtype == 1)
				members.push(obj.roleid);
			
        }
		if(members.length >= 2){
			cc.ll.net.send('c2s_relation_add', {
				roleId: cc.ll.player.roleid,
				relationId: this.relationId,
				relationType: this.relationType,	
				relationName: this.relationName,
				members: members
			});
		}
	},

	onBtnLeaveClicked(e,d){

		cc.ll.net.send('c2s_relation_leave', {
			roleId: cc.ll.player.roleid,
			relationId: Number(this.lblRelationId.getComponent(cc.Label).string)	
		});
		
	},

});
