cc.Class({
	extends: cc.Component,

	properties: {
		nameEdit: cc.EditBox,
		aimEdit: cc.EditBox
	},

	onLoad() {
	},

	createBtnClicked(e, d) {
		if (this.nameEdit.string == '') {
			return;
		}
		cc.ll.loading.showLoading(10);
		cc.ll.net.send('c2s_createbang', {
			name: this.nameEdit.string,
			aim: this.aimEdit.string,
			masterid: cc.ll.player.roleid,
			mastername: cc.ll.player.name
		});
	},

	onCloseBtnClicked(e, d) {
		this.node.active = false;
	},
});
