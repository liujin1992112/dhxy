let GameRes = require('../game/GameRes');
let CPubFunction = require('../game/PubFunction');
let CMainPlayerInfo = require('../game/MainPlayerInfo');

cc.Class({
    extends: cc.Component,

	properties: {
        cardsList: [cc.Node],
        btnPartner:cc.Prefab,
        svPartnerList:cc.Prefab,
	},

    onLoad() {
        this.currentBtn = null;
        this.timecnt = 0;
        this.maxtiemcnt = 30;
        this.nCurSelect = -1;
    },

    start() {


    },

    loadInfo(data) {
        this.partnerList = {};
        this.schemeId = data.schemeId;
        this.schemeData = data;
        this.initCardList(data);
       
	},


    initCardList() {

        for (var it in this.schemeData.content.partner) {
            let partnerInfo = CMainPlayerInfo.GetPartner(this.schemeData.content.partner[it]);

            let cardObj = this.cardsList[it];
            if (cardObj) {
                this.hiddenCardInfo(cardObj);
                if (partnerInfo) {
                    this.setCardInfo(cardObj, partnerInfo);
                    this.partnerList[it] = this.schemeData.content.partner[it];
                }
            }
        }
    },


    hiddenCardInfo(card) {
        let goAnmRole = cc.find('anmRole', card);
        goAnmRole.active = false;

        cc.find('labName', card).active = false;
        cc.find('labLevel', card).active = false;
        cc.find('picRace', card).active = false;

    },

    setCardInfo(card, objInfo) {
        console.log(objInfo);

        CPubFunction.SetAnimation(cc.find('anmRole', card), objInfo.resid, 'stand_1'); //zzzErr 需整理资源

        let stData = cc.ll.propData.resanchor[objInfo.resid];

        card.partnerId = objInfo.id;

        let goAnmRole = cc.find('anmRole', card);
        if (stData) {
            goAnmRole.anchorY = stData.anchorY;
        }
        goAnmRole.active = true;

        cc.find('labName', card).getComponent(cc.Label).string = objInfo.name;
        cc.find('labLevel', card).color = CPubFunction.getReliveColor(objInfo.relive);
        cc.find('labLevel', card).getComponent(cc.Label).string = `${objInfo.relive}转${objInfo.level}级`;
        cc.find('labName', card).active = true;
        cc.find('labLevel', card).active = true;
        CPubFunction.SetSpriteFrame(cc.find('picRace', card), 'Common/ui_common', CPubFunction.GetRaceFileName(objInfo.race));
        cc.find('picRace', card).active = true;
    },


    TeamChangePartnerState(e, nIndex) {
        this.ClosePartnerList();

        let stInfo = CMainPlayerInfo.GetPartner(nIndex);//CMainPlayerInfo.vecPartnerInfo[nIndex].id);
        let cardObj = this.cardsList[this.nCurSelect];
        if (cardObj && stInfo) {
            this.setCardInfo(cardObj, stInfo);
        }

        if (this.partnerList.hasOwnProperty(this.nCurSelect)) {
            this.partnerList[this.nCurSelect] = nIndex;
        }
        cc.ll.player.send('c2s_scheme_changePartner', {
            roleId: cc.ll.player.roleid,
            schemeId: this.schemeId,
            partnerId: nIndex,
            order: this.nCurSelect
        });
    },

    ClosePartnerList() {
        CPubFunction.FindAndDeleteNode(cc.find('Canvas/MainUI'), 'svPartnerList');
    },

    ShowRemainPartnerList(e, data) {

        this.nCurSelect = Number(data);

        let selectCard = this.cardsList[Number(data)];

        if (CMainPlayerInfo.vecPartnerInfo.length > 0) {

            let svPartnerList = CPubFunction.CreateSubNode(cc.find('Canvas/MainUI'), { nX: selectCard.nX, nY: selectCard.nY }, this.svPartnerList, 'svPartnerList');

            cc.find('btnClose', svPartnerList).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "SchemePartnerPanel", "ClosePartnerList", 0));

            let content = cc.find('ScrollView/view/content', svPartnerList);

            let nY = 30;
            let count = 0;
            for (var it in CMainPlayerInfo.vecPartnerInfo) {

                let stInfo = CMainPlayerInfo.vecPartnerInfo[it];

                let check = this.cardsList.some(e => {
                    return e.partnerId == stInfo.id;
                })

                if (check)
                    continue;

                count++;

                nY -= 90;
                let goPartner = CPubFunction.CreateSubNode(content, { nX: 140, nY: nY }, this.btnPartner, 'btnPartner');

                CPubFunction.SetSpriteFrame(cc.find('HeadPic/Icon', goPartner), 'Common/huoban', 'huoban_' + stInfo.resid);
                cc.find('Name', goPartner).getComponent(cc.Label).string = stInfo.name;
                cc.find('Level', goPartner).getComponent(cc.Label).string = `${stInfo.relive}转${stInfo.level}级`;
                cc.find('picState', goPartner).active = false;

                goPartner.getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "SchemePartnerPanel", "TeamChangePartnerState", stInfo.id));
            }

            content.height = Math.max(content.height, count * 90);

        } else {
            CPubFunction.CreateNotice(cc.find('Canvas'), '当前没有其他伙伴', 2);
        }

    },

});
