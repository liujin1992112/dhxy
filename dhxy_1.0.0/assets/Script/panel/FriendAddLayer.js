cc.Class({
	extends: cc.Component,

	properties: {
		tuijianNode: cc.Node,
		requestNode: cc.Node,
		searchNode: cc.Node,
		friendsContent: cc.Node,
		friendItem: cc.Prefab,
		searchBox: cc.EditBox,
	},

	onLoad() {
		this.friendList = [];
		this.currentY = 0;
		this.type = 0;
	},

	showTuiJianPanel() {
		this.requestNode.active = false;
		this.searchNode.active = false;
		this.tuijianNode.active = true;
	},

	showSearchPanel() {
		this.requestNode.active = false;
		this.searchNode.active = true;
		this.tuijianNode.active = false;
	},

	showRequestPanel() {
		this.requestNode.active = true;
		this.searchNode.active = false;
		this.tuijianNode.active = false;
	},

	setPanelType(type) {//type=0 推荐 1 搜索  2 申请
		this.type = type;
		if (type == 1) {
			this.showSearchPanel();
		}
		else if (type == 2) {
			this.showRequestPanel();
		}
		else {
			this.showTuiJianPanel();
		}

		this.friendsContent.destroyAllChildren();
		if (type == 0) {
			cc.ll.net.send('c2s_search_friends', {
				type: 0,
				roleid: cc.ll.player.roleid,
				data: cc.ll.player.level
			});
		}
	},

	showFriendList(list) {
		this.friendList = list;
		this.currentY = 0;
		this.friendsContent.destroyAllChildren();
		this.friendsContent.height = this.friendsContent.parent.height;
		for (let index = 0; index < list.length; index++) {
			const info = list[index];
			let friendItem = cc.instantiate(this.friendItem);
			friendItem.parent = this.friendsContent;
			friendItem.getComponent('FriendAddItem').loadInfo(info, this.type, this);
			friendItem.x = -148 + (index % 2) * 296;
			friendItem.y = this.currentY;

			if (friendItem.y - friendItem.height < -this.friendsContent.height) {
				this.friendsContent.height = friendItem.height - friendItem.y;
			}
			if (index % 2 == 1) {
				this.currentY -= friendItem.height;
			}
		}
	},

	delItem(roleid) {
		for (let index = 0; index < this.friendList.length; index++) {
			if (this.friendList[index].roleid == roleid) {
				this.friendList.splice(index, 1);
				this.showFriendList(this.friendList);
			}
		}
	},

	onAllRejectBtnClicked(e, d) {
		cc.ll.net.send('c2s_update_friends', {
			operation: 4
		});
	},

	onAllAgreeBtnClicked(e, d) {
		cc.ll.net.send('c2s_update_friends', {
			operation: 3
		});
	},

	onSearchBtnClicked(e, d) {
		this.setPanelType(1);
		cc.ll.net.send('c2s_search_friends', {
			type: 1,
			roleid: cc.ll.player.roleid,
			data: this.searchBox.string
		});
	},

	onBackBtnClicked(e, d) {
		this.setPanelType(0);
	},

	onCloseBtnClicked(e, d) {
		this.node.destroy();
	},
});
