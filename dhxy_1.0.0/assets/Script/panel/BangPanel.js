cc.Class({
	extends: cc.Component,

	properties: {
		bangNode: cc.Node,
		addNode: cc.Node,
		createNode: cc.Node,
		xiulianNode: cc.Node,
		requestNode: cc.Node,
		upgradeNode: cc.Node,

		biddingNode: cc.Node,
	},

	onLoad() {
		if (cc.ll.player.bangid == 0){
			this.showAddlayer();
			cc.ll.net.send('c2s_getbanglist', {
				roleid: cc.ll.player.roleid
			});
		}
		else{
			this.showBangLayer();
			cc.ll.net.send('c2s_getbanginfo', {
				roleid: cc.ll.player.roleid,
				bangid: cc.ll.player.bangid
			});
		}
	},

	showBangLayer() {
		this.bangNode.active = true;
		this.addNode.active = false;
		this.createNode.active = false;
		// this.xiulianNode.active = false;
	},

	showAddlayer() {
		this.bangNode.active = false;
		this.addNode.active = true;
		this.createNode.active = false;
		this.xiulianNode.active = false;
	},

	createBtnClicked(e, d) {
		this.createNode.active = true;
	},

	xiulianBtnClicked(e, d) {
		this.xiulianNode.active = true;
		this.xiulianNode.getComponent('BangXiulian').loadInfo();
	},

	upgradeBtnClicked(e, d){
		this.upgradeNode.active = true;
		this.upgradeNode.getComponent('BangUpgrade').loadInfo();
	},

	showBangInfo(data) {
		this.bangNode.getComponent('BangInfo').loadBangInfo(data);
		// this.requestNode.getComponent('BangList').showRequestList(data.requestlist);
	},

	showBangList(data){
		this.addNode.getComponent('BangAdd').loadBangList(data);
	},

	showBidding(){
		this.biddingNode.active = true;
		this.biddingNode.getComponent('BiddingPanel').onReset();
	},

	hideBidding(){
		this.biddingNode.active = false;
	},

	backToBang(){
		this.node.destroy();
		let gamelogic = this.node.parent.getComponent('GameLogic');
		gamelogic.mapLogic.changeMap(3002);
	},

	onRequestBtnClicked(e, d){
		this.requestNode.active = true;
		cc.ll.net.send('c2s_getbangrequest', {
			roleid: cc.ll.player.roleid,
			bangid: cc.ll.player.bangid
		});
	},

	onCloseBtnClicked(e, d) {
		//zfy --关闭音效
		cc.ll.AudioMgr.playAudio('ui/ui_guanbi');
		this.bangNode.getComponent('BangInfo').clear();
		this.node.destroy();
	},
});
