let gameDefine = require('../game/GameDefine');
let GameRes = require('../game/GameRes');


cc.Class({
	extends: cc.Component,

	properties: {
		propertyNode: cc.Node,
		addPointNode: cc.Node,
	},

	onLoad() {
		this.currentBtn = null;
		this.timecnt = 0;
		this.maxtiemcnt = 30;
	},

	loadInfo(data){
		this.resetBtnStatus();
		this.loadGameData(data);
	},

	loadGameData(data) {

		this.attr1 = {};
		this.addpoint = {};
		this.qianneng = 0;

		this.schemeId = 0;

		this.schemeId = data.schemeId;

		this.activate = data.activate;

		this.addpoint = data.content.attribute.addPoint;
		this.addpointT = JSON.parse(JSON.stringify(this.addpoint));
		
		this.attr1 = cc.ll.player.gameData.attr1;

		this.qianneng = data.content.attribute.qianNeng;
		this.qiannengT = this.qianneng;

		let showData = {addpoint1:this.attr1,qneng:this.qianneng};
		this.showInfo();
	},


	showInfo() {

		cc.find('blood/gengu', this.propertyNode).getComponent(cc.Label).string = Number(cc.ll.player.gameData.level) +  Number(this.addpoint[gameDefine.AttrTypeL2.GENGU]);// + Number(this.addpoint[gameDefine.AttrTypeL2.GENGU]));
		cc.find('blood/genguadd', this.propertyNode).getComponent(cc.Label).string = '(' + (Number(cc.ll.player.gameData.level)) + '+' +  this.addpoint[gameDefine.AttrTypeL2.GENGU]  + ')';// + Number(this.addpoint[gameDefine.AttrTypeL2.GENGU])) + '+0)';
		cc.find('skill/lingxing', this.propertyNode).getComponent(cc.Label).string = Number(cc.ll.player.gameData.level) +  Number(this.addpoint[gameDefine.AttrTypeL2.LINGXING]);// + Number(this.addpoint[gameDefine.AttrTypeL2.LINGXING]));
		cc.find('skill/lingxingadd', this.propertyNode).getComponent(cc.Label).string = '(' + (Number(cc.ll.player.gameData.level)) + '+' +  this.addpoint[gameDefine.AttrTypeL2.LINGXING]  + ')';// + Number(this.addpoint[gameDefine.AttrTypeL2.LINGXING])) + '+0)';
		cc.find('attack/liliang', this.propertyNode).getComponent(cc.Label).string = Number(cc.ll.player.gameData.level) +  Number(this.addpoint[gameDefine.AttrTypeL2.LILIANG]);// + Number(this.addpoint[gameDefine.AttrTypeL2.LILIANG]));
		cc.find('attack/liliangadd', this.propertyNode).getComponent(cc.Label).string = '(' + (Number(cc.ll.player.gameData.level)) + '+' +  this.addpoint[gameDefine.AttrTypeL2.LILIANG]  + ')';// + Number(this.addpoint[gameDefine.AttrTypeL2.LILIANG])) + '+0)';
		cc.find('speed/minjie', this.propertyNode).getComponent(cc.Label).string = Number(cc.ll.player.gameData.level) +  Number(this.addpoint[gameDefine.AttrTypeL2.MINJIE]);// + Number(this.addpoint[gameDefine.AttrTypeL2.MINJIE]));
		cc.find('speed/minjieadd', this.propertyNode).getComponent(cc.Label).string = '(' + (Number(cc.ll.player.gameData.level)) + '+' +  this.addpoint[gameDefine.AttrTypeL2.MINJIE]  + ')';// + Number(this.addpoint[gameDefine.AttrTypeL2.MINJIE])) + '+0)';
		
		cc.find('qianneng/qianneng', this.propertyNode).getComponent(cc.Label).string = this.qianneng;


		cc.find('QixueNode/point/point', this.addPointNode).getComponent(cc.Label).string = this.addpoint[gameDefine.AttrTypeL2.GENGU];
		cc.find('FaliNode/point/point', this.addPointNode).getComponent(cc.Label).string = this.addpoint[gameDefine.AttrTypeL2.LINGXING];
		cc.find('GongjiNode/point/point', this.addPointNode).getComponent(cc.Label).string = this.addpoint[gameDefine.AttrTypeL2.LILIANG];
		cc.find('SuduNode/point/point', this.addPointNode).getComponent(cc.Label).string = this.addpoint[gameDefine.AttrTypeL2.MINJIE];


		cc.find('qianNeng/qianneng', this.addPointNode).getComponent(cc.Label).string = this.qianneng;

		this.initBtn(cc.find('FaliNode/point/addBtn', this.addPointNode), 101, 0);
		this.initBtn(cc.find('FaliNode/point/subBtn', this.addPointNode), 101, 1);
		this.initBtn(cc.find('GongjiNode/point/addBtn', this.addPointNode), 102, 0);
		this.initBtn(cc.find('GongjiNode/point/subBtn', this.addPointNode), 102, 1);
		this.initBtn(cc.find('QixueNode/point/addBtn', this.addPointNode), 100, 0);
		this.initBtn(cc.find('QixueNode/point/subBtn', this.addPointNode), 100, 1);
		this.initBtn(cc.find('SuduNode/point/addBtn', this.addPointNode), 103, 0);
		this.initBtn(cc.find('SuduNode/point/subBtn', this.addPointNode), 103, 1);

		this.initPropertBtn();
	},

	initBtn(btn, data, type) {
			btn.datainfo = data;
			btn.opertype = type;
			btn.on(cc.Node.EventType.TOUCH_START, this.propertyBtnClick.bind(this));
			btn.on(cc.Node.EventType.TOUCH_CANCEL, this.propertyBtnClick.bind(this));
			btn.on(cc.Node.EventType.TOUCH_END, this.propertyBtnClick.bind(this));
	},

	propertyBtnClick(e) {
		if (e.type == cc.Node.EventType.TOUCH_START) {
			this.maxtiemcnt = 30;
			this.timecnt = 0;
			this.currentBtn = e.target;
		}
		else if (e.type == cc.Node.EventType.TOUCH_END || e.type == cc.Node.EventType.TOUCH_CANCEL) {
			this.timecnt = this.maxtiemcnt;
			this.update();
			this.currentBtn = null;
		}
	},

	update() {
		if (this.currentBtn == null) {
			return;
		}
		this.timecnt++;
		if (this.timecnt >= this.maxtiemcnt) {
			if (this.maxtiemcnt > 4) {
				this.maxtiemcnt -= 2;
			}
			this.timecnt = 0;
			if (this.currentBtn.opertype == 0) {
				this.propertyAddPoint(this.currentBtn.datainfo);
			}
			else if (this.currentBtn.opertype == 1) {
				this.propertySubPoint(this.currentBtn.datainfo);
			}
		}
	},

	onClickedBtnAddPoint(e, d) {
		this.propertyNode.active = false;
		this.addPointNode.active = true;
	},

	
	onClickedBtnBack(e, d) {
		this.propertyNode.active = true;
		this.addPointNode.active = false;

		this.addpoint = JSON.parse(JSON.stringify(this.addpointT));
		this.qianneng = this.qiannengT;

		this.showInfo();
	},


	propertyAddPoint(d) {
		if (this.qianneng <= 0) {
			this.currentBtn = null;
			return;
		}
		this.addpoint[d]++;
		this.qianneng--;
		this.showInfo();
	},


	initPropertBtn() {
		this.setPropertBtnInfo('QixueNode', gameDefine.AttrTypeL1.GENGU);
		this.setPropertBtnInfo('FaliNode', gameDefine.AttrTypeL1.LINGXING);
		this.setPropertBtnInfo('GongjiNode', gameDefine.AttrTypeL1.LILIANG);
		this.setPropertBtnInfo('SuduNode', gameDefine.AttrTypeL1.MINJIE);
	},

	setPropertBtnInfo(name, type) {
		if (this.qianneng <= 0) {
			cc.find(`${name}/point/addBtn`, this.addPointNode).active = false;
		} else {
			cc.find(`${name}/point/addBtn`, this.addPointNode).active = true;
		}
		if (this.addpoint[type] > 0) {
			cc.find(`${name}/point/subBtn`, this.addPointNode).active = true;
		} else {
			cc.find(`${name}/point/subBtn`, this.addPointNode).active = false;
		}
	},
	
	resetBtnStatus(){
		this.propertyNode.active = true;
		this.addPointNode.active = false;
	},
	
	propertySubPoint(d) {
		if (this.addpoint[d] <= 0) {
			this.currentBtn = null;
			return;
		}
		this.addpoint[d]--;
		this.qianneng++;
		this.showInfo();
	},

	porpertySure(e, d) {
		cc.ll.net.send('c2s_scheme_addCustomPoint', {
			roleId: cc.ll.player.roleid,
			addPoint: JSON.stringify(this.addpoint),
			qianNeng: this.qianneng,
			schemeId: this.schemeId
		});


		this.propertyNode.active = true;
		this.addPointNode.active = false;
	},

});
