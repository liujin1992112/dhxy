let gameDefine = require('../game/GameDefine');
let roleTitleUtil = require('../utils/RoleTitlesUtil');
cc.Class({
	extends: cc.Component,

	properties: {
		propertyNode: cc.Node,
		addPointNode: cc.Node,
		bangName: cc.Label,

		SetRoleTitleUI: cc.Prefab,
		BangPanel: cc.Prefab,
	},

	onLoad() {
		this.attr1 = {};
		this.addpoint = {};
		this.addpointT = {};
		this.qianneng = 0;
		this.hp = 0;
		this.mp = 0;
		this.maxhp = 1;
		this.maxmp = 1;
		this.atk = 0;
		this.spd = 0;
		this.exp = 0;
		this.maxexp = 1;
		this.loadGameData();
		this.currentBtn = null;
		this.timecnt = 0;
		this.maxtiemcnt = 30;
	},

	loadGameData() {
		this.addpoint = cc.ll.player.gameData.addattr2;
		this.attr1 = cc.ll.player.gameData.attr1;
		for (const key in gameDefine.AttrTypeL2) {
			if (gameDefine.AttrTypeL2.hasOwnProperty(key)) {
				this.addpointT[gameDefine.AttrTypeL2[key]] = 0;
			}
		}
		this.qianneng = cc.ll.player.gameData.qianneng;
		this.showInfo();
	},

	initBtn(btn, data, type) {
		btn.datainfo = data;
		btn.opertype = type;
		btn.on(cc.Node.EventType.TOUCH_START, this.propertyBtnClick.bind(this));
		btn.on(cc.Node.EventType.TOUCH_CANCEL, this.propertyBtnClick.bind(this));
		btn.on(cc.Node.EventType.TOUCH_END, this.propertyBtnClick.bind(this));
	},

	propertyBtnClick(e) {
		if (e.type == cc.Node.EventType.TOUCH_START) {
			this.maxtiemcnt = 30;
			this.timecnt = 0;
			this.currentBtn = e.target;
		}
		else if (e.type == cc.Node.EventType.TOUCH_END || e.type == cc.Node.EventType.TOUCH_CANCEL) {
			this.timecnt = this.maxtiemcnt;
			this.update();
			this.currentBtn = null;
		}
	},

	update() {
		if (this.currentBtn == null) {
			return;
		}
		this.timecnt++;
		if (this.timecnt >= this.maxtiemcnt) {
			if (this.maxtiemcnt > 4) {
				this.maxtiemcnt -= 2;
			}
			this.timecnt = 0;
			if (this.currentBtn.opertype == 0) {
				this.propertyAddPoint(this.currentBtn.datainfo);
			}
			else if (this.currentBtn.opertype == 1) {
				this.propertySubPoint(this.currentBtn.datainfo);
			}
		}
	},

	showInfo() {
		cc.find('jyProgress', this.propertyNode).getComponent(cc.ProgressBar).progress = cc.ll.player.gameData.exp / cc.ll.player.gameData.maxexp;
		cc.find('jyProgress/progress', this.propertyNode).getComponent(cc.Label).string = cc.ll.player.gameData.exp + '/' + cc.ll.player.gameData.maxexp;

		cc.find('blood/blood', this.propertyNode).getComponent(cc.Label).string = this.attr1[gameDefine.AttrTypeL1.HP] + '/' + this.attr1[gameDefine.AttrTypeL1.MAXHP];
		cc.find('blood/gengu', this.propertyNode).getComponent(cc.Label).string = (Number(this.attr1[gameDefine.AttrTypeL2.GENGU]));// + Number(this.addpoint[gameDefine.AttrTypeL2.GENGU]));
		//cc.find('blood/genguadd', this.propertyNode).getComponent(cc.Label).string = '(' + (Number(this.attr1[gameDefine.AttrTypeL2.GENGU])) + '+0)';// + Number(this.addpoint[gameDefine.AttrTypeL2.GENGU])) + '+0)';
		cc.find('blood/genguadd', this.propertyNode).getComponent(cc.Label).string = '(' + (Number(cc.ll.player.gameData.level)) + '+' + cc.ll.player.gameData.addattr2[gameDefine.AttrTypeL2.GENGU] + ')';// + Number(this.addpoint[gameDefine.AttrTypeL2.GENGU])) + '+0)';
		cc.find('skill/skill', this.propertyNode).getComponent(cc.Label).string = this.attr1[gameDefine.AttrTypeL1.MP]+ '/' + this.attr1[gameDefine.AttrTypeL1.MAXMP];
		cc.find('skill/lingxing', this.propertyNode).getComponent(cc.Label).string = (Number(this.attr1[gameDefine.AttrTypeL2.LINGXING]));// + Number(this.addpoint[gameDefine.AttrTypeL2.LINGXING]));
		//cc.find('skill/lingxingadd', this.propertyNode).getComponent(cc.Label).string = '(' + (Number(this.attr1[gameDefine.AttrTypeL2.LINGXING])) + '+0)';// + Number(this.addpoint[gameDefine.AttrTypeL2.LINGXING])) + '+0)';
		cc.find('skill/lingxingadd', this.propertyNode).getComponent(cc.Label).string = '(' + (Number(cc.ll.player.gameData.level)) + '+' + cc.ll.player.gameData.addattr2[gameDefine.AttrTypeL2.LINGXING] + ')';// + Number(this.addpoint[gameDefine.AttrTypeL2.LINGXING])) + '+0)';
		cc.find('attack/attack', this.propertyNode).getComponent(cc.Label).string = this.attr1[gameDefine.AttrTypeL1.ATK];
		cc.find('attack/liliang', this.propertyNode).getComponent(cc.Label).string = (Number(this.attr1[gameDefine.AttrTypeL2.LILIANG]));// + Number(this.addpoint[gameDefine.AttrTypeL2.LILIANG]));
		//cc.find('attack/liliangadd', this.propertyNode).getComponent(cc.Label).string = '(' + (Number(this.attr1[gameDefine.AttrTypeL2.LILIANG])) + '+0)';// + Number(this.addpoint[gameDefine.AttrTypeL2.LILIANG])) + '+0)';
		cc.find('attack/liliangadd', this.propertyNode).getComponent(cc.Label).string = '(' + (Number(cc.ll.player.gameData.level)) + '+' + cc.ll.player.gameData.addattr2[gameDefine.AttrTypeL2.LILIANG] + ')';// + Number(this.addpoint[gameDefine.AttrTypeL2.LILIANG])) + '+0)';
		cc.find('speed/speed', this.propertyNode).getComponent(cc.Label).string = this.attr1[gameDefine.AttrTypeL1.SPD];
		cc.find('speed/minjie', this.propertyNode).getComponent(cc.Label).string = (Number(this.attr1[gameDefine.AttrTypeL2.MINJIE]));// + Number(this.addpoint[gameDefine.AttrTypeL2.MINJIE]));
		//cc.find('speed/minjieadd', this.propertyNode).getComponent(cc.Label).string = '(' + (Number(this.attr1[gameDefine.AttrTypeL2.MINJIE])) + '+0)';// + Number(this.addpoint[gameDefine.AttrTypeL2.MINJIE])) + '+0)';
		cc.find('speed/minjieadd', this.propertyNode).getComponent(cc.Label).string = '(' + (Number(cc.ll.player.gameData.level)) + '+' + cc.ll.player.gameData.addattr2[gameDefine.AttrTypeL2.MINJIE] + ')';// + Number(this.addpoint[gameDefine.AttrTypeL2.MINJIE])) + '+0)';
		cc.find('qianneng/qianneng', this.propertyNode).getComponent(cc.Label).string = cc.ll.player.gameData.qianneng;
		let roleTitleInfo = roleTitleUtil.getRoleTitle(cc.ll.player.titleid,cc.ll.player.titleval,cc.ll.player.bangname);
		if(roleTitleInfo)
			cc.find('chengweiLab', this.propertyNode).getComponent(cc.Label).string = roleTitleInfo.name;




		cc.find('QixueNode/qixue', this.addPointNode).getComponent(cc.Label).string = this.attr1[gameDefine.AttrTypeL1.HP] + '/' + this.attr1[gameDefine.AttrTypeL1.MAXHP];
		cc.find('QixueNode/point/point', this.addPointNode).getComponent(cc.Label).string = this.addpoint[gameDefine.AttrTypeL2.GENGU];
		cc.find('FaliNode/fali', this.addPointNode).getComponent(cc.Label).string = this.attr1[gameDefine.AttrTypeL1.MP]+ '/' + this.attr1[gameDefine.AttrTypeL1.MAXMP];
		cc.find('FaliNode/point/point', this.addPointNode).getComponent(cc.Label).string = this.addpoint[gameDefine.AttrTypeL2.LINGXING];
		cc.find('GongjiNode/atk', this.addPointNode).getComponent(cc.Label).string = this.attr1[gameDefine.AttrTypeL1.ATK];
		cc.find('GongjiNode/point/point', this.addPointNode).getComponent(cc.Label).string = this.addpoint[gameDefine.AttrTypeL2.LILIANG];
		cc.find('SuduNode/spd', this.addPointNode).getComponent(cc.Label).string = this.attr1[gameDefine.AttrTypeL1.SPD];
		cc.find('SuduNode/point/point', this.addPointNode).getComponent(cc.Label).string = this.addpoint[gameDefine.AttrTypeL2.MINJIE];

		this.initBtn(cc.find('FaliNode/point/addBtn', this.addPointNode), 101, 0);
		this.initBtn(cc.find('FaliNode/point/subBtn', this.addPointNode), 101, 1);
		this.initBtn(cc.find('GongjiNode/point/addBtn', this.addPointNode), 102, 0);
		this.initBtn(cc.find('GongjiNode/point/subBtn', this.addPointNode), 102, 1);
		this.initBtn(cc.find('QixueNode/point/addBtn', this.addPointNode), 100, 0);
		this.initBtn(cc.find('QixueNode/point/subBtn', this.addPointNode), 100, 1);
		this.initBtn(cc.find('SuduNode/point/addBtn', this.addPointNode), 103, 0);
		this.initBtn(cc.find('SuduNode/point/subBtn', this.addPointNode), 103, 1);

		cc.find('qianNeng/qianneng', this.addPointNode).getComponent(cc.Label).string = this.qianneng;
		this.initPropertBtn();

		if (cc.ll.player.bangid > 0 && cc.ll.player.bangname != null) {
			this.bangName.string = cc.ll.player.bangname;
		}
	},
	

	initPropertBtn() {
		this.setPropertBtnInfo('QixueNode', gameDefine.AttrTypeL1.GENGU);
		this.setPropertBtnInfo('FaliNode', gameDefine.AttrTypeL1.LINGXING);
		this.setPropertBtnInfo('GongjiNode', gameDefine.AttrTypeL1.LILIANG);
		this.setPropertBtnInfo('SuduNode', gameDefine.AttrTypeL1.MINJIE);
	},

	setPropertBtnInfo(name, type) {
		if (this.qianneng <= 0) {
			cc.find(`${name}/point/addBtn`, this.addPointNode).active = false;
		} else {
			cc.find(`${name}/point/addBtn`, this.addPointNode).active = true;
		}
		if (this.addpointT[type] > 0) {
			cc.find(`${name}/point/subBtn`, this.addPointNode).active = true;
		} else {
			cc.find(`${name}/point/subBtn`, this.addPointNode).active = false;
		}
	},

	addPointBtnClicked(e, d) {
		this.propertyNode.active = false;
		this.addPointNode.active = true;
	},

	backBtnClicked(e, d) {
		this.propertyNode.active = true;
		this.addPointNode.active = false;
	},


	//显示用户称谓设置界面
    onRoleTitleClicked(e, d) {
		let roleTitlePanel = cc.instantiate(this.SetRoleTitleUI);
		roleTitlePanel.name = "SetRoleTitlePanel";
        roleTitlePanel.parent = cc.find('Canvas');
        //cc.ll.AudioMgr.playOpenAudio();
    },


	propertyAddPoint(d) {
		if (this.qianneng <= 0) {
			this.currentBtn = null;
			return;
		}
		this.addpoint[d]++;
		this.addpointT[d]++;
		this.qianneng--;
		this.showInfo();
	},

	propertySubPoint(d) {
		if (this.addpointT[d] <= 0) {
			this.currentBtn = null;
			return;
		}
		this.addpoint[d]--;
		this.addpointT[d]--;
		this.qianneng++;
		this.showInfo();
	},

	porpertySure(e, d) {
		cc.ll.net.send('c2s_player_addpoint', {
			roleid: cc.ll.player.roleid,
			addattr: JSON.stringify(this.addpointT)
		});
	},

	bangBtnClicked(e, d) {
		let bang = cc.instantiate(this.BangPanel);
		bang.parent = cc.find('Canvas');
	},
});
