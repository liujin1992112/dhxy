cc.Class({
	extends: cc.Component,

	properties: {
		listContent: cc.Node,
		upgradeNode: cc.Node,
		tipsNode: cc.Node,
		lowNode: cc.Node,
		highNode: cc.Node,
		chooseNode: cc.Node,

		equipListItem: cc.Prefab,
	},

	ctor(){
		this.issend = false;
		this.listItems = [];
		this.curSelected = null;
		this.curNode = null;
		this.useEquipInfo = null;
	},

	onLoad() {
		this.node.name = 'ShenBingUpPanel';
		this.node.on(cc.Node.EventType.TOUCH_START, this.touchBegan.bind(this));
	},

	start() {
		this.loadEquipList();
	},

	showNextEquipInfo(info) {
		this.upgradeNode.getChildByName('nextEquip').getComponent('EquipItem').loadInfo(info);
	},

	reloadListData(info) {
		for (let index = 0; index < this.listItems.length; index++) {
			if (this.listItems[index].getComponent('EquipListItem').iteminfo.EquipID == info.EquipID) {
				this.listItems[index].getComponent('EquipListItem').loadInfo(info);
				if (this.listItems[index] == this.curSelected) {
					this.showInfo();
				}
			}
		}
	},

	loadEquipList() {
		let curListY = 0;
		this.listContent.destroyAllChildren();
		this.listItems = [];

		this.upgradeNode.active = false;
		this.tipsNode.active = false;
		if (!cc.ll.player.equipdata || !cc.ll.player.equipdata.info) {
			this.tipsNode.active = true;
			this.tipsNode.getChildByName('tips').getComponent(cc.Label).string = '你还没有神兵哦';
			return;
		}
		for (const key in cc.ll.player.equipdata.info) {
			let itemInfo = cc.ll.player.equipdata.info[key];
			if (itemInfo.EquipType != 2) {
				continue;
			}
			let item = cc.instantiate(this.equipListItem);
			item.x = 0;
			item.y = curListY;
			curListY -= 90;
			item.parent = this.listContent;
			item.getComponent('EquipListItem').loadInfo(itemInfo);
			if (cc.ll.player.equipdata.use[itemInfo.EIndex] == itemInfo.EquipID) {
				cc.find('tips', item).active = true;
			}

			let btn = item.getComponent(cc.Button);
			var clickEventHandler = new cc.Component.EventHandler();
			clickEventHandler.target = this.node;
			clickEventHandler.component = "ShenBingUpPanel";
			clickEventHandler.handler = "equipItemClicked";
			btn.clickEvents.push(clickEventHandler);

			this.listItems.push(item);
		}
		this.listContent.height = -curListY;
		if (this.listContent.height < this.listContent.parent.height) {
			this.listContent.height = this.listContent.parent.height;
		}
		if (this.listItems.length == 0) {
			this.tipsNode.active = true;
			this.tipsNode.getChildByName('tips').getComponent(cc.Label).string = '你还没有神兵哦';
			return;
		}
		this.curSelected = this.listItems[0];
		this.curSelected.getComponent('EquipListItem').selected();

		this.upgradeNode.active = true;
		this.showInfo();
	},

	getNextEquipInfo() {
		if (this.curInfo && this.curInfo.NextType > 0) {
			cc.ll.net.send('c2s_next_equip', {
				resid: this.curInfo.NextType,
				type: this.curInfo.EquipType,
				index: this.curInfo.EIndex,
				grade: this.curInfo.Grade + 1,
				roleid: cc.ll.player.roleid
			});
		} else {
			return;
		}
	},

	showInfo() {
		this.highNode.getChildByName('ChooseBtn').active = true;
		this.useEquipInfo = null;
		this.curInfo = this.curSelected.getComponent('EquipListItem').iteminfo;
		if (!this.curInfo || !this.curInfo.BaseAttr) {
			return;
		}

		this.upgradeNode.active = true;
		this.tipsNode.active = false;
		this.lowNode.active = false;
		this.highNode.active = false;
		if (this.curInfo.Grade == 1) {
			this.lowNode.active = true;
			this.curNode = this.lowNode;
		}
		else if(this.curInfo.NextType > 0 && this.curInfo.Grade < 5){
			this.highNode.active = true;
			this.curNode = this.highNode;
		}
		else if(this.curInfo.NextType == 0 || this.curInfo.Grade == 5){
			this.upgradeNode.active = false;
			this.tipsNode.active = true;
			this.tipsNode.getChildByName('tips').getComponent(cc.Label).string = '该装备不能升级';
			return;
		}
		
		this.curNode.getChildByName('EquipItem1').getComponent('EquipItem').loadInfo(this.curInfo);
	
		this.getNextEquipInfo();
	},

	equipItemClicked(e, d) {
		if (this.curSelected == e.target || !e.target.getComponent('EquipListItem').iteminfo.EName) {
			return;
		}
		if (this.curSelected != null) {
			this.curSelected.getComponent('EquipListItem').unSelected();
		}
		this.curSelected = e.target;
		this.curSelected.getComponent('EquipListItem').selected();
		this.curInfo = this.curSelected.getComponent('EquipListItem').iteminfo;

		this.showInfo();
	},
	
	chooseItemClicked(e, d) {
		this.chooseNode.active = true;
		let listContent =  cc.find('view/content', this.chooseNode);
		let curListY = 0;
		listContent.destroyAllChildren();
		for (const key in cc.ll.player.equipdata.info) {
			let itemInfo = cc.ll.player.equipdata.info[key];
			if (itemInfo.EquipType != 2 || itemInfo.Grade >= this.curInfo.Grade) {
				continue;
			}
			let item = cc.instantiate(this.equipListItem);
			item.scaleX = 0.7;
			item.scaleY = 0.7;
			item.x = 0;
			item.y = curListY;
			curListY -= 80;
			item.parent = listContent;
			item.getComponent('EquipListItem').loadInfo(itemInfo);

			let btn = item.getComponent(cc.Button);
			var clickEventHandler = new cc.Component.EventHandler();
			clickEventHandler.target = this.node;
			clickEventHandler.component = "ShenBingUpPanel";
			clickEventHandler.handler = "chooseEquipClicked";
			btn.clickEvents.push(clickEventHandler);
		}
		listContent.height = -curListY;
		if (listContent.height < listContent.parent.height) {
			listContent.height = listContent.parent.height;
		}
	},

	chooseEquipClicked(e, d){
		this.curNode.getChildByName('ChooseBtn').active = false;

		this.useEquipInfo = e.target.getComponent('EquipListItem').iteminfo;
		this.curNode.getChildByName('EquipItem2').getComponent('EquipItem').loadInfo(this.useEquipInfo);
	},

	upgradeBtnClicked(e, d) {
		if(this.curInfo == null){
			return;
		}
		this.chooseNode.active = false;
		if (this.curInfo.Grade > 1 && this.useEquipInfo == null ) {
			return;
		}
		let useid = 0;
		if (this.curInfo.Grade > 1) {
			useid = this.useEquipInfo.EquipID;
		}

		// 避免多次点击
		if(this.issend == true){
			return;
		}
		this.issend = true;

		cc.ll.net.send('c2s_shenbing_upgrade', {
			roleid: cc.ll.player.roleid,
			equipid: this.curInfo.EquipID,
			use: useid
		});

		setTimeout(() => {
			this.issend = false;
		}, 2*1000);

		this.curInfo = null;
	},

	onCloseBtnClicked(e, d) {
		this.node.destroy();
	},

	touchBegan(event) {
		this.chooseNode.active = false;
	},
});
