/*
 * 设置中的安全锁
 */
cc.Class({
	extends: cc.Component,
	properties: {
	},

	onLoad () {
		this.panel_type = null;
	},

	start () {
	},

	/*
	 * 初始化，控制界面显示
	 */
	init () {
		let has_password = (cc.ll.player.safe_password.length > 0);
		let has_lock = (cc.ll.player.safe_lock == 1);
		let rich_text1 = cc.find('RichText1', this.node);
		let rich_text2 = cc.find('RichText2', this.node);
		rich_text1.active = !has_password;
		rich_text2.active = has_password;

		let set_button = cc.find('SettingButton', this.node);
		let change_button = cc.find('ChangeButton', this.node);
		let free_button = cc.find('FreeButton', this.node);
		let lock_button = cc.find('LockButton', this.node);
		set_button.active = !has_password;
		change_button.active = has_password;
		free_button.active = (has_password && has_lock);
		lock_button.active = (has_password && !has_lock);
	},

	/*
	 * 密码控制界面
	 */
	showPasswordPanel (type) {
		this.panel_type = type;
		let set_node = cc.find('SetPassword', this.node);
		set_node.active = true;

		let edit_node1 = cc.find('inputBg1', set_node);
		let edit_node2 = cc.find('inputBg2', set_node);
		let edit_node3 = cc.find('inputBg3', set_node);
		let edit_node4 = cc.find('inputBg4', set_node);
		edit_node1.active = (type == 'change' || type == 'setting');
		edit_node2.active = (type == 'change' || type == 'setting');
		edit_node3.active = (type == 'free');
		edit_node4.active = (type == 'change');
		for (let item of [edit_node1, edit_node2, edit_node3, edit_node4]) {
			let edit = item.getChildByName('EditBox');
			edit.getComponent(cc.EditBox).string = '';
		}

		let tip = cc.find('Tip', set_node);
		tip.active = (type == 'change' || type == 'setting');

		let title = cc.find('Title', set_node);
		title.active = (type == 'setting' || type == 'free');

		if (type == 'setting') { // 设置密码 
			title.getComponent(cc.Label).string = '设置安全锁密码';
		}
		else if (type == 'change') { // 修改密码 
			title.getComponent(cc.Label).string = '修改密码';
		}
		else if (type == 'free') { // 解锁 
			title.getComponent(cc.Label).string = '解锁';
		}
	},

	/*
	 * 修改密码
	 */
	onChangePasswordClick (event, param) {
		this.showPasswordPanel('change');
	},

	/*
	 * 设置密码
	 */
	onSettingPasswordClick (event, param) {
		this.showPasswordPanel('setting');
	},

	/*
	 * 解锁
	 */
	onFreeClick (event, param) {
		this.showPasswordPanel('free');
	},

	/*
	 * 上锁
	 */
	onLockClick (event, param) {
		if (cc.ll.player.safe_lock == 0 && cc.ll.player.safe_password.length > 0) {
			cc.ll.net.send('c2s_safepass_msg', {
				pass: cc.ll.player.safe_password,
				lock: 1,
			});
		}
	},

	/*
	 * 删除密码
	 */
	onDeletePasswordClick (event, param) {
		let str3 = cc.find('SetPassword/inputBg3/EditBox', this.node).getComponent(cc.EditBox).string;
		if (this.panel_type == 'free') {
			if (str3 != cc.ll.player.safe_password) {
				cc.ll.msgbox.addMsg('密码错误！');
				return;
			}
			cc.ll.net.send('c2s_safepass_msg', {
				pass: '',
				lock: 0,
			});
		}
		let set_node = cc.find('SetPassword', this.node);
		set_node.active = false;
	},

	/*
	 * 确定
	 */
	onConfirmClick (event, param) {
		let edit_node1 = cc.find('SetPassword/inputBg1', this.node);
		let edit_node2 = cc.find('SetPassword/inputBg2', this.node);
		let edit_node3 = cc.find('SetPassword/inputBg3', this.node);
		let edit_node4 = cc.find('SetPassword/inputBg4', this.node);
		let str1 = edit_node1.getChildByName('EditBox').getComponent(cc.EditBox).string;
		let str2 = edit_node2.getChildByName('EditBox').getComponent(cc.EditBox).string;
		let str3 = edit_node3.getChildByName('EditBox').getComponent(cc.EditBox).string;
		let str4 = edit_node4.getChildByName('EditBox').getComponent(cc.EditBox).string;

		let has_error = false; // 验证 
		if (!has_error && edit_node1.active && str1 != str2) {
			cc.ll.msgbox.addMsg('两次密码不一致！');
			has_error = true;
		}
		if (!has_error && edit_node3.active && str3 != cc.ll.player.safe_password) {
			cc.ll.msgbox.addMsg('输入密码错误！');
			has_error = true;
		}
		if (!has_error && edit_node4.active && str4 != cc.ll.player.safe_password) {
			cc.ll.msgbox.addMsg('输入密码错误！');
			has_error = true;
		}

		if (!has_error) {
			if (this.panel_type == 'setting') {
				if (!this.verifyPassword(str1)) {
					return;
				}
				cc.ll.net.send('c2s_safepass_msg', {
					pass: str1,
					lock: 0,
				});
			}
			else if (this.panel_type == 'change') {
				if (!this.verifyPassword(str1)) {
					return;
				}
				cc.ll.net.send('c2s_safepass_msg', {
					pass: str1,
					lock: cc.ll.player.safe_lock || 0,
				});
			}
			else if (this.panel_type == 'free') {
				cc.ll.net.send('c2s_safepass_msg', {
					pass: cc.ll.player.safe_password,
					lock: 0,
				});
			}
		}
		let set_node = cc.find('SetPassword', this.node);
		set_node.active = false;
	},

	/*
	 * 取消
	 */
	onCancelClick (event, param) {
		let set_node = cc.find('SetPassword', this.node);
		set_node.active = false;
	},

	/*
	 * 验证密码
	 */
	verifyPassword (pass) {
		if (pass.length < 4 || pass.length > 12) {
			cc.ll.msgbox.addMsg('密码长度不对！');
			return false;
		}
		let regx = /^[A-Za-z0-9]*$/;
		if (!regx.test(pass)) {
			cc.ll.msgbox.addMsg('密码必须为字母或数字！');
			return false;
		}
		return true;
	},

	/*
	 * 检验输入是否合格
	 */
	onEditEnd (event, param) {
		let edit_node1 = cc.find('SetPassword/inputBg1', this.node);
		let edit_node2 = cc.find('SetPassword/inputBg2', this.node);
		let edit_node3 = cc.find('SetPassword/inputBg3', this.node);
		let edit_node4 = cc.find('SetPassword/inputBg4', this.node);
		let str1 = edit_node1.getChildByName('EditBox').getComponent(cc.EditBox).string;
		let str2 = edit_node2.getChildByName('EditBox').getComponent(cc.EditBox).string;
		let str3 = edit_node3.getChildByName('EditBox').getComponent(cc.EditBox).string;
		let str4 = edit_node4.getChildByName('EditBox').getComponent(cc.EditBox).string;
		if (param == 'oldpass') {
			if (edit_node3.active && str3.length > 0 && str3 != cc.ll.player.safe_password) {
				cc.ll.msgbox.addMsg('输入密码错误！');
			}
			else if (edit_node4.active && str4.length > 0 && str4 != cc.ll.player.safe_password) {
				cc.ll.msgbox.addMsg('输入密码错误！');
			}
		}
		else if (param == 'newpass' || param == 'confirmpass') {
			if (str1.length != 0 && str2.length != 0) {
				if (this.verifyPassword(str1) && this.verifyPassword(str2)) {
					if (str1 != str2) {
						cc.ll.msgbox.addMsg('两次密码不一致！');
					}
				}
			}
		}
	},
});
