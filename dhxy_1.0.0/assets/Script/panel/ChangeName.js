cc.Class({
	extends: cc.Component,

	properties: {
		nameEdit: cc.EditBox
	},

	onLoad() {
	},

	createBtnClicked(e, d) {
		if (this.nameEdit.string == '') {
            cc.ll.msgbox.addMsg('名字不能为空');
			return;
		}
		cc.ll.loading.showLoading(30);
		cc.ll.net.send('c2s_changename', {
			roleid: cc.ll.player.roleid,
			name: this.nameEdit.string
		});
		this.node.destroy();
	},

	onCloseBtnClicked(e, d) {
		this.node.destroy();
	},
});
