cc.Class({
	extends: cc.Component,

	properties: {
		bagContent: cc.Node,
		lockerContent: cc.Node,

		shigongLab: cc.Label,
		yinliangLab: cc.Label,
		xianyuLab: cc.Label,

		lockerItem: cc.Prefab,
	},

	onLoad() {
		this.bagSelected = -1;
		cc.ll.net.send('c2s_get_lockeritem', { roleid: cc.ll.player.roleid });
		this.bagList = {};
		this.lockerList = {};
		this.bagEquip = [];
		this.lockerEquip = [];
	},

	loadInfo(baginfo, lockerinfo, equipinfo) {
		this.bagList = baginfo;
		this.lockerList = lockerinfo;
		this.bagEquip = equipinfo.list;
		this.lockerEquip = equipinfo.locker;
		this.loadBagList();
		this.loadLockerList();
	},

	addItem(index, parent) {
		let item = cc.instantiate(this.lockerItem);
		item.x = -132 + (index % 5) * 66;
		item.y = -33 - Math.floor(index / 5) * 66;
		item.parent = parent;
		return item;
	},

	loadBagList() {
		let keys = Object.getOwnPropertyNames(this.bagList);
		this.bagContent.destroyAllChildren();
		let maxLength = 30;
		if (keys.length + this.bagEquip.length > 30) {
			maxLength = keys.length + this.bagEquip.length;
		}
		this.bagContent.height = Math.ceil(maxLength / 5) * 66;
		let curIndex = 0;
		for (const info of this.bagEquip) {
			let item = this.addItem(curIndex++, this.bagContent);
			item.getComponent('LockerItem').panelLogic = this;
			item.getComponent('LockerItem').type = 1;
			item.getComponent('LockerItem').isequip = true;
			item.getComponent('LockerItem').loadEquipInfo(info);
		}

		for (let index = 0; index < maxLength - this.bagEquip.length; index++) {
			if (this.bagList[keys[index]] == 0) {
				continue;
			}
			let item = this.addItem(curIndex++, this.bagContent);
			if (index < keys.length) {
				item.getComponent('LockerItem').panelLogic = this;
				item.getComponent('LockerItem').type = 1;
				item.getComponent('LockerItem').loadInfo(keys[index], this.bagList[keys[index]]);
			}
		}
	},

	loadLockerList() {
		let keys = Object.getOwnPropertyNames(this.lockerList);
		this.lockerContent.destroyAllChildren();
		let maxLength = 30;
		if (keys.length + this.lockerEquip.length > 30) {
			maxLength = keys.length + this.lockerEquip.length;
		}
		this.lockerContent.height = Math.ceil(maxLength / 5) * 66;
		let curIndex = 0;
		for (const info of this.lockerEquip) {
			let item = this.addItem(curIndex++, this.lockerContent);
			item.getComponent('LockerItem').panelLogic = this;
			item.getComponent('LockerItem').type = 0;
			item.getComponent('LockerItem').isequip = true;
			item.getComponent('LockerItem').loadEquipInfo(info);
		}
		for (let index = 0; index < maxLength - this.lockerEquip.length; index++) {
			if (this.lockerList[keys[index]] == 0) {
				continue;
			}
			let item = this.addItem(curIndex++, this.lockerContent);

			if (index < keys.length) {
				item.getComponent('LockerItem').panelLogic = this;
				item.getComponent('LockerItem').type = 0;
				item.getComponent('LockerItem').loadInfo(keys[index], this.lockerList[keys[index]]);
			}
		}
	},

	changeItemType(type, itemid) {
		if (type == 0) {
			if (this.bagList[itemid] == null) this.bagList[itemid] = 0;
			this.bagList[itemid] += this.lockerList[itemid];
			delete this.lockerList[itemid];
		}
		else {
			if (this.lockerList[itemid] == null) this.lockerList[itemid] = 0;
			this.lockerList[itemid] += this.bagList[itemid];
			delete this.bagList[itemid];
		}
		this.loadBagList();
		this.loadLockerList();
		this.sendChange(0, type, itemid);
	},

	changeEquipType(type, info) {
		if (type == 0) {
			let lockerindex = this.lockerEquip.indexOf(info);
			let listindex = this.bagEquip.indexOf(info);
			if (lockerindex != -1) {
				this.lockerEquip.splice(lockerindex, 1);
			}
			if (listindex == -1) {
				this.bagEquip.push(info);
			}
		}
		else {
			let lockerindex = this.lockerEquip.indexOf(info);
			let listindex = this.bagEquip.indexOf(info);
			if (lockerindex == -1) {
				this.lockerEquip.push(info);
			}
			if (listindex != -1) {
				this.bagEquip.splice(listindex, 1);
			}
		}
		this.loadBagList();
		this.loadLockerList();
		this.sendChange(1, type, info.EquipID)
	},

	sendChange(type, operation, operateid) {
		cc.ll.net.send('c2s_update_lockeritem', {
			roleid: cc.ll.player.roleid,
			type: type,
			operation: operation,
			operateid: operateid,
		});
	},

	onCloseBtnClicked(e, d) {
		// let equipInfo = {};
		// equipInfo.locker = [];
		// equipInfo.list = [];
		// for (const info of this.bagEquip) {
		// 	equipInfo.list.push(info.EquipID);
		// }
		// for (const info of this.lockerEquip) {
		// 	equipInfo.locker.push(info.EquipID);
		// }
		// cc.ll.net.send('c2s_update_lockeritem', {
		// 	roleid: cc.ll.player.roleid,
		// 	bag: JSON.stringify(this.bagList),
		// 	locker: JSON.stringify(this.lockerList),
		// 	equip: JSON.stringify(equipInfo),
		// });
		this.node.destroy();
	},
});
