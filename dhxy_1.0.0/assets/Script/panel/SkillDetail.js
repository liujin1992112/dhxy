let SkillMgr = require('../game/Skill');

cc.Class({
	extends: cc.Component,

	properties: {
		bgNode: cc.Node,
		itemIcon: cc.Sprite,
		nameLab: cc.Label,
		infoLab: cc.Label, 
		SkillAtlas: cc.SpriteAtlas,
		operation:cc.Node,
	},

	ctor(){
		this.skillid = 0;
	},

	onLoad() {
		this.node.on(cc.Node.EventType.TOUCH_START, this.touchBegan.bind(this));
		this.operation.active = false;
		this.cbfunc = {};
	},

	loadInfo(skillid) {
		let skillLogic = SkillMgr.GetSkillInfo(skillid);
		this.itemIcon.spriteFrame = this.SkillAtlas.getSpriteFrame(skillLogic.strIcon);
		this.nameLab.string = skillLogic.strName;
		this.infoLab.string = skillLogic.GetDetail();
		this.skillid = skillid;
	},

	showOperation(btncbfuncs){
		this.operation.active = true;
		this.cbfunc = btncbfuncs;
	},

	nodeDestroy(){
		this.node.runAction(cc.sequence(cc.fadeOut(0.1), cc.removeSelf()));
	},

	touchBegan(event) {
		if (this.bgNode.getBoundingBoxToWorld().contains(event.getLocation())) {
			return;
		}
		this.nodeDestroy();
	},

	operationSkill(e, d){
		let func = this.cbfunc[d];
		if(func){
			func();
		}
	},
});
