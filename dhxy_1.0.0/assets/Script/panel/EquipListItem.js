let GameRes = require('../game/GameRes');

cc.Class({
	extends: cc.Component,

	properties: {
		nameLab: cc.Label,
		levelLab: cc.Label,
		equipNode: cc.Node,
		useNode: cc.Node,

		normalNode: cc.Node,
		selectedNode: cc.Node,
	},

	onLoad() {
	},

	loadInfo(info) {
		this.iteminfo = info;
		if (!info.BaseAttr) {
			cc.ll.net.send('c2s_equip_info', {
				roleid: cc.ll.player.roleid,
				equipid: info.EquipID
			});
			return;
		}
		this.nameLab.string = info.EName;
		this.levelLab.string = info.EDesc;
		this.equipNode.getComponent('EquipItem').loadInfo(info);
	},

	selected() {
		this.selectedNode.active = true;
		this.normalNode.active = false;
	},

	unSelected() {
		this.selectedNode.active = false;
		this.normalNode.active = true;
	}
});
