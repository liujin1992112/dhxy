let PracticeMgr = require('../game/RolePracticeMgr');
let GameDefine = require('../game/GameDefine');
let AttrTypeL1 = GameDefine.AttrTypeL1;
cc.Class({
	extends: cc.Component,

	properties: {
		contentNode: cc.Node,
		sureNode: cc.Node,
		xiuNode: cc.Node,

		leftPointLab: cc.Label,
		xiuLevelLab: cc.Label,
		panelReset: cc.Node,
	},

	onLoad() {
	},

	loadInfo() {
		this.xiuNode.active = true;
		this.sureNode.active = false;
		this.dpoint = JSON.parse(JSON.stringify(cc.ll.player.gameData.addattr1));
		this.dpointBase = JSON.parse(JSON.stringify(this.dpoint));
		
		this.attr1 = cc.ll.player.gameData.attr1;
		this.xlevel = cc.ll.player.gameData.xiulevel;
		this.leftpoint = this.xlevel;//PracticeMgr.GetLevelPoint(cc.ll.player.relive, this.xlevel);
		for (const key in this.dpoint) {
			if (this.dpoint.hasOwnProperty(key)) {
				this.leftpoint -= this.dpoint[key];
			}
		}
		if (this.leftpoint < 0) {
			this.leftpoint = 0;
		}

		this.dpointT = {};
		this.dshangxian = this.attr1[AttrTypeL1.BFSWSHANGXIAN];
		this.dfengyin = this.attr1[AttrTypeL1.DFENGYIN];
		this.dhunlun = this.attr1[AttrTypeL1.DHUNLUAN];
		this.dhunshui = this.attr1[AttrTypeL1.DHUNSHUI];
		this.dyiwang = this.attr1[AttrTypeL1.DYIWANG];
		this.dfeng = this.attr1[AttrTypeL1.DFENG];
		this.dlei = this.attr1[AttrTypeL1.DLEI];
		this.dshui = this.attr1[AttrTypeL1.DSHUI];
		this.dhuo = this.attr1[AttrTypeL1.DHUO];
		this.dguihuo = this.attr1[AttrTypeL1.DGUIHUO];
		this.dsanshi = this.attr1[AttrTypeL1.DSANSHI];
		this.ddu = this.attr1[AttrTypeL1.DDU];
		this.pxishou = this.attr1[AttrTypeL1.PXISHOU];
		this.showDInfo();
	},

	calculateProperty() {
		this.xiuNode.active = false;
		this.sureNode.active = true;
		this.leftpoint = this.xlevel;//PracticeMgr.GetLevelPoint(cc.ll.player.relive, this.xlevel);
		for (const key in this.dpoint) {
			if (this.dpoint.hasOwnProperty(key)) {
				this.leftpoint -= this.dpoint[key];
			}
		}
		if (this.leftpoint < 0) {
			this.leftpoint = 0;
		}
		(this.dpointT.hasOwnProperty(AttrTypeL1.BFSWSHANGXIAN)) && (this.dshangxian = this.attr1[AttrTypeL1.BFSWSHANGXIAN] + this.dpointT[AttrTypeL1.BFSWSHANGXIAN] * 2.5);
		(this.dpointT.hasOwnProperty(AttrTypeL1.DFENGYIN)) && (this.dfengyin = this.attr1[AttrTypeL1.DFENGYIN] + this.dpointT[AttrTypeL1.DFENGYIN] * 5);
		(this.dpointT.hasOwnProperty(AttrTypeL1.DHUNLUAN)) && (this.dhunlun = this.attr1[AttrTypeL1.DHUNLUAN] + this.dpointT[AttrTypeL1.DHUNLUAN] * 5);
		(this.dpointT.hasOwnProperty(AttrTypeL1.DHUNSHUI)) && (this.dhunshui = this.attr1[AttrTypeL1.DHUNSHUI] + this.dpointT[AttrTypeL1.DHUNSHUI] * 5);
		(this.dpointT.hasOwnProperty(AttrTypeL1.DYIWANG)) && (this.dyiwang = this.attr1[AttrTypeL1.DYIWANG] + this.dpointT[AttrTypeL1.DYIWANG] * 5);
		if (this.dfengyin > this.dshangxian) {
			this.dfengyin = this.dshangxian;
		}
		if (this.dhunlun > this.dshangxian) {
			this.dhunlun = this.dshangxian;
		}
		if (this.dhunshui > this.dshangxian) {
			this.dhunshui = this.dshangxian;
		}
		if (this.dyiwang > this.dshangxian) {
			this.dyiwang = this.dshangxian;
		}
		
		(this.dpointT.hasOwnProperty(AttrTypeL1.DFENG)) && (this.dfeng = this.attr1[AttrTypeL1.DFENG] + this.dpointT[AttrTypeL1.DFENG] * 5);
		(this.dpointT.hasOwnProperty(AttrTypeL1.DLEI)) && (this.dlei = this.attr1[AttrTypeL1.DLEI] + this.dpointT[AttrTypeL1.DLEI] * 5);
		(this.dpointT.hasOwnProperty(AttrTypeL1.DSHUI)) && (this.dshui = this.attr1[AttrTypeL1.DSHUI] + this.dpointT[AttrTypeL1.DSHUI] * 5);
		(this.dpointT.hasOwnProperty(AttrTypeL1.DHUO)) && (this.dhuo = this.attr1[AttrTypeL1.DHUO] + this.dpointT[AttrTypeL1.DHUO] * 5);
		(this.dpointT.hasOwnProperty(AttrTypeL1.DGUIHUO)) && (this.dguihuo = this.attr1[AttrTypeL1.DGUIHUO] + this.dpointT[AttrTypeL1.DGUIHUO] * 5);
		(this.dpointT.hasOwnProperty(AttrTypeL1.DSANSHI)) && (this.dsanshi = this.attr1[AttrTypeL1.DSANSHI] + this.dpointT[AttrTypeL1.DSANSHI] * 5);
		(this.dpointT.hasOwnProperty(AttrTypeL1.DDU)) && (this.ddu = this.attr1[AttrTypeL1.DDU] + this.dpointT[AttrTypeL1.DDU] * 5);
		(this.dpointT.hasOwnProperty(AttrTypeL1.PXISHOU)) && (this.pxishou = this.attr1[AttrTypeL1.PXISHOU] + this.dpointT[AttrTypeL1.PXISHOU] * 4);

		this.showDInfo();
	},

	initBtn(btn, data, type) {
		btn.datainfo = data;
		btn.opertype = type;
		btn.on(cc.Node.EventType.TOUCH_START, this.propertyBtnClick.bind(this));
		btn.on(cc.Node.EventType.TOUCH_CANCEL, this.propertyBtnClick.bind(this));
		btn.on(cc.Node.EventType.TOUCH_END, this.propertyBtnClick.bind(this));
	},

	propertyBtnClick(e) {
		if (e.type == cc.Node.EventType.TOUCH_START) {
			this.maxtiemcnt = 30;
			this.timecnt = 0;
			this.currentBtn = e.target;
		}
		else if (e.type == cc.Node.EventType.TOUCH_END || e.type == cc.Node.EventType.TOUCH_CANCEL) {
			this.timecnt = this.maxtiemcnt;
			this.update();
			this.currentBtn = null;
		}
	},

	update() {
		if (this.currentBtn == null) {
			return;
		}
		this.timecnt++;
		if (this.timecnt >= this.maxtiemcnt) {
			if (this.maxtiemcnt > 4) {
				this.maxtiemcnt -= 2;
			}
			this.timecnt = 0;
			if (this.currentBtn.opertype == 0) {
				this.propertyAddPoint(this.currentBtn.datainfo);
			}
			else if (this.currentBtn.opertype == 1) {
				this.propertySubPoint(this.currentBtn.datainfo);
			}
		}
	},

	initKangBtn() {
		this.setKangBtnInfo('item', AttrTypeL1.BFSWSHANGXIAN);
		this.setKangBtnInfo('itemfengyin', AttrTypeL1.DFENGYIN);
		this.setKangBtnInfo('itemhunluan', AttrTypeL1.DHUNLUAN);
		this.setKangBtnInfo('itemhunshui', AttrTypeL1.DHUNSHUI);
		this.setKangBtnInfo('itemyiwang', AttrTypeL1.DYIWANG);

		this.setKangBtnInfo('itemfeng', AttrTypeL1.DFENG);
		this.setKangBtnInfo('itemlei', AttrTypeL1.DLEI);
		this.setKangBtnInfo('itemshui', AttrTypeL1.DSHUI);
		this.setKangBtnInfo('itemhuo', AttrTypeL1.DHUO);

		this.setKangBtnInfo('itemguihuo', AttrTypeL1.DGUIHUO);
		this.setKangBtnInfo('itemsanshi', AttrTypeL1.DSANSHI);
		this.setKangBtnInfo('itemdushang', AttrTypeL1.DDU);
		this.setKangBtnInfo('itemwuli', AttrTypeL1.PXISHOU);
	},

	setKangBtnInfo(name, type) {
		this.initBtn(cc.find(`${name}/point/addBtn`, this.contentNode), type, 0);
		this.initBtn(cc.find(`${name}/point/subBtn`, this.contentNode), type, 1);
		if (this.leftpoint <= 0) {
			cc.find(`${name}/point/addBtn`, this.contentNode).active = false;
		} else {
			cc.find(`${name}/point/addBtn`, this.contentNode).active = true;
		}
		if (this.dpointT[type] > 0) {
			cc.find(`${name}/point/subBtn`, this.contentNode).active = true;
		} else {
			cc.find(`${name}/point/subBtn`, this.contentNode).active = false;
		}
	},

	showDInfo() {
		this.leftPointLab.string = this.leftpoint;
		this.xiuLevelLab.string = '修炼LV.' + this.xlevel;

		cc.find('item/point/num', this.contentNode).getComponent(cc.Label).string = this.dshangxian.toFixed(1) + '%';
		cc.find('item/point/add', this.contentNode).getComponent(cc.Label).string = (this.dpoint[AttrTypeL1.BFSWSHANGXIAN] == null ? 0 : this.dpoint[AttrTypeL1.BFSWSHANGXIAN]);

		cc.find('itemfengyin/point/num', this.contentNode).getComponent(cc.Label).string = this.dfengyin.toFixed(1) + '%';
		cc.find('itemfengyin/point/add', this.contentNode).getComponent(cc.Label).string = (this.dpoint[AttrTypeL1.DFENGYIN] == null ? 0 : this.dpoint[AttrTypeL1.DFENGYIN]);
		cc.find('itemhunluan/point/num', this.contentNode).getComponent(cc.Label).string = this.dhunlun.toFixed(1) + '%';
		cc.find('itemhunluan/point/add', this.contentNode).getComponent(cc.Label).string = (this.dpoint[AttrTypeL1.DHUNLUAN] == null ? 0 : this.dpoint[AttrTypeL1.DHUNLUAN]);
		cc.find('itemhunshui/point/num', this.contentNode).getComponent(cc.Label).string = this.dhunshui.toFixed(1) + '%';
		cc.find('itemhunshui/point/add', this.contentNode).getComponent(cc.Label).string = (this.dpoint[AttrTypeL1.DHUNSHUI] == null ? 0 : this.dpoint[AttrTypeL1.DHUNSHUI]);
		cc.find('itemyiwang/point/num', this.contentNode).getComponent(cc.Label).string = this.dyiwang.toFixed(1) + '%';
		cc.find('itemyiwang/point/add', this.contentNode).getComponent(cc.Label).string = (this.dpoint[AttrTypeL1.DYIWANG] == null ? 0 : this.dpoint[AttrTypeL1.DYIWANG]);

		cc.find('itemfeng/point/num', this.contentNode).getComponent(cc.Label).string = this.dfeng.toFixed(1) + '%';
		cc.find('itemfeng/point/add', this.contentNode).getComponent(cc.Label).string = (this.dpoint[AttrTypeL1.DFENG] == null ? 0 : this.dpoint[AttrTypeL1.DFENG]);
		cc.find('itemlei/point/num', this.contentNode).getComponent(cc.Label).string = this.dlei.toFixed(1) + '%';
		cc.find('itemlei/point/add', this.contentNode).getComponent(cc.Label).string = (this.dpoint[AttrTypeL1.DLEI] == null ? 0 : this.dpoint[AttrTypeL1.DLEI]);
		cc.find('itemshui/point/num', this.contentNode).getComponent(cc.Label).string = this.dshui.toFixed(1) + '%';
		cc.find('itemshui/point/add', this.contentNode).getComponent(cc.Label).string = (this.dpoint[AttrTypeL1.DSHUI] == null ? 0 : this.dpoint[AttrTypeL1.DSHUI]);
		cc.find('itemhuo/point/num', this.contentNode).getComponent(cc.Label).string = this.dhuo.toFixed(1) + '%';
		cc.find('itemhuo/point/add', this.contentNode).getComponent(cc.Label).string = (this.dpoint[AttrTypeL1.DHUO] == null ? 0 : this.dpoint[AttrTypeL1.DHUO]);

		cc.find('itemguihuo/point/num', this.contentNode).getComponent(cc.Label).string = this.dguihuo.toFixed(1) + '%';
		cc.find('itemguihuo/point/add', this.contentNode).getComponent(cc.Label).string = (this.dpoint[AttrTypeL1.DGUIHUO] == null ? 0 : this.dpoint[AttrTypeL1.DGUIHUO]);
		cc.find('itemsanshi/point/num', this.contentNode).getComponent(cc.Label).string = this.dsanshi.toFixed(1) + '%';
		cc.find('itemsanshi/point/add', this.contentNode).getComponent(cc.Label).string = (this.dpoint[AttrTypeL1.DSANSHI] == null ? 0 : this.dpoint[AttrTypeL1.DSANSHI]);
		cc.find('itemdushang/point/num', this.contentNode).getComponent(cc.Label).string = this.ddu.toFixed(1) + '%';
		cc.find('itemdushang/point/add', this.contentNode).getComponent(cc.Label).string = (this.dpoint[AttrTypeL1.DDU] == null ? 0 : this.dpoint[AttrTypeL1.DDU]);
		cc.find('itemwuli/point/num', this.contentNode).getComponent(cc.Label).string = this.pxishou.toFixed(1) + '%';
		cc.find('itemwuli/point/add', this.contentNode).getComponent(cc.Label).string = (this.dpoint[AttrTypeL1.PXISHOU] == null ? 0 : this.dpoint[AttrTypeL1.PXISHOU]);
		
		this.initKangBtn();
	},

	propertyAddPoint(d) {
		if (this.leftpoint <= 0) {
			return;
		}
		if (this.dpointT[d] == null) {
			this.dpointT[d] = 0;
			//this.dpoint[d] = 0;
		}
		this.dpointT[d]++;
		this.dpoint[d]++;
		this.calculateProperty();
	},

	propertySubPoint(d) {
		if (this.dpointT[d] == null || this.dpointT[d] <= 0) {
			return;
		}
		this.dpointT[d]--;
		this.dpoint[d]--;
		this.calculateProperty();
	},


	onClickedShowResetUI(e,d){
		this.panelReset.active = true;
	},

	onClickedCancelResetUI(e,d){
		this.panelReset.active = false;
	},


	porpertyReset(e, d) {
		if (cc.ll.player.gameData.money < GameDefine.resetXiulian) {
			cc.ll.msgbox.addMsg(`银两不足,重置需要消耗${GameDefine.resetXiulian}银两！`);
			return;
		}


		cc.ll.net.send('c2s_xiulian_point', {
			roleid: cc.ll.player.roleid,
			type: 0,
			info: '{}'
		});

		this.panelReset.active = false;

	},

	porpertyCancel(e, d) {
		this.dpoint = JSON.parse(JSON.stringify(this.dpointBase));
		this.dpointT = {};
		this.loadInfo();
	},

	porpertySure(e, d) {
		for (const key in this.dpointT) {
			if (this.dpointT.hasOwnProperty(key)) {
				const num = this.dpointT[key];
				if(num < 0){
					return;
				}
			}
		}
		cc.ll.net.send('c2s_xiulian_point', {
			roleid: cc.ll.player.roleid,
			type: 1,
			info: JSON.stringify(this.dpointT)
		});
	},

	onCloseBtnClicked(e, d) {
		this.node.active = false;
	},
});
