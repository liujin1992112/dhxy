let LivingThing = require('../game/game_object/living_thing');
let GameRes = require('../game/GameRes');
let CPubFunction = require('../game/PubFunction');
let GameDefine = require('../game/GameDefine');
let roleTitleUtil = require('../utils/RoleTitlesUtil');


cc.Class({
    extends: LivingThing,
    properties: {
        weaponNode: cc.Node
    },

    onLoad () {
        this._super();
        this.resid = 0;
        this.run_type = 0;
        this.netInfo = {};

        this.animationNode = this.node.getChildByName('frame');
		this.animationMaskNode = this.animationNode.getChildByName('maskframe');
        this.nameImgTitleNode = this.node.getChildByName('imgTitle');
		this.name_list = ['stand_1', 'stand_3', 'attack_1', 'attack_3', 'run_1', 'run_3', 'magic_1', 'magic_3'];
    },

	start () {
		this.setInfo(cc.ll.player);
	},

	/*
	 * 播放下一个动作
	 * @param next +1 -1
	 */
	playNextAction (next) {
		let node_ani = this.animationNode.getComponent(cc.Animation);
		let cur_name = node_ani.currentClip.name;
		let index = this.name_list.indexOf(cur_name);
		index = (index+next+this.name_list.length)%this.name_list.length;
		if (index >= 0 && index < this.name_list.length) {
			this.loadResThenPlay(this.name_list[index]);
		}
	},

	showColorMask (color1, color2) {
		if (color1.r == 0 && color1.g == 0 && color1.b == 0 && color2.r == 0 && color2.g == 0 && color2.b == 0) {
			this.unshowColorMask();
		}
		else {
			this.animationMaskNodeShow = true;
			this.animationMaskNode.active = true;
		}
		let node_ani = this.animationNode.getComponent(cc.Animation);
		if (node_ani.currentClip) {
			this.loadResThenPlay(node_ani.currentClip.name);
		}
		else {
			this.loadResThenPlay('stand_1');
		}

		this.scheduleOnce(() => {
			let logic = this.animationMaskNode.getComponent('role_color');
			if (color1.r != 0 || color1.g != 0 || color1.b != 0) {
				let t = Math.floor(color1.r) * 1000000 + Math.floor(color1.g) * 1000 + Math.floor(color1.b);
				logic.dH1 = t;
			}
			else {
				logic.dH1 = 0;
			}

			if (color2.r != 0 || color2.g != 0 || color2.b != 0) {
				let t = Math.floor(color2.r) * 1000000 + Math.floor(color2.g) * 1000 + Math.floor(color2.b);
				logic.dH2 = t;
			}
			else {
				logic.dH2 = 0;
			}
		}, 0);
	},

	unshowColorMask () {
		this.animationMaskNodeShow = false;
		this.animationMaskNode.active = false;
	},

    setInfo(objinfo, loadGame) {
        this.netInfo = objinfo;
        this.resAtlas = GameRes.getRoleRes(objinfo.resid);
        this.resid = objinfo.resid;

        this.loadResThenPlay('stand_1');
		// CPubFunction.addWeaponAnimation(this.netInfo.weapon, this.weaponNode, this.animationNode, this, this.resid, 'stand', 1);

        let nID = cc.ll.propData.mapdata[objinfo.mapid].mapid;
        let mapinfo = cc.ll.propData.map[nID];
        this.gridInfoArr = mapinfo.mapInfo;

		if (cc.ll.propData.resanchor[this.resid]) {
			this.animationNode.anchorY = cc.ll.propData.resanchor[this.resid].anchorY;
			this.animationMaskNode.anchorY = this.animationNode.anchorY;
		} 

        if (this.animationNode && this.animationNode.getComponent(cc.Animation)) {
            let clips = this.animationNode.getComponent(cc.Animation).getClips();
            for (const clip of clips) {
                this.animationNode.getComponent(cc.Animation).removeClip(clip, true);
            }
        }
    },

    changeWeapon(weapon) {
        this.netInfo.weapon = weapon;
    },

	loadResThenPlay (name) {
		let index = 0;
        cc.loader.loadRes(`shap/${this.resid}/${name}`, cc.SpriteAtlas, (err, atlas) => {
			++ index;
			if (index == 2) {
				this.playAnimation(name);
			}
		});
        cc.loader.loadRes(`shape_mask/${this.resid}/${name}`, cc.SpriteAtlas, (err, atlas) => {
			++ index;
			if (index == 2) {
				this.playAnimation(name);
			}
		});
	},

	playAnimation(name) {
        if (!this.animationNode.active) {
            return;
        }

        let clips = this.animationNode.getComponent(cc.Animation).getClips();
        for (const clip of clips) {
            if (clip.name == name) {
                this.animationNode.getComponent(cc.Animation).play(name);
				this.playMaskAnimation(name);
				this.scheduleOnce(() => {
					let preScaleX = this.animationNode.scaleX;
					this.animationNode.scaleX = preScaleX > 0 ? 500 / this.animationNode.width : -500 / this.animationNode.width;
					this.animationNode.scaleY = 500 / this.animationNode.height; 
					/* this.animationMaskNode.scaleX = this.animationNode.scaleX;
					this.animationMaskNode.scaleY = this.animationNode.scaleY; */
					this.animationMaskNode.anchorY = this.animationNode.anchorY;
				}, 0); 
                return;
            }
        }
        cc.loader.loadRes(`shap/${this.resid}/${name}`, cc.SpriteAtlas, (err, atlas) => {
            if (!this.animationNode) return;
            let curFrames = atlas.getSpriteFrames();
            let curClip = cc.AnimationClip.createWithSpriteFrames(curFrames, 15);
            curClip.name = name;
			curClip.wrapMode = cc.WrapMode.Loop;

            let nodeAni = this.animationNode.getComponent(cc.Animation);
            nodeAni.addClip(curClip);
            nodeAni.play(name);
			this.playMaskAnimation(name);
			this.scheduleOnce(() => {
				let preScaleX = this.animationNode.scaleX;
				this.animationNode.scaleX = preScaleX > 0 ? 500 / this.animationNode.width : -500 / this.animationNode.width;
				this.animationNode.scaleY = 500 / this.animationNode.height; 
				/* this.animationMaskNode.scaleX = this.animationNode.scaleX;
				this.animationMaskNode.scaleY = this.animationNode.scaleY; */
				this.animationMaskNode.anchorY = this.animationNode.anchorY;
			}, 0); 
        });
    },

	playMaskAnimation (name) {
        if (!this.animationMaskNode.active) {
			return;
		}
		name = name || this.animationNode.getComponent(cc.Animation).currentClip.name;
        let clips = this.animationMaskNode.getComponent(cc.Animation).getClips();
        for (const clip of clips) {
            if (clip.name == name) {
                this.animationMaskNode.getComponent(cc.Animation).play(name);
                return;
            }
        }

        cc.loader.loadRes(`shape_mask/${this.resid}/${name}`, cc.SpriteAtlas, (err, atlas) => {
			if (err) {
				console.log(err);
				return;
			}
            let curFrames = atlas.getSpriteFrames();
            let curClip = cc.AnimationClip.createWithSpriteFrames(curFrames, 15);
            curClip.name = name;
			curClip.wrapMode = cc.WrapMode.Loop;

            let nodeAni = this.animationMaskNode.getComponent(cc.Animation);
            nodeAni.addClip(curClip);
            nodeAni.play(name);
        });
	},


    OnClick() {
		console.log('OnClick');
    },
});

