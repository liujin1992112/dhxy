let GameRes      = require('../game/GameRes');

cc.Class({
    extends: cc.Component,

    properties: {
        musicSlider:     cc.Slider,
        effectSlider:    cc.Slider,
        voiceSlider:     cc.Slider,
        musicProgress:   cc.ProgressBar,
        effectProgress:  cc.ProgressBar,
        voiceProgress:   cc.ProgressBar,

        allToggle:       cc.Toggle,
        musicToggle:     cc.Toggle,
        effectToggle:    cc.Toggle,
        voiceToggle:     cc.Toggle,
        otherHideToggle: cc.Toggle,
        HdToggle:        cc.Toggle,

        viewPanel:       cc.Node,
        soundPanel:      cc.Node,
        gamePanel:       cc.Node,
        fightPanel:      cc.Node,

        headIcon:        cc.Sprite,
        nameLab:         cc.Label,
        numberLab:       cc.Label,
    },

    onLoad() {
        let allchecked = cc.sys.localStorage.getItem('setting_all_checked');
        allchecked = allchecked == null ? false : allchecked;
        let musichecked = cc.sys.localStorage.getItem('setting_music_checked');
        musichecked = musichecked == null ? true : musichecked;
        let effectchecked = cc.sys.localStorage.getItem('setting_effect_checked');
        effectchecked = effectchecked == null ? true : effectchecked;
        let voicechecked = cc.sys.localStorage.getItem('setting_voice_checked');
        voicechecked = voicechecked == null ? true : voicechecked;



        this.allToggle.isChecked = allchecked;
        this.musicToggle.isChecked = musichecked;
        this.effectToggle.isChecked = effectchecked;
        this.voiceToggle.isChecked = voicechecked;


        this.headIcon.spriteFrame = GameRes.getRoleHead(cc.ll.player.resid);
        this.nameLab.string = cc.ll.player.name;
        this.numberLab.string = '编号:' + cc.ll.player.roleid;

        this.currentPanel = this.soundPanel;
    },

    start() {
        let mn = cc.ll.AudioMgr.getMusicVolume();
        let en = cc.ll.AudioMgr.getEffectVolume();
        let vn = cc.ll.AudioMgr.getVoiceVolume();

        this.musicSlider.progress = mn;
        this.effectSlider.progress = en;
        this.voiceSlider.progress = vn;
        this.musicProgress.progress = mn;
        this.effectProgress.progress = en;
        this.voiceProgress.progress = vn;

        let hd = cc.sys.localStorage.getItem('setting_hd');
        this.HdToggle.isChecked = hd == 1;
        this.otherHideToggle.isChecked = cc.ll.allowOtherHide
    },

    onDestroy() {
        cc.sys.localStorage.setItem('setting_all_checked', this.allToggle.isChecked);
        cc.sys.localStorage.setItem('setting_music_checked', this.musicToggle.isChecked);
        cc.sys.localStorage.setItem('setting_effect_checked', this.effectToggle.isChecked);
        cc.sys.localStorage.setItem('setting_voice_checked', this.voiceToggle.isChecked);
    },


    onMenuClicked(e, d) {
        switch (Number(d)) {
            case 1:
                this.currentPanel.active = false;
                this.soundPanel.active = true;
                this.currentPanel = this.soundPanel;
                break;
            case 2:
                this.currentPanel.active = false;
                this.viewPanel.active = true;
                this.currentPanel = this.viewPanel;
                break;
        }
    },

    onCloseClicked(e, d) {
        cc.ll.AudioMgr.playCloseAudio();
        this.node.destroy();
    },

    onSettingMusicBarCallback(slider, customEventData) {
        cc.ll.AudioMgr.setMusicVolume(slider.progress);
        this.musicProgress.progress = slider.progress;
    },
    onSettingEffectBarCallback(slider, customEventData) {
        cc.ll.AudioMgr.setEffectVolume(slider.progress);
        this.effectProgress.progress = slider.progress;
    },
    onSettingVoiceBarCallback(slider, customEventData) {
        cc.ll.AudioMgr.setVoiceVolume(slider.progress);
        this.voiceProgress.progress = slider.progress;
    },



    //声音面板
    allToggleClicked(e, d) {
        if (!this.allToggle.isChecked) {
            this.allToggle.isChecked = true;
            return;
        }
        this.musicToggle.isChecked = false;
        this.effectToggle.isChecked = false;
        this.voiceToggle.isChecked = false;
    },

    singleToggleClicked(e, d) {
        if (this.musicToggle.isChecked == false && this.effectToggle.isChecked == false && this.voiceToggle.isChecked == false) {
            this.allToggle.isChecked = true;
        } else {
            this.allToggle.isChecked = false;
        }
    },


    //界面面板
    onOtherHideClicked(e, d) {
        cc.ll.local.set('setting_otherHide_checked', this.otherHideToggle.isChecked);
        cc.ll.allowOtherHide = this.otherHideToggle.isChecked;
        // cc.ll.hideChanged = true;
        let logic = cc.find('Canvas').getComponent('GameLogic');
        if (logic) {
            logic.setAllPlayerActive();
        }
    },

    onHDpicture(e, d){
        let hdflag = this.HdToggle.isChecked ? 1: 0;
        cc.sys.localStorage.setItem('setting_hd', hdflag);
        if(this.HdToggle.isChecked){
            cc.Texture2D.defaultPixelFormat = cc.Texture2D.PIXEL_FORMAT_RGBA8888;   
        }else{
            cc.Texture2D.defaultPixelFormat = cc.Texture2D.PIXEL_FORMAT_RGBA4444;
        }
    },

    onLogoutBtnClicked(e, d) {
        // cc.ll.net.send('exit');
        cc.ll.net.needNotice = false;
        cc.ll.net.close();
        cc.ll.sceneMgr.changeScene('LoginScene');
    },

	/*
	 * 选择toggle页
	 */
	onChooseToggle (event, param) {
		console.log('onChooseToggle', param);
		let setting_node = cc.find('SettingNode', this.node);
		let safe_node = cc.find('SafeNode', this.node);
		setting_node.active = (param == 'setting');
		safe_node.active = (param == 'safe'); 
		if (param == 'safe') {
			safe_node.getComponent('SafeNode').init();
		}
	},
});
