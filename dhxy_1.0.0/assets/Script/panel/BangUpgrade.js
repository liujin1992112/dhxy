let PracticeMgr = require('../game/RolePracticeMgr');
cc.Class({
	extends: cc.Component,

	properties: {
		progressNode: cc.ProgressBar,
		levelLab: cc.Label,
		consumeLab: cc.Label,
		yinliangLab: cc.Label,
	},

	onLoad() {
	},

	loadInfo() {
		this.progressNode.progress = cc.ll.player.gameData.xiulevel/PracticeMgr.GetMaxPriactiveLevel(cc.ll.player.relive);
		this.levelLab.string = `Lv.${cc.ll.player.gameData.xiulevel}/Lv.${PracticeMgr.GetMaxPriactiveLevel(cc.ll.player.relive)}`;
		this.consumeLab.string = PracticeMgr.GetUpdateYinLiang(cc.ll.player.gameData.xiulevel);
		this.yinliangLab.string = cc.ll.player.gameData.money;
	},

	upgradeBtnClicked(e, d) {
		cc.ll.net.send('c2s_xiulian_upgrade', {
			roleid: cc.ll.player.roleid
		});
	},

	onCloseBtnClicked(e, d) {
		this.node.active = false;
	},
});
