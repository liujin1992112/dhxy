let GameDefine = require('../game/GameDefine');
let schemeArgs = require('./SchemeArgs');
let CPubFunction = require('../game/PubFunction');


cc.Class({
	extends: cc.Component,

	properties: {
		scrollViewContent: cc.Node,
		scrollViewCategoryContent: cc.Node,
		sp_active:cc.Node,
		schemeItem: cc.Prefab,
		equipsPanel: cc.Node,
		propertyPanel: cc.Node,
		defendPanel: cc.Node,
		partnerPanel: cc.Node,
		btnCreateScheme: cc.Button,
		createSchemePanel: cc.Node,		
		createSchemeNameBox: cc.EditBox,
		useSchemePanel: cc.Node,
		categorys: [cc.Node],
		lblNoScheme: cc.Label,
		btnActive: cc.Button
		
	},

	onLoad() { 
		this.modifySchemeName = '';
		this.operate = 0;		//0，新建，1,修改
		this.initView();
    },

    start(){
		this.activateSchemeId = -1;
	},
	
	initView(){
		this.categorys.forEach((e,index) =>{
			switch(index){
				case 0:
					e.comName = 'SchemeEquipsPanel';
					break;
				case 1:
					e.comName = 'SchemePropertyPanel';
					break;
				case 2:
					e.comName = 'SchemeDefendPanel';
					break;
				case 3:
					e.comName = 'SchemePartnerPanel';
					break;
			}
		});
	},
    
    //
    initSchemeList(data){
		this.schemeList = [];
		this.schemeList = JSON.parse(data.schemeList);
		this.scrollViewContent.destroyAllChildren();

		if(this.schemeList && this.schemeList.length == 0){
			this.lblNoScheme.node.active = true;
			this.scrollViewCategoryContent.active = false;
		}else{
			this.lblNoScheme.node.active = false;
			this.scrollViewCategoryContent.active = true;
		}

		this.schemeList.forEach((e,i) =>{
			let schemeItem = cc.instantiate(this.schemeItem);
			if(schemeItem){
				if(e.status == 1){
					this.activateSchemeId = e.schemeId;
				}
				schemeItem.schemeId = e.schemeId;
				schemeItem.name = e.schemeName;
				let lblSchemeName = schemeItem.getChildByName('LblSchemeName');
				if(lblSchemeName){
					lblSchemeName.getComponent(cc.Label).string = e.schemeName;
				}

				
				var toggle = schemeItem.getChildByName('btnToggle').getComponent(cc.Toggle);
				let btnModify = schemeItem.getChildByName('btn_modify').getComponent(cc.Button);
				if(toggle && btnModify){
					toggle.schemeId = e.schemeId;
					if(i == 0){
						toggle.isChecked = true;
						this.selectSchemeId = schemeItem.schemeId;
						schemeArgs.schemeId = this.selectSchemeId;						
					}

					var checkEventHandler = new cc.Component.EventHandler();
					checkEventHandler.target = this; 
					checkEventHandler.component = "SchemeListPanel";
					checkEventHandler.handler = "onSelectScheme";            
					checkEventHandler.customEventData = schemeItem.schemeId;  

					toggle.checkEvents.push(checkEventHandler);

					
					var clickModifyEventHandler = new cc.Component.EventHandler();
					clickModifyEventHandler.target = this.node; 
					clickModifyEventHandler.component = "SchemeListPanel";
					clickModifyEventHandler.handler = "onClickSchemeModify";
					clickModifyEventHandler.customEventData = schemeItem.schemeId;
			
					btnModify.clickEvents.push(clickModifyEventHandler);
					
					if(e.status == 1){
						schemeItem.getChildByName('icon_activation').active = true;
						this.activateSchemeId = e.schemeId;
						this.btnActive.node.active = false;
						this.sp_active.active = true;
					}else if(e.status == -1){
						//
						btnModify.interactable = false;	
						toggle.interactable = false;
						
						var clickBtnEventHandler = new cc.Component.EventHandler();
						clickBtnEventHandler.target = this.node; //这个 node 节点是你的事件处理代码组件所属的节点
						clickBtnEventHandler.component = "SchemeListPanel";//这个是代码文件名
						clickBtnEventHandler.handler = "onClickedShowUseScheme";
						clickBtnEventHandler.customEventData = schemeItem.schemeId;
						
						let node_disabled = schemeItem.getChildByName('node_disabled');
						node_disabled.active = true;
						let button = node_disabled.getComponent(cc.Button);
						button.clickEvents.push(clickBtnEventHandler);
						
					}
				
				}
	
				
				schemeItem.parent = this.scrollViewContent;
			} 
		});

		if(this.schemeList.length > 0){
			//请求第一个属性方案详情
			cc.ll.player.send('c2s_scheme_info',{
				roleId: cc.ll.player.roleid,
				schemeId: this.selectSchemeId
			});
		}

		//this.scrollViewContent.height = Math.max(this.scrollViewContent.height, this.schemeItem.length * 30);		
    
	},

	useScheme(data){
		let children = this.scrollViewContent.getChildren();
		children.forEach((e,index) =>{
			if(e.schemeId == data.schemeId){
				let toggle = e.getChildByName('btnToggle').getComponent(cc.Toggle);
				toggle.interactable = true;

				e.getChildByName('node_disabled').active = false;
				let btnModify = e.getChildByName('btn_modify').getComponent(cc.Button);
				btnModify.interactable = true;	
			}
		});	
	},


	appendSchemeList(data){
		let newScheme = JSON.parse(data.newSchemeInfo);
        let schemeItem = cc.instantiate(this.schemeItem);
        if(schemeItem){
            let lblSchemeName = schemeItem.getChildByName('LblSchemeName');
            if(lblSchemeName){
                lblSchemeName.getComponent(cc.Label).string = newScheme.schemeName;
            }

            if(newScheme.status == 1){
                schemeItem.getChildByName('icon_activation').active = true;
			}
			

            
			var toggle = schemeItem.getComponent(cc.Toggle);
			let btnModify = schemeItem.getChildByName('btn_modify').getComponent(cc.Button);
            if(toggle && btnModify){
                toggle.schemeId = newScheme.schemeId;
				toggle.isChecked = true;

				var checkEventHandler = new cc.Component.EventHandler();
				checkEventHandler.target = this; 
				checkEventHandler.component = "SchemeListPanel";
				checkEventHandler.handler = "onSelectScheme";            
				checkEventHandler.customEventData = newScheme.schemeId;  

				toggle.checkEvents.push(checkEventHandler);
			
			
			
				var clickEventHandler = new cc.Component.EventHandler();
				clickEventHandler.target = this.node; 
				clickEventHandler.component = "SchemeListPanel";
				clickEventHandler.handler = "onClickSchemeModify";
				clickEventHandler.customEventData = newScheme.schemeId;
		
				btnModify.clickEvents.push(clickEventHandler);

				if(newScheme.status == 1){
					schemeItem.getChildByName('icon_activation').active = true;
					this.activateSchemeId = newScheme.schemeId;
				}else if(newScheme.status == -1){
					schemeItem.getChildByName('node_disable').active = true;
					btnModify.interactable = false;			
					toggle.enabled = false;
					schemeItem.getComponent(cc.Button).interactable = true;	
				}
				 
			}


			schemeItem.parent = this.scrollViewContent;
			this.selectSchemeId = newScheme.schemeId;

			this.onSelectScheme(null,this.selectSchemeId);

			this.createSchemePanel.active = false;

			this.lblNoScheme.node.active = false;
			this.scrollViewCategoryContent.active = true;
        }
	},
	

	showCreateSchemePanel(e,d){
		if(d){
			this.operate = Number(d);
		}		
		if(this.operate == 1 && this.modifySchemeName != ''){			
			this.createSchemeNameBox.string = this.modifySchemeName;
		}else{
			this.createSchemeNameBox.string = '';
		}
		
		this.createSchemePanel.active = true;
	},
	
	onClickSchemeModify(e,d){
		this.modifySchemeId = Number(d);
		this.modifySchemeName = e.target.parent.getChildByName('LblSchemeName').getComponent(cc.Label).string;
		this.operate = 1;
		this.showCreateSchemePanel();
	},

	onClickedShowUseScheme(e,d){
		if(cc.ll.player.gameData.money < GameDefine.schemeUseMoney){
			cc.ll.msgbox.addMsg(`激活方案需要消耗${GameDefine.schemeUseMoney}银两，银两不足！`);
			return;
		}
		this.useSchemeId = Number(d);
		this.useSchemePanel.active = true;
	},

	onClickedUseScheme(e,d){
		cc.ll.player.send('c2s_scheme_use',{
			roleId: cc.ll.player.roleid,
			schemeId: this.useSchemeId
		});

	},

			
	onClickCancelUseScheme(e,d){
		cc.ll.AudioMgr.playCloseAudio();
		this.useSchemePanel.active = false;
	},

	  
	onSelectScheme(e,d){
		let children = this.scrollViewContent.getChildren();
		children.forEach((i,index) =>{
			let toggle = i.getChildByName('btnToggle').getComponent(cc.Toggle);

			toggle.isChecked = (e.node.parent.schemeId == i.schemeId);
		});
		this.selectSchemeId = Number(d);
		schemeArgs.schemeId = this.selectSchemeId;						
		cc.ll.player.send('c2s_scheme_info',{
			roleId: cc.ll.player.roleid,
			schemeId: this.selectSchemeId
		});

		this.onClickedCategoryToggle(null,0);
	},

	onClickCreateScheme(e,d){
		if (this.createSchemeNameBox.string == '' || this.createSchemeNameBox.string.length > 6){
			CPubFunction.CreateNotice(cc.find('Canvas'), '名称不能为空，并且最多6个字', 2);
			return;
		}
		if(this.operate == 0){
			cc.ll.player.send('c2s_scheme_create', {
				roleId: cc.ll.player.roleid,
				name: this.createSchemeNameBox.string,
			});			
		}else{
			cc.ll.player.send('c2s_scheme_changeName', {
				roleId: cc.ll.player.roleid,
				schemeId: this.modifySchemeId,
				name: this.createSchemeNameBox.string,
			});
		}


		this.btnCreateScheme.interactable = false;
		this.btnCreateScheme.node.runAction(cc.sequence(cc.delayTime(1),cc.callFunc(() =>{
			this.btnCreateScheme.interactable = true;
		})));	
	},

		
	onClickCancelCreateScheme(e,d){
		cc.ll.AudioMgr.playCloseAudio();
		this.createSchemePanel.active = false;
	},



	onClickedBtnActivate(e,d){
		if(cc.ll.player.gameData.money < GameDefine.schemeActiveMoney){
			cc.ll.msgbox.addMsg(`银两不足，激活方案需${GameDefine.schemeActiveMoney}银两！`);
			return;
		}

		cc.ll.player.send('c2s_scheme_activate',{
			roleId: cc.ll.player.roleid,
    		schemeId: this.selectSchemeId
		});
	

		this.sp_active.active = true;
		this.btnActive.node.active = false;
	},

	onActivateScheme(data){
		let children = this.scrollViewContent.getChildren();
		children.forEach((e,index) =>{
			if(e.schemeId == data.schemeId){
				e.getChildByName('icon_activation').active = true;
			}else{
				e.getChildByName('icon_activation').active = false;
			}
		});
	},

	setSchemeInfo(data){
		data = JSON.parse(data.schemeInfo);
		
		this.categorys.forEach((e,index) =>{
			let logic = e.getComponent(e.comName);
			if(logic){
				logic.loadInfo(data);
			}
			e.active = index == 0 ? true : false;
		});	

		if(data.status == 1){
			this.sp_active.active = true;
			this.btnActive.node.active = false;
		}else{
			this.sp_active.active = false;
			this.btnActive.node.active = true;
		}
		
	},

	setSchemeName(data){
		let children = this.scrollViewContent.getChildren();
		children.forEach((e,index) =>{
			if(e.schemeId == data.schemeId){
				let lblSchemeName = e.getChildByName('LblSchemeName');
				if(lblSchemeName){
					lblSchemeName.getComponent(cc.Label).string = data.newName;
				}
			}
		});
	},

	onClickedCategoryToggle(e,d){
		this.categorys.forEach((m,index)=>{
			if(index == Number(d)){
				m.active = true;
			}else{
				m.active = false;
			}
		});

		
		let toggleList = this.scrollViewCategoryContent.getChildren();
		toggleList.forEach(n =>{			
			n.getChildByName('toggleLabel').color = new cc.Color(114,16,18);
			if(!e){
				n.getComponent(cc.Toggle).isChecked = false;
				if(n.name == 'toggle1'){
					n.getChildByName('toggleLabel').color = cc.Color.WHITE;
					n.getComponent(cc.Toggle).isChecked = true;
				}
			}

		});
		if(e)
			e.node.getChildByName('toggleLabel').color = cc.Color.WHITE;
	}
});
