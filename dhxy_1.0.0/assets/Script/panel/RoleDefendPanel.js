
let DefendFa = {
	DHUNLUAN: 0, // 抗混乱
	DFENGYIN: 1, // 抗封印
	DHUNSHUI: 2, // 抗昏睡
	DDU: 3, // 抗毒
	DFENG: 4, // 抗风
	DHUO: 5, // 抗火
	DSHUI: 6, // 抗水
	DLEI: 7, // 抗雷
	DGUIHUO: 8, // 抗鬼火
	DYIWANG: 9, // 抗遗忘
	DSANSHI: 10, // 抗三尸
	DZHENSHE: 11, // 抗震慑
}

let DefendWuli = {
	PXISHOU: 13, // 物理吸收
	PMINGZHONG: 14, // 命中
	PSHANBI: 15, // 闪避
}

let DefendZeng = {
	HDHUNLUAN: 16, // 忽视抗混乱
	HDFENGYIN: 17, // 忽视抗封印
	HDHUNSHUI: 18, // 忽视抗昏睡
	HDDU: 19, // 忽视抗毒
	HDFENG: 20, // 忽视抗风
	HDHUO: 21, // 忽视抗火
	HDSHUI: 22, // 忽视抗水
	HDLEI: 23, // 忽视抗雷
	HDGUIHUO: 24, // 忽视抗鬼火
	HDYIWANG: 25, // 忽视抗遗忘
	HDSANSHI: 26, // 忽视抗三尸
	HDZHENSHE: 27, // 忽视抗震慑
}

let DefendStr = {
	DHUNLUAN: '抗混乱',
	DFENGYIN: '抗封印',
	DHUNSHUI: '抗昏睡',
	DDU: '抗毒',
	DFENG: '抗风',
	DHUO: '抗火',
	DSHUI: '抗水',
	DLEI: '抗雷',
	DGUIHUO: '抗鬼火',
	DYIWANG: '抗遗忘',
	DSANSHI: '抗三尸',
	DZHENSHE: '抗震慑',
	PXISHOU: '物理吸收',
	PMINGZHONG: '命中率',
	PSHANBI: '闪躲率',
	HDHUNLUAN: '忽视抗混乱',
	HDFENGYIN: '忽视抗封印',
	HDHUNSHUI: '忽视抗昏睡',
	HDDU: '忽视抗毒',
	HDFENG: '忽视抗风',
	HDHUO: '忽视抗火',
	HDSHUI: '忽视抗水',
	HDLEI: '忽视抗雷',
	HDGUIHUO: '忽视抗鬼火',
	HDYIWANG: '忽视抗遗忘',
	HDSANSHI: '忽视抗三尸',
	HDZHENSHE: '忽视抗震慑',
}

cc.Class({
	extends: cc.Component,

	properties: {
		faNode: cc.Node,
		wuliNode: cc.Node,
		zengNode: cc.Node,
		itemNode: cc.Node,
		line1Node: cc.Node,
		line2Node: cc.Node,

		infoContent: cc.Node,
	},

	onLoad() {
		this.items = [];
		this.loadGameData();
	},

	loadGameData() {
		this.wuliNode.active = false;
		this.zengNode.active = false;
		this.itemNode.active = false;
		this.line1Node.active = false;
		this.line2Node.active = false;
		this.curY = -55;
		this.curX = -215;
		this.leftItem = true;
		if (this.items) {
			for (const item of this.items) {
				item.destroy();
			}
		}
		this.items = [];

		let attr1 = cc.ll.player.gameData.attr1;
		for (const key in DefendFa) {
			if (attr1[DefendFa[key]] != null && attr1[DefendFa[key]] != 0) {
				let item = cc.instantiate(this.itemNode);
				this.items.push(item);
				item.getChildByName('title').getComponent(cc.Label).string = DefendStr[key];
				item.getChildByName('value').getComponent(cc.Label).string = attr1[DefendFa[key]].toFixed(1) + '%';
				item.parent = this.infoContent;
				item.active = true;
				item.x = this.curX;
				item.y = this.curY;
				this.leftItem = !this.leftItem;
				this.curX = 15;
				if (this.leftItem) {
					this.curX = -215;
					this.curY -= 40;
				}
			}
		}
		if (!this.leftItem) {
			this.curX = -215;
			this.curY -= 40;
			this.leftItem = true;
		}
		for (const key in DefendWuli) {
			if (attr1[DefendWuli[key]] != null && attr1[DefendWuli[key]] != 0) {
				if (this.wuliNode.active == false) {
					this.line1Node.active = true;
					this.line1Node.y = this.curY + 20;
					
					this.wuliNode.active = true;
					this.wuliNode.y = this.curY - 10;
					this.curY -= 45;
				}
				let item = cc.instantiate(this.itemNode);
				this.items.push(item);
				item.getChildByName('title').getComponent(cc.Label).string = DefendStr[key];
				item.getChildByName('value').getComponent(cc.Label).string = attr1[DefendWuli[key]].toFixed(1) + '%';
				item.parent = this.infoContent;
				item.active = true;
				item.x = this.curX;
				item.y = this.curY;
				this.leftItem = !this.leftItem;
				this.curX = 15;
				if (this.leftItem) {
					this.curX = -215;
					this.curY -= 40;
				}
			}
		}		
		if (!this.leftItem) {
			this.curX = -215;
			this.curY -= 40;
			this.leftItem = true;
		}
		for (const key in DefendZeng) {
			if (attr1[DefendZeng[key]] != null && attr1[DefendZeng[key]] != 0) {
				if (this.zengNode.active == false) {
					
					this.line2Node.active = true;
					this.line2Node.y = this.curY + 20;
					this.zengNode.active = true;
					this.zengNode.y = this.curY - 10;
					this.curY -= 45;
				}
				let item = cc.instantiate(this.itemNode);
				this.items.push(item);
				item.getChildByName('title').getComponent(cc.Label).string = DefendStr[key];
				item.getChildByName('value').getComponent(cc.Label).string = attr1[DefendZeng[key]].toFixed(1) + '%';
				item.parent = this.infoContent;
				item.active = true;
				item.x = this.curX;
				item.y = this.curY;
				this.leftItem = !this.leftItem;
				this.curX = 15;
				if (this.leftItem) {
					this.curX = -215;
					this.curY -= 40;
				}
			}
		}
		if (!this.leftItem) {
			this.curY -= 40;
		}
		this.infoContent.height = -this.curY;
		if (this.infoContent.height < this.infoContent.parent.height) {
			this.infoContent.height = this.infoContent.parent.height;
		}
	},
});
