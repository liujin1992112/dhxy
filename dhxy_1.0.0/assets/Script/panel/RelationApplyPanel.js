let GameDefine = require('../game/GameDefine');
let GameRes = require('../game/GameRes');

cc.Class({
	extends: cc.Component,

	properties: {
        memberPanel: cc.Node,
        loadingBar: cc.ProgressBar,
        lblRelationDesc: cc.RichText,
        btnCancel: cc.Button,
        btnClose: cc.Button,
        btnAgree:cc.Button,
        lblWaitOthers: cc.Label,
        lblLeftTime:cc.Label
	},

	onLoad() {
    },
    
    initApplyData(data){
        console.log('初始化申请信息');
        let leadId = data.leadId;
        this.relationId = data.relationId;
        this.relationType = data.relationType;
        let members = JSON.parse(data.playersInfo);
        
        let membersNum = members.length;
        this.lblRelationDesc.string = `${membersNum}位少侠，确认要结拜为<color=#3CA1DF>${data.relationName}</color>吗？`;

        for(var i = 0;i < members.length;i++){
            let member = members[i];
    
            let str = 'player_' + i;
            let playerPanel = this.memberPanel.getChildByName(str);
            if(playerPanel){
                this.setPlayerInfo(playerPanel,member);
            }
        }
        this.leftTime = 60
        this.lblLeftTime.string = this.leftTime + '秒';

        this.schedule(this.updateProgress,1,60,1);

    },

    updateProgress(){
        this.leftTime = --this.leftTime;
        if(this.leftTime < 0){
            this.unschedule(this.checkPlayerMove);
            this.onCloseBtnClicked();
        }else{
            this.loadingBar.progress = this.leftTime / 60;        
            this.lblLeftTime.string = this.leftTime + '秒';
        }
        
    },

    setPlayerInfo(panel,data){
        let headIcon = panel.getChildByName('headicon');
        let sp = GameRes.getRoleHead(data.resid);
        if (sp) {
            headIcon.spriteFrame = sp;
            headIcon.active = true;
        }
        let lblName = panel.getChildByName('LblName');
        lblName.getComponent(cc.Label).string = data.name;
        lblName.active = true;


        let sp_ready = panel.getChildByName('IconReady');
        if(data.agree){
            if(sp_ready)
                sp_ready.active = true;
            panel.getChildByName('LblWait').active = false;
        }else{
            panel.getChildByName('LblWait').active = true;
        }
        
        panel.getChildByName('LblPlayerId').getComponent(cc.Label).string = data.playerid;
        
        let sp_leader = panel.getChildByName('IconLeader');
        if(sp_leader && data.isLeader){
            sp_leader.active = true;
        }

        if(data.playerid == cc.ll.player.roleid){
            this.btnAgree.node.active = !data.agree;
            this.lblWaitOthers.node.active = data.agree;
            this.btnCancel.node.active = data.agree;
        }

        panel.active = true;

    },

    confirmBtnClicked(e,c){
        cc.ll.player.send('c2s_relation_agree',{
            relationId:this.relationId,
            roleId:cc.ll.player.roleid,
            agree:1
        });
    },


    updateAgreeInfo(data){
        let children = this.memberPanel.getChildren();
        for(const child in children){
            let playerPanel = children[child];
            let lblPlayerId = playerPanel.getChildByName('LblPlayerId').getComponent(cc.Label).string;
            if(data.playerId == Number(lblPlayerId)){
                playerPanel.getChildByName('IconReady').active = true;
                playerPanel.getChildByName('LblWait').active = false;                
            }
        }

        if(data.playerId == cc.ll.player.roleid){
            this.btnAgree.node.active = false;
            this.btnCancel.node.active = true;
            this.lblWaitOthers.node.active = true;
        }
    },

    addLoadingBar(n) {
        this.loadPercent += n;
        // console.log('this.loadPercent', this.loadPercent);
        this.loadingBar.progress = this.loadPercent;
    },

	onCloseBtnClicked(e, d) {

        //关闭则默认拒绝加入
        cc.ll.player.send('c2s_relation_reject',{
            roleId:cc.ll.player.roleid,
            relationId: this.relationId
        });


        cc.ll.AudioMgr.playCloseAudio();
        this.node.destroy();	
    },
});
