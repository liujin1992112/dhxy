let GameRes = require('../game/GameRes');

/**
 * 聊天Item
 */
cc.Class({
	extends: cc.Component,

	properties: {
		headSprite: cc.Sprite,
		nameLabel: cc.Label,
		talkBgNode: cc.Node,
		richTextNode: cc.Node,
		yuyinNode: cc.Node,
	},

	ctor() {
		this.roleid = 0;
		this.voice_index = 0;
	},

	start() {
		this.yuyinNode.active = false;
	},

	loadInfo(info) {
		this.roleid = info.roleid;

		this.nameLabel.string = info.name;
		this.headSprite.spriteFrame = GameRes.getRoleHead(info.resid);
		if (info.msg.length > 0) {
			this.setMsg(info.msg);
		} else {
			if (info.voice >= 0) {
				this.voice_index = info.voice;
				this.yuyinNode.active = true;
				this.setMsg('    [语音消息]    ');
			}
		}
	},

	setMsg(msg) {
		let richText = this.richTextNode.getComponent('CustomRichText');
		richText.string = msg;//info.msg;

		this.talkBgNode.width = richText.node.width + 25;
		this.talkBgNode.height = richText.node.height + 16;
		let bottom = this.talkBgNode.y - this.talkBgNode.height;
		if (-bottom > this.node.height) {
			this.node.height = -bottom;
		}
	},

	onclicked(e, d) {
		if (this.voice_index == 0) {
			return;
		}
		this.playNodeVoice();
	},

	playVoiceAct() {
		this.yuyinNode.stopAllActions();
		let act = cc.repeatForever(cc.sequence(cc.fadeTo(0.2, 80), cc.fadeTo(0.2, 255)));
		this.yuyinNode.runAction(act);
	},

	playNodeVoice(callback) {
		this.playVoiceAct();
		cc.ll.voicemgr.playVoice(this.voice_index, () => {
			this.stopVoice();
			if (callback) {
				callback();
			}
		});
	},

	stopVoice() {
		// cc.ll.voicemgr.playVoice(this.voice_index);
		this.yuyinNode.stopAllActions();
		this.yuyinNode.opacity = 255;
	},
});

