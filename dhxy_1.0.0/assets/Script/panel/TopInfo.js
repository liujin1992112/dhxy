cc.Class({
	extends: cc.Component,

	properties: {
		shigongLab: cc.Label,
		yinliangLab: cc.Label,
		xianyuLab: cc.Label,
		bindLab :cc.Label,

		scoreLayer:cc.Node,
		scoreList:[cc.Label],
		NpcShopUI:cc.Prefab,
    },

    onLoad(){
        this.updateGameMoney();
	},

	updateGameMoney(){
		this.shigongLab.string = '0';

        let getColor = (num) => {
			let color = cc.Color.WHITE;
			if (num >= 0 && num < 1000000) {
				color = cc.Color.WHITE;
			} else if (num >= 1000000 && num < 10000000) {
				color = cc.Color.YELLOW;
			} else if (num >= 10000000 && num < 100000000) {
				color = cc.color(0, 255, 255, 255);
			} else if (num >= 100000000 && num < 1000000000) {
				color = cc.Color.GREEN;
			} else if (num >= 1000000000 && num < 10000000000) {
				color = cc.color(255, 0, 120, 255);
			}
			return color;
		}

		this.yinliangLab.string = cc.ll.player.gameData.money;
        this.yinliangLab.node.color = getColor(cc.ll.player.gameData.money);
        
        this.xianyuLab.string = cc.ll.player.gameData.jade;
		this.xianyuLab.node.color = getColor(cc.ll.player.gameData.jade);
		
		this.bindLab.string = cc.ll.player.gameData.bindjade;
		this.bindLab.node.color = getColor(cc.ll.player.gameData.bindjade);
		
        
        this.scoreList[0].string = cc.ll.player.gameData.shuilugj;
	},
	
	ioScoreLayer(){
		this.scoreLayer.active = !this.scoreLayer.active;
	},

	onExchangeClick(e, d){
		if(d == 'gongji'){
			let CPubFunction = require('../game/PubFunction');
			let GameRes = require('../game/GameRes');
			let goUI = CPubFunction.CreateSubNode(cc.find('Canvas/MainUI'), { nX: 0, nY: 0 }, this.NpcShopUI, 'NpcShopUI');
            let comUI = goUI.getComponent('NpcShopUI');
			comUI.nNpcConfigID = 10242;// 魏征id

			this.node.parent.destroy();
		}
	}
})