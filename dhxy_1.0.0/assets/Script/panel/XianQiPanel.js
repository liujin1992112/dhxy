let GameDefine = require('../game/GameDefine');
let PubFun = require('../game/PubFunction');
let StrCommon = require('../etc/str_common')

cc.Class({
	extends: cc.Component,

	properties: {
		titleNodes: [cc.Node],

		upgradeNode: cc.Node,
		combineNode: cc.Node,
	},

	onLoad() {
		this.node.name = 'XianQiPanel';
		this.showInfo();
	},

	start() {
	},

	showInfo() {
		for (let index = 0; index < 2; index++) {
			if (cc.find(`ToggleGroup/toggle${index + 1}`, this.node).getComponent(cc.Toggle).isChecked) {
				this.panelToggleClicked(null, index + 1);
			}
		}
	},

	panelToggleClicked(e, d) {
		for (const lab of this.titleNodes) {
			lab.active = false;
		}
		this.titleNodes[d - 1].active = true;

		this.upgradeNode.active = false;
		this.combineNode.active = false;
		if (d == 1) {
			this.combineNode.active = true;
			this.combineNode.getComponent('XianQiCombinePanel').getTypeList();
		}
		else if (d == 2) {
			this.upgradeNode.active = true;
			this.upgradeNode.getComponent('XianQiUpPanel').loadEquipList();
			
		}
	},

	onCloseBtnClicked(e, d) {
		this.node.destroy();
	},
});
