cc.Class({
	extends: cc.Component,

	properties: {
		listContent: cc.Node,
		listItem: cc.Node,
	},

	onLoad() {
	},

	getList() {
		cc.ll.net.send('c2s_team_requeslist', {
		});
	},

	showRequestList(list) {
		if (!list) {
			return;
		}
		this.requestList = list;
		this.currentY = 0;
		this.listContent.destroyAllChildren();
		this.listContent.height = this.listContent.parent.height;
		for (let index = 0; index < list.length; index++) {
			const info = list[index];
			let listItem = cc.instantiate(this.listItem);
			listItem.active = true;
			listItem.parent = this.listContent;
			listItem.getComponent('TeamRequestItem').loadInfo(info);
			listItem.x = -148 + (index % 2) * 296;
			listItem.y = this.currentY;

			if (listItem.y - listItem.height < -this.listContent.height) {
				this.listContent.height = listItem.height - listItem.y;
			}
			if (index % 2 == 1) {
				this.currentY -= listItem.height;
			}
		}
	},

	delItem(roleid) {
		for (let index = 0; index < this.requestList.length; index++) {
			if (this.requestList[index].roleid == roleid) {
				this.requestList.splice(index, 1);
				this.showRequestList(this.requestList);
			}
		}
	},

	onCloseBtnClicked(e, d) {
		this.node.active = false;
	},
});
