let CPubFunction = require('../game/PubFunction');
let GameRes = require('../game/GameRes');
cc.Class({
	extends: cc.Component,

	properties: {
		shuxingNode: cc.Node,
		kangxingNode: cc.Node,
		jinengNode: cc.Node,

		roleDragon: dragonBones.ArmatureDisplay,
		
		ChangeName:cc.Prefab,
	},

	onLoad() {
		this.roleDragon.node.active = false;
		this.loadRoleInfo();
		this.name = 'RolePanel';
		this.SetMoneyInfo();
	},

	onPanelBtnClicked(e, d) {
		this.shuxingNode.active = false;
		this.kangxingNode.active = false;
		this.jinengNode.active = false;
		if (d == 1) {
			this.shuxingNode.active = true;
		}
		else if (d == 2) {
			this.kangxingNode.active = true;
		}
		else if (d == 3) {
			this.jinengNode.active = true;
		}
		//zfy --分页音效
		cc.ll.AudioMgr.playFenyeAudio();
	},

	loadRoleInfo() {
		this.resid = cc.ll.player.resid;
		let self = this;
		cc.loader.loadResDir(`dragonbone/${this.resid}`, function (err, assets) {
			cc.sys.isNative && self.roleDragon._factory.clear(false);//在手机上加载相同的资源会出现闪退
			self.roleDragon.dragonAsset = null;//设置新的资源前必须清除原有的资源，在手机会出现程序闪退
			self.roleDragon.dragonAtlasAsset = null;
			for (let i = 0; i < assets.length; i++) {
				if (assets[i] instanceof dragonBones.DragonBonesAsset) {
					self.roleDragon.dragonAsset = assets[i];
				}
				if (assets[i] instanceof dragonBones.DragonBonesAtlasAsset) {
					self.roleDragon.dragonAtlasAsset = assets[i];
				}
			}
			self.roleDragon.node.active = true;
			self.roleDragon.playAnimation('idle', 0);
		});
		cc.find('back/name', this.node).getComponent(cc.Label).string = cc.ll.player.name;
		cc.find('back/level', this.node).color = CPubFunction.getReliveColor(cc.ll.player.relive);
		cc.find('back/level', this.node).getComponent(cc.Label).string = cc.ll.player.relive + '转' + cc.ll.player.level + '级';
		cc.find('back/number', this.node).getComponent(cc.Label).string = '编号:' + cc.ll.player.roleid;
		cc.find('back/race/' + cc.ll.player.race, this.node).active = true;
	},

	loadGameData() {
		this.shuxingNode.getComponent('RolePropertyPanel').loadGameData();
		this.kangxingNode.getComponent('RoleDefendPanel').loadGameData();
		this.jinengNode.getComponent('RoleSkillPanel').loadGameData();
	},

	SetMoneyInfo() {
		// cc.find('topNode/yinliang/ui_common_icon_yinliang/yinliangLabel', this.node).getComponent(cc.Label).string = cc.ll.player.gameData.money;
		// cc.find('topNode/xianyu/ui_common_icon_xianyu/xianyuLabel', this.node).getComponent(cc.Label).string = cc.ll.player.gameData.jade;
		let topnode = cc.find('topInfo', this.node);
		let toplogic = topnode.getComponent('TopInfo');
		toplogic.updateGameMoney();
	},

	changNameBtnClicked(e, d) {
		let bang = cc.instantiate(this.ChangeName);
		bang.parent = cc.find('Canvas');
	},

	resetRoleName(name){
		cc.ll.player.name = name;
		cc.find('back/name', this.node).getComponent(cc.Label).string = cc.ll.player.name;
	},

	onCloseBtnClicked(e, d) {
		//zfy --关闭音效
		cc.ll.AudioMgr.playCloseAudio();
		this.node.destroy();
	},
});
