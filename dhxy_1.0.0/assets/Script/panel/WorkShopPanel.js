let GameDefine = require('../game/GameDefine');
let PubFun = require('../game/PubFunction');
let StrCommon = require('../etc/str_common')

cc.Class({
	extends: cc.Component,

	properties: {
		titleNodes: [cc.Node],
		listContent: cc.Node,

		upgradeNode: cc.Node,
		refineNode: cc.Node,
		inlayNode: cc.Node,
		recastNode: cc.Node,
		tipsNode: cc.Node,

		refinePreNode: cc.Node,
		recastPreNode: cc.Node,

		equipListItem: cc.Prefab,
		shenbingUpPre: cc.Prefab,
		xianqiUpPre: cc.Prefab,
	},

	onLoad() {
		this.name = 'WorkShopPanel';
		this.listItems = [];
		this.curSelected = null;
		this.node.on(cc.Node.EventType.TOUCH_START, this.touchBegan.bind(this));

		this.gemarr = [
			[30025, 30026, 30027, 30028, 30029, 30030],
			[30013, 30014, 30015, 30016, 30017, 30018],
			[30019, 30020, 30021, 30022, 30023, 30024],
			[30001, 30002, 30003, 30004, 30005, 30006],
			[30007, 30008, 30009, 30010, 30011, 30012]
		];
		this.nextEquipInfo = null;

		this.refineData = null;
		this.recastData = null;
		this.refineItemId = 0;
		this.loadEquipList();
	},

	start() {
	},

	reloadListData(info) {
		for (let index = 0; index < this.listItems.length; index++) {
			if (this.listItems[index].getComponent('EquipListItem').iteminfo.EquipID == info.EquipID) {
				this.listItems[index].getComponent('EquipListItem').loadInfo(info);
				if (this.listItems[index] == this.curSelected) {
					this.showInfo();
				}
			}
		}
	},

	loadEquipList() {
		this.upgradeNode.active = false;
		this.refineNode.active = false;
		this.inlayNode.active = false;
		this.recastNode.active = false;
		this.tipsNode.active = false;

		let curListY = 0;
		this.listContent.destroyAllChildren();
		this.listItems = [];

		if (!cc.ll.player.equipdata || !cc.ll.player.equipdata.info) {
			return;
		}
		for (const key in cc.ll.player.equipdata.info) {
			let item = cc.instantiate(this.equipListItem);
			item.x = 0;
			item.y = curListY;
			curListY -= 90;
			item.parent = this.listContent;
			let itemInfo = cc.ll.player.equipdata.info[key];
			item.getComponent('EquipListItem').loadInfo(itemInfo);
			if (cc.ll.player.equipdata.use[itemInfo.EIndex] == itemInfo.EquipID) {
				cc.find('tips', item).active = true;
			}

			let btn = item.getComponent(cc.Button);
			var clickEventHandler = new cc.Component.EventHandler();
			clickEventHandler.target = this.node;
			clickEventHandler.component = "WorkShopPanel";
			clickEventHandler.handler = "equipItemClicked";
			btn.clickEvents.push(clickEventHandler);

			this.listItems.push(item);
		}
		this.listContent.height = -curListY;
		if (this.listContent.height < this.listContent.parent.height) {
			this.listContent.height = this.listContent.parent.height;
		}
		if (this.listItems.length == 0) {
			return;
		}
		this.curSelected = this.listItems[0];
		this.curSelected.getComponent('EquipListItem').selected();
		this.curInfo = this.curSelected.getComponent('EquipListItem').iteminfo;

		this.upgradeNode.active = true;
		this.showInfo();
	},


	getNextEquipInfo() {
		let resid = 0;
		let type = 0;
		let grade = 0;
		if (this.curInfo.EquipType == 0) {
			type = 1;
			grade = 40;
		} else if (this.curInfo.EquipType == 1 && this.curInfo.Grade < 120) {
			type = 1;
			grade = this.curInfo.Grade + 20;
		} else if ((this.curInfo.EquipType == 2 || this.curInfo.EquipType == 3) && this.curInfo.NextType > 0) {
			resid = this.curInfo.NextType;
			type = this.curInfo.EquipType;
			grade = this.curInfo.Grade + 1;
		} else {
			return;
		}
		cc.ll.net.send('c2s_next_equip', {
			resid: resid,
			type: type,
			index: this.curInfo.EIndex,
			grade: grade,
			roleid: cc.ll.player.roleid
		});
	},

	getRecastInfo() {
		let resid = 0;
		let type = 0;
		let grade = 0;
		if (this.curInfo.EquipType == 0 || this.curInfo.EquipType == 2) {
			return;
		}
		else if (this.curInfo.EquipType == 1) {
			type = 1;
			grade = this.curInfo.Grade;
		} else if (this.curInfo.EquipType == 3) {
			resid = 0;
			type = this.curInfo.EquipType;
			grade = this.curInfo.Grade;
		} else {
			return;
		}
		cc.ll.net.send('c2s_next_equip', {
			resid: resid,
			type: type,
			index: this.curInfo.EIndex,
			grade: grade,
			roleid: cc.ll.player.roleid
		});
	},

	setCurItem(equipid) {
		for (const item of this.listItems) {
			let itemInfo = item.getComponent('EquipListItem').iteminfo;
			if (itemInfo.EquipID == equipid) {
				if (this.curSelected != null) {
					this.curSelected.getComponent('EquipListItem').unSelected();
				}
				this.refineItemId = 0;
				this.curSelected = item;
				this.curSelected.getComponent('EquipListItem').selected();
				this.curInfo = itemInfo;
				this.nextEquipInfo = null;

				this.showInfo();

				if (this.listContent.y + this.listContent.parent.height < -(item.y - 90)) {
					this.listContent.y = -this.listContent.parent.height - (item.y - 90)
				}
			}
		}
	},

	setCurPanel(pos) {
		for (let index = 1; index <= 4; index++) {
			if (pos == index) {
				cc.find(`ToggleGroup/toggle${index}`, this.node).getComponent(cc.Toggle).isChecked = true;
				this.panelToggleClicked(null, pos);
			} else {
				cc.find(`ToggleGroup/toggle${index}`, this.node).getComponent(cc.Toggle).isChecked = false;
			}
		}
	},

	showInfo() {
		this.curInfo = this.curSelected.getComponent('EquipListItem').iteminfo;
		if (!this.curInfo.BaseAttr) {
			return;
		}

		for (let index = 0; index < 4; index++) {
			if (cc.find(`ToggleGroup/toggle${index + 1}`, this.node).getComponent(cc.Toggle).isChecked) {
				this.panelToggleClicked(null, index + 1);
			}
		}

		this.showPreProperty();
	},

	showUpgradeInfo() {
		this.upgradeNode.getChildByName('EquipItem1').getComponent('EquipItem').loadInfo(this.curInfo);
		if (this.nextEquipInfo != null) {
			this.upgradeNode.getChildByName('EquipItem2').getComponent('EquipItem').loadInfo(this.nextEquipInfo);
		} else {
			this.getNextEquipInfo();
		}
		this.upgradeNode.getChildByName('BagItem').getComponent('BagItem').loadInfo({ itemid: 10405, count: 1 });
		this.upgradeNode.getChildByName('needlab').getComponent(cc.Label).string = `${cc.ll.player.itemList[10405] == null ? 0 : cc.ll.player.itemList[10405]}/1`;
	},

	showRefineInfo() {
		this.refineNode.getChildByName('EquipItem').getComponent('EquipItem').loadInfo(this.curInfo);
		this.refineNode.getChildByName('tips').getComponent(cc.Label).string = StrCommon.ChooseRefineItem;
	},

	showInlayInfo() {
		this.inlayNode.getChildByName('EquipItem').getComponent('EquipItem').loadInfo(this.curInfo);
		let gemLevel = Math.floor(this.curInfo.GemCnt / 3) + 1;
		this.inlayNode.getChildByName('BagItem').getChildByName('levellab').getComponent(cc.Label).string = `Lv.${gemLevel}`;
		let curitemid = this.gemarr[this.curInfo.EIndex - 1][gemLevel - 1];
		let curitemcnt = 0;
		if (cc.ll.player.itemList[curitemid]) curitemcnt = cc.ll.player.itemList[curitemid];
		this.inlayNode.getChildByName('BagItem').getComponent('BagItem').loadInfo({ itemid: curitemid, count: curitemcnt });
		// if (curitemcnt <= 0) {
		// 	this.inlayNode.getChildByName('BagItem').color = cc.Color.GRAY;
		// }
		this.inlayNode.getChildByName('gemlist').destroyAllChildren();
		let gemX = -(this.curInfo.MaxEmbedGemCnt - 1) * 10;
		for (let index = 0; index < this.curInfo.MaxEmbedGemCnt; index++) {
			let gem = cc.instantiate(this.inlayNode.getChildByName('gem'));
			gem.active = true;
			gem.x = gemX + index * 20;
			gem.y = 0;
			gem.parent = this.inlayNode.getChildByName('gemlist');
			if (index < this.curInfo.GemCnt) {
				gem.getChildByName(`gem${this.curInfo.EIndex}`).active = true;
			}
		}
	},

	showRecastInfo() {
		this.recastNode.getChildByName('EquipItem1').getComponent('EquipItem').loadInfo(this.curInfo);
		if (this.nextEquipInfo != null) {
			this.nextEquipInfo.BaseAttr = {};
			this.recastNode.getChildByName('EquipItem2').getComponent('EquipItem').loadInfo(this.nextEquipInfo);
		} else {
			this.getRecastInfo();
		}
		// this.recastNode.getChildByName('EquipItem2').getComponent('EquipItem').loadInfo(this.curInfo);
		if (this.curInfo.EquipType == 3) {
			this.recastNode.getChildByName('itemname').getComponent(cc.Label).string = '悔梦石';
			this.recastNode.getChildByName('BagItem').getComponent('BagItem').loadInfo({ itemid: 10401, count: 1 });
			this.recastNode.getChildByName('needlab').getComponent(cc.Label).string = `${cc.ll.player.itemList[10401] == null ? 0 : cc.ll.player.itemList[10401]}/1`;
		}
		else {
			this.recastNode.getChildByName('itemname').getComponent(cc.Label).string = '盘古精铁';
			this.recastNode.getChildByName('BagItem').getComponent('BagItem').loadInfo({ itemid: 10405, count: 1 });
			this.recastNode.getChildByName('needlab').getComponent(cc.Label).string = `${cc.ll.player.itemList[10405] == null ? 0 : cc.ll.player.itemList[10405]}/1`;
		}

	},

	showNewProperty(info) {
		if (this.refinePreNode.active) {
			this.showPropertyInfo(info, this.refinePreNode.getChildByName('new'));
		}
		else if (this.recastPreNode.active) {
			this.showPropertyInfo(info, this.recastPreNode.getChildByName('new'));
		}
	},

	showPreProperty() {
		if (this.refinePreNode.active) {
			this.showPropertyInfo(this.curInfo.LianhuaAttr, this.refinePreNode.getChildByName('pre'));
		}
		else if (this.recastPreNode.active) {
			this.showPropertyInfo(this.curInfo.BaseAttr, this.recastPreNode.getChildByName('pre'));
		}
	},

	showPropertyInfo(info, node) {
		if (!info || info.length == 0) {
			info = '{}';
		}
		node.destroyAllChildren();
		this.infoPos = cc.v2(0, 120);
		let propertyInfo = JSON.parse(info);
		if (Array.isArray(propertyInfo)) {
			for (const info of propertyInfo) {
				this.addPropertyInfo(info, node);
			}
		} else {
			this.addPropertyInfo(propertyInfo, node);
		}
	},

	addPropertyInfo(propertyInfo, node) {
		for (const key in propertyInfo) {
			let valuestr = propertyInfo[key];
			if (GameDefine.EquipTypeNumerical.indexOf(Number(key)) == -1) {
				valuestr = (valuestr > 0 ? '+' : '') + (valuestr / 10).toFixed(1) + '%';
			}
			else {
				valuestr = (valuestr > 0 ? '+' : '') + valuestr;
			}
			this.addInfoLab(GameDefine.AttrTypeL1Str[key], valuestr, node);
		}
	},

	addInfoLab(name, value, node) {
		let detail = cc.instantiate(this.node.getChildByName('item'));
		detail.active = true;
		detail.parent = node;
		detail.setPosition(this.infoPos);
		detail.getChildByName('name').getComponent(cc.Label).string = name;
		detail.getChildByName('value').getComponent(cc.Label).string = value;
		this.infoPos.y -= 60;
	},


	equipItemClicked(e, d) {
		if (this.curSelected == e.target || !e.target.getComponent('EquipListItem').iteminfo.EName) {
			return;
		}
		this.refineItemId = 0;
		if (this.curSelected != null) {
			this.curSelected.getComponent('EquipListItem').unSelected();
		}
		this.curSelected = e.target;
		this.curSelected.getComponent('EquipListItem').selected();
		this.curInfo = this.curSelected.getComponent('EquipListItem').iteminfo;
		this.nextEquipInfo = null;

		this.showInfo();
	},

	chooseItemClicked(e, d) {
		this.refineNode.getChildByName('itemlist').active = true;
		cc.find('itemlist/item1/BagItem', this.refineNode).getComponent('BagItem').loadInfo({ itemid: 10402, count: 0 });
		cc.find('itemlist/item1/countlab', this.refineNode).getComponent(cc.Label).string = `拥有${cc.ll.player.itemList[10402] == null ? 0 : cc.ll.player.itemList[10402]}个`;
		cc.find('itemlist/item2/BagItem', this.refineNode).getComponent('BagItem').loadInfo({ itemid: 10403, count: 0 });
		cc.find('itemlist/item2/countlab', this.refineNode).getComponent(cc.Label).string = `拥有${cc.ll.player.itemList[10403] == null ? 0 : cc.ll.player.itemList[10403]}个`;
		cc.find('itemlist/item3/BagItem', this.refineNode).getComponent('BagItem').loadInfo({ itemid: 10404, count: 0 });
		cc.find('itemlist/item3/countlab', this.refineNode).getComponent(cc.Label).string = `拥有${cc.ll.player.itemList[10404] == null ? 0 : cc.ll.player.itemList[10404]}个`;
	},

	refineItemClicked(e, d) {
		if (cc.ll.player.itemList[d] > 0) {
			this.refineItemId = d;
			this.refineNode.getChildByName('BagItem').active = true;
			this.refineNode.getChildByName('BagItem').getComponent('BagItem').loadInfo({ itemid: d, count: 0 });

			this.refineNode.getChildByName('BagBtn').active = false;
			this.refineNode.getChildByName('tips').getComponent(cc.Label).string = PubFun.GetItemName(this.refineItemId);
		}
		this.refineNode.getChildByName('itemlist').active = false;
	},

	panelToggleClicked(e, d) {
		if (this.curInfo == null) {
			return;
		}
		this.refineItemId = 0;
		for (const lab of this.titleNodes) {
			lab.active = false;
		}
		this.titleNodes[d - 1].active = true;

		this.upgradeNode.active = false;
		this.refineNode.active = false;
		this.inlayNode.active = false;
		this.recastNode.active = false;
		this.tipsNode.active = false;
		if (d == 1) {
			if (this.curInfo.EquipType == 1 && this.curInfo.Grade == 120) {
				this.tipsNode.active = true;
				this.tipsNode.getChildByName('tips').active = true;
				this.tipsNode.getChildByName('shenbing').active = false;
				this.tipsNode.getChildByName('xianqi').active = false;
				this.tipsNode.getChildByName('tips').getComponent(cc.Label).string = '该装备不能升级哦';
			}
			else if (this.curInfo.EquipType == 2) {
				this.tipsNode.active = true;
				this.tipsNode.getChildByName('tips').active = false;
				this.tipsNode.getChildByName('shenbing').active = true;
				this.tipsNode.getChildByName('xianqi').active = false;
			}
			else if (this.curInfo.EquipType == 3) {
				this.tipsNode.active = true;
				this.tipsNode.getChildByName('tips').active = false;
				this.tipsNode.getChildByName('shenbing').active = false;
				this.tipsNode.getChildByName('xianqi').active = true;
			}
			else {
				this.upgradeNode.active = true;
				this.showUpgradeInfo();
			}
		}
		else if (d == 2) {
			if (this.curInfo.EquipType == 0) {
				this.tipsNode.active = true;
				this.tipsNode.getChildByName('tips').active = true;
				this.tipsNode.getChildByName('shenbing').active = false;
				this.tipsNode.getChildByName('xianqi').active = false;
				this.tipsNode.getChildByName('tips').getComponent(cc.Label).string = '该装备不能炼化哦';
			}
			else {
				this.refineNode.active = true;
				this.showRefineInfo();
				this.refineNode.getChildByName('BagItem').active = false;
				this.refineNode.getChildByName('BagBtn').active = true;
				this.refineNode.getChildByName('tips').getComponent(cc.Label).string = StrCommon.ChooseRefineItem;
			}
		}
		else if (d == 3) {
			this.inlayNode.active = true;
			this.showInlayInfo();
		}
		else if (d == 4) {
			if (this.curInfo.EquipType == 0 || this.curInfo.EquipType == 2) {
				this.tipsNode.active = true;
				this.tipsNode.getChildByName('tips').active = true;
				this.tipsNode.getChildByName('shenbing').active = false;
				this.tipsNode.getChildByName('xianqi').active = false;
				this.tipsNode.getChildByName('tips').getComponent(cc.Label).string = '该装备不能重铸哦';
			}
			else {
				this.recastNode.active = true;
				this.showRecastInfo();
			}
		}
	},

	inlayBtnClicked(e, d) {
		if (d == 1 && this.GemCnt >= this.MaxEmbedGemCnt) {
			return;
		}
		cc.ll.net.send('c2s_equip_inlay', {
			operation: d,
			roleid: cc.ll.player.roleid,
			equipid: this.curInfo.EquipID
		})
	},

	upgradeBtnClicked(e, d) {
		cc.ll.net.send('c2s_equip_upgrade', {
			roleid: cc.ll.player.roleid,
			equipid: this.curInfo.EquipID
		})
		this.nextEquipInfo = null;
	},

	refinePreBtnClicked(e, d) {
		if (this.refineItemId == 0) {
			cc.ll.msgbox.addMsg(StrCommon.ChooseRefineItem);
			return;
		}
		this.refinePreNode.active = true;
		this.showPropertyInfo(this.curInfo.LianhuaAttr, this.refinePreNode.getChildByName('pre'));
		this.showNewProperty('');
		this.refinePreNode.getChildByName('BagItem').getComponent('BagItem').loadInfo({ itemid: this.refineItemId, count: 0 });
		this.refinePreNode.getChildByName('needlab').getComponent(cc.Label).string = `${cc.ll.player.itemList[this.refineItemId] == null ? 0 : cc.ll.player.itemList[this.refineItemId]}/1`;
	},

	recastPreBtnClicked(e, d) {
		this.recastPreNode.active = true;
		this.showPropertyInfo(this.curInfo.BaseAttr, this.recastPreNode.getChildByName('pre'));
		this.showNewProperty('');
		if (this.curInfo.EquipType == 3) {
			this.recastPreNode.getChildByName('BagItem').getComponent('BagItem').loadInfo({ itemid: 10401, count: 1 });
			this.recastPreNode.getChildByName('needlab').getComponent(cc.Label).string = `${cc.ll.player.itemList[10401] == null ? 0 : cc.ll.player.itemList[10401]}/1`;
		}
		else {
			this.recastPreNode.getChildByName('BagItem').getComponent('BagItem').loadInfo({ itemid: 10405, count: 1 });
			this.recastPreNode.getChildByName('needlab').getComponent(cc.Label).string = `${cc.ll.player.itemList[10405] == null ? 0 : cc.ll.player.itemList[10405]}/1`;
		}
	},

	refineBtnClicked(e, d) {
		this.refineData = null;
		let level = 0;
		if (this.refineItemId == 10403) level = 1;
		if (this.refineItemId == 10404) level = 2;
		cc.ll.player.itemList[this.refineItemId]--;
		this.refinePreBtnClicked();

		cc.ll.net.send('c2s_equip_refine', {
			operation: 0,
			roleid: cc.ll.player.roleid,
			level: level,
			equipid: this.curInfo.EquipID
		})
	},

	recastBtnClicked(e, d) {
		let itemid = 10401;
		if (this.curInfo.EquipType == 3) {
			itemid = 10401;
		}else{
			itemid = 10405;
		}
		let itemnum = cc.ll.player.itemList[itemid];
		if(itemnum == null || itemnum <= 0){
			cc.ll.msgbox.addMsg('材料不足');
			return;
		}
		this.recastData = null;
		cc.ll.net.send('c2s_equip_recast', {
			operation: 0,
			roleid: cc.ll.player.roleid,
			equipid: this.curInfo.EquipID
		});
		cc.ll.player.itemList[itemid]--;
		this.recastPreBtnClicked();
	},
	refineChangeBtnClicked(e, d) {
		this.refineData = null;
		cc.ll.net.send('c2s_equip_refine', {
			operation: 1,
			roleid: cc.ll.player.roleid,
			level: 0,
			equipid: this.curInfo.EquipID
		})
	},

	recastChangeBtnClicked(e, d) {
		this.recastData = null;
		cc.ll.net.send('c2s_equip_recast', {
			operation: 1,
			roleid: cc.ll.player.roleid,
			equipid: this.curInfo.EquipID
		})
	},

	closeRefine() {
		this.refinePreNode.active = false;
	},

	closeRecast() {
		this.recastPreNode.active = false;
	},

	touchBegan(event) {
		this.refineNode.getChildByName('itemlist').active = false;
	},

	onCloseBtnClicked(e, d) {
		this.node.destroy();
	},

	onShenBingUpClicked(e, d) {
		let shenbingup = cc.instantiate(this.shenbingUpPre);
		shenbingup.parent = cc.find('Canvas');
		this.node.destroy();
	},

	onXianQiUpClicked(e, d) {
		let xianqiup = cc.instantiate(this.xianqiUpPre);
		xianqiup.parent = cc.find('Canvas');
		this.node.destroy();
	},

});
