let SkillConfig = require('../etc/skill_config');

cc.Class({
	extends: cc.Component,

	properties: {
		typeNodes: [cc.Sprite],
		skillNodes: [cc.Node],
		nameLab: cc.Label,
		expLab: cc.Label,
		blueLab: cc.Label,
		faLab: cc.Label,
		infoLab: cc.Label,
		nextLab: cc.Label,
		consumeLab: cc.Label,

		typeTxtAtlas: cc.SpriteAtlas
	},

	onLoad() {
		this.curItemLogic = 0;
		this.loadGameData();
	},

	loadGameData() {
		let skillList = cc.ll.player.gameData.skill ? JSON.parse(cc.ll.player.gameData.skill) : {};
		let roleindex = (cc.ll.player.race - 1) * 2 + cc.ll.player.sex - 1;
		let curids = SkillConfig.roleSkillIds[roleindex];
		let curtypes = SkillConfig.roleSkillTypes[roleindex];
		for (let index = 0; index < curtypes.length; index++) {
			this.typeNodes[index].spriteFrame = this.typeTxtAtlas.getSpriteFrame(`skill_type_${curtypes[index]}`);
		}

		for (let index = 0; index < curids.length; index++) {
			this.skillNodes[index].getComponent('RoleSkillItem').loadInfo(curids[index], skillList[curids[index]]);

			let btn = this.skillNodes[index].getComponent(cc.Button);
			var clickEventHandler = new cc.Component.EventHandler();
			clickEventHandler.target = this.node;
			clickEventHandler.component = "RoleSkillPanel";
			clickEventHandler.handler = "skillItemClicked";
			btn.clickEvents.push(clickEventHandler);
		}
		if (!this.curItemLogic) {
			this.curItemLogic = this.skillNodes[0].getComponent('RoleSkillItem');
			this.curItemLogic.selected();
		}
		this.showSkillInfo();
	},

	showSkillInfo() {
		let curSkillLogic = this.curItemLogic.skillLogic;
		this.expLab.string = '【熟练度】' + curSkillLogic.nCurExp + '/' + curSkillLogic.nMaxExp;
		this.blueLab.string = '【耗法值】' + curSkillLogic.GetCostMP();
		this.faLab.string = '【法术系】' + curSkillLogic.getFaxiName();
		this.nameLab.string = curSkillLogic.getName();
		this.infoLab.string = curSkillLogic.GetDetail();
		this.nextLab.string = curSkillLogic.GetNextLevelDetail();
		this.consumeLab.string = curSkillLogic.GetExpPrice();
	},

	skillItemClicked(e, d) {
		this.curItemLogic.unSelected();
		this.curItemLogic = e.target.getComponent('RoleSkillItem');
		this.curItemLogic.selected();
		this.showSkillInfo();
	},

	onUpgradeBtnClicked(e, d) {
		this.curItemLogic.upgradeExp();
		this.showSkillInfo();
		cc.ll.net.send('c2s_player_upskill', {
			roleid: cc.ll.player.roleid,
			skillid: this.curItemLogic.skillid
		});
	}
});
