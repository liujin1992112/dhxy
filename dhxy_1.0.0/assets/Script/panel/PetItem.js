let GameRes = require('../game/GameRes');
let CPubFunction = require('../game/PubFunction');

cc.Class({
	extends: cc.Component,

	properties: {
		itemIcon: cc.Sprite,
		nameLab: cc.Label,
		petLevel: cc.Label,
		onbattleIcon: cc.Node,
		selectedNode: cc.Node,
	},

	onLoad() {
		this.selectedNode.active = false;
		this.itemInfo = null;
	},

	loadInfo(info, onbattle) {
		this.itemInfo = info;
		this.itemIcon.spriteFrame = GameRes.getPetHead(info.resid);
		this.nameLab.string = info.name;
		this.petLevel.node.color = CPubFunction.getReliveColor(info.relive, 1);
		this.petLevel.string = info.relive + '转' + info.level + '级';
		this.onbattleIcon.active = onbattle;
	},

	selected(){
		this.selectedNode.active = true;
	},

	unSelected(){
		this.selectedNode.active = false;
	}
});
