let CPubFunction = require('../game/PubFunction');

cc.Class({
    extends: cc.Component,
    properties: {
        idEditBox:      cc.EditBox,
        mailEditBox:    cc.EditBox,
        background:     cc.Node,
        listItem:       cc.Prefab,
        content:        cc.Node,
        spreadButton:   cc.Node,
        unspreadButton: cc.Node,
        nearButton:     cc.Node,
        friendsButton:  cc.Node,
        enemyButton:    cc.Node,
        scrollview:     cc.Node,
        xianyuIcon:     cc.Node,
        yinliangIcon:   cc.Node,
        searchButton:   cc.Node,
        unsearchButton: cc.Node,
        costLabel:      cc.Label,
    },

    onLoad() {
        this.setButtonChoose(this.spreadButton, false);
        this.type = 1;
    },

    start () {
        this.onSpreadClick('unspread');
    },

    onButtonClick (event, param) {
        if (['near', 'friends', 'enemy'].indexOf(param) != -1) { // 附近，好友，黑名单 
            this.onShowListClick(param);
        }
        else if (param == 'battle') { // pk 
            this.onBattleClick();
        }
        else if (param == 'search') {
            this.onSearchClick();
        }
        else if (param == 'unsearch') {
            this.onUnSearchClick();
        }
        else if (param == 'spread' || param == 'unspread') { // 战书 
            this.onSpreadClick(param);
        }
        else if (param == 'close') {
            this.node.destroy();
        }
        else if (param == 'panel') {
            this.scrollview.active = false;
            this.onShowListClick(null);
        }
    },

    onSpreadClick (param) {
        let isSpread = (param == 'spread');
        this.type = (isSpread)? 1:0;
        this.costLabel.string = (isSpread)? '2000':'1500000';
        this.setButtonChoose(this.spreadButton, !isSpread);
        this.setButtonChoose(this.unspreadButton, isSpread);
        this.xianyuIcon.active = isSpread;
        this.yinliangIcon.active = !isSpread;
    },

    onShowListClick (param) {
        // this.setButtonChoose(this.nearButton, param != 'near');
        // this.setButtonChoose(this.friendsButton, param != 'friends');
        // this.setButtonChoose(this.enemyButton, param != 'enemy');
        if (this.scrollview.active) {
            this.scrollview.active = false;
            return;
        }
        if (param == 'friends') {
            cc.ll.net.send('c2s_get_friends', {});
        }
    },

    onSearchClick () {
        let str = this.idEditBox.string;
        if (str && str.length > 0) {
            let id = str.split('[')[0];
            if (this.checkId(id)) {
                cc.ll.net.send('c2s_search_friends', {
                    type: 1,
                    roleid: cc.ll.player.roleid,
                    data: id,
                });
                return;
            }
        }
    },

    showSearch (info) {
        if (info.length == 0) {
            cc.ll.msgbox.addMsg('未搜索到玩家！');
        }
        else {
            this.searchButton.active = false;
            this.unsearchButton.active = true;
            // this.nearButton.active = false;
            this.friendsButton.active = false;
            // this.enemyButton.active = false;
            if (this.searchNode) {
                this.searchNode.destroy();
                this.searchNode = null;
            }
            let node = cc.instantiate(this.listItem);
            node.parent = this.background;
            node.x = this.friendsButton.x;
            node.y = this.friendsButton.y+30;
            info[0].online = 1;
            node.getComponent('PKListItem').loadInfo(info[0]);
            this.searchNode = node;
        }
    },

    onUnSearchClick () {
        if (this.searchNode) {
            this.searchNode.destroy();
            this.searchNode = null;
        }
        this.searchButton.active = true;
        this.unsearchButton.active = false;
        // this.nearButton.active = true;
        this.friendsButton.active = true;
        // this.enemyButton.active = true;
    },

    showScrollList (info) {
        this.content.destroyAllChildren();
        let num = 0;
        for (let data of info) {
            if (data.online == 1) {
                ++ num;
                let item = cc.instantiate(this.listItem);
                item.parent = this.content;
                let logic = item.getComponent('PKListItem');
                logic.loadInfo(data);
                logic.setSearchId = this.setSearchId.bind(this);
            }
        }
        this.scrollview.active = true;
        if (num == 0) {
            this.scrollview.active = false;
            cc.ll.msgbox.addMsg('你没有好友在线！');
        }
    },

    setButtonChoose (node, choose) {
        node.getComponent(cc.Button).interactable = choose;
    },

    setSearchId (id, name) {
        this.idEditBox.string = `${id}[${name}]`;
        this.scrollview.active = false;
    },

    checkId (str) {
        return (!isNaN(Number(str))) && (Number(str) == parseInt(str));
    },

    onBattleClick () {
        let recipientid = this.idEditBox.string.split('[')[0];
        if (this.checkId(recipientid)) {
            if (parseInt(recipientid) == cc.ll.player.roleid) {
                cc.ll.msgbox.addMsg('不能跟自己决斗！');
                return;
            }
            cc.ll.net.send('c2s_palace_fight', {
                sponsorid: cc.ll.player.roleid,
                recipientid: parseInt(recipientid),
                type: this.type,
                msg: this.mailEditBox.string,
            });
        }
        else {
            cc.ll.msgbox.addMsg('搜索格式不正确！');
        }
    },
});
