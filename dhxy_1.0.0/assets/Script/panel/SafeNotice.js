cc.Class({
	extends: cc.Component,
	properties: {
		edit: cc.EditBox,
	},

	onLoad () {
	},

	start () {
	},

	/*
	 * 设置成功回调
	 */
	setCallback (fun) {
		this.fun_ok = fun;
	},

	/*
	 * 输入验证
	 */
	onEditEnd () {
	},

	/*
	 * 确定
	 */
	onConfirmClick () {
		if (this.edit.string == cc.ll.player.safe_password) {
			if (this.fun_ok) {
				this.fun_ok();
			}
			this.node.destroy();
		}
		else {
			cc.ll.msgbox.addMsg('输入密码错误！');
			this.edit.string = '';
		}
	},

	/*
	 * 取消
	 */
	onCancelClick () {
		this.node.destroy();
	},
});
