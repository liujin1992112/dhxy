let logic = require('../game/PetLogic');
let CPubFunction = require('../game/PubFunction');
let GameDefine = require('../game/GameDefine');
let AttrTypeL1 = GameDefine.AttrTypeL1;
let GameRes = require('../game/GameRes');
let skillMgr = require('../game/Skill');

cc.Class({
	extends: cc.Component,

	properties: {
		titleNodes: [cc.Node],
		petContent: cc.Node,
		resistanceContent: cc.Node,
		resistanceContent1: cc.Node,
		resistanceLevelNode: cc.Node,
		resistanceSureNode: cc.Node,

		propertyNode: cc.Node,
		propertyPanel: cc.Node,
		addPointPanel: cc.Node,
		petPropertyPanel: cc.Node,
		/* 天资面板 */
		resistanceNode: cc.Node,
		skillNode: cc.Node,
		tujianNode: cc.Node,
		washPropertyPanel: cc.Node,
		/* 洗练属性 */

		petNode: cc.Node,
		petItem: cc.Prefab,
		useItemUI: cc.Prefab,
		commonUIAtlas: cc.SpriteAtlas,
		SkillAtlas: cc.SpriteAtlas,
		PetShenSkillUI: cc.Prefab,

		SafeNotice:cc.Prefab,
		SkillDetail: cc.Prefab,
	},

	onLoad() {
		this.petLogic = new logic.PetLogic();
		this.currentSelected = -1;
		this.itemNodes = [];
		this.resistanceContent.parent.parent.active = false;
		this.resistanceContent1.parent.parent.active = false;
		this.resistancePanel = this.resistanceContent;
		this.resistancePanel.parent.parent.active = true;
		this.kangPointAdd = 0;
		this.loadPetList();

		this.currentBtn = null;
		this.timecnt = 0;
		this.maxtiemcnt = 30;
	},

	initBtn(btn, data, type, iskang) {
		btn.datainfo = data;
		btn.opertype = type;
		btn.iskang = iskang;
		btn.on(cc.Node.EventType.TOUCH_START, this.propertyBtnClick.bind(this));
		btn.on(cc.Node.EventType.TOUCH_CANCEL, this.propertyBtnClick.bind(this));
		btn.on(cc.Node.EventType.TOUCH_END, this.propertyBtnClick.bind(this));
	},

	propertyBtnClick(e) {
		if (e.type == cc.Node.EventType.TOUCH_START) {
			this.maxtiemcnt = 30;
			this.timecnt = 0;
			this.currentBtn = e.target;
		} else if (e.type == cc.Node.EventType.TOUCH_END || e.type == cc.Node.EventType.TOUCH_CANCEL) {
			this.timecnt = this.maxtiemcnt;
			this.update();
			this.currentBtn = null;
		}
	},

	update() {
		if (this.currentBtn == null) {
			return;
		}
		this.timecnt++;
		if (this.timecnt >= this.maxtiemcnt) {
			if (this.maxtiemcnt > 4) {
				this.maxtiemcnt -= 2;
			}
			this.timecnt = 0;
			if (this.currentBtn.iskang) {
				if (this.currentBtn.opertype == 0) {
					this.resistanceAddPoint(this.currentBtn.datainfo);
				} else if (this.currentBtn.opertype == 1) {
					this.resistanceSubPoint(this.currentBtn.datainfo);
				}
			} else {
				if (this.currentBtn.opertype == 0) {
					this.propertyAddPoint(this.currentBtn.datainfo);
				} else if (this.currentBtn.opertype == 1) {
					this.propertySubPoint(this.currentBtn.datainfo);
				}
			}
		}
	},

	initKangBtn() {
		cc.find('xiupoint', this.resistanceNode).getComponent(cc.Label).string = this.petLogic.xiulianleft - this.kangPointAdd + '/' + this.petLogic.xiulianfree;

		this.setKangBtnInfo('fengyinitem', AttrTypeL1.DFENGYIN);
		this.setKangBtnInfo('hunluanitem', AttrTypeL1.DHUNLUAN);
		this.setKangBtnInfo('hunshuiitem', AttrTypeL1.DHUNSHUI);
		this.setKangBtnInfo('yiwangitem', AttrTypeL1.DYIWANG);

		this.setKangBtnInfo('fengitem', AttrTypeL1.DFENG);
		this.setKangBtnInfo('leiitem', AttrTypeL1.DLEI);
		this.setKangBtnInfo('shuiitem', AttrTypeL1.DSHUI);
		this.setKangBtnInfo('huoitem', AttrTypeL1.DHUO);
		this.setKangBtnInfo('guihuoitem', AttrTypeL1.DGUIHUO);
		this.setKangBtnInfo('sanshiitem', AttrTypeL1.DSANSHI);
		this.setKangBtnInfo('duitem', AttrTypeL1.DDU);

		this.setKangBtnInfo('xishouitem', AttrTypeL1.PXISHOU);
		this.setKangBtnInfo('mingzhongitem', AttrTypeL1.PMINGZHONG);
		this.setKangBtnInfo('shanduoitem', AttrTypeL1.PSHANBI);
		this.setKangBtnInfo('lianjiitem', AttrTypeL1.PLIANJI);
		this.setKangBtnInfo('lianjilvitem', AttrTypeL1.PLIANJILV);
		this.setKangBtnInfo('kuangbaoitem', AttrTypeL1.PKUANGBAO);
		this.setKangBtnInfo('pofangitem', AttrTypeL1.PPOFANG);
		this.setKangBtnInfo('pofanglvitem', AttrTypeL1.PPOFANGLV);
		this.setKangBtnInfo('fanzhenlvitem', AttrTypeL1.PFANZHENLV);
		this.setKangBtnInfo('fanzhenitem', AttrTypeL1.PFANZHEN);
	},

	setKangBtnInfo(name, type) {
		this.initBtn(cc.find(`${name}/point/addBtn`, this.resistancePanel), type, 0, true);
		this.initBtn(cc.find(`${name}/point/subBtn`, this.resistancePanel), type, 1, true);
		// cc.find(`${name}/point/addBtn`, this.resistancePanel).getComponent(cc.Button).clickEvents[0].customEventData = type;
		// cc.find(`${name}/point/subBtn`, this.resistancePanel).getComponent(cc.Button).clickEvents[0].customEventData = type;
		if (this.petLogic.xiulianleft - this.kangPointAdd <= 0 || this.petLogic.dpoint[type] >= this.petLogic.getMaxAddPoint(type)) {
			cc.find(`${name}/point/addBtn`, this.resistancePanel).active = false;
		} else {
			cc.find(`${name}/point/addBtn`, this.resistancePanel).active = true;
		}
		if (this.petLogic.dpointT[type] > 0) {
			cc.find(`${name}/point/subBtn`, this.resistancePanel).active = true;
		} else {
			cc.find(`${name}/point/subBtn`, this.resistancePanel).active = false;
		}
	},

	loadPetList() {
		this.propertyNode.active = false;
		this.resistanceNode.active = false;
		this.skillNode.active = false;
		this.tujianNode.active = false;

		let petInfo = cc.ll.player.petInfo;
		this.petContent.destroyAllChildren();
		this.itemNodes = [];
		if (cc.ll.player.petInfo == null) {
			return;
		}

		let select_index = 0;
		this.petLogic.setInfo(null);

		this.petContent.height = petInfo.list.length * 100;
		if (this.petContent.height < this.petContent.parent.height) {
			this.petContent.height = this.petContent.parent.height;
		}
		for (let index = 0; index < petInfo.list.length; index++) {
			let pinfo = petInfo.list[index];
			let item = cc.instantiate(this.petItem);
			item.x = 0;
			item.y = -index * 100;
			item.parent = this.petContent;

			let btn = item.getComponent(cc.Button);
			var clickEventHandler = new cc.Component.EventHandler();
			clickEventHandler.target = this.node;
			clickEventHandler.component = "PetPanel";
			clickEventHandler.handler = "petItemClicked";
			clickEventHandler.customEventData = pinfo.petid;
			btn.clickEvents.push(clickEventHandler);

			this.itemNodes.push(item);
			item.getComponent('PetItem').loadInfo(pinfo, pinfo.petid == petInfo.curid);
			if (pinfo.petid == petInfo.curid) {
				select_index = index;
			}
		}

		let item = this.itemNodes[select_index];
		item.getComponent('PetItem').selected();
		this.petLogic.setInfo(petInfo.list[select_index]);
		this.showInfo();
		this.currentSelected = petInfo.list[select_index].petid;
	},

	reloadInfo(info) {
		for (const item of this.itemNodes) {
			if (item.getComponent('PetItem').itemInfo.petid == info.petid) {
				item.getComponent('PetItem').loadInfo(info, info.petid == cc.ll.player.petInfo.curid);
			}
		}
		if (this.currentSelected == info.petid) {
			this.petLogic.setInfo(info);
			this.showInfo();
		}
		if (this.useui && this.useui.parent != null) {
			this.useui.getComponent('UseItemUI').refreshInfo();
		}
		if (this.useui1 && this.useui1.parent != null) {
			this.useui1.getComponent('UseItemUI').refreshInfo();
		}
		if (this.useui2 && this.useui2.parent != null) {
			this.useui2.getComponent('UseItemUI').refreshInfo();
		}
	},

	showPropertyInfo() {
		this.propertyPanel.getChildByName('jyProgress').getComponent(cc.ProgressBar).progress = this.petLogic.exp / this.petLogic.maxexp;
		this.propertyPanel.getChildByName('jyProgress').getChildByName('jingyan').getComponent(cc.Label).string = this.petLogic.exp + '/' + this.petLogic.maxexp;
		this.propertyPanel.getChildByName('QixueLabel').getComponent(cc.Label).string = this.petLogic.cur_hp;
		this.propertyPanel.getChildByName('FaliLabel').getComponent(cc.Label).string = this.petLogic.cur_mp;
		this.propertyPanel.getChildByName('GongjiLabel').getComponent(cc.Label).string = this.petLogic.cur_atk;
		this.propertyPanel.getChildByName('SuduLabel').getComponent(cc.Label).string = this.petLogic.cur_spd;
		this.propertyPanel.getChildByName('CloseLabel').getComponent(cc.Label).string = this.petLogic.qinmi;

		this.addPointPanel.getChildByName('qianNeng').getChildByName('qianneng').getComponent(cc.Label).string = this.petLogic.qianneng;

		this.addPointPanel.getChildByName('QixueNode').getChildByName('Value1Label').getComponent(cc.Label).string = this.petLogic.cur_hp;
		this.addPointPanel.getChildByName('FaliNode').getChildByName('Value1Label').getComponent(cc.Label).string = this.petLogic.cur_mp;
		this.addPointPanel.getChildByName('GongjiNode').getChildByName('Value1Label').getComponent(cc.Label).string = this.petLogic.cur_atk;
		this.addPointPanel.getChildByName('SuduNode').getChildByName('Value1Label').getComponent(cc.Label).string = this.petLogic.cur_spd;
		this.addPointPanel.getChildByName('QixueNode').getChildByName('point').getChildByName('Value2Label').getComponent(cc.Label).string = this.petLogic.ppoint[AttrTypeL1.GENGU];
		this.addPointPanel.getChildByName('FaliNode').getChildByName('point').getChildByName('Value2Label').getComponent(cc.Label).string = this.petLogic.ppoint[AttrTypeL1.LINGXING];
		this.addPointPanel.getChildByName('GongjiNode').getChildByName('point').getChildByName('Value2Label').getComponent(cc.Label).string = this.petLogic.ppoint[AttrTypeL1.LILIANG];
		this.addPointPanel.getChildByName('SuduNode').getChildByName('point').getChildByName('Value2Label').getComponent(cc.Label).string = this.petLogic.ppoint[AttrTypeL1.MINJIE];
		this.initBtn(cc.find('SuduNode/point/addBtn', this.addPointPanel), 103, 0);
		this.initBtn(cc.find('SuduNode/point/subBtn', this.addPointPanel), 103, 1);
		this.initBtn(cc.find('GongjiNode/point/addBtn', this.addPointPanel), 102, 0);
		this.initBtn(cc.find('GongjiNode/point/subBtn', this.addPointPanel), 102, 1);
		this.initBtn(cc.find('FaliNode/point/addBtn', this.addPointPanel), 101, 0);
		this.initBtn(cc.find('FaliNode/point/subBtn', this.addPointPanel), 101, 1);
		this.initBtn(cc.find('QixueNode/point/addBtn', this.addPointPanel), 100, 0);
		this.initBtn(cc.find('QixueNode/point/subBtn', this.addPointPanel), 100, 1);

		this.initPropertBtn();
	},

	initPropertBtn() {
		this.setPropertBtnInfo('QixueNode', AttrTypeL1.GENGU);
		this.setPropertBtnInfo('FaliNode', AttrTypeL1.LINGXING);
		this.setPropertBtnInfo('GongjiNode', AttrTypeL1.LILIANG);
		this.setPropertBtnInfo('SuduNode', AttrTypeL1.MINJIE);
	},

	setPropertBtnInfo(name, type) {
		if (this.petLogic.qianneng <= 0) {
			cc.find(`${name}/point/addBtn`, this.addPointPanel).active = false;
		} else {
			cc.find(`${name}/point/addBtn`, this.addPointPanel).active = true;
		}
		if (this.petLogic.ppointT[type] > 0) {
			cc.find(`${name}/point/subBtn`, this.addPointPanel).active = true;
		} else {
			cc.find(`${name}/point/subBtn`, this.addPointPanel).active = false;
		}
	},

	showDInfo() {
		cc.find('fengyinitem/value', this.resistancePanel).getComponent(cc.Label).string = this.petLogic.dfengyin.toFixed(1) + '%';
		cc.find('fengyinitem/add', this.resistancePanel).getComponent(cc.Label).string = this.petLogic.dpoint[AttrTypeL1.DFENGYIN];
		cc.find('hunluanitem/value', this.resistancePanel).getComponent(cc.Label).string = this.petLogic.dhunlun.toFixed(1) + '%';
		cc.find('hunluanitem/add', this.resistancePanel).getComponent(cc.Label).string = this.petLogic.dpoint[AttrTypeL1.DHUNLUAN];
		cc.find('hunshuiitem/value', this.resistancePanel).getComponent(cc.Label).string = this.petLogic.dhunshui.toFixed(1) + '%';
		cc.find('hunshuiitem/add', this.resistancePanel).getComponent(cc.Label).string = this.petLogic.dpoint[AttrTypeL1.DHUNSHUI];
		cc.find('yiwangitem/value', this.resistancePanel).getComponent(cc.Label).string = this.petLogic.dyiwang.toFixed(1) + '%';
		cc.find('yiwangitem/add', this.resistancePanel).getComponent(cc.Label).string = this.petLogic.dpoint[AttrTypeL1.DYIWANG];

		cc.find('fengitem/value', this.resistancePanel).getComponent(cc.Label).string = this.petLogic.dfeng.toFixed(1) + '%';
		cc.find('fengitem/add', this.resistancePanel).getComponent(cc.Label).string = this.petLogic.dpoint[AttrTypeL1.DFENG];
		cc.find('leiitem/value', this.resistancePanel).getComponent(cc.Label).string = this.petLogic.dlei.toFixed(1) + '%';
		cc.find('leiitem/add', this.resistancePanel).getComponent(cc.Label).string = this.petLogic.dpoint[AttrTypeL1.DLEI];
		cc.find('shuiitem/value', this.resistancePanel).getComponent(cc.Label).string = this.petLogic.dshui.toFixed(1) + '%';
		cc.find('shuiitem/add', this.resistancePanel).getComponent(cc.Label).string = this.petLogic.dpoint[AttrTypeL1.DSHUI];
		cc.find('huoitem/value', this.resistancePanel).getComponent(cc.Label).string = this.petLogic.dhuo.toFixed(1) + '%';
		cc.find('huoitem/add', this.resistancePanel).getComponent(cc.Label).string = this.petLogic.dpoint[AttrTypeL1.DHUO];
		cc.find('guihuoitem/value', this.resistancePanel).getComponent(cc.Label).string = this.petLogic.dguihuo.toFixed(1) + '%';
		cc.find('guihuoitem/add', this.resistancePanel).getComponent(cc.Label).string = this.petLogic.dpoint[AttrTypeL1.DGUIHUO];
		cc.find('sanshiitem/value', this.resistancePanel).getComponent(cc.Label).string = this.petLogic.dsanshi.toFixed(1) + '%';
		cc.find('sanshiitem/add', this.resistancePanel).getComponent(cc.Label).string = this.petLogic.dpoint[AttrTypeL1.DSANSHI];
		cc.find('duitem/value', this.resistancePanel).getComponent(cc.Label).string = this.petLogic.ddu.toFixed(1) + '%';
		cc.find('duitem/add', this.resistancePanel).getComponent(cc.Label).string = this.petLogic.dpoint[AttrTypeL1.DDU];

		cc.find('xishouitem/value', this.resistancePanel).getComponent(cc.Label).string = this.petLogic.pxishou.toFixed(1) + '%';
		cc.find('xishouitem/add', this.resistancePanel).getComponent(cc.Label).string = this.petLogic.dpoint[AttrTypeL1.PXISHOU];
		cc.find('mingzhongitem/value', this.resistancePanel).getComponent(cc.Label).string = this.petLogic.pmingzhong.toFixed(1) + '%';
		cc.find('mingzhongitem/add', this.resistancePanel).getComponent(cc.Label).string = this.petLogic.dpoint[AttrTypeL1.PMINGZHONG];
		cc.find('shanduoitem/value', this.resistancePanel).getComponent(cc.Label).string = this.petLogic.pshanbi.toFixed(1) + '%';
		cc.find('shanduoitem/add', this.resistancePanel).getComponent(cc.Label).string = this.petLogic.dpoint[AttrTypeL1.PSHANBI];
		cc.find('lianjiitem/value', this.resistancePanel).getComponent(cc.Label).string = this.petLogic.plianji;
		cc.find('lianjiitem/add', this.resistancePanel).getComponent(cc.Label).string = this.petLogic.dpoint[AttrTypeL1.PLIANJI];
		cc.find('lianjilvitem/value', this.resistancePanel).getComponent(cc.Label).string = this.petLogic.plianjilv.toFixed(1) + '%';
		cc.find('lianjilvitem/add', this.resistancePanel).getComponent(cc.Label).string = this.petLogic.dpoint[AttrTypeL1.PLIANJILV];
		cc.find('kuangbaoitem/value', this.resistancePanel).getComponent(cc.Label).string = this.petLogic.pkuangbao.toFixed(1) + '%';
		cc.find('kuangbaoitem/add', this.resistancePanel).getComponent(cc.Label).string = this.petLogic.dpoint[AttrTypeL1.PKUANGBAO];
		cc.find('pofangitem/value', this.resistancePanel).getComponent(cc.Label).string = this.petLogic.ppofang.toFixed(1) + '%';
		cc.find('pofangitem/add', this.resistancePanel).getComponent(cc.Label).string = this.petLogic.dpoint[AttrTypeL1.PPOFANG];
		cc.find('pofanglvitem/value', this.resistancePanel).getComponent(cc.Label).string = this.petLogic.ppofanglv.toFixed(1) + '%';
		cc.find('pofanglvitem/add', this.resistancePanel).getComponent(cc.Label).string = this.petLogic.dpoint[AttrTypeL1.PPOFANGLV];
		cc.find('fanzhenlvitem/value', this.resistancePanel).getComponent(cc.Label).string = this.petLogic.pfanzhenlv.toFixed(1) + '%';
		cc.find('fanzhenlvitem/add', this.resistancePanel).getComponent(cc.Label).string = this.petLogic.dpoint[AttrTypeL1.PFANZHENLV];
		cc.find('fanzhenitem/value', this.resistancePanel).getComponent(cc.Label).string = this.petLogic.pfanzhen.toFixed(1) + '%';
		cc.find('fanzhenitem/add', this.resistancePanel).getComponent(cc.Label).string = this.petLogic.dpoint[AttrTypeL1.PFANZHEN];
	},

	showSkillInfo() {
		while (this.skillNode.getChildByName('skillbtn')) {
			let node = this.skillNode.getChildByName('skillbtn');
			node.parent = null;
			node.destroy();
		}
		let self = this;
		cc.loader.loadRes(`ui/photo/${this.petLogic.resid}`, cc.SpriteFrame, (e, frame) => {
			if (!e) {
				cc.find('photo/petNode/pet', self.skillNode).getComponent(cc.Sprite).spriteFrame = frame;
			}
		});
		let skillCnt = this.petLogic.maxskillcnt;
		let skillItem = cc.find('skillitem', this.skillNode);
		let deltaAngle = 360 / skillCnt;
		let firstPos = skillItem.getPosition();
		let centerPos = cc.find('photo', this.skillNode).getPosition();
		let radius = firstPos.y - centerPos.y;

		let skillinfolist = this.petLogic.petinfo.skill;
		let tmpskilllist = [];
		for (const skillid in skillinfolist) {
			if (skillinfolist.hasOwnProperty(skillid)) {
				const skillinfo = skillinfolist[skillid];
				skillinfo.skillid = skillid;
				tmpskilllist.push(skillinfo);
			}
		}
		tmpskilllist.sort((a, b) => {
			return a.idx - b.idx;
		});
		for (let index = 0; index < skillCnt; index++) {
			let skillinfo = tmpskilllist[index];
			let item = cc.instantiate(skillItem);
			item.active = true;
			item.name = 'skillbtn';
			let curAngle = -index * deltaAngle / 180 * GameDefine.pai;
			let curPos = cc.v2(-radius * Math.sin(curAngle), radius * Math.cos(curAngle));
			curPos = cc.ll.pAdd(centerPos, curPos);
			item.parent = this.skillNode;
			item.setPosition(curPos);
			if (index < this.petLogic.skill.length) {
				item.getChildByName('base').active = true;
				item.getChildByName('lock').active = false;
				let skillid = skillinfo.skillid;
				let skilldata = skillMgr.GetSkillInfo(skillid);
				cc.find('base/icon', item).getComponent(cc.Sprite).spriteFrame = this.SkillAtlas.getSpriteFrame(skilldata.strIcon);
				item.getChildByName('lockflag').active = skillinfo.lck == 1;
				let btn = item.getChildByName('base').getComponent(cc.Button);
				let handle = btn.clickEvents[0];
				handle.customEventData = skillid;

			} else {
				item.getChildByName('base').active = false;
				item.getChildByName('lock').active = true;
			}
		}

	},

	changeShenSkill(petid, skillid) {
		if (this.petLogic.petid == petid) {
			let shenskillnode = cc.find('SkillNode/shenSkill', this.node);
			shenskillnode.active = true;
			let sprite = cc.find('skillicon', shenskillnode).getComponent(cc.Sprite);
			sprite.spriteFrame = this.SkillAtlas.getSpriteFrame(skillid);
		}
	},

	showInfo() {
		this.kangPointAdd = 0;
		this.propertyPanel.getChildByName('NameLabel').getComponent(cc.Label).string = this.petLogic.name;
		this.propertyPanel.getChildByName('LevelLabel').color = CPubFunction.getReliveColor(this.petLogic.relive, 1);
		this.propertyPanel.getChildByName('LevelLabel').getComponent(cc.Label).string = this.petLogic.relive + '转' + this.petLogic.level + '级';
		this.petNode.getComponent('UIRole').setInfo({
			resid: this.petLogic.resid,
			name: '',
			petcolor: this.petLogic.petinfo.color
		});
		this.showPropertyInfo();

		cc.find('xiupoint', this.resistanceNode).getComponent(cc.Label).string = this.petLogic.xiulianleft + '/' + this.petLogic.xiulianfree;
		cc.find('xiuexp', this.resistanceLevelNode).getComponent(cc.Label).string = this.petLogic.xexp + '/' + this.petLogic.xmaxexp;
		cc.find('xiulevel', this.resistanceNode).getComponent(cc.Label).string = '修炼LV.' + this.petLogic.xlevel;
		cc.find('xiuprogress', this.resistanceLevelNode).getComponent(cc.ProgressBar).progress = this.petLogic.xexp / this.petLogic.xmaxexp;
		if (this.petLogic.xlevel > 0) {
			this.resistancePanel.parent.parent.active = false;
			this.resistancePanel = this.resistanceContent1;
			this.resistancePanel.parent.parent.active = true;
			this.initKangBtn();
			this.showDInfo();
		} else {
			this.resistancePanel.parent.parent.active = false;
			this.resistancePanel = this.resistanceContent;
			this.resistancePanel.parent.parent.active = true;
		}
		this.resistanceLevelNode.active = true;
		this.resistanceSureNode.active = false;
		this.showSkillInfo();

		if (this.petPropertyPanel.active == true) {
			this.refrushPetpropertyPanel();
		}

		let shenskillnode = cc.find('SkillNode/shenSkill', this.node);
		if (this.petLogic.grade >= 3) {
			shenskillnode.active = true;
			let sprite = cc.find('skillicon', shenskillnode).getComponent(cc.Sprite);
			if (this.petLogic.shenSkill == 0) {
				sprite.spriteFrame = '';
			} else {
				sprite.spriteFrame = this.SkillAtlas.getSpriteFrame(this.petLogic.shenSkill);
			}
		} else {
			shenskillnode.active = false;
		}

		// let wuxingX = this.resistancePanel.getChildByName('wuxingitem0').x;
		// let wuxingY = this.resistancePanel.getChildByName('wuxingitem0').y;
		// let wuxingCount = 0;
		// for (let index = 0; index < 5; index++) {
		// 	if (this.petLogic.wuxing[index] == 0) {
		// 		this.resistancePanel.getChildByName('wuxingitem' + index).active = false;
		// 		continue;
		// 	}
		// 	this.resistancePanel.getChildByName('wuxingitem' + index).active = true;
		// 	this.resistancePanel.getChildByName('wuxingitem' + index).x = (wuxingCount % 2) * 228 + wuxingX;
		// 	this.resistancePanel.getChildByName('wuxingitem' + index).y = wuxingY - Math.floor(wuxingCount / 2) * 35;
		// 	this.resistancePanel.getChildByName('wuxingitem' + index).getChildByName('value').getComponent(cc.Label).string = this.petLogic.wuxing[index];
		// 	this.resistancePanel.height = -(wuxingY - Math.floor(wuxingCount / 2) * 35 - 15);
		// 	wuxingCount++;
		// }

		if (cc.ll.player.petInfo.curid == this.petLogic.petid) {
			this.propertyPanel.getChildByName('canzhan').active = true;
			this.propertyPanel.getChildByName('joinBtn').active = false;
		} else {
			this.propertyPanel.getChildByName('canzhan').active = false;
			this.propertyPanel.getChildByName('joinBtn').active = true;
		}

		//显示隐藏换色按钮
		for (const itemid in cc.ll.player.itemList) {
			if (cc.ll.player.itemList[itemid] > 0) {
				let itemInfo = CPubFunction.GetItemInfo(itemid);
				if (itemInfo && itemInfo.num == this.petLogic.petinfo.dataid) {
					this.propertyPanel.getChildByName('btnChangeColor').active = true;
					this.propertyPanel.getChildByName('btnChangeColor').getComponent(cc.Button).clickEvents[0].customEventData = itemid;
					break;
				}
			}
		}


		for (const item of this.titleNodes) {
			item.active = false;
		}
		if (cc.find('ToggleGroup/toggle1', this.node).getComponent(cc.Toggle).isChecked) {
			this.propertyNode.active = true;
			this.titleNodes[0].active = true;
		} else if (cc.find('ToggleGroup/toggle2', this.node).getComponent(cc.Toggle).isChecked) {
			this.resistanceNode.active = true;
			this.titleNodes[1].active = true;
		} else if (cc.find('ToggleGroup/toggle3', this.node).getComponent(cc.Toggle).isChecked) {
			this.skillNode.active = true;
			this.titleNodes[2].active = true;
		} else if (cc.find('ToggleGroup/toggle4', this.node).getComponent(cc.Toggle).isChecked) {
			this.tujianNode.active = true;
			this.titleNodes[3].active = true;
		}
	},

	propertyJoinBtnClicked(e, d) {
		cc.ll.net.send('c2s_change_pet', {
			petid: this.petLogic.petid
		});
	},
	//--zfy 增加 二次确认弹窗
	propertyDelBtnClicked(e, d) {
		let mainuilogic = cc.ll.player.getMainUILogic();
		if (mainuilogic && mainuilogic.isBattle) {
			cc.ll.msgbox.addMsg('战斗中无法放生宠物');
			return;
		}

		cc.ll.notice.addMsg(1, '是否放生召唤兽' + '\n' + this.petLogic.name + '(' + this.petLogic.relive + '转' + this.petLogic.level + '级' + ')', () => {
			if (cc.ll.player.safe_lock == 1) {
				let safe_notice = cc.instantiate(this.SafeNotice);
				safe_notice.parent = cc.find('Canvas');
				safe_notice.getComponent('SafeNotice').setCallback(() => {
					cc.ll.net.send('c2s_del_pet', {
						roleid: cc.ll.player.roleid,
						petid: this.petLogic.petid
					});
				});
			}
			else {
				cc.ll.net.send('c2s_del_pet', {
					roleid: cc.ll.player.roleid,
					petid: this.petLogic.petid
				});
			}
		}, () => {}, 0);
	},

	propertyAddBtnClicked(e, d) {
		this.addPointPanel.active = true;
		this.propertyPanel.active = false;
	},

	propertyBackBtnClicked(e, d) {
		this.addPointPanel.active = false;
		this.propertyPanel.active = true;
	},

	propertyAddPoint(d) {
		if (this.petLogic.qianneng <= 0) {
			this.currentBtn = null;
			return;
		}
		this.petLogic.ppointT[d]++;
		this.petLogic.ppoint[d]++;
		this.petLogic.calculateProperty();
		this.showPropertyInfo();
	},

	propertySubPoint(d) {
		if (this.petLogic.ppointT[d] <= 0) {
			this.currentBtn = null;
			return;
		}
		this.petLogic.ppointT[d]--;
		this.petLogic.ppoint[d]--;
		this.petLogic.calculateProperty();
		this.showPropertyInfo();
	},

	porpertyReset(e, d) {
		cc.ll.net.send('c2s_update_pet', {
			roleid: cc.ll.player.roleid,
			type: 0,
			petid: this.petLogic.petid,
			info: '{}'
		});
	},

	porpertySure(e, d) {
		cc.ll.net.send('c2s_update_pet', {
			roleid: cc.ll.player.roleid,
			type: 2,
			petid: this.petLogic.petid,
			info: JSON.stringify(this.petLogic.ppointT)
		});
	},

	addExpBtnClicked(e, d) {
		if (this.useui) {
			this.useui.destroy();
			this.useui = null;
		}
		this.useui = cc.instantiate(this.useItemUI);
		this.useui.parent = this.node;
		this.useui.getComponent('UseItemUI').loadList('点击/长按可使用经验丹', [10112, 10113, 10114], this.petLogic.petid, 1);
	},

	addColorBtnClicked(e, d) {
		if (this.useui) {
			this.useui.destroy();
			this.useui = null;
		}
		this.useui = cc.instantiate(this.useItemUI);
		this.useui.parent = cc.find('Canvas');
		this.useui.getComponent('UseItemUI').loadList('点击/长按可使用元气丹', [d], this.petLogic.petid, 1);
	},


	resistanceAddPoint(d) {
		if (this.petLogic.xiulianleft - this.kangPointAdd <= 0) {
			this.currentBtn = null;
			return;
		}
		if (this.petLogic.dpoint[d] >= this.petLogic.getMaxAddPoint(d)) {
			this.currentBtn = null;
			return;
		}
		this.petLogic.dpointT[d]++;
		this.petLogic.dpoint[d]++;
		this.resistanceLevelNode.active = false;
		this.resistanceSureNode.active = true;
		this.kangPointAdd++;
		this.initKangBtn();
		this.showDInfo();
	},

	resistanceSubPoint(d) {
		if (this.petLogic.dpointT[d] <= 0) {
			this.currentBtn = null;
			return;
		}
		this.petLogic.dpointT[d]--;
		this.petLogic.dpoint[d]--;
		this.resistanceLevelNode.active = false;
		this.resistanceSureNode.active = true;
		this.kangPointAdd--;
		this.initKangBtn();
		this.showDInfo();
	},

	kangAddPoint(e, d) {},

	kangSubPoint(e, d) {},

	kangSureBtnClicked(e, d) {
		this.resistanceLevelNode.active = true;
		this.resistanceSureNode.active = false;
		if (d == 1) {
			cc.ll.net.send('c2s_update_pet', {
				roleid: cc.ll.player.roleid,
				type: 3,
				petid: this.petLogic.petid,
				info: JSON.stringify(this.petLogic.dpointT)
			});
		} else {
			this.kangPointAdd = 0;
			this.petLogic.initPoint();
			this.initKangBtn();
		}
	},

	kangReset(e, d) {
		this.resistanceLevelNode.active = true;
		this.resistanceSureNode.active = false;
		cc.ll.net.send('c2s_update_pet', {
			roleid: cc.ll.player.roleid,
			type: 1,
			petid: this.petLogic.petid,
			info: '{}'
		});
	},

	xiulianBtnClicked(e, d) {
		if (this.useui1) {
			this.useui1.destroy();
			this.useui1 = null;
		}
		// if (this.petLogic.level >= cc.ll.player.level + 10) {
		// 	return;
		// }
		this.useui1 = cc.instantiate(this.useItemUI);
		this.useui1.parent = this.node;
		this.useui1.getComponent('UseItemUI').loadList('点击/长按可操作', [10116, 90001], this.petLogic.petid, 1);
	},

	longguBtnClicked(e, d) {
		if (this.useui2) {
			this.useui2.destroy();
			this.useui2 = null;
		}
		// if (this.petLogic.level >= cc.ll.player.level + 10) {
		// 	return;
		// }
		this.useui2 = cc.instantiate(this.useItemUI);
		this.useui2.parent = this.node;
		this.useui2.getComponent('UseItemUI').loadList('点击/长按可使用龙之骨', [10117], this.petLogic.petid, 1);
	},

	panelToggleClicked(e, d) {
		if (this.petLogic.petinfo == null && d != 4) {
			return;
		}
		this.propertyNode.active = false;
		this.resistanceNode.active = false;
		this.skillNode.active = false;
		this.tujianNode.active = false;
		this.petContent.parent.parent.active = true;
		if (d == 1) {
			this.propertyNode.active = true;
		} else if (d == 2) {
			this.resistanceNode.active = true;
		} else if (d == 3) {
			this.skillNode.active = true;
		} else if (d == 4) {
			this.petContent.parent.parent.active = false;
			this.tujianNode.active = true;
		}
		for (const item of this.titleNodes) {
			item.active = false;
		}
		this.titleNodes[d - 1].active = true;
		//zfy --分页音效
		cc.ll.AudioMgr.playFenyeAudio();
	},

	petItemClicked(e, d) {
		if (this.currentSelected == d) {
			return;
		}


		//显示隐藏换色按钮
		let petItem = e.target.getComponent('PetItem').itemInfo;

		if (petItem != null) {
			for (const itemid in cc.ll.player.itemList) {
				if (cc.ll.player.itemList[itemid] > 0) {
					let itemInfo = CPubFunction.GetItemInfo(itemid);
					if (itemInfo.num == petItem.dataid) {
						this.propertyPanel.getChildByName('btnChangeColor').active = true;
						this.propertyPanel.getChildByName('btnChangeColor').getComponent(cc.Button).clickEvents[0].customEventData = itemid;
						break;
					}
				} else {
					this.propertyPanel.getChildByName('btnChangeColor').active = false;
				}
			}
		} else {
			this.propertyPanel.getChildByName('btnChangeColor').active = false;
		}

		this.currentSelected = d;
		for (const item of this.itemNodes) {
			if (e.target == item) {
				item.getComponent('PetItem').selected();
			} else {
				item.getComponent('PetItem').unSelected();
			}
		}

		this.petLogic.setInfo(e.target.getComponent('PetItem').itemInfo);
		this.showInfo();
		if (this.petPropertyPanel.active == true) {
			this.refrushPetpropertyPanel();
		}
	},

	reliveBtnClicked(e, d) {
		cc.ll.net.send('c2s_relive_pet', {
			petid: this.petLogic.petid
		});
	},

	onCloseBtnClicked(e, d) {
		//zfy --关闭音效
		cc.ll.AudioMgr.playCloseAudio();
		this.node.destroy();
	},

	/*
	 * 显示天资界面
	 */
	showPetpropertyPanel() {
		this.petPropertyPanel.active = true;
		this.addPointPanel.active = false;
		this.propertyPanel.active = false;
		this.refrushPetpropertyPanel();
	},

	/*
	 * 刷新天资界面
	 */
	refrushPetpropertyPanel() {
		let info = {
			rate: this.petLogic.rate,
			cur_hp: this.petLogic.hp,
			cur_mp: this.petLogic.mp,
			cur_atk: this.petLogic.atk,
			cur_spd: this.petLogic.spd,
			max_rate: this.petLogic.maxRate
		};
		for (let key in cc.ll.propData.pet) {
			if (cc.ll.propData.pet.hasOwnProperty(key)) {
				let item = cc.ll.propData.pet[key];
				if (key == this.petLogic.dataid) {
					//info.max_rate = JSON.parse(item.rate)[1];
					info.max_hp = JSON.parse(item.hp)[1];
					info.max_mp = JSON.parse(item.mp)[1];
					info.max_atk = JSON.parse(item.atk)[1];
					info.max_spd = JSON.parse(item.spd)[1];
				}
			}
		}
		let hp_progress = this.petPropertyPanel.getChildByName('hpProgress');
		let hp_label = hp_progress.getChildByName('label').getComponent(cc.Label);
		let hpProgress = (info.cur_hp == 0) ? 0 : (info.cur_hp / info.max_hp);
		hp_progress.getComponent(cc.ProgressBar).progress = hpProgress > 1 ? 1 : hpProgress;
		hp_label.string = info.cur_hp + '/' + info.max_hp;

		let mp_progress = this.petPropertyPanel.getChildByName('mpProgress');
		let mp_label = mp_progress.getChildByName('label').getComponent(cc.Label);
		let mpProgress = (info.cur_mp == 0) ? 0 : (info.cur_mp / info.max_mp);
		mp_progress.getComponent(cc.ProgressBar).progress = mpProgress > 1 ? 1 : mpProgress;
		mp_label.string = info.cur_mp + '/' + info.max_mp;

		let att_progress = this.petPropertyPanel.getChildByName('attProgress');
		let att_label = att_progress.getChildByName('label').getComponent(cc.Label);
		let atkProgress = (info.cur_atk == 0) ? 0 : (info.cur_atk / info.max_atk);
		att_progress.getComponent(cc.ProgressBar).progress = atkProgress > 1 ? 1 : atkProgress;
		att_label.string = info.cur_atk + '/' + info.max_atk;

		let spe_progress = this.petPropertyPanel.getChildByName('speProgress');
		let spe_label = spe_progress.getChildByName('label').getComponent(cc.Label);
		let spdProgress = (info.cur_spd == 0) ? 0 : (info.cur_spd / info.max_spd);
		spe_progress.getComponent(cc.ProgressBar).progress = spdProgress > 1 ? 1 : spdProgress;
		spe_label.string = info.cur_spd + '/' + info.max_spd;

		let rate_label = this.petPropertyPanel.getChildByName('RateLabel').getComponent(cc.Label);
		//let currate = info.rate + this.petLogic.longgu * 0.01;
		rate_label.string = info.rate.toFixed(3) + '/' + info.max_rate.toFixed(3);
		for (let i = 1; i <= 5; ++i) {
			let sprite = this.petPropertyPanel.getChildByName('grow' + i).getComponent(cc.Sprite);
			let url = 'ui_common_loading_bar2';
			if (i / 5 >= info.rate / info.max_rate * 0.99)
				url = 'ui_common_loading_bar_bg';
			sprite.spriteFrame = this.commonUIAtlas.getSpriteFrame(url);
		}

		cc.find('longgu', this.petPropertyPanel).getComponent(cc.Label).string = `龙骨(${this.petLogic.longgu}/${this.petLogic.getMaxLongGu()})`;
	},

	/*
	 * 不显示天资界面
	 */
	unshowPetpropertyPanel() {
		this.petPropertyPanel.active = false;
		this.addPointPanel.active = false;
		this.propertyPanel.active = true;
	},

	/*
	 * 显示洗练界面
	 */
	showPetWashPropertyPanel() {
		let logic = this.washPropertyPanel.getComponent('WashProperty');
		logic.close_callback = this.refrushPetpropertyPanel.bind(this);
		logic.show(this.petLogic);
	},

	showSkillDetail(e, d) {
		let detail = cc.instantiate(this.SkillDetail);
		let detailLogic = detail.getComponent('SkillDetail');
		detailLogic.loadInfo(d);
		detail.parent = cc.find('Canvas');

		detailLogic.showOperation({
			['forget']: () => {
				let skillLogic = skillMgr.GetSkillInfo(d);
				cc.ll.notice.addMsg(1, `${this.petLogic.name} 遗忘掉 ${skillLogic.strName} ？`, () => {
					cc.ll.net.send('c2s_pet_forgetskill', {
						petid: this.petLogic.petid,
						skillid: d,
					});
					detailLogic.nodeDestroy();
				});
			},
			['lock']: () => {
				let skillLogic = skillMgr.GetSkillInfo(d);
				let list = this.petLogic.petinfo.skill;
				let n = 0;
				for (const skillid in list) {
					if (list.hasOwnProperty(skillid)) {
						const skillinfo = list[skillid];
						if(skillinfo.lck == 1){
							n ++;
						}
					}
				}
				cc.ll.notice.addMsg(1, `是否花费 ${Math.pow(2, n) * 1000}仙玉 来锁定 ${skillLogic.strName} ？`, () => {
					cc.ll.net.send('c2s_pet_lockskill', {
						petid: this.petLogic.petid,
						skillid: d,
					});
					detailLogic.nodeDestroy();
				});
			}
		});
	},

	openShenSkillPanel() {
		let petshenskill = cc.instantiate(this.PetShenSkillUI);
		petshenskill.name = 'PetShenSkill';
		petshenskill.parent = this.node;

		let shenlogic = petshenskill.getComponent('PetShenSkill');
		shenlogic.setPetId(this.petLogic.petid);
		shenlogic.init();
	},

	/*
	 * 点击亲密按钮
	 */
	onQinmiBtnClick() {
		if (this.useui) {
			this.useui.destroy();
			this.useui = null;
		}
		this.useui = cc.instantiate(this.useItemUI);
		this.useui.parent = this.node;
		// this.useui.getComponent('UseItemUI').loadList('点击/长按可使用经验丹', [10112, 10113, 10114], this.petLogic.petid, 1);
		this.useui.getComponent('UseItemUI').loadList('点击/长按可使用亲密丹', [10111], this.petLogic.petid, 1);
	},
});
