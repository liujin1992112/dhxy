cc.Class({
	extends: cc.Component,

	properties: {
		listContent: cc.Node,
		listItem: cc.Node,
		aimLab: cc.Label,
	},

	onLoad() {
	},

	getList(type){
		this.aimLab.string = type;
		cc.ll.net.send('c2s_team_list',{
			roleid: cc.ll.player.roleid,
			type: type
		});
	},

	showTeamList(list) {
		if (!list) {
			return;
		}
		this.requestList = list;
		this.currentY = 0;
		this.listContent.destroyAllChildren();
		this.listContent.height = this.listContent.parent.height;
		for (let index = 0; index < list.length; index++) {
			const info = list[index];
			let listItem = cc.instantiate(this.listItem);
			listItem.active = true;
			listItem.parent = this.listContent;
			listItem.getComponent('TeamListItem').loadInfo(info);
			listItem.x = -148 + (index % 2) * 296;
			listItem.y = this.currentY;

			if (listItem.y - listItem.height < -this.listContent.height) {
				this.listContent.height = listItem.height - listItem.y;
			}
			if (index % 2 == 1) {
				this.currentY -= listItem.height;
			}
		}
	},

	onCloseBtnClicked(e, d) {
		this.node.active = false;
	},
});
