let GameDefine = require('../game/GameDefine');
let GameRes = require('../game/GameRes');
let pubFun = require('../game/PubFunction');
let StrCommon = require('../etc/str_common')
let GoodsMgr = require('../game/GoodsMgr');

cc.Class({
	extends: cc.Component,

	properties: {
		listContent: cc.Node,
		listItem: cc.Node,
		typeNode: cc.Node,
		xiedaiLab: cc.Label,
		discriptionLab: cc.Label,
		nameLab: cc.Label,
		rateLab: cc.Label,
		hpLab: cc.Label,
		mpLab: cc.Label,
		atkLab: cc.Label,
		spdLab: cc.Label,

		tipNode: cc.Node,
		buzhuoBtn: cc.Node,
		hechengBtn: cc.Node,
		hechengPre: cc.Prefab,

		SkillAtlas: cc.SpriteAtlas,
		skillNode: cc.Node,
	},

	onLoad() {
		this.curPetInfo = null;
		this.loadPetList();
	},

	loadPetList() {
		let curListY = -40;
		let curListX = -80;
		this.listContent.destroyAllChildren();

		for (const key in cc.ll.propData.pet) {
			if (key == 'datatype') {
				continue;
			}
			let itemInfo =  pubFun.deepClone(cc.ll.propData.pet[key]);

			let item = cc.instantiate(this.listItem);
			item.getChildByName('icon').getComponent(cc.Sprite).spriteFrame = GameRes.getPetHead(itemInfo.resid);
			item.getChildByName('level').getComponent(cc.Label).string = itemInfo.xiedailevel;
			item.getChildByName('level').color = pubFun.getReliveColor(itemInfo.xiedairelive);
	
			if (cc.ll.player.level >= itemInfo.xiedailevel && cc.ll.player.relive >= itemInfo.xiedairelive) {
				item.getChildByName('level').active = false;
			} else {
				item.getChildByName('mask').active = true;
			}
			item.active = true;
			item.x = curListX;
			item.y = curListY;
			curListX += 80;
			if (curListX > 80) {
				curListX = -80;
				curListY -= 80;
			}
			item.parent = this.listContent;
			item.petid = key;

			if (this.curPetInfo == null) {
				this.curPetInfo = itemInfo;
				item.getComponent(cc.Toggle).isChecked = true;
				this.showPetInfo();
			}
		}
		if (curListX > -80) {
			curListY -= 80;
		}
		this.listContent.height = -curListY;
		if (this.listContent.height < this.listContent.parent.height) {
			this.listContent.height = this.listContent.parent.height;
		}
	},

	showPetInfo() {
		this.curPetInfo.rate = JSON.parse(this.curPetInfo.rate);
		this.curPetInfo.hp = JSON.parse(this.curPetInfo.hp);
		this.curPetInfo.mp = JSON.parse(this.curPetInfo.mp);
		this.curPetInfo.atk = JSON.parse(this.curPetInfo.atk);
		this.curPetInfo.spd = JSON.parse(this.curPetInfo.spd);
		for (let index = 0; index < 5; index++) {
			this.typeNode.getChildByName(index + '').active = false;
		}
		this.node.getChildByName('UIRole').getComponent('UIRole').setInfo({ resid: this.curPetInfo.resid, name: '' });

		this.typeNode.getChildByName(this.curPetInfo.grade + '').active = true;
		this.nameLab.string = this.curPetInfo.name;
		this.xiedaiLab.node.color = pubFun.getReliveColor(this.curPetInfo.xiedairelive, 1);
		this.xiedaiLab.string = `${this.curPetInfo.xiedairelive}转${this.curPetInfo.xiedailevel}级`;
		
		this.rateLab.string = this.curPetInfo.rate[0] + ' - ' + this.curPetInfo.rate[1];
		this.hpLab.string = this.curPetInfo.hp[0] + ' - ' + this.curPetInfo.hp[1];
		this.mpLab.string = this.curPetInfo.mp[0] + ' - ' + this.curPetInfo.mp[1];
		this.atkLab.string = this.curPetInfo.atk[0] + ' - ' + this.curPetInfo.atk[1];
		this.spdLab.string = this.curPetInfo.spd[0] + ' - ' + this.curPetInfo.spd[1];

		this.discriptionLab.string = this.curPetInfo.intro;

		this.tipNode.active = false;
		this.buzhuoBtn.active = false;
		this.hechengBtn.active = false;
		if (cc.ll.player.level >= this.curPetInfo.xiedailevel && cc.ll.player.relive >= this.curPetInfo.xiedairelive) {
			if (this.curPetInfo.gettype == 1) {
				this.hechengBtn.active = true;
			} else {
				this.buzhuoBtn.active = true;
			}
		} else {
			this.tipNode.active = true;
			this.tipNode.getChildByName('level').getComponent(cc.Label).string = this.xiedaiLab.string;
		}

		this.skillNode.active = false;
		if (this.curPetInfo.skill.length > 0) {
			this.skillNode.active = true;
			this.skillNode.getChildByName('icon').getComponent(cc.Sprite).spriteFrame = this.SkillAtlas.getSpriteFrame(this.curPetInfo.skill);
			let btn = this.skillNode.getComponent(cc.Button);
			let handle = btn.clickEvents[0];
			handle.customEventData = this.curPetInfo.skill
		}
	},

	petItemClicked(e, d) {
		if (e.target.getComponent(cc.Toggle).isChecked) {
			let petid = Number(e.target.petid);
			this.curPetInfo = pubFun.deepClone(cc.ll.propData.pet[petid]);
			this.showPetInfo();
		}
	},

	buzhuoBtnClicked(e, d){
		if (!cc.ll.player.itemList[10001]) {
			cc.ll.msgbox.addMsg(StrCommon.YinYaoNotEnough);
		}else{
			GoodsMgr.useItem(10001);
			let partnerlogic = this.node.parent.getComponent('PetPanel');
			partnerlogic.destroy();
		}
	},

	clear(){
		let uirolenode = this.node.getChildByName('UIRole');
		uirolenode.getComponent('UIRole').clear();
	},

	hechengBtnClicked(e, d){
		let hecheng = cc.instantiate(this.hechengPre);
		hecheng.parent = this.node.parent;
	},
});
