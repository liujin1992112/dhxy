let GameRes = require('../game/GameRes');
let CPubFunction = require('../game/PubFunction');

cc.Class({
    extends: cc.Component,
    properties: {
        pfpanel: cc.Prefab,
    },

    onLoad () {
        this.pfdata = null;
    },

    start () {
        this.node.active = false;
    },

    showPalaceFightIcon () {
        this.node.getComponent(cc.Animation).play('huangchengPK');
        this.node.active = true;
    },

    unshowPalaceFightIcon () {
        this.node.getComponent(cc.Animation).stop('huangchengPK');
        this.node.active = false;
    },

    clickPalaceFightIcon () {
        let npcdata = cc.ll.propData.npc['30060'];
        let posmsg = npcdata.auto_create.split(';');
        CPubFunction.MainPlayerToDo(posmsg[0], parseInt(posmsg[1])-3, parseInt(posmsg[2])+3, () => { });
    },

    syncPalaceFightData (data) {
        console.log('syncPalaceFightData', data);
        this.pfdata = data;
        /* 有人拒绝决斗 */
        if ((data.recipient.state == 2 || data.sponsor.state == 2) && (data.recipient.roleid == cc.ll.player.roleid || data.sponsor.roleid == cc.ll.player.roleid)) {
            this.unshowPalaceFightIcon();
            this.deletePalaceFightPanel();
            return;
        }
        /* 决斗开始 */
        if (data.recipient.state == 1 && data.sponsor.state == 1) {
            this.unshowPalaceFightIcon();
            let palaceFightPanel = cc.find('Canvas/MainUI/PalaceFightPanel');
            if (palaceFightPanel) {
                palaceFightPanel.getComponent('PalaceFightPanel').setTm(Math.floor(data.tm/1000));
            }
            else if (this.canShowPalaceFightPanel()) {
                cc.ll.net.send('c2s_palace_rolelist', {
                    roleid: cc.ll.player.roleid,
                });
            }
            // this.pfdata = null;
            return;
        }

        if (data.sponsor.state == 1 && data.recipient.state == 0) { // 发起决斗 
            if (cc.ll.player.roleid == data.sponsor.roleid) {
                let pkpanel = cc.find('Canvas/MainUI/PKPanel');
                if (pkpanel) {
                    pkpanel.destroy();
                }
            }
            this.showPalaceFightIcon();
        }
    },

    deletePalaceFightPanel () {
        this.pfdata = null;
        let pfpanel = cc.find('Canvas/MainUI/PalaceFightPanel');
        if (pfpanel) {
            pfpanel.destroy();
        }
    },

    hasPalaceFight () {
        return (this.pfdata && this.pfdata.recipient.roleid == cc.ll.player.roleid);
    },

    /*
     * 能否显示palaceFightPanel界面
     */
    canShowPalaceFightPanel () {
        let canshow = (this.pfdata.sponsor.roleid == cc.ll.player.roleid || this.pfdata.recipient.roleid == cc.ll.player.roleid);
        canshow = canshow && (cc.ll.player.mapid == 1206 && !cc.ll.player.getMainUILogic().isBattle);
        if (this.pfdata.sponsor.roleid == cc.ll.player.roleid) {
            canshow = canshow && (this.pfdata.recipient.state == 1);
        }
        return canshow;
    },

    getCurTm () {
        if (this.pfdata) {
            return Math.floor(this.pfdata.tm/1000);
        }
        return 0;
    },

    getIsAllPrepare () {
        if (this.pfdata) {
            return (this.pfdata.sponsor.state == 1 && this.pfdata.recipient.state == 1);
        }
        return false;
    },

    isSponsor () {
        return (this.pfdata && this.pfdata.sponsor.roleid == cc.ll.player.roleid);
    },

    /*
     * 能否关闭palaceFightPanel界面 
     */
    canClosePalaceFight () {
        if (this.pfdata) {
            return (this.pfdata.recipient.state == 0);
        }
        return true;
    },

    update (dt) {
        if (this.pfdata) {
            this.pfdata.tm -= dt*1000;
            if (this.pfdata.tm < 0) {
                this.pfdata = null;
            }
        }
    },
});
