let GameRes = require('../game/GameRes');
let CPubFunction = require('../game/PubFunction');

cc.Class({
	extends: cc.Component,

	properties: {
		nameLab: cc.Label,
		levelLab: cc.Label,
		headIcon: cc.Sprite,
		cntLab: cc.Label,
		requestSign: cc.Node,
		requestBtn: cc.Node,
	},

	onLoad() {
	},

	loadInfo(info) {
		this.teamInfo = info;

		this.nameLab.string = info.name;
		this.levelLab.node.color = CPubFunction.getReliveColor(info.relive);
		this.levelLab.string = info.relive + '转' + info.level + '级';
		this.headIcon.spriteFrame = GameRes.getRoleHead(info.resid);
		this.cntLab.string = info.cnt + '/5';
		if (info.request) {
			this.requestSign.active = true;
			this.requestBtn.active = false;
		}
	},

	
	onRequestBtnClicked(e, d) {
		if (cc.ll.player.teamid > 0) {
            return;
        }
        cc.ll.net.send('c2s_requst_team', {
            roleid: cc.ll.player.roleid,
            teamid: this.teamInfo.teamid
        });
	},
	
});
