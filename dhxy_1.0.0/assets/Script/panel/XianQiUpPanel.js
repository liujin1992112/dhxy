let GameRes = require('../game/GameRes');

cc.Class({
	extends: cc.Component,

	properties: {
		listContent: cc.Node,
		useEquipNodes: [cc.Node],
		nextEquipNode: cc.Node,
		listItem: cc.Node,
		upgradeNode: cc.Node,
		tipsNode: cc.Node,
	},

	ctor() {
		this.issend = false;
	},

	showNextEquipInfo(info) {
		this.nextEquipNode.getComponent('EquipItem').loadInfo(info);
	},

	loadEquipList() {
		for (const item of this.useEquipNodes) {
			item.getComponent('EquipItem').reset();
			item.equiptag = 0;
		}
		this.nextEquipNode.getComponent('EquipItem').reset();
		this.selectedCnt = 0;
		this.curInfo = null;
		let hadXianqi = false;

		let curListY = -40;
		let curListX = -80;
		this.listContent.destroyAllChildren();

		this.upgradeNode.active = false;
		this.tipsNode.active = false;
		if (!cc.ll.player.equipdata || !cc.ll.player.equipdata.info) {
			this.tipsNode.active = true;
			this.tipsNode.getChildByName('tips').getComponent(cc.Label).string = '你还没有仙器哦';
			return;
		}
		for (const key in cc.ll.player.equipdata.info) {
			let itemInfo = cc.ll.player.equipdata.info[key];
			if (itemInfo.EquipType != 3) {
				continue;
			}
			let item = cc.instantiate(this.listItem);
			item.getChildByName('icon').getComponent(cc.Sprite).spriteFrame = GameRes.getItemIcon(itemInfo.Shape);
			item.getChildByName('level').getComponent(cc.Label).string = itemInfo.Grade;
			item.active = true;
			item.x = curListX;
			item.y = curListY;
			curListX += 80;
			if (curListX > 80) {
				curListX = -80;
				curListY -= 80;
			}
			item.parent = this.listContent;
			item.equiptag = key;
			hadXianqi = true;
		}
		if (curListX > -80) {
			curListY -= 80;
		}
		this.listContent.height = -curListY;
		if (this.listContent.height < this.listContent.parent.height) {
			this.listContent.height = this.listContent.parent.height;
		}
		if (!hadXianqi) {
			this.tipsNode.active = true;
			this.tipsNode.getChildByName('tips').getComponent(cc.Label).string = '你还没有仙器哦';
			return;
		}

		this.upgradeNode.active = true;
	},

	getNextEquipInfo() {
		if (this.curInfo && this.curInfo.NextType > 0) {
			cc.ll.net.send('c2s_next_equip', {
				resid: this.curInfo.NextType,
				type: this.curInfo.EquipType,
				index: this.curInfo.EIndex,
				grade: this.curInfo.Grade + 1,
				roleid: cc.ll.player.roleid
			});
		} else {
			return;
		}
	},

	equipItemClicked(e, d) {
		if (!e.target.getComponent(cc.Toggle).isChecked) {
			for (let index = 0; index < this.useEquipNodes.length; index++) {
				const item = this.useEquipNodes[index];
				if (item.equiptag == e.target.equiptag) {
					if (index == 0) {
						this.nextEquipNode.getComponent('EquipItem').reset();
					}
					item.getComponent('EquipItem').reset();
					item.equiptag = 0;
					if (this.selectedCnt > 0) {
						this.selectedCnt--;
					}
					if (this.selectedCnt == 0) {
						this.curInfo = null;
					}
					return;
				}
			}
			return;
		}

		if (this.selectedCnt >= 3 || (this.curInfo && this.curInfo.Grade > 1 && this.selectedCnt >= 2)) {
			e.target.getComponent(cc.Toggle).isChecked = false;
			return;
		}
		let selectedInfo = cc.ll.player.equipdata.info[e.target.equiptag];
		if (selectedInfo.Grade >= 4 || (this.curInfo != null && this.curInfo.Grade != selectedInfo.Grade)) {
			e.target.getComponent(cc.Toggle).isChecked = false;
			return;
		}
		// for (let index = 0; index < this.selectedCnt; index++) {
		// 	if (this.useEquipNodes[index].equiptag == e.target.equiptag) {
		// 		return;
		// 	}
		// }
		let emptyPos = -1;
		for (let index = 0; index < this.useEquipNodes.length; index++) {
			const item = this.useEquipNodes[index];
			if (item.equiptag == 0) {
				emptyPos = index;
				break;
			}
		}
		if (emptyPos >= 0) {
			this.useEquipNodes[emptyPos].getComponent('EquipItem').loadInfo(selectedInfo);
			this.useEquipNodes[emptyPos].equiptag = e.target.equiptag;
			if (emptyPos == 0) {
				this.curInfo = selectedInfo;
				this.getNextEquipInfo();
			}
			this.selectedCnt++;
		}
	},

	upgradeBtnClicked(e, d) {
		if (!this.curInfo) {
			return;
		}
		if ((this.curInfo.Grade > 1 && this.selectedCnt < 2) || (this.curInfo.Grade == 1 && this.selectedCnt < 3)) {
			return;
		}
		// 避免多次点击
		if (this.issend == true) {
			return;
		}
		this.issend = true;

		// for(let i = 0; i < 10; i++){
		cc.ll.net.send('c2s_xianqi_upgrade', {
			roleid: cc.ll.player.roleid,
			equipid: this.curInfo.EquipID,
			use1: this.useEquipNodes[1].equiptag,
			use2: this.useEquipNodes[2].equiptag
		});
		// }

		setTimeout(() => {
			this.issend = false;
		}, 5 * 1000);
		this.curInfo = null;
		this.nextEquipInfo = null;
	},


	onCloseBtnClicked(e, d) {
		this.node.destroy();
	}
});
