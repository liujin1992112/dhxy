let GameRes = require('../game/GameRes');
let CPubFunction = require('../game/PubFunction');

cc.Class({
	extends: cc.Component,

	properties: {
		nameLab: cc.Label,
		levelLab: cc.Label,
		headIcon: cc.Sprite,
	},

	onLoad() {
	},

	loadInfo(info) {
		this.roleInfo = info;

		this.nameLab.string = info.name;
		this.levelLab.node.color = CPubFunction.getReliveColor(info.relive);
		this.levelLab.string = info.relive + '转' + info.level + '级';
		this.headIcon.spriteFrame = GameRes.getRoleHead(info.resid);
	},

	onRejectBtnClicked(e, d) {
		cc.ll.net.send('c2s_operteam', {
			operation: 0,
			roleid: this.roleInfo.roleid,
			teamid: cc.ll.player.teamid
		});
	},

	onAgreeBtnClicked(e, d) {
		cc.ll.net.send('c2s_operteam', {
			operation: 1,
			roleid: this.roleInfo.roleid,
			teamid: cc.ll.player.teamid
		});
	},
});
