cc.Class({
	extends: cc.Component,
	properties: {
		edit1: cc.EditBox,
		edit2: cc.EditBox,
		edit3: cc.EditBox,
		edit4: cc.EditBox,
	},

	start () {
	},

	/*
	 * 点击确认按钮
	 */
	onConfirmClick () {
		if (!this.verifyAllPassword()) {
			return;
		}
		let data = {
			account: this.edit4.string,
			safecode: this.edit1.string,
			password: this.edit2.string,
		};
		cc.ll.http.send('/changePassword', data, ret => {
			if (ret.errcode == 0) {
				console.log('data.password', data.password);
				cc.sys.localStorage.setItem('password', data.password);
				let logic = cc.find('Canvas').getComponent('LoginLogic');
				if (logic && logic.psdEditBox) {
					logic.psdEditBox.string = data.password;
				}
				cc.ll.msgbox.addMsg('修改登录密码成功！');
			}
			this.node.active = false;
		});
	},

	/*
	 * 点击取消按钮
	 */
	onCancelClick () {
		this.node.active = false;
	},

	/*
	 * 验证所有格式
	 */
	verifyAllPassword () {
		if (!this.verifyPassword(this.edit1.string, { minlen: 4, maxlen: 12, }, '安全密码')) {
			return false;
		}
		if (!this.verifyPassword(this.edit2.string, { minlen: 6, maxlen: 20, }, '帐号密码')) {
			return false;
		}
		if (!this.verifyPassword(this.edit3.string, { minlen: 6, maxlen: 20, }, '帐号密码')) {
			return false;
		}
		if (this.edit2.string != this.edit3.string) {
			cc.ll.msgbox.addMsg('两次密码不一致！');
			return false;
		}
		if (!this.verifyPassword(this.edit4.string, { minlen: 6, maxlen: 20, onlynum: false, }, '帐号')) {
			return false;
		}
		return true;
	},

	onEditEnd (event, param) {
		if (this.edit1.string.length > 0 && 
			!this.verifyPassword(this.edit1.string, { minlen: 4, maxlen: 12, }, '安全密码')) {
			return;
		}
		if (this.edit2.string.length > 0 && 
			!this.verifyPassword(this.edit2.string, { minlen: 6, maxlen: 20, }, '帐号密码')) {
			return;
		}
		if (this.edit3.string.length > 0 &&
			!this.verifyPassword(this.edit3.string, { minlen: 6, maxlen: 20, }, '帐号密码')) {
			return;
		}
		if (this.edit3.string.length > 0 && this.edit2.string.length > 0 && this.edit2.string != this.edit3.string) {
			cc.ll.msgbox.addMsg('两次密码不一致！');
			return;
		}
		if (this.edit4.string.length > 0 && 
			!this.verifyPassword(this.edit4.string, { minlen: 6, maxlen: 20, onlynum: false, }, '帐号')) {
			return;
		}
	},

	/*
	 * 验证密码格式
	 */
	verifyPassword (pass, opt, tipName) {
		tipName = tipName || '';
		if (opt.minlen && pass.length < opt.minlen) {
			cc.ll.msgbox.addMsg(`${tipName}密码长度不能少于${opt.minlen}位！`);
			return false;
		}
		if (opt.maxlen && pass.length > opt.maxlen) {
			cc.ll.msgbox.addMsg(`${tipName}密码长度不能大于${opt.maxlen}位！`);
			return false;
		}
		if (opt.mixing) {
			if (/^[0-9]*$/.test(pass) || /^[a-zA-Z]*$/.test(pass)) {
				cc.ll.msgbox.addMsg(`${tipName}密码必须含有字母和数字！`);
				return false;
			}
		}
		if (typeof(opt.onlynum) == 'boolean' && !opt.onlynum) {
			if (/^[0-9]*$/.test(pass)) {
				cc.ll.msgbox.addMsg(`${tipName}密码必须含有字母和数字！`);
				return false;
			}
		}
		if (! /^[a-zA-z0-9]*$/.test(pass)) {
			cc.ll.msgbox.addMsg(`${tipName}密码必须为字母或数字！`);
			return false;
		}
		return true;
	},

	/*
	 * 点击修改密码按钮
	 */
	onChangePassClick () {
		this.edit1.string = '';
		this.edit2.string = '';
		this.edit3.string = '';
		this.edit4.string = '';
		this.node.active = true;
	},
});
