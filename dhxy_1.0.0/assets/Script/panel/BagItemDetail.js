let GameRes = require('../game/GameRes');
let GoodsMgr = require('../game/GoodsMgr');
let pComposMgr = require('../game/ComposMgr');
let PubFunction = require('../game/PubFunction');




cc.Class({
	extends: cc.Component,

	properties: {
		bgNode: cc.Node,
		itemIcon: cc.Sprite,
		typeLab: cc.Label,
		nameLab: cc.Label,
		infoLab: cc.Label,
		gongxiaoLab: cc.Label,
		delNode: cc.Node,
        operationNode: cc.Node,
		composeNode: cc.Node,
		
		ComposUI: cc.Prefab,
		PetsPanel: cc.Prefab,
		WorkShopPanel: cc.Prefab,
		RestoryUI: cc.Prefab,
		WorkShopPanel: cc.Prefab,
		WorkShopPanel: cc.Prefab,
		XianQiUpPanel: cc.Prefab,
		ShenBingCombinePanel: cc.Prefab,
		PetHeChengPanel: cc.Prefab,
		PartnerUI: cc.Prefab,
		PetsPanel: cc.Prefab,
		NoticeEditUI:cc.Prefab,
	},

	onLoad() {
        this.operationNode.active = false;
        this.composeNode.active = false;
		this.delNode.active = false;
		this.node.on(cc.Node.EventType.TOUCH_START, this.touchBegan.bind(this));
		this.typearr = ['物品', '五行天书', '材料', '药品', '经验', '银两', '仙玉', '神兵', '仙器', '召唤兽', '技能书'];
	},

	loadInfo(info) {
		this.iteminfo = info;
		this.itemIcon.spriteFrame = GameRes.getItemIcon(info.icon);
		this.typeLab.string = this.typearr[info.type];
		this.nameLab.string = info.name;
		this.infoLab.string = info.description;
		this.gongxiaoLab.string = '【功效】' + info.usedetail;
		this.gongxiaoLab.node.y = this.infoLab.node.y - this.infoLab.node.height - 5;
	},

	showOperation(){
		this.operationNode.active = true;
        this.delNode.active = true;


        if (pComposMgr.GetFatherItem(this.iteminfo.id) != 0)
            this.composeNode.active = true;
    },

    composBtnClicked()
    {
        let goComposUI = PubFunction.CreateSubNode(cc.find('Canvas/MainUI'), { nX: 0, nY: 0 }, this.ComposUI, 'ComposUI');
        this.node.destroy();
    },

	useBtnClicked(){
		this.node.destroy();
		let canuse = true;
        if ([10111, 10112, 10113, 10114, 10116, 10117, 10118].indexOf(this.iteminfo.id) != -1) { // 亲密丹,灵兽丹,圣兽丹,神兽丹,凝魂丹,龙之骨,金柳露 
			let petpanel = cc.instantiate(this.PetsPanel);
			petpanel.name = 'PetPanel';
			petpanel.parent = cc.find('Canvas/MainUI');
			canuse = false;
		}
        else if (this.iteminfo.id == 10401) {//悔梦石
			let workshop = cc.instantiate(this.WorkShopPanel);
            workshop.parent = cc.find('Canvas');
            workshop.getComponent('WorkShopPanel').setCurPanel(4);
			canuse = false;
        }

        else if (this.iteminfo.id == 10201)  //回梦丹
        {
            let goRestoryUI = PubFunction.CreateSubNode(cc.find('Canvas/MainUI'), { nX: 0, nY: 0 }, this.RestoryUI, 'RestoryUI');
            canuse = false;
        }
		else if (this.iteminfo.id >= 10402 && this.iteminfo.id <= 10404) {
			let workshop = cc.instantiate(this.WorkShopPanel);
            workshop.parent = cc.find('Canvas');
            workshop.getComponent('WorkShopPanel').setCurPanel(2);
			canuse = false;
		}
		else if (this.iteminfo.id == 10405) {//盘古精铁
			let workshop = cc.instantiate(this.WorkShopPanel);
            workshop.parent = cc.find('Canvas');
			canuse = false;
		}

		else if (this.iteminfo.id == 10406) {//八荒遗风
			let xianqi = cc.instantiate(this.XianQiUpPanel);
            xianqi.parent = cc.find('Canvas');
			canuse = false;
		}
		
		else if (this.iteminfo.id == 10408) {//神兵碎片
			let shenbing = cc.instantiate(this.ShenBingCombinePanel);
            shenbing.parent = cc.find('Canvas');
			canuse = false;
		}
		else if (this.iteminfo.id >= 20001 && this.iteminfo.id <= 20015) {//五行材料，合成召唤兽
			let hecheng = cc.instantiate(this.PetHeChengPanel);
            hecheng.parent = cc.find('Canvas');
			canuse = false;
		}
        else if (this.iteminfo.id == 10202 || this.iteminfo.id == 10204) {//伙伴修炼册
            PubFunction.CreateSubNode(cc.find('Canvas/MainUI'), { nX: 0, nY: 0 }, this.PartnerUI, 'PartnerUI');
			canuse = false;
		}
		else if (this.iteminfo.id == 10205) { // 世界铃铛广播 
			let notice_edit_node = cc.instantiate(this.NoticeEditUI);
			notice_edit_node.parent = cc.find('Canvas');
			canuse = false;
		}
		else if (this.iteminfo.id >= 10501 && this.iteminfo.id <= 10553) {	//元气丹
			let petpanel = cc.instantiate(this.PetsPanel);
			petpanel.name = 'PetPanel';
			petpanel.parent = cc.find('Canvas/MainUI');
			canuse = false;
		}
		if (!canuse) {
			let bagLayer = cc.find('Canvas/MainUI/BagPanel');
            if (bagLayer != null) {
                bagLayer.destroy();
			}
			return;
		}
		GoodsMgr.useItem(this.iteminfo.id);
	},
 
	delBtnClicked(){
		GoodsMgr.subItem(this.iteminfo.id);
		this.node.destroy();
	},

	touchBegan(event) {
		if (this.bgNode.getBoundingBoxToWorld().contains(event.getLocation())) {
			return;
		}
		this.node.runAction(cc.sequence(cc.fadeOut(0.1), cc.removeSelf()));
	},
});
