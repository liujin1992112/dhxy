let skillMgr = require('../game/Skill');

cc.Class({
	extends: cc.Component,

	properties: {
        skillitem:cc.Node,

        pSkillName:cc.Label,
        pSkillIcon:cc.Sprite,
        pSkillDetail:cc.Label,

        skillAtlas: cc.SpriteAtlas,
    },

    ctor(){
        this.select_skill = 0;
        this.cur_pet_id = 0;
    },

    start(){
        
    },

    init(){
        let bg = cc.find('skillpanel/ScrollView/view/content', this.node);
        let skilllist = skillMgr.getShenSkill();
        let height = 0;
        for (let i = 0; i < skilllist.length; i++) {
            const skill = skilllist[i];
            let sitem = cc.instantiate(this.skillitem);
            sitem.active = true;
            sitem.x = this.skillitem.x;
            sitem.y = this.skillitem.y - (this.skillitem.height + 5) * i;

            let iconnode = cc.find('/skillicon', sitem);
            let spr = iconnode.getComponent(cc.Sprite);
            spr.spriteFrame = this.skillAtlas.getSpriteFrame(skill.strIcon + '');

            let namenode = cc.find('/skillname', sitem);
            let lab = namenode.getComponent(cc.Label);
            lab.string = skill.strName;

            sitem.skillid = skill.nID;
            sitem.parent = bg;

            if(this.select_skill == 0){
                this.selectSkill(skill.nID);
            }

            height = Math.abs(sitem.y);
        }
        bg.height = height + 100;
    },

    setPetId(petid){
        this.cur_pet_id = petid;
    },

    clickSkillItem(e, d){
        let skillitem = e.target;
        this.selectSkill(skillitem.skillid);
    },

    selectSkill(skillid){
        let skill = skillMgr.GetSkillInfo(skillid);
        this.pSkillName.string = skill.strName;
        this.pSkillIcon.spriteFrame = this.skillAtlas.getSpriteFrame(skill.strIcon);
        this.pSkillDetail.string = skill.GetDetail();

        this.select_skill = skillid;
    },

    destroySelf(){
        this.node.destroy();
    },

    onStadySkill(){
        // if(true){
        //     cc.ll.msgbox.addMsg('即将开放');
        //     return;
        // }
        if(cc.ll.player.gameData.money < 50000000){
            cc.ll.msgbox.addMsg('银两不足');
            return;
        }

        cc.ll.net.send('c2s_pet_changeSskill',{
            petid:this.cur_pet_id,
            skillid:this.select_skill,
        });
    }
});