let GameRes = require('../game/GameRes');

cc.Class({
	extends: cc.Component,

	properties: {
		itemIcon: cc.Sprite,
		itemDetail: cc.Prefab,
		itemBg: cc.Node,
	},

	onLoad() {
		this.itemCount = this.node.getChildByName('itembg').getChildByName('count');
		this.selectedNode = this.node.getChildByName('selected');
		if (this.selectedNode) {
			this.selectedNode.active = false;
		}
		this.itemInfo = null;
	},

	loadInfo(info) {
		if (cc.ll.propData.item[info.itemid]) {
			this.itemInfo = cc.ll.propData.item[info.itemid];
			this.itemInfo.count = info.count;
			this.itemBg.active = true;
			if (this.itemCount) {
				this.itemCount.getComponent(cc.Label).string = info.count;
			}
			this.itemIcon.spriteFrame = GameRes.getItemIcon(this.itemInfo.icon);
		}
	},

	selected() {
		if (this.selectedNode) {
			this.selectedNode.active = true;
		}

		if (this.itemInfo != null) {
			let canvas = cc.find('Canvas');
			if (canvas.getChildByName('BagItemDetail') == null) {
				let detail = cc.instantiate(this.itemDetail);
				detail.parent = canvas;
				detail.name = 'BagItemDetail';
				detail.getComponent('BagItemDetail').loadInfo(this.itemInfo);

				let bagLayer = cc.find('MainUI/BagPanel', canvas);
				if (bagLayer != null) {
					detail.getComponent('BagItemDetail').showOperation();
				}
			}
		}
	},

	unSelected() {
		if (this.selectedNode) {
			this.selectedNode.active = false;
		}
	},

	hideCount() {
		if (this.itemCount) {
			this.itemCount.node.active = false;
		}
	},

	getItemid () {
		return this.itemInfo.itemid;
	},

	setItemCount (count) {
		this.itemInfo.count = count;
		this.itemCount.getComponent(cc.Label).string = count;
	},

	rmSelectedNode () {
		if (this.selectedNode) {
			this.selectedNode.destroy();
		}
	},
});
