/*
 * 消耗世界铃铛，编辑信息界面
 */
cc.Class({
	extends: cc.Component,

	properties: {
		edit: cc.EditBox,
	},

	onLoad () {

	},

	start () {

	},

	onConfirmClick () {
		let str = this.edit.string;
		if (str.length == 0) {
			cc.ll.msgbox.addMsg('请输入广播内容！');
			return;
		}
		cc.ll.net.send('c2s_bell_msg', {
			msg: str,
		});
		this.node.destroy();
	},

	onCancelClick () {
		this.node.destroy();
	},
});
