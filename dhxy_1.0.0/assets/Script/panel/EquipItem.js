let GameRes = require('../game/GameRes');

cc.Class({
	extends: cc.Component,

	properties: {
		itemIcon: cc.Sprite,
		itemDetail: cc.Prefab,
		schemeItemDetail: cc.Prefab,
		itemBg: cc.Node,
	},

	onLoad() {
		//this.levelNode = this.itemBg.getChildByName('level');
		this.selectedNode = this.node.getChildByName('selected');
		if (this.selectedNode) {
			this.selectedNode.active = false;
		}
	},

	reset(){
		this.itemBg.active = false;
		this.itemInfo = null;
	},

	/*
	 * type: 0,普通，1，属性方案使用
     * onUse: 属性方案中是否已装载
	 */
	loadInfo(info,type=0,onUse=false) {
		this.itemInfo = info;
		this.itemBg.active = true;
		this.itemIcon.spriteFrame = GameRes.getItemIcon(this.itemInfo.Shape);
		if (this.itemBg.getChildByName('level')) {
			this.itemBg.getChildByName('level').getComponent(cc.Label).string = this.itemInfo.Grade;
		}
		this.type = type;
		this.onUse = onUse;
	},

	selected() {
		if (this.selectedNode) {
			this.selectedNode.active = true;
		}

		if (this.itemInfo != null) {
			let canvas = cc.find('Canvas');
			let prefab = this.itemDetail;
			if(this.type == 1){
				prefab = this.schemeItemDetail;
			}
			if (canvas.getChildByName('EquipItemDetail') == null) {
				let detail = cc.instantiate(prefab);
				detail.parent = canvas;
				
				let name = 'EquipItemDetail';
				if(this.type == 1){
					name = 'SchemeEquipItemDetail';
				}
				detail.name = name;
				detail.getComponent(name).loadInfo(this.itemInfo,this.onUse);

				let schemePanel = cc.find('MainUI/SchemePanel', canvas);
				if(schemePanel != null){
					detail.getComponent(name).showOperation();
				}else{
					let bagLayer = cc.find('MainUI/BagPanel', canvas);
					if (bagLayer != null) {
						detail.getComponent(name).showOperation();
					}
				}
			}
		}
	},

	unSelected() {
		this.selectedNode.active = false;
	},
});
