let GameRes = require('../game/GameRes');
let PubFunc = require('../game/PubFunction');

cc.Class({
	extends: cc.Component,

	properties: {
		nameLab:    cc.Label,
		levelLab:   cc.Label,
		headIcon:   cc.Sprite,
		raceNodes:  [cc.Node],
	},

	onLoad() {

	},

	loadInfo(info) {
        this.id = info.roleid;
		this.nameLab.string = info.name;
		if(info.level == -1){
			this.levelLab.string = '离线';
			this.levelLab.node.color = cc.color(255,0,0);
		}else{
			this.levelLab.string = info.level + '级';
			this.levelLab.node.color = PubFunc.getReliveColor(info.relive);
		}
		if(info.race != 0){
			this.raceNodes[parseInt(info.race)-1].active = true;
		}
		this.headIcon.spriteFrame = GameRes.getRoleHead(info.resid);
	},

	onButtonClick(event, param) {
        if (this.setSearchId) {
            this.setSearchId(this.id, this.nameLab.string);
        }
	},
});

