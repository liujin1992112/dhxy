let GameRes = require('../game/GameRes');
let schemeArgs = require('./SchemeArgs');
cc.Class({
	extends: cc.Component,

	properties: {
		itemContent: cc.Node,
		equipNodes: [cc.Node],
		roleNode: cc.Node,
		bagItem: cc.Prefab,
		equipItem: cc.Prefab,
	},

	onLoad() {
		this.currentY = 0;
		this.currentSelected = -1;		
		// cc.ll.net.send('c2s_get_bagitem', { roleid: cc.ll.player.roleid });
        
        this.roleNode.getComponent('UIRole').setInfo(cc.ll.player);
	},

	start() {
		
	},

    loadInfo(data){
        this.itemNodes = [];
        this.equipList = [];
        this.curEquips = {};
        console.log('--------------------装备hello');

        this.curEquips = data.content.curEquips;
        this.loadEquips();    

        if (cc.ll.player.getItemList() == null || cc.ll.player.getItemNum() == 0) {
			cc.ll.net.send('c2s_get_bagitem', {
				roleid: cc.ll.player.roleid
			});
		} else {                    
            this.loadBagList();
		}
	},

	loadEquips() {        
        this.equipNodes.forEach(e => {
            e.getComponent('EquipItem').reset();
        });

    

        for(var key in this.curEquips){      
            let isShow = false;
            //如果当前装备被删除，不存在（既不在使用装备里，也不在背包装备里），则不显示
            for(var k in cc.ll.player.equipdata.use){
                if(cc.ll.player.equipdata.use.hasOwnProperty(k) && cc.ll.player.equipdata.use[k] == this.curEquips[key])
                    isShow = true;                    
            }
            if(cc.ll.player.equipdata.list.indexOf(this.curEquips[key]) != -1){
                isShow = true;
            }    

            if(isShow){
                let equipid = this.curEquips[key];
                let equipNode = this.equipNodes[key-1];
                if(equipNode){
                    equipNode.getComponent('EquipItem').reset();
                    if (equipid && cc.ll.player.equipdata.info[equipid]) {
                        equipNode.getChildByName('itembg').active = true;
                        equipNode.getComponent('EquipItem').loadInfo(cc.ll.player.equipdata.info[equipid],1,true);
                        
                    }
                }
            }

        }		
		
    },

    checkEquipsOnUse(equipId){
        for(var key in this.curEquips){    
            if(equipId == this.curEquips[key])
                return true;
        }

        return false;
    },

    useEquipClicked(e, d) {
		e.target.getComponent('EquipItem').selected();
	},
    

    loadBagList(){
        this.itemContent.destroyAllChildren();
        this.itemNodes = [];
                
        if(this.equipList && this.equipList.length == 0){
            if (cc.ll.player.equipdata) {		
                
                let equipBagList = cc.ll.player.equipdata.list.slice(0);
                let useList = cc.ll.player.equipdata.use
                //将已穿上的装备也加入到背包列表里
                for(var it in useList){
                    if(useList.hasOwnProperty(it)){
                        equipBagList.push(useList[it]);
                    }
                }
                //去重
                var singleEquipList=[];
                for (var i = 0; i < equipBagList.length; i++) {
                    if(equipBagList.indexOf(equipBagList[i]) == i){
                        singleEquipList.push(equipBagList[i]);
                    }
                } 

                for (const equip of singleEquipList) {
                    if (cc.ll.player.equipdata.info[equip]) {
                        this.equipList.push({
                            itemid: equip,
                            info: cc.ll.player.equipdata.info[equip],
                        });
                    }
                }
            }
        }

        
        let equipIndex = -1

        for(var key in this.curEquips){    
            equipIndex = -1;        
            this.equipList.find((e,index) => {
                if(e.itemid == this.curEquips[key]){
                    equipIndex = index;
                    return true;
                }else{
                    return false;
                }
            });

            if(equipIndex != -1){
                this.equipList.splice(equipIndex,1);
            }

        }


        this.itemContent.height = Math.ceil(this.equipList.length / 3) * 75;

		for (let index = 0; index < this.equipList.length; index++) {
			let item = null;
			if (index <  this.equipList.length) {
                item = cc.instantiate(this.equipItem);
                item.parent = this.itemContent;
                item.getComponent('EquipItem').loadInfo(this.equipList[index].info,1);				
            }
            if(item){
                item.x = -75 + (index % 3) * 75;
                item.y = -37.5 - Math.floor(index / 3) * 75;

                let btn = item.getComponent(cc.Button);
                var clickEventHandler = new cc.Component.EventHandler();
                clickEventHandler.target = this.node;
                clickEventHandler.component = "SchemeEquipsPanel";
                clickEventHandler.customEventData = index;
                clickEventHandler.handler = "bagItemClicked";
                btn.clickEvents.push(clickEventHandler);

                this.itemNodes.push(item);
            }
        }
        
    },

    updateEquipsList(){
    },

	useEquipClicked(e, d) {
		e.target.getComponent('EquipItem').selected();
	},

	bagItemClicked(e, d) {
		if (this.currentSelected == d) {
			return;
		}
		for (const item of this.itemNodes) {
			if (e.target == item) {
				continue;
			}
			item.getComponent('BagItem') && item.getComponent('BagItem').unSelected();
			item.getComponent('EquipItem') && item.getComponent('EquipItem').unSelected();
		}
    },
    

    updateEquips(data){
        this.curEquips = JSON.parse(data.curEquips);
        this.loadEquips();
        if(data.unloadEquipId != -1){
            if(cc.ll.player.equipdata.info[data.unloadEquipId]){
                this.equipList.push({
                    itemid: data.unloadEquipId,
                    info: cc.ll.player.equipdata.info[data.unloadEquipId],
                });
            }
        }
        this.loadBagList();
        
    },

    clear(){
        this.roleNode.getComponent('UIRole').clear();
    },
});
