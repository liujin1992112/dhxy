let GameRes = require('../game/GameRes');
let GameDefine = require('../game/GameDefine')
let schemeArgs = require('./SchemeArgs');

let roleName = GameDefine.roleName;

cc.Class({
	extends: cc.Component,

	properties: {
		bgNode: cc.Node,
		itemIcon: cc.Sprite,
		typeLab: cc.Label,
		nameLab: cc.Label,
		infoLab: cc.Node,

		operationNode: cc.Node,
		useNode: cc.Node,
		unloadNode: cc.Node,
		tipsNode: cc.Node,
	},

	onLoad() {
		this.operationNode.active = false;
		this.height = cc.find('Canvas').height;
		this.node.on(cc.Node.EventType.TOUCH_START, this.touchBegan.bind(this));
		this.indexarr = ['武器', '项链', '衣服', '头盔', '鞋子'];
		this.sexarr = ['男', '女'];
		this.racearr = ['人', '仙', '魔', '鬼'];
	},

	loadInfo(info) {

		let schemePanel = cc.find('Canvas/MainUI/SchemePanel');
		if(schemePanel){
			let equipsNode = schemePanel.getChildByName('schemeNode').getChildByName('EquipsNode');
			let equipsLogic = equipsNode.getComponent('SchemeEquipsPanel');
			if(equipsLogic){
				this.onUse = equipsLogic.checkEquipsOnUse(info.EquipID);
			}

		}

		this.attr1 = cc.ll.player.gameData.attr1;
		this.iteminfo = info;
		if (!info.BaseAttr) {
			cc.ll.net.send('c2s_equip_info', {
				roleid: cc.ll.player.roleid,
				equipid: info.EquipID
			});
			return;
		}
		this.itemIcon.spriteFrame = GameRes.getItemIcon(info.Shape);
		this.typeLab.string = info.EDesc;
		this.nameLab.string = info.EName;

		this.infoPos = this.infoLab.getPosition();
		this.addInfoLab(info.Detail);
		this.addInfoLab(`【装备部位】${this.indexarr[info.EIndex - 1]}`);
		if (info.Sex == 9 || info.Race == 9) {
			this.addInfoLab(`【装备适用】通用`);
		}
		else if (info.OwnerRoleId && info.OwnerRoleId > 0) {
			if (info.OwnerRoleId != cc.ll.player.resid) {
				this.addInfoLab(`【装备适用】${roleName[info.OwnerRoleId]}`, this.infoPos, true, cc.Color.RED);
				// this.useNode.getComponent(cc.Button).interactable = false;
				// this.useNode.color = cc.Color.GRAY;
			} else {
				this.addInfoLab(`【装备适用】${roleName[info.OwnerRoleId]}`);
			}
		} else {
			if (info.Sex != cc.ll.player.sex || info.Race != cc.ll.player.race) {
				this.addInfoLab(`【装备适用】${this.sexarr[info.Sex - 1]}${this.racearr[info.Race - 1]}`, this.infoPos, true, cc.Color.RED);
				// this.useNode.getComponent(cc.Button).interactable = false;
				// this.useNode.color = cc.Color.GRAY;
			} else {
				this.addInfoLab(`【装备适用】${this.sexarr[info.Sex - 1]}${this.racearr[info.Race - 1]}`);
			}
		}
		let levelstr = '【等级要求】';
		if (info.NeedGrade) {
			if (info.NeedRei) {
				levelstr += `${info.NeedRei}转`;
			}
			levelstr += `${info.NeedGrade}级`;
			if (info.NeedGrade > cc.ll.player.level || info.NeedRei > cc.ll.player.relive) {
				this.addInfoLab(levelstr, this.infoPos, true, cc.Color.RED);
				// this.useNode.getComponent(cc.Button).interactable = false;
				// this.useNode.color = cc.Color.GRAY;
			}
			else {
				this.addInfoLab(levelstr);
			}
		}

		if (info.Shuxingxuqiu && info.Shuxingxuqiu.length > 0) {
			let xuqiuInfo = JSON.parse(info.Shuxingxuqiu);
			let xuqiustr = '';
			for (const key in xuqiuInfo) {
				xuqiustr = `【${GameDefine.AttrTypeL2Str[key]}需求】${xuqiuInfo[key]}`
				if (this.attr1[key] < xuqiuInfo[key]) {
					this.addInfoLab(xuqiustr, this.infoPos, true, cc.Color.RED);
					// this.useNode.getComponent(cc.Button).interactable = false;
					// this.useNode.color = cc.Color.GRAY;
				}
				else {
					this.addInfoLab(xuqiustr);
				}
				break;
			}
		}
		this.addInfoLab(`【装备耐久】${info.MaxEndure}/${info.MaxEndure}`);
		if (!info.BaseAttr) {
			info.BaseAttr = '{}';
		}
		let baseInfo = JSON.parse(info.BaseAttr);
		for (const key in baseInfo) {
			this.addInfoLab(GameDefine.AttrTypeL1Str[key], this.infoPos, false, new cc.color(141, 135, 103));
			let valuestr = baseInfo[key];
			if (GameDefine.EquipTypeNumerical.indexOf(Number(key)) == -1) {
				valuestr = (valuestr > 0 ? '+' : '') + (valuestr / 10).toFixed(1) + '%';
			}
			else {
				valuestr = (valuestr > 0 ? '+' : '') + valuestr;
			}
			this.addInfoLab(valuestr, cc.ll.pAdd(this.infoPos, cc.v2(120, 0)), true, new cc.color(141, 135, 103));
		}

		if (!info.LianhuaAttr) {
			info.LianhuaAttr = '{}';
		}
		let lianhuaInfo = JSON.parse(info.LianhuaAttr);
		if (Array.isArray(lianhuaInfo)) {
			for (const info of lianhuaInfo) {
				for (const key in info) {
					this.addInfoLab(GameDefine.AttrTypeL1Str[key], this.infoPos, false, new cc.color(46, 143, 9));
					let valuestr = info[key];
					if (GameDefine.EquipTypeNumerical.indexOf(Number(key)) == -1) {
						valuestr = (valuestr > 0 ? '+' : '') + (valuestr / 10).toFixed(1) + '%';
					}
					else {
						valuestr = (valuestr > 0 ? '+' : '') + valuestr;
					}
					this.addInfoLab(valuestr, cc.ll.pAdd(this.infoPos, cc.v2(120, 0)), true, new cc.color(46, 143, 9));
					break;
				}
			}
		}

		//if (cc.ll.player.equipdata.use[this.iteminfo.EIndex] && this.iteminfo.EquipID && cc.ll.player.equipdata.use[this.iteminfo.EIndex] == this.iteminfo.EquipID) {
		if(this.onUse){
			this.unloadNode.active = true;
			this.useNode.active = false;
			this.tipsNode.active = true;
		}
		else {
			this.unloadNode.active = false;
			this.useNode.active = true;
			this.tipsNode.active = false;
		}
		this.showGemInfo(info.GemCnt, info.EIndex);
	},

	showGemInfo() {
		let gemX = this.bgNode.getChildByName('gem').x;
		let gemY = this.bgNode.getChildByName('gem').y;
		for (let index = 0; index < this.iteminfo.MaxEmbedGemCnt; index++) {
			let gem = cc.instantiate(this.bgNode.getChildByName('gem'));
			gem.active = true;
			gem.x = gemX + index * 15;
			gem.y = gemY;
			gem.parent = this.bgNode;
			if (index < this.iteminfo.GemCnt) {
				gem.getChildByName(`gem${this.iteminfo.EIndex}`).active = true;
			}
		}
	},

	showOperation(onUse) {
		this.operationNode.active = true;
	},

	addInfoLab(info, pos, newline = true, color = cc.Color.WHITE) {//, color){
		if (!pos && pos == null) {
			pos = this.infoPos;
		}
		let detail = cc.instantiate(this.infoLab);
		detail.active = true;
		detail.parent = this.bgNode;
		detail.setPosition(pos);
		detail.color = color;
		detail.getComponent(cc.Label).string = info;
		if (newline) {
			this.infoPos.y -= (detail.height + 4);
		}
		if (-this.infoPos.y > this.bgNode.height) {
			this.bgNode.height = -this.infoPos.y;
			if (this.bgNode.y + this.infoPos.y < -this.height / 2) {
				this.bgNode.y = -this.height / 2 - this.infoPos.y;
			}
		}
	},

	useBtnClicked() {
		cc.ll.net.send('c2s_scheme_updateEquip', {
			roleId: cc.ll.player.roleid,
			schemeId: schemeArgs.schemeId,
			equipId: this.iteminfo.EquipID,			
			type: 0
		});
		this.node.destroy();
	},

	unloadBtnClicked() {
		cc.ll.net.send('c2s_scheme_updateEquip', {
			roleId: cc.ll.player.roleid,			
			schemeId: schemeArgs.schemeId,
			equipId: this.iteminfo.EquipID,
			type:1
		});
		this.node.destroy();
	},
	

	touchBegan(event) {
		if (this.bgNode.getBoundingBoxToWorld().contains(event.getLocation())) {
			return;
		}
		this.node.runAction(cc.sequence(cc.fadeOut(0.1), cc.removeSelf()));
	},
});
