let GameRes = require('../game/GameRes');
let PubFunc = require('../game/PubFunction');

cc.Class({
	extends: cc.Component,

	properties: {
		nameLab: cc.Label,
		level1Lab: cc.Label,
		level2Lab: cc.Label,
		headIcon: cc.Sprite,
	},

	onLoad() {
	},

	loadInfo(info, type, logic) {
		this.addLogic = logic;
		this.roleInfo = info;
		if (type == 0) {
			cc.find('sureBtn', this.node).active = false;
			cc.find('rejectBtn', this.node).active = false;
			cc.find('levelLabel', this.node).active = false;
		}
		else if (type == 1) {
			cc.find('role/levelBack', this.node).active = false;
			cc.find('sureBtn', this.node).active = false;
			cc.find('rejectBtn', this.node).active = false;
			cc.find('levelSign', this.node).active = false;
		}
		else if (type == 2) {
			cc.find('role/levelBack', this.node).active = false;
			cc.find('addBtn', this.node).active = false;
			cc.find('levelSign', this.node).active = false;
		}
		this.nameLab.string = info.name;
		this.level1Lab.string = info.level;
		this.level2Lab.string = info.level + '级';
		this.level1Lab.node.color = PubFunc.getReliveColor(info.relive);
		this.level2Lab.node.color = PubFunc.getReliveColor(info.relive);
		this.headIcon.spriteFrame = GameRes.getRoleHead(info.resid);
	},

	onAddBtnClicked(e, d) {
		cc.ll.net.send('c2s_add_friend', {
			roleid: this.roleInfo.roleid
		});
		this.addLogic.delItem(this.roleInfo.roleid);
	},

	onRejectBtnClicked(e, d) {
		cc.ll.net.send('c2s_update_friends', {
			roleid: this.roleInfo.roleid,
			operation: 2
		});
	},

	onAgreeBtnClicked(e, d) {
		cc.ll.net.send('c2s_update_friends', {
			roleid: this.roleInfo.roleid,
			operation: 1
		});
	},
});
