cc.Class({
	extends: cc.Component,

	properties: {
		SchemeListPanel: cc.Node,
		SettingPanel: cc.Node,

		EquipPanel: cc.Node,
	},

	onLoad() {
	},

	start() {
		cc.ll.player.send('c2s_scheme_List', {
			roleId: cc.ll.player.roleid,
		});
		cc.ll.net.send('c2s_ask_partner_list', { nRoleID: cc.ll.player.roleid });
	},

	onCloseBtnClicked(e, d) {
		cc.ll.AudioMgr.playCloseAudio();
		this.EquipPanel.getComponent('SchemeEquipsPanel').clear();
		this.node.destroy();
	},

	changeFuncPanel(e, d) {
		if (d == 1) {
		} else if (d == 2) {

		} else if (d == 3) {

		}
	}
});
