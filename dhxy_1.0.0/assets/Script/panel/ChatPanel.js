let GoodsMgr = require('../game/GoodsMgr');
let GameRes = require('../game/GameRes');
let CPubFunction = require('../game/PubFunction');

let zhufuyu = '大哥大嫂新年好！';

/**
 * 聊天面板逻辑处理
 * 聊天频道(0:世界 1:队伍 2:帮会 3:系统 4:祝福)
 */
cc.Class({
	extends: cc.Component,

	properties: {
		/** 对方的聊天消息Item */
		chatItemOther: cc.Prefab,
		/**自己的聊天消息Item */
		chatItemSelf: cc.Prefab,
		/** 滚动列表的Content区域节点 */
		contentNode: cc.Node,
		/** 聊天输入框 */
		editbox: cc.EditBox,

		bgNode: cc.Node,
		progressNode: cc.Node,
		joinBangNode: cc.Node,

		/** 聊天消息框输入的根节点 */
		inputNode: cc.Node,
		preBang: cc.Prefab,

		emojiContent: cc.Node,
		/** 表情图集资源 */
		emojiAtlas: cc.SpriteAtlas,
		GmUI: cc.Prefab,
		btnGmButton: cc.Prefab,

		autoPlayToggle: cc.Toggle,
		voiceBtn: cc.Node,
	},

	onLoad() {
		// this.node.on(cc.Node.EventType.TOUCH_START, this.touchBegan.bind(this));
		this.teamItemList = [];//队伍消息
		this.campItemList = [];//帮会消息
		this.worldItemList = [];//世界消息
		this.sysItemList = [];//系统消息
		this.maxItems = 50;
		this.curItemList = this.worldItemList;
		this.curChatScale = 0;//当前聊天频道
		this.chatItemNodes = [];//所有聊天信息节点
		this.bgOpacity = 255;
		this.isTransparent = false;
		this.opacityProgress = this.progressNode.getComponent(cc.ProgressBar);
		this.autoPlayToggle.isChecked = cc.ll.auto_world_voice == 1;
		this.isEmojiShow = false;
		let eCnt = 0;
		for (let index = 0; index < 179; index++) {
			let emojiFrame = this.emojiAtlas.getSpriteFrame(('000' + index).substr(-3) + '0000');
			if (emojiFrame == null) {
				continue;
			}
			let biaoqing = new cc.Node();
			biaoqing.scale = 1;
			biaoqing.parent = this.emojiContent;
			biaoqing.setPosition(cc.v2(30 + 60 * (Math.floor(eCnt / 3)), 60 - 60 * Math.floor(eCnt % 3)));
			biaoqing.addComponent(cc.Sprite).spriteFrame = emojiFrame; // this.emojiAtlas.getSpriteFrame(`emoji_${index + 1}_1`);
			let btn = biaoqing.addComponent(cc.Button);
			btn.transition = cc.Button.Transition.SCALE;
			var clickEventHandler = new cc.Component.EventHandler();
			clickEventHandler.target = this.node;
			clickEventHandler.component = "ChatPanel";
			clickEventHandler.handler = "onBiaoqingItemClicked";
			clickEventHandler.customEventData = index;
			btn.clickEvents.push(clickEventHandler);
			if (30 + 60 * (Math.floor(eCnt / 3) + 1) > this.emojiContent.width) {
				this.emojiContent.width = 30 + 60 * (Math.floor(eCnt / 3) + 1);
			}
			eCnt++;
		}
		this.name = 'ChatPanel';
		this.vecGmBtn = [];
	},

	start() {
		let mainUILogic = cc.find('Canvas/MainUI').getComponent('MainUI');
		if (mainUILogic) {
			this.voiceBtn.on(cc.Node.EventType.TOUCH_START, mainUILogic.onWorldVoiceRecord.bind(this));
			this.voiceBtn.on(cc.Node.EventType.TOUCH_CANCEL, mainUILogic.onWorldVoiceRecord.bind(this));
			this.voiceBtn.on(cc.Node.EventType.TOUCH_END, mainUILogic.onWorldVoiceRecord.bind(this));
		}
	},

	onBiaoqingItemClicked(e, d) {
		this.editbox.string += `[${d}]`;
	},

	/**
	 * 隐藏表情面板
	 */
	hideEmoji() {
		if (this.isEmojiShow) {
			this.isEmojiShow = false;
			this.node.stopAllActions();
			this.node.runAction(cc.moveTo(0.1, cc.v2(this.node.x, 0)));
		}
	},

	editingDidBegan(e, d) {
		this.hideEmoji();
	},

	loadListInfo(list) {
		for (const info of list) {
			if (info.scale == 0) {
				this.worldItemList.push(info);
			} else if (info.scale == 1) {
				this.teamItemList.push(info);
			} else if (info.scale == 2) {
				this.campItemList.push(info);
			} else if (info.scale == 3) {
				this.sysItemList.push(info);
			}
		}
		this.showListInfo();
	},

	/**
	 * 添加消息
	 * @param {*} info 
	 * @returns 
	 */
	addListInfo(info) {
		if (info.scale == 0) {//世界消息
			this.worldItemList.push(info);
		} else if (info.scale == 1) {//队伍消息
			this.teamItemList.push(info);
		} else if (info.scale == 2) {//帮会消息
			this.campItemList.push(info);
		} else if (info.scale == 3) {//系统消息
			this.sysItemList.push(info);
		}
		if (info.scale == 5) {//祝福消息
			for (let i = 0; i < this.worldItemList.length; i++) {
				const chatinfo = this.worldItemList[i];
				if (chatinfo.roleid == info.roleid) {
					chatinfo.msg = zhufuyu;
				}
			}
			for (let i = 0; i < this.campItemList.length; i++) {
				const chatinfo = this.campItemList[i];
				if (chatinfo.roleid == info.roleid) {
					chatinfo.msg = zhufuyu;
				}
			}
			for (let i = 0; i < this.teamItemList.length; i++) {
				const chatinfo = this.teamItemList[i];
				if (chatinfo.roleid == info.roleid) {
					chatinfo.msg = zhufuyu;
				}
			}
			this.fixChatItemNode(info.roleid);
			return;
		}

		//当前消息属于当前选择的频道,则添加消息到滚动列表中
		if (this.curChatScale == info.scale) {
			this.addChatItem(info);
		}

		if (this.curItemList.length > this.maxItems) {
			this.delFirstChatItem();
		}
		if (this.worldItemList.length > this.maxItems) {
			this.worldItemList.shift();
		}
		if (this.teamItemList.length > this.maxItems) {
			this.teamItemList.shift();
		}
		if (this.campItemList.length > this.maxItems) {
			this.campItemList.shift();
		}
		if (this.sysItemList.length > this.maxItems) {
			this.sysItemList.shift();
		}
	},

	showListInfo() {
		this.inputNode.active = true;
		this.joinBangNode.active = false;
		this.contentNode.destroyAllChildren();
		this.chatItemNodes = [];
		this.contentNode.height = this.contentNode.parent.height;
		// for (const info of this.curItemList) {
		// 	this.addChatItem(info);
		// }
		if (this.curItemList.length > 0) {
			let index = 0;
			this.schedule(() => {
				this.addChatItem(this.curItemList[index]);
				let id = cc.ll.voicemgr.getPlayingId();
				if (id != -1 && this.curItemList[index].voice == id) {
					let node = this.chatItemNodes[this.chatItemNodes.length - 1];
					let logic = node.getComponent('ChatItem');
					logic.playVoiceAct();
				}
				if (index == this.curItemList.length - 1) {

				}
				index++;
			}, 0.01, this.curItemList.length);
		}
	},

	delFirstChatItem() {
		let delItem = this.chatItemNodes.shift();
		if (delItem) {
			this.contentNode.height -= delItem.height;
			for (const item of this.chatItemNodes) {
				item.y += delItem.height;
			}
			delItem.destroy();
		}
	},

	/**
	 * 添加消息Item到滚动列表中
	 * @param {*} info 
	 * @returns 
	 */
	addChatItem(info) {
		if (info == null) {
			return;
		}
		let chatItem = null;
		if (this.curChatScale == 3) {//系统消息
			//采用富文本框
			chatItem = new cc.Node();
			chatItem.parent = this.contentNode;
			let richText = chatItem.addComponent('CustomRichText');
			richText.maxWidth = this.contentNode.width - 6;
			richText.fontSize = 18;
			richText.lineHeight = 20;
			richText.type = 1;
			richText.rolename = info.name;
			richText.scale = info.scale;
			richText.emojiAtlas = this.emojiAtlas;
			richText.string = info.msg;
			chatItem.x = -this.contentNode.width / 2 + 3;
			chatItem.color = cc.color(200, 0, 0, 255);
		} else {
			if (info.roleid == cc.ll.player.roleid) {//自己消息
				chatItem = cc.instantiate(this.chatItemSelf);
			} else {
				chatItem = cc.instantiate(this.chatItemOther);//对方消息,自己可以点击
				cc.find('HeadSprite', chatItem).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "ChatPanel", "OnClickSpeaker", info));
			}
			chatItem.parent = this.contentNode;
			chatItem.getComponent('ChatItem').loadInfo(info);
			chatItem.x = 0;
			chatItem.roleid = info.roleid;
			chatItem.voiceid = info.voice;
		}

		//聊天记录从上面添加
		chatItem.y = 0;
		if (this.chatItemNodes.length > 0) {
			let preItem = this.chatItemNodes[this.chatItemNodes.length - 1];
			chatItem.y = preItem.y - preItem.height;
		}
		if (chatItem.y - chatItem.height < -this.contentNode.height) {
			this.contentNode.height = chatItem.height - chatItem.y;
		}

		/**
		 * 聊天记录从下面开始显示,不要删 
		 */

		// let blankHeight = this.contentNode.height;
		// if (this.chatItemNodes[0]) {
		// 	blankHeight = -this.chatItemNodes[0].y;
		// }
		// if (chatItem.height > blankHeight) {
		// 	this.contentNode.height += chatItem.height - blankHeight;
		// 	for (const item of this.chatItemNodes) {
		// 		item.y += blankHeight;
		// 	}
		// }
		// else {
		// 	for (const item of this.chatItemNodes) {
		// 		item.y += chatItem.height;
		// 	}
		// }
		// chatItem.y = -this.contentNode.height + chatItem.height;

		this.chatItemNodes.push(chatItem);
		if (this.contentNode.y > this.contentNode.height - this.contentNode.parent.height - chatItem.height - 20) {
			this.contentNode.y = this.contentNode.height - this.contentNode.parent.height;
		}
	},

	fixChatItemNode(roleid) {
		for (const chatitem of this.chatItemNodes) {
			if (chatitem.roleid == roleid) {
				chatitem.getComponent('ChatItem').setMsg(zhufuyu);
			}
		}
	},

	/**
	 * 点击对方的消息Item
	 * @param {*} e 
	 * @param {*} info 聊天信息数据
	 */
	OnClickSpeaker(e, info) {
		CPubFunction.DestroyVecNode(this.vecGmBtn);

		let goGmUI = CPubFunction.CreateSubNode(this.node, {
			nX: 0,
			nY: 0
		}, this.GmUI, 'GmUI');
		let goContent = cc.find('ScrollView/view/content', goGmUI);
		cc.find('btnClose', goGmUI).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "ChatPanel", 'CloseGmUI', info));

		cc.find('nodPlayerInfo/picPlayerAvater', goGmUI).getComponent(cc.Sprite).spriteFrame = GameRes.getRoleHead(info.resid);
		cc.find('nodPlayerInfo/labID', goGmUI).getComponent(cc.Label).string = info.roleid;
		cc.find('nodPlayerInfo/labName', goGmUI).getComponent(cc.Label).string = info.name;

		let vecFun = [{
			name: '加入队伍',
			fun: 'ApplyJoint'
		}, {
			name: '添加好友',
			fun: 'AddFriend'
		}, {
			name: '禁言',
			fun: 'ShutUp'
		}, {
			name: '封号',
			fun: 'KickOff'
		}, {
			name: '封IP',
			fun: 'FreezeIP'
		}, {
			name: '封设备',
			fun: 'FreezeMAC'
		}];

		for (var it in vecFun) {
			if (cc.ll.player.gmlevel < 1 && vecFun[it].name == '禁言')
				continue;

			if (cc.ll.player.gmlevel < 1 && vecFun[it].name == '封号')
				continue;

			if (cc.ll.player.gmlevel < 15 && vecFun[it].name == '封IP')
				continue;

			if (cc.ll.player.gmlevel < 30 && vecFun[it].name == '封设备')
				continue;

			if ((cc.ll.player.teamid > 0 || info.teamid == 0) && vecFun[it].name == '加入队伍')
				continue;

			let btnGmButton = CPubFunction.CreateSubNode(goContent, {
				nX: 0,
				nY: 0 - this.vecGmBtn.length * 55
			}, this.btnGmButton, 'btnGmButton');
			cc.find('Label', btnGmButton).getComponent(cc.Label).string = vecFun[it].name;
			btnGmButton.getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "ChatPanel", vecFun[it].fun, info));
			this.vecGmBtn.push(btnGmButton);
		}

		goContent.height = Math.max(goContent.height, this.vecGmBtn.length * 60 + 10);
	},

	//zfy  请求 红包列表
	onworldBtnClick() {
		// //打开UI
		cc.ll.net.send('c2s_world_reward_list');
		this.hideEmoji();
	},


	CloseGmUI(e, d) {
		CPubFunction.FindAndDeleteNode(this.node, 'GmUI');
	},

	ShutUp(e, info) {
		cc.ll.net.send('c2s_player_shutup', {
			nRoleID: info.roleid
		});
		this.CloseGmUI(0, 0);
	},

	KickOff(e, info) {
		cc.ll.net.send('c2s_kick_off', {
			nRoleID: info.roleid
		});
		this.CloseGmUI(0, 0);
	},

	FreezeIP(e, info) {
		cc.ll.net.send('c2s_freeze_ip', {
			nRoleID: info.roleid
		});
		this.CloseGmUI(0, 0);
	},

	FreezeMAC(e, info) {
		cc.ll.net.send('c2s_freeze_mac', {
			nRoleID: info.roleid
		});
		this.CloseGmUI(0, 0);
	},

	ApplyJoint(e, info) {
		cc.ll.net.send('c2s_requst_team', {
			roleid: cc.ll.player.roleid,
			teamid: info.teamid
		});
		this.CloseGmUI(0, 0);
	},

	AddFriend(e, info) {
		cc.ll.net.send('c2s_add_friend', {
			roleid: info.roleid
		});
		this.CloseGmUI(0, 0);
	},

	onToggleClicked(e, param) {
		this.contentNode.parent.height = 500;

		this.autoPlayToggle.node.active = false;

		if (param == 'team') {
			this.curChatScale = 1;
			this.curItemList = this.teamItemList;
			this.showListInfo();
		} else if (param == 'world') {
			this.curChatScale = 0;
			this.curItemList = this.worldItemList;
			this.showListInfo();
			this.autoPlayToggle.node.active = true;
		} else if (param == 'camp') {
			this.curChatScale = 2;
			this.curItemList = this.campItemList;
			this.showListInfo();
			if (cc.ll.player.bangid == 0) {
				this.joinBangNode.active = true;
				this.inputNode.active = false;
				return;
			}
		} else if (param == 'system') {
			this.curChatScale = 3;
			this.curItemList = this.sysItemList;
			this.contentNode.parent.height = 560;
			this.showListInfo();
			this.inputNode.active = false;
		} else {
			this.curChatScale = -1;
			this.curItemList = [];
			this.showListInfo();
			this.inputNode.active = false;
		}
	},

	onButtonClick(event, param) {
		console.log(param);
		if (param == 'close') {
			this.node.destroy();//点击关闭,销毁聊天弹窗
		} else if (param == 'emoji') {//弹窗表情弹窗
			if (!this.isEmojiShow) {
				this.isEmojiShow = true;
				this.node.stopAllActions();
				this.node.runAction(cc.moveTo(0.1, cc.v2(this.node.x, 250)));
			}
		} else if (param == 'send') {//点击发送消息
			if (this.editbox.string.length == 0) {
				return;
			}
			let str = this.editbox.string;
			if (str.substr(0, 1) == ':') {
				if (str.substr(1, 1) == '+') {
					GoodsMgr.addItem(str.substr(2));
				} else {
					GoodsMgr.subItem(str.substr(2));
				}
			}
			this.hideEmoji();//隐藏表情面板
			let msg = this.editbox.string;

			if (msg[0] == '/') {
				let str = msg.substr(1, msg.length);
				let command = str.split(',');
				cc.ll.net.send('gm_command', {
					commands: command
				});
				this.node.destroy();
				return;
			}

			// let flitlist = GameDefine.limitWordList;
			// for (let i = 0; i < flitlist.length; i++) {
			// 	const fword = flitlist[i];
			// 	if (msg.indexOf(fword) != -1) {
			// 		msg = msg.replace(new RegExp(fword, 'g'), '*');
			// 	}
			// }

			//发送聊天消息{scale:频道,msg:消息内容}
			cc.ll.net.send('c2s_game_chat', {
				scale: this.curChatScale,
				msg: msg,
			});
			this.editbox.string = '';
		} else if (param == 'opacity') {
			this.isTransparent = !this.isTransparent;
			if (!this.isTransparent) {
				cc.find('handleNode/handle', this.progressNode).active = false;
			} else if (this.bgNode.opacity <= 155) {
				cc.find('handleNode/handle', this.progressNode).active = true;
			}
		} else if (param == 'joinbang') {
			if (cc.find('Canvas').getChildByName('BangPanel')) {
				cc.find('Canvas').getChildByName('BangPanel').destroy();
			}
			let bang = cc.instantiate(this.preBang);
			bang.name = 'BangPanel';
			bang.parent = cc.find('Canvas');
			let bangid = cc.ll.player.bangid;
			if (bangid > 0) {
				bang.getComponent('BangPanel').showBangLayer();
			} else {
				bang.getComponent('BangPanel').showAddlayer();
			}
			this.node.destroy();
		}
	},

	update() {
		if (this.isTransparent && this.bgNode.opacity > 155) {
			this.bgNode.opacity -= 5;
			if (this.bgNode.opacity <= 155) {
				this.bgNode.opacity = 155;
				cc.find('handleNode/handle', this.progressNode).active = true;
			}
			this.opacityProgress.progress = (255 - this.bgNode.opacity) / 100;
			cc.find('handleNode', this.progressNode).y = 3 + 0.9 * this.progressNode.height * this.opacityProgress.progress;
		} else if (!this.isTransparent && this.bgNode.opacity < 255) {
			this.bgNode.opacity += 5;
			if (this.bgNode.opacity >= 255) {
				this.bgNode.opacity = 255;
			}
			this.opacityProgress.progress = (255 - this.bgNode.opacity) / 100;
			cc.find('handleNode', this.progressNode).y = 3 + 0.9 * this.progressNode.height * this.opacityProgress.progress;
		}
	},

	touchBegan(event) {
		// this.hideEmoji();
	},

	onAutoPlayWorldToggle(e, d) {
		cc.ll.auto_world_voice = this.autoPlayToggle.isChecked ? 1 : 0;
		cc.sys.localStorage.setItem('auto_play_world', cc.ll.auto_world_voice);
	},

	changeVoicePlayedId(id) {
		for (const chatitem of this.chatItemNodes) {
			if (chatitem.voiceid == id) {
				chatitem.getComponent('ChatItem').playVoiceAct();
				break;
			}
		}
	}
});