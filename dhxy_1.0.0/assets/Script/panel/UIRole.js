cc.Class({
    extends: cc.Component,
    properties: {
        animationNode: cc.Node,
        nameNode: cc.Label,
        weaponNode: cc.Node
    },

    onLoad() {
        this.node.on(cc.Node.EventType.TOUCH_START, this.touchBegan.bind(this));
    },

    setInfo(objinfo, nameshow = false) {
        this.resid = objinfo.resid;
        this.nameNode.string = objinfo.name;
        this.nameNode.node.active = nameshow;
        this.weapon = objinfo.weapon;
        this.petColor = objinfo.petcolor;
        if (cc.ll.propData.resanchor[this.resid]) {
            this.animationNode.anchorY = cc.ll.propData.resanchor[this.resid].anchorY;
        }

        //设置宠物颜色
        let colorUtil = this.node.getComponent('pet_color');
        if (colorUtil) {
            if (this.petColor != 0 && this.petColor != -1) {
                colorUtil.dH = this.petColor;
            } else {
                colorUtil.dH = 0;
            }
        }
        this.animationNode.opacity = 0;
        this.playAnimation('stand');

        this.addWeaponAnimation('stand', 1);
    },

    getWeaponName(equip) {
        let weaponname = '';
        if (equip.type == 0) {
            weaponname = '0000';
        }//新手装备
        if (equip.type == 1) {//高级装备
            weaponname = '0100';
            if (equip.level == 100 && equip.gemcnt >= 10) {//100级打满宝石
                weaponname = '0101';
            }
            else if (equip.level == 120) {//120级
                weaponname = '0200';
                if (equip.gemcnt >= 12) {//打满宝石
                    weaponname = '0202';
                }
            }
        } else if (equip.type == 2) {
            weaponname = '0300';
            if (equip.gemcnt >= 13) {
                weaponname = '0302';
            }
            else if (equip.gemcnt >= 10) {
                weaponname = '0301';
            }
        } else if (equip.type == 3) {
            weaponname = '0400';
            if (equip.gemcnt >= 16) {
                weaponname = '0501';
            }
            else if (equip.gemcnt >= 13) {
                weaponname = '0403';
            }
            else if (equip.gemcnt >= 10) {
                weaponname = '0401';
            }
        }
        return weaponname;
    },

    addWeaponAnimation(act, dir) {
        if (!this.weaponNode) {
            return;
        }
        this.weaponNode.active = false;
        if (this.weapon && this.weapon.length > 0) {
            this.weaponname = this.getWeaponName(JSON.parse(this.weapon));
        }
        else {
            this.weaponname = '0000';
        }
        if (this.weaponname == '') {
            return;
        }
        let self = this;
        cc.loader.loadRes(`weapon/${this.resid}/${this.weaponname}_${act}_${dir}_wp`, cc.SpriteAtlas, function (err, atlas) {
            let curFrames = [];
            for (let i = 1; ; i++) {
                let frame = atlas.getSpriteFrame(i);
                if (frame) curFrames.push(frame);
                else break;
            }
            let curClip = cc.AnimationClip.createWithSpriteFrames(curFrames, 10 / 12 * curFrames.length);
            curClip.name = act;
            if (act == 'stand' || act == 'run') {
                curClip.wrapMode = cc.WrapMode.Loop;
            }
            else {
                curClip.wrapMode = cc.WrapMode.Normal;
            }

            let nodeAni = self.weaponNode.getComponent(cc.Animation);
            nodeAni.addClip(curClip);
            nodeAni.play(act);

            if (cc.ll.propData.resanchor[self.resid]) {
                self.weaponNode.anchorY = cc.ll.propData.resanchor[self.resid].anchorY;
            }

            self.weaponNode.active = true;
            self.scheduleOnce(() => {
                self.weaponNode.scaleX = self.animationNode.width / self.weaponNode.width;//preScaleX > 0 ? 500 / self.weaponNode.width : -500 / self.weaponNode.width;
                self.weaponNode.scaleY = self.animationNode.height / self.weaponNode.height;
            }, 0);
        });
    },

    playAnimation(name) {
        // let clips = this.animationNode.getComponent(cc.Animation).getClips();
        // for (const clip of clips) {
        //     if (clip.name == name) {
        //         this.animationNode.getComponent(cc.Animation).play(name);
        //         return;
        //     }
        // }
        let self = this;
        cc.loader.loadRes(`shap/${this.resid}/${name}_1`, cc.SpriteAtlas, function (err, atlas) {
            let curFrames = [];
            for (let i = 1; ; i++) {
                let frame = atlas.getSpriteFrame(i);
                if (frame) curFrames.push(frame);
                else break;
            }
            let curClip = cc.AnimationClip.createWithSpriteFrames(curFrames, 10 / 12 * curFrames.length);
            curClip.name = name;
            curClip.wrapMode = cc.WrapMode.Loop;

            let nodeAni = self.animationNode.getComponent(cc.Animation);
            nodeAni.addClip(curClip);
            nodeAni.play(name);

            self.scheduleOnce(() => {
                self.animationNode.scaleX = 500 / self.animationNode.width;
                self.animationNode.scaleY = 500 / self.animationNode.height;
                self.animationNode.opacity = 255;
            }, 0);
        });
    },

    clear(){
        let weaponNode = this.weaponNode;
        if (weaponNode) {
            let clips = weaponNode.getComponent(cc.Animation).getClips();
            for (const clip of clips) {
                weaponNode.getComponent(cc.Animation).stop(clip.name);
                weaponNode.getComponent(cc.Animation).removeClip(clip, true);
            }
        }

        let animationNode = this.animationNode;
        if (animationNode) {
            let clips = animationNode.getComponent(cc.Animation).getClips();
            for (const clip of clips) {
                animationNode.getComponent(cc.Animation).stop(clip.name);
                animationNode.getComponent(cc.Animation).removeClip(clip, true);
            }
        }
    },

    

    touchBegan(event) {
        this.playAnimation('run');
        this.addWeaponAnimation('run', 1);
        this.node.stopAllActions();
        this.node.runAction(cc.sequence(cc.delayTime(10), cc.callFunc(() => {
            this.playAnimation('stand');
            this.addWeaponAnimation('stand', 1);
        })));
    },
});
