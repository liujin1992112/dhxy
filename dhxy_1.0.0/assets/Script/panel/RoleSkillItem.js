let SkillMgr = require('../game/Skill');
let ExpMgr = require('../game/ExpMgr');

cc.Class({
	extends: cc.Component,

	properties: {
		itemIcon: cc.Sprite,
		progressLab: cc.Label,
		selectedNode: cc.Node,
		progressBar: cc.ProgressBar,

		SkillAtlas: cc.SpriteAtlas,
	},

	onLoad() {
		// this.selectedNode.active = false;
	},

	loadInfo(skillid, curexp) {
		this.skillid = skillid;
		if (curexp == null || curexp == 0) {
			curexp = 1;
		}
		this.skillLogic = SkillMgr.GetSkillInfo(skillid);
		this.skillLogic.nCurExp = curexp;
		this.skillLogic.nMaxExp = ExpMgr.GetMaxSkillLevel(cc.ll.player.relive);
		this.itemIcon.spriteFrame = this.SkillAtlas.getSpriteFrame(this.skillLogic.strIcon);
		this.progressBar.progress = this.skillLogic.nCurExp / this.skillLogic.nMaxExp;
		this.progressLab.string = this.skillLogic.nCurExp + '/' + this.skillLogic.nMaxExp;
	},

	upgradeExp() {
		if (this.skillLogic.nCurExp >= this.skillLogic.nMaxExp) {
			return;
		}
		if (this.skillLogic.nCurExp == 1) {
			this.skillLogic.nCurExp = 100;
		}
		else{
			this.skillLogic.nCurExp += 100;
		}
		// if (this.skillLogic.nCurExp == this.skillLogic.nMaxExp) {
		// 	this.skillLogic.nCurExp -= 1;
		// }
		this.progressBar.progress = this.skillLogic.nCurExp / this.skillLogic.nMaxExp;
		this.progressLab.string = this.skillLogic.nCurExp + '/' + this.skillLogic.nMaxExp;

	},

	selected() {
		this.selectedNode.active = true;
	},

	unSelected() {
		this.selectedNode.active = false;
	}
});
