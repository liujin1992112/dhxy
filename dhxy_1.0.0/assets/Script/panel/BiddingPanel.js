
cc.Class({
    extends: cc.Component,

    properties: {
        txt: cc.Label,
    },

    start () {
        this.curtxt = 1000;
        this.step = 1000;
    },

    onReset(){
        this.curtxt = 1000;
        this.txt.string = this.curtxt;
    },

    onAdd(){
        this.curtxt += this.step;
        if(this.curtxt > 10000){
            this.curtxt = 10000;
        }
        this.txt.string = this.curtxt;
    },

    onBidding(){
        cc.ll.net.send('c2s_bang_bid', {money : this.curtxt});
        this.node.active = false;
    },

    // update (dt) {},
});
