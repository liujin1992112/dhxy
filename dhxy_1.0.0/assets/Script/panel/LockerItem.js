let GameRes = require('../game/GameRes');

cc.Class({
	extends: cc.Component,

	properties: {
		itemBg: cc.Node,
		itemIcon: cc.Sprite,
		itemCount: cc.Label,
		selectedNode: cc.Node,

		itemDetail: cc.Prefab,

		EquipItemDetail:cc.Prefab,
	},

	onLoad() {
		this.node.on(cc.Node.EventType.TOUCH_START, this.onTouched.bind(this));
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this.onTouched.bind(this));
		this.node.on(cc.Node.EventType.TOUCH_END, this.onTouched.bind(this));
		this.node.on(cc.Node.EventType.TOUCH_CANCEL, this.onTouched.bind(this));
        this.onTouch = false;
		this.panelLogic = null;
		this.type = 0;
		this.isequip = false;
		this.count = 0;
		
		this.itemBg.active = false;
		this.selectedNode.active = false;
		this.itemInfo = null;
	},

	loadInfo(itemid, itemcount) {
		if (cc.ll.propData.item[itemid]) {
			this.count = itemcount;
			this.itemInfo = cc.ll.propData.item[itemid];
			this.itemInfo.count = itemcount;
			this.itemBg.active = true;
			this.itemCount.string = itemcount;
			this.itemIcon.spriteFrame = GameRes.getItemIcon(this.itemInfo.icon);
		}
	},

	loadEquipInfo(info){
		this.itemBg.active = true;
		this.itemInfo = info;
		this.itemIcon.spriteFrame = GameRes.getItemIcon(info.Shape);
		this.itemCount.string = info.Grade;
	},

	selected(){
		this.selectedNode.active = true;
	},

	unSelected(){
		this.selectedNode.active = false;
	},

    onTouched(e) {
        if (e.type == cc.Node.EventType.TOUCH_START) {
            this.onTouch = true;
            this.beginPos = e.getLocation();
            e.target.stopAllActions();
            e.target.runAction(cc.scaleTo(0.1, 0.9));
            this.scheduleOnce(this.showDetailInfo, 1);
        }
        else if (e.type == cc.Node.EventType.TOUCH_MOVE) {
            if (this.onTouch && cc.ll.pDistance(this.beginPos, e.getLocation()) > 5) {
                this.onTouch = false;
                e.target.stopAllActions();
                e.target.runAction(cc.scaleTo(0.1, 1));
                this.unschedule(this.showDetailInfo);
            }
		}
		else if (e.type == cc.Node.EventType.TOUCH_CANCEL) {
            e.target.stopAllActions();
            e.target.runAction(cc.scaleTo(0.1, 1));
            this.unschedule(this.showDetailInfo);
        }
        else if (e.type == cc.Node.EventType.TOUCH_END) {
            e.target.stopAllActions();
            e.target.runAction(cc.scaleTo(0.1, 1));
            this.unschedule(this.showDetailInfo);
            if (!this.onTouch) {
                return;
            }
            if (this.panelLogic) {
				if (this.itemInfo != null) {
					if (this.isequip) {
						this.panelLogic.changeEquipType(this.type, this.itemInfo);
					}
					else{
						this.panelLogic.changeItemType(this.type, this.itemInfo.id);
					}
				}
            }
        }
	},

	showDetailInfo(){
		this.onTouch = false;
		if (this.itemInfo != null) {
			let canvas = cc.find('Canvas');
			if (this.isequip) {
				if (canvas.getChildByName('EquipItemDetail') == null) {
					let detail = cc.instantiate(this.EquipItemDetail);
					detail.parent = canvas;
					detail.name = 'EquipItemDetail';
					detail.getComponent('EquipItemDetail').loadInfo(this.itemInfo);
				}
			}
			else{
				if (canvas.getChildByName('BagItemDetail') == null) {
					let detail = cc.instantiate(this.itemDetail);
					detail.parent = canvas;
					detail.name = 'BagItemDetail';
					detail.getComponent('BagItemDetail').loadInfo(this.itemInfo);
				}
			}
		}
    }
});
