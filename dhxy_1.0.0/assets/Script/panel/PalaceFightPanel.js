let resAnchor = {
    [1001]: { scale: 1.5, x: 0.55, y: 0.4 },
    [1002]: { scale: 2, x: 0.55, y: 0.4 },
    [1011]: { scale: 1.6, x: 0.3, y: 0.35 },
    [1012]: { scale: 1.5, x: 0.55, y: 0.35 },
    [1031]: { scale: 1.5, x: 0.55, y: 0.35 },
    [1032]: { scale: 1.5, x: 0.6, y: 0.35 },

    [2003]: { scale: 1.7, x: 0.52, y: 0.4 },
    [2004]: { scale: 1.5, x: 0.65, y: 0.3 },
    [2013]: { scale: 1.6, x: 0.65, y: 0.4 },
    [2014]: { scale: 1.6, x: 0.65, y: 0.35 },
    [2033]: { scale: 1.7, x: 0.55, y: 0.42 },
    [2034]: { scale: 1.7, x: 0.5, y: 0.4 },

    [3005]: { scale: 1.8, x: 0.52, y: 0.35 },
    [3006]: { scale: 1.7, x: 0.45, y: 0.35 },
    [3015]: { scale: 1.5, x: 0.55, y: 0.35 },
    [3016]: { scale: 1.7, x: 0.55, y: 0.35 },
    [3035]: { scale: 1.5, x: 0.45, y: 0.4 },
    [3036]: { scale: 1.7, x: 0.45, y: 0.45 },

    [4007]: { scale: 1.7, x: 0.45, y: 0.45 },
    [4008]: { scale: 1.7, x: 0.58, y: 0.35 },
    [4017]: { scale: 1.5, x: 0.5, y: 0.4 },
    [4018]: { scale: 1.7, x: 0.58, y: 0.4 },
    [4037]: { scale: 1.6, x: 0.55, y: 0.4 },
    [4038]: { scale: 1.5, x: 0.68, y: 0.4 },
};

cc.Class({
    extends: cc.Component,

    properties: {
        timeLabel: cc.Label,
        roleres: cc.SpriteAtlas,
    },


    start() {
        let palaceFightIcon = cc.find('Canvas/MainUI/PalaceFightIcon');
        if (palaceFightIcon.getComponent('PalaceFightIcon').isSponsor()) {
            cc.find('FightButton', this.node).active = false;
        }
    },

    setTm (tick) {
        this.tick = tick;
    },

    showTwoList (data) {
        console.log('showTwoList', data);
        let teams = [data.sponsorlist, data.recipientlist];
        let sides = ['our_side_frame', 'enemy_frame'];
        for (let team of teams) {
            let num = 1;
            let side = sides.shift();
            for (let item of team) {
                this.setRoleInfo(item, side+num);
                ++ num;
            }
            while (num <= 5) {
                this.unshowRole(side+num);
                ++ num;
            }
        }
        this.tick = cc.find('Canvas/MainUI/PalaceFightIcon').getComponent('PalaceFightIcon').getCurTm();
        this.timeLabel.string = this.tick;
    },

    setRoleInfo (data, side) {
        let tpinfo = resAnchor[data.resid];
        let roleresnode = cc.find(`${side}/mask/roleres`, this.node);
        let sprite = roleresnode.getComponent(cc.Sprite);
        sprite.spriteFrame = this.roleres.getSpriteFrame(data.resid);
        for (let key in resAnchor) {
            if (key == data.race) {
                roleresnode.scale   = tpinfo.scale;
                roleresnode.anchorX = tpinfo.anchorX;
                roleresnode.anchorY = tpinfo.anchorY;
            }
        }

        let levelnode = cc.find(`${side}/levelbg/level`, this.node);
        let levellabel = levelnode.getComponent(cc.Label);
        levellabel.string = data.level;

        let namenode = cc.find(`${side}/levelbg/name`, this.node);
        let namelabel = namenode.getComponent(cc.Label);
        namelabel.string = data.name;
        roleresnode.active = true;
        if (data.state == 1) {
            cc.find(`${side}/fightLabel`, this.node).active = true;
            cc.find(`${side}/fightSprite`, this.node).active = true;
        }
    },

    unshowRole (side) {
        let node = cc.find(`${side}`, this.node);
        node.active = false;
        let list1 = ['our_side_frame1', 'our_side_frame2', 'our_side_frame3', 'our_side_frame4', 'our_side_frame5'];
        let list2 = ['enemy_frame1', 'enemy_frame2', 'enemy_frame3', 'enemy_frame4', 'enemy_frame5'];
        if (list1.indexOf(side) != -1) {
            for (let name of list1) {
                cc.find(name, this.node).x += 40;
            }
        }
        if (list2.indexOf(side) != -1) {
            for (let name of list2) {
                cc.find(name, this.node).x -= 40;
            }
        }
    },

    onFightClick () {
        cc.ll.net.send('c2s_palace_agree', {
            roleid: cc.ll.player.roleid,
            battle: 1,
        });
        cc.find('FightButton', this.node).active = false;
        this.showCloseButton(false);
    },

    showCloseButton (show) {
        let node = cc.find('CloseButton', this.node);
        if (node) {
            node.active = show;
        }
    },

    update (dt) {
        if (this.tick) {
            this.tick -= dt;
            this.timeLabel.string = Math.floor(this.tick);
            if (this.tick <= 0) {
                this.node.destroy();
            }
        }
    },

    onCloseClick () {
        this.node.destroy();
    },
});
