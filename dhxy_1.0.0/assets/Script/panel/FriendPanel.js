cc.Class({
	extends: cc.Component,

	properties: {
		chatItemOther: cc.Prefab,
		chatItemSelf: cc.Prefab,
		chatContent: cc.Node,
		editbox: cc.EditBox,

		emojiContent: cc.Node,
		friendsContent: cc.Node,
		emojiAtlas: cc.SpriteAtlas,

		upNode: cc.Node,
		downNode: cc.Node,

		addPanel: cc.Prefab,
		friendItem: cc.Prefab,

		tipNode: cc.Node,
	},

	onLoad() {
		this.node.on(cc.Node.EventType.TOUCH_START, this.touchBegan.bind(this));
		this.friendListNodes = [];
		// this.chatItemList = {};
		this.maxItems = 50;

		this.curChatRole = 0;
		this.chatItemNodes = [];
		this.requestList = [];

		this.isEmojiShow = false;
		let eCnt = 0;
		for (let index = 0; index < 179; index++) {
			let emojiFrame = this.emojiAtlas.getSpriteFrame(('000' + index).substr(-3) + '0000');
			if (emojiFrame == null) {
				continue;
			}
			let biaoqing = new cc.Node();
			biaoqing.scale = 1;
			biaoqing.parent = this.emojiContent;
			biaoqing.setPosition(cc.v2(30 + 60 * (Math.floor(eCnt / 3)), 60 - 60 * Math.floor(eCnt % 3)));
			biaoqing.addComponent(cc.Sprite).spriteFrame = emojiFrame;//this.emojiAtlas.getSpriteFrame(`emoji_${index + 1}_1`);
			let btn = biaoqing.addComponent(cc.Button);
			btn.transition = cc.Button.Transition.SCALE;
			var clickEventHandler = new cc.Component.EventHandler();
			clickEventHandler.target = this.node;
			clickEventHandler.component = "FriendPanel";
			clickEventHandler.handler = "onBiaoqingItemClicked";
			clickEventHandler.customEventData = index;
			btn.clickEvents.push(clickEventHandler);

			if (30 + 60 * (Math.floor(eCnt / 3) + 1) > this.emojiContent.width) {
				this.emojiContent.width = 30 + 60 * (Math.floor(eCnt / 3) + 1);
			}
			eCnt++;
		}

		cc.ll.net.send('c2s_get_friends', {});
		this.currentY = 0;
	},

	onBiaoqingItemClicked(e, d) {
		this.editbox.string += `[${d}]`;
	},

	hideEmoji() {
		if (this.isEmojiShow) {
			this.isEmojiShow = false;
			this.upNode.stopAllActions();
			this.upNode.runAction(cc.moveTo(0.1, cc.v2(this.upNode.x, 0)));
			this.downNode.stopAllActions();
			this.downNode.runAction(cc.moveTo(0.1, cc.v2(this.downNode.x, -288)));
		}
	},

	editingDidBegan(e, d) {
		this.hideEmoji();
	},

	addChatListInfo(info) {
		// if (this.chatItemList[info.toid + info.fromid] == null) {
		// 	this.chatItemList[info.toid + info.fromid] = [];
		// }
		// this.chatItemList[info.toid + info.fromid].push(info);

		if (this.curChatRole == info.fromid || this.curChatRole == info.toid) {
			this.addChatItem(info);
		}

		for (const item of this.friendListNodes) {
			if (info.fromid == item.getComponent('FriendListItem').friendInfo.roleid) {
				if (info.fromid == this.curChatRole) {
					cc.ll.player.unreadInfoId[this.curChatRole] = 0;
				} else {
					item.getChildByName('tip').active = true;
				}
			}
		}
		if (this.chatItemNodes.length > this.maxItems) {
			this.delFirstChatItem();
		}
	},

	showChatListInfo() {
		this.chatContent.destroyAllChildren();
		this.chatItemNodes = [];
		this.chatContent.height = this.chatContent.parent.height;
		if (cc.ll.player.chatItemList[this.curChatRole + cc.ll.player.roleid]) {
			for (const info of cc.ll.player.chatItemList[this.curChatRole + cc.ll.player.roleid]) {
				this.addChatItem(info);
			}
		}
	},

	delFirstChatItem() {
		let delItem = this.chatItemNodes.shift();
		if (delItem) {
			this.chatContent.height -= delItem.height;
			for (const item of this.chatItemNodes) {
				item.y += delItem.height;
			}
			delItem.destroy();
		}
	},

	addChatItem(info) {
		let chatItem = null;
		if (info.fromid == cc.ll.player.roleid) {
			chatItem = cc.instantiate(this.chatItemSelf);
		} else {
			chatItem = cc.instantiate(this.chatItemOther);
		}
		chatItem.width = 420;
		chatItem.parent = this.chatContent;
		chatItem.getComponent('ChatItem').loadInfo(info);
		chatItem.x = 0;
		let blankHeight = this.chatContent.height;
		if (this.chatItemNodes[0]) {
			blankHeight = -this.chatItemNodes[0].y;
		}
		if (chatItem.height > blankHeight) {
			this.chatContent.height += chatItem.height - blankHeight;
			for (const item of this.chatItemNodes) {
				item.y += blankHeight;
			}
		}
		else {
			for (const item of this.chatItemNodes) {
				item.y += chatItem.height;
			}
		}

		chatItem.y = -this.chatContent.height + chatItem.height;
		this.chatItemNodes.push(chatItem);

		if (this.chatContent.y > this.chatContent.height - this.chatContent.parent.height - chatItem.height - 20) {
			this.chatContent.y = this.chatContent.height - this.chatContent.parent.height;
		}
	},

	loadFriendListInfo(list) {
		this.tipNode.active = false;
		this.requestList = [];
		let currentList = [];
		for (const info of list) {
			if (info.state == 0) {
				this.requestList.push(info);
			} else {
				currentList.push(info);
			}
		}
		currentList.sort(function (a, b) {
			let change = -1;
			if (cc.ll.player.unreadInfoId[a.roleid] == null || cc.ll.player.unreadInfoId[a.roleid] == 0) {
				if (cc.ll.player.unreadInfoId[b.roleid] == null || cc.ll.player.unreadInfoId[b.roleid] == 0)
					change = 0;
				if (cc.ll.player.unreadInfoId[b.roleid] == 1)
					change = 1;
			}
			return change;
		});

		if (this.requestList.length > 0) {
			this.tipNode.active = true;
			let num = this.requestList.length > 9 ? '9+' : this.requestList.length;
			this.tipNode.getChildByName('num').getComponent(cc.Label).string = num;
			if (this.node.parent.getChildByName('FriendAddLayer')) {
				this.node.parent.getChildByName('FriendAddLayer').destroy();
			}
			let add = cc.instantiate(this.addPanel);
			add.parent = this.node.parent;
			add.name = 'FriendAddLayer';
			add.getComponent('FriendAddLayer').setPanelType(2);
			add.getComponent('FriendAddLayer').showFriendList(this.requestList);
		}
		this.friendsContent.destroyAllChildren();
		this.friendListNodes = [];
		this.friendsContent.height = this.friendsContent.parent.height;
		this.currentY = 0;
		for (let index = 0; index < currentList.length; index++) {
			const info = currentList[index];
			let friendItem = cc.instantiate(this.friendItem);
			friendItem.parent = this.friendsContent;
			friendItem.getComponent('FriendListItem').loadInfo(info);
			friendItem.x = 0;
			friendItem.y = this.currentY;

			if (friendItem.y - friendItem.height < -this.friendsContent.height) {
				this.friendsContent.height = friendItem.height - friendItem.y;
			}
			this.currentY -= friendItem.height;

			this.friendListNodes.push(friendItem);
			let btn = friendItem.getComponent(cc.Button);
			var clickEventHandler = new cc.Component.EventHandler();
			clickEventHandler.target = this.node;
			clickEventHandler.component = "FriendPanel";
			clickEventHandler.handler = "friendItemClicked";
			clickEventHandler.customEventData = info.roleid;
			btn.clickEvents.push(clickEventHandler);

			if (cc.ll.player.unreadInfoId[info.roleid]) {
				friendItem.getChildByName('tip').active = true;
			}
		}
	},

	friendItemClicked(e, d) {
		this.selectedFriend(e.target, d);
	},

	selectedFriend(n, d) {
		if (n) {
			n.getChildByName('tip').active = false;
			cc.ll.player.unreadInfoId[d] = 0;
		}
		if (this.curChatRole == d) {
			return;
		}
		for (const item of this.friendListNodes) {
			item.getComponent('FriendListItem').unSelected();
		}
		this.curChatRole = d;
		this.showChatListInfo();
		n.getComponent('FriendListItem').selected();
	},

	onAddBtnClicked(e, d) {
		this.hideEmoji();
		let add = cc.instantiate(this.addPanel);
		add.parent = this.node.parent;
		add.name = 'FriendAddLayer';
		if (this.requestList.length > 0) {
			add.getComponent('FriendAddLayer').setPanelType(2);
			add.getComponent('FriendAddLayer').showFriendList(this.requestList);
		}
		else {
			add.getComponent('FriendAddLayer').setPanelType(0);
		}
	},

	onClearBtnClicked(e, d) {
		this.chatContent.destroyAllChildren();
		this.chatItemNodes = [];
		this.chatContent.height = this.chatContent.parent.height;
		cc.ll.player.chatItemList[this.curChatRole + cc.ll.player.roleid] = [];
	},

	onButtonClick(event, param) {
		console.log(param);
		if (param == 'close') {
			//zfy --关闭音效
			cc.ll.AudioMgr.playCloseAudio();
			this.node.destroy();
		}
		else if (param == 'emoji') {
			if (!this.isEmojiShow) {
				this.isEmojiShow = true;
				this.upNode.stopAllActions();
				this.upNode.runAction(cc.moveTo(0.1, cc.v2(this.upNode.x, 150)));
				this.downNode.stopAllActions();
				this.downNode.runAction(cc.moveTo(0.1, cc.v2(this.downNode.x, -88)));
			}
		}
		else if (param == 'send') {
			if (this.editbox.string.length == 0 || this.curChatRole == 0) {
				return;
			}
			this.hideEmoji();
			let chatdata = {
				fromid: cc.ll.player.roleid,
				toid: this.curChatRole,
				msg: this.editbox.string,
				name: cc.ll.player.name,
				resid: cc.ll.player.resid,
			};
			cc.ll.net.send('c2s_friend_chat', chatdata);
			cc.ll.player.addFriendChatInfo(chatdata);
			// this.addChatListInfo(chatdata);
			this.editbox.string = '';
		}
	},

	touchBegan(event) {
		this.hideEmoji();
	},
});
