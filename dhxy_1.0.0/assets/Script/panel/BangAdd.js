cc.Class({
	extends: cc.Component,

	properties: {
		aimLab: cc.Label,
		listItem: cc.Node,
		listContent: cc.Node,
		searchEdit: cc.EditBox,
	},

	onLoad() {
		this.clickNum = 1;
		this.listdata = {};
		this.bangdata = {};
		this.currentSelected = -1;
		this.preitem = null;
	},

	loadBangList(data) {
		cc.ll.player.bangid = 0;
		cc.ll.loading.unshowLoading();
		this.currentSelected = -1;
		this.listdata = data;
		this.aimLab.string = '';
		this.listContent.destroyAllChildren();
		this.maxLength = data.length;

		this.currentLenth = 7;
		this.listContent.height = this.currentLenth * this.listItem.height;
		if (this.listContent.height < this.listContent.parent.height) {
			this.listContent.height = this.listContent.parent.height;
		}
		for (let index = 0; index < this.currentLenth; index++) {
			if(index>=this.maxLength){
				return;
			}
			let item = cc.instantiate(this.listItem);
			item.active = true;
			item.parent = this.listContent;
			item.active = true;
			item.x = 0;
			item.y = -index * item.height;
			if (index % 2 == 0) {
				item.getChildByName('bg2').active = true;
			}
			item.getChildByName('name').getComponent(cc.Label).string = data[index].name;
			item.getChildByName('num').getComponent(cc.Label).string = data[index].bangid;
			item.getChildByName('count').getComponent(cc.Label).string = data[index].rolenum;
			item.getChildByName('master').getComponent(cc.Label).string = data[index].mastername;
			item.bangtag = index;
			this.tagIndex = index;
		}
	},

	onNextBtn(e,d){
		this.clickNum = this.clickNum+1;
		if(this.clickNum >= this.maxLength) return;
		if(this.clickNum >= Math.ceil(this.maxLength/7)+1){
			this.clickNum = 5;
			return;
		}
		this.listContent.destroyAllChildren();
		let num = 7;
		if (this.listContent.height < this.listContent.parent.height) {
			this.listContent.height = this.listContent.parent.height;
		}
		let y_N = 0;
		let dx;
		for (let index = 0; index < num; index++) {
			dx = (this.clickNum*num-num)+index;
			if(dx>=this.maxLength){
				return;
			}
			let item = cc.instantiate(this.listItem);
			item.active = true;
			item.parent = this.listContent;
			item.active = true;
			item.x = 0;
			item.y = -y_N * item.height;
			if (index % 2 == 0) {
				item.getChildByName('bg2').active = true;
			}
			item.getChildByName('name').getComponent(cc.Label).string = this.listdata[dx].name;
			item.getChildByName('num').getComponent(cc.Label).string = this.listdata[dx].bangid;
			item.getChildByName('count').getComponent(cc.Label).string = this.listdata[dx].rolenum;
			item.getChildByName('master').getComponent(cc.Label).string = this.listdata[dx].mastername;
			item.bangtag = dx;
			this.tagIndex = dx;
			y_N = y_N+1;
		}
	},
	
	onUpBtn(){
		if(this.clickNum >= this.maxLength) return;
		if(this.clickNum == 1){
			return;
		}
		if(this.clickNum>1){
			this.clickNum = this.clickNum - 1;
		}
		this.listContent.destroyAllChildren();
		let num = 7;
		if (this.listContent.height < this.listContent.parent.height) {
			this.listContent.height = this.listContent.parent.height;
		}
		let y_N = 0;
		let dx;
		for (let index = 0; index < num; index++) {
			y_N = y_N+1;
			dx = (num*this.clickNum)-y_N;
			let item = cc.instantiate(this.listItem);
			item.parent = this.listContent;
			item.active = true;
			item.x = 0;
			item.y = -(num-y_N) * item.height;
			if (index % 2 == 0) {
				item.getChildByName('bg2').active = true;
			}
			item.getChildByName('name').getComponent(cc.Label).string = this.listdata[dx].name;
			item.getChildByName('num').getComponent(cc.Label).string = this.listdata[dx].bangid;
			item.getChildByName('count').getComponent(cc.Label).string = this.listdata[dx].rolenum;
			item.getChildByName('master').getComponent(cc.Label).string = this.listdata[dx].mastername;
			item.bangtag = dx;
			this.tagIndex = dx;
		}
		
	},

	searchBtnClicked(e, d) {
		// if (this.searchEdit.string == "") {
		// 	return;
		// }
		cc.ll.net.send('c2s_searchbang', {
			roleid: cc.ll.player.roleid,
			data: this.searchEdit.string
		});
		cc.ll.loading.showLoading(10);
	},

	joinBtnClicked(e, d){
		if (this.currentSelected == -1) {
			return;
		}
		cc.ll.net.send('c2s_requestbang', {
			roleid: cc.ll.player.roleid,
			bangid: this.bangdata.bangid
		});
	},

	itemClicked(e, d) {
		let item = e.target;
		if (this.currentSelected == item.bangtag) {
			return;
		}
		if (this.preitem != null) {
			this.preitem.getChildByName('bg3').active = false;
		}
		this.preitem = item;
		item.getChildByName('bg3').active = true;
		this.currentSelected = item.bangtag;
		this.bangdata = this.listdata[this.currentSelected];
		this.aimLab.string = this.bangdata.aim;
	},
});
