let GameRes = require('../game/GameRes');
cc.Class({
	extends: cc.Component,

	properties: {
		nameLab: cc.Label,
		aimLab: cc.Label,
		roleInfo: cc.Node,
		bangInfo: cc.Node,
		roleNode: cc.Node,
		listItem: cc.Node,
		listContent: cc.Node,
		weight: cc.Label,
		bidnode: cc.Node,

		SafeNotice:cc.Prefab,
	},

	onLoad() {
		this.bangdata = {};
		this.roledata = {};
		this.currentSelected = -1;
		this.onShowRoleInfo = false;
		this.roleInfo.active = false;
		this.preitem = null;
		this.ismaster = false;
	},

	loadBangInfo(data) {
		cc.ll.loading.unshowLoading();
		this.currentSelected = -1;
		this.roleInfo.active = false;
		this.bangdata = data;
		this.nameLab.string = data.info.name;
		this.aimLab.string = data.info.aim;

		cc.ll.player.bangid = data.info.bangid;
		this.ismaster = (cc.ll.player.roleid == data.info.masterid);

		this.weight.node.active = this.ismaster;
		this.bidnode.active = this.ismaster;

		if (this.ismaster) {
			this.weight.string = '帮派权重:' + data.info.weight;
		}

		this.listContent.destroyAllChildren();
		this.itemNodes = [];
		let maxLength = data.rolelist.length;
		// this.listContent.height = maxLength * this.listItem.height;
		// if (this.listContent.height < this.listContent.parent.height) {
		// 	this.listContent.height = this.listContent.parent.height;
		// }
		let itemheight = 0;
		for (let index = 0; index < maxLength; index++) {
			let item = cc.instantiate(this.listItem);
			if (item) {
				item.parent = this.listContent;
				item.active = true;
				item.x = 0;
				item.y = -index * item.height;
				if (index % 2 == 0) {
					item.getChildByName('bg2').active = true;
				}
				if (itemheight == 0) {
					itemheight = item.height;
				}
				let jobarr = ['帮主', '帮众'];
				let sexarr = ['男', '女'];
				let racearr = ['人', '仙', '魔', '鬼'];
				let onlinearr = ['离线', '在线'];

				item.getChildByName('Name').getComponent(cc.Label).string = data.rolelist[index].name;
				item.getChildByName('Level').getComponent(cc.Label).string = data.rolelist[index].level;
				item.getChildByName('Position').getComponent(cc.Label).string = data.rolelist[index].roleid == data.info.masterid ? jobarr[0] : jobarr[1];
				item.getChildByName('Race').getComponent(cc.Label).string = sexarr[data.rolelist[index].sex - 1] + racearr[data.rolelist[index].race - 1];
				item.getChildByName('OffLine').getComponent(cc.Label).string = onlinearr[data.rolelist[index].online];

				item.bangtag = index;
				this.itemNodes.push(item);
			}
		}

		this.listContent.height = maxLength * itemheight;
		if (this.listContent.height < this.listContent.parent.height) {
			this.listContent.height = this.listContent.parent.height;
		}

		if (!this.onShowRoleInfo) {
			this.bangInfo.active = true;
		}
		if (this.ismaster) {
			this.titleStr = '解散';
			this.bangInfo.getChildByName('requestListBtn').active = true;
			this.node.getChildByName('ButtomBtn').getChildByName('ExitBang').active = false;
			this.node.getChildByName('ButtomBtn').getChildByName('DisbandBang').active = true;
		} else {
			this.titleStr = '离开';
		}
	},

	onLeaveBtnClicked(e, d) {
		//--zfy增加提示
		cc.ll.notice.addMsg(1, '是否' + this.titleStr + this.nameLab.string + '帮派', () => {
			if (cc.ll.player.safe_lock == 1) {
				let safe_notice = cc.instantiate(this.SafeNotice);
				safe_notice.parent = cc.find('Canvas');
				safe_notice.getComponent('SafeNotice').setCallback(() => {
					cc.ll.net.send('c2s_leavebang', {
						roleid: cc.ll.player.roleid,
						bangid: cc.ll.player.bangid
					});
					cc.find('Canvas').getComponent('GameLogic').mapLogic.changeMap('1011');
				});
			}
			else {
				cc.ll.net.send('c2s_leavebang', {
					roleid: cc.ll.player.roleid,
					bangid: cc.ll.player.bangid
				});
				cc.find('Canvas').getComponent('GameLogic').mapLogic.changeMap('1011');
			}
		}, () => { });

	},

	onKickOutBtnClicked(e, d) {
		if (this.roledata.roleid == null) {
			return;
		}
		cc.ll.notice.addMsg(1, '是否将[' + this.roledata.name + ']踢出', () => {
			cc.ll.loading.showLoading(10);
			cc.ll.net.send('c2s_leavebang', {
				roleid: this.roledata.roleid,
				bangid: cc.ll.player.bangid
			});
		}, () => { });

	},

	onChangeShow(e, d) {
		if (d == 0) {
			this.onShowRoleInfo = false;
			this.bangInfo.active = true;
			this.roleInfo.active = false;
		}
		else {
			this.onShowRoleInfo = true;
			this.bangInfo.active = false;
			this.roleInfo.active = false;
			if (this.currentSelected != -1) {
				this.roleInfo.active = true;
			}
		}
	},

	showRoleInfo() {
		this.roleNode.getComponent('UIRole').setInfo(this.roledata, true);
		if (this.ismaster && this.roledata.roleid != cc.ll.player.roleid) {
			this.roleInfo.getChildByName('kickoutBtn').active = true;
		}
		else {
			this.roleInfo.getChildByName('kickoutBtn').active = false;
		}
		if (this.onShowRoleInfo) {
			this.roleInfo.active = true;
		}
	},

	itemClicked(e, d) {
		let item = e.target;
		if (this.currentSelected == item.bangtag) {
			return;
		}
		if (this.preitem != null) {
			this.preitem.getChildByName('bg3').active = false;
		}
		this.preitem = item;
		item.getChildByName('bg3').active = true;
		this.currentSelected = item.bangtag;
		this.roledata = this.bangdata.rolelist[this.currentSelected];
		this.showRoleInfo();
	},

	clear() {
		this.roleNode.getComponent('UIRole').clear();
	},
});
