let c2s = {
    ['test']: (agent, data) => {
        console.log('socket disconnect');
    },

    /////////////////////客户端不要做方法 是否存在的判断////////////////////////////
    ['s2c_operation_result']: (agent, data) => {
        agent.s2c_operation_result(data);
    },

    ['s2c_login']: (agent, data) => {
        agent.s2c_login(data);
    },

    ['s2c_otherlogin']: (agent, data) => {
        agent.s2c_otherlogin(data);
    },

    ['s2c_change_map']: (agent, data) => {
        agent.s2c_change_map(data);
    },

    ['s2c_team_info']: (agent, data) => {
        agent.s2c_team_info(data);
    },

    ['s2c_team_list']: (agent, data) => {
        agent.s2c_team_list(data);
    },

    ['s2c_team_requeslist']: (agent, data) => {
        agent.s2c_team_requeslist(data);
    },

    ['s2c_prison_time']: (agent, data) => {
        agent.s2c_prison_time(data);
    },

    ['s2c_pet_changeSskill']: (agent, data) => {
        agent.s2c_pet_changeSskill(data);
    },

    ['s2c_player_pos']: (agent, data) => {
        agent.s2c_player_pos(data);
    },

    ['s2c_team_join']: (agent, data) => {
        agent.s2c_team_join(data);
    },

    ['s2c_transfer_team_requst']: (agent, data) =>{
        agent.s2c_transfer_team_requst(data);
    },

    ['s2c_aoi_pinfo']: (agent, data) => {
        agent.s2c_aoi_pinfo(data);
    },

    ['s2c_aoi_exit']: (agent, data) => {
        agent.s2c_aoi_exit(data);
    },

    ['s2c_aoi_stop']: (agent, data) => {
        agent.s2c_aoi_stop(data);
    },

    ['s2c_player_data']: (agent, data) => {
        agent.s2c_player_data(data);
    },

    ['s2c_game_chat']: (agent, data) => {
        agent.s2c_game_chat(data);
    },

    ['s2c_friends_info']: (agent, data) => {
        agent.s2c_friends_info(data);
    },

    ['s2c_search_friends']: (agent, data) => {
        agent.s2c_search_friends(data);
    },

    ['s2c_friend_chat']: (agent, data) => {
        agent.s2c_friend_chat(data);
    },

    ['s2c_partner_exp']: (agent, data) => {
        agent.s2c_partner_exp(data);
    },

    ['s2c_partner_info']: (agent, data) =>
    {
        agent.s2c_partner_info(data);
    },

    ['s2c_partner_list']: (agent, data) => {
        agent.s2c_partner_list(data);
    },

    ['s2c_partner_exchange_exp_ok']: (agent, data) => {
        agent.s2c_partner_exchange_exp_ok(data);
    },

    ['c2s_other_info']: (agent, data) => {
        agent.c2s_other_info(data);
    },

    ['s2c_role_task_list']: (agent, data) => {
        agent.s2c_role_task_list(data);
    },

    ['s2c_npc_notice']: (agent, data) =>
    {
        agent.s2c_npc_notice(data.nNpcConfigID, data.strRichText);
    },

    ['s2c_notice']: (agent, data) => {
        agent.s2c_notice(data);
    },

    ['s2c_screen_msg']: (agent, data) =>
    {
        agent.s2c_screen_msg(data);
    },

    ['s2c_star_waiting']: (agent, data) =>
    {
        agent.s2c_star_waiting(data);
    },

    ['s2c_daily_info']: (agent, data) =>
    {
        agent.s2c_daily_info(data);
    },




    ['s2c_you_get_item']: (agent, data) => {
        agent.s2c_you_get_item(data);
    },

    ['s2c_create_task_npc']: (agent, data) =>
    {
        agent.s2c_create_task_npc(data);
    },




    ['s2c_paihang']: (agent, data) => {
        agent.s2c_paihang(data);
    },

    ['s2c_roles_goods']: (agent, data) => {
        agent.s2c_roles_goods(data);
    },

    ['s2c_goods']: (agent, data) => {
        agent.s2c_goods(data);
    },

    ['s2c_add_exp']: (agent, data) => {
        agent.s2c_add_exp(data);
    },

    ['s2c_level_up']: (agent, data) => {
        agent.s2c_level_up(data);
    },

    ['s2c_you_money']: (agent, data) => {
        agent.s2c_you_money(data.nKind, data.nNum,data.nChange );
    },

    ['s2c_charge']: (agent, data) => {
        agent.s2c_charge(data);
    },

    ['s2c_bagitem']: (agent, data) => {
        agent.s2c_bagitem(data);
    },
    ['s2c_mallitems']: (agent, data) => {
        agent.s2c_mallitems(data);
    },

    ['s2c_npc_shop_item']: (agent, data) =>
    {
        agent.s2c_npc_shop_item(data);
    },

    ['s2c_lottery_info']: (agent, data) =>
    {
        agent.s2c_lottery_info(data);
    },

    ['s2c_lottery_result']: (agent, data) =>
    {
        agent.s2c_lottery_result(data);
    },


    ['s2c_relive_list']: (agent, data) =>
    {
        agent.s2c_relive_list(data);
    },
    ['s2c_level_reward']: (agent, data) =>
    {
        agent.s2c_level_reward(data);
    },


    ['s2c_lockeritem']: (agent, data) => {
        agent.s2c_lockeritem(data);
    },

    ['s2c_incense_state']: (agent, data) => {
        agent.s2c_incense_state(data);
    },

    ['s2c_getbanglist']: (agent, data) => {
        agent.s2c_getbanglist(data);
    },

    ['s2c_join_bang']: (agent, data) => {
        agent.s2c_join_bang(data);
    },

    ['s2c_getbanginfo']: (agent, data) => {
        agent.s2c_getbanginfo(data);
    },

    ['s2c_getbangrequest']: (agent, data) => {
        agent.s2c_getbangrequest(data);
    },

    ['s2c_getbangrequest_info']: (agent, data) => {
        agent.s2c_getbangrequest_info(data);
    },

    ['s2c_new_pet']: (agent, data) => {
        agent.s2c_new_pet(data);
    },

    ['s2c_get_petlist']: (agent, data) => {
        agent.s2c_get_petlist(data);
    },

    ['s2c_change_pet']: (agent, data) => {
        agent.s2c_change_pet(data);
    },

    ['s2c_del_pet']: (agent, data) => {
        agent.s2c_del_pet(data);
    },

    ['s2c_update_pet']: (agent, data) => {
        agent.s2c_update_pet(data);
    },

    ['s2c_wash_petproperty']: (agent, data) => {
        agent.s2c_wash_petproperty(data);
    },

    ['s2c_save_petproperty']: (agent, data) => {
        agent.s2c_save_petproperty(data);
    },

    ['s2c_equip_list']: (agent, data) => {
        agent.s2c_equip_list(data);
    },

    ['s2c_equip_info']: (agent, data) => {
        agent.s2c_equip_info(data);
    },

    ['s2c_next_equip']: (agent, data) => {
        agent.s2c_next_equip(data);
    },

    ['s2c_equip_property']: (agent, data) => {
        agent.s2c_equip_property(data);
    },

    ['s2c_change_weapon']: (agent, data) => {
        agent.s2c_change_weapon(data);
    },

    ['s2c_xianqi_list']: (agent, data) => {
        agent.s2c_xianqi_list(data);
    },


    //-------------------- zzzHere

    ['s2c_btl']: (agent, data) => { //战斗开始
        agent.s2c_btl(data);
    },

    ['s2c_btl_end']: (agent, data) => { //战斗结束
        agent.s2c_btl_end(data);
    },

    ['s2c_btl_act']: (agent, data) => {
        agent.s2c_btl_act(data);
    },

    ['s2c_relive']: (agent, data) => {
        agent.s2c_relive(data);
    },

    ['s2c_changerace']: (agent, data) => {
        agent.s2c_changerace(data);
    },

    ['s2c_btl_round']: (agent, data) => { //一局
        agent.s2c_btl_round(data);
    },

    ['s2c_btl_roundbegin']: (agent, data) => { //战斗开始
        agent.s2c_btl_roundbegin(data);
    },


    ['s2c_hongbao_open']: (agent, data) => {
        agent.s2c_hongbao_open(data);
    },

    ['s2c_hongbao_result']: (agent, data) => {
        agent.s2c_hongbao_result(data);
    },

    ['s2c_remunerate']: (agent, data) => {
        agent.s2c_remunerate(data);
    },


    /////////////////////水陆大会/////////////////////////////////
    ['s2c_shuilu_sign']: (agent, data) => {
        agent.s2c_shuilu_sign(data);
    },

    ['s2c_shuilu_unsign']: (agent, data) => {
        agent.s2c_shuilu_unsign(data);
    },

    ['s2c_shuilu_info']: (agent, data) => {
        agent.s2c_shuilu_info(data);
    },

    ['s2c_shuilu_match']:(agent, data) => {
        agent.s2c_shuilu_match(data);
    },

    ['s2c_shuilu_state']:(agent, data) => {
        agent.s2c_shuilu_state(data);
    },

    ['s2c_shuilu_battleres']:(agent, data) => {
        agent.s2c_shuilu_battleres(data);
    },
    ///////////////////水陆大会结束//////////////////////////////////

    ['s2c_world_reward_list']:(agent,data)=>{
        agent.s2c_world_reward_list(data);
    },

    ['s2c_reward_get_list']:(agent,data)=>{
        agent.s2c_reward_get_list(data);
    },

    //角色称谓
    ['s2c_title_info']:(agent,data)=>{
        agent.s2c_title_info(data);
    },
    ['s2c_title_change']:(agent,data)=>{
        agent.s2c_title_change(data);
    },

    //解散，退出帮派
    ['s2c_leavebang']:(agent,data)=>{
        agent.s2c_leavebang(data);
    },

    ['s2c_linghou_fight']:(agent,data)=>{
        agent.s2c_linghou_fight(data);
    },

    ['s2c_update_shop_info']:(agent,data)=>{
        agent.s2c_update_shop_info(data);
    },

    ['s2c_palace_fight']:(agent,data)=>{
        agent.s2c_palace_fight(data);
    },

    ['s2c_palace_rolelist']:(agent,data)=>{
        agent.s2c_palace_rolelist(data);
    },

    ['s2c_relation_apply_res']:(agent,data)=>{
        agent.s2c_relation_apply_res(data);
    },

    ['s2c_relation_apply_info']:(agent,data)=>{
        agent.s2c_relation_apply_info(data);
    },

    ['s2c_relation_apply_answer']:(agent,data)=>{
        agent.s2c_relation_apply_answer(data);
    },

    ['s2c_relation_created']:(agent,data)=>{
        agent.s2c_relation_created(data);
    },

    ['s2c_relation_List']:(agent,data)=>{
        agent.s2c_relation_List(data);
    },

    ['s2c_relation_destroy']:(agent,data)=>{
        agent.s2c_relation_destroy(data);
    },

    ['s2c_relation_leave']:(agent,data)=>{
        agent.s2c_relation_leave(data);
    },

    ['s2c_relation_reject']:(agent,data)=>{
        agent.s2c_relation_reject(data);
    },

    ['s2c_scheme_info']:(agent,data)=>{
        agent.s2c_scheme_info(data);
    },

    ['s2c_scheme_List']:(agent,data)=>{
        agent.s2c_scheme_List(data);
    },

    ['s2c_scheme_create']:(agent,data)=>{
        agent.s2c_scheme_create(data);
    },

    ['s2c_scheme_update']:(agent,data)=>{
        agent.s2c_scheme_update(data);
    },

    ['s2c_scheme_delete']:(agent,data)=>{
        agent.s2c_scheme_delete(data);
    },

    ['s2c_scheme_updateEquip']:(agent,data)=>{
        agent.s2c_scheme_updateEquip(data);
    },

    ['s2c_change_role_color']:(agent,data)=>{
        agent.s2c_change_role_color(data);
    },

    ['s2c_scheme_changeName']:(agent,data)=>{
        agent.s2c_scheme_changeName(data);
    },

    ['s2c_scheme_activate']:(agent,data)=>{
        agent.s2c_scheme_activate(data);
    },

    ['s2c_scheme_use']:(agent,data)=>{
        agent.s2c_scheme_use(data);
    },

    ['s2c_bell_msg']:(agent,data)=>{
        agent.s2c_bell_msg(data);
    },

    ['s2c_safepass_msg']:(agent,data)=>{
        agent.s2c_safepass_msg(data);
    },
    
    ['s2c_scheme_resetXiulianPoint']:(agent,data)=>{
        agent.s2c_scheme_resetXiulianPoint(data);
    },
    
};

module.exports = c2s;
