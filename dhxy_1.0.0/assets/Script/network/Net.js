var StrCommon = require('../etc/str_common');
var MsgCode = require('../etc/msg_code');
let GameRes = require('../game/GameRes');
let CPubFunction = require('../game/PubFunction');
let CMainPlayerInfo = require('../game/MainPlayerInfo');
var ByteBuffer = require('../3rdparty/bytebuffer');

if (window.io == null) {
	window.io = require("socket-io");
}

let packet = require('./packet');

/** 消息router路由器/消息分发列表/消息路由列表:负责服务器消息返回的分发*/
let c2s = require('./proto_c');

class Net {
	constructor() {
		this.ip = '';
		this.port = 0;

		this.sio = null;

		this.isPinging = false;
		this.hasClose = true;

		this.connectFn = null;
		this.pingTimer = 0;
		this.needNotice = true;
	}

	formatBuffer(buffer) {
		var bufferArray = Object.keys(buffer).map(function (k) {
			return buffer[k];
		})
		return bufferArray
	}

	connect(ip, port, callback, discallback) {
		ip && (this.ip = ip);
		port && (this.port = port);
		callback && (this.connectFn = callback);
		discallback && (this.fnDisconnect = discallback);

		let self = this;
		let ws = new WebSocket(`ws://${this.ip}:${this.port}`);
		ws.binaryType = 'arraybuffer';
		this.sio = ws;

		ws.onopen = () => {
			console.log("onopen");
			self.sio.connected = true;
			self.hasClose = false;
			if (self.connectFn) {
				self.connectFn(self);
			}
			self.startHearbeat();
		}

		ws.onmessage = (event) => {
			if (typeof event.data == 'string') {
				self.onStrMsg(event.data);
				return;
			}
			//解包
			var buffer = new ByteBuffer();
			buffer.append(event.data);
			buffer.flip();
			let headlen = buffer.readShort();
			let head = buffer.readString(headlen);

			//消息分发/消息路由
			const func = c2s[head];
			if (func) {
				let leftbuff = new Uint8Array(buffer.buffer).subarray(buffer.offset, buffer.limit);
				let pack = new packet(head);
				let data = pack.todata(leftbuff);
				func(this, data);
				pack = null;
				leftbuff = null;
			}
			buffer = null;
		};

		ws.onclose = () => {
			self.close();
		};

		ws.onerror = () => {
			console.log("异常关闭")
			self.close();
		};
	}

	close() {
		console.log('close');
		if (this.sio && this.sio.connected) {
			this.sio.connected = false;
			this.sio = null;
		}
		if (this.fnDisconnect) {
			if (this.needNotice && cc.ll.net == this) {
				this.fnDisconnect();
			}
			this.fnDisconnect = null;
		}

		if (this.pingTimer != 0) {
			clearInterval(this.pingTimer);
			this.pingTimer = 0;
		}
		this.hasClose = true;
	}

	startHearbeat() {
		this.lastRecieveTime = Date.now();
		if (!this.isPinging) {
			this.isPinging = true;
			let self = this;
			this.pingTimer = setInterval(function () {
				let nowdate = Date.now();
				if (nowdate - self.lastRecieveTime > 20 * 1000) {
					self.close();
					return;
				}
				if (self.sio) {
					self.sio.send('ping');
				}
			}, 5 * 1000);
		}
	}

	onStrMsg(str) {
		if (str == 'pong') {
			this.lastRecieveTime = Date.now();
		}
	}

	send(event, obj) {
		if (this.sio) {
			let pack = new packet(event);
			pack.create(obj);
			let buffer = pack.tobuffer();
			this.sio.send(buffer);
		}
	}


	getHallLogic() {
		if (cc.ll.runningSceneName == 'HallScene') {
			let logic = cc.find('Canvas').getComponent('HallLogic');
			if (logic != null) {
				return logic;
			}
		}
		return null;
	}

	getGameLogic() {
		if (cc.ll.runningSceneName == 'MainScene') {
			let logic = cc.find('Canvas').getComponent('GameLogic');
			if (logic != null) {
				return logic;
			}
		}
		return null;
	}

	getBattleStageLogic() {
		if (cc.ll.runningSceneName == 'MainScene') {
			let logic = cc.find('Canvas/BattleLayer/BattleStage').getComponent('BattleStage');
			if (logic != null) {
				return logic;
			}
		}
		return null;
	}

	GetMainUI() {
		if (cc.ll.runningSceneName == 'MainScene') {
			let comMainUI = cc.find('Canvas/MainUI').getComponent('MainUI');
			if (comMainUI != null) {
				return comMainUI;
			}
		}
		return null;
	}

	s2c_operation_result(data) {
		cc.ll.loading.unshowLoading();
		cc.ll.msgbox.addMsg(data.code);
	}

	s2c_login(data) {
		if (data.errorcode != 0) {
			cc.ll.notice.addMsg(0, data.errorcode, () => { });
			return;
		}

		if (data.info) {
			cc.ll.player.setUserInfo(data.info);
			cc.ll.local.setUserid(data.info.roleid);

			cc.ll.sceneMgr.changeScene('GameLoadScene');
		}
	}

	s2c_otherlogin() {
		cc.ll.notice.addMsg(1, `其他设备登录帐号`, () => {
			this.fnDisconnect = null;
			this.close();

			cc.ll.sceneMgr.changeScene('LoginScene');
		}, () => { });
	}

	s2c_change_map(data) {
		let logic = this.getGameLogic()
		if (logic != null) {
			logic.mapLogic.changeMap(data.mapid, JSON.parse(data.pos), false);
		}
	}

	s2c_team_info(data) {
		let jinfo = JSON.parse(data.info);
		cc.ll.player.setTeam(jinfo);

		let goTeamUI = cc.find('Canvas/MainUI/TeamUI');
		if (goTeamUI != null)
			goTeamUI.getComponent('TeamUI').CreateCardList();
	}

	s2c_team_list(data) {
		let list = JSON.parse(data.list);
		let goTeamUI = cc.find('Canvas/MainUI/TeamUI');
		if (goTeamUI != null && goTeamUI.getChildByName('teamList').active == true)
			goTeamUI.getChildByName('teamList').getComponent('TeamList').showTeamList(list);
	}

	s2c_team_join() {
		let uilogic = cc.ll.player.getMainUILogic();
		if (uilogic) {
			uilogic.joinTeamWarn();
		}
	}

	s2c_transfer_team_requst(data) {
		let uilogic = cc.ll.player.getMainUILogic();
		if (uilogic) {
			uilogic.transferTeamDlg(data);
		}
	}

	s2c_team_requeslist(data) {
		let list = data.list;
		let goTeamUI = cc.find('Canvas/MainUI/TeamUI');
		if (goTeamUI != null && goTeamUI.getChildByName('requestList').active == true) {
			goTeamUI.getChildByName('requestList').getComponent('TeamRequestList').showRequestList(list);
		}
	}

	s2c_charge(data) {
		if (data.jade)
			cc.ll.player.gameData.jade = data.jade;
		if (data.chargesum)
			cc.ll.player.chargesum = data.chargesum;
		cc.ll.msgbox.addMsg(`玩家充值${data.money}人民币成功!`);
	}

	s2c_prison_time(data) {
		cc.ll.player.changePrisonTime(data.time);
	}

	s2c_player_pos(data) {
		let logic = this.getGameLogic()
		if (logic != null) {
			logic.synPlayerPos(data);
		}
	}

	s2c_aoi_pinfo(data) {
		let logic = this.getGameLogic();
		if (logic != null) {
			// console.log('synPlayerInfo', data.list);
			logic.synPlayerByFrame(data.list);
		}
	}

	s2c_aoi_stop(data) {
		let logic = this.getGameLogic();
		if (logic != null) {
			logic.synPlayerStop(data);
		}
	}

	s2c_aoi_exit(data) {
		let logic = this.getGameLogic();
		if (logic != null) {
			// console.log('s2c_aoi_exit', data);
			logic.synPlayerExit(data);
		}
	}

	s2c_player_data(data) {
		let logic = this.getGameLogic()
		if (logic != null) {
			logic.synPlayerData(data);
		}
	}

	/**
	 * 推送游戏聊天消息
	 * @param {*} data 
	 */
	s2c_game_chat(data) {
		let logic = this.getGameLogic()
		if (logic != null) {
			logic.synChatInfo(data);
		}
	}

	s2c_friends_info(data) {
		let logic = this.getGameLogic()
		if (logic != null) {
			logic.synGetFriendList(data.list);
		}
	}

	s2c_search_friends(data) {
		let logic = this.getGameLogic()
		if (logic != null) {
			logic.synSearchFriendList(data.list);
		}
	}

	s2c_friend_chat(data) {
		let logic = this.getGameLogic()
		if (logic != null) {
			logic.synFriendChatInfo(data);
		}
	}

	s2c_partner_exp(data) {

	}

	s2c_partner_exchange_exp_ok(data) {
		let goChuanGongUI = cc.find('Canvas/MainUI/ChuanGongUI');
		if (goChuanGongUI != null) {
			let logic = goChuanGongUI.getComponent('ChuanGongUI');
			logic.OnChuanGongOK(CMainPlayerInfo.vecPartnerInfo, data);
		}
	}

	s2c_partner_list(data) {

		CMainPlayerInfo.vecPartnerInfo = [];

		for (let i = 0; i < data.vecPartner.length; i++) {
			let strJson = data.vecPartner[i].strJson;
			let stInfo = JSON.parse(strJson);
			CMainPlayerInfo.vecPartnerInfo.push(stInfo);
		}
		CMainPlayerInfo.vecChuZhan = JSON.parse(data.strJsonPos);


		let goPartnerUI = cc.find('Canvas/MainUI/PartnerUI');
		if (goPartnerUI != null)
			goPartnerUI.getComponent('PartnerUI').ShowPartnerList();

		/*
		let schemePanel = cc.find('Canvas/MainUI/SchemePanel');
		if(schemePanel){
			let partnerNode = schemePanel.getChildByName('schemeNode').getChildByName('PartnerNode');
			let partnerLogic = partnerNode.getComponent('SchemePartnerPanel');
			if(partnerLogic){
				partnerLogic.initCardList();
			}

		}
		*/

		let goTeamUI = cc.find('Canvas/MainUI/TeamUI');
		if (goTeamUI != null)
			goTeamUI.getComponent('TeamUI').CreateCardList();
	}

	s2c_partner_info(data) //单个
	{
		let strJson = data.strJson;
		let stInfo = JSON.parse(strJson);

		let nIndex = CMainPlayerInfo.IsHasPartner(stInfo.id);
		if (-1 == nIndex) {
			CMainPlayerInfo.vecPartnerInfo.push(stInfo);
			nIndex = CMainPlayerInfo.vecPartnerInfo.length - 1;
		} else {
			CMainPlayerInfo.vecPartnerInfo[nIndex] = stInfo;
		}

		let goPartnerUI = cc.find('Canvas/MainUI/PartnerUI');
		if (goPartnerUI != null)
			goPartnerUI.getComponent('PartnerUI').ShowPartnerInfo(0, nIndex);

		let goTeamUI = cc.find('Canvas/MainUI/TeamUI');
		if (goTeamUI != null)
			goTeamUI.getComponent('TeamUI').CreateCardList();
	}

	c2s_other_info(data) {
		let goMainUI = cc.find('Canvas/MainUI');
		if (null == goMainUI) {
			return;
		}

		cc.loader.loadRes(`Prefabs/PlayerFunUI`, cc.Prefab, (err, prefab) => {
			let goFunUI = CPubFunction.CreateSubNode(goMainUI, {
				nX: 0,
				nY: 0
			}, prefab, 'PlayerFunUI');
			goFunUI.getComponent('PlayerFunUI').PlayerFunUI_Init2(data.nResID, data.nLevel, data.nRoleID, data.nRelive, data.strName, data.strBangName);
		});
	}

	/**
	 * 服务器推送角色任务列表
	 * @param {*} data 
	 */
	s2c_role_task_list(data) {
		CMainPlayerInfo.OnReceiveRoleTask(data);

		let goTaskTip = cc.find('Canvas/MainUI/right/TaskTip');
		if (goTaskTip)
			goTaskTip.getComponent('TaskTip').IniTip();
	}

	s2c_notice(data) {
		cc.ll.loading.unshowLoading();
		CPubFunction.CreateNotice(cc.find('Canvas'), data.strRichText, 2);
	}

	s2c_screen_msg(data) {
		CPubFunction.CreateScreenNotice(data.strRichText, data.bInsertFront);
	}



	s2c_npc_notice(nNpcConfigID, strText) {
		CPubFunction.CreateNpcNotice(nNpcConfigID, strText);
	}


	s2c_star_waiting(data) {
		CPubFunction.CreateWaitTip('正在选择对手', 2, null);
	}

	s2c_daily_info(data) {
		CPubFunction.FindAndDoUIFunction('DailyUI', 'OnReceiveDailyInfo', data.strJson);
	}

	s2c_you_get_item(data) {
		let nItemName = CPubFunction.GetItemName(data.nItem);

		let strFlag = data.nNum >= 0 ? '+' : '-'

		let strTip = `${nItemName}  ${strFlag}${Math.abs(data.nNum)}`;

		CPubFunction.CreateNotice(cc.find('Canvas'), strTip, 2);
	}

	s2c_create_task_npc(stData) { }


	s2c_paihang(data) {
		let goPaiHangBangUI = cc.find('Canvas/MainUI/PaiHangBangUI');
		if (null == goPaiHangBangUI)
			return;

		let comPaiHangUI = goPaiHangBangUI.getComponent('PaiHangBangUI');
		let info = JSON.parse(data.vecRow);
		comPaiHangUI.OnReceiveVecRow(data.rankKind, info);
	}

	s2c_roles_goods(data) {
		CMainPlayerInfo.vecMyGoods = data.vecGoods;

		CPubFunction.FindAndDoUIFunction('ShopUI', 'OnReceiveMyGoods', 0);
		CPubFunction.FindAndDoUIFunction('ShangJiaUI', 'ShowSellingItem', 0);

	}

	s2c_goods(data) {
		let goShopUI = cc.find('Canvas/MainUI/ShopUI');
		if (null == goShopUI)
			return;

		let comShopUI = goShopUI.getComponent('ShopUI');
		comShopUI.InitGoodList(data.vecGoods);
	}

	s2c_add_exp(data) {
		let str = cc.js.formatStr(StrCommon.AddExp, data.addexp);
		CPubFunction.CreateNotice(cc.find('Canvas'), str, 2);

		if (data.onlyid == cc.ll.player.onlyid) {
			cc.ll.player.changeExp(data.curexp)
		}
	}

	s2c_level_up(data) {
		if (data.onlyid == cc.ll.player.onlyid) {
			cc.ll.player.levelUp(data.curlevel);
		}

	}

	s2c_you_money(nKind, nNum, nAdd) {
		let szKind = ['银两', '仙玉', '绑定仙玉'];

		if (nKind == 0 || nKind == 1 || nKind == 2) {
			let strTip = szKind[nKind] + (nAdd >= 0 ? ` +${nAdd}` : `  ${nAdd}`);
			CPubFunction.CreateNotice(cc.find('Canvas'), strTip, 2);
		}

		switch (nKind) {
			case 0:
				cc.ll.player.gameData.money = nNum;
				break;
			case 1:
				cc.ll.player.gameData.jade = nNum;
				break;
			case 2:
				cc.ll.player.gameData.bindjade = nNum;
				break;
			case 3:
				cc.ll.player.gameData.shuilugj = nNum;
				break;
		}

		CPubFunction.FindAndDoUIFunction('ShopUI', 'SetMoneyInfo', 0);
		CPubFunction.FindAndDoUIFunction('BagPanel', 'SetMoneyInfo', 0);
		CPubFunction.FindAndDoUIFunction('DailyUI', 'SetMoneyInfo', 0);
		CPubFunction.FindAndDoUIFunction('LotteryUI', 'SetMoneyInfo', 0);
		CPubFunction.FindAndDoUIFunction('RolePanel', 'SetMoneyInfo', 0);
		CPubFunction.FindAndDoUIFunction('NpcShopUI', 'SetMoneyInfo', 0);
	}



	s2c_bagitem(data) {
		let logic = this.getGameLogic()
		if (logic != null) {
			logic.synBagItemList(data);
		}
	}
	s2c_mallitems(data) {
		let goShopUI = cc.find('Canvas/MainUI/ShopUI');
		if (null == goShopUI)
			return;

		let comShopUI = goShopUI.getComponent('ShopUI');
		comShopUI.InitMallList(data);

	}

	s2c_npc_shop_item(data) {
		let goNpcShopUI = cc.find('Canvas/MainUI/NpcShopUI');
		if (null == goNpcShopUI)
			return;

		let comNpcShopUI = goNpcShopUI.getComponent('NpcShopUI');
		comNpcShopUI.InitNpcShopGoodsList(data);

	}

	s2c_lottery_info(data) {
		CPubFunction.FindAndDoUIFunction('LotteryUI', 'ShowItemList', data.strJson);
		cc.ll.player.getLogic().playStop();
	}

	s2c_lottery_result(data) {
		CPubFunction.FindAndDoUIFunction('LotteryUI', 'ShowLightMove', data);
	}


	s2c_relive_list(data) {
		CPubFunction.FindAndDoUIFunction('RestoryUI', 'Init', data.strJson);
	}

	s2c_level_reward(data) {
		if (data.level) {
			let item = cc.ll.propData.level[data.level];
			for (let reward of item.reward_item.split(';')) {
				reward = reward.split(':');
				let name = cc.ll.propData.item[reward[0]].name;
				let strTip = `获得${name}x${reward[1]}`;
				CPubFunction.CreateNotice(cc.find('Canvas'), strTip, 2);
			}
		}
	}



	s2c_lockeritem(data) {
		let logic = this.getGameLogic()
		if (logic != null) {
			logic.synLockerItemList(data);
		}
	}

	s2c_incense_state(data) {
		let logic = this.getGameLogic()
		if (logic != null) {
			logic.synIncenseState(data);
		}
	}

	s2c_getbanglist(data) {
		let logic = this.getGameLogic()
		if (logic != null) {
			logic.synBangList(data.list);
		}
	}

	s2c_join_bang(data) {
		let uilogic = cc.ll.player.getMainUILogic();
		if (uilogic) {
			uilogic.joinBangWarn();
		}
	}

	s2c_getbanginfo(data) {
		let logic = this.getGameLogic()
		if (logic != null) {
			logic.synBangInfo(data);
			logic.resetSelfPlayerTitle();
		}
	}

	s2c_getbangrequest(data) {
		let logic = this.getGameLogic()
		if (logic != null) {
			logic.synBangRequest(data);
		}
	}

	s2c_getbangrequest_info(data) {
		let uilogic = cc.ll.player.getMainUILogic();
		if (uilogic) {
			uilogic.bangInfo(data);
		}
	}

	s2c_new_pet(data) {
		let logic = this.getGameLogic()
		if (logic != null) {
			logic.choosePet(data);
		}
	}

	s2c_get_petlist(data) {
		let logic = this.getGameLogic()
		if (logic != null) {
			logic.synPetInfo(data);
		}
	}

	s2c_change_pet(data) {
		let logic = this.getGameLogic()
		if (logic != null) {
			logic.synChangePet(data);
		}
	}

	s2c_del_pet(data) {
		let logic = this.getGameLogic()
		if (logic != null) {
			logic.synDelPet(data);
		}
	}
	s2c_update_pet(data) {
		let logic = this.getGameLogic()
		if (logic != null) {
			logic.synUpdatePet(data);
		}
	}

	s2c_pet_changeSskill(data) {
		let logic = this.getGameLogic()
		if (logic != null) {
			logic.updatePetSSkill(data.petid, data.skillid);
		}
	}

	s2c_wash_petproperty(data) {
		let washnode = cc.find('Canvas/MainUI/PetPanel/washProperty');
		if (washnode) {
			let logic = washnode.getComponent('WashProperty');
			logic.washProperty(data);
		}
	}

	s2c_save_petproperty(data) {
		let washnode = cc.find('Canvas/MainUI/PetPanel/washProperty');
		if (washnode) {
			let logic = washnode.getComponent('WashProperty');
			logic.saveProperty(data);
		}
	}

	s2c_equip_list(data) {
		let logic = this.getGameLogic()
		if (logic != null) {
			logic.synEquipList(data);
		}
	}

	s2c_next_equip(data) {
		let logic = this.getGameLogic()
		if (logic != null) {
			logic.synNextEquip(data);
		}
	}

	s2c_equip_info(data) {
		let logic = this.getGameLogic()
		if (logic != null) {
			logic.synEquipInfo(data);
		}
	}

	s2c_equip_property(data) {
		let logic = this.getGameLogic()
		if (logic != null) {
			logic.synEquipProperty(data);
		}
	}

	s2c_change_weapon(data) {
		let logic = this.getGameLogic()
		if (logic != null) {
			logic.synChangeWeapon(data.weapon);
		}
	}

	s2c_xianqi_list(data) {
		let logic = this.getGameLogic()
		if (logic != null) {
			logic.synXianQiList(data);
		}
	}

	s2c_btl(data) {
		// 检查 水陆大会
		let ShuiLuMgr = require('../game/Activity/ShuiLuDaHui/sldh_mgr');
		ShuiLuMgr.getInstance().destroyShuiLuReadyBtl();

		let gamelogic = this.getGameLogic();
		gamelogic.enterBattle(data.teamS, data.teamE, data.petlist);
	}

	s2c_btl_roundbegin(data) {
		let btllogic = this.getBattleStageLogic();
		if (btllogic) {
			btllogic.btlRoundBegin(data);
		}
	}

	s2c_btl_round(data) {
		let btllogic = this.getBattleStageLogic();
		if (btllogic) {
			btllogic.btlRound(data);
		}
	}

	s2c_btl_end(data) {
		let gamelogic = this.getGameLogic();
		gamelogic.exitBattle(data);
	}

	s2c_btl_act(data) {
		let btllogic = this.getBattleStageLogic();
		if (btllogic) {
			btllogic.playerAction(data);
		}
	}

	s2c_relive(data) {
		let relive_layer = cc.find('Canvas/relivelayer');
		if (relive_layer) {
			relive_layer.destroy();
		}
		if (data.result == MsgCode.SUCCESS) {
			cc.ll.player.setUserInfo(data.info);
			let rolelogic = cc.ll.player.getLogic();
			rolelogic.setInfo(cc.ll.player);

			let logic = this.getGameLogic()
			if (logic != null) {
				logic.synPlayerData(data.data);
			}

			let uilogic = cc.ll.player.getMainUILogic();
			uilogic.setHeadIcon(cc.ll.player.resid);

			let battleui = cc.find('Canvas/BattleUILayer/BattleUI');
			if (battleui != null) {
				let btlUIlogic = battleui.getComponent('BattleUI');
				btlUIlogic.initSkillPanel();
			}
		}
	}

	s2c_changerace(data) {
		let changerace_layer = cc.find('Canvas/changeracelayer');
		if (changerace_layer) {
			changerace_layer.destroy();
		}
		if (data.result == MsgCode.SUCCESS) {
			cc.ll.player.setUserInfo(data.info);

			let rolelogic = cc.ll.player.getLogic();
			rolelogic.setInfo(cc.ll.player);

			let logic = this.getGameLogic()
			if (logic != null) {
				logic.synPlayerData(data.data);
			}

			let uilogic = cc.ll.player.getMainUILogic();
			uilogic.setHeadIcon(cc.ll.player.resid);

			let battleui = cc.find('Canvas/BattleUILayer/BattleUI');
			if (battleui != null) {
				let btlUIlogic = battleui.getComponent('BattleUI');
				btlUIlogic.initSkillPanel();
			}

			// 重置 战斗自动技能
			cc.ll.local.set('battle_r_skill', 0);
		}
	}

	OnMsgBattleOver(stData) {
		CPubFunction.FindAndDeleteNode(this.node, "BattleUI");
	}

	OnMsgAtk(stData) {
		let comBattle = cc.find('Canvas/BattleUILayer/BattleUI').getComponent('Battle');

		let nRoundIndex = stData.round;
		let vecAtk = stData.acts;

		for (const it in vecAtk) {
			let nAtker = vecAtk[it].actid;
			let vecTarget = vecAtk[it].act;
			let nSkill = 21105; //zzzErr ��Ϣ�ṹ��Ķ�

			comBattle.CreateAtkEvent(nAtker, nSkill, vecTarget);
		}

	}

	s2c_hongbao_open() {
		let uilogic = cc.ll.player.getMainUILogic();
		if (uilogic) {
			uilogic.showHongBaoIcon();
		}
	}

	s2c_hongbao_result(data) {
		let uilogic = cc.ll.player.getMainUILogic();
		if (uilogic) {
			uilogic.hideHongBaoPanel();
			uilogic.hideHongBaoIcon();
		}

		if (data.errorcode != MsgCode.SUCCESS) {
			cc.ll.msgbox.addMsg(data.errorcode);
		}
	}

	s2c_remunerate(data) {
		if (data.errorcode != MsgCode.SUCCESS) {
			cc.ll.msgbox.addMsg(data.errorcode);
		}
	}


	// 水陆大会
	s2c_shuilu_sign(data) {
		if (data.errorcode == MsgCode.SUCCESS) {
			CPubFunction.CreateNotice(cc.find('Canvas'), '已报名，请不要离开皇宫，离开即视为放弃！', 2);
			let ShuiLuMgr = require('../game/Activity/ShuiLuDaHui/sldh_mgr');
			ShuiLuMgr.getInstance().changeState(data.shuilustate);
		} else {
			cc.ll.msgbox.addMsg(data.errorcode);
		}
	}

	s2c_shuilu_unsign(data) {
		if (data.errorcode == MsgCode.SUCCESS) {
			CPubFunction.CreateNotice(cc.find('Canvas'), '离开皇宫，自动放弃水陆大会', 2);
			let ShuiLuMgr = require('../game/Activity/ShuiLuDaHui/sldh_mgr');
			ShuiLuMgr.getInstance().destroyShuiLuIcon();
			ShuiLuMgr.getInstance().destroyShuiLuPanel();
		} else {
			cc.ll.msgbox.addMsg(data.errorcode);
		}
	}

	s2c_shuilu_info(data) {
		let ShuiLuMgr = require('../game/Activity/ShuiLuDaHui/sldh_mgr');
		ShuiLuMgr.getInstance().showShuiLuPanel(data);
	}


	s2c_world_reward_list(data) {
		let worldReward = require('../game/WorldRewardMgr');
		worldReward.showLoadList(data);
	}


	s2c_shuilu_match(data) {
		let ShuiLuMgr = require('../game/Activity/ShuiLuDaHui/sldh_mgr');
		ShuiLuMgr.getInstance().showReadyBattle(data);
	}

	s2c_shuilu_state(data) {
		let ShuiLuMgr = require('../game/Activity/ShuiLuDaHui/sldh_mgr');
		ShuiLuMgr.getInstance().changeState(data.slstate);
	}

	s2c_shuilu_battleres(data) {
		let ShuiLuMgr = require('../game/Activity/ShuiLuDaHui/sldh_mgr');
		ShuiLuMgr.getInstance().showBtlEnd(data);
	}

	/**角色称谓 */
	s2c_title_info(data) {
		let SetRoleTitleUIMgr = require('../game/SetRoleTitleUIMgr');
		SetRoleTitleUIMgr.showTitleList(data);
	}

	s2c_title_change(data) {
		let SetRoleTitleUIMgr = require('../game/SetRoleTitleUIMgr');
		SetRoleTitleUIMgr.onRoleTitleChanged(data);
	}
	//退出帮派
	s2c_leavebang(data) {
		let logic = this.getGameLogic()
		if (logic != null && data.ecode == 0) {
			logic.clearSelfRoleTitleInfo();
			logic.closeBangList();
			logic.resetSelfPlayerTitle();
		}
	}

	s2c_linghou_fight(data) {
		if (data.errorcode != MsgCode.SUCCESS) {
			// cc.ll.msgbox.addMsg(data.ecode);
			CPubFunction.CreateNotice(cc.find('Canvas'), data.ecode, 2);
		}
	}
	//更新单个商品信息
	s2c_update_shop_info(data) {
		let npcShopUI = cc.find('Canvas/MainUI/NpcShopUI');
		if (npcShopUI) {
			let npcShopUIScript = npcShopUI.getComponent('NpcShopUI');
			npcShopUIScript.updateGoodsInfo(data);
		}
	}

	s2c_palace_fight(data) {
		console.log('s2c_palace_fight', data);
		let pficon = cc.find('Canvas/MainUI/PalaceFightIcon');
		if (pficon) {
			if (data.type == 1) {
				if (data.recipient.state == 2 && data.sponsor.state == 1) { // 有玩家放弃决斗
					let strRichText = `<color=#00FF00>玩家</c><color=#FFFF00>[${data.recipient.name}]${data.recipient.roleid}</c><color=#00FF00>放弃了玩家</c><color=#FFFF00>[${data.sponsor.name}]${data.sponsor.roleid}</c><color=#00FF00>与玩家</c><color=#FFFF00>[${data.recipient.name}]${data.recipient.roleid}</c><color=#00FF00>的皇城决斗，真是耸！</c>`;
					cc.find('Canvas/PalaceNotice').getComponent('PalaceNotice').addStrToList(strRichText);
				}
				if (data.recipient.state == 0 && data.sponsor.state == 1) { // 有玩家决斗
					let strRichText = '';
					if (data.msg.length > 0) {
						strRichText = `<color=#00FF00>玩家</c><color=#FFFF00>[${data.sponsor.name}]${data.sponsor.roleid}</c><color=#00FF00>邀请玩家</c><color=#FFFF00>[${data.recipient.name}]${data.recipient.roleid}</c><color=#00FF00>进行皇城决斗，并写下战书：</c><color=#FF0000>${data.msg}</c>`;
					} else {
						strRichText = `<color=#00FF00>玩家</c><color=#FFFF00>[${data.sponsor.name}]${data.sponsor.roleid}</c><color=#00FF00>邀请玩家</c><color=#FFFF00>[${data.recipient.name}]${data.recipient.roleid}</c><color=#00FF00>进行皇城决斗！</c>`;
					}
					cc.find('Canvas/PalaceNotice').getComponent('PalaceNotice').addStrToList(strRichText);
				}

				if (data.win == 1) { // 决斗结果
					let strRichText = `<color=#00FF00>玩家</c><color=#FFFF00>[${data.sponsor.name}]${data.sponsor.roleid}</c><color=#00FF00>与玩家</c><color=#FFFF00>[${data.recipient.name}]${data.recipient.roleid}</c><color=#00FF00>的皇城决斗，玩家</c><color=#FFFF00>[${data.sponsor.name}]${data.sponsor.roleid}</c><color=#00FF00>取得了胜利，玩家</c><color=#FFFF00>[${data.recipient.name}]${data.recipient.roleid}</c><color=#00FF00>被打败！</c>`;
					cc.find('Canvas/PalaceNotice').getComponent('PalaceNotice').addStrToList(strRichText);
				} else if (data.win == 2) {
					let strRichText = `<color=#00FF00>玩家</c><color=#FFFF00>[${data.sponsor.name}]${data.sponsor.roleid}</c><color=#00FF00>与玩家</c><color=#FFFF00>[${data.recipient.name}]${data.recipient.roleid}</c><color=#00FF00>的皇城决斗，玩家</c><color=#FFFF00>[${data.recipient.name}]${data.recipient.roleid}</c><color=#00FF00>取得了胜利，玩家</c><color=#FFFF00>[${data.sponsor.name}]${data.sponsor.roleid}</c><color=#00FF00>被打败！</c>`;
					cc.find('Canvas/PalaceNotice').getComponent('PalaceNotice').addStrToList(strRichText);
				}
			}
			let logic = pficon.getComponent('PalaceFightIcon');
			if (data.recipient.roleid == cc.ll.player.roleid || data.sponsor.roleid == cc.ll.player.roleid) {
				logic.syncPalaceFightData(data);
			}
		}
	}

	s2c_palace_rolelist(data) {
		let palaceFightIcon = cc.find('Canvas/MainUI/PalaceFightIcon');
		let iconlogic = palaceFightIcon.getComponent('PalaceFightIcon');
		if (iconlogic.canShowPalaceFightPanel() && !cc.find('Canvas/MainUI/PalaceFightPanel')) {
			cc.loader.loadRes('Prefabs/PalaceFightPanel', cc.Prefab, (err, prefab) => {
				let palaceFightPanel = cc.instantiate(prefab);
				palaceFightPanel.parent = cc.find('Canvas/MainUI');
				let palaceLogic = palaceFightPanel.getComponent('PalaceFightPanel');
				palaceLogic.showTwoList(data);
				palaceLogic.showCloseButton(iconlogic.canClosePalaceFight());
			});
		}
	}

	s2c_relation_apply_res(data) {
		if (data.ecode == MsgCode.SUCCESS) {
			let inputUI = cc.find('Canvas/MainUI/RelationInputUI');
			if (inputUI) {
				inputUI.destroy();
			}
		} else {
			let errStr = '申请结拜关系错误';
			if (data.errorMsg && data.errorMsg != '') {
				switch (Number(data.errorMsg)) {
					case 1:
						errStr = '名称已经存在，请重新输入';
						break;
					case 2:
						errStr = '当前结拜关系已存在';
						break;
					case 3:
						errStr = '结拜人数超过上限';
						break;
					case 4:
						errStr = '每人最多与他人结拜三次';
						break;
					case 5:
						errStr = '所有成员必须在线，才能结拜';
						break;
				}

			}
			CPubFunction.CreateNotice(cc.find('Canvas'), errStr, 2);
		}
	}

	s2c_relation_apply_info(data) {
		let mainUILogic = cc.ll.player.getMainUILogic();
		if (mainUILogic) {
			mainUILogic.onOpenRelationApplyUI(data);
		}
	}

	s2c_relation_apply_answer(data) {
		let relationApplyUI = cc.find('Canvas/MainUI/RelationApplyUI');
		if (relationApplyUI) {
			let relationApplyLogic = relationApplyUI.getComponent('RelationApplyPanel');
			if (relationApplyLogic) {
				relationApplyLogic.updateAgreeInfo(data);
			}

		}
	}

	s2c_relation_created(data) {
		if (data.ecode == MsgCode.SUCCESS) {
			let applyUI = cc.find('Canvas/MainUI/RelationApplyUI');
			if (applyUI)
				applyUI.destroy();

			let listUI = cc.find('Canvas/MainUI/RelationListUI');
			if (listUI)
				listUI.destroy();



			CPubFunction.CreateNotice(cc.find('Canvas'), '恭喜结拜成功', 2);

			//播放场景动画效果
			cc.loader.loadRes('Prefabs/RelationShowUI', cc.Prefab, (err, prefab) => {
				let createShowUI = CPubFunction.CreateSubNode(cc.find('Canvas/MainUI'), {
					nX: 0,
					nY: 0
				}, prefab, 'RelationShowUI');
				if (createShowUI) {
					createShowUI.getComponent('RelationShowPanel').playBrotherCreateShow(data);
				}
			});
		} else {
			CPubFunction.CreateNotice(cc.find('Canvas'), data.errorMsg, 2);
		}
	}

	s2c_relation_List(data) {
		if (data.ecode == MsgCode.SUCCESS) {
			let listUILogic = cc.find('Canvas/MainUI/RelationListUI').getComponent('RelationListPanel');
			if (listUILogic) {
				listUILogic.initRelationList(data);
			}
		} else {
			CPubFunction.CreateNotice(cc.find('Canvas'), data.errorMsg, 2);
		}
	}

	s2c_relation_leave(data) {
		if (data.ecode == MsgCode.SUCCESS) {
			let listUI = cc.find('Canvas/MainUI/RelationListUI');
			if (listUI) {
				let listUILogic = listUI.getComponent('RelationListPanel');
				if (listUILogic) {
					listUILogic.leaveRelation(data);
				}
			}

			if (data.leaveRoleId == cc.ll.player.roleid) {
				CPubFunction.CreateNotice(cc.find('Canvas'), '你已脱离结拜关系:' + data.relationName, 2);
				if (data.titleId == cc.ll.player.titleid) {
					cc.ll.player.titleid = -1;
					cc.ll.player.titletype = -1;
					cc.ll.player.titleval = '';

					let logic = this.getGameLogic();
					if (logic != null)
						logic.resetSelfPlayerTitle();
				}
			}
		} else {
			CPubFunction.CreateNotice(cc.find('Canvas'), data.errorMsg, 2);
		}
	}

	s2c_relation_destroy(data) {
		console.log('关系删除');
		if (data.ecode == MsgCode.SUCCESS) {
			let listUI = cc.find('Canvas/MainUI/RelationListUI');
			if (listUI) {
				let listUILogic = listUI.getComponent('RelationListPanel');
				if (listUILogic) {
					listUILogic.deleteRelationItem(data);
				}
			}

			if (data.titleId == cc.ll.player.titleid) {
				cc.ll.player.titleid = -1;
				cc.ll.player.titletype = -1;
				cc.ll.player.titleval = '';
				let logic = this.getGameLogic()
				if (logic != null)
					logic.resetSelfPlayerTitle();

			}

		} else {
			CPubFunction.CreateNotice(cc.find('Canvas'), data.errorMsg, 2);
		}
	}

	s2c_relation_reject(data) {
		let relationApplyUI = cc.find('Canvas/MainUI/RelationApplyUI');
		if (relationApplyUI) {
			CPubFunction.CreateNotice(cc.find('Canvas'), '结拜操作需要所有成员都同意', 2);
			relationApplyUI.destroy();
		}
	}


	s2c_scheme_List(data) {
		let schemePanel = cc.find('Canvas/MainUI/SchemePanel');
		if (schemePanel) {
			let schemeListLogic = schemePanel.getChildByName('schemeNode').getComponent('SchemeListPanel');
			if (schemeListLogic)
				schemeListLogic.initSchemeList(data);
		}
	}

	s2c_scheme_create(data) {
		if (data.ecode == MsgCode.SUCCESS) {
			let schemePanel = cc.find('Canvas/MainUI/SchemePanel');
			if (schemePanel) {
				let schemeListLogic = schemePanel.getChildByName('schemeNode').getComponent('SchemeListPanel');
				if (schemeListLogic) {
					schemeListLogic.appendSchemeList(data);
				}

			}
		} else {
			CPubFunction.CreateNotice(cc.find('Canvas'), '名称重复，请重新输入', 2);
		}
	}

	s2c_scheme_update(data) {

	}

	s2c_scheme_info(data) {
		if (data.ecode == MsgCode.SUCCESS) {
			let schemePanel = cc.find('Canvas/MainUI/SchemePanel');
			if (schemePanel) {
				let schemeInputUI = schemePanel.getChildByName('schemeNode').getChildByName('SchemeInputUI');
				if (schemeInputUI)
					schemeInputUI.active = false;

				let listpanel = schemePanel.getChildByName('schemeNode');
				listpanel.active = true;
				let schemeLogic = listpanel.getComponent('SchemeListPanel');
				if (schemeLogic) {
					schemeLogic.setSchemeInfo(data);
				}
			}

		} else {
			CPubFunction.CreateNotice(cc.find('Canvas'), '属性方案信息有误', 2);
		}
	}

	s2c_scheme_updateEquip(data) {
		if (data.ecode == MsgCode.SUCCESS) {
			let schemePanel = cc.find('Canvas/MainUI/SchemePanel');
			if (schemePanel) {
				let equipsNode = schemePanel.getChildByName('schemeNode').getChildByName('EquipsNode');
				let equipsLogic = equipsNode.getComponent('SchemeEquipsPanel');
				if (equipsLogic) {
					equipsLogic.updateEquips(data);
				}

			}
		} else {
			CPubFunction.CreateNotice(cc.find('Canvas'), '属性方案装备更新失败', 2);
		}
	}

	s2c_scheme_changeName(data) {
		if (data.ecode == MsgCode.SUCCESS) {
			let schemePanel = cc.find('Canvas/MainUI/SchemePanel');
			if (schemePanel) {
				let schemeInputUI = schemePanel.getChildByName('schemeNode').getChildByName('SchemeInputUI');
				if (schemeInputUI)
					schemeInputUI.active = false;


				let schemeLogic = schemePanel.getChildByName('schemeNode').getComponent('SchemeListPanel');
				if (schemeLogic) {
					schemeLogic.setSchemeName(data);
				}
			}
		} else {
			CPubFunction.CreateNotice(cc.find('Canvas'), '属性方案改名失败', 2);
		}
	}

	s2c_scheme_use(data) {
		if (data.ecode == MsgCode.SUCCESS) {
			let schemePanel = cc.find('Canvas/MainUI/SchemePanel');
			if (schemePanel) {
				let schemeUseUI = schemePanel.getChildByName('schemeNode').getChildByName('SchemeUseUI');
				if (schemeUseUI)
					schemeUseUI.active = false;


				let schemeLogic = schemePanel.getChildByName('schemeNode').getComponent('SchemeListPanel');
				if (schemeLogic) {
					schemeLogic.useScheme(data);
				}
			}
		} else {
			CPubFunction.CreateNotice(cc.find('Canvas'), '属性方案改名失败', 2);
		}
	}


	s2c_scheme_activate(data) {
		if (data.ecode == MsgCode.SUCCESS) {
			let schemePanel = cc.find('Canvas/MainUI/SchemePanel');
			if (schemePanel) {

				let schemeLogic = schemePanel.getChildByName('schemeNode').getComponent('SchemeListPanel');
				if (schemeLogic) {
					schemeLogic.onActivateScheme(data);
				}
			}
		}
	}

	s2c_scheme_resetXiulianPoint(data) {
		if (data.ecode == MsgCode.SUCCESS) {
			let schemePanel = cc.find('Canvas/MainUI/SchemePanel');
			if (schemePanel) {
				let kangxingNode = schemePanel.getChildByName('schemeNode').getChildByName('kangxingNode');
				let kangxingLogic = kangxingNode.getComponent('SchemeDefendPanel');
				if (kangxingLogic) {
					kangxingLogic.onResetXiuLianPoint();
				}

			}
		} else {
			cc.ll.msgbox.addMsg(data.errorMsg);
		}
	}


	s2c_change_role_color(data) {
		cc.ll.player.color1 = data.color1;
		cc.ll.player.color2 = data.color2;
		// console.log('s2c_change_role_color', data);
		let gamelogic = this.getGameLogic();
		let role = cc.ll.player.getNode();
		if (role) {
			let logic = role.getComponent('role');
			logic.showColorMask();
			let name = logic.getCurAnimateName();
			if (name) {
				logic.playAnimation(name);
			}
		}
		let coloringRolePanel = cc.find('Canvas/MainUI/ColoringRolePanel');
		if (coloringRolePanel) {
			let logic = coloringRolePanel.getComponent('ColoringRolePanel');
			logic.setCurColor();
		}
	}

	s2c_bell_msg(data) {
		let str = `<color=#00FF00>玩家</c><color=#FFFF00>[${data.name}]${data.roleid}</c> : <color=#00FF00>${data.msg}</c>`;
		cc.find('Canvas/PalaceNotice').getComponent('PalaceNotice').addStrToList(str);
	}

	s2c_safepass_msg(data) {
		cc.ll.player.safe_password = data.pass;
		cc.ll.player.safe_lock = data.lock;
		let set_panel = cc.find('Canvas/SetPanel');
		if (set_panel) {
			let safe_logic = cc.find('SafeNode', set_panel).getComponent('SafeNode');
			safe_logic.init();
		}
	}


}

module.exports = Net;
