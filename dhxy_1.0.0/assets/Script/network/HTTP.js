var StrCommon = require('../etc/str_common');

var HTTP = cc.Class({
    extends: cc.Component,

    statics: {
        sessionId: 0,
        accountId: 0,
        url: '',
        send: function (path, data, callback, extraUrl) {
            var xhr = cc.loader.getXMLHttpRequest();
            xhr.timeout = 30 * 1000;
            var str = "?";
            for (var k in data) {
                if (str != "?") {
                    str += "&";
                }
                str += k + "=" + data[k];
            }
            if (extraUrl == null) {
                extraUrl = HTTP.url;
            }
            var requestURL = extraUrl + path + encodeURI(str);
            console.log("RequestURL:" + requestURL);
            xhr.open("GET", requestURL, true);
            if (cc.sys.isNative) {
                xhr.setRequestHeader("Accept-Encoding", "gzip,deflate", "text/html;charset=UTF-8");
            }

            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status >= 200 && xhr.status < 300) {
                        console.log("http res(" + xhr.responseText.length + "):" + xhr.responseText);
                        try {
                            var ret = JSON.parse(xhr.responseText);
                            if (callback !== null) {
                                callback(ret);
                            } /* code */
                        } catch (e) {
                            console.log("err:" + e);
                            //callback(null);
                        } finally {
                           
                        }
                    } else {
                        cc.ll.msgbox.addMsg(StrCommon.NetWorkError);
                        cc.ll.loading.unshowLoading();
                    }
                }
            };

            if (cc.ll && cc.ll.wc) {
                //cc.ll.wc.show();
            }
            xhr.send();
            return xhr;
        },

        sendurl: function (callback, extraUrl) {
            var xhr = cc.loader.getXMLHttpRequest();
            xhr.timeout = 5000;
            console.log("RequestURL:" + extraUrl);
            xhr.open("GET", extraUrl, true);
            if (cc.sys.isNative) {
                xhr.setRequestHeader("Accept-Encoding", "gzip,deflate", "text/html;charset=UTF-8");
            }

            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status >= 200 && xhr.status < 300) {
                        console.log("http res(" + xhr.responseText.length + "):" + xhr.responseText);
                        try {
                            var ret = JSON.parse(xhr.responseText);
                            if (callback !== null) {
                                callback(ret);
                            } /* code */
                        } catch (e) {
                            console.log("err:" + e);
                            //callback(null);
                        } finally {
                            if (cc.ll && cc.ll.wc) {
                                //       cc.ll.wc.hide();    
                            }
                        }
                    } else {
                        cc.ll.msgbox.addMsg(StrCommon.NetWorkError);
                        cc.ll.loading.unshowLoading();
                    }
                }
            };

            if (cc.ll && cc.ll.wc) {
                //cc.ll.wc.show();
            }
            xhr.send();
            return xhr;
        },
    },
});
