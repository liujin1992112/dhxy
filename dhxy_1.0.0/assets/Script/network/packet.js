/**
 * 协议包
 */
// let pako = require('../3rdparty/pako');
var base64 = require('./base64');
var ByteBuffer = require('../3rdparty/bytebuffer');

class packet {
    constructor(template, s2s = false) {
        if (cc.pbroot == null) {
            return;
        }
        this.msg_type = 'c2s';
        this.template = template;
        this.msg = cc.pbroot.lookupType(`commander.${this.msg_type}.${this.template}`);
        this.pack = null;
    }

    toBase64(content) {
        return base64.encode(content);
    }

    fromBase64(content) {
        return base64.decode(content);
    }


    create(obj) {
        this.pack = this.msg.create(obj);
    }

    formatBuffer(buffer) {
        var bufferArray = Object.keys(buffer).map(function (k) {
            return buffer[k];
        })
        return bufferArray
    }

    todata(buffer) {
        // let buff = this.formatBuffer(buffer);
        // buffer = this.fromBase64(buffer);
        // buffer = JSON.parse(pako.inflate(buffer, { to: 'string' }));
        // try {
        //     buffer = JSON.parse(buffer);
        // } catch (error) {}
     
        
        // buffer.to
        // let data = this.formatBuffer(buffer);
        if(buffer == null){
            return null;
        }
        return this.msg.decode(buffer);
    }

    tobuffer() {
        let buff = this.msg.encode(this.pack).finish();
        // buff = this.formatBuffer(buff);
        // let bufferlength = this.template.length + 4 + 4 + buff.length;
        
        let buffer = new ByteBuffer();
        buffer.writeShort(this.template.length);
        buffer.writeString(this.template);
        buffer.append(buff);
        buffer.flip();

        // buff = JSON.stringify(buff);
        // buff = pako.deflate(JSON.stringify(buff), { to: 'string' });
        // buff = this.toBase64(buff);
        return buffer.toBuffer(); // ;
    }
}

module.exports = packet;