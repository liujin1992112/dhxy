﻿let GameRes = require('./GameRes');
let CPubFunction = require('./PubFunction');
let CMainPlayerInfo = require('./MainPlayerInfo');
var GameDefine = require('./GameDefine');




cc.Class({
    extends: cc.Component,


    properties:
        {
        },


    onLoad()
    {
        this.nSeleLive = 0;

        this.vecOldInfo = [];
        this.vecLiveInfo = [];
        this.vecBtnLive = [];
        this.vecBtnRace = [];
        this.vecBtnSex = [];
    },

    start()
    {
        for (let nLive = 0; nLive <= 2; nLive++)
        {
            let goBtnLive = cc.find(`btnLive_${nLive}`, this.node);
            goBtnLive.getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "RestoryUI", "OnClickLive", nLive));
            this.vecBtnLive.push(goBtnLive);
        }

        for (let nRace = 1; nRace <= 4; nRace++)
        {
            let goBtnRace = cc.find(`btnRace_${nRace}`, this.node);
            goBtnRace.getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "RestoryUI", "OnSelecetRace", nRace));
            this.vecBtnRace.push(goBtnRace);
        }

        for (let nSex = 1; nSex <= 2; nSex++)
        {
            let btnSex = cc.find(`nodRole/btnSex${nSex}`, this.node);
            btnSex.getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "RestoryUI", "OnSelecetSex", nSex));
            this.vecBtnSex.push(btnSex);
        }


        cc.find('btnClose', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "RestoryUI", "Close", 0));
        cc.find('btnOK', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "RestoryUI", "OnOK", 0));

        this.UpdateAvatar(cc.ll.player.race, cc.ll.player.sex, cc.ll.player.relive);



        cc.ll.net.send('c2s_ask_relive_list', {});
    },


    Init(strJson)
    {
        let vecTmp = JSON.parse(strJson);

        for (let i = 0; i < 3; i++)
        {
            this.vecLiveInfo.push({ nRace: vecTmp[i][0], nSex: vecTmp[i][1], nLive: i });
            this.vecOldInfo.push({ nRace: vecTmp[i][0], nSex: vecTmp[i][1], nLive: i });
        }

        this.OnClickLive(0, 0);
    },



    UpdateAvatar( nRace,nSex, nLive )
    {

        let nResID = CPubFunction.GetResID(nRace, nSex, nLive );
        CPubFunction.SetAnimation(cc.find('nodRole/anmRole', this.node), nResID, 'stand_1'); //zzzErr 需整理资源

        let picName = cc.ll.propData.role.data[nResID].name;
        CPubFunction.SetSpriteFrame(cc.find('nodRole/picRaceName/roleName', this.node), 'Login/ui_login_roletxt', picName);

        this.SetAtribInfo(cc.find('nodCur', this.node), this.vecOldInfo[this.nSeleLive]);
        this.SetAtribInfo(cc.find('nodNew', this.node), this.vecLiveInfo[this.nSeleLive]);
    },


    SetAtribInfo(goNod, stLiveInfo )
    {
        if (null == goNod || null == stLiveInfo)
            return;

        let vecSex = ['男', '女'];
        let vecRace = ['人', '仙', '魔', '鬼'];
        let strName = `${vecSex[stLiveInfo.nSex - 1]}${vecRace[stLiveInfo.nRace - 1]}`;

        cc.find('picTitle/Label', goNod).getComponent(cc.Label).string = strName;

        let stReliveAttr = {};

        if (stLiveInfo.nLive == 0)
            stReliveAttr = GameDefine.reliveFixAttr1;

        if (stLiveInfo.nLive == 1)
            stReliveAttr = GameDefine.reliveFixAttr2;

        if (stLiveInfo.nLive == 2)
            stReliveAttr = GameDefine.reliveFixAttr3;

        let mapAttr = stReliveAttr[stLiveInfo.nRace][stLiveInfo.nSex];

        let strText = '';

        let nCnt = 0;

        for (var it in mapAttr)
        {
            nCnt++;

            let strKey = GameDefine.AttrTypeL1Str[it];
            let nValue = mapAttr[it];
            strText += `+${strKey}  ${nValue}%`;

            if (nCnt % 2 == 0)
                strText += '\n';
            else
                strText += '        ';

        }

        cc.find('picDetil/Label', goNod).getComponent(cc.Label).string = strText;


    },


    GetLiveInfo(nIndex)
    {
        if (nIndex < 0 || nIndex > 2)
            return null;

        if (this.vecLiveInfo[nIndex].nRace == 0)
            return null;

        return this.vecLiveInfo[nIndex];
    },


    OnClickLive(e, nLive)
    {
        let pInfo = this.GetLiveInfo(nLive );
        if (null == pInfo)
            return;

        this.nSeleLive = nLive;
        CPubFunction.ChangeTeamButtonState(this.vecBtnLive, 'Common/ui_common', 'ui_common_btn_tab_subheading0', 'ui_common_btn_tab_subheading1', nLive);

        this.OnSelecetRace(0, this.vecLiveInfo[nLive].nRace);

    },

    OnSelecetRace(e, nRace)
    {
        let pInfo = this.GetLiveInfo(this.nSeleLive);
        if (null == pInfo)
            return;

        if (cc.ll.player.relive <= this.nSeleLive)
        {
            CPubFunction.CreateNotice(cc.find('Canvas/MainUI'), '回梦丹只能修改前世属性', 2)
            return;
        }


        pInfo.nRace = nRace;
        CPubFunction.ChangeTeamButtonState(this.vecBtnRace, 'Common/ui_common', 'ui_common_partner_roll1', 'ui_common_exp_guangquan_back', nRace - 1);

        this.OnSelecetSex(0,  pInfo.nSex);
    },

    OnSelecetSex(e, nSex)
    {
        let pInfo = this.GetLiveInfo(this.nSeleLive);
        if (null == pInfo)
            return;

        if (cc.ll.player.relive <= this.nSeleLive)
        {
            CPubFunction.CreateNotice(cc.find('Canvas/MainUI'), '回梦丹只能修改前世属性', 2)
            return;
        }

        pInfo.nSex = nSex;

        this.UpdateAvatar(pInfo.nRace, pInfo.nSex, pInfo.nLive);

        for (var it in this.vecBtnSex)
        {
            if (it == nSex - 1)
            {
                cc.find('light', this.vecBtnSex[it]).active = true;
            }
            else
            {
                cc.find('light', this.vecBtnSex[it]).active = false;
            }
        }
    },


    

    Close()
    {
        this.node.destroy();
    },

    OnOK(e, d)
    {
        let strJson = JSON.stringify(this.vecLiveInfo);
        cc.ll.net.send('c2s_change_relive_list', { strJson: strJson });
    }




});
