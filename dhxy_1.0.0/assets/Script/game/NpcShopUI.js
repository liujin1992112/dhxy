﻿let GameRes = require('./GameRes');
let CPubFunction = require('./PubFunction');
let GameDefine = require('./GameDefine');

cc.Class({
    extends: cc.Component,


    properties:{
        btnMallInfo:cc.Prefab,
        NumPad:cc.Prefab,
    },


    onLoad() {
        this.nNpcConfigID = 0;
        this.vecTmpGoods = [];
        this.vecGoodsInfo = [];

        this.pGoodsInfo = null;
        this.nBuyCnt = 0;
        this.selectedGoodsQuantity = 10000;
        this.selectedGoods = 0;
    },

    start() {
        cc.find('btnClose', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "NpcShopUI", "Close", 0));
        cc.find('picFrame/nodDetail/nodNum/btnPad', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "NpcShopUI", "OpenNumPad", 0));
        cc.find('picFrame/nodDetail/nodNum/btnAdd', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "NpcShopUI", "ChangeBuyCnt", 1));
        cc.find('picFrame/nodDetail/nodNum/btnCut', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "NpcShopUI", "ChangeBuyCnt", -1));

        cc.find('picFrame/nodDetail/btnOK', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "NpcShopUI", "OnBuy", 0));

        this.vecMutexNode = [cc.find('picFrame/picTip', this.node), cc.find('picFrame/nodDetail', this.node)];
        CPubFunction.ChangeTeamNodeState(this.vecMutexNode, 0);



        cc.ll.net.send('c2s_ask_npc_shop_item', { nNpcConfigID: this.nNpcConfigID });

    },

    Close() {
        this.node.destroy();
    },

    GetMoneyIconFile(nKind) {
        let strMoneyIcon = 'ui_common_icon_yinliang';
        switch(nKind){
            case 0:{strMoneyIcon = 'ui_common_icon_yinliang';break;}
            case 1:{strMoneyIcon = 'ui_common_icon_xianyu';break;}
            case 3:{strMoneyIcon = 'ui_common_score_shuilu';break;}
            default:
                strMoneyIcon = 'ui_common_icon_yinliang';
        }
        return strMoneyIcon;
    },

    updateGoodsInfo(updateItem){
        let goContent = cc.find('scvGoods/view/content', this.node);
        let childrens = goContent.getChildren();
        for (const child of childrens) {
            var showId = cc.find('lblItemID', child).getComponent(cc.Label).string;
            if(showId == updateItem.nItemID){
                cc.find('labQuantity', child).getComponent(cc.Label).string = updateItem.quantity;
                break;
            }
        }

    },

    SetGoodsInfo(goItem, pInfo) {
        let stConfigInfo = {};
        if (pInfo.type && pInfo.type == 'weapon') {
            let equipdata = cc.ll.propData.equip[pInfo.itemid];
            stConfigInfo.name = equipdata.Name;
            stConfigInfo.icon = equipdata.Shape;
        }
        else {
            stConfigInfo = cc.ll.propData.item[pInfo.itemid];
        }

        cc.find('nodItemInfo/Icon', goItem).getComponent(cc.Sprite).spriteFrame = GameRes.getItemIcon(stConfigInfo.icon);
        cc.find('labName', goItem).getComponent(cc.Label).string = stConfigInfo.name;
        cc.find('picText/Label', goItem).getComponent(cc.Label).string = pInfo.price;
        cc.find('lblItemID', goItem).getComponent(cc.Label).string = pInfo.itemid;
        if(pInfo.quantity != null){
           var lblQuantity = cc.find('labQuantity', goItem)
           lblQuantity.getComponent(cc.Label).string = pInfo.quantity ;
           lblQuantity.active = true;

        }
        

        CPubFunction.SetSpriteFrame(cc.find('picText/picYinLiang', goItem), 'Common/ui_common', this.GetMoneyIconFile(pInfo.moneykind));
    },

    InitNpcShopGoodsList(data) {
        let tmpData = JSON.parse(data.info);
        this.vecGoodsInfo = tmpData.goods;

        let goContent = cc.find('scvGoods/view/content', this.node);
        goContent.destroyAllChildren();

        CPubFunction.DestroyVecNode(this.vecTmpGoods);

        let stStart = { nX: 125, nY: -48 };


        for (var nIndex in this.vecGoodsInfo) {
            let stPos = { nX: stStart.nX + (nIndex % 2) * 245, nY: stStart.nY - Math.floor(nIndex / 2) * 82 };
            let goItem = CPubFunction.CreateSubNode(goContent, stPos, this.btnMallInfo, '');
            goItem.getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "NpcShopUI", 'OnClickGoods', nIndex));
            this.SetGoodsInfo(goItem, this.vecGoodsInfo[nIndex])
            this.vecTmpGoods.push(goItem);
        }

        this.OnClickGoods(0,this.selectedGoods);
        
        goContent.height = Math.max(goContent.height, Math.ceil((this.vecGoodsInfo.length + 1) / 2) * 80 + 60);
    },


    OnClickGoods(e, nIndex) {
        CPubFunction.ChangeTeamNodeState(this.vecMutexNode, 1);
        CPubFunction.ChangeTeamButtonState(this.vecTmpGoods, 'Common/ui_common', 'ui_common_public_list0', 'ui_common_public_list2', nIndex);

        this.pGoodsInfo = this.vecGoodsInfo[nIndex];
        let pItemInfo = {};
       
        this.selectedGoods = nIndex;

        if (this.pGoodsInfo.type && this.pGoodsInfo.type == 'weapon') {
            let equipdata = cc.ll.propData.equip[this.pGoodsInfo.itemid];
            pItemInfo.name = equipdata.Name;
            pItemInfo.icon = equipdata.Shape;
            pItemInfo.description = equipdata.Detail;
            let indexarr = ['武器', '项链', '衣服', '头盔', '鞋子'];
            let sexarr = ['男', '女'];
            let racearr = ['人', '仙', '魔', '鬼'];
            pItemInfo.description += '\n' + `【装备部位】${indexarr[equipdata.Index - 1]}`;
            if (equipdata.Sex == 9 || equipdata.Race == 9) {
                pItemInfo.description += '\n' + '【装备适用】通用';
            }
            else if (equipdata.OwnerRoleId && equipdata.OwnerRoleId > 0) {
                pItemInfo.description += '\n' + `【装备适用】${GameDefine.roleName[equipdata.OwnerRoleId]}`;
            } else {
                pItemInfo.description += '\n' + `【装备适用】${sexarr[equipdata.Sex - 1]}${racearr[equipdata.Race - 1]}`;
            }
            pItemInfo.description += '\n' + `【等级要求】${equipdata.NeedGrade}级`;

            if (equipdata.BaseAttr) {
                let basearr = equipdata.BaseAttr.split(';');
                for (const item of basearr) {
                    let itemarr = item.split(':');
                    if (itemarr.length == 2 && GameDefine.AttrEquipTypeStr[itemarr[0]] != null) {
                        let key = GameDefine.AttrEquipTypeStr[itemarr[0]];
                        let value = Number(itemarr[1]);
                        let valuestr = '';
                        if (GameDefine.EquipTypeNumerical.indexOf(Number(key)) == -1) {
                            valuestr = (value > 0 ? '+' : '') + (value / 10).toFixed(1) + '%';
                        }
                        else {
                            valuestr = (value > 0 ? '+' : '') + value;
                        }
                        pItemInfo.description += '\n' + GameDefine.AttrTypeL1Str[key] + '    ' + valuestr;
                    }
                }
            }
        }
        else {
            pItemInfo = cc.ll.propData.item[this.pGoodsInfo.itemid];
        }

        cc.find('picFrame/nodDetail/picText/Label', this.node).getComponent(cc.Label).string = pItemInfo.name;
        cc.find('picFrame/nodDetail/txDiscrib', this.node).getComponent(cc.Label).string = pItemInfo.description;
        cc.find('picFrame/nodDetail/nodNum/numLabel', this.node).getComponent(cc.Label).string = 1;

        this.SetBuyCnt(1);
        if(this.pGoodsInfo.quantity != null){
            this.selectedGoodsQuantity = this.pGoodsInfo.quantity;
            if(this.pGoodsInfo.quantity == 0)
                this.SetBuyCnt(0);
        }
    },

    OpenNumPad(e, d) {
        let goNumPad = CPubFunction.CreateSubNode(cc.find('Canvas/MainUI'), { nX: 60, nY: 195 }, this.NumPad, 'NumPad');
        goNumPad.getComponent('NumPad').NumPad_Init((nNum) => {
            let nCnt = CPubFunction.ChangeNumToRange(nNum, 1, 10000);
            this.SetBuyCnt(nCnt);
        });
    },


    SetBuyCnt(nBuyCnt) {
        if (this.pGoodsInfo == null)
            return;

        this.nBuyCnt = nBuyCnt;

        let nMoney = this.pGoodsInfo.price;

        cc.find('picFrame/nodDetail/nodNum/numLabel', this.node).getComponent(cc.Label).string = nBuyCnt;
        cc.find('picFrame/nodDetail/btnOK/money', this.node).getComponent(cc.Label).string = nBuyCnt * nMoney;
        CPubFunction.SetSpriteFrame(cc.find('picFrame/nodDetail/btnOK/icon', this.node), 'Common/ui_common', this.GetMoneyIconFile(this.pGoodsInfo.moneykind));
    },

    ChangeBuyCnt(stEvent, nAdd) {
        if (this.pGoodsInfo == null)
            return;

        let nNewNum = CPubFunction.ChangeNumToRange(this.nBuyCnt + nAdd, 1, this.selectedGoodsQuantity);
        
        this.SetBuyCnt(nNewNum);
    },



    OnBuy() {
        if (null == this.pGoodsInfo)
            return;

        cc.ll.net.send('c2s_buy_from_npc', { nConfigID: this.nNpcConfigID, nItemID: this.pGoodsInfo.itemid, nCnt: this.nBuyCnt });
    },

    SetMoneyInfo() {
		let topnode = cc.find('topInfo', this.node);
		let toplogic = topnode.getComponent('TopInfo');
		toplogic.updateGameMoney();
	},
});
