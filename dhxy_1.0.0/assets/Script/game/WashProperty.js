let CPubFunction = require('./PubFunction');
const jll_id = 10118; /* 金柳露id */

cc.Class({
	extends: cc.Component,
	properties: {
		left_panel: cc.Node,
		right_panel: cc.Node,
		sprite_atlas: cc.SpriteAtlas,
		bag_item: cc.Node,
		jll_count_label: cc.Label,
	},

	/*
	 * 显示界面
	 * @param pet_logic 宠物logic
	 */
	show(pet_logic) {
		this.node.active = true;
		this.pet_logic = pet_logic;
		let resid = this.pet_logic.resid;
		let info = {
			rate: this.pet_logic.rate,
			/* cur_hp:  this.pet_logic.cur_hp,
			cur_mp:  this.pet_logic.cur_mp,
			cur_atk: this.pet_logic.cur_atk,
			cur_spd: this.pet_logic.cur_spd, */
			cur_hp: this.pet_logic.hp,
			cur_mp: this.pet_logic.mp,
			cur_atk: this.pet_logic.atk,
			cur_spd: this.pet_logic.spd,
			max_rate: this.pet_logic.maxRate
		};
		this.showPropertyPanel(info, this.left_panel);
		for (let node of this.right_panel.children)
			node.active = false;
		this.showBagItem();
	},

	showBagItem() {
		let logic = this.bag_item.getComponent('BagItem');
		logic.loadInfo({ itemid: jll_id });
		logic.selectedNode.opacity = 0;
		let jll_count = cc.ll.player.itemList[jll_id] || 0;
		this.jll_count_label.string = jll_count + '/' + 3;
	},

	unshow() {
		this.node.active = false;
		for (let node of this.right_panel.children)
			node.active = false;
		if (this.close_callback)
			this.close_callback();
	},

	/*
	 * 显示属性信息
	 * @param info 信息
	 * @param panel 面板
	 */
	showPropertyPanel(info, panel) {
		//if (!info.max_rate) {
			for (let key in cc.ll.propData.pet) {
				if (cc.ll.propData.pet.hasOwnProperty(key)) {
					let item = cc.ll.propData.pet[key];
					if (key == this.pet_logic.dataid) {
						//info.max_rate = JSON.parse(item.rate)[1];
						info.max_hp = JSON.parse(item.hp)[1];
						info.max_mp = JSON.parse(item.mp)[1];
						info.max_atk = JSON.parse(item.atk)[1];
						info.max_spd = JSON.parse(item.spd)[1];
					}
				}
			}
		//}
		let hp_progress = panel.getChildByName('hpProgress');
		let hp_label = hp_progress.getChildByName('label').getComponent(cc.Label);
		hp_progress.getComponent(cc.ProgressBar).progress = (info.cur_hp == 0) ? 0 : (info.cur_hp / info.max_hp);
		hp_label.string = info.cur_hp + '/' + info.max_hp;

		let mp_progress = panel.getChildByName('mpProgress');
		let mp_label = mp_progress.getChildByName('label').getComponent(cc.Label);
		mp_progress.getComponent(cc.ProgressBar).progress = (info.cur_mp == 0) ? 0 : (info.cur_mp / info.max_mp);
		mp_label.string = info.cur_mp + '/' + info.max_mp;

		let att_progress = panel.getChildByName('attProgress');
		let att_label = att_progress.getChildByName('label').getComponent(cc.Label);
		att_progress.getComponent(cc.ProgressBar).progress = (info.cur_atk == 0) ? 0 : (info.cur_atk / info.max_atk);
		att_label.string = info.cur_atk + '/' + info.max_atk;

		let spe_progress = panel.getChildByName('speProgress');
		let spe_label = spe_progress.getChildByName('label').getComponent(cc.Label);
		spe_progress.getComponent(cc.ProgressBar).progress = (info.cur_spd == 0) ? 0 : (info.cur_spd / info.max_spd);
		spe_label.string = info.cur_spd + '/' + info.max_spd;

		let rate_label = panel.getChildByName('RateLabel').getComponent(cc.Label);
		
		rate_label.string = info.rate.toFixed(3) + '/' + info.max_rate.toFixed(3);
		for (let i = 1; i <= 5; ++i) {
			let sprite = panel.getChildByName('grow' + i).getComponent(cc.Sprite);
			let url = 'ui_common_loading_bar2';
			if (i / 5 >= info.rate / info.max_rate * 0.99)
				url = 'ui_common_loading_bar_bg';
			sprite.spriteFrame = this.sprite_atlas.getSpriteFrame(url);
		}
	},

	/*
	 * 洗练属性
	 */
	washProperty(data) {
		for (let node of this.right_panel.children)
			node.active = true;
		let info = {
			rate: data.rate / 10000,
			cur_hp: data.hp,
			cur_mp: data.mp,
			cur_atk: data.atk,
			cur_spd: data.spd,
			max_rate: data.maxrate / 10000
		};
		this.showPropertyPanel(info, this.right_panel);
		this.can_save_property = true;
		let jll_count = cc.ll.player.itemList[jll_id] || 0;
		this.jll_count_label.string = jll_count + '/' + 3;
	},

	/*
	 * 保存洗练属性
	 */
	saveProperty(data) {
		CPubFunction.CreateNotice(cc.find('Canvas'), '洗练的属性保存成功！', 2);
		this.can_save_property = false;
		this.pet_logic.rate = data.rate / 10000;
		this.pet_logic.hp = data.hp;
		this.pet_logic.mp = data.mp;
		this.pet_logic.atk = data.atk;
		this.pet_logic.spd = data.spd;

		let info = {
			rate: data.rate / 10000,
            max_rate: data.maxrate/10000,
			cur_hp: data.hp,
			cur_mp: data.mp,
			cur_atk: data.atk,
			cur_spd: data.spd,
		};
		this.showPropertyPanel(info, this.left_panel);
	},

	onButtonClick(event, param) {
		if (param == 'close') {
			this.unshow();
		}
		else if (param == 'wash') {
			let jll_count = cc.ll.player.itemList[jll_id] || 0;
			if (jll_count < 3) {
				CPubFunction.CreateNotice(cc.find('Canvas'), '金柳露数量不足！', 2);
				return;
			}
			cc.ll.net.send('c2s_wash_petproperty', {
				petid: this.pet_logic.petid,
				dataid: this.pet_logic.dataid,
			});
		}
		else if (param == 'save') {
			if (!this.can_save_property) {
				CPubFunction.CreateNotice(cc.find('Canvas'), '请先洗练属性，再保存！', 2);
				return;
			}
			cc.ll.net.send('c2s_save_petproperty', {
				petid: this.pet_logic.petid,
			});
		}
	},
});
