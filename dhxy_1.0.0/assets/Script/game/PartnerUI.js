﻿let GameRes = require('./GameRes');
let CPubFunction = require('./PubFunction');
let CSkillMgr = require('./Skill');
let CMainPlayerInfo = require('./MainPlayerInfo');
var pExpMgr = require('./ExpMgr');

cc.Class({
    extends: cc.Component,
    properties:{
        itemUI: cc.Prefab,

        rowPartnerAttrib:cc.Prefab,
        goAtribLine:cc.Prefab,
        rowPartnerAttrib:cc.Prefab,
        rowPartnerZiZhi:cc.Prefab,
        ChuanGongUI:cc.Prefab,
        btnPartner:cc.Prefab,
    },

    onLoad() {
    },

    start() {
        this.svPartnerAttribute = cc.find('svPartnerAttribute', this.node);
        this.cvZiZhi = cc.find('cvZiZhi', this.node);

        cc.find('btnClose', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "PartnerUI", "Close", 0));
        cc.find('btnChangeState', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "PartnerUI", "ChangePartnerState", 0));
        cc.find('btnChuanGong', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "PartnerUI", "OpenChunGongUI", 0));
        cc.find('btnRelive', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "PartnerUI", "DoRelive", 0));
        cc.find('nodRole/btnKangXing', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "PartnerUI", "ShowAttribute", 0));
        cc.find('svPartnerAttribute/btnClose', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "PartnerUI", "CloseAtrib", 0));
        cc.find('nodRole/btnZiZhi', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "PartnerUI", "ShowZiZhi", 0));
        cc.find('cvZiZhi/btnClose', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "PartnerUI", "CloseZiZhi", 0));

        cc.find('nodRole/btnAddExp', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "PartnerUI", "ShowBook", 0));


        this.nCurSelect = 0;
        this.vecPartnerBtn = [];

        this.vecAtrib = [];


        //-------------------------------

        this.ShowPartnerList();
        cc.ll.net.send('c2s_get_bagitem', { roleid: cc.ll.player.roleid });
        cc.ll.net.send('c2s_ask_partner_list', { nRoleID: cc.ll.player.roleid });
    },


    CloseAtrib() {
        this.svPartnerAttribute.active = false;
    },

    CloseZiZhi() {
        this.cvZiZhi.active = false;
    },

    ReplaceAtriPos(contentRow, mapAll) {
        let nCurY = 0;
        contentRow.height = 480;

        for (var itAll in mapAll) {
            nCurY -= 50;
            let goTitle = CPubFunction.CreateSubNode(contentRow, { nX: 300, nY: nCurY }, this.rowPartnerAttrib, 'rowPartnerAttrib');
            goTitle.getComponent(cc.Label).string = itAll;
            cc.find('Value', goTitle).getComponent(cc.Label).string = '';
            this.vecAtrib.push(goTitle);

            nCurY -= 20;
            let goLine = CPubFunction.CreateSubNode(contentRow, { nX: 330, nY: nCurY }, this.goAtribLine, 'goAtribLine');
            this.vecAtrib.push(goLine);

            let mapAtrib = mapAll[itAll];

            let nIndex = -1;
            for (var it in mapAtrib) {
                nIndex++;
                let bNewLine = (nIndex % 2) == 0 ? true : false;
                nCurY -= bNewLine ? 50 : 0;

                let goAtrib = CPubFunction.CreateSubNode(contentRow, { nX: (bNewLine ? 72 : 372), nY: nCurY }, this.rowPartnerAttrib, 'rowPartnerAttrib');
                goAtrib.getComponent(cc.Label).string = CPubFunction.GetAttributeName(it);

                let nValue = mapAtrib[it];

                let bIn = false;
                if (CPubFunction.IsDataInVecter(it, ['Atk', 'Speed', 'AdDefEhan', 'HitCombo', 'HpMax', 'MpMax', 'Basecon', 'Wakan', 'Power', 'Agility'])) {
                    bIn = true;
                }

                nValue = bIn ? nValue : nValue / 10;
                let strValue = bIn ? `${nValue}` : `${nValue}%`

                cc.find('Value', goAtrib).getComponent(cc.Label).string = strValue;
                this.vecAtrib.push(goAtrib);
            }

        }

        contentRow.height = Math.max(contentRow.height, Math.abs(nCurY) + 50);

    },

    //zzzErr
    ShowAttribute() {
        this.svPartnerAttribute.active = true;
        let contentRow = cc.find('view/content', this.svPartnerAttribute)
        CPubFunction.DestroyVecNode(this.vecAtrib);

        let mapInfo = this.GetCurSelectPartnerInfo();

        let mapFenLei = {};

        for (var it in mapInfo) {
            let strTypeName = CPubFunction.GetAttributeType(it);
            if ('' == strTypeName)
                continue;

            if (mapFenLei.hasOwnProperty(strTypeName) == false)
                mapFenLei[strTypeName] = {};

            mapFenLei[strTypeName][it] = mapInfo[it];
        }

        this.ReplaceAtriPos(contentRow, mapFenLei);
    },


    GetCurSelectPartnerInfo() {
        if (CMainPlayerInfo.vecPartnerInfo.length <= this.nCurSelect)
            return null;

        return CMainPlayerInfo.vecPartnerInfo[this.nCurSelect];
    },

    ShowZiZhi() {
        this.cvZiZhi.active = true;
        CPubFunction.DestroyVecNode(this.vecAtrib);

        let mapCurInfo = this.GetCurSelectPartnerInfo();

        cc.find('DingWei/Label', this.cvZiZhi).getComponent(cc.Label).string = mapCurInfo['dingwei'];

        let mapZiZhi = JSON.parse(mapCurInfo['mapZiZhi']);

        let nCurY = 84;

        let nIndex = -1;
        for (var it in mapZiZhi) {
            nIndex++;
            let bNewLine = (nIndex % 2) == 0 ? true : false;
            nCurY -= bNewLine ? 40 : 0;

            let rowPartnerZiZhi = CPubFunction.CreateSubNode(this.cvZiZhi, { nX: (bNewLine ? -230 : 17), nY: nCurY }, this.rowPartnerZiZhi, 'rowPartnerZiZhi');
            rowPartnerZiZhi.getComponent(cc.Label).string = CPubFunction.GetAttributeName(it);

            let nValue = mapZiZhi[it];
            cc.find('picPecent/Label', rowPartnerZiZhi).getComponent(cc.Label).string = `${nValue}%`;
            cc.find('picPecent/picBar', rowPartnerZiZhi).width = nValue;

            this.vecAtrib.push(rowPartnerZiZhi);
        }
    },


    loadInfo(info) {
        if (cc.ll.propData.item[info.itemid]) {
            this.itemInfo = cc.ll.propData.item[info.itemid];
            this.itemInfo.count = info.count;
            this.itemBg.active = true;
            this.itemCount.string = info.count;
            this.itemIcon.spriteFrame = GameRes.getItemIcon(this.itemInfo.icon);
        }
    },

    ShowBook(e, d) {
        if (this.itemui) {
            this.itemui.destroy();
            this.itemui = null;
        }

        let stPartnerInfo = this.GetCurSelectPartnerInfo();

        this.itemui = cc.instantiate(this.itemUI);
        this.itemui.parent = this.node;
        this.itemui.getComponent('UseItemUI').setUIPos(220, -150);
        this.itemui.getComponent('UseItemUI').loadList('点击/长按可使用修炼册', [10202, 10204], stPartnerInfo.id, true);
    },

    OnReceiveBagItem() {

        if (this.itemui != null && this.itemui.parent != null) {
            this.itemui.getComponent('UseItemUI').refreshInfo();
            // this.ShowBook();
        }
    },

    GetPartnerInfoByPartnerID(nID) {
        for (var it in CMainPlayerInfo.vecPartnerInfo) {
            if (CMainPlayerInfo.vecPartnerInfo[it].id == nID)
                return CMainPlayerInfo.vecPartnerInfo[it];
        }

        return null;
    },


    GetCurSelectPartnerInfo() {
        if (this.nCurSelect >= CMainPlayerInfo.vecPartnerInfo)
            return null;

        return CMainPlayerInfo.vecPartnerInfo[this.nCurSelect];
    },



    EatBook() {
        let CGoodsMgr = require('./GoodsMgr');
        let itemInfo = CGoodsMgr.getItem(10202);
        if (itemInfo == null || itemInfo.count <= 0)
            return;

        let stPartnerInfo = this.GetCurSelectPartnerInfo();
        CGoodsMgr.useItem(itemInfo.id, 1, stPartnerInfo.id);
    },

    DoRelive() {
        let pPartnerInfo = this.GetCurSelectPartnerInfo();
        if (null == pPartnerInfo)
            return;

        cc.ll.net.send('c2s_partner_relive', { nPartnerID: pPartnerInfo.id });
    },


    OpenChunGongUI() {
        let goUI = CPubFunction.CreateSubNode(cc.find('Canvas/MainUI'), { nX: 0, nY: 0 }, this.ChuanGongUI, 'ChuanGongUI');

        if (CMainPlayerInfo.vecPartnerInfo.length < 2)
            return;

        goUI.getComponent('ChuanGongUI').Init(CMainPlayerInfo.vecPartnerInfo, this.nCurSelect);
    },


    GetFreePos() {
        for (var it in CMainPlayerInfo.vecChuZhan) {
            if (CMainPlayerInfo.vecChuZhan[it] == 0)
                return it;
        }

        return -1;
    },



    ChangePartnerState() {

        if (CMainPlayerInfo.vecPartnerInfo.length <= this.nCurSelect || this.vecPartnerBtn.length <= this.nCurSelect)
            return;

        let stInfo = CMainPlayerInfo.vecPartnerInfo[this.nCurSelect];
        let nChuZhanPos = CMainPlayerInfo.GetParterChuZhanPos(stInfo.id);

        if (nChuZhanPos == -1) {
            let nToPos = this.GetFreePos();

            if (nToPos == -1) {
                CPubFunction.CreateNotice(cc.find('Canvas/MainUI'), '最多只能4个伙伴参战', 2);
                return;
            }

            cc.find('picState', this.vecPartnerBtn[this.nCurSelect]).active = true;
            CMainPlayerInfo.ChangeChuZhan(nToPos, stInfo.id);
            cc.ll.net.send('c2s_change_partner_state', { nPartnerID: stInfo.id, nPos: nToPos });
        }
        else {
            cc.find('picState', this.vecPartnerBtn[this.nCurSelect]).active = false;
            CMainPlayerInfo.ChangeChuZhan(nChuZhanPos, 0);
            cc.ll.net.send('c2s_change_partner_state', { nPartnerID: 0, nPos: nChuZhanPos });
        }

        this.SetButtonText(stInfo.id);

    },




    ShowPartnerList() {
        CPubFunction.DestroyVecNode(this.vecPartnerBtn);

        let goContent = cc.find('svPartnerList/view/content', this.node);

        let stStart = { nX: 120, nY: -45 };

        for (let i = 0; i < CMainPlayerInfo.vecPartnerInfo.length; i++) {
            let stInfo = CMainPlayerInfo.vecPartnerInfo[i];

            let goPartner = CPubFunction.CreateSubNode(goContent, { nX: stStart.nX, nY: stStart.nY - i * 90 }, this.btnPartner, 'btnPartner');
            goPartner.getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "PartnerUI", "ShowPartnerInfo", i));

			CPubFunction.SetSpriteFrame(cc.find('HeadPic/Icon', goPartner), 'Common/huoban', 'huoban_' + stInfo.resid);
            cc.find('Name', goPartner).getComponent(cc.Label).string = stInfo.name;
            cc.find('Level', goPartner).color = CPubFunction.getReliveColor(stInfo.relive);
            cc.find('Level', goPartner).getComponent(cc.Label).string = `${stInfo.relive} 转${stInfo.level} 级`;
            cc.find('picState', goPartner).active = CMainPlayerInfo.GetParterChuZhanPos(stInfo.id) >= 0;

            this.vecPartnerBtn.push(goPartner);
        }

        goContent.height = Math.max(goContent.height, CMainPlayerInfo.vecPartnerInfo.length * 90);

        this.ShowPartnerInfo(0, 0);
    },

    ChangePartnerExp(nResID, nExp) {
        for (var it in CMainPlayerInfo.vecPartnerInfo) {
            if (CMainPlayerInfo.vecPartnerInfo[it].resid == nResID) {
                CMainPlayerInfo.vecPartnerInfo[it].exp = nExp;
                break;
            }
        }
    },


    ShowPartnerInfo(stEvent, nIndex) {
        let stInfo = CMainPlayerInfo.vecPartnerInfo[nIndex];
        if (null == stInfo)
            return;


        cc.find('nodRole/Name', this.node).getComponent(cc.Label).string = stInfo.name;
        cc.find('nodRole/Level', this.node).color = CPubFunction.getReliveColor(stInfo.relive);
        cc.find('nodRole/Level', this.node).getComponent(cc.Label).string = `${stInfo.relive} 转${stInfo.level} 级`;
        cc.find('nodRole/DingWei/Label', this.node).getComponent(cc.Label).string = stInfo.dingwei;
        CPubFunction.SetSpriteFrame(cc.find('nodRole/Race', this.node), 'Common/ui_common', CPubFunction.GetRaceFileName(stInfo.race));


        for (var itSkill in stInfo.skill_list) {
            CPubFunction.SetSpriteFrame(cc.find('nodRole/nodSkill', this.node), 'icon/skill', itSkill);
            cc.find('nodRole/nodSkill/Name', this.node).getComponent(cc.Label).string = CSkillMgr.GetSkillInfo(itSkill).strName;
            cc.find('nodRole/nodSkill/SkillExp/Label', this.node).getComponent(cc.Label).string = stInfo.skill_list[itSkill];
            break;
        }


        cc.find('nodRole/Blood/Title/Label', this.node).getComponent(cc.Label).string = stInfo.HpMax;
        cc.find('nodRole/Atk/Title/Label', this.node).getComponent(cc.Label).string = stInfo.Atk;
        cc.find('nodRole/Speed/Title/Label', this.node).getComponent(cc.Label).string = stInfo.Speed;

        let nMax = pExpMgr.GetHuoBanUpgradeExp(stInfo.relive, stInfo.level);
        let strTmp = `${stInfo.exp}/${nMax}`;
        cc.find('nodRole/nodExp/picText/Label', this.node).getComponent(cc.Label).string = strTmp;
        let fPercent = Math.min(stInfo.exp / nMax, 1);
        cc.find('nodRole/nodExp/picText/picBar', this.node).width = cc.find('nodRole/nodExp/picText', this.node).width * fPercent;
        CPubFunction.SetDragonBoneAvatar(cc.find('nodRole/goAvater', this.node), null, stInfo.resid);
        this.SetButtonText(stInfo.id);
        this.nCurSelect = nIndex;

        cc.find('Level', this.vecPartnerBtn[nIndex]).getComponent(cc.Label).string = `${stInfo.relive} 转${stInfo.level} 级`;
        cc.find('Level', this.vecPartnerBtn[nIndex]).color = CPubFunction.getReliveColor(stInfo.relive);


    },


    SetButtonText(nPartnerID) {
        let nPos = CMainPlayerInfo.GetParterChuZhanPos(nPartnerID);
        cc.find('btnChangeState/Label', this.node).getComponent(cc.Label).string = nPos == -1 ? '参战' : '休息';
    },

    Close() {
        this.node.destroy();

        let goTeamUI = cc.find('Canvas/MainUI/TeamUI');
        if (goTeamUI)
            goTeamUI.getComponent('TeamUI').CreateCardList();

    },

});
