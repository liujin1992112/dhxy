﻿
let CPubFunction = require('./PubFunction');
let GameRes = require('./GameRes');

cc.Class({
    extends: cc.Component,


    properties: {
    },


    onLoad() {
        this.vecPrizeIcon = [];
    },

    start() {
        cc.find('btnClose', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "ActiveDetailUI", "Close", 0));
    },

    ActiveDetailUI_Init(strTitle, strInfo, mapPrize) {
        cc.find(`picText/Title`, this.node).getComponent(cc.Label).string = strTitle;

        let goLabInfo = cc.find(`labInfo`, this.node);
        let goLayoutPeize = cc.find('LayoutPrize', this.node);

        goLabInfo.getComponent(cc.Label).string = strInfo;

        this.node.height = 220 + goLabInfo.height;
        this.node.y = this.node.height / 2;

        goLayoutPeize.y = -70 - goLabInfo.height;


        CPubFunction.DestroyVecNode(this.vecPrizeIcon);

        cc.loader.loadRes(`Prefabs/preItem`, cc.Prefab, (err, prefab) => {
            let nIndex = -1;
            for (var it in mapPrize) {
                nIndex++;

                let nX = 40 + (nIndex % 5) * 70;
                let nY = -33 - Math.floor(nIndex / 5) * 70;

                let goPrizeIcon = CPubFunction.CreateSubNode(goLayoutPeize, { nX: nX, nY: nY }, prefab, 'preItem');

                if (it == 'exp') {
                    CPubFunction.SetSpriteFrame(cc.find('Icon', goPrizeIcon), 'Common/ui_common', 'ui_common_icon_exp');
                }
                else if (it == 'petexp') {
                    CPubFunction.SetSpriteFrame(cc.find('Icon', goPrizeIcon), 'Common/ui_common', 'ui_common_icon_exppet');
                }
                else if (it == 'money') {
                    CPubFunction.SetSpriteFrame(cc.find('Icon', goPrizeIcon), 'Common/ui_common', 'ui_common_icon_yinliang');
                }

                else {
                    let stConfigInfo = cc.ll.propData.item[it];
                    cc.find('Icon', goPrizeIcon).getComponent(cc.Sprite).spriteFrame = GameRes.getItemIcon(stConfigInfo.icon);
                }

                this.vecPrizeIcon.push(goPrizeIcon);
            }
        });
    },


    Close() {
        this.node.destroy();
    },
});
