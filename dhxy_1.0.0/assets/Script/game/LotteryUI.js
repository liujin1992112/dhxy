﻿
let CPubFunction = require('./PubFunction');

cc.Class({
    extends: cc.Component,


    properties:
    {
    },


    onLoad() {

        this.vecBtnPrize = new Array(16);

        this.picFlag = cc.find('picFlag', this.node);

        this.nLen = 0;
        this.nCurPos = 0;

        this.nBoxID = 0;

    },

    start() {
        cc.find('btnClose', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "LotteryUI", "Close", 0));

        for (let i = 0; i < 15; i++) {
            this.vecBtnPrize[i] = cc.find(`nodItem/preItem_${i}`, this.node);
        }


        this.SetMoneyInfo();
        cc.ll.net.send('c2s_ask_lottery_info', {});

    },

    Close() {
        this.node.destroy();
    },


    Go() {
        if (this.nLen > 0)
            return;

        cc.ll.net.send('c2s_lottery_go', { nID: this.nBoxID });
    },

    SetMoneyInfo() {
        return;

        cc.find('layMoney/yinliang/ui_common_icon_yinliang/yinliangLabel', this.node).getComponent(cc.Label).string = cc.ll.player.gameData.money;
        cc.find('layMoney/xianyu/ui_common_icon_xianyu/xianyuLabel', this.node).getComponent(cc.Label).string = cc.ll.player.gameData.jade;
    },

    ShowItemList(strJon) {
        let stData = JSON.parse(strJon);

        this.nBoxID = stData.nBoxID;
        let mapValue = stData.mapValue;

        let nSize = CPubFunction.GetMapLen(mapValue);

        if (nSize != 15)
            return;

        let nCnt = -1;

        for (var it in mapValue) {
            nCnt++;
            let strName = mapValue[it].strItem;
            let nNum = mapValue[it].nNum
            this.SetPrizesInfo(this.vecBtnPrize[nCnt], strName, parseInt(nNum));
        }


        setTimeout(() => {
            this.Go()
        }, 500);
    },

    ShowLightMove(stData) {
        let nSelect = stData.nSelect;
        this.nLen = stData.nLen;

        this.nCurPos = 0;
        let goNextItem = this.vecBtnPrize[this.nCurPos];
        this.SetFlagPos(goNextItem);

        this.CheckAndMove();

    },

    SetFlagPos(goItem) {
        this.picFlag.setPosition(cc.v2(goItem.x - 23, goItem.y - 5));
    },



    GetPassTime() {
        if (this.nLen >= 8)
            return 0.1

        return (8 - this.nLen) / 10;
    },

    CheckAndMove() {
        if (this.nLen <= 0)
            return;

        let pSelf = this;

        this.scheduleOnce(() => {
            pSelf.nLen -= 1;
            this.nCurPos = this.GetNextPos(this.nCurPos);
            let goNextItem = this.vecBtnPrize[this.nCurPos]
            this.SetFlagPos(goNextItem);

            if (pSelf.nLen > 0) {
                pSelf.CheckAndMove();
            }

        }, this.GetPassTime());
    },


    GetNextPos(nPos) {
        return nPos >= 14 ? 0 : nPos + 1;
    },



    SetPrizesInfo(goItem, strItem, nNum) {
        CPubFunction.SetPrizeIcon(cc.find('Icon', goItem), strItem);
        cc.find('txLap', goItem).getComponent(cc.Label).string = nNum;
    },



});
