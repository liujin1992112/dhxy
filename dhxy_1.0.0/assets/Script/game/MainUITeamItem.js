﻿let GameRes = require('./GameRes');

cc.Class({
    extends: cc.Component,
    properties: {
        MainTeamPanel:cc.Prefab,
    },



    setInfo(info) {
        this.objInfo = info;
        if (info.livingtype == 1) {
            cc.find('headicon', this.node).getComponent(cc.Sprite).spriteFrame = GameRes.getRoleHead(info.resid);
            cc.find('levelback', this.node).active = false;
            cc.find('levelback/level', this.node).getComponent(cc.Label).string = info.level;
            cc.find('huoban', this.node).active = false;
        }
        else {
            cc.find('headicon', this.node).getComponent(cc.Sprite).spriteFrame = GameRes.getHuobanHead(info.resid);
            cc.find('levelback', this.node).active = false;
            cc.find('huoban', this.node).active = true;
        }
    },

    openMemberPanel() {
        if (this.objInfo.livingtype == 1) {
            let memberPanel = cc.instantiate(this.MainTeamPanel);
            memberPanel.parent = cc.find('Canvas');
            memberPanel.getComponent('MainUITeamPanel').setInfo(this.objInfo);
        }
    }
});
