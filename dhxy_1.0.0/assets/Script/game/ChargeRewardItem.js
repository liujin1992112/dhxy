cc.Class({
	extends: cc.Component,
	properties: {
		progress: cc.ProgressBar,
		jadesum_label: cc.Label,
		jade_label: cc.Label,
		reward_button: cc.Node,
		gained_sprite: cc.Node,
		bag_item: cc.Prefab,
	},

	/*
	 * 初始化
	 * @param reward_money 达到奖励所需money
	 * @param charge_sum 总充值
	 * @param has_gain 是否领取
	 */
	init (reward_money, charge_sum, has_gain, reward_list, id) {
		this.id = id;
		reward_money *= 100;
		charge_sum *= 100;
		if (charge_sum > reward_money)
			charge_sum = reward_money;
		this.jade_label.string = charge_sum + '/' + reward_money;
		this.jadesum_label.string = reward_money;
		this.progress.progress = charge_sum/reward_money;
		if (charge_sum/reward_money == 1) {
			this.progress.node.active = false;
			this.jade_label.node.active = false;
			this.reward_button.active = !has_gain;
			this.gained_sprite.active = has_gain;
		}
		else
			this.progress.node.active = true;
		let node_list = [];
		for (let item of reward_list) {
			let node = cc.instantiate(this.bag_item);
			let logic = node.getComponent('BagItem');
			node_list.push(node);
			node.parent = this.node;
			node.setContentSize(80, 80);
			node.getChildByName('itembg').setScale(0.8);
			node.y = 0;
			logic.loadInfo({ itemid: item.gid, count: item.count, });
			logic.selectedNode.opacity = 0;
		} 
		for (let i = 0; i < node_list.length; ++i)
			node_list[i].x = 34 + (i - (node_list.length-1)/2) * 80;
	},

	onButtonClick (event, param) {
		let send_data = {
			roleid: cc.ll.player.roleid,
			id: this.id,
		};
		cc.ll.player.rewardrecord |= (1<<(this.id-1));
		cc.ll.net.send('c2s_charge_reward', {rewardid: this.id});
		this.gained_sprite.active = true;
		this.reward_button.active = false;
	},
});
