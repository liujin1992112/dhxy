﻿let GameRes = require('./GameRes');
let CPubFunction = require('./PubFunction');
let CMainPlayerInfo = require('./MainPlayerInfo');


cc.Class({
    extends: cc.Component,


    properties:{
        NumPad: cc.Prefab,
    },


    onLoad() {
        this.nID = 0;
        this.nConfigID = 0;
        this.nMax = 0;
        this.nPrice = 0;
        this.nBuyCnt = 0;

    },

    start() {
        cc.find('btnClose', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "BuyUI", "Close", 0));
        cc.find('bntBuy', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "BuyUI", "OnBuy", 0));


        cc.find('nodNum/btnAdd', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "BuyUI", "ChangeBuyCnt", 1));
        cc.find('nodNum/btnCut', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "BuyUI", "ChangeBuyCnt", -1));

        cc.find('nodNum/btnPad', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "BuyUI", "OpenNumPad", 0));

        this.nBuyCnt = 1;
        this.ShowGoodsInfo();
    },


    Init() {
    },


    Close() {
        this.node.destroy();
    },


    ShowGoodsInfo() {
        if (this.nConfigID <= 0)
            return;

        let stConfigInfo = cc.ll.propData.item[this.nConfigID];

        let goIcon = cc.find('svItemDetail/view/content/ItemIcon/icon', this.node);

        goIcon.getComponent(cc.Sprite).spriteFrame = GameRes.getItemIcon(stConfigInfo.icon);
        cc.find('svItemDetail/view/content/txTitle', this.node).getComponent(cc.Label).string = stConfigInfo.name;

        let strText = stConfigInfo.description;
        strText += '\n\n' + '【用途】' + stConfigInfo.usedetail;

        cc.find('svItemDetail/view/content/txDetal', this.node).getComponent(cc.Label).string = strText;


        //----------------------------------

        let goGoods = cc.find('btnGoodsInfo', this.node);
        cc.find('nodItemInfo/Icon', goGoods).getComponent(cc.Sprite).spriteFrame = GameRes.getItemIcon(stConfigInfo.icon);
        cc.find('nodItemInfo/txLap', goGoods).getComponent(cc.Label).string = '';

        cc.find('labName', goGoods).getComponent(cc.Label).string = stConfigInfo.name;
        cc.find('picText/Label', goGoods).getComponent(cc.Label).string = this.nPrice;

        //----------------------------------

        cc.find('bntBuy/txNum', this.node).getComponent(cc.Label).string = this.nBuyCnt * this.nPrice;

    },

    ChangeBuyCnt(stEvent, nAdd) {
        this.nBuyCnt = CPubFunction.ChangeNumToRange(this.nBuyCnt + nAdd, 1, this.nMax);
        cc.find('nodNum/Label', this.node).getComponent(cc.Label).string = this.nBuyCnt;
        cc.find('bntBuy/txNum', this.node).getComponent(cc.Label).string = this.nBuyCnt * this.nPrice;
    },

    OpenNumPad(e, d) {
        this.nBuyCnt = 0;
        cc.find('nodNum/Label', this.node).getComponent(cc.Label).string = this.nBuyCnt;

        let goNumPad = CPubFunction.CreateSubNode(cc.find('Canvas/MainUI'), { nX: 70, nY: 190 }, this.NumPad, 'NumPad');
        goNumPad.getComponent('NumPad').NumPad_Init((nNum) => {
            this.nBuyCnt = CPubFunction.ChangeNumToRange(nNum, 1, this.nMax);
            cc.find('nodNum/Label', this.node).getComponent(cc.Label).string = this.nBuyCnt;
        });
    },




    OnBuy() {
        if (this.nConfigID <= 0 || this.nBuyCnt <= 0 || this.nBuyCnt > this.nMax)
            return;

        cc.ll.net.send('c2s_buy_goods', { nID: this.nID, nCnt: this.nBuyCnt });
        this.Close();
    },


});
