﻿let GameRes = require('./GameRes');
let CPubFunction = require('./PubFunction');
let CSpeak = require('./Speak');

var ESpeakerPos = {
    Left: 1,
    Right: 2,
};

/**
 * 对话框
 */
cc.Class({
    extends: cc.Component,
    properties: {},

    onLoad() { },

    start() { },

    Init(vecSpeak, pCallBack) {

        this.nIndex = 6000;
        this.vecSpeakEvent = [];

        this.mapFaceFrame = {};
        this.vecCharItem = [];
        this.pCallBack = pCallBack;

        for (let i = 0; i < vecSpeak.length; i++) {
            this.vecSpeakEvent.push(vecSpeak[i]);
        }

        this.NextSpeak();
    },

    ShowTalkText(strText, emPos) {
        cc.find('nodCustomRichText', this.node).setPosition(cc.v2(emPos == ESpeakerPos.Left ? 130 : -100, 0));
        let nodContent = cc.find('nodCustomRichText/view/content', this.node);
        nodContent.getComponent(cc.Label).string = strText;
    },

    onDestroy() { },

    NextSpeak() {
        if (this.vecSpeakEvent.length <= 0) {
            CPubFunction.FindAndHidenNode(cc.find('Canvas/MainUI'), {
                nX: 0,
                nY: -1000
            });

            if (this.pCallBack)
                this.pCallBack();
            return;
        }

        let stEvent = this.vecSpeakEvent.shift();
        this.ShowTalkText(stEvent.strText, stEvent.emPos);
        this.SetSpeaker(stEvent.nSpeakerID, stEvent.strName, stEvent.emPos);
    },

    /**
     * 设置说话者信息
     * @param {*} nSpeakerID        说话者ID
     * @param {*} strSpeakerName    说话者名称 
     * @param {*} emSpeakerPos      说话者位置
     */
    SetSpeaker(nSpeakerID, strSpeakerName, emSpeakerPos) {
        let pSelf = this;
        cc.find('picName/label', this.node).getComponent(cc.Label).string = strSpeakerName;
        CPubFunction.SetDragonBoneAvatar(
            cc.find('goSpeaker', this.node),
            cc.find('picAvatar', this.node),
            nSpeakerID
        );
        cc.find('goSpeaker', pSelf.node).setPosition(cc.v2(emSpeakerPos == ESpeakerPos.Left ? -300 : 300, -160));
    },
});