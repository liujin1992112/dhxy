﻿let GameRes = require('./GameRes');
let CPubFunction = require('./PubFunction');
let CItemMgr = require('./ItemMgr').g_ctCItemMgr;
let EItemType = require('./ItemMgr').EItemType;
let CItemType = require('./ItemMgr').CItemType;
let CMainPlayerInfo = require('./MainPlayerInfo');

cc.Class({
    extends: cc.Component,


    properties: {
        WXcontent: cc.Node,
        OneWxAccount: cc.Prefab,

        //多宝阁
        malltip: cc.Node,
        mallmessge: cc.Node,
        mallname: cc.Label,
        mallmessgeuse: cc.Label,
        mallnum: cc.Label,
        mallsprices: cc.Label,
        shop_charge_item: cc.Prefab,
        reward_prefab: cc.Prefab,

        btnGoodsInfo: cc.Prefab,
        btnShangJia: cc.Prefab,
        btnItemType: cc.Prefab,
        btnEquipGoodsInfo: cc.Prefab,
        btnMallInfo: cc.Prefab,
        BuyUI: cc.Prefab,
        NumPad: cc.Prefab,
        ShangJiaUI: cc.Prefab,
        btnItemName: cc.Prefab,
    },


    onLoad() {
        this.m_nMaxGoodsCnt = 8;

        this.vecItemName = [];
        this.vecTmpGoods = [];
        this.vecItemType = [];

        this.malldata = {};
    },

    start() {

        this.vecTagTeamBtn = [cc.find('btnBaiTan', this.node), cc.find('btnDuoBaoGe', this.node), cc.find('btnChongZhi', this.node)];
        this.vecShopTeamBtn = [cc.find('tabBaiTan/btnBuy', this.node), cc.find('tabBaiTan/btnSell', this.node), cc.find('tabBaiTan/btnGongShiQi', this.node)];
        this.vecDuoBaoGeTeamBtn = [cc.find('tabDuoBaoGe/btnFhesh', this.node), cc.find('tabDuoBaoGe/btnMerial', this.node), cc.find('tabDuoBaoGe/btnWeekLimit', this.node), cc.find('tabDuoBaoGe/btnFashion', this.node)];

        this.vecTabNode = [cc.find('tabBaiTan', this.node), cc.find('tabDuoBaoGe', this.node), cc.find('tabChongZhi', this.node)];

        //------------------

        cc.ll.net.send('c2s_ask_roles_goods', { nRoleID: cc.ll.player.roleid });
        cc.ll.net.send('c2s_get_bagitem', { roleid: cc.ll.player.roleid });
        cc.find('tabDuoBaoGe/nodSell/picFrame/MallMessage/MallNum/btnPad', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "ShopUI", "OpenNumPad", 0));
        this.ShopUI_Init();
        this.SetMoneyInfo();
    },

    Close() {
        cc.ll.AudioMgr.playCloseAudio();
        this.node.destroy();
    },

    SetMoneyInfo() {
        // cc.find('layMoney/yinliang/ui_common_icon_yinliang/yinliangLabel', this.node).getComponent(cc.Label).string = cc.ll.player.gameData.money;
        // cc.find('layMoney/xianyu/ui_common_icon_xianyu/xianyuLabel', this.node).getComponent(cc.Label).string = cc.ll.player.gameData.jade;
        let topnode = cc.find('topInfo', this.node);
		let toplogic = topnode.getComponent('TopInfo');
		toplogic.updateGameMoney();
    },


    ShopUI_Init() {
        cc.find('btnClose', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "ShopUI", "Close", 0));

        cc.find('btnBaiTan', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "ShopUI", "OnTabBaiTan", 0));
        cc.find('btnDuoBaoGe', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "ShopUI", "OnTabDuoBaoGe", 0));
        cc.find('btnChongZhi', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "ShopUI", "OnTabChongZhi", 0));


        cc.find('tabBaiTan/btnBuy', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "ShopUI", "InitBuyUI", 0));
        cc.find('tabBaiTan/btnSell', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "ShopUI", "InitSellUI", 0));
        cc.find('tabBaiTan/btnGongShiQi', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "ShopUI", "InitBuyUI", 2));

        cc.find('tabDuoBaoGe/btnFhesh', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "ShopUI", "DbgSelect", 0));
        cc.find('tabDuoBaoGe/btnMerial', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "ShopUI", "DbgSelect", 1));
        cc.find('tabDuoBaoGe/btnWeekLimit', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "ShopUI", "DbgSelect", 2));
        cc.find('tabDuoBaoGe/btnFashion', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "ShopUI", "DbgSelect", 3));

        this.InitBuyUI(0, 0);

    },


    SetUITitle(strName) {
        cc.find('picBg/Name/Lable', this.node).getComponent(cc.Label).string = strName;
    },

    OnTabBaiTan() {
        cc.ll.AudioMgr.playFenyeAudio();
        CPubFunction.ChangeTeamNodeState(this.vecTabNode, 0);
        CPubFunction.ChangeTeamButtonState(this.vecTagTeamBtn, 'Common/ui_common', 'ui_common_btn_tab1', 'ui_common_btn_tab2', 0);
        this.SetUITitle('摆 摊');
    },

    OnTabDuoBaoGe() {
        cc.ll.AudioMgr.playFenyeAudio();
        CPubFunction.ChangeTeamNodeState(this.vecTabNode, 1);
        CPubFunction.ChangeTeamButtonState(this.vecTagTeamBtn, 'Common/ui_common', 'ui_common_btn_tab1', 'ui_common_btn_tab2', 1);
        cc.ll.net.send('c2s_get_mall', { roleid: cc.ll.player.roleid })
        this.malltip.active = true;
        this.mallmessge.active = false;
        this.SetUITitle('多宝阁');
    },

    OnTabChongZhi() {
        cc.ll.AudioMgr.playFenyeAudio();
        this._record_num = 0;
        this.xnum = 0;
        this.ynum = 0;
        this.WXcontent.destroyAllChildren();
        CPubFunction.ChangeTeamNodeState(this.vecTabNode, 2);
        CPubFunction.ChangeTeamButtonState(this.vecTagTeamBtn, 'Common/ui_common', 'ui_common_btn_tab1', 'ui_common_btn_tab2', 2);
        
        let charge_list = cc.ll.propData.charge;
        for (let i = 1; charge_list[i]; ++i) {
            this.addChargeGoodsItem(charge_list[i]);
        }
        this.addChargeGoodsItem();
        this.SetUITitle('充 值');
    },

	/*
	 * 添加充值购买物品
	 */
    addChargeGoodsItem(data) {
        let node = cc.instantiate(this.shop_charge_item);
        node.parent = this.WXcontent;
        if (data) {
            node.getComponent('ShopChargeItem').init(data);
        }
        else {
            node.getComponent('ShopChargeItem').showEditbox();
        }
    },

    //添加每个客服的微信号信息
    addWXInfo(element) {
        let infonode = cc.instantiate(this.OneWxAccount);
        let infologic = infonode.getComponent('WXInfo');
        infologic.setInfo(element.account);
        infonode.parent = this.WXcontent;
        //infonode.setScale(cc.v2(0.8, 0.8));
        infonode.x = 0;
        infonode.y = 0;
        let tx = 620 - this.ynum * (infonode.width + 50);
        let ty = -100 - this.xnum * (infonode.height + 30);
        let t = 0.1 * (1 + this._record_num);
        let act1 = cc.moveTo(t, cc.v2(tx, ty));
        this._record_num++;
        var act2 = cc.fadeIn(t);
        infonode.runAction(act1);
        infonode.runAction(act2);
    },



    SetGoodsInfo(goGoods, stGoodsInfo) {
        let stConfigInfo = cc.ll.propData.item[stGoodsInfo.nConfigID];

        cc.find('nodItemInfo/Icon', goGoods).getComponent(cc.Sprite).spriteFrame = GameRes.getItemIcon(stConfigInfo.icon);
        cc.find('nodItemInfo/txLap', goGoods).getComponent(cc.Label).string = stGoodsInfo.nCnt;

        cc.find('labName', goGoods).getComponent(cc.Label).string = stConfigInfo.name;  //zzzErr time(null)-nTime
        cc.find('picText/Label', goGoods).getComponent(cc.Label).string = stGoodsInfo.nPrice;

        cc.find('labTime', goGoods).getComponent(cc.Label).string = Math.max(Math.floor((stGoodsInfo.nTime + 86400 - CPubFunction.GetTime()) / 3600), 0) + 'H';

    },

    SetMallInfo(goGoods, stGoodsInfo) {
        let stConfigInfo = cc.ll.propData.item[stGoodsInfo.itemid];

        cc.find('nodItemInfo/Icon', goGoods).getComponent(cc.Sprite).spriteFrame = GameRes.getItemIcon(stConfigInfo.icon);
        // cc.find('nodItemInfo/txLap', goGoods).getComponent(cc.Label).string = stGoodsInfo.nCnt;

        cc.find('labName', goGoods).getComponent(cc.Label).string = stConfigInfo.name;  //zzzErr time(null)-nTime
        cc.find('picText/Label', goGoods).getComponent(cc.Label).string = stGoodsInfo.price;
        // cc.find('itemid', goGoods).getComponent(cc.Label).string = stConfigInfo.itemid;

        // cc.find('labTime', goGoods).getComponent(cc.Label).string = Math.max(Math.floor((stGoodsInfo.nTime + 86400 - CPubFunction.GetTime()) / 3600), 0) + 'H';

    },


    HideAll() {
        cc.find('tabBaiTan/nodBuy', this.node).active = false;
        cc.find('tabBaiTan/nodSell', this.node).active = false;

        CPubFunction.DestroyVecNode(this.vecTmpGoods);
        CPubFunction.DestroyVecNode(this.vecItemType);

    },

    OnReceiveMyGoods() {
        if (cc.find('tabBaiTan/nodSell', this.node).active == false)
            return;

        this.InitSellUI();
    },

    InitSellUI() {

        CPubFunction.ChangeTeamButtonState(this.vecShopTeamBtn, 'Common/ui_common', 'ui_common_btn_tab_subheading0', 'ui_common_btn_tab_subheading1', 1);

        this.HideAll();

        cc.find('tabBaiTan/nodSell', this.node).active = true;

        cc.find('tabBaiTan/nodSell/picText/Label', this.node).getComponent(cc.Label).string = `我的摊位(${CMainPlayerInfo.vecMyGoods.length}/${this.m_nMaxGoodsCnt})`;

        let contentGoods = cc.find('tabBaiTan/nodSell/scvGoods/view/content', this.node);

        let stStart = { nX: 127, nY: -48 };


        let i = 0;
        for (; i < this.m_nMaxGoodsCnt; i++) {

            let stPos = { nX: stStart.nX + (i % 2) * 245, nY: stStart.nY - Math.floor(i / 2) * 82 };

            if (i < CMainPlayerInfo.vecMyGoods.length) {
                let goGoods = CPubFunction.CreateSubNode(contentGoods, stPos, this.btnGoodsInfo, '');
                this.SetGoodsInfo(goGoods, CMainPlayerInfo.vecMyGoods[i]);

                if (CMainPlayerInfo.vecMyGoods[i].nCnt > 0) {
                    goGoods.getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "ShopUI", 'OpenShangJiaUI', { nIndex: i, nFlag: 1 }));
                }
                else {
                    cc.find('nodItemInfo/picTiXian', goGoods).active = true;
                    goGoods.getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "ShopUI", 'OnTakeMoney', CMainPlayerInfo.vecMyGoods[i].nID));
                }

                this.vecTmpGoods.push(goGoods);

            }

            if (i >= CMainPlayerInfo.vecMyGoods.length && i < this.m_nMaxGoodsCnt) {
                let goShangJia = CPubFunction.CreateSubNode(contentGoods, stPos, this.btnShangJia, '');
                goShangJia.getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "ShopUI", 'OpenShangJiaUI', { nIndex: i, nFlag: 0 }));
                cc.find('Label', goShangJia).getComponent(cc.Label).string = '点击上架物品';
                this.vecTmpGoods.push(goShangJia);
            }
        }

        let stPos = { nX: stStart.nX + (i % 2) * 245, nY: stStart.nY - Math.floor(i / 2) * 82 };
        let goLock = CPubFunction.CreateSubNode(contentGoods, stPos, this.btnShangJia, '');
        goLock.getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "ShopUI", 'OpenShangJiaUI', { nIndex: i, nFlag: 0 }));
        CPubFunction.SetSpriteFrame(cc.find('nodBtnAdd/Icon', goLock), 'Common/ui_common', 'ui_common_package_lock');
        cc.find('Label', goLock).getComponent(cc.Label).string = '增加摊位';
        this.vecTmpGoods.push(goLock);


        contentGoods.height = Math.max(contentGoods.height, Math.ceil((this.m_nMaxGoodsCnt + 1) / 2) * 80 + 0);


    },



    InitBuyUI(stEvent, stParam) {
        CPubFunction.ChangeTeamButtonState(this.vecShopTeamBtn, 'Common/ui_common', 'ui_common_btn_tab_subheading0', 'ui_common_btn_tab_subheading1', stParam);

        this.HideAll();

        cc.find('tabBaiTan/nodBuy', this.node).active = true;

        let preButton = this.btnItemType;
        let contentItemType = cc.find('tabBaiTan/nodBuy/scrItemType/view/content', this.node);

        let nBtnHight = preButton.data.height;
        let nDis = 0;
        let nIndex = -1;

        let stStart = { nX: 117, nY: -36 };


        for (const it in CItemMgr.mapItemKind) {
            nIndex++;
            let goItemType = CPubFunction.CreateSubNode(contentItemType, { nX: stStart.nX, nY: stStart.nY - nIndex * (nBtnHight + nDis) }, preButton, it);
            goItemType.getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "ShopUI", 'OnClickMainKind', it));
            cc.find('Label', goItemType).getComponent(cc.Label).string = CItemMgr.mapItemKind[it];

            this.vecItemType.push(goItemType);
        }

        contentItemType.height = Math.max(contentItemType.height, nIndex * (nBtnHight + nDis) + 80);

        this.OnClickMainKind(0, EItemType.Equip);
    },

    InitEquipGoodsList(vecEquips) {
        vecEquips = [{
            Shape: 100105,
            EName: '武器(一级神兵)',
            EIndex: 1,
            EquipType: 2
        },
        {
            Shape: 102605,
            EName: '项链(一级神兵)',
            EIndex: 2,
            EquipType: 2
        },
        {
            Shape: 102305,
            EName: '衣服(一级神兵)',
            EIndex: 3,
            EquipType: 2
        },
        {
            Shape: 102105,
            EName: '头盔(一级神兵)',
            EIndex: 4,
            EquipType: 2
        },
        {
            Shape: 102505,
            EName: '鞋子(一级神兵)',
            EIndex: 5,
            EquipType: 2
        },
        {
            Shape: 100604,
            EName: '武器(一阶仙器)',
            EIndex: 1,
            EquipType: 3
        },
        {
            Shape: 102604,
            EName: '项链(一阶仙器)',
            EIndex: 2,
            EquipType: 3
        },
        {
            Shape: 102304,
            EName: '衣服(一阶仙器)',
            EIndex: 3,
            EquipType: 3
        },
        {
            Shape: 102204,
            EName: '头盔(一阶仙器)',
            EIndex: 4,
            EquipType: 3
        },
        {
            Shape: 102504,
            EName: '鞋子(一阶仙器)',
            EIndex: 5,
            EquipType: 3
        },
        ];
        cc.find('tabBaiTan/nodBuy', this.node).active = true;
        let contentGoods = cc.find('tabBaiTan/nodBuy/scvGoods/view/content', this.node);

        CPubFunction.DestroyVecNode(this.vecTmpGoods);

        let stStart = {
            nX: 125,
            nY: -48
        };
        for (let i = 0; i < vecEquips.length; i++) {

            let stPos = {
                nX: stStart.nX + (i % 2) * 245,
                nY: stStart.nY - Math.floor(i / 2) * 82
            };

            let goGoods = CPubFunction.CreateSubNode(contentGoods, stPos, this.btnEquipGoodsInfo, '');
            goGoods.getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "ShopUI", 'BuyEquip', vecEquips[i]));

            cc.find('nodItemInfo/Icon', goGoods).getComponent(cc.Sprite).spriteFrame = GameRes.getItemIcon(vecEquips[i].Shape);
            cc.find('labName', goGoods).getComponent(cc.Label).string = vecEquips[i].EName; //zzzErr time(null)-nTime

            this.vecTmpGoods.push(goGoods);
        }

        contentGoods.height = Math.max(contentGoods.height, Math.ceil((vecEquips.length + 1) / 2) * 80 + 0);
    },

    BuyEquip(e, equipInfo) {
        cc.ll.net.send('c2s_creat_equip', {
            type: equipInfo.EquipType,
            roleid: cc.ll.player.roleid,
            index: equipInfo.EIndex
        });
    },




    InitGoodList(vecGoods) {


        CPubFunction.ChangeTeamButtonState(this.vecShopTeamBtn, 'Common/ui_common', 'ui_common_btn_tab_subheading0', 'ui_common_btn_tab_subheading1', 0);

        cc.find('tabBaiTan/nodBuy', this.node).active = true;
        let contentGoods = cc.find('tabBaiTan/nodBuy/scvGoods/view/content', this.node);

        CPubFunction.DestroyVecNode(this.vecTmpGoods);

        let stStart = { nX: 125, nY: -48 };
        for (let i = 0; i < vecGoods.length; i++) {

            let stPos = { nX: stStart.nX + (i % 2) * 245, nY: stStart.nY - Math.floor(i / 2) * 82 };

            let goGoods = CPubFunction.CreateSubNode(contentGoods, stPos, this.btnGoodsInfo, '');
            goGoods.getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "ShopUI", 'OpenBuyUI', vecGoods[i]));
            this.SetGoodsInfo(goGoods, vecGoods[i]);
            this.vecTmpGoods.push(goGoods);
        }

        contentGoods.height = Math.max(contentGoods.height, Math.ceil((vecGoods.length + 1) / 2) * 84 + 0);
    },
    //加载出售商品展示
    InitMallList(data) {
        CPubFunction.DestroyVecNode(this.vecTmpGoods);

        let contentGoods = cc.find('tabDuoBaoGe/nodSell/scvGoods/view/content', this.node);
        contentGoods.destroyAllChildren();
        let stStart = { nX: 125, nY: -48 };
        this.malldata = JSON.parse(data.info);
        let length = CPubFunction.GetMapLen(this.malldata);
        for (let i = 1; i <= length; i++) {
            let stPos = { nX: stStart.nX + ((i - 1) % 2) * 245, nY: stStart.nY - Math.floor((i - 1) / 2) * 82 };
            let goMall = CPubFunction.CreateSubNode(contentGoods, stPos, this.btnMallInfo, '');
            goMall.getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "ShopUI", 'OpenBuyMall', i - 1));
            this.SetMallInfo(goMall, this.malldata[i]);
            this.vecTmpGoods.push(goMall);
        }
        contentGoods.height = Math.max(contentGoods.height, Math.ceil((length + 1) / 2) * 80 + 60);
    },

    OpenBuyUI(stEvent, stGoodsInfo) {
        CPubFunction.FindAndDeleteNode(cc.find('Canvas/MainUI'), 'BuyUI');
        let goBuyUI = CPubFunction.CreateSubNode(cc.find('Canvas/MainUI'), { nX: 0, nY: 0 }, this.BuyUI, 'BuyUI');
        let comBuyUI = goBuyUI.getComponent('BuyUI');
        comBuyUI.nID = stGoodsInfo.nID;
        comBuyUI.nConfigID = stGoodsInfo.nConfigID;
        comBuyUI.nPrice = stGoodsInfo.nPrice;
        comBuyUI.nMax = stGoodsInfo.nCnt;
        comBuyUI.nBuyCnt = 1;
    },

    OpenBuyMall(e, nIndex) {
        CPubFunction.ChangeTeamButtonState(this.vecTmpGoods, 'Common/ui_common', 'ui_common_public_list0', 'ui_common_public_list2', nIndex);
        let d = this.malldata[nIndex + 1];

        let mall = cc.ll.propData.item[d.itemid];
        this.malltip.active = false;
        this.mallmessge.active = true;
        this.mallname.string = mall.name;
        this.mallmessgeuse.string = mall.description;
        this.mallnum.string = 1;
        this.price = Number(d.price);
        this.mallsprices.string = this.price;
        this.price = Number(d.price);
        this.itemid = d.itemid;
        this.mallid = d.id;
    },
    Onaddmall() {
        if (this.mallnum.string >= 10000) {
            return;
        }
        this.mallnum.string += 1;
        this.mallsprices.string += Number(this.price);
    },
    onminmall() {
        if (this.mallnum.string <= 1) {
            return;
        }
        this.mallnum.string -= 1;
        this.mallsprices.string -= Number(this.price);
    },

    OpenNumPad(e, d) {
        this.mallnum.string = 0;

        let goNumPad = CPubFunction.CreateSubNode(cc.find('Canvas/MainUI'), { nX: 60, nY: 195 }, this.NumPad, 'NumPad');
        goNumPad.getComponent('NumPad').NumPad_Init((nNum) => {
            this.mallnum.string = CPubFunction.ChangeNumToRange(nNum, 1, 10000);
            this.mallsprices.string = Number(this.price) * Number(this.mallnum.string);
        });
    },


    OnBuyMall() {
        cc.ll.net.send('c2s_buymall_items', {
            mallnum: this.mallnum.string,
            mallprices: this.mallsprices.string,
            mallid: this.mallid,
        });
    },

    BuyPreview(nItemID, nPrice) {
        if (null == stInfo)
            return;

        this.stCurItem = cc.ll.propData.item[nItemID];

        let goIcon = cc.find('svItemDetail/view/content/ItemIcon/icon', this.node);

        goIcon.getComponent(cc.Sprite).spriteFrame = GameRes.getItemIcon(this.stCurItem.icon);
        cc.find('svItemDetail/view/content/txTitle', this.node).getComponent(cc.Label).string = this.stCurItem.name;


        let strText = this.stCurItem.description;
        strText += '\n\n' + '【用途】' + this.stCurItem.usedetail;

        cc.find('svItemDetail/view/content/txDetal', this.node).getComponent(cc.Label).string = strText;

    },


    OpenShangJiaUI(stEvent, stParam) {
        CPubFunction.FindAndDeleteNode(cc.find('Canvas/MainUI'), 'ShangJiaUI');
        let goShangJiaUI = CPubFunction.CreateSubNode(cc.find('Canvas/MainUI'), { nX: 0, nY: 0 }, this.ShangJiaUI, 'ShangJiaUI');

        let comShangJiaUI = goShangJiaUI.getComponent('ShangJiaUI');

        if (stParam.nFlag == 0) {
            comShangJiaUI.ShowBagItem();
        }

        if (stParam.nFlag == 1) {
            comShangJiaUI.ShowSellingItem();
        }

    },

    DbgSelect(stEvent, stParam) {
        CPubFunction.ChangeTeamButtonState(this.vecDuoBaoGeTeamBtn, 'Common/ui_common', 'ui_common_btn_tab_subheading0', 'ui_common_btn_tab_subheading1', stParam);
    },






    OnClickMainKind(stEvent, nKind) {
        cc.find('tabBaiTan/nodBuy/scvGoods', this.node).active = false;
        cc.find('tabBaiTan/nodBuy/scrItemName', this.node).active = true;

        if (nKind == EItemType.Equip) {
            this.ShowEquipNameList(CItemMgr.mapEquipType);
        }
        else {
            let mapTmp = {};

            mapTmp[0] = new CItemType(nKind, 0, '全部', 0);

            for (var it in cc.ll.propData.item) {
                let stConfig = cc.ll.propData.item[it];

                if (stConfig.type == nKind) {
                    mapTmp[stConfig.id] = new CItemType(nKind, stConfig.id, stConfig.name, stConfig.icon);
                }
            }

            this.ShowEquipNameList(mapTmp);
        }

    },


    ShowEquipNameList(mapEquipType)  //显示物品
    {
        CPubFunction.DestroyVecNode(this.vecItemName);

        //按钮框

        let preButton = this.btnItemName;
        let contItemName = cc.find('tabBaiTan/nodBuy/scrItemName/view/content', this.node);

        let nBtnHight = preButton.data.height;
        let nBtnWidth = preButton.data.width;
        let nDis = 4;
        let nIndex = -1;

        let stStart = { nX: 117, nY: -60 };


        for (const it in mapEquipType) {
            nIndex++;

            let goBtn = CPubFunction.CreateSubNode(contItemName, { nX: stStart.nX + (nIndex % 2) * nBtnWidth, nY: stStart.nY - Math.floor(nIndex / 2) * (nBtnHight + nDis) }, preButton, it);
            goBtn.getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "ShopUI", 'OnClickEquipName', mapEquipType[it]));

            cc.find('Label', goBtn).getComponent(cc.Label).string = mapEquipType[it].strTypeName;
            cc.find('nodIcon/Icon', goBtn).getComponent(cc.Sprite).spriteFrame = GameRes.getItemIcon(mapEquipType[it].icon);

            this.vecItemName.push(goBtn);
        }

        contItemName.height = Math.max(contItemName.height, Math.ceil(nIndex / 2) * (nBtnHight + nDis) + 80);

    },



    OnClickEquipName(stEvent, stItemType) {
        if (stItemType.nItemKind == 1)
            return;

        cc.find('tabBaiTan/nodBuy/scrItemName', this.node).active = false;
        cc.find('tabBaiTan/nodBuy/scvGoods', this.node).active = true;

        if (stItemType.nItemKind == 1) {
            this.InitEquipGoodsList([]);
        }
        else {
            cc.ll.net.send('c2s_get_shop_items', { nKind: stItemType.nItemKind, nItem: stItemType.nParam });
        }
    },


    OnTakeMoney(e, nID) {
        cc.ll.net.send('c2s_take_back_goods',
            {
                nID: nID,
            });

        //  this.Close();
    },

	/*
	 * 打开充值奖励界面
	 */
    onOpenChargeReward() {
        cc.instantiate(this.reward_prefab).parent = this.node;
    },
});
