let StrCommon = require('../../etc/str_common');

class VoiceMgr {
    constructor() {
        this.autoplay_index = 0;
        this.voice_list = [];
        this.auto_play_list = [];
        this.voice_played = false;
        this.playing_id = -1;

        this.record_func = null;
        this.recored_timer = null;
    }

    voiceRecord(callback){
        if (!CC_JSB) {
            if(this.record_func){
                this.record_func(false);
            }
            return;
        }
        this.startRecord();

        this.record_func = callback;
        this.recored_timer = setTimeout(() => {
            this.endRecord();
        }, 20 * 1000);
    }

    startRecord() {
        cc.log(jsb.fileUtils.getWritablePath());
        if (CC_JSB) {
            let soundName = jsb.fileUtils.getWritablePath() + "SoundRecord.wav";
            if (cc.sys.OS_IOS == cc.sys.os) {
                jsb.reflection.callStaticMethod("IOSWeChatMgr", "beginRecordWithName:", soundName);
                return true;
            } else if (cc.sys.OS_ANDROID == cc.sys.os) {
                if (jsb.reflection.callStaticMethod("com/jianghu/shouyou/Native", "startSoundRecord", "()I") == 0) {
                    cc.ll.msgbox.addMsg(StrCommon.RecordNotPermision);
                    return false;
                }
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

    endRecord() {
        if (CC_JSB) {
            let soundName = '';
            if (cc.sys.OS_IOS == cc.sys.os) {
                soundName = jsb.fileUtils.getWritablePath() + "SoundRecord.wav";
                jsb.reflection.callStaticMethod("IOSWeChatMgr", "endRecord");
            }
            else if (cc.sys.OS_ANDROID == cc.sys.os) {
                soundName = jsb.reflection.callStaticMethod("com/jianghu/shouyou/Native", "stopSoundRecord", "()Ljava/lang/String;");
            }
            if (soundName.length > 0) {
                cc.log(soundName);
                let data = jsb.fileUtils.getDataFromFile(soundName);
                if (data.length < 8192) {
                    cc.ll.msgbox.addMsg(StrCommon.RecordTooShot);
                    return null;
                }
                return data;
            }
        }
        return null;
    }

    addVoice(data) {
        if (CC_JSB) {
            let audio_data = new Uint8Array(data);
            cc.log(audio_data);
            let index = this.voice_list.length;
            let soundName = jsb.fileUtils.getWritablePath() + 'voice_' + index + '.wav';
            jsb.fileUtils.writeDataToFile(audio_data, soundName);
            this.voice_list[index] = soundName;

            if (cc.ll.auto_world_voice == 1) {
                this.auto_play_list.push(index);
                this.autoPlay();
            }
            return index;
        }
        return 0;
    }

    playVoice(index, callback) {
        if (CC_JSB) {
            let soundName = this.voice_list[index];
            if (soundName) {
                if (this.playing_id != -1) {
                    cc.ll.AudioMgr.stopPlay(this.playing_id);
                }
                cc.ll.AudioMgr.playLocalAudio(soundName, (id) => {
                    if (id != -1) {
                        this.voice_played = true;
                        this.playing_id = id;

                        let chatpanel = cc.find('Canvas/ChatPanel');
                        if (chatpanel) {
                            let logic = chatpanel.getComponent('ChatPanel');
                            logic.changeVoicePlayedId(id)
                        }
                    }
                }, callback)
                return true;
            }
        }
        return false;
    }

    autoPlay() {
        if (this.voice_played) {
            return;
        }
        let index = this.auto_play_list.shift();
        if (index == null) {
            return;
        }
        this.playVoice(index, () => {
            this.playing_id = -1;
            this.voice_played = false;
            this.autoPlay();
        });
    }

    getPlayingId() {
        return this.playing_id;
    }
}

module.exports = VoiceMgr;