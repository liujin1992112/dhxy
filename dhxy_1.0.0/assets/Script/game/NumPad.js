﻿let GameRes = require('./GameRes');
let CPubFunction = require('./PubFunction');
let CSpeak = require('./Speak');
let pNpcMgr = require('./NpcMgr');


cc.Class({
    extends: cc.Component,


    properties:
        {
        },


    onLoad()
    {
        this.pCallBack = null;

        let goOld = cc.find('NumPad');
        if (goOld != null )
        {
            goOld.destroy();
        }

    },

    NumPad_Init( pCallBack )
    {
        this.pCallBack = pCallBack;
    },

    start()
    {
        let vecChild = this.node.getChildren();
        for (let it in vecChild)
        {
            let strName = vecChild[it].name;
            cc.find(strName, this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "NumPad", "OnButton", strName));
        }

        this.nNum = 0;
    },


    OnButton(e, strBtn)
    {
        if (null == this.pCallBack)
            return;

        if (CPubFunction.IsDataInVecter(strBtn, ['1', '2', '3', '4', '5', '6', '7', '8', '9', '0']))
        {
            this.nNum = this.nNum *10 + parseInt(strBtn);
        }

        if (CPubFunction.IsDataInVecter(strBtn, ['delete']))
        {
            this.nNum = 0;
        }

        if (CPubFunction.IsDataInVecter(strBtn, ['ok', 'btnClose']))
        {
            this.node.destroy();
        }

        this.pCallBack(this.nNum);
    },
});
