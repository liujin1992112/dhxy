class local {
	constructor(){
		this.userid = 0;
		this.storage = {};
	}

	setUserid(userid){
		this.userid = userid;
	}

	set(key, value){
		let fkey = this.userid + key;
		cc.sys.localStorage.setItem(fkey, value);
	}

	get(key){
		let fkey = this.userid + key;
		return cc.sys.localStorage.getItem(fkey);
	}
}
module.exports = local;