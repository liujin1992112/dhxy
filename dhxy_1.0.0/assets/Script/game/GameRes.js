/**
 * 游戏资源管理
 */
class GameRes {
    constructor() {
        this.reslist = {};
        this.mapResList = {};
        this.roleResList = {};
        this.petResList = {};
        this.partnerResList = {};
        this.roleHeadAtlas = null;//角色头像图集
        this.petHeadAtlas = null;//宠物头像图集
        this.huobanHeadAtlas = null;
        this.emojiAtlas = null;
        this.itemIconAtlas = null;//物品图标图集
        this.skillIconAtlas = null;
        this.buffIconAtlas = null;

        this.m_mapEffectRes = {};

        this.m_mapPrefab = {};
        this.m_ChatEmojiAtlas = null;//聊天列表表情图集
    }

    setMapRes(mapid, sRes) {
        this.mapResList[mapid] = sRes;
    }

    setRoleRes(resid, sRes) {
        this.roleResList[resid] = sRes;
    }

    getRoleRes(resid) {
        return this.roleResList[resid];
    }

    setPetAtlas(resid, atlas) {
        this.petResList[resid] = atlas;
    }

    getPetRes(resid) {
        return this.petResList[resid];
    }

    setPartnerAtlas(resid, atlas) {
        this.partnerResList[resid] = atlas;
    }

    getPartnerRes(resid) {
        return this.partnerResList[resid];
    }

    setRoleHead(atlas) {
        this.roleHeadAtlas = atlas;
    }

    getRoleHead(roleid) {
        if (this.roleHeadAtlas == null) {
            return null;
        }
        return this.roleHeadAtlas.getSpriteFrame('role_' + roleid);
    }

    setPetHead(atlas) {
        this.petHeadAtlas = atlas;
    }

    getPetHead(petid) {
        if (this.petHeadAtlas == null) {
            return null;
        }
        return this.petHeadAtlas.getSpriteFrame(petid);
    }

    setHuobanHead(atlas) {
        this.huobanHeadAtlas = atlas;
    }

    getHuobanHead(huobanid) {
        if (this.huobanHeadAtlas == null) {
            return null;
        }
        return this.huobanHeadAtlas.getSpriteFrame('huoban_' + huobanid);
    }

    setItemIcon(atlas) {
        this.itemIconAtlas = atlas;
    }

    getItemIcon(iconid) {
        if (this.itemIconAtlas == null) {
            return null;
        }
        return this.itemIconAtlas.getSpriteFrame(iconid);
    }

    GetClipFromAtlas(strAtlas) {
        let vecFrame = [];

        for (let i = 1; ; i++) {
            let stFrame = this.m_mapEffectRes[strAtlas].getSpriteFrame(i);
            if (null == stFrame)
                break;

            vecFrame.push(stFrame);
        }

        let curClip = cc.AnimationClip.createWithSpriteFrames(vecFrame, 15);
        return curClip;
    }

    loadClip(strAtlas, callback) {
        cc.loader.loadRes('effect/' + strAtlas, cc.SpriteAtlas, function (err, atlas) {
            let vecFrame = [];
            for (let i = 1; ; i++) {
                let stFrame = atlas.getSpriteFrame(i);
                if (null == stFrame)
                    break;
                vecFrame.push(stFrame);
            }
            let curClip = cc.AnimationClip.createWithSpriteFrames(vecFrame, 15);
            callback(curClip);
        });
    }
}

let gameres = null;
module.exports = (() => {
    if (gameres == null) {
        gameres = new GameRes();
    }
    return gameres;
})();