﻿let GameRes = require('./GameRes');
let CPubFunction = require('./PubFunction');
let CSpeak = require('./Speak');
let pNpcMgr = require('./NpcMgr');
let CMainPlayerInfo = require('./MainPlayerInfo');

cc.Class({
    extends: cc.Component,


    properties:{
        btnNpcFunction:cc.Prefab,
        XianQiUpPanel:cc.Prefab,
        XianQiUpPanel:cc.Prefab,
        ShenBingUpPanel:cc.Prefab,
        ShenBingCombinePanel:cc.Prefab,
        RelationInputUI:cc.Prefab,
        RelationListUI:cc.Prefab,
        RelationListUI:cc.Prefab,
        NpcShopUI: cc.Prefab,
        PKPanel: cc.Prefab,
        ColoringRolePanel: cc.Prefab,
        PetFlyPanel: cc.Prefab,
    },


    onLoad() {
        this.nOnlyID = 0;
        this.nResID = 0;
    },

    NpcFunUI_Init(nOnlyID, nConfigID, strName, strDefaultTalk, mapBtnFunction, mapTaskBtn) {
        this.nOnlyID = nOnlyID;
        this.nConfigID = nConfigID;

        let stConfigInfo = pNpcMgr.GetNpcConfigInfo(nConfigID);

        cc.find('btnClose', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "NpcFunUI", "Close", 0));


        //形象

        CPubFunction.SetDragonBoneAvatar(cc.find('nodNpc/goSpeaker', this.node), cc.find('nodNpc/picAvatar', this.node), stConfigInfo.nResID);

        cc.find('nodNpc/picName/label', this.node).getComponent(cc.Label).string = strName;

        //语言框

        let nodContentTalk = cc.find('nodCustomRichText/view/content', this.node);
        let comCusRichText = nodContentTalk.getComponent('CustomRichText');
        comCusRichText.maxWidth = nodContentTalk.width - 6;
        comCusRichText.emojiAtlas = GameRes.m_ChatEmojiAtlas;
        comCusRichText.string = CPubFunction.GetDefault(strDefaultTalk, '');

        //按钮框

        let preButton = this.btnNpcFunction;
        let nodContent2 = cc.find('ScrollView/view/content', this.node);

        let nBtnHight = preButton.data.height;
        let nDis = 2;
        let nIndex = -1;

        let stStart = { nX: 145, nY: -30 };


        for (const it in mapTaskBtn) {
            nIndex++;
            let goBtn = CPubFunction.CreateSubNode(nodContent2, { nX: stStart.nX, nY: stStart.nY - nIndex * (nBtnHight + nDis) }, preButton, 'Task');
            goBtn.getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "NpcFunUI", 'OnNpcFunction', { strTaskName: it, stData: mapTaskBtn[it] }));
            cc.find('Label', goBtn).getComponent(cc.Label).string = it;

            let goTaskTip = cc.find('TaskTip', cc.find('Canvas/MainUI/right'));
            let comTaskTip = goTaskTip.getComponent('TaskTip');

            if (comTaskTip.stAuto.nTaskID == mapTaskBtn[it].nTaskID) {
                this.OnNpcFunction(goBtn.getComponent(cc.Button), { strTaskName: it, stData: mapTaskBtn[it] });
            }
        }


        for (const it in mapBtnFunction) {
            if (it.indexOf('daily') != -1) {
                let vecTmp = it.split(":");
                if (CMainPlayerInfo.IsAlreadyHasThisGropTask(vecTmp[1]))
                    continue;
            }

            nIndex++;
            let goBtn = CPubFunction.CreateSubNode(nodContent2, { nX: stStart.nX, nY: stStart.nY - nIndex * (nBtnHight + nDis) }, preButton, it);
            goBtn.getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "NpcFunUI", 'OnNpcFunction', it));

            cc.find('Label', goBtn).getComponent(cc.Label).string = mapBtnFunction[it];
        }

        nodContent2.height = Math.max(nodContent2.height, (nIndex + 1) * (nBtnHight + nDis) + 40);
    },


    Close() {
        this.node.destroy();
    },

    OnNpcFunction(stEvent, stParam) {
        let strTmp = stEvent.target.name;

        if (strTmp == 'Task') {
            let nTaskID = stParam.stData.nTaskID;
            let nStep = stParam.stData.nStep;

            let pSelf = this;

            let vecSpeak = stParam.stData.vecTmp;
            CPubFunction.CreateNpcTalk(vecSpeak, () => {
                cc.ll.net.send('c2s_task_talk_npc', { nTaskID: nTaskID, nStep: nStep, nNpcConfigID: pSelf.nConfigID, nNpcOnlyID: pSelf.nOnlyID });
            });
        }
        else if (strTmp == 'Box') {
        }
        else if (strTmp == 'Teacher') {
        }
        else if (strTmp == 'Team') {
        }
        else if (strTmp == 'bpjyr_bang'){  //回到帮派
        
            let comMapLogic = cc.find('Canvas/MapUI').getComponent('GameMapLogic');
            comMapLogic.changeMap(3002);
        }
        else if (strTmp.indexOf('daily') != -1) {
            let vecTmp = strTmp.split(":");
            if (vecTmp.length != 2)
                return;

            let nTaskGrop = vecTmp[1];
            cc.ll.net.send('c2s_start_grop_task', { nNpcOnlyID: this.nOnlyID, nTaskGrop: nTaskGrop });
        }
        else if (strTmp.indexOf('fuben') != -1) {
            let vecTmp = strTmp.split(":");
            if (vecTmp.length != 2)
                return;

            let nTaskGrop = vecTmp[1];
            cc.ll.net.send('c2s_incept_fuben_task', { nNpcOnlyID: this.nOnlyID, nTaskID: vecTmp[1] });
        }
        else if (strTmp == 'tiaozhan') {
            cc.ll.net.send('c2s_challenge_npc', { nOnlyID: this.nOnlyID, nConfigID: this.nConfigID });
        }
        else if (strTmp == 'hdl_xqhc') {
            let xqhc = cc.instantiate(this.XianQiUpPanel);
            xqhc.parent = cc.find('Canvas');
        }
        else if (strTmp == 'hdl_xqsj') {
            let xqsj = cc.instantiate(this.XianQiUpPanel);
            xqsj.parent = cc.find('Canvas');
        }
        else if (strTmp == 'hdl_sbsj') {
            let sbsj = cc.instantiate(this.ShenBingUpPanel);
            sbsj.parent = cc.find('Canvas');
        }
        else if (strTmp == 'hdl_sbhc') {
            let sbhc = cc.instantiate(this.ShenBingCombinePanel);
            sbhc.parent = cc.find('Canvas');
        }
        else if (strTmp == 'shop') {
            let goUI = CPubFunction.CreateSubNode(cc.find('Canvas/MainUI'), { nX: 0, nY: 0 }, this.NpcShopUI, 'NpcShopUI');
            let comUI = goUI.getComponent('NpcShopUI');
            comUI.nNpcConfigID = this.nConfigID;
        }
        else if (strTmp == 'taskreset') {
            cc.ll.net.send('c2s_task_reset', {});
        }
        else if (strTmp == 'changerace'){  //换种族
            cc.loader.loadRes("Prefabs/LoginCreateRole", function (err, prefab) {
                let mainui = cc.find('Canvas');
                let relive = cc.instantiate(prefab);
                relive.parent = mainui;
                relive.name = 'changeracelayer';
                let logic = relive.getComponent('LoginCreateLogic');
                logic.setOpenType(2);
            });
        }
        else if (strTmp == 'zhuansheng') {
            let strErr = '';
            let vecNeed = [100, 120, 140];
            if (cc.ll.player.relive >= 3) {
                strErr = `无法再转生了`
            }
            else if (cc.ll.player.level < vecNeed[cc.ll.player.relive]) {
                strErr = `你的等级不够，需达到${vecNeed[cc.ll.player.relive]}级才能转生`
            }

            if (strErr == '') {
                cc.loader.loadRes("Prefabs/LoginCreateRole", function (err, prefab) {
                    let mainui = cc.find('Canvas');
                    let relive = cc.instantiate(prefab);
                    relive.parent = mainui;
                    relive.name = 'relivelayer';
                    let logic = relive.getComponent('LoginCreateLogic');
                    logic.setOpenType(1);
                });
            }
            else {
                CPubFunction.CreateNpcNotice(10094, strErr);
            }

        }
        // 探监
        else if (strTmp == 'tanjian') {
            let comMapLogic = cc.find('Canvas/MapUI').getComponent('GameMapLogic');
            comMapLogic.changeMap(1201);
        }
        // 探监结束
        else if (strTmp == 'tanjianend') {
            let comMapLogic = cc.find('Canvas/MapUI').getComponent('GameMapLogic');
            comMapLogic.changeMap(1011);
        }
        // 水陆大会报名
        else if(strTmp == 'sldh'){
            cc.ll.net.send('c2s_shuilu_sign');
        }
        else if(strTmp == 'linghou'){
            cc.ll.net.send('c2s_linghou_fight',{
                mid: this.nOnlyID,
            });
        }
        // "mapButton": "{ \"planwar\":\"帮我写战书\", \"receivewar\":\"我要接受对方的挑战\", \"warrank\":\"决斗榜\", \"close\":\"打架是不好的行为\" }",
        else if (strTmp == 'planwar') {
            CPubFunction.CreateSubNode(cc.find('Canvas/MainUI'), { nX: 0, nY: 0 }, this.PKPanel, 'PKPanel');
        }
        else if (strTmp == 'receivewar') {
            let palaceFightIcon = cc.find('Canvas/MainUI/PalaceFightIcon');
            let logic = palaceFightIcon.getComponent('PalaceFightIcon');
            if (logic.hasPalaceFight()) {
                cc.ll.net.send('c2s_palace_rolelist', {
                    roleid: cc.ll.player.roleid,
                });
            }
            else {
                cc.ll.msgbox.addMsg('你没有被其他人邀请皇城决斗！');
            }

            /* if (!palaceFightIcon.active) {
                cc.ll.msgbox.addMsg('你没有要进行的决斗！');
            }
            else {
                palaceFightIcon.getComponent('PalaceFightIcon').showPalaceFightPanel();
            } */
        }
        else if (strTmp == 'warrank') {
            cc.ll.msgbox.addMsg('暂未开放！');
        }
        //我要结拜
        else if(strTmp == 'newBrother'){
            if (!cc.ll.player.teamInfo.objlist || !Array.isArray(cc.ll.player.teamInfo.objlist)) {
                CPubFunction.CreateNotice(cc.find('Canvas'), '必须先组队才能结拜', 2);
                return false;
            }

            CPubFunction.CreateSubNode(cc.find('Canvas/MainUI'), { nX: 0, nY: 0 }, this.RelationInputUI, 'RelationInputUI');
        }
        //新人加入结拜
        else if(strTmp == 'addBrother'){
            CPubFunction.CreateSubNodeByType(cc.find('Canvas/MainUI'), { nX: 0, nY: 0 }, this.RelationListUI, 'RelationListUI',1);       
        }
        //退出结拜
        else if(strTmp == 'leaveBrother'){
            CPubFunction.CreateSubNodeByType(cc.find('Canvas/MainUI'), { nX: 0, nY: 0 }, this.RelationListUI, 'RelationListUI',2);
        }
        else if (strTmp == 'rolecolor') { // 染色 
            let panel = cc.instantiate(this.ColoringRolePanel);
			panel.parent = cc.find('Canvas/MainUI');
			panel.setPosition(0, 0);
		}
        else if (strTmp == 'flyingup') { // 宠物飞升 
            let panel = cc.instantiate(this.PetFlyPanel);
			panel.parent = cc.find('Canvas/MainUI');
		}

        this.node.destroy();
    },
});
