let gameDefine = require('../common/define');

cc.Class({
    extends: cc.Component,

    properties: {
        NameLabel: cc.Label,
        HeadIcon: cc.Sprite,

        PerLabel: cc.Label,

        Race: cc.Sprite,
        RaceFont: cc.Sprite,

        RaceList: [cc.SpriteFrame],
        RaceFontList: [cc.SpriteFrame],
    },

    ctor(){
        this._accountid = 0;
        this._race = 0;
    },


    start() {
        
    },

    setInfo(accountid, name, race, headicon){
        this._accountid = accountid;
        this._race = race;

        this.NameLabel.string = name;

        this.Race.spriteFrame = this.RaceList[this.race];
        if (this.RaceFont && this.RaceFontList.length > 0){
            this.RaceFont.spriteFrame = this.RaceFontList[this.race];
        }
    },

    setLoadPercent(pre){
        this.PerLabel.string = pre;
    }
});