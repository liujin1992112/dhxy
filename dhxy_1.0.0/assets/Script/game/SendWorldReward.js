
let CPubFunction = require('./PubFunction');
cc.Class({
    extends: cc.Component,

    properties: {
        editbox_xianyu: cc.EditBox,
        editbox_num: cc.EditBox,
        curNum: cc.Label,

    },

    // onLoad () {},

    start() {
        this.curNum.string = cc.ll.player.gameData.jade;
    },

    // update (dt) {},



    onSendBtnClick() {
        let nodeCan = cc.find('Canvas');
        let yuNumStr = this.editbox_xianyu.string;
        let numStr = this.editbox_num.string;
        //提示玩家的操作信息
        if (yuNumStr < 1000) {
            CPubFunction.CreateNotice(nodeCan, '红包要低于1000仙玉', 2);
            return;
        }
        if (numStr < 50 || numStr > 200) {
            CPubFunction.CreateNotice(nodeCan, '红包数量请在50~200之间', 2);
            return;
        }
        cc.ll.net.send('c2s_world_reward', {
            roleid: cc.ll.player.roleid,
            yuNum: yuNumStr,
            num: numStr
        });
        // let msgStr = '我发了一个大大滴红包,快来抢。'
        // //聊天框提示
        // cc.ll.net.send('c2s_game_chat', {
        //     roleid: cc.ll.player.roleid,
        //     onlyid: cc.ll.player.onlyid,
        //     scale: 0,
        //     msg: msgStr,
        //     name: cc.ll.player.name,
        //     resid: cc.ll.player.resid,
        // });


        this.node.destroy();
    },




});



