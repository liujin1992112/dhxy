cc.Class({
	extends: cc.Component,
	properties: {
		ex_xianyu_label: cc.Label,
		xianyu_label:    cc.Label,
		money_label:     cc.Label,
		web_view_prefab: cc.Prefab,
        editbox:         cc.EditBox,
        xnode:           cc.Node,
	},

	init (data) {
        if (data) {
            this.goodsid = data.goodsid;
            this.ex_xianyu_label.string = data.ex_jade;
            this.xianyu_label.string = data.jade;
            this.money_label.string = '￥'+data.money;
        }
	},

	onButtonClick (event, param) {
        if (this.goodsid) {
            let send_data = {
                roleid: cc.ll.player.roleid,
                goodsid: this.goodsid,
                goodscount: 1,
                money: this.checkNum(this.money_label.string),
            };

            let node = cc.instantiate(this.web_view_prefab);
            node.parent = cc.find('Canvas');  
            node.getComponent('ChargeWebUI').setData(send_data);
            /* this.node.destroy(); */
        }
        else if(this.checkNum(this.editbox.string) > 998) {
            let send_data = {
                roleid: cc.ll.player.roleid,
                goodsid: 0,
                goodscount: 1,
                money: this.checkNum(this.editbox.string),
            };
            let node = cc.instantiate(this.web_view_prefab);
            node.parent = cc.find('Canvas');  
            node.getComponent('ChargeWebUI').setData(send_data);
        }
        else {
            cc.ll.msgbox.addMsg('请输入正确的充值金额！');
        }
	},

    showEditbox () {
        this.editbox.node.active = true;
        this.ex_xianyu_label.string = '0';
        this.xianyu_label.string = '0';
        this.money_label.string = '0';
        this.money_label.node.active = false;
        this.editbox.node.on('editing-did-ended', this.editEnd.bind(this));
    },

    checkNum (num) {
        num = num.replace(/￥/, '');
        if (isNaN(parseInt(num))) {
            num = 0;
        }
        else {
            num = Math.abs(parseInt(num));
        }
        return num;
    },

    editEnd () {
        let money = this.checkNum(this.editbox.string);
        if (money <= 998) {
            if (money == 0) {
                cc.ll.msgbox.addMsg('格式错误！'); 
            } else {
                cc.ll.msgbox.addMsg('充值不能少于998！');
                money = 0;
            }
            this.ex_xianyu_label.string = '0';
            this.xianyu_label.string = '0';
            this.editbox.string = '';
        }
        else if (money > 998) {
            this.ex_xianyu_label.string = this.calculateExJade(money);
            this.xianyu_label.string = money*100;
            this.editbox.string = '￥' + money;
        }
        this.xnode.x = -this.xianyu_label.node.width-this.xnode.width/2;
    },

    /*
     * 自定义充值金额计算奖励仙玉
     */
    calculateExJade (money) {
        let exjade = 0;
        for (let i = 6; i >= 1; --i) {
            let item = cc.ll.propData.charge[i];
            if (item && money >= item.money) {
                let count = Math.floor(money/item.money);
                exjade += count*item.ex_jade;
                money -= count*item.money;
            }
        }
        return exjade;
    },
});
