let GameRes = require('../GameRes');
/**
 * 战斗UI操作界面上的宠物Item节点脚本
 */
cc.Class({
    extends: cc.Component,

    properties: {
        nameLabel: cc.Label,
        levelLabel: cc.Label,
        headIcon: cc.Sprite,
        curFlag: cc.Node,
        zhenwangFlag: cc.Node,

        _fight: false,
        isFight: {
            get() {
                return this._fight;
            },
            set(n) {
                this._fight = n;
                this.curFlag.active = this._fight;
            }
        },

        _dead: false,
        isDead: {
            get() {
                return this._dead;
            },
            set(n) {
                this._dead = n;
                this.zhenwangFlag.active = this._dead;
            }
        },
    },

    ctor() {
        this.selectCallback = null;
    },

    start() {

    },

    setHeadIcon(resid) {
        this.headIcon.spriteFrame = GameRes.getPetHead(resid);
    },

    setName(str) {
        this.nameLabel.string = str;
    },
    setLevel(relive, level) {
        this.levelLabel.string = cc.js.formatStr('%s转%s级', relive, level);
    },

    selected(e, d) {
        if (this.selectCallback) {
            this.selectCallback();
        }
    }
});
