cc.Class({
	extends: cc.Component,

	properties: {
		fontList: [cc.Font],
		numLabel: cc.Label,
		num: {
			get() {
				return this.numLabel ? this.numLabel.string : '';
			},
			set(n) {
				this.numLabel && (this.numLabel.string = n, this.numLabel.node.active = n != 0);
			}
		},

		_font: 0,
		subhp: {
			get() {
				return this._font == 0;
			},
			set(n) {
				this._font = 0;
				this.numLabel.font = this.fontList[0];
			}
		},

		addhp: {
			get() {
				return this._font == 1;
			},
			set(n) {
				this._font = 1;
				this.numLabel.font = this.fontList[1];
				this.effect.node.active = false;
			}
		},

		mp: {
			get() {
				return this._font == 2;
			},
			set(n) {
				this._font = 2;
				this.numLabel.font = this.fontList[2];
				this.effect.node.active = false;
			}
		},

		hpmp: {
			get() {
				return this._font == 3;
			},
			set(n) {
				this._font = 3;
				this.numLabel.font = this.fontList[3];
				this.effect.node.active = false;
			}
		},

		effect: cc.Sprite,
		flags: [cc.SpriteFrame],
		_effnum: 0,
		baoji: {
			get() {
				return this._effnum == 0;
			},
			set(n) {
				if (n) {
					this._effnum = 0;
					this.effect && (this.effect.spriteFrame = this.flags[0]);
					this.numLabel.font = this.fontList[0];
					this.effect.node.active = true;
					this.effect.node.x = 0;
					this.effect.node.y = 0;
				} else {
					this._effnum = -1;
					this.effect.node.active = false;
				}
			}
		},

		kuangbao: {
			get() {
				return this._effnum == 1;
			},
			set(n) {
				if (n) {
					this._effnum = 1;
					this.effect && (this.effect.spriteFrame = this.flags[1]);
					this.numLabel.font = this.fontList[4];
					this.effect.node.active = true;
					this.effect.node.x = 0;
					this.effect.node.y = 0;
				} else {
					this._effnum = -1;
					this.effect.node.active = false;
				}
			}
		},

		pofang: {
			get() {
				return this._effnum == 2;
			},
			set(n) {
				if (n) {
					this._effnum = 2;
					this.effect && (this.effect.spriteFrame = this.flags[2]);
					this.numLabel.font = this.fontList[5];
					this.effect.node.active = true;
					this.effect.node.x = 5;
					this.effect.node.y = -10;
				} else {
					this._effnum = -1;
					this.effect.node.active = false;
				}
			}
		},
	},

	showFlag(flag) {
		for (let i = 0; i < this.flags.length; i++) {
			const flagNode = this.flags[i];
			flagNode.active = i == flag;
		}
	}
});