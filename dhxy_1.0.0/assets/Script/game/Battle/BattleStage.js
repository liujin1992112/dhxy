var SkillMgr = require('../Skill');
var SkillConfig = require('../../etc/skill_config')
var CPubFunction = require('../PubFunction');
let GameRes = require('../GameRes');
var GameDefine = require('../GameDefine');

let xuanrenpos = [{
	x: -330,
	y: 130
},
{
	x: 360,
	y: -140
},
]

/**
 * 战斗场景
 */
cc.Class({
	extends: cc.Component,

	properties: {
		/** 角色层(存放战斗角色) */
		objLayer: cc.Node,
		effectLayer: cc.Node,

		battleObj: cc.Prefab,
		effect: cc.Prefab,
		skillIcon: cc.Prefab,

		itemAtlas: cc.SpriteAtlas,
		xuanren: cc.Prefab,
		yihuan: cc.Prefab,
		huawu: cc.Prefab,

		lianjiNode: cc.Node,
		skillEffectNode: cc.Node,
	},

	ctor() {
		// 我方位置
		let rootpos = {
			x: 264,
			y: -144
		};
		let nextpos = {
			x: 264,
			y: -144
		};

		// 我方站位列表
		this.spos = [];
		for (let i = 0; i < 5; i++) {
			let temp = {
				role_x: nextpos.x,
				role_y: nextpos.y,
				pet_x: nextpos.x - 92,
				pet_y: nextpos.y + 54,
				hit_r_x: nextpos.x - 60,
				hit_r_y: nextpos.y + 28,
				hit_p_x: nextpos.x - 158,
				hit_p_y: nextpos.y + 80,
			}
			this.spos.push(temp);
			let k = i % 2 == 0 ? 1 : -1;
			let tmp = {
				x: rootpos.x - (67 * k * (Math.floor(i / 2) + 1)),
				y: rootpos.y - (36 * k * (Math.floor(i / 2) + 1))
			}
			nextpos = tmp;
		}
		// 我方特效位置
		this.sEpos = {
			nX: 205,
			nY: -84
		}

		// 敌方位置
		rootpos = {
			x: -285,
			y: 18
		};
		nextpos = {
			x: -285,
			y: 18
		};

		// 敌方站位列表
		this.epos = [];
		for (let i = 0; i < 5; i++) {
			let temp = {
				role_x: nextpos.x,
				role_y: nextpos.y,
				pet_x: nextpos.x + 90,
				pet_y: nextpos.y - 48,
				hit_r_x: nextpos.x + 50,
				hit_r_y: nextpos.y - 28,
				hit_p_x: nextpos.x + 140,
				hit_p_y: nextpos.y - 76,
			}
			this.epos.push(temp);
			let k = i % 2 == 0 ? 1 : -1;
			nextpos = {
				x: rootpos.x + (72 * k * (Math.floor(i / 2) + 1)),
				y: rootpos.y + (35 * k * (Math.floor(i / 2) + 1))
			}
		}
		// 敌方特效位置
		this.eEpos = {
			nX: -243,
			nY: 19
		};

		/** 战斗对象信息列表  key:对象的onlyid */
		this.btlObjInfoList = {};

		/** 战斗角色列表 key:对象的onlyid value:战斗对象节点BattleObj所在节点 */
		this.bobjList = {};
		this.teamA = [];
		this.teamB = [];

		// 自己的camp 队伍
		this.selfCamp = 0;

		// 默认选择技能
		this.selectSource = 0;
		this.roleSkillid = 0;
		this.petSkillid = 0;

		this.actions = [];

		/** 战斗阵营列表 key:对象的onlyid  value:阵营类型 */
		this.campList = {};

		this.effectResList = [];
	},

	start() {
		cc.ll.AudioMgr.playBgm('pve/pve1');//播放副本pve音效
	},

	clear() {
		for (const onlyid in this.bobjList) {
			if (this.bobjList.hasOwnProperty(onlyid)) {
				const obj = this.bobjList[onlyid];
				if (cc.isValid(obj)) {
					let objlogic = obj.getComponent('BattleObj');
					objlogic.clear();
					// obj.destroy();
				}
			}
		}
	},

	onDestroy() {
		// for (const resstr of this.effectResList) {
		// 	cc.loader.releaseRes(resstr, cc.SpriteFrame);
		// 	cc.loader.releaseRes(resstr, cc.SpriteAtlas);
		// 	cc.loader.releaseRes(resstr, cc.Texture2D);
		// }


		// cc.ll.AudioMgr.playBgm('yewai');
		cc.sys.garbageCollect();

		let audio = GameDefine.getMapAudio(cc.ll.player.mapid);
		cc.ll.AudioMgr.playBgm(audio);
	},

	onBattle() {
		let bui = cc.find('Canvas/BattleUILayer/BattleUI');
		this.btlUIlogic = bui.getComponent('BattleUI');
	},

	getSelfPetLogic() {
		for (const onlyid in this.bobjList) {
			if (this.bobjList.hasOwnProperty(onlyid)) {
				const obj = this.bobjList[onlyid];
				if (obj.ownid == cc.ll.player.onlyid && obj.pos > 0) {
					let logic = obj.getComponent('BattleObj');
					if (logic && logic.isPet()) {
						return logic;
					}
				}
			}
		}
		return null;
	},

	/**
	 * 设置队伍信息
	 * @param {*} teaminfo	队伍信息
	 * @param {*} camp 		阵营(1:自己  2:对方)
	 */
	setTeam(teaminfo, camp) {
		for (const member of teaminfo) {
			this.addBattleObj(member, camp);
		}
	},

	/**
	 * 创建战斗对象
	 * @param {*} btlinfo 战斗信息
	 */
	createBtlObj(btlinfo) {
		// console.log('btlinfo', btlinfo);
		let onlyid = btlinfo.onlyid;
		let resid = btlinfo.resid;
		let pos = btlinfo.pos;
		let name = btlinfo.name;
		let roletype = btlinfo.type;
		let hp = btlinfo.hp;
		let mp = btlinfo.mp;
		let maxhp = btlinfo.maxhp;
		let maxmp = btlinfo.maxmp;
		let ownid = btlinfo.ownonlyid;
		let weapon = btlinfo.weapon;
		let skilllist = btlinfo.skilllist;
		let isbb = btlinfo.isbb;
		let relive = btlinfo.relive;
		let camp = btlinfo.camp;
		let petColor = btlinfo.color ? btlinfo.color : -1;

		let team = this.teamA;
		let nodepos = this.getNodePos(camp, pos);
		let dir = 'up';
		if (camp == 1) {
			team = this.teamA;
			dir = 'up';
		} else if (camp == 2) {
			team = this.teamB;
			dir = 'down';
		}

		let bobj = cc.instantiate(this.battleObj);

		bobj.x = nodepos.x;
		bobj.y = nodepos.y;

		bobj.onlyid = onlyid;
		bobj.oldx = bobj.x;
		bobj.oldy = bobj.y;
		bobj.pos = pos;
		bobj.camp = camp;
		bobj.ownid = ownid;
		bobj.isbb = isbb;

		let objlogic = bobj.getComponent('BattleObj');
		objlogic.setInfo({
			resid: resid,
			name: name,
			onlyid: onlyid,
			type: roletype,
			hp: hp,
			mp: mp,
			maxhp: maxhp,
			maxmp: maxmp,
			ownid: ownid,
			weapon: weapon,
			skilllist: skilllist,
			camp: camp,
			relive: relive,
			petColor: petColor,
		});

		//是否变色
		objlogic.dir = dir;
		objlogic.play();//播放默认的待机动画
		this.scheduleOnce(() => {
			objlogic.setColor(btlinfo.color1, btlinfo.color2);
		}, 0);//设置战斗角色染色

		team.push(bobj);//将战斗角色加入队伍中
		this.bobjList[onlyid] = bobj;//将战斗角色节点管理
		if (cc.ll.player.onlyid == onlyid) {
			this.selfCamp = camp;
		}
		bobj.parent = this.objLayer;//设置战斗角色所属角色层

		if (ownid == cc.ll.player.onlyid && roletype == GameDefine.LivingType.Pet && pos > 0) {
			this.btlUIlogic.setPetId(onlyid);
			this.btlUIlogic.setPetSkill(skilllist);
		}

		this.campList[onlyid] = camp;//设置自己所属阵营
	},

	/**
	 * 添加战斗对象
	 * @param {*} btlinfo	战斗信息 
	 * @param {*} camp 		阵营
	 * @returns 
	 */
	addBattleObj(btlinfo, camp) {
		this.btlObjInfoList[btlinfo.onlyid] = btlinfo; // {}
		this.btlObjInfoList[btlinfo.onlyid].camp = camp;

		if (btlinfo.pos == 0) {
			return;
		}
		this.createBtlObj(this.btlObjInfoList[btlinfo.onlyid]);
	},

	removeBattleObj(onlyid) {
		let obj = this.bobjList[onlyid];
		if (obj) {
			// delete this.bobjList[onlyid];
			let index = this.teamA.indexOf(obj);
			if (index != -1) {
				this.teamA.splice(index, 1);
			}
			index = this.teamB.indexOf(obj);
			if (index != -1) {
				this.teamB.splice(index, 1);
			}
			// obj.destroy();
			obj.active = false;
			obj.pos = -1;
		}
	},

	getRoleLogicByOnlyId(onlyid) {
		let tarobj = this.bobjList[onlyid];
		if (tarobj) {
			let tarlogic = tarobj.getComponent('BattleObj');
			return tarlogic;
		}
		return null;
	},

	isEnemy(onlyid) {
		let bobj = this.bobjList[onlyid];
		return this.selfCamp != bobj.camp;
	},

	sortAllRole() {
		for (const onlyid in this.bobjList) {
			if (this.bobjList.hasOwnProperty(onlyid)) {
				const bobj = this.bobjList[onlyid];
				if (!bobj || !cc.isValid(bobj)) {
					continue;
				}
				bobj.zIndex = 1000 - bobj.y;
				bobj.oldy = bobj.y;
			}
		}
	},

	showSkillIconFly(skillid, x, y, tx, ty) {
		let sicon = cc.instantiate(this.skillIcon);
		let slogic = sicon.getComponent('SkillIcon');
		let skillinfo = SkillMgr.GetSkillInfo(skillid);
		slogic.icon = skillinfo.strIcon;
		slogic.width = 70;
		slogic.height = 70;
		sicon.x = x;
		sicon.y = y;
		sicon.zIndex = 99999;

		sicon.parent = this.node;
		sicon.runAction(cc.sequence(
			cc.jumpTo(0.6, tx, ty, 30, 1).easing(cc.easeInOut(3)),
			cc.delayTime(0.5),
			cc.removeSelf()
		));
	},

	selectCallback(actid = null, skillid = null) {
		let only_id = actid == null ? cc.ll.player.onlyid : actid;
		let skill_id = skillid == null ? SkillConfig.SkillIds.NormalAtkSkill : skillid;

		return (targetid) => {
			cc.ll.net.send('c2s_btl_act', {
				action: GameDefine.ActType.Skill,
				actionid: skill_id,
				targetid: targetid,
				onlyid: only_id,
			});
		}
	},

	selectSkill(onlyid, skillid) {
		let stData = SkillMgr.GetSkillInfo(skillid);
		let team = null;

		if (stData.skillActOn == SkillConfig.ActOn.All) {
			team = this.teamA.concat(this.teamB);
		} else if (stData.skillActOn == SkillConfig.ActOn.Self) {
			team = this.teamA;
		} else {
			team = this.teamB;
		}

		for (const bnode of team) {
			let bobj = bnode.getComponent('BattleObj');
			bobj.showSelectFlag(this.selectCallback(onlyid, skillid));
		}
	},

	getBattleObjNode(onlyid) {
		return this.bobjList[onlyid];
	},

	objTalk(info) {
		let obj = this.getBattleObjNode(info.onlyid);
		if (obj) {
			obj.getComponent('BattleObj').objTalk(info.msg);
		}
	},

	getObjCamp(onlyid) {
		return this.campList[onlyid];
	},

	hasBB() {
		for (const userid in this.bobjList) {
			const bnode = this.bobjList[userid];
			if (bnode.isbb) {
				return true;
			}
		}
		return false;
	},

	selectTarget(camp, func) {
		let team = null;
		if (camp == 1) {
			team = this.teamA;
		} else {
			team = this.teamB;
		}

		if (team) {
			for (const bnode of team) {
				let bobj = bnode.getComponent('BattleObj');
				bobj.showSelectFlag((targetid) => {
					func(targetid);
				});
			}
		}
	},

	playerAction(data) {
		let actobj = this.bobjList[data.onlyid];
		if (data.action == GameDefine.ActType.Skill && actobj) {
			let tox = actobj.x;
			let toy = actobj.y + 200;
			if (data.targetid != 0) {
				let tarobj = this.bobjList[data.targetid];
				tox = tarobj.x;
				toy = tarobj.y + 200;
			}
			this.showSkillIconFly(data.actionid, actobj.x, actobj.y + 200, tox, toy);
		}
		if (data.action == GameDefine.ActType.Item && actobj) {
			let tox = actobj.x;
			let toy = actobj.y + 200;
			if (data.targetid != 0) {
				let tarobj = this.bobjList[data.targetid];
				tox = tarobj.x;
				toy = tarobj.y + 200;
			}
			this.itemFly(data.actionid, actobj.x, actobj.y + 200, tox, toy);
		}

		this.cleanAllRoleSelectFlag();


		if (this.btlUIlogic.isAuto) {
			this.btlUIlogic.finishOper();
		} else {
			let logic = this.getSelfPetLogic();
			if (data.onlyid == cc.ll.player.onlyid) {
				if (logic && logic.isdead == 0) {
					this.btlUIlogic.setPetId(logic.onlyid);
					this.btlUIlogic.petOperation();
					for (const onlyid in this.bobjList) {
						const bobj = this.bobjList[onlyid];
						if (!cc.isValid(bobj)) {
							continue;
						}
						let objlogic = bobj.getComponent('BattleObj');
						if (objlogic.isdead == 0) {
							objlogic.onSkillTarget = true;
							objlogic.selectFunc = this.selectCallback(logic.onlyid);
						}
					}
					return;
				} else {
					this.btlUIlogic.setPetId(0);
					this.btlUIlogic.finishOper();
				}
			}
			if (logic && logic.onlyid == data.onlyid) {
				this.btlUIlogic.finishOper();
			}
		}
	},

	cleanAllRoleSelectFlag() {
		for (const bnode of this.teamA) {
			let bobj = bnode.getComponent('BattleObj');
			bobj.hideSelectFlag();
		}
		for (const bnode of this.teamB) {
			let bobj = bnode.getComponent('BattleObj');
			bobj.hideSelectFlag();
		}
	},

	roleBeHit(roleid, skillid, info) {
		let respone = info.respone;
		let subhp = info.num;
		let lefthp = info.hp;
		let leftmp = info.mp;
		let isdead = info.isdead;
		let bufflist = info.bufflist;
		let lianji = null;

		if (info.actaffix != null && info.actaffix != 'null' && info.actaffix != '') {
			let actaffix = CPubFunction.safeJson(info.actaffix);
			if (actaffix) {
				lianji = actaffix.lianji;
			}
		}

		let tarobj = this.bobjList[roleid];
		if (tarobj == null) {
			return;
		}
		if (!cc.isValid(tarobj)) {
			return;
		}
		let tarlogic = tarobj.getComponent('BattleObj');

		if (respone == GameDefine.BtlRespone.Dodge) {
			tarlogic.showShanbi();
		} else if (respone == GameDefine.BtlRespone.NoThing || respone == GameDefine.BtlRespone.Defense ||
			respone == GameDefine.BtlRespone.Protect || respone == GameDefine.BtlRespone.CriticalHit ||
			respone == GameDefine.BtlRespone.PoFang) {
			if (subhp < 0) {
				let spdrate = 1;
				if (lianji && lianji.hurt.length > 0) {
					spdrate = 0.4 + 0.6 / lianji.hurt.length;
				}
				tarlogic.play('hit', spdrate);
			}
			// 防御判断
			if (respone == GameDefine.BtlRespone.Defense) {
				tarlogic.showDef();
			}
			// 播放技能效果
			if (skillid != 0 && skillid != SkillConfig.SkillIds.NormalAtkSkill) {
				tarlogic.playEffect(skillid);
			}
			let effect = 0;
			if (respone == GameDefine.BtlRespone.CriticalHit) {
				effect = 1;
			} else if (respone == GameDefine.BtlRespone.PoFang) {
				effect = 5;
			}
			tarlogic.setmp(leftmp);
			if (cc.ll.player.onlyid == tarlogic.onlyid) {
				if (this.btlUIlogic) {
					this.btlUIlogic.setMp(leftmp);
				}
			}
			if (lianji && lianji.hurt.length > 0) {
				tarlogic.lianji(lianji.hurt, lefthp, isdead, effect);
				this.showLianji(lianji.hurt.length);
			} else {
				tarlogic.subHp(subhp, lefthp, isdead, effect);
			}

			// 如果是宠物死了，更新召唤面板
			if (isdead && tarlogic.isPet()) {
				this.onPetDead(tarlogic, info.actaffix);
			} else {
				if (bufflist) {
					tarlogic.checkBuff(bufflist);
				}
			}
		}
	},

	/**
	 * 根据阵营和站位获取战斗对象的位置
	 * @param {*} camp 
	 * @param {*} pos 
	 * @returns 
	 */
	getNodePos(camp, pos) {
		let poslist = this.spos;
		if (camp == 1) {
			poslist = this.spos;
		} else if (camp == 2) {
			poslist = this.epos;
		}
		if (pos <= 5) {
			let posinfo = poslist[pos - 1];
			return {
				x: posinfo.role_x,
				y: posinfo.role_y,
			}
		} else {
			let posinfo = poslist[pos - 5 - 1];
			return {
				x: posinfo.pet_x,
				y: posinfo.pet_y,
			}
		}
	},

	roleAttack(actid, skillid, targets) {
		cc.ll.AudioMgr.playActAudio();

		let actionid = actid;
		let actobj = this.bobjList[actionid];
		if (!cc.isValid(actobj)) {
			return;
		}
		let actlogic = actobj.getComponent('BattleObj');
		let nodepos = this.getNodePos(actobj.camp, actobj.pos);
		let oldx = nodepos.x;
		let oldy = nodepos.y;

		let self = this;
		let actlist = [];
		for (let i = 0; i < targets.length; i++) {
			const target = targets[i];
			let targetid = target.targetid;
			let tarobj = this.bobjList[targetid];
			if (!cc.isValid(tarobj)) {
				continue;
			}
			let tox = tarobj.x + 80;
			let toy = tarobj.y - 50;
			if (tarobj.camp == 1) {
				tox = tarobj.x - 80;
				toy = tarobj.y + 30;
			}
			//攻击者 跑向 被攻击者
			let t = Math.pow(Math.pow(toy - oldy, 2) + Math.pow(tox - oldx, 2), 0.5) / 600 * 0.3;
			actlist[actlist.length] = cc.moveTo(t, tox, toy);

			//保护者 跑向 被攻击者
			let lianjiinfo = null;
			let protectinfo = null;
			let protecter = null;
			let geshaninfo = null;
			let pt = 0,
				poldx = 0,
				poldy = 0;
			if (target.actaffix != null && target.actaffix != 'null') {
				let actaffix = CPubFunction.safeJson(target.actaffix);
				if (actaffix) {
					protectinfo = actaffix.protect;
					lianjiinfo = actaffix.lianji;
					geshaninfo = actaffix.geshan;
				}
			}
			if (target.respone == GameDefine.BtlRespone.Protect && protectinfo) {
				let protecter_id = protectinfo.roleid;
				protecter = this.bobjList[protecter_id];

				let ptx = tarobj.x + 40;
				let pty = tarobj.y - 20;
				if (tarobj.camp == 1) {
					ptx = tarobj.x - 40;
					pty = tarobj.y + 10;
				}

				let pnodepos = this.getNodePos(protecter.camp, protecter.pos);
				poldx = pnodepos.x;
				poldy = pnodepos.y;

				let pt = Math.pow(Math.pow(pty - poldy, 2) + Math.pow(ptx - poldx, 2), 0.5) / 200 * 0.1;

				actlist[actlist.length] = cc.callFunc(() => {
					protecter.stopActionByTag(987895);
					let action = cc.moveTo(pt, ptx, pty)
					action.setTag(987895);
					protecter.runAction(action);
				});
			}
			actlist[actlist.length] = cc.callFunc(() => {
				if (tarobj.camp == 2) {
					actlogic.dir = 'up';
				}
				if (tarobj.camp == 1) {
					actlogic.dir = 'down';
				}
				actobj.zIndex = 1000 - toy;
				let spdrate = 1;
				if (lianjiinfo && lianjiinfo.hurt.length > 0) {
					spdrate = 0.4 + 0.6 / lianjiinfo.hurt.length;
				}
				actlogic.play('attack', spdrate);
				// 
			});
			if (target.respone == GameDefine.BtlRespone.Protect && protectinfo) {
				actlist[actlist.length] = cc.callFunc(() => {
					this.roleBeHit(protecter.onlyid, skillid, {
						respone: protectinfo.respone,
						num: protectinfo.hurt,
						hp: protectinfo.hp,
						mp: protectinfo.mp,
						isdead: protectinfo.isdead,
					});
				});
			} else {
				if (lianjiinfo && lianjiinfo.hurt.length > 0) {
					actlist[actlist.length] = cc.callFunc(() => {
						this.roleBeHit(targetid, skillid, target);
					});
					actlist[actlist.length] = cc.delayTime(lianjiinfo.hurt.length * 0.15);
				} else {
					actlist[actlist.length] = cc.callFunc(() => {
						this.roleBeHit(targetid, skillid, target);
					});
				}
				if (geshaninfo) {
					actlist[actlist.length] = cc.delayTime(0.8);
					actlist[actlist.length] = cc.callFunc(() => {
						this.roleBeHit(geshaninfo.roleid, skillid, geshaninfo);
					});
				}
			}
			actlist[actlist.length] = cc.delayTime(1);
			actlist[actlist.length] = cc.callFunc(() => {
				if (targets.length - 1 == i) {
					actobj.runAction(cc.moveTo(t, oldx, oldy));
				}
				if (actobj.camp == 1) {
					actlogic.dir = 'up';
				}
				if (actobj.camp == 2) {
					actlogic.dir = 'down';
				}
				actlogic.play();
				actobj.zIndex = 1000 - oldy;
				if (target.respone == GameDefine.BtlRespone.Protect) {
					protecter.stopActionByTag(987895);
					let action = cc.moveTo(pt, poldx, poldy)
					action.setTag(987895);
					protecter.runAction(action);
				}
			});

			actlist[actlist.length] = cc.delayTime(0.2);
			actlist[actlist.length] = cc.callFunc(() => {
				if (targets.length - 1 == i) {
					self.playAction();
				}
			});
		}
		actobj.stopActionByTag(987895);
		let action = cc.sequence(actlist);
		if (action) {
			action.setTag(987895);
			actobj.runAction(action);
		} else {
			this.playAction();
		}

	},


	// roleAttack(actid, skillid, targets) {
	// 	cc.ll.AudioMgr.playActAudio();

	// 	let actionid = actid;
	// 	let actobj = this.bobjList[actionid];
	// 	let actlogic = actobj.getComponent('BattleObj');
	// 	let nodepos = this.getNodePos(actobj.camp, actobj.pos);
	// 	let oldx = nodepos.x;
	// 	let oldy = nodepos.y;

	// 	let self = this;
	// 	let actlist = [];
	// 	// for (let i = 0; i < targets.length; i++) {
	// 	const target = targets[0];
	// 	let targetid = target.targetid;
	// 	let tarobj = this.bobjList[targetid];

	// 	let tox = tarobj.x + 80;
	// 	let toy = tarobj.y - 50;
	// 	if (tarobj.camp == 1) {
	// 		tox = tarobj.x - 80;
	// 		toy = tarobj.y + 30;
	// 	}
	// 	//攻击者 跑向 被攻击者
	// 	let t = Math.pow(Math.pow(toy - oldy, 2) + Math.pow(tox - oldx, 2), 0.5) / 600 * 0.3;
	// 	actlist[actlist.length] = cc.moveTo(t, tox, toy);

	// 	//保护者 跑向 被攻击者
	// 	let lianjiinfo = null;
	// 	let protectinfo = null;
	// 	let protecter = null;
	// 	let pt = 0,
	// 		poldx = 0,
	// 		poldy = 0;
	// 	if (target.actaffix != null && target.actaffix != 'null') {
	// 		let actaffix = CPubFunction.safeJson(target.actaffix);
	// 		if (actaffix) {
	// 			protectinfo = actaffix.protect;
	// 			lianjiinfo = actaffix.lianji;
	// 		}
	// 	}
	// 	if (target.respone == GameDefine.BtlRespone.Protect && protectinfo) {
	// 		let protecter_id = protectinfo.roleid;
	// 		protecter = this.bobjList[protecter_id];

	// 		let ptx = tarobj.x + 40;
	// 		let pty = tarobj.y - 20;
	// 		if (tarobj.camp == 1) {
	// 			ptx = tarobj.x - 40;
	// 			pty = tarobj.y + 10;
	// 		}

	// 		let pnodepos = this.getNodePos(protecter.camp, protecter.pos);
	// 		poldx = pnodepos.x;
	// 		poldy = pnodepos.y;

	// 		let pt = Math.pow(Math.pow(pty - poldy, 2) + Math.pow(ptx - poldx, 2), 0.5) / 200 * 0.1;

	// 		actlist[actlist.length] = cc.callFunc(() => {
	// 			protecter.stopActionByTag(987895);
	// 			let action = cc.moveTo(pt, ptx, pty)
	// 			action.setTag(987895);
	// 			protecter.runAction(action);
	// 		});
	// 	}
	// 	actlist[actlist.length] = cc.callFunc(() => {
	// 		if (tarobj.camp == 2) {
	// 			actlogic.dir = 'up';
	// 		}
	// 		if (tarobj.camp == 1) {
	// 			actlogic.dir = 'down';
	// 		}
	// 		actobj.zIndex = 1000 - toy;
	// 		let spdrate = 1;
	// 		if (lianjiinfo && lianjiinfo.hurt.length > 0) {
	// 			spdrate = 0.4 + 0.6 / lianjiinfo.hurt.length;
	// 		}
	// 		actlogic.play('attack', spdrate);
	// 		// 
	// 	});
	// 	if (target.respone == GameDefine.BtlRespone.Protect && protectinfo) {
	// 		actlist[actlist.length] = cc.callFunc(() => {
	// 			this.roleBeHit(protecter.onlyid, skillid, {
	// 				respone: protectinfo.respone,
	// 				num: protectinfo.hurt,
	// 				hp: protectinfo.hp,
	// 				mp: protectinfo.mp,
	// 				isdead: protectinfo.isdead,
	// 			});
	// 		});
	// 	} else {
	// 		if (lianjiinfo && lianjiinfo.hurt.length > 0) {
	// 			actlist[actlist.length] = cc.callFunc(() => {
	// 				this.roleBeHit(targetid, skillid, target);
	// 			});
	// 			actlist[actlist.length] = cc.delayTime(lianjiinfo.hurt.length * 0.15);
	// 		} else {
	// 			actlist[actlist.length] = cc.callFunc(() => {
	// 				this.roleBeHit(targetid, skillid, target);
	// 			});
	// 		}
	// 	}

	// 	if (targets.length > 1) {
	// 		actlist[actlist.length] = cc.delayTime(0.8);
	// 		actlist[actlist.length] = cc.callFunc(() => {
	// 			for (let otheridx = 1; otheridx < targets.length; otheridx++) {
	// 				const otarget = targets[otheridx];
	// 				let otargetid = otarget.targetid;
	// 				this.roleBeHit(otargetid, skillid, otarget);
	// 			}
	// 		});
	// 	}

	// 	actlist[actlist.length] = cc.delayTime(1);
	// 	actlist[actlist.length] = cc.callFunc(() => {
	// 		if (target.respone == GameDefine.BtlRespone.Protect) {
	// 			protecter.stopActionByTag(987895);
	// 			let action = cc.moveTo(pt, poldx, poldy)
	// 			action.setTag(987895);
	// 			protecter.runAction(action);
	// 		}
	// 	});
	// 	actlist[actlist.length] = cc.callFunc(() => {

	// 		actobj.runAction(cc.moveTo(t, oldx, oldy));
	// 		if (actobj.camp == 1) {
	// 			actlogic.dir = 'up';
	// 		}
	// 		if (actobj.camp == 2) {
	// 			actlogic.dir = 'down';
	// 		}
	// 		actlogic.play();
	// 		actobj.zIndex = 1000 - oldy;
	// 	});

	// 	actlist[actlist.length] = cc.delayTime(0.2);
	// 	actlist[actlist.length] = cc.callFunc(() => {
	// 		self.playAction();
	// 	});

	// 	actobj.stopActionByTag(987895);
	// 	let action = cc.sequence(actlist);
	// 	if (action) {
	// 		action.setTag(987895);
	// 		actobj.runAction(action);
	// 	} else {
	// 		this.playAction();
	// 	}

	// },

	magicHit(skillid, actinfos) {
		for (const actinfo of actinfos) {
			let targetid = actinfo.targetid;

			this.roleBeHit(targetid, skillid, actinfo);
		}
	},

	// 远程释放魔法
	roleMagic(btlact) {
		let actionid = btlact.actid;
		let skillid = btlact.actionid;

		let skilldata = SkillMgr.GetSkillInfo(skillid);
		if (skilldata.bEffectPos == SkillConfig.EffectPos.Stage) {
			if (skilldata.skillActOn == SkillConfig.ActOn.Self) {
				// 自己加buff
				if (this.isEnemy(actionid)) {
					this.playEffectOnStage(skillid, true);
				} else {
					this.playEffectOnStage(skillid, false);
				}
			} else {
				if (this.isEnemy(actionid)) {
					this.playEffectOnStage(skillid, false);
				} else {
					this.playEffectOnStage(skillid, true);
				}
			}
		}
		let addtime = 0;
		for (const actinfos of btlact.act) {
			if (actinfos.actaffix != '' && actinfos.actaffix != null && actinfos.actaffix != 'null') {
				let actaccfix = CPubFunction.safeJson(actinfos.actaffix);
				if (actaccfix && actaccfix.niepan) {
					addtime = 1;
					break;
				}
			}
		}

		let actobj = this.bobjList[actionid];
		if (!cc.isValid(actobj)) {
			return;
		}
		let actlogic = actobj.getComponent('BattleObj');
		let self = this;
		actobj.stopActionByTag(987895);
		let action = cc.sequence(
			cc.callFunc(() => {
				actlogic.useSkill(skillid);
			}),
			cc.delayTime(0.5),
			cc.callFunc(() => {
				self.magicHit(skillid, btlact.act);
			}),
			cc.delayTime(1.6 + addtime),
			cc.callFunc(() => {
				self.playAction();
			}),
		)
		action.setTag(987895);
		actobj.runAction(action);
	},

	playEffectOnStage(skillid, enemypos = true) {
		let stData = SkillMgr.GetSkillInfo(skillid);
		let pos = enemypos ? this.eEpos : this.sEpos;
		let str = stData.strParticleEffect;
		if (stData.bEffectDir) {
			str = enemypos ? stData.strParticleEffect : (stData.strParticleEffect + '_3');
		}

		this.effectResList.push('effect/' + str);
		// CPubFunction.CreateSubFrame(this.node, pos, this.effect, str, () => {
		// });
		this.skillEffectNode.x = pos.nX;
		this.skillEffectNode.y = pos.nY;

		let find = false;
		let animation = this.skillEffectNode.getComponent(cc.Animation);
		let clips = animation.getClips();
		for (const clip of clips) {
			if (clip.name == str) {
				find = true;
				break;
			}
		}
		let playAnimation = () => {
			animation.play(str);
			animation.on('stop', () => {
				if (this.skillEffectNode) {
					let spr = this.skillEffectNode.getComponent(cc.Sprite);
					spr.spriteFrame = null;
				}
			});
		}
		if (!find) {
			GameRes.loadClip(str, newclip => {
				newclip.name = str
				newclip.wrapMode = cc.WrapMode.Normal; //循环播放 ？
				animation.addClip(newclip);
				playAnimation();
			});
		} else {
			playAnimation();
		}
	},

	roleCatch(actionid, targetid, respone) {
		let actobj = this.bobjList[actionid];
		let actlogic = actobj.getComponent('BattleObj');
		let tarobj = this.bobjList[targetid];
		let tarlogic = tarobj.getComponent('BattleObj');

		if (!cc.isValid(actobj) || !cc.isValid(tarobj)) {
			return;
		}

		let self = this;
		actobj.runAction(cc.sequence(
			cc.callFunc(() => {
				actlogic.play('magic');
			}),
			cc.delayTime(0.5),
			cc.callFunc(() => {
				tarlogic.play('hit');
			}),
			cc.delayTime(1),
			cc.callFunc(() => {
				if (GameDefine.BtlRespone.Catched == respone) {
					tarlogic.clean();
					self.removeBattleObj(targetid);
				}
			}),
			cc.delayTime(0.5),
			cc.callFunc(() => {
				self.playAction();
			}),
		));


		CPubFunction.CreateSubFrame(this.node, this.eEpos, this.effect, 100003, () => {

		});
	},

	onPetEnter(enterinfo) {
		let buffs = enterinfo.buffs;
		if (buffs) {
			for (const skillid in buffs) {
				if (buffs.hasOwnProperty(skillid)) {
					const broleids = buffs[skillid];
					for (const roleid of broleids) {
						let rolelogic = this.getRoleLogicByOnlyId(roleid);
						if (rolelogic) {
							rolelogic.addBuff(skillid);
						}
					}
				}
			}
		}
	},

	onPetShanXian(shanxianinfo) {
		let bobj = this.btlObjInfoList[shanxianinfo.petoid];
		bobj.pos = shanxianinfo.pos;
		this.createBtlObj(bobj);
	},

	onPetDead(petlogic, actaffix) {
		let realDead = () => {
			this.btlUIlogic.uiPetDead(petlogic.onlyid);
			setTimeout(() => {
				this.removeBattleObj(petlogic.onlyid);
			}, 1 * 1000);
		}
		// 宠物死了是否有后续动作
		let affix = CPubFunction.safeJson(actaffix);
		if (affix) {
			if (affix.niepan) {
				setTimeout(() => {
					petlogic.playEffect(SkillConfig.SkillIds.NiePan, () => {
						petlogic.subHp(affix.niepan.hp, affix.niepan.mp, false, 4); // 4 涅槃重生
					});
				}, 0.8 * 1000);
				petlogic.checkBuff(affix.niepan.bufflist)
			} else {
				realDead();
			}
			if (affix.shanxian) {
				this.onPetShanXian(affix.shanxian);
				if (affix.petenter) {
					this.onPetEnter(affix.petenter);
				}
			}
		} else {
			realDead();
		}
	},

	roleSummon(acts) {
		let action = acts[0];
		if (action) {
			if (action.targetid != 0 && action.respone == GameDefine.BtlRespone.SummonBack) {
				this.removeBattleObj(action.targetid);
			}
			action = acts[1];
			if (action.targetid == 0 && action.respone == GameDefine.BtlRespone.SummonFaild) {
				CPubFunction.CreateNotice(cc.find('Canvas/MainUI'), '召唤失败', 2);
			}
			if (action.targetid != 0 && action.respone == GameDefine.BtlRespone.Summon) {
				let bobj = this.btlObjInfoList[action.targetid];
				bobj.pos = action.num;

				this.createBtlObj(bobj);
				if (action.actaffix) {
					let tinfo = CPubFunction.safeJson(action.actaffix);
					if (tinfo) {
						if (tinfo.petenter) {
							this.onPetEnter(tinfo.petenter)
						}
					}
				}
			}
		}
	},

	roleSummonBack(acts) {
		let action = acts[0];
		if (action) {
			if (action.targetid != 0 && action.respone == GameDefine.BtlRespone.SummonBack) {
				this.removeBattleObj(action.targetid);
			}
		}
	},

	itemFly(itemid, x, y, tx, ty) {
		let sicon = cc.instantiate(this.skillIcon);
		let slogic = sicon.getComponent('SkillIcon');
		// slogic.icon = itemid;

		slogic.sprite.spriteFrame = this.itemAtlas.getSpriteFrame(itemid);
		slogic.width = 70;
		slogic.height = 70;
		sicon.x = x;
		sicon.y = y;
		sicon.zIndex = 99999;

		sicon.parent = this.node;
		sicon.runAction(cc.sequence(
			cc.jumpTo(0.6, tx, ty, 30, 1).easing(cc.easeInOut(3)),
			cc.delayTime(0.5),
			cc.removeSelf(),
		));
	},

	roleItem(actionid, acts) {
		let action = acts[0];
		if (action) {
			if (action.targetid != 0) {
				let actobj = this.bobjList[actionid];
				let toobj = this.bobjList[action.targetid];
				let tox = toobj.x;
				let toy = toobj.y + 200;
				this.itemFly(action.respone, actobj.x, actobj.y + 200, tox, toy);
				let actlogic = actobj.getComponent('BattleObj');
				actlogic.play('attack');

				setTimeout(() => {
					let logic = toobj.getComponent('BattleObj');
					if (action.acttype == 2) {
						logic.subHp(action.num, action.hp, action.isdead, 2);
					} else if (action.acttype == 3) {
						logic.subMp(action.num);
					} else if (action.acttype == 6) {
						logic.subHp(action.num, action.hp, action.isdead, 3);
					}
				}, 0.6 * 1000);

				setTimeout(() => {
					this.playAction();
				}, 1.5 * 1000);
			}
		}
	},

	showStageEffect(effect) {
		for (const einfo of effect) {
			let roleid = einfo.role;
			let effect = einfo.eff;
			let camp = this.getObjCamp(roleid);
			let epos = xuanrenpos[0];
			let targetcamp = 2;
			if (camp == 2) {
				epos = xuanrenpos[1];
				targetcamp = 1;
			}
			if (effect == SkillConfig.SkillIds.XuanRen) {
				let xuanren = this.effectLayer.getChildByName(targetcamp + 'xuanren');
				if (xuanren == null) {
					xuanren = cc.instantiate(this.xuanren);
					xuanren.name = targetcamp + 'xuanren';
					xuanren.x = epos.x;
					xuanren.y = epos.y;
					xuanren.parent = this.effectLayer;
				}
			}

			if (effect == SkillConfig.SkillIds.YiHuan) {
				let yihuan = this.effectLayer.getChildByName(targetcamp + 'yihuan');
				if (yihuan == null) {
					yihuan = cc.instantiate(this.yihuan);
					yihuan.name = targetcamp + 'yihuan';
					yihuan.x = epos.x;
					yihuan.y = epos.y;
					yihuan.parent = this.effectLayer;
				}
			}

			if (effect == SkillConfig.SkillIds.HuaWu) {
				let huawu = this.effectLayer.getChildByName(targetcamp + 'huawu');
				if (huawu == null) {
					huawu = cc.instantiate(this.huawu);
					huawu.name = targetcamp + 'huawu';
					huawu.x = epos.x;
					huawu.y = epos.y;
					huawu.parent = this.effectLayer;
				}
			}
		}
	},

	destroyStageEffect(camp, effect) {
		let eff = this.effectLayer.getChildByName(camp + '' + effect);
		if (eff != null) {
			eff.destroy();
		}
	},

	////////////////////////////战斗部分///////////////////////////////
	btlRoundBegin(data) {
		let self = this;
		this.sortAllRole();

		this.magicHit(0, data.act);

		this.showStageEffect(data.effect);

		setTimeout(() => {
			self.btlUIlogic.startRound();
			for (const onlyid in self.bobjList) {
				if (self.bobjList.hasOwnProperty(onlyid)) {
					const bobj = self.bobjList[onlyid];
					if (!cc.isValid(bobj)) {
						continue;
					}

					let objlogic = bobj.getComponent('BattleObj');
					if (objlogic.isdead == 0) {
						objlogic.onSkillTarget = true;
						objlogic.selectFunc = self.selectCallback();
					}
				}
			}
		}, 1 * 1000);
	},

	btlRound(data) {
		this.btlUIlogic.round = data.round;
		this.actions = data.acts;
		this.btlUIlogic.hideBtlTimer();
		this.btlUIlogic.finishState();
		this.cleanAllRoleSelectFlag();
		this.playAction();
	},

	playAction() {
		// int32 actid = 1; // 行动者的onlyid
		// int32 action = 2; // 1技能 2道具 3召唤
		// int32 actionid = 3; // 随action改变
		// repeated btlAct act = 4; // 效果影响多少目标

		let action = this.actions.shift();
		if (action) {
			let actcallback = () => {
				if (action.action == GameDefine.ActType.Skill) {
					// 使用技能
					let skill = SkillMgr.GetSkillInfo(action.actionid);
					if (skill.atkType == SkillConfig.AttackType.Melee) {
						this.roleAttack(action.actid, skill.nID, action.act);
					} else {
						this.roleMagic(action);
					}
				} else if (action.action == GameDefine.ActType.RunAway) {
					// 逃跑
					let actlogic = this.getRoleLogicByOnlyId(action.actid);
					actlogic.playRunAway(action.actionid);
					let self = this;
					this.node.runAction(cc.sequence(
						cc.delayTime(1),
						cc.callFunc(() => {
							self.playAction();
						})
					));
				} else if (action.action == GameDefine.ActType.Catch) {
					// 捕捉
					//action.act[0].targetid
					//respone
					this.roleCatch(action.actid, action.act[0].targetid, action.act[0].respone);
				} else if (action.action == GameDefine.ActType.Protect) {
					this.playAction();
				} else if (action.action == GameDefine.ActType.Summon) {
					this.roleSummon(action.act);
					setTimeout(() => {
						this.playAction();
					}, 1000);
				} else if (action.action == GameDefine.ActType.SummonBack) {
					this.roleSummonBack(action.act);
					setTimeout(() => {
						this.playAction();
					}, 1000);
				} else if (action.action == GameDefine.ActType.Item) {
					this.roleItem(action.actid, action.act);
				}
				let actlogic = this.getRoleLogicByOnlyId(action.actid);
				actlogic.checkBuff(action.bufflist);
			}

			let cc_act = [];
			if (action.actbef != null && action.actbef != 'null') {
				let actbef = null;
				try {
					actbef = JSON.parse(action.actbef);
				} catch (e) { }
				if (actbef) {
					let actobj = this.bobjList[action.actid];
					let actlogic = actobj.getComponent('BattleObj');
					if (actbef.huawu) {
						actlogic.showSkillTips(SkillConfig.SkillIds.HuaWu);
						this.destroyStageEffect(actobj.camp, 'huawu');
						setTimeout(() => {
							this.playAction();
						}, 1000);
						return;
					}
					if (actbef.xuanren) {
						cc_act[cc_act.length] = cc.callFunc(() => {
							actlogic.showSkillTips(SkillConfig.SkillIds.XuanRen);
							actlogic.subHp(-actbef.xuanren, actbef.hp, actbef.isdead);
							this.destroyStageEffect(actobj.camp, 'xuanren');
						});
						cc_act[cc_act.length] = cc.delayTime(0.5);

					}
					if (actbef.yihuan) {
						cc_act[cc_act.length] = cc.callFunc(() => {
							actlogic.showSkillTips(SkillConfig.SkillIds.YiHuan);
							actlogic.subMp(-actbef.yihuan);
							this.destroyStageEffect(actobj.camp, 'yihuan');
						});
						cc_act[cc_act.length] = cc.delayTime(0.5);

					}

					cc_act[cc_act.length] = cc.callFunc(() => {
						actlogic.sethp(actbef.hp);
						actlogic.setmp(actbef.mp);
						if (cc.ll.player.onlyid == actlogic.onlyid) {
							if (this.btlUIlogic) {
								this.btlUIlogic.setMp(actbef.mp);
							}
						}
					});

					if (action.isdead == 1) {
						cc_act[cc_act.length] = cc.delayTime(0.5);
						cc_act[cc_act.length] = cc.callFunc(() => {
							actlogic.play('die');
						});
					}
					cc_act[cc_act.length] = cc.callFunc(actcallback);
				}
			}
			if (cc_act.length > 0) {
				this.node.runAction(cc.sequence(cc_act));
			} else {
				actcallback();
			}
		} else {

		}
	},

	showLianji(num) {
		let numNode = cc.find('num', this.lianjiNode);
		let numLabel = numNode.getComponent(cc.Label);
		for (let i = 0; i < num; i++) {
			let actlist = [];
			actlist[actlist.length] = cc.delayTime(i * 0.15);
			actlist[actlist.length] = cc.callFunc(() => {
				this.lianjiNode.opacity = 255;
			})
			actlist[actlist.length] = cc.scaleTo(0.1, 1.2);
			actlist[actlist.length] = cc.scaleTo(0.3, 1);
			actlist[actlist.length] = cc.delayTime(0.5);
			if (i == num - 1) {
				actlist[actlist.length] = cc.fadeOut(0.3);
			}
			this.lianjiNode.runAction(cc.sequence(actlist));

			let actlist2 = [];
			actlist2[actlist2.length] = cc.delayTime(i * 0.15);
			actlist2[actlist2.length] = cc.callFunc(() => {
				numLabel.string = i + 1;
			})
			actlist2[actlist2.length] = cc.scaleTo(0.1, 1.2);
			actlist2[actlist2.length] = cc.scaleTo(0.1, 1);
			numNode.runAction(cc.sequence(actlist2));
		}
	},








	///////////////   TEST  /////////////////////////

	test() {
		for (let i = 0; i < 5; i++) {
			let oid = 1000 + i;
			if (i == 0) {
				oid = cc.ll.player.onlyid
			}
			this.addBattleObj({
				onlyid: oid,
				resid: 1001,
				pos: i + 1,
				name: 'role' + i,
			}, 1);
			this.addBattleObj({
				onlyid: 1000 + 5 + i,
				resid: 1001,
				pos: i + 5 + 1,
				name: 'pet' + i,
			}, 1);
			this.addBattleObj({
				onlyid: 2000 + i,
				resid: 1001,
				pos: i + 1,
				name: 'role' + i,
			}, 2);
			this.addBattleObj({
				onlyid: 2000 + 5 + i,
				resid: 1001,
				pos: i + 5 + 1,
				name: 'pet' + i,
			}, 2);
		}

		this.sortAllRole();
	},

	onTestAttack() {
		let actid = Math.floor(Math.random() * (1009 - 1000 + 1) + 1000);
		if (actid == 1000) {
			actid = cc.ll.player.onlyid;
		}
		let target = Math.floor(Math.random() * (2009 - 2000 + 1) + 2000);
		let tnum = Math.floor(Math.random() * (4000 + 1)) - 2000;
		// this.objAttack({
		// 	actionid: actid,
		// 	targetid: target,
		// 	respone: 0,
		// 	num: tnum,
		// });
		this.objMagic({
			actionid: actid,
			targetid: target,
			respone: 0,
			num: tnum,
		});
	},
	onTestAttack2() {
		let actid = Math.floor(Math.random() * (1009 - 1000 + 1) + 1000);
		if (actid == 1000) {
			actid = cc.ll.player.onlyid;
		}
		let target = Math.floor(Math.random() * (2009 - 2000 + 1) + 2000);
		let tnum = Math.floor(Math.random() * (4000 + 1)) - 2000;
		this.objMagic({
			actionid: target,
			targetid: actid,
			respone: 0,
			num: tnum,
		});
	},

	testObjEffect() {
		let actid = Math.floor(Math.random() * (1009 - 1000 + 1) + 1000);
		if (actid == 1000) {
			actid = cc.ll.player.onlyid;
		}
		let target = Math.floor(Math.random() * (2009 - 2000 + 1) + 2000);
		let a = Math.floor(Math.random() * 100);
		let onlyid = actid;
		if (a > 50) {
			onlyid = target;
		}
		let skillid = 1005;

	},
})