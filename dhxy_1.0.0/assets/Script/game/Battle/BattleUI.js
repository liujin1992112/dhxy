let SkillConfig = require('../../etc/skill_config');
let GameDefine = require('../GameDefine');
let SkillMgr = require('../Skill');

/**
 * 战斗UI操作界面
 */
cc.Class({
	extends: cc.Component,

	properties: {
		/** 每一回合操作倒计时节点 */
		RoundTime: cc.Node,

		/** 数组 0:自动战斗按钮 1:取消自动战斗按钮 */
		AutoBtn: [cc.Node],

		/** 非自动战斗操作节点 */
		btmBtl: cc.Node,
		upBtl: cc.Node,
		petBtns: cc.Node,

		btnLayer: cc.Node,

		/** 操作事件倒计时Label */
		btlTimerLable: cc.Label,

		/** 第几回合Label */
		roundLable: cc.Label,
		round: {
			get() {
				return this.roundLable.string;
			},
			set(n) {
				this.roundLable.string = n;
			}
		},

		skillPanel: cc.Node,
		autoSkillPanel: cc.Node,
		autoPetSkillPanel: cc.Node,
		petSkillPanel: cc.Node,
		petSkillNode: cc.Node,

		backSkillBtn: cc.Node,

		autoIcons: [cc.Sprite],
		autoLayer: cc.Node,

		typeNodes: [cc.Sprite],
		typeTxtAtlas: cc.SpriteAtlas,
		skillNodes: [cc.Node],
		autoSkillNodes: [cc.Node],
		autoPetSkillNodes: [cc.Node],

		skillAtlas: cc.SpriteAtlas,

		catchBtn: cc.Node,

		mpLabel: cc.Label,

		petPanel: cc.Node,
		petItem: cc.Node,

		itemPanel: cc.Node,

		_test: 0,
		test: {
			get() {
				return this._test;
			},
			set(n) {
				this._test = n;
				n == 0 ? this.setPetSkill([]) : this.setPetSkill([1011]);
			}
		}
	},

	ctor() {
		this.stageLogic = null;
		this.MainUILogic = null;
		this.lastSkill = 0;
		this.petLastSkill = 0;
		this.backType = 0; // 1 skill 2 btnlayer
		this.battleTimer = 0;
		this.petId = 0;

		this.petItems = [];

		this.petSkillNodes = [];
		this.petAutoSkillNodes = [];

		this.isAuto = null;
	},


	start() {
		this.initSkillPanel();
		this.finishState();

		if (this.MainUILogic == null) {
			let MainUI = cc.find('Canvas/MainUI');
			let MainUILogic = MainUI.getComponent('MainUI');
			this.MainUILogic = MainUILogic;
		}
		if (this.isAuto == null) {
			let isauto = cc.ll.local.get('battle_isauto');
			if (isauto == '' || isauto == null || isNaN(isauto)) {
				isauto = 0;
			}
			this.isAuto = isauto != 0;
		}

		let skill = cc.ll.local.get('battle_r_skill');
		if (skill == null || skill == 0) {
			skill = SkillConfig.SkillIds.NormalAtkSkill;
		}
		this.setLastAction({
			acttype: GameDefine.ActType.Skill,
			actid: skill,
		});

		let pskill = cc.ll.local.get('battle_p_skill');
		if (pskill == null || pskill == 0) {
			pskill = SkillConfig.SkillIds.NormalAtkSkill;
		}
		this.setPetLastAction({
			acttype: GameDefine.ActType.Skill,
			actid: pskill,
		});
	},

	onDestroy() {
		clearInterval(this.battleTimer);
	},

	onEnable() {
		this.onBattle();

		this.round = 1;
		this.hideBtlTimer();

		this.finishState();
		let SLDHMgr = require('../Activity/ShuiLuDaHui/sldh_mgr');
		SLDHMgr.getInstance().hideShuiLuIcon()

		for (const node of this.petSkillNodes) {
			node.destroy();
		}
		this.petSkillNodes = [];
	},

	setMp(mp, maxmp = null) {
		if (maxmp == null) {
			maxmp = cc.ll.player.gameData.attr1[GameDefine.AttrTypeL1.MAXMP];
		}
		this.mpLabel.string = '当前法力：' + mp + '/' + maxmp;
	},

	setPetId(petid) {
		this.petId = petid;
	},

	onBattle() {
		let stage = cc.find('Canvas/BattleLayer/BattleStage');
		if (stage) {
			this.stageLogic = stage.getComponent('BattleStage');
		}

		if (this.isAuto) {
			this.setAutoTimer(3);
		}
	},

	normalState() {
		this.isAuto = false;
		if (this.canOperation == false) {
			return;
		}
		this.backSkillBtn.active = false;
		this.petBtns.active = false;
		this.AutoBtn[0].active = true;
		this.AutoBtn[1].active = false;

		this.MainUILogic && (this.MainUILogic.btmOper.active = false);
		this.MainUILogic && (this.MainUILogic.btmOper.x = 0);
		this.skillPanel.active = false;

		this.autoLayer.active = false;
		this.scheduleOnce(() => {
			this.runStartAction();
		}, 0);
	},

	autoState() {
		this.isAuto = true;
		this.backSkillBtn.active = false;
		this.AutoBtn[1].active = true;
		this.AutoBtn[0].active = false;

		this.btmBtl.active = false;
		this.upBtl.active = false;
		this.MainUILogic && (this.MainUILogic.btmOper.active = true);
		this.MainUILogic && (this.MainUILogic.btmOper.x = -100);
		this.skillPanel.active = false;
		this.petBtns.active = false;
		this.autoLayer.active = true;
	},

	petState() {
		this.isAuto = false;
		if (this.canOperation == false) {
			return;
		}
		this.backSkillBtn.active = false;
		this.petBtns.active = true;
	},

	setAutoTimer(t = 3) {
		this.isAuto = true;
		let self = this;
		this.autoTimer = setTimeout(() => {
			let acttype = this.lastAction.acttype;
			let actid = this.lastAction.actid;
			cc.ll.net.send('c2s_btl_act', {
				action: acttype,
				actionid: actid,
				targetid: 0,
				onlyid: cc.ll.player.onlyid,
			});

			acttype = this.petLastAction.acttype;
			actid = this.petLastAction.actid;
			cc.ll.net.send('c2s_btl_act', {
				action: acttype,
				actionid: actid,
				targetid: 0,
				onlyid: self.petId,
			});
		}, t * 1000);
	},

	finishState() {
		this.btnLayer.active = true;

		this.backSkillBtn.active = false;
		this.AutoBtn[0].active = !this.isAuto;
		this.AutoBtn[1].active = this.isAuto;
		this.petBtns.active = false;

		this.btmBtl.active = false;
		this.upBtl.active = false;
		this.MainUILogic && (this.MainUILogic.btmOper.active = true);
		this.MainUILogic && (this.MainUILogic.btmOper.x = -100);
		this.skillPanel.active = false;

		this.autoLayer.active = this.isAuto;
		this.canOperation = false;

		this.hideSkillPanel();
		this.hidePetSkillPanel();
		this.closeAutoSkillPanel();
		this.closePetAutoSkillPanel();
	},

	startRound() {
		if (this.node.active == false) {
			return;
		}
		this.reSetBtlTimer();
		this.catchBtn.active = this.stageLogic.hasBB();
		this.canOperation = true;

		this.btnLayer.active = true;
		if (this.isAuto) {
			this.autoState();
			this.setAutoTimer();
		} else {
			// let rolelogic = this.stageLogic.getRoleLogicByOnlyId(cc.ll.player.onlyid);
			// if (rolelogic == null || rolelogic.isdead) {
			// 	let petlogic = this.stageLogic.getRoleLogicByOnlyId(this.petId);
			// 	if (petlogic == null || petlogic.isdead) {
			// 		this.finishState();
			// 		return;
			// 	}
			// 	this.petOperation();
			// 	return;
			// }
			this.normalState();
		}
	},

	finishOper() {
		this.finishState();
	},

	hideBtlTimer() {
		this.btlTimerLable.node.active = false;
	},

	reSetBtlTimer() {
		this.btlTimerNum = 30;
		this.btlTimerLable.node.active = true;
		this.btlTimerLable.string = this.btlTimerNum;
		let self = this;
		if (this.battleTimer == 0) {
			this.battleTimer = setInterval(() => {
				this.btlTimerNum--;
				if (this.btlTimerNum <= 0) {
					this.btlTimerNum = 0;
				}
				self.btlTimerLable.string = this.btlTimerNum;
			}, 1000);
		}
	},

	setRound(round) {
		this.roundLable.string = round;
	},

	openPetPanel() {
		this.petPanel.active = true;
		this.petPanel.runAction(
			cc.moveTo(0.2, 419, -80)
		);
		this.btnLayer.active = false;
	},

	closePetPanel() {
		this.petPanel.x = 1064;
		this.petPanel.active = false;
		this.btnLayer.active = true;
	},

	openItemPanel(e, d) {
		this.backType = 6;
		this.itemPanel.active = true;
		this.itemPanel.x = 1064;
		this.itemPanel.y = -80;
		let logic = this.itemPanel.getComponent('BattleUIItemPanel');
		let operid = cc.ll.player.onlyid
		if (d == 'pet') {
			operid = this.petId;
		}
		logic.updateItemList(operid);

		this.itemPanel.runAction(
			cc.moveTo(0.2, 419, -80)
		);
		this.btnLayer.active = false;
	},

	closeItemPanel() {
		this.itemPanel.x = 1064;
		this.itemPanel.active = false;
		this.btnLayer.active = true;
	},

	closeSkillPanel() {
		this.skillPanel.x = 1064;
		this.skillPanel.active = false;
		this.btnLayer.active = true;
	},

	openSkillPanel() {
		this.skillPanel.active = true;
		this.skillPanel.runAction(
			cc.moveTo(0.2, 419, -80)
		);
		this.btnLayer.active = false;
	},

	hideSkillPanel() {
		this.skillPanel.active = false;
	},

	showSkillPanel() {
		this.skillPanel.active = true;
		this.skillPanel.x = 364;
	},

	skillBtnClicked(e, d) {
		if (this.skillPanel.active) {
			this.closeSkillPanel();
		} else {
			this.openSkillPanel();
		}
	},

	initSkillPanel() {
		let roleindex = (cc.ll.player.race - 1) * 2 + cc.ll.player.sex - 1;
		let curids = SkillConfig.roleSkillIds[roleindex];
		let curtypes = SkillConfig.roleSkillTypes[roleindex];
		for (let index = 0; index < curtypes.length; index++) {
			this.typeNodes[index].spriteFrame = this.typeTxtAtlas.getSpriteFrame(`skill_type_${curtypes[index]}`);
		}

		for (let index = 0; index < curids.length; index++) {
			let skillid = curids[index];
			let skillnode = this.skillNodes[index];
			skillnode.skillid = skillid;

			let autonode = this.autoSkillNodes[index];
			autonode.skillid = skillid;

			let skillinfo = SkillMgr.GetSkillInfo(skillid);
			if (skillinfo) {
				let skillframenode = cc.find('icon', skillnode);
				let spr = skillframenode.getComponent(cc.Sprite);
				spr.spriteFrame = this.skillAtlas.getSpriteFrame(skillinfo.strIcon + '');

				let skillframe = cc.find('icon', autonode);
				let autospr = skillframe.getComponent(cc.Sprite);
				autospr.spriteFrame = this.skillAtlas.getSpriteFrame(skillinfo.strIcon + '');
			}
		}

		let autonode = this.autoSkillNodes[6];
		autonode.skillid = SkillConfig.SkillIds.NormalAtkSkill;
		autonode = this.autoSkillNodes[7];
		autonode.skillid = SkillConfig.SkillIds.NormalDefSkill;
		let attr = cc.ll.player.gameData.attr1;
		this.setMp(attr[GameDefine.AttrTypeL1.MP], attr[GameDefine.AttrTypeL1.MAXMP]);
	},

	/**
	 * 初始化宠物面板
	 * @param {*} pets 
	 * @returns 
	 */
	initPetPanel(pets) {
		if (typeof (pets) != 'object') {
			return;
		}
		let n = 0;
		let self = this;
		for (const pet of pets) {
			if (pet.type != GameDefine.LivingType.Pet) {
				continue;
			}
			if (pet.ownonlyid != cc.ll.player.onlyid) {
				continue;
			}

			let petitem = cc.instantiate(this.petItem);
			let logic = petitem.getComponent('BattleUIPetItem');
			logic.setName(pet.name);
			logic.setLevel(pet.relive, pet.level);
			logic.setHeadIcon(pet.resid);
			logic.isFight = pet.isfight == 1;
			logic.isDead = false;
			logic.selectCallback = () => {
				self.closePetPanel();
				cc.ll.net.send('c2s_btl_act', {
					action: GameDefine.ActType.Summon,
					actionid: pet.onlyid,
					targetid: 0,
					onlyid: cc.ll.player.onlyid,
				});
			}
			petitem.onlyid = pet.onlyid;
			petitem.active = true;
			petitem.parent = this.petItem.parent;
			petitem.y = this.petItem.y - (this.petItem.height + 10) * n;
			n++;

			this.petItems.push(petitem);
		}
		this.petItem.parent.height = (n + 1) * (this.petItem.height + 10)
	},

	uiPetDead(onlyid) {
		for (const petitem of this.petItems) {
			if (petitem.onlyid == onlyid) {
				let logic = petitem.getComponent('BattleUIPetItem');
				logic.isDead = true;
			}
		}
	},

	setPetSkill(skilllist) {
		for (const petskillnode of this.petSkillNodes) {
			petskillnode.destroy();
		}

		for (const petskillnode of this.petAutoSkillNodes) {
			petskillnode.destroy();
		}

		let SkillMgr = require('../Skill');
		let autonode = this.autoPetSkillNodes[0];
		autonode.skillid = SkillConfig.SkillIds.NormalAtkSkill;
		autonode = this.autoPetSkillNodes[1];
		autonode.skillid = SkillConfig.SkillIds.NormalDefSkill;

		let n = 0;
		for (const skillid of skilllist) {
			let skill = SkillMgr.GetSkillInfo(skillid);
			if (skill == null) {
				continue;
			}
			if (skill.action_type == SkillConfig.BtlActionType.Passive) {
				continue;
			}
			let autoskillnode = cc.instantiate(autonode);
			autoskillnode.skillid = skillid;
			let skillframe = cc.find('skillicon', autoskillnode);
			let autospr = skillframe.getComponent(cc.Sprite);
			autospr.spriteFrame = this.skillAtlas.getSpriteFrame(skill.strIcon);
			autoskillnode.parent = autonode.parent;
			autoskillnode.x = 80 + (Math.floor(n / 2) + 1) * 100;
			autoskillnode.y = n % 2 == 0 ? -57 : 57;
			this.petAutoSkillNodes.push(autoskillnode);

			let psnode = cc.instantiate(this.petSkillNode);
			psnode.active = true;
			psnode.parent = this.petSkillNode.parent;
			psnode.skillid = skillid;
			psnode.y = -50 - (115 * n);
			let pnamelabel = cc.find('RoleSkillItem1/nameLab', psnode);
			let pnamel = pnamelabel.getComponent(cc.Label);
			pnamel.string = skill.getName();
			let sprnode = cc.find('RoleSkillItem1/icon', psnode);
			let spr = sprnode.getComponent(cc.Sprite);
			spr.spriteFrame = this.skillAtlas.getSpriteFrame(skill.strIcon + '');

			this.petSkillNodes.push(psnode);
			n++;
		}
		this.autoPetSkillPanel.width = Math.ceil(n / 2) * 100 + 160;
		this.petSkillNode.parent.height = 115 * n + 50;
	},

	onAutoBtnClick(e, d) {
		this.setAutoTimer(3);
		this.autoState();
		cc.ll.local.set('battle_isauto', 1);
	},

	onUnAutoBtnClick(e, d) {
		clearTimeout(this.autoTimer);
		this.normalState();
		this.AutoBtn[1].active = false;
		this.AutoBtn[0].active = true;
		this.autoLayer.active = false;

		cc.ll.local.set('battle_isauto', 0);
	},

	setLastSkill(skillid) {
		this.lastSkill = skillid;
		this.setPlayerAutoSkill(skillid);
	},

	setPetLastSkill(skillid) {
		this.petLastSkill = skillid;
		this.petLastAction.acttype = GameDefine.ActType.Skill;
		this.petLastAction.actid = skillid;
		this.setPetAutoSkill(skillid);
	},

	setLastAction(act) {
		this.lastAction = act;
		if (act.acttype == GameDefine.ActType.Skill) {
			this.setLastSkill(act.actid);
		}
	},

	setPetLastAction(act) {
		this.petLastAction = act;
		if (act.acttype == GameDefine.ActType.Skill) {
			this.setPetLastSkill(act.actid);
		}
	},

	onSelectSkill(e, d) {
		let node = e.target;
		this.setLastAction({
			acttype: GameDefine.ActType.Skill,
			actid: node.skillid,
		});
		this.stageLogic.selectSkill(cc.ll.player.onlyid, node.skillid);

		this.hideSkillPanel();
		this.backType = 1;
		this.backSkillBtn.active = true;
	},

	onPetSelectSkill(e, d) {
		let node = e.target;
		this.setPetLastAction({
			acttype: GameDefine.ActType.Skill,
			actid: node.skillid,
		});
		this.stageLogic.selectSkill(this.petId, node.skillid);

		this.hidePetSkillPanel();
		this.backType = 4;
		this.backSkillBtn.active = true;
	},

	onBackSelectSkill(e, d) {
		this.stageLogic.cleanAllRoleSelectFlag();
		if (this.backType == 1) {
			this.showSkillPanel();
		}
		if (this.backType == 2) {
			this.btnLayer.active = true;
		}
		if (this.backType == 3) {
			this.btnLayer.active = true;
		}
		if (this.backType == 4) {
			this.showPetSkillPanel();
		}
		if (this.backType == 5) {
			this.petOperation();
		}
		if (this.backType == 6) {
			this.openItemPanel();
		}
		this.backSkillBtn.active = false;
	},

	onButtonClick(e, d) {
		let node = e.target;
		if (d == 'atk') {
			this.setLastAction({
				acttype: GameDefine.ActType.Skill,
				actid: node.skillid,
			});
			this.stageLogic.selectSkill(cc.ll.player.onlyid, SkillConfig.SkillIds.NormalAtkSkill);
			this.backType = 2;
			this.btnLayer.active = false;
			this.backSkillBtn.active = true;
		} else if (d == 'last') {
			this.stageLogic.selectSkill(cc.ll.player.onlyid, this.lastSkill);
			this.backType = 2;
			this.btnLayer.active = false;
			this.backSkillBtn.active = true;
		} else if (d == 'run') {
			// 逃跑
			cc.ll.net.send('c2s_btl_act', {
				action: GameDefine.ActType.RunAway,
				actionid: 0,
				targetid: 0,
				onlyid: cc.ll.player.onlyid,
			});
			// this.stageLogic.showLianji(11);
		} else if (d == 'def') {
			// 防御
			this.setLastAction({
				acttype: GameDefine.ActType.Skill,
				actid: SkillConfig.SkillIds.NormalDefSkill,
			});
			cc.ll.net.send('c2s_btl_act', {
				action: GameDefine.ActType.Skill,
				actionid: SkillConfig.SkillIds.NormalDefSkill,
				targetid: 0,
				onlyid: cc.ll.player.onlyid,
			});
		} else if (d == 'catch') {
			// 捕捉
			// let camp = this.stageLogic.getObjCamp(cc.ll.player.onlyid);
			this.stageLogic.selectTarget(2, (targetid) => {
				cc.ll.net.send('c2s_btl_act', {
					action: GameDefine.ActType.Catch,
					actionid: 0,
					targetid: targetid,
					onlyid: cc.ll.player.onlyid,
				});
			});
			this.backType = 2;
			this.btnLayer.active = false;
			this.backSkillBtn.active = true;
		} else if (d == 'sum') {
			// 召唤
			this.openPetPanel();

		} else if (d == 'sumback') {
			// 召唤
			cc.ll.net.send('c2s_btl_act', {
				action: GameDefine.ActType.SummonBack,
				actionid: 0,
				targetid: 0,
				onlyid: cc.ll.player.onlyid,
			});
		} else if (d == 'protect') {
			// 保护
			let camp = this.stageLogic.getObjCamp(cc.ll.player.onlyid);
			this.stageLogic.selectTarget(camp, (targetid) => {
				cc.ll.net.send('c2s_btl_act', {
					action: GameDefine.ActType.Protect,
					actionid: 0,
					targetid: targetid,
					onlyid: cc.ll.player.onlyid,
				});
			});
			this.backType = 2;
			this.btnLayer.active = false;
			this.backSkillBtn.active = true;
			// cc.ll.msgbox.addMsg(StrCommon.NotDeveloped);
		} else if (d == 'autoskill') {
			// 自动战斗技能
			this.showAutoSkillPanel();
		} else if (d == 'autopetskill') {
			// 自动战斗技能
			this.showPetAutoSkillPanel();
		}
	},

	runStartAction() {
		this.upBtl.active = true;
		this.btmBtl.active = true;
		this.upBtl.stopAllActions();
		this.btmBtl.stopAllActions();
		this.petBtns.active = false;

		let uppos = {
			x: -52,
			y: 113
		};
		let btmpos = {
			x: -105,
			y: 60
		};
		this.upBtl.y = -500;
		this.btmBtl.x = 700;
		this.upBtl.runAction(cc.moveTo(0.3, uppos).easing(cc.easeBackOut()));
		this.btmBtl.runAction(cc.moveTo(0.3, btmpos).easing(cc.easeBackOut()));
	},

	setPlayerAutoSkill(skillid) {
		this.lastSkill = skillid;
		this.lastAction.acttype = GameDefine.ActType.Skill;
		this.lastAction.actid = skillid;

		let skillinfo = SkillMgr.GetSkillInfo(skillid);
		if (skillinfo) {
			this.autoIcons[0].spriteFrame = this.skillAtlas.getSpriteFrame(skillinfo.strIcon + '');
		}
		cc.ll.local.set('battle_r_skill', skillid);
	},

	setPetAutoSkill(skillid) {
		this.petLastSkill = skillid;
		this.petLastAction.acttype = GameDefine.ActType.Skill;
		this.petLastAction.actid = skillid;

		let skillinfo = SkillMgr.GetSkillInfo(skillid);
		if (skillinfo) {
			this.autoIcons[1].spriteFrame = this.skillAtlas.getSpriteFrame(skillinfo.strIcon + '');
		}

		cc.ll.local.set('battle_p_skill', skillid);
	},

	ontestAtk(e, d) {
		if (d == 1) {
			// this.stageLogic.playEffectOnStage(1007, false);
			// this.stageLogic.onTestAttack2();
		} else {
			// this.stageLogic.playEffectOnStage(1007, true);
			// this.stageLogic.onTestAttack();
		}
		this.stageLogic.testObjEffect()
	},

	onSelectAutoSkill(e, d) {
		let node = e.target;
		this.setPlayerAutoSkill(node.skillid);
		this.closeAutoSkillPanel();
	},

	showAutoSkillPanel() {
		if (this.autoSkillPanel.x <= 600) {
			this.closeAutoSkillPanel();
		} else {
			this.autoSkillPanel.x = 1052;
			this.closePetAutoSkillPanel();
			this.autoSkillPanel.runAction(cc.moveTo(0.2, {
				x: 256,
				y: -128
			}));
		}
	},

	closeAutoSkillPanel() {
		this.autoSkillPanel.x = 1052;
	},

	onSelectPetAutoSkill(e, d) {
		let node = e.target;
		this.setPetAutoSkill(node.skillid);
		this.closePetAutoSkillPanel();
	},

	showPetAutoSkillPanel() {
		if (this.autoPetSkillPanel.x <= 600) {
			this.closePetAutoSkillPanel();
		} else {
			this.autoPetSkillPanel.x = 1000;
			this.closeAutoSkillPanel();
			let w = this.autoPetSkillPanel.width;
			let x = 360 - (w - 160);
			this.autoPetSkillPanel.runAction(cc.moveTo(0.2, {
				x: x,
				y: -128
			}));
		}
	},

	closePetAutoSkillPanel() {
		this.autoPetSkillPanel.x = 1000;
	},

	petOperation() {
		if (this.petId == 0) {
			this.finishOper();
			return;
		}
		this.backType = 3;
		this.btmBtl.active = false;
		this.upBtl.active = false;

		this.MainUILogic && (this.MainUILogic.btmOper.active = false);
		this.MainUILogic && (this.MainUILogic.btmOper.x = 0);

		this.backSkillBtn.active = false;
		this.petBtns.active = true;
	},

	closePetSkillPanel() {
		this.petSkillPanel.x = 1164;
		this.petSkillPanel.active = false;
		this.petBtns.active = true;
	},

	openPetSkillPanel() {
		this.petSkillPanel.active = true;
		this.petSkillPanel.runAction(
			cc.moveTo(0.2, 420, -80)
		);
		this.petBtns.active = false;
	},

	hidePetSkillPanel() {
		this.petSkillPanel.active = false;
	},

	showPetSkillPanel() {
		this.petSkillPanel.active = true;
		this.petSkillPanel.x = 420;
	},

	onPetBtnClick(e, d) {
		if (d == 'atk') {
			this.setPetLastAction({
				acttype: GameDefine.ActType.Skill,
				actid: SkillConfig.SkillIds.NormalAtkSkill,
			});
			this.stageLogic.selectSkill(this.petId, SkillConfig.SkillIds.NormalAtkSkill);
		} else if (d == 'def') {
			this.setPetLastAction({
				acttype: GameDefine.ActType.Skill,
				actid: SkillConfig.SkillIds.NormalDefSkill,
			});
			cc.ll.net.send('c2s_btl_act', {
				action: GameDefine.ActType.Skill,
				actionid: SkillConfig.SkillIds.NormalDefSkill,
				targetid: 0,
				onlyid: this.petId,
			});
		} else if (d == 'showskill') {
			this.openPetSkillPanel();
		} else if (d == 'closeskill') {
			this.closePetSkillPanel();
		} else if (d == 'protect') {
			// cc.ll.msgbox.addMsg(StrCommon.NotDeveloped);
			// 保护
			let camp = this.stageLogic.getObjCamp(this.petId);
			this.stageLogic.selectTarget(camp, (targetid) => {
				cc.ll.net.send('c2s_btl_act', {
					action: GameDefine.ActType.Protect,
					actionid: 0,
					targetid: targetid,
					onlyid: this.petId,
				});
			});
			this.backType = 5;
			this.petBtns.active = false;
			this.backSkillBtn.active = true;
		}
	},
})