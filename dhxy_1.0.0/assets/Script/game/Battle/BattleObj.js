let GameRes = require('../GameRes');
var SkillMgr = require('../Skill');
var SkillConfig = require('../../etc/skill_config');
let CPubFunction = require('../PubFunction');
let GameDefine = require('../GameDefine');
let pNpcMgr = require('../NpcMgr');

/**
 * 战斗场景中战斗对象逻辑脚本
 */
cc.Class({
	extends: cc.Component,

	properties: {
		animationNode: cc.Node,
		animationMaskNode: cc.Node,
		// animation: cc.Animation,
		hpPorgress: cc.ProgressBar,
		mpPorgress: cc.ProgressBar,

		roleSpr: cc.Sprite,
		nameLabel: cc.Label,
		roleName: {
			get() {
				return this.nameLabel == null ? '' : this.nameLabel.string;
			},
			set(n) {
				this.nameLabel.string = n;
			}
		},

		_self: false,
		isSelf: {
			get() {
				return this._self;
			},
			set(n) {
				this._self = n;
				this.mpPorgress.node.active = n;
			}
		},

		_dir: 'up',
		dir: {
			set(n) {
				this._dir = n;
			}
		},

		effectLayer: cc.Node, //身前 buff
		effectBg: cc.Node, //身后buff
		actionEffect: cc.Node,
		effect: cc.Prefab,

		bufflayer: cc.Node,
		buffprefab: cc.Prefab,

		skillTitle: cc.Node,
		skillLabel: cc.Label,

		numPrefab: cc.Prefab,

		selectFlag: cc.Node,
		waitFlag: cc.Node,
		weaponNode: cc.Node,
		talkNode: cc.Node,
		iswait: {
			get() {
				return this.waitFlag ? this.waitFlag.active : false;
			},
			set(n) {
				this.waitFlag && (this.waitFlag.active = n);
			}
		}
	},

	ctor() {
		this.onlyid = 0;
		this.resid = 0;
		this.hp = 0;
		this.mp = 0;
		this.maxhp = 0;
		this.maxmp = 0;

		this.isdead = 0;

		this.objType = GameDefine.LivingType.Unknow; //
		this._dir = 'up'; // 0 self  1enemy

		this.bufflist = {};
		this.buffEffectList = {};

		this.onSkillTarget = false;

		// BattleState脚本
		this.stageLogic = null;

		this.selectFunc = 0;

		// 战斗对象资源路径
		this.resUrl = '';
		this.effectResList = [];
		this.petColor = -1;
	},


	start() {
		if (this.resid == 0) {
			this.objType = 1;
			this.resid = 1001;

			this.resAtlas = GameRes.getRoleRes(this.resid);
			this.initRoleAnimation();
		}

		//获取战斗对象所在舞台
		let stage = cc.find('Canvas/BattleLayer/BattleStage');
		this.stageLogic = stage.getComponent('BattleStage');

		let spr = this.actionEffect.getComponent(cc.Sprite);
		let ef_animation = this.actionEffect.getComponent(cc.Animation);
		ef_animation.on('finished', () => {
			// this.actionEffect.active = false;
			spr.spriteFrame = null;
		}, this);

		let anchorinfo = cc.ll.propData.resanchor[this.resid];
		if (anchorinfo) {
			if (anchorinfo.anchorX) {
				this.animationNode.anchorX = anchorinfo.anchorX;
				this.animationMaskNode.anchorX = this.animationNode.anchorX;
			}
			if (anchorinfo.anchorY) {
				this.animationNode.anchorY = anchorinfo.anchorY;
				this.animationMaskNode.anchorY = this.animationNode.anchorY;
			}
		}
	},

	clear() {
		this.animationNode.getComponent(cc.Animation).destroy();
		this.animationMaskNode.getComponent(cc.Animation).destroy();
	},

	onDestroy() {
		// let deps = cc.loader.getDependsRecursively('Prefabs/Battle/BattleRole');
		// cc.loader.release(deps);

		// for (const resstr of this.effectResList) {
		// 	cc.loader.releaseRes(resstr);
		// }
		if (GameDefine.roleName[this.resid] == null && pNpcMgr.CheckNpcRes(0, this.resid) == false) {
			// cc.loader.releaseResDir(this.resUrl, cc.Texture2D);
			// cc.loader.releaseResDir(this.resUrl, cc.SpriteFrame);
			// cc.loader.releaseResDir(this.resUrl, cc.SpriteAtlas);
		}
	},

	clean() {
		this.node.destroy();
	},

	isPlayer() {
		return this.objType == GameDefine.LivingType.Player;
	},

	isPet() {
		return this.objType == GameDefine.LivingType.Pet;
	},

	isPartner() {
		return this.objType == GameDefine.LivingType.Parent;
	},

	isMonster() {
		return this.objType == GameDefine.LivingType.Monster;
	},

	sethp(hp) {
		this.hpPorgress.progress = hp / this.maxhp;
	},

	setmp(mp) {
		this.mpPorgress.progress = mp / this.maxmp;
	},

	hideHp() {
		this.hpPorgress.node.active = false;
	},

	hideMp() {
		this.mpPorgress.node.active = false;
	},

	/**
	 * 设置战斗对象信息
	 * @param {*} objinfo 
	 */
	setInfo(objinfo) {
		this.onlyid = objinfo.onlyid;
		this.resid = objinfo.resid;
		this.roleName = objinfo.name;
		this.objType = objinfo.type;
		this.maxhp = objinfo.maxhp;
		this.hp = objinfo.hp;
		this.maxmp = objinfo.maxmp;
		this.mp = objinfo.mp;

		this.sethp(objinfo.hp);
		this.setmp(objinfo.mp);

		this.ownid = objinfo.ownid;
		this.relive = objinfo.relive;
		this.weapon = objinfo.weapon;
		this.skill_list = objinfo.skilllist;

		this.resUrl = `shap/${objinfo.resid}`;//设置外形资源路径

		this.petColor = objinfo.petColor;

		if (this.isPartner()) {
			this.hideMp();//隐藏魔法
		}

		if (this.isMonster() || objinfo.camp == 2) {//怪物或者敌方阵营,隐藏Hp(血量)和Mp(魔法)进度条
			this.hideHp();
			this.hideMp();
			this.nameLabel.node.color = cc.Color.WHITE;//设置名字颜色为白色
		} else if (this.isPet()) {
			this.nameLabel.node.color = CPubFunction.getReliveColor(objinfo.relive, 1);
		} else if (this.isPlayer() || this.isPartner()) {
			this.nameLabel.node.color = CPubFunction.getReliveColor(objinfo.relive, 0);
		}
		// if(this.isPlayer()){
		// 	this.nameLabel.node.color = CPubFunction.getReliveColor(objinfo.relive);
		// }

		if (this.hp <= 0) {
			this.play('die');
		}
	},

	/*
	 * 战斗角色染色
	 */
	setColor(color1, color2) {
		let maskframe = this.animationMaskNode;
		if (!maskframe) { return; }
		if (!color1 && !color2) {
			maskframe.active = false;
			return;
		}
		maskframe.active = true;
		let logic = maskframe.getComponent('role_color');
		if (color1 != 0) {
			let r = Math.floor(color1 / 256 / 256);
			let g = Math.floor(color1 / 256 % 256);
			let b = Math.floor(color1 % 256);
			logic.dH1 = Math.floor(r) * 1000000 + Math.floor(g) * 1000 + Math.floor(b);
		}

		if (color2 != 0) {
			let r = Math.floor(color2 / 256 / 256);
			let g = Math.floor(color2 / 256 % 256);
			let b = Math.floor(color2 % 256);
			logic.dH2 = Math.floor(r) * 1000000 + Math.floor(g) * 1000 + Math.floor(b);
		}
	},

	/**
	 * 播放动画
	 * @param {*} action 	动作
	 * @param {*} spdrate 
	 * @returns 
	 */
	play(action = 'stand', spdrate = 1) {
		if (this.isdead && action != 'die') {
			return;
		}
		let callfun = (curNode, finishfunc = true) => {
			if (action == 'hit') {
				let t = this._dir == 'up' ? -1 : 1;
				let pos = { x: -t * 15, y: t * 8 };
				let back = { x: t * 15, y: -t * 8 };
				this.animationNode.runAction(cc.sequence(
					cc.moveBy(0.4, pos).easing(cc.easeOut(3.0)),
					cc.moveBy(0.1, back)
				));
			}

			if (action != 'stand' && action != 'die' && finishfunc) {
				curNode.getComponent(cc.Animation).on('finished', () => {
					this.play();
				}, this);
			}
			this.scheduleOnce(() => {
				curNode.scaleX = 450 / curNode.width;
				curNode.scaleY = 450 / curNode.height;
			}, 0);
		};

		let playfunmask = (curNode, curName) => {
			// if (curName.indexOf('_ef') != -1) { return; }
			if (!curNode.active) { return; }
			let clips = curNode.getComponent(cc.Animation).getClips();
			for (const clip of clips) {
				if (clip.name == curName) {
					curNode.getComponent(cc.Animation).play(curName);
					return;
				}
			}

			cc.loader.loadRes(`shape_mask/${this.resid}/${curName}`, cc.SpriteAtlas, function (err, atlas) {
				if (err) { return; }
				if (!curNode) { return; }
				if (Array.isArray(atlas)) { return; }
				let curFrames = atlas.getSpriteFrames();

				let fn = 12;
				if (action == 'hit') { fn = 2; }
				let curClip = cc.AnimationClip.createWithSpriteFrames(curFrames, fn);
				curClip.name = curName;
				if (action == 'stand' || action == 'run') {
					curClip.wrapMode = cc.WrapMode.Loop;
				}
				else {
					curClip.wrapMode = cc.WrapMode.Normal;
				}
				curClip.speed = spdrate;

				let nodeAni = curNode.getComponent(cc.Animation);
				nodeAni.addClip(curClip);
				nodeAni.play(curName);
			});
		};

		let playfun = (curNode, curName, finishfunc = true) => {
			let clips = curNode.getComponent(cc.Animation).getClips();
			for (const clip of clips) {
				if (clip.name == curName) {
					curNode.getComponent(cc.Animation).play(curName);
					callfun(curNode, finishfunc);
					return;
				}
			}
			// curNode.active = false;
			let that = this;
			cc.loader.loadRes(`shap/${this.resid}/${curName}`, cc.SpriteAtlas, function (err, atlas) {
				if (err) {
					console.log(err);
					return;
				}
				if (!curNode) return;
				if (Array.isArray(atlas)) {
					return;
				}
				let curFrames = atlas.getSpriteFrames();

				let fn = 12;
				if (action == 'hit') {
					fn = 2;
				}
				let curClip = cc.AnimationClip.createWithSpriteFrames(curFrames, fn);
				curClip.name = curName;

				if (action == 'stand' || action == 'run') {
					curClip.wrapMode = cc.WrapMode.Loop;
				} else {
					curClip.wrapMode = cc.WrapMode.Normal;
				}

				curClip.speed = spdrate;

				let nodeAni = curNode.getComponent(cc.Animation);
				nodeAni.addClip(curClip);
				nodeAni.play(curName);

				if (that.petColor != null && that.petColor != 0 && that.petColor != -1) {
					if (curNode.getComponent('pet_color')) {
						curNode.getComponent('pet_color').dH = that.petColor;
					}
				}
				// curNode.active = true;

				callfun(curNode, finishfunc);
			});

			/* let masknode = curNode.getChildByName('maskframe');
			if (masknode && masknode.active && curName.indexOf('_ef') == -1) {
				playfunmask(masknode, curName);
			} */
		};


		let actName = action + (this._dir == 'up' ? '_3' : '_1');
		let actNode = this.animationNode;
		playfun(actNode, actName);
		playfunmask(this.animationMaskNode, actName);
		// this.playAnimation(this.animationNode, actName);
		// let animState = this.animation.play(action + this._dir);
		CPubFunction.addWeaponAnimation(this.weapon, this.weaponNode, this.animationNode, this, this.resid, action, this._dir == 'up' ? 3 : 1);
		if (action == 'magic' || action == 'attack') {
			actName = actName + '_ef';
			actNode = this.actionEffect;
			playfun(actNode, actName, false);
			// this.playAnimation(this.actionEffect, actName);
			// this.actionEffect.getComponent(cc.Animation).play(action + this._dir);
			// this.actionEffect.getComponent(cc.Animation).play('magicup');
		}

		// this.scheduleOnce(() => {
		// 	this.animationNode.scaleX = 450 / this.animationNode.width;
		// 	this.animationNode.scaleY = 450 / this.animationNode.height;
		// }, 0);
	},

	playEffectStr(strEffect, x, y, callback) {
		CPubFunction.CreateSubFrame(this.effectLayer, {
			nX: x,
			nY: y
		}, this.effect, strEffect, callback);

		this.scheduleOnce(() => {
			this.actionEffect.scaleX = 450 / this.actionEffect.width;
			this.actionEffect.scaleY = 450 / this.actionEffect.height;
		}, 0);
	},

	playEffect(skillid, callback) {
		let stData = SkillMgr.GetSkillInfo(skillid);
		if (stData) {
			if (stData.bEffectPos == SkillConfig.EffectPos.Stage) {
				return;
			}
			if (stData.strParticleEffect == 0) {
				return;
			}

			this.effectResList.push('effect/' + stData.strParticleEffect);

			this.playEffectStr(stData.strParticleEffect, stData.nSkillEffectX,
				stData.nSkillEffectY, callback);
		}
	},

	checkBuff(bufflist) {
		for (const skillid in this.buffEffectList) {
			if (this.buffEffectList.hasOwnProperty(skillid)) {
				let numskill = parseInt(skillid);
				if (bufflist.indexOf(numskill) == -1) {
					this.delBuff(numskill);
				}
			}
		}

		for (const skillid of bufflist) {
			if (this.bufflist[skillid] == null) {
				this.addBuff(skillid);
			}
		}
	},

	addBuff(skillid) {
		if (this.buffEffectList[skillid] != null) {
			return;
		}

		if (this._dir == 'up') {
			let buff = cc.instantiate(this.buffprefab);
			let logic = buff.getComponent('SkillIcon');
			logic.width = 28;
			logic.height = 28;
			logic.icon = skillid;

			buff.skillid = skillid;

			let index = Object.keys(this.bufflist).length;
			let x = -30 + (2 + 30) * index;
			let y = 190;
			buff.x = x;
			buff.y = y;
			this.bufflist[skillid] = buff;
			buff.parent = this.bufflayer;
		}

		let stData = SkillMgr.GetSkillInfo(skillid);
		if (stData.strBuffEffect != null) {
			// 先排除隐身
			if (stData.strBuffEffect == 'yinshen') {
				this.animationNode.opacity = 120;
				this.buffEffectList[skillid] = 0;
			} else {
				if (stData.nBuffFront) {
					let effect = CPubFunction.CreateSubFrameLoop(this.effectLayer, {
						nX: 0,
						nY: stData.nBuffEffectY
					}, this.effect, stData.strBuffEffect);
					this.buffEffectList[skillid] = effect;
				} else {
					let effect = CPubFunction.CreateSubFrameLoop(this.effectBg, {
						nX: 0,
						nY: stData.nBuffEffectY
					}, this.effect, stData.strBuffEffect);
					this.buffEffectList[skillid] = effect;
				}

			}
		}
	},

	delBuff(skillid) {
		let buff = this.bufflist[skillid];
		if (buff) buff.destroy();
		delete this.bufflist[skillid];

		let effect = this.buffEffectList[skillid];
		if (effect) effect.destroy();
		delete this.buffEffectList[skillid];

		if (skillid == SkillConfig.SkillIds.YinShen || skillid == SkillConfig.SkillIds.ZiXuWuYou) {
			this.animationNode.opacity = 255;
		}
	},

	useSkill(skillid) {
		if (skillid == SkillConfig.SkillIds.NormalAtkSkill ||
			skillid == SkillConfig.SkillIds.NormalDefSkill) {
			return;
		}
		this.play('magic');
		cc.ll.AudioMgr.playMagicAudio(this.resid);
		//zfy--技能音效
		cc.ll.AudioMgr.playSkillAudio(skillid);
		this.showSkillTips(skillid);
	},

	showSkillTips(skillid) {
		let stData = SkillMgr.GetSkillInfo(skillid);
		let skillname = stData.getName();
		this.skillLabel.string = skillname;
		this.skillTitle.active = true;

		let self = this;
		this.skillTitle.runAction(cc.sequence(
			cc.delayTime(1.5),
			cc.callFunc(() => {
				self.skillTitle.active = false;
			})
		));
	},

	showSelectFlag(selFunc) {
		this.selectFlag.stopAllActions();
		let act = cc.repeatForever(cc.sequence(cc.moveTo(0.3, 0, 200), cc.moveTo(0.5, 0, 225)));
		this.selectFlag.active = true;
		this.selectFlag.runAction(act);
		this.selectFunc = selFunc;
		this.onSkillTarget = true;
	},

	hideSelectFlag() {
		this.selectFlag.stopAllActions();
		this.selectFlag.active = false;
	},

	onSelectSkillTarget() {
		if (this.onSkillTarget) {
			if (this.selectFunc) {
				this.selectFunc(this.onlyid);
			} else {

			}
			this.selectFunc = null;
		}
	},

	lianji(hps, lefthp, isdead, style = 0) {
		if (hps.length <= 0) {
			return;
		}
		this.isdead = isdead;
		let lianjicallback = () => {
			let skillid = this.bufflist[SkillConfig.SkillIds.BaiRiMian];
			if (skillid) {
				this.delBuff(skillid);
			}
			skillid = this.bufflist[SkillConfig.SkillIds.MiHunZui];
			if (skillid) {
				this.delBuff(skillid);
			}

			this.sethp(lefthp);
			if (isdead) {
				this.play('die');
			} else {
				this.play();
			}
		}

		let i = 0;
		for (const hp of hps) {
			let numlabel = cc.instantiate(this.numPrefab);
			let numlogic = numlabel.getComponent('BattleNum');
			numlogic.num = hp;
			numlogic.subhp = true;
			if (style == 1) {
				numlogic.baoji = true;
			} else if (style == 5) {
				numlogic.pofang = true;
			}
			numlabel.parent = this.node;
			numlabel.x = 0;
			numlabel.y = 100;
			let times = (hps.length - i) > 4 ? 4 : (hps.length - i);

			let actlist = [];
			actlist[actlist.length] = cc.hide();
			actlist[actlist.length] = cc.delayTime(0.15 * i);
			actlist[actlist.length] = cc.show();
			actlist[actlist.length] = cc.scaleTo(0.01, 1.5);
			actlist[actlist.length] = cc.scaleTo(0.02, 1);
			actlist[actlist.length] = cc.repeat(cc.sequence(cc.moveBy(0.1, 0, 40), cc.delayTime(0.05)), times);
			if (times < 4) {
				actlist[actlist.length] = cc.delayTime(0.15 * (4 - times));
			}
			if (i == hps.length - 1) {
				actlist[actlist.length] = cc.callFunc(lianjicallback);
			}
			actlist[actlist.length] = cc.removeSelf();

			numlabel.runAction(cc.sequence(actlist));
			// if(i < hps.length - 1){
			// 	numlabel.runAction(cc.repeat(cc.sequence(
			// 		cc.delayTime(0.1 * i),
			// 		cc.moveBy(0.1, 0, 40),
			// 	), hps.length- i));
			// }
			i++;
		}

	},


	//style 0 常规 1 暴击 2 吃药加血 3 吃药加血加蓝 4 涅槃 5 破防
	subHp(hp, lefthp, isdead, style = 0) {
		if (hp == 0) {
			return;
		}

		let left_hp = lefthp;
		let is_dead = isdead;
		this.isdead = isdead;
		if (style != 4) {
			setTimeout(() => {
				let lhp = hp + '';
				if (hp > 0) {
					lhp = '+' + hp;
				}
				let numlabel = cc.instantiate(this.numPrefab);
				let numlogic = numlabel.getComponent('BattleNum');
				numlogic.num = lhp;
				if (style == 0) {
					if (hp > 0) {
						numlogic.addhp = true;
					} else {
						numlogic.subhp = true;
					}
				} else if (style == 1) {
					numlogic.baoji = true;
				} else if (style == 2) {
					numlogic.addhp = true;
				} else if (style == 3) {
					numlogic.hpmp = true;
				} else if (style == 5) {
					numlogic.pofang = true;
				}
				numlabel.parent = this.node;
				numlabel.x = 0;
				numlabel.y = 110;

				numlabel.runAction(cc.sequence(
					cc.moveTo(0.8, 0, 130),
					cc.delayTime(0.2),
					cc.removeSelf(),
				));
			}, 0.2 * 1000);
		}

		if (hp < 0) {
			let skillid = this.bufflist[SkillConfig.SkillIds.BaiRiMian];
			if (skillid) {
				this.delBuff(skillid);
			}
			skillid = this.bufflist[SkillConfig.SkillIds.MiHunZui];
			if (skillid) {
				this.delBuff(skillid);
			}
		}

		setTimeout(() => {
			this.sethp(left_hp);
			if (is_dead) {
				this.play('die');
			} else {
				this.play();
			}
		}, 0.5 * 1000);
	},

	subMp(mp) {
		let callback = () => {
			if (mp > 0) {
				mp = '+' + mp;
			}
			let numlabel = cc.instantiate(this.numPrefab);
			let numlogic = numlabel.getComponent('BattleNum');
			numlogic.num = mp;
			numlogic.mp = true;
			numlabel.parent = this.node;
			numlabel.x = 0;
			numlabel.y = 110;

			numlabel.runAction(cc.sequence(
				cc.moveTo(0.8, 0, 130),
				cc.delayTime(0.2),
				cc.removeSelf(),
			));
		}
		this.scheduleOnce(callback, 0.1);
	},

	showDef() {
		CPubFunction.CreateSubFrame(this.effectLayer, {
			nX: 0,
			nY: 40
		}, this.effect, 'fangyu');

		this.scheduleOnce(() => {
			this.actionEffect.scaleX = 500 / this.actionEffect.width;
			this.actionEffect.scaleY = 500 / this.actionEffect.height;
		}, 0);
	},

	showShanbi() {
		if (this._dir == 'up') {
			this.animationNode.runAction(cc.sequence(
				cc.moveBy(0.2, cc.v2(50, -20)),
				cc.delayTime(0.3),
				cc.moveBy(0.2, cc.v2(-50, 20)),
			));
		} else {
			this.animationNode.runAction(cc.sequence(
				cc.moveBy(0.2, cc.v2(-50, 20)),
				cc.delayTime(0.3),
				cc.moveBy(0.2, cc.v2(50, -20)),
			));
		}
	},

	playRunAway(success = 0) {
		let olddir = this._dir;
		if (this._dir == 'down') {
			this._dir = 'up';
		}
		if (this._dir == 'up') {
			this._dir = 'down';
		}
		this.play('run');
		let go = () => {
			let x = 300,
				y = -120;
			if (olddir == 'down') {
				x = -300;
				y = 120;
			}
			this.node.runAction(cc.moveBy(0.5, cc.v2(x, y)));
		}
		let retdir = () => {
			this._dir = olddir;
			this.play();
		}
		this.node.runAction(cc.sequence(
			cc.delayTime(0.5),
			cc.callFunc(() => {
				if (success) {
					go();
				} else {
					retdir();
				}
			}),
		));
	},

	objTalk(msg) {
		this.talkNode.stopAllActions();
		this.talkNode.runAction(cc.sequence(cc.fadeIn(0), cc.delayTime(3), cc.fadeOut(1)));
		let msgNode = this.talkNode.getChildByName('msg');
		msgNode.getComponent('CustomRichText').string = msg;
		this.talkNode.height = msgNode.height + 16;
	},
});
