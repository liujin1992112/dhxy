cc.Class({
	extends: cc.Component,

	properties: {
		item:cc.Prefab,
		itemLayer: cc.Node,
	},

	ctor(){
		this.operid = 0;
		this.itemlist = {};
	},

	onLoad(){
		this.initItemPanel();
		this.parentLogic = this.node.parent.getComponent('BattleUI');
	},

	clean(){
		this.node.destroy();
	},

	onDestroy(){
		this.parentLogic.closeItemPanel();
	},

	initItemPanel(operid) {
		let itemlist = cc.ll.player.itemList;
		let propItems = cc.ll.propData.item;
		let n = 0;
		for (const itemid in itemlist) {
			if (Math.floor(itemid / 10000) == 4) {
				let iteminfo = propItems[itemid];
				if (!iteminfo) {
					continue;
				}
				let t = null;
				try {
					t = JSON.parse(iteminfo.json);
				} catch (error) {
					continue;
				}
				
				const count = itemlist[itemid];
				let item = cc.instantiate(this.item);
				item.parent = this.itemLayer;
				let itemlogic = item.getComponent('BattleUIItemPanelItem');
				itemlogic.setItemInfo({
					itemid:itemid,
					num:count,
					effect: t.hm,
				});

				item.x = -96 + (n % 3) * (item.width + 10);
				item.y = -50 - Math.floor(n / 3) * (item.height + 10);

				itemlogic.operid = operid;

				this.itemlist[itemid] = item;
				n++;
			}
		}
	},

	updateItemList(operid) {
		this.itemLayer.destroyAllChildren(true);
		this.initItemPanel(operid);
	},

	// setOperId(operid){
	// 	this.operid = operid;
	// 	for (const itemid in this.itemlist) {
	// 		const item = this.itemlist[itemid];
	// 		let itemlogic = item.getComponent('BattleUIItemPanelItem');
	// 		itemlogic.operid = operid;
	// 	}
	// },
})

