let GameDefine = require('../GameDefine');

cc.Class({
	extends: cc.Component,

	properties: {
		iconAtlas: cc.SpriteAtlas,
		NumLabel: cc.Label,
		iconSpr: cc.Sprite,
		effect:[cc.Node],
	},

	ctor(){
		this.selectCallback = null;
		this.itemid = 0;
		this.operid = 0;
	},

	setItemInfo(info) {
		let iconFrame = this.iconAtlas.getSpriteFrame(info.itemid);
		this.iconSpr.spriteFrame = iconFrame;
		this.NumLabel.string = info.num;
		this.itemid = info.itemid;

		this.effect[0].active = info.effect == 'hm';
		this.effect[1].active = info.effect == 'h';
		this.effect[2].active = info.effect == 'm';
		this.effect[3].active = info.effect == 't';
	},
	
	onItemClick(e, d){
		let stage = cc.find('Canvas/BattleLayer/BattleStage');
		if (stage) {
			let stageLogic = stage.getComponent('BattleStage');
			stageLogic.btlUIlogic.closeItemPanel();
			let itemid = this.itemid;
			stageLogic.selectTarget(1, (targetid) => {
				stageLogic.btlUIlogic.backType = 6;
				cc.ll.net.send('c2s_btl_act', {
					action: GameDefine.ActType.Item,
					actionid: itemid,
					targetid: targetid,
					onlyid: this.operid,
				});
			});
		}
	},
});