﻿let CPubFunction = require('./PubFunction');

/**
 * 任务事件类型
 */
let EEventType =
{
    PlayerTalkNpc: 1,           //对话
    PlayerKillNpc: 2,           //击杀Npc
    PlayerGatherNpc: 3,         //获取Npc
    PlayerDoAction: 4,
    PlayerArriveArea: 5,        //到达指定区域
    PlayerGiveNpcItem: 6,       //给Npc道具

    FailEventPlayerDead: 11,
    FailEventTimeOut: 12,

};

/**
 * 任务类型
 */
let ETaskKind =
{
    EStory: 1,  //故事
    EDaily: 2,  //日常
    EFuBen: 3,  //副本
};

/**
 * 任务事件状态
 */
let EState =
{
    ELock: 0,   //未解锁
    EDoing: 1,  //进行中
    EDone: 2,   //完成
    EFaild: 3,  //失败
};

class CEventBase {
    constructor() {
        this.nEventType = 0;
        this.strTip = '';
        this.vecPrize = [];
    }
}
//--------------------- 对话 ---------------//

class CEventTalkNpc extends CEventBase {
    constructor() {
        super();
        this.vecCreateNpc = [];
        this.nNpcConfigID = 0;
        this.vecSpeak = [];
        this.bAutoTrigle = 0;
    }

}


//--------------------- 采集Npc  ---------------//

class CGatherNpc extends CEventBase {
    constructor() {
        super();
        this.vecCreateNpc = [];
        this.vecNpc = [];
    }
}


//--------------------- 在指定区域做动作 ---------------//

class CDoActionInArea extends CEventBase {
    constructor() {
        super();
        this.nMap = 0;
        this.nX = 0;
        this.nY = 0;
        this.strAction;
        this.strTalk = '';
    }
}

//--------------------- 到达区域 ---------------//

class CArriveArea extends CEventBase {
    constructor() {
        super();
        this.nMap = 0;
        this.nX = 0;
        this.nY = 0;
    }
}


//--------------------- 给Npc物品 ---------------//

class CGiveNpcItem extends CEventBase {
    constructor() {
        super();
        this.nItemID = 0;
        this.nNum = 0;
        this.nFromNpc;
        this.nNpcConfigID;
    }
}

//---------------------  杀死Npc  ---------------//

class CKillDynamicNpc extends CEventBase {
    constructor() {
        super();
        this.vecCreateNpc = [];
        this.vecNpc = [];
        this.bAutoTrigle = 0;
    }
}


//---------------------  失败事件：玩家死亡  ---------------//

class CFailEventPlayerDead extends CEventBase {

    constructor() {
        super();
        this.nEventType = EEventType.FailEventPlayerDead;
        this.nDeadCnt = 1;
    }
}


//---------------------  失败事件：超时  ---------------//

class CFailEventTimeOut extends CEventBase {
    constructor() {
        super();
        this.nEventType = EEventType.FailEventTimeOut;
        this.nMaxTime = 60;
    }
}


/**********************************************************************/

class CTask {
    constructor() {
        this.nTaskID = 0;
        this.nKind = 0;
        this.nTaskGrop = 0;
        this.nDailyCnt = 0;
        this.strTaskName = '';
        this.vecLimit = [];
        this.vecEvent = [];  //事件列表
        this.vecFailEvent = [];
    }


}


/*********************************** 任务配置信息 ***********************************/

class CConfigTaskMgr {
    constructor() {
        this.mapConfigTask = {};
        //    this.InitAllTask();
    }

    GetTaskEventCreateNpc(vecString) {
        let vecData = [];

        for (let it in vecString) {
            let strData = vecString[it];

            let vecTmp = strData.split(",");
            if (vecTmp.length != 4)
                continue;

            vecData.push({ nNpc: vecTmp[0], nMap: vecTmp[1], nX: vecTmp[2], nY: vecTmp[3] });  //zzzErr
        }
        return vecData;
    }


    GetDailyMaxCnt(nGrop) {
        if (nGrop == 2 || nGrop == 3)
            return 20;

        if (nGrop == 4)
            return 15;

        if (nGrop == 5)
            return 5;

        if (nGrop == 6)
            return 120;

        if (nGrop == 7)
            return 40;
        return 0;
    }

    StringVecorToDataVector(vecString) {
        let vecData = [];

        for (let it in vecString) {
            let strData = vecString[it];

            let vecTmp = strData.split(":");
            if (vecTmp.length != 2)
                continue;

            vecData.push({ nKey: vecTmp[0], nValue: vecTmp[1] });
        }
        return vecData;
    }

    AddTask(nKind, stTask) {
        if (this.mapConfigTask.hasOwnProperty(nKind) == false)
            this.mapConfigTask[nKind] = [];

        this.mapConfigTask[nKind].push(stTask);
    }

    InitAllTask() {
        for (const itTask in cc.ll.propData.task) {
            if (itTask == 'datatype')
                continue;

            const stData = cc.ll.propData.task[itTask];

            let stTask = new CTask();

            stTask.nTaskID = parseInt(itTask);
            stTask.nKind = stData.nKind;
            stTask.strTaskName = stData.strName;
            stTask.nTaskGrop = CPubFunction.GetDefault(stData.nTaskGrop, 0);
            stTask.nDailyCnt = CPubFunction.GetDefault(stData.nDailyCnt, 0);

            for (let nIndex in stData.vecEvent) {
                if (stData.vecEvent[nIndex].nEventType == EEventType.PlayerTalkNpc)  //对话
                {
                    let stTalk = new CEventTalkNpc();
                    stTalk.nEventType = EEventType.PlayerTalkNpc;
                    stTalk.vecPrize = this.StringVecorToDataVector(stData.vecEvent[nIndex].vecPrize);
                    stTalk.strTip = stData.vecEvent[nIndex].strTip;
                    stTalk.vecCreateNpc = this.GetTaskEventCreateNpc(stData.vecEvent[nIndex].vecCreateNpc);
                    stTalk.vecNpc = stData.vecEvent[nIndex].vecNpc.slice(0);
                    stTalk.vecSpeak = this.StringVecorToDataVector(stData.vecEvent[nIndex].vecSpeak);
                    stTalk.bAutoTrigle = CPubFunction.GetDefault(stData.vecEvent[nIndex].bAutoTrigle, 0);
                    stTask.vecEvent.push(stTalk);
                }

                if (stData.vecEvent[nIndex].nEventType == EEventType.PlayerGatherNpc) {
                    let stGather = new CGatherNpc();
                    stGather.nEventType = EEventType.PlayerGatherNpc;
                    stGather.strTip = stData.vecEvent[nIndex].strTip;
                    stGather.vecPrize = this.StringVecorToDataVector(stData.vecEvent[nIndex].vecPrize);
                    stGather.vecCreateNpc = this.GetTaskEventCreateNpc(stData.vecEvent[nIndex].vecCreateNpc);
                    stGather.vecNpc = stData.vecEvent[nIndex].vecNpc.slice(0);
                    stTask.vecEvent.push(stGather);
                }

                if (stData.vecEvent[nIndex].nEventType == EEventType.PlayerDoAction) {
                    let stAction = new CDoActionInArea();
                    stAction.nEventType = EEventType.PlayerDoAction;
                    stAction.vecPrize = this.StringVecorToDataVector(stData.vecEvent[nIndex].vecPrize);
                    stAction.strTip = stData.vecEvent[nIndex].strTip;
                    stAction.nMap = stData.vecEvent[nIndex].nMap;
                    stAction.nX = stData.vecEvent[nIndex].nX;
                    stAction.nY = stData.vecEvent[nIndex].nY;
                    stAction.strAction = stData.vecEvent[nIndex].strAction;
                    stAction.strTalk = stData.vecEvent[nIndex].strTalk;
                    stTask.vecEvent.push(stAction);
                }

                if (stData.vecEvent[nIndex].nEventType == EEventType.PlayerArriveArea) {
                    let stAction = new CArriveArea();
                    stAction.nEventType = EEventType.PlayerArriveArea;
                    stAction.vecPrize = this.StringVecorToDataVector(stData.vecEvent[nIndex].vecPrize);
                    stAction.strTip = stData.vecEvent[nIndex].strTip;
                    stAction.nMap = stData.vecEvent[nIndex].nMap;
                    stAction.nX = stData.vecEvent[nIndex].nX;
                    stAction.nY = stData.vecEvent[nIndex].nY;
                    stTask.vecEvent.push(stAction);
                }

                if (stData.vecEvent[nIndex].nEventType == EEventType.PlayerGiveNpcItem) {
                    let stAction = new CGiveNpcItem();
                    stAction.nEventType = EEventType.PlayerGiveNpcItem;
                    stAction.vecPrize = this.StringVecorToDataVector(stData.vecEvent[nIndex].vecPrize);
                    stAction.strTip = stData.vecEvent[nIndex].strTip;
                    stAction.nItemID = stData.vecEvent[nIndex].nItemID;
                    stAction.nNum = stData.vecEvent[nIndex].nNum;
                    stAction.nFromNpc = stData.vecEvent[nIndex].nFromNpc;
                    stAction.nNpcConfigID = stData.vecEvent[nIndex].nToNpc;
                    stAction.strTip2 = stData.vecEvent[nIndex].strTip2;
                    stTask.vecEvent.push(stAction);
                }

                if (stData.vecEvent[nIndex].nEventType == EEventType.PlayerKillNpc) {
                    let stEvent = new CKillDynamicNpc();
                    stEvent.nEventType = EEventType.PlayerKillNpc;
                    stEvent.vecPrize = this.StringVecorToDataVector(stData.vecEvent[nIndex].vecPrize);
                    stEvent.strTip = stData.vecEvent[nIndex].strTip;
                    stEvent.vecCreateNpc = this.GetTaskEventCreateNpc(stData.vecEvent[nIndex].vecCreateNpc);
                    stEvent.vecNpc = stData.vecEvent[nIndex].vecNpc.slice(0);
                    stEvent.bAutoTrigle = stData.vecEvent[nIndex].bAutoTrigle;
                    stTask.vecEvent.push(stEvent);
                }
            }

            for (let nIndex in stData.vecFailEvent) {
                if (stData.vecFailEvent[nIndex].nEventType == EEventType.FailEventPlayerDead) {
                    let stEvent = new CFailEventPlayerDead();
                    stTask.vecFailEvent.push(stEvent);
                }

                if (stData.vecFailEvent[nIndex].nEventType == EEventType.FailEventTimeOut) {
                    let stEvent = new CFailEventTimeOut();
                    stTask.vecFailEvent.push(stEvent);
                }
            }
            stTask.vecLimit = this.StringVecorToDataVector(stData.vecLimit);
            this.AddTask(stTask.nKind, stTask);
        }
    }

    GetTaskInfo(nTaskID) {
        if (CPubFunction.GetMapLen(this.mapConfigTask) <= 0) {
            this.InitAllTask();
        }

        for (let nKind in this.mapConfigTask) {
            let vecTask = this.mapConfigTask[nKind];

            for (let nIndex in vecTask) {
                if (vecTask[nIndex].nTaskID == nTaskID)
                    return vecTask[nIndex];
            }
        }
        return null;
    }

    GetTaskStepInfo(nTaskID, nStep) {
        let stTaskConfig = this.GetTaskInfo(nTaskID);
        if (null == stTaskConfig)
            return null;

        if (nStep < 0 || nStep >= stTaskConfig.vecEvent.length)
            return null;

        return stTaskConfig.vecEvent[nStep];
    }

    GetFailEventInfo(nTaskID, nStep) {
        let stTaskConfig = this.GetTaskInfo(nTaskID);
        if (null == stTaskConfig)
            return null;

        if (nStep < 0 || nStep >= stTaskConfig.vecFailEvent.length)
            return null;

        return stTaskConfig.vecFailEvent[nStep];
    }

    IsTeamTask(nTaskID) {

        if (nTaskID >= 500)
            return true;

        return false;
    }

    IsTeamDaily(nGrop) {
        if (Common.IsDataInVecter(nGrop, [5, 6, 7]))
            return true;

        return false;
    }
}

let g_pTaskConfigMgr = null;

module.exports = (() => {
    if (g_pTaskConfigMgr == null) {
        g_pTaskConfigMgr = new CConfigTaskMgr();
    }
    return { g_pTaskConfigMgr, CTask, CGatherNpc, CEventTalkNpc, EEventType, ETaskKind, EState };
})();



