﻿cc.Class({
    extends: cc.Component,

    properties: {
    },


    onLoad() {},


    start() {},


    onDestroy() {},

    CreateAndPlayAnimation(curClip, callback) {

        let comAnimation = this.node.getComponent(cc.Animation);
        comAnimation.addClip(curClip);
        comAnimation.play(curClip.name);

        let self = this;

        comAnimation.on('stop', function () {
            if (callback){
                callback();
            }
            
            if (self.node) {
                self.node.destroy();
            }
        }, this);
    },
});