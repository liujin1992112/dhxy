cc.Class({
    extends: cc.Component,

    properties: {
        lockerPre: cc.Prefab,
    },

    onLoad() {

    },

    onLockerClicked(e, d){
        let canvas = cc.find('Canvas');
        if (canvas.getChildByName('LockerPanel') == null) {
            let locker = cc.instantiate(this.lockerPre);
            locker.parent = canvas;
            locker.name = 'LockerPanel';
        }
    }
});