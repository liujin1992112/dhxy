﻿let LivingThing = require('./game_object/living_thing');
let GameRes = require('./GameRes');
let CPubFunction = require('./PubFunction');
let CSpeak = require('./Speak');
let CMainPlayerInfo = require('./MainPlayerInfo');
let pTaskConfigMgr = require('./task_config').g_pTaskConfigMgr;
let EState = require('./task_config').EState;
let EEventType = require('./task_config').EEventType;
let GameDefine = require('./GameDefine');
let pNpcMgr = require('./NpcMgr');


let ENpcKind = {
    ERole: 1,
    EThing: 2,
    EBomb: 3
}

cc.Class({

    extends: LivingThing,

    properties: {
        NpcFunUI: cc.Prefab,
    },

    onLoad() {
        this._super();
        this.nConfigID = 0;
        this.obj_type = 0;
        this.resid = 0;
        this.run_type = 0;

        this.move_speed = 280;
        this.move_tiems = 0;
        this.animationNode = this.node.getChildByName('frame');
        this.weaponNode = this.animationNode.getChildByName('weapon');
        this.nameNode = this.node.getChildByName('name');
        this.moveAngle = 0;
        this.weaponname = '';
        this.resUrl = '';

        // cc.find('Button', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "Npc", "OnNpcClick", 0));

    },

    start() {
        this.schedule(this.objUpdate, 1, cc.macro.REPEAT_FOREVER, 0.1);

    },

    onDestroy() {
        if (pNpcMgr.CheckNpcRes(this.onlyid, this.resid) == false) {
            if (GameDefine.roleName[this.resid] == null) {
                // cc.loader.releaseRes(this.resUrl, cc.Texture2D);
                // cc.loader.releaseRes(this.resUrl, cc.SpriteFrame);
                // cc.loader.releaseRes(this.resUrl, cc.SpriteAtlas);
                // var deps = cc.loader.getDependsRecursively(this.resUrl);
                // cc.loader.release(deps);
            }
        }
    },

    clear() {
        let weaponNode = this.weaponNode;
        if (weaponNode) {
            let clips = weaponNode.getComponent(cc.Animation).getClips();
            for (const clip of clips) {
                weaponNode.getComponent(cc.Animation).stop(clip.name);
                weaponNode.getComponent(cc.Animation).removeClip(clip, true);
            }
        }

        let animationNode = this.animationNode;
        if (animationNode) {
            let clips = animationNode.getComponent(cc.Animation).getClips();
            for (const clip of clips) {
                animationNode.getComponent(cc.Animation).stop(clip.name);
                animationNode.getComponent(cc.Animation).removeClip(clip, true);
            }
        }
    },

    setInfo(objinfo) {
        this.netInfo = objinfo;

        if (this.netInfo.x < 0 || this.netInfo.y < 0) {
            this.netInfo.x = 0;
            this.netInfo.y = 0;
        }

        this.x = objinfo.x;
        this.y = objinfo.y

        this.onlyid = objinfo.onlyid;
        this.resid = objinfo.resid;
        this.name = objinfo.name;
        this.nConfigID = objinfo.npcconfig;
        this.nameNode.getComponent(cc.Label).string = objinfo.name;
        this.initAnimation();

        let nID = cc.ll.propData.mapdata[objinfo.mapid].mapid;
        let mapinfo = cc.ll.propData.map[nID];
        this.gridInfoArr = mapinfo.mapInfo;
        if (mapinfo.baseInfo.rows <= this.netInfo.y || mapinfo.baseInfo.lines <= this.netInfo.x) {
            this.netInfo.x = mapinfo.startPos.x;
            this.netInfo.y = mapinfo.startPos.y;
        }
        this.setObjPos(this.netInfo.x, this.netInfo.y);
        if (cc.ll.propData.resanchor[this.resid]) {
            this.animationNode.anchorX = CPubFunction.GetDefault(cc.ll.propData.resanchor[this.resid].anchorX, 0.5);
            this.animationNode.anchorY = CPubFunction.GetDefault(cc.ll.propData.resanchor[this.resid].anchorY, 0.3);
        }

        if (this.gridInfoArr[this.netInfo.y][this.netInfo.x] == 2) {
            this.node.opacity = 150;
        }
    },

    initAnimation() {
        this.animationNode.active = false;
        this.resUrl = `shap/${this.resid}/stand_1`;
        let self = this;

        cc.loader.loadRes(this.resUrl, cc.SpriteAtlas, function (err, atlas) {
            if (err != null)
                return;

            if (self == null || self.animationNode == null)
                return;

            let curFrames = atlas.getSpriteFrames();
            let curClip = cc.AnimationClip.createWithSpriteFrames(curFrames, 10);
            curClip.name = 'stand';
            curClip.wrapMode = cc.WrapMode.Loop;

            let nodeAni = self.animationNode.getComponent(cc.Animation);
            nodeAni.addClip(curClip);
            nodeAni.play('stand');
            self.scheduleOnce(() => {
                let xDir = 1;
                if (self.netInfo.dir == GameDefine.EDir.ELeft || self.netInfo.dir == GameDefine.EDir.ERight) {
                    xDir = -1;
                }
                self.animationNode.scaleY = 500 / self.animationNode.height;
                self.animationNode.scaleX = 500 / self.animationNode.width * xDir;
            }, 0);

            self.animationNode.active = true;
            CPubFunction.addWeaponAnimation('', self.weaponNode, self.animationNode, self, self.resid, 'stand', 1);
        });


    },

    Npc_Init(strDefaultTalk, mapButton) {
        this.strDefaultTalk = strDefaultTalk;
        this.mapButton = mapButton;

        this.mapTaskBtn = {};
    },




    ReconTaskBtnList() {
        this.mapTaskBtn = {};

        let comRole = this.node.getComponent('Npc');
        let strNpcName = cc.find('name', this.node).getComponent(cc.Label).string;

        for (var it in CMainPlayerInfo.vecTaskState) {
            let stTaskState = CMainPlayerInfo.vecTaskState[it];
            let stTaskConfig = pTaskConfigMgr.GetTaskInfo(stTaskState.nTaskID);

            for (var itStep in stTaskState.vecStepState) {
                if (stTaskState.vecStepState[itStep].nState != EState.EDoing)
                    continue;

                let stStep = stTaskConfig.vecEvent[itStep];

                if (stStep.nEventType == EEventType.PlayerTalkNpc) {
                    if (stStep.vecNpc[0] != this.nConfigID)
                        continue;

                    let vecTmp = CPubFunction.GetTaskTalkList(stTaskConfig.nTaskID, itStep);

                    this.mapTaskBtn[stTaskConfig.strTaskName] = { vecTmp: vecTmp, nTaskID: stTaskState.nTaskID, nStep: itStep };
                }

                if (stStep.nEventType == EEventType.PlayerGiveNpcItem) {
                    if (stStep.nNpcConfigID != this.nConfigID)
                        continue;

                    let vecTmp = [];
                    this.mapTaskBtn[stTaskConfig.strTaskName] = { vecTmp: vecTmp, nTaskID: stTaskState.nTaskID, nStep: itStep };
                }


                if (stStep.nEventType == EEventType.PlayerKillNpc) {
                    for (let it2 in stStep.vecNpc) {
                        if (stStep.vecNpc[it2] != this.nConfigID)
                            continue;

                        this.mapTaskBtn[stTaskConfig.strTaskName] = { vecTmp: [], nTaskID: stTaskState.nTaskID, nStep: itStep };
                        break;
                    }
                }
            }
        }
    },


    CloseCurUI() {
        CPubFunction.FindAndDeleteNode(cc.find('Canvas/MainUI'), "NpcFunUI");
        CPubFunction.FindAndHidenNode(cc.find('Canvas/MainUI'), { nX: 0, nY: -1000 });
    },


    OpenFunctionUI() {
        let strNpcName = cc.find('name', this.node).getComponent(cc.Label).string;

        let goNpcFunUI = CPubFunction.CreateSubNode(cc.find('Canvas/MainUI'), { nX: 300, nY: 0 }, this.NpcFunUI, 'NpcFunUI');
        goNpcFunUI.getComponent('NpcFunUI').NpcFunUI_Init(this.GetOnlyID(), this.nConfigID, strNpcName, this.strDefaultTalk, this.GetFunctionMapBtn(), this.mapTaskBtn);
    },


    GetOnlyID() {
        let comRole = this.node.getComponent('Npc');
        let nID = comRole.onlyid;
        return nID;
    },

    IsGrass() {
        let resid = this.node.getComponent('Npc').resid;
        if (resid == 2003)
            return true;

        return false;
    },


    GetKind() {
        let stConfig = pNpcMgr.GetNpcConfigInfo(this.nConfigID);
        return stConfig.nKind;
    },

    OnGrassClick() {
        CPubFunction.CreateWaitTip('采集', 1, () => {
            cc.ll.net.send('c2s_act_npc', { nOnlyID: this.GetOnlyID(), nNpcConfigID: this.nConfigID, });
        });
    },

    OnBombClick() {
        cc.ll.net.send('c2s_trigle_npc_bomb', { nNpcConfigID: this.nConfigID, nNpcOnlyID: this.GetOnlyID() });
    },


    GetFunctionMapBtn() {

        let mapTmp = Object.assign({}, this.mapButton);

        for (var it in mapTmp) {
            if (it.indexOf('daily') == -1)
                continue;

            if (this.nConfigID == 30062 && cc.ll.player.race != 1) // 菩提祖师-人族
            {
                delete mapTmp[it];
                continue
            }

            if (this.nConfigID == 10118 && cc.ll.player.race != 2) // 太上老君-仙族
            {
                delete mapTmp[it];
                continue
            }

            if (this.nConfigID == 30063 && cc.ll.player.race != 3) // 牛魔王-魔族
            {
                delete mapTmp[it];
                continue
            }

            if (this.nConfigID == 30065 && cc.ll.player.race != 4) // 聂小倩-鬼族
            {
                delete mapTmp[it];
                continue
            }


            let vecTmp = it.split(":");
            if (CMainPlayerInfo.IsAlreadyHasThisGropTask(vecTmp[1])) {
                delete mapTmp[it];
                continue
            }
        }

        return mapTmp;

    },

    OnHumanClick() {
        this.ReconTaskBtnList();

        if (CPubFunction.GetMapLen(this.GetFunctionMapBtn()) > 0) {
            this.OpenFunctionUI();
            return;
        }

        if (CPubFunction.GetMapLen(this.mapTaskBtn) > 1) {
            this.OpenFunctionUI();
        }

        else if (CPubFunction.GetMapLen(this.mapTaskBtn) == 1) {
            let pSelf = this;

            let stData = CPubFunction.GetMapDataByIndex(this.mapTaskBtn, 0);
            CPubFunction.CreateNpcTalk(stData.vecTmp, (data) => {
                cc.ll.net.send('c2s_task_talk_npc', { nTaskID: stData.nTaskID, nStep: stData.nStep, nNpcConfigID: pSelf.nConfigID, nNpcOnlyID: pSelf.GetOnlyID() });
            });

        } else {
            //没有功能 没有任务
            if (this.strDefaultTalk && this.strDefaultTalk != '') {
                let stSpeak = new CSpeak(this.node.getComponent('Npc').resid, this.strDefaultTalk, cc.find('name', this.node).getComponent(cc.Label).string, 2); //  Npc 默认的自言自语黑框
                CPubFunction.CreateNpcTalk([stSpeak], null);
            }
        }
    },

    OnNpcClick() {
        let comRole = this.node.getComponent('Npc');
        let nDistance = CPubFunction.GetDistance({ x: cc.ll.player.x, y: cc.ll.player.y }, { x: comRole.x, y: comRole.y });
        if (nDistance >= 20)
            return;

        this.CloseCurUI();

        let nKind = this.GetKind();

        if (nKind == ENpcKind.ERole) {
            this.OnHumanClick();
        }

        if (nKind == ENpcKind.EThing) {
            this.OnGrassClick();
        }

        if (nKind == ENpcKind.EBomb) {
            this.OnBombClick();
        }
    },
});
