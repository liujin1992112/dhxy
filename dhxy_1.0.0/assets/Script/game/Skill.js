﻿let SkillConfig = require('../etc/skill_config')
let SkillStr = require('../etc/skill_str')

class CBaseSkill {
    constructor() {
        this.nID = 0; //技能id
        this.strName = ''; // 技能名字
        this.strIcon = ''; //技能图标
        this.strDesc = ''; //技能介绍
        this.strParticleEffect = ''; //特效
        this.bEffectDir = false; // 特效是否区分方向
        this.bEffectPos = SkillConfig.EffectPos.Body; // 
        this.strBuffEffect = null; // buff特效名字
        this.nSkillEffectX = 0; // 技能特效 显示修正位置
        this.nSkillEffectY = 0; // 技能特效 显示修正位置
        this.nBuffEffectY = 0; //buff特效 显示位置
        this.nBuffFront = true; // buff显示在人物之前 还是之后
        this.nMaxExp = 10000; //最大熟练度
        this.nCurExp = 0; //当前熟练度
        this.atkType = SkillConfig.AttackType.Normal; //近身|远程
        this.nFaXi = 0; //法系
        this.nScale = SkillConfig.AffectType.Single; //使用范围，单攻/群攻
        this.vecLevelExp = []; //有序vecter，技能等级对应的熟练度
        this.action_type = SkillConfig.BtlActionType.Initiative; // 主动 被动
        this.skillActOn = SkillConfig.ActOn.Enemy;
    }

    GetExpPrice() { //升级需要消费的银两
        let level = Math.floor(this.nCurExp / 100);
        let price = 0;
        let levelprice = SkillConfig.SkillConsume[level];
        if (levelprice) {
            price = levelprice[this.nScale];
        }
        return price;
    }

    //当前等级描述
    GetDetail() {
        return "";
    }

    //下一等级描述
    GetNextLevelDetail() {
        let nCurGrade = this.GetCurGrade();
        if (nCurGrade >= this.vecLevelExp.length)
            return '';

        let nNextGrade = nCurGrade + 1;
        let stLevelData = this.GetLevelData(this.vecLevelExp[nNextGrade - 1]);
        if (null == stLevelData)
            return '';

        if (stLevelData.nRound != null)
            return `熟练度到达${this.vecLevelExp[nNextGrade - 1]}时，目标人数${stLevelData.nTargetCnt}人，持续${stLevelData.nRound}个回合。`;
        else
            return `熟练度到达${this.vecLevelExp[nNextGrade - 1]}时，目标人数${stLevelData.nTargetCnt}人。`;
    }

    //获得持续回合、目标人数
    GetLevelData() {
        return null;
    }

    GetCurGrade() {
        let nMax = 0;
        for (let i = 0; i < this.vecLevelExp.length; i++) {
            if (this.nCurExp >= this.vecLevelExp[i])
                nMax = i + 1;
        }
        return nMax;
    }

    //耗蓝 所有技能每增加100点熟练，蓝消耗增加40
    GetCostMP() {
        return Math.floor(this.nCurExp / 100) * 40;
    }

    getName() {
        return this.strName;
    }

    getFaxiName() {
        return SkillStr.EMagicStr[this.nFaXi];
    }

    getIntro() {
        return SkillStr.SkillIntros[this.nID];
    }

    getEffect() {
        return {};
    }
}

//--------------------- 普通攻击 ---------------//
class CNormalAttack extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.NormalAtkSkill;
        this.strName = '普通攻击';
        this.strIcon = '1001';
        this.strDesc = '攻击';
        this.nFaXi = SkillConfig.EMagicType.Physics;
        this.atkType = SkillConfig.AttackType.Melee;
        this.skillActOn = SkillConfig.ActOn.All;
    }
}

//--------------------- 防御 ---------------//
class CNormalDefend extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.NormalDefSkill;
        this.strName = '防御';
        this.strIcon = '1002';
        this.strDesc = '防御';
        this.nFaXi = SkillConfig.EMagicType.Defense;
    }
}

//--------------------- 鹤顶红粉 ---------------//
class CHeDingHong extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.HeDingHongFen;
        this.strName = '鹤顶红粉 ';
        this.strIcon = '1003';
        this.strDesc = '使单个目标中毒。';
        this.strParticleEffect = '21101';
        this.strBuffEffect = 'du';
        this.nFaXi = SkillConfig.EMagicType.Toxin;
        this.vecLevelExp = [1700, 13000];
    }

    GetLevelData(nLevel) {
        return {
            nTargetCnt: 1,
            nRound: Math.floor(2 * (1 + Math.pow(nLevel, 0.34) * 4 / 100))
        }
    }

    GetDetail() {
        let stLevelData = this.GetLevelData(this.nCurExp);
        return `利用无色无味的剧毒修炼出来的药粉，消灭敌人于无形。目标人数${stLevelData.nTargetCnt}人，持续${stLevelData.nRound}个回合。(毒发伤害不超过最大生命值50%）`;
    }
}

//--------------------- 万毒攻心  ---------------//
class CWanDuGongXin extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.WanDuGongXin;
        this.strName = '万毒攻心 ';
        this.strIcon = '1007';
        this.strDesc = '使多个目标中毒。';
        this.strParticleEffect = '21202';
        this.strBuffEffect = 'du';
        this.nFaXi = SkillConfig.EMagicType.Toxin;
        this.nScale = SkillConfig.AffectType.Group;
        this.bEffectPos = SkillConfig.EffectPos.Stage;
        this.vecLevelExp = [300, 1700, 5300, 11900, 13000];
    }

    GetLevelData(nLevel) {
        return {
            nTargetCnt: Math.min(7, Math.floor(3 * (1 + Math.pow(nLevel, 0.35) * 5 / 100))),
            nRound: Math.floor(2 * (1 + Math.pow(nLevel, 0.34) * 4 / 100))
        }
    }

    GetDetail() {
        let stLevelData = this.GetLevelData(this.nCurExp);
        return `集天下最毒之物所修炼而成的法术，剧毒无比，中者不死即伤。目标人数${stLevelData.nTargetCnt}人，持续${stLevelData.nRound}个回合。(毒发伤害不超过最大生命值50%）`;
    }
}

//--------------------- 借刀杀人  ---------------//
class CJieDaoShaRen extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.JieDaoShaRen;
        this.strName = '借刀杀人 ';
        this.strIcon = '1004';
        this.strDesc = '使单个目标混乱。';
        this.strParticleEffect = '21103';
        this.strBuffEffect = 'luan';
        this.nFaXi = SkillConfig.EMagicType.Chaos;
        this.vecLevelExp = [300, 1700, 5300, 11900, 13000];
    }

    GetLevelData(nLevel) {
        return {
            nTargetCnt: 1,
            nRound: Math.floor(3 * (1 + Math.pow(nLevel, 0.3) * 5 / 100))
        }
    }

    GetDetail() {
        let stLevelData = this.GetLevelData(this.nCurExp);
        return `非常巧妙的计谋，借他人之手而杀敌，使得他人进行混淆攻击。目标人数${stLevelData.nTargetCnt}人，持续${stLevelData.nRound}个回合。`;
    }
}

//--------------------- 失心狂乱  ---------------//
class CShiXinKuangLuan extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.ShiXinKuangLuan;
        this.strName = '失心狂乱';
        this.strIcon = '1008';
        this.strDesc = '使多个目标混乱。';
        this.strParticleEffect = '21204';
        this.strBuffEffect = 'luan';
        this.nFaXi = SkillConfig.EMagicType.Chaos;
        this.nScale = SkillConfig.AffectType.Group;
        this.bEffectPos = SkillConfig.EffectPos.Stage;
        this.vecLevelExp = [600, 1000, 5700, 7100, 21800];
    }

    GetLevelData(nLevel) {
        return {
            nTargetCnt: Math.min(5, Math.floor(3 * (1 + Math.pow(nLevel, 0.35) * 3 / 100))),
            nRound: Math.floor(3 * (1 + Math.pow(nLevel, 0.3) * 5 / 100))
        }
    }

    GetDetail() {
        let stLevelData = this.GetLevelData(this.nCurExp);
        return `使用混乱法术，使目标变得丧心病狂，而自相攻击起来。目标人数${stLevelData.nTargetCnt}人，持续${stLevelData.nRound}个回合。`;
    }
}

//--------------------- 迷魂醉  ---------------//
class CMiHunZui extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.MiHunZui;
        this.strName = '迷魂醉';
        this.strIcon = '1005';
        this.strDesc = '使单个目标昏睡。';
        this.strParticleEffect = '21105';
        this.strBuffEffect = 'shui';
        this.nFaXi = SkillConfig.EMagicType.Sleep;
        this.vecLevelExp = [200, 1900, 7100, 18500];
    }

    GetLevelData(nLevel) {
        return {
            nTargetCnt: 1,
            nRound: Math.floor(3 * (1 + Math.pow(nLevel, 0.3) * 7 / 100))
        }
    }

    GetDetail() {
        let stLevelData = this.GetLevelData(this.nCurExp);
        return `道家的不二法门，可令敌人如同醉酒，昏昏欲睡。目标人数${stLevelData.nTargetCnt}人，持续${stLevelData.nRound}个回合。`;
    }
}

//--------------------- 百日眠 ---------------//
class CBaiRiMian extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.BaiRiMian;
        this.strName = '百日眠';
        this.strIcon = '1009';
        this.strDesc = '使多个目标昏睡。';
        this.strParticleEffect = '21206';
        this.strBuffEffect = 'shui';
        this.nFaXi = SkillConfig.EMagicType.Sleep;
        this.nScale = SkillConfig.AffectType.Group;
        this.bEffectPos = SkillConfig.EffectPos.Stage;
        this.vecLevelExp = [200, 300, 1700, 1900, 5300, 7100, 11900, 18500];
    }

    GetLevelData(nLevel) {
        return {
            nTargetCnt: Math.min(7, Math.floor(3 * (1 + Math.pow(nLevel, 0.35) * 5 / 100))),
            nRound: Math.floor(3 * (1 + Math.pow(nLevel, 0.3) * 7 / 100))
        }
    }

    GetDetail() {
        let stLevelData = this.GetLevelData(this.nCurExp);
        return `菩提祖师的修炼法门之一，据说使用可以让人百日不醒。目标人数${stLevelData.nTargetCnt}人，持续${stLevelData.nRound}个回合。`;
    }
}

//--------------------- 作壁上观  ---------------//
class CZuoBiShangGuan extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.ZuoBiShangGuan;
        this.strName = '作壁上观';
        this.strIcon = '1006';
        this.strDesc = '使单个目标封印。';
        this.strParticleEffect = '21107';
        this.strBuffEffect = 'bing';
        this.nFaXi = SkillConfig.EMagicType.Seal;
        this.vecLevelExp = [200, 1900, 7100, 18500];
    }

    GetLevelData(nLevel) {
        return {
            nTargetCnt: 1,
            nRound: Math.floor(3 * (1 + Math.pow(nLevel, 0.3) * 7 / 100))
        }
    }

    GetDetail() {
        let stLevelData = this.GetLevelData(this.nCurExp);
        return `以强大的异能封住对手，使其完全无法动弹，而只能作壁上观。目标人数${stLevelData.nTargetCnt}人，持续${stLevelData.nRound}个回合。`;
    }
}

//--------------------- 四面楚歌  ---------------//
class CSiMianChuGe extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.SiMianChuGe;
        this.strName = '四面楚歌';
        this.strIcon = '1010';
        this.strDesc = '使多个目标封印。';
        this.strParticleEffect = '21208';
        this.strBuffEffect = 'bing';
        this.nFaXi = SkillConfig.EMagicType.Seal;
        this.nScale = SkillConfig.AffectType.Group;
        this.bEffectPos = SkillConfig.EffectPos.Stage;
        this.vecLevelExp = [200, 300, 1700, 1900, 5300, 7100, 11900, 18500];
    }

    GetLevelData(nLevel) {
        return {
            nTargetCnt: Math.min(7, Math.floor(3 * (1 + Math.pow(nLevel, 0.35) * 5 / 100))),
            nRound: Math.floor(3 * (1 + Math.pow(nLevel, 0.3) * 7 / 100))
        }
    }

    GetDetail() {
        let stLevelData = this.GetLevelData(this.nCurExp);
        return `发动四面八方的能量，隔绝对手与外界的接触，使其只能坐以待毙。目标人数${stLevelData.nTargetCnt}人，持续${stLevelData.nRound}个回合。`;
    }
}

//--------------------- 烈火骄阳  ---------------//
class CLieHuoJiaoYang extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.LieHuoJiaoYang;
        this.strName = '烈火骄阳';
        this.strIcon = SkillConfig.SkillIds.LieHuoJiaoYang;
        this.strDesc = '火法攻击单目标。';
        this.strParticleEffect = '22109';
        this.nFaXi = SkillConfig.EMagicType.Fire;
        this.vecLevelExp = [];
    }

    GetLevelData(nLevel) {
        let nCurGrade = this.GetCurGrade();
        return {
            nTargetCnt: 1,
            Hurt: Math.floor(65 * nCurGrade * (Math.pow(nLevel, 0.4) * 2.8853998118144273 / 100 + 1))
        }
    }

    GetDetail() {
        let stLevelData = this.GetLevelData(this.nCurExp);
        return `吸收天上太阳的热力才凝聚起来的强烈火焰，让人受到严重伤害。目标人数${stLevelData.nTargetCnt}人。(基础伤害${stLevelData.Hurt})`;
    }
}

//--------------------- 九阴纯火  ---------------//
class CJiuYinChunHuo extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.JiuYinChunHuo;
        this.strName = '九阴纯火';
        this.strIcon = '1015';
        this.strDesc = '火法攻击多目标。';
        this.strParticleEffect = '22210';
        this.nFaXi = SkillConfig.EMagicType.Fire;
        this.nScale = SkillConfig.AffectType.Group;
        this.bEffectPos = SkillConfig.EffectPos.Stage;
        this.vecLevelExp = [600, 5700];
    }

    GetLevelData(nLevel) {
        let nCurGrade = this.GetCurGrade();
        return {
            nTargetCnt: Math.min(5, Math.floor(3 * (1 + Math.pow(nLevel, 0.3) * 5 / 100))),
            Hurt: Math.floor(60 * nCurGrade * (Math.pow(nLevel, 0.4) * 2.8853998118144273 / 100 + 1))
        }
    }

    GetDetail() {
        let stLevelData = this.GetLevelData(this.nCurExp);
        return `天下最厉害的火，不是阳刚之火，而是至柔至阴的以柔克刚的阴火，威力不可想象。目标人数${stLevelData.nTargetCnt}人。(基础伤害${stLevelData.Hurt})`;
    }
}

//--------------------- 风雷涌动  ---------------//
class CFengLeiYongDong extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.FengLeiYongDong;
        this.strName = '风雷涌动';
        this.strIcon = SkillConfig.SkillIds.FengLeiYongDong;
        this.strDesc = '风法攻击单目标。';
        this.strParticleEffect = '22111';
        this.nFaXi = SkillConfig.EMagicType.Wind;
        this.vecLevelExp = [];
    }

    GetLevelData(nLevel) {
        let nCurGrade = this.GetCurGrade();
        return {
            nTargetCnt: 1,
            Hurt: Math.floor(65 * nCurGrade * (Math.pow(nLevel, 0.4) * 2.8853998118144273 / 100 + 1))
        }
    }

    GetDetail() {
        let stLevelData = this.GetLevelData(this.nCurExp);
        return `利用自己身体中的能量产生强烈的有毒的风进行攻击。目标人数${stLevelData.nTargetCnt}人。(基础伤害${stLevelData.Hurt})`;
    }
}

//--------------------- 袖里乾坤  ---------------//
class CXiuLiQianKun extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.XiuLiQianKun;
        this.strName = '袖里乾坤';
        this.strIcon = '1016';
        this.bEffectDir = true;
        this.strDesc = '风法攻击多目标。';
        this.strParticleEffect = '22212';
        this.nFaXi = SkillConfig.EMagicType.Wind;
        this.nScale = SkillConfig.AffectType.Group;
        this.bEffectPos = SkillConfig.EffectPos.Stage;
        this.vecLevelExp = [600, 5700];
    }

    GetLevelData(nLevel) {
        let nCurGrade = this.GetCurGrade();
        return {
            nTargetCnt: Math.min(5, Math.floor(3 * (1 + Math.pow(nLevel, 0.3) * 5 / 100))),
            Hurt: Math.floor(60 * nCurGrade * (Math.pow(nLevel, 0.4) * 2.8853998118144273 / 100 + 1))
        }
    }

    GetDetail() {
        let stLevelData = this.GetLevelData(this.nCurExp);
        return `镇元大仙的绝技，乾坤袖一抖就令天地动容，风云变色，伤人于无形。目标人数${stLevelData.nTargetCnt}人。(基础伤害${stLevelData.Hurt})`;
    }
}

//--------------------- 电闪雷鸣  ---------------//
class CDianShanLeiMing extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.DianShanLeiMing;
        this.strName = '电闪雷鸣';
        this.strIcon = SkillConfig.SkillIds.DianShanLeiMing;
        this.strDesc = '雷法攻击单目标。';
        this.strParticleEffect = '22113';
        this.nSkillEffectY = 40;
        this.nFaXi = SkillConfig.EMagicType.Thunder;
        this.vecLevelExp = [];
    }

    GetLevelData(nLevel) {
        let nCurGrade = this.GetCurGrade();
        return {
            nTargetCnt: 1,
            Hurt: Math.floor(65 * nCurGrade * (Math.pow(nLevel, 0.4) * 2.8853998118144273 / 100 + 1))
        }
    }

    GetDetail() {
        let stLevelData = this.GetLevelData(this.nCurExp);
        return `让掌管雷电的神仙帮助施法，强大的力量和声音伤害他人。目标人数${stLevelData.nTargetCnt}人。(基础伤害${stLevelData.Hurt})`;
    }
}

//--------------------- 天诛地灭  ---------------//
class CTianZhuDiMie extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.TianZhuDiMie;
        this.strName = '天诛地灭';
        this.strIcon = '1017';
        this.strDesc = '雷法攻击多目标。';
        this.strParticleEffect = '22214';
        this.nFaXi = SkillConfig.EMagicType.Thunder;
        this.nScale = SkillConfig.AffectType.Group;
        this.bEffectPos = SkillConfig.EffectPos.Stage;
        this.vecLevelExp = [600, 5700];
    }

    GetLevelData(nLevel) {
        let nCurGrade = this.GetCurGrade();
        return {
            nTargetCnt: Math.min(5, Math.floor(3 * (1 + Math.pow(nLevel, 0.3) * 5 / 100))),
            Hurt: Math.floor(60 * nCurGrade * (Math.pow(nLevel, 0.4) * 2.8853998118144273 / 100 + 1))
        }
    }

    GetDetail() {
        let stLevelData = this.GetLevelData(this.nCurExp);
        return `缭绕的仙气，唤起天界异人之雷力，施与对手无比绝伦的伤害。目标人数${stLevelData.nTargetCnt}人。(基础伤害${stLevelData.Hurt})`;
    }
}

//--------------------- 蛟龙出海  ---------------//
class CJiaoLongChuHai extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.JiaoLongChuHai;
        this.strName = '蛟龙出海';
        this.strIcon = SkillConfig.SkillIds.JiaoLongChuHai;
        this.strDesc = '水法攻击单目标。';
        this.strParticleEffect = '22115';
        this.nFaXi = SkillConfig.EMagicType.Water;
        this.vecLevelExp = [];
    }

    GetLevelData(nLevel) {
        let nCurGrade = this.GetCurGrade();
        return {
            nTargetCnt: 1,
            Hurt: Math.floor(65 * nCurGrade * (Math.pow(nLevel, 0.4) * 2.8853998118144273 / 100 + 1))
        }
    }

    GetDetail() {
        let stLevelData = this.GetLevelData(this.nCurExp);
        return `传说中蛟龙飞升的时候天会降大雨，利用这个能量进行攻击伤害别人。目标人数${stLevelData.nTargetCnt}人。(基础伤害${stLevelData.Hurt})`;
    }
}

//--------------------- 九龙冰封  ---------------//
class CJiuLongBingFeng extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.JiuLongBingFeng;
        this.strName = '九龙冰封';
        this.strIcon = '1018';
        this.strDesc = '水法攻击多目标。';
        this.strParticleEffect = '22216';
        this.nFaXi = SkillConfig.EMagicType.Water;
        this.nScale = SkillConfig.AffectType.Group;
        this.bEffectPos = SkillConfig.EffectPos.Stage;
        this.vecLevelExp = [600, 5700];
    }

    GetLevelData(nLevel) {
        let nCurGrade = this.GetCurGrade();
        return {
            nTargetCnt: Math.min(5, Math.floor(3 * (1 + Math.pow(nLevel, 0.3) * 5 / 100))),
            Hurt: Math.floor(60 * nCurGrade * (Math.pow(nLevel, 0.4) * 2.8853998118144273 / 100 + 1))
        }
    }

    GetDetail() {
        let stLevelData = this.GetLevelData(this.nCurExp);
        return `利用巨大的水流转化而成的冰块进行袭击敌人，在攻击的过程中把人冰封了。目标人数${stLevelData.nTargetCnt}人。(基础伤害${stLevelData.Hurt})`;
    }
}

//--------------------- 魔神护体  ---------------//
class CMoShenHuTi extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.MoShenHuTi;
        this.strName = '魔神护体';
        this.strIcon = '1019';
        this.strDesc = '使单目标进入加防状态。';
        this.strParticleEffect = '23117';
        this.strBuffEffect = 'fang';
        this.nBuffEffectY = 45;
        this.nFaXi = SkillConfig.EMagicType.Defense;
        this.vecLevelExp = [300, 1700, 5300, 11900, 22500];
        this.skillActOn = SkillConfig.ActOn.Self;
    }

    GetLevelData(nLevel) {
        return {
            nTargetCnt: 1,
            nRound: Math.floor(3 * (1 + Math.pow(nLevel, 0.35) * 5 / 100)),
            nKongAdd: Math.round(1.1 * (Math.pow(nLevel, 0.35) * 20 / 100 + 1)),
            nFaAdd: Math.round(18 * (Math.pow(nLevel, 0.35) * 2 / 100 + 1)),
            nFangAdd: Math.round(15 * (Math.pow(nLevel, 0.35) * 2 / 100 + 1))
        }
    }

    GetDetail() {
        let stLevelData = this.GetLevelData(this.nCurExp);
        return `召唤鬼神依附到自己身上。控制抗性增加${stLevelData.nKongAdd}%，伤法抗性增加${stLevelData.nFaAdd}%，防御增加${stLevelData.nFangAdd}%。目标人数${stLevelData.nTargetCnt}人，持续${stLevelData.nRound}个回合。`;
    }
}

//--------------------- 含情脉脉  ---------------//
class CHanQingMoMo extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.HanQingMoMo;
        this.strName = '含情脉脉';
        this.strIcon = '1023';
        this.strDesc = '使多目标进入加防状态。';
        this.strParticleEffect = '23218';
        this.strBuffEffect = 'fang';
        this.nBuffEffectY = 45;
        this.nFaXi = SkillConfig.EMagicType.Defense;
        this.nScale = SkillConfig.AffectType.Group;
        this.bEffectPos = SkillConfig.EffectPos.Stage;
        this.vecLevelExp = [200, 300, 1200, 1700, 4600, 5300, 11900, 22500];
        this.skillActOn = SkillConfig.ActOn.Self;
    }

    GetLevelData(nLevel) {
        return {
            nTargetCnt: Math.min(7, Math.floor(3 * (1 + Math.pow(nLevel, 0.3) * 8 / 100))),
            nRound: Math.floor(3 * (1 + Math.pow(nLevel, 0.35) * 5 / 100)),
            nKongAdd: Math.round(0.9 * (Math.pow(nLevel, 0.35) * 20 / 100 + 1)),
            nFaAdd: Math.round(15 * (Math.pow(nLevel, 0.35) * 2 / 100 + 1)),
            nFangAdd: Math.round(12 * (Math.pow(nLevel, 0.35) * 2 / 100 + 1))
        }
    }

    GetDetail() {
        let stLevelData = this.GetLevelData(this.nCurExp);
        return `脉脉含情的眼神让对手忘记伤害。控制抗性增加${stLevelData.nKongAdd}%，伤法抗性增加${stLevelData.nFaAdd}%，防御增加${stLevelData.nFangAdd}%。目标人数${stLevelData.nTargetCnt}人，持续${stLevelData.nRound}个回合。`;
    }
}

//--------------------- 天外飞魔  ---------------//
class CTianWaiFeiMo extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.TianWaiFeiMo;
        this.strName = '天外飞魔';
        this.strIcon = '1020';
        this.strDesc = '使单目标进入加速状态。';
        this.strParticleEffect = '23119';
        this.strBuffEffect = 'su';
        this.nBuffEffectY = 30;
        this.nFaXi = SkillConfig.EMagicType.Speed;
        this.vecLevelExp = [300, 1700, 5300, 11900, 22500];
        this.skillActOn = SkillConfig.ActOn.Self;
    }

    GetLevelData(nLevel) {
        return {
            nTargetCnt: 1,
            nRound: Math.floor(3 * (1 + Math.pow(nLevel, 0.35) * 5 / 100)),
            nSpeedAdd: Math.round(15 + nLevel / 5000),
        }
    }

    GetDetail() {
        let stLevelData = this.GetLevelData(this.nCurExp);
        return `天外魔神处学来的法术，激发自身和队友潜能。速度增加${stLevelData.nSpeedAdd}%(负敏单位无效）。目标人数${stLevelData.nTargetCnt}人，持续${stLevelData.nRound}个回合。`;
    }
}

//--------------------- 乾坤借速  ---------------//
class CQianKunJieSu extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.QianKunJieSu;
        this.strName = '乾坤借速';
        this.strIcon = '1024';
        this.strDesc = '使多目标进入加速状态。';
        this.strParticleEffect = '23220';
        this.strBuffEffect = 'su';
        this.nBuffEffectY = 30;
        this.nFaXi = SkillConfig.EMagicType.Speed;
        this.nScale = SkillConfig.AffectType.Group;
        this.bEffectPos = SkillConfig.EffectPos.Stage;
        this.vecLevelExp = [200, 300, 1200, 1700, 4600, 5300, 11900, 22500];
        this.skillActOn = SkillConfig.ActOn.Self;
    }

    GetLevelData(nLevel) {
        return {
            nTargetCnt: Math.min(7, Math.floor(3 * (1 + Math.pow(nLevel, 0.3) * 8 / 100))),
            nRound: Math.floor(3 * (1 + Math.pow(nLevel, 0.35) * 5 / 100)),
            nSpeedAdd: Math.round(12 + 8 * nLevel / 25000),
        }
    }

    GetDetail() {
        let stLevelData = this.GetLevelData(this.nCurExp);
        return `借助战斗魔神的力量，增加自己和队友主动性。速度增加${stLevelData.nSpeedAdd}%(负敏单位无效）。目标人数${stLevelData.nTargetCnt}人，持续${stLevelData.nRound}个回合。`;
    }
}

//--------------------- 兽王神力  ---------------//
class CShouWangShenLi extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.ShouWangShenLi;
        this.strName = '兽王神力';
        this.strIcon = '1021';
        this.strDesc = '使单目标进入加攻状态。';
        this.strParticleEffect = '23121';
        this.strBuffEffect = 'liliang';
        this.nBuffEffectY = 25;
        this.nFaXi = SkillConfig.EMagicType.Attack;
        this.vecLevelExp = [300, 1700, 5300, 11900, 22500];
        this.skillActOn = SkillConfig.ActOn.Self;
    }

    GetLevelData(nLevel) {
        return {
            nTargetCnt: 1,
            nRound: Math.floor(3 * (1 + Math.pow(nLevel, 0.35) * 5 / 100)),
            nAttackAdd: Math.round(30 * (Math.pow(nLevel, 0.35) * 3 / 100 + 1))
        }
    }

    GetDetail() {
        let stLevelData = this.GetLevelData(this.nCurExp);
        return `共工法术，激发自己和队友的魔性。攻击力增加${stLevelData.nAttackAdd}%，命中增加15%。目标人数${stLevelData.nTargetCnt}人，持续${stLevelData.nRound}个回合。`;
    }
}

//--------------------- 魔神附身  ---------------//
class CMoShenFuShen extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.MoShenFuShen;
        this.strName = '魔神附身';
        this.strIcon = '1025';
        this.strDesc = '使多目标进入加攻状态。';
        this.strParticleEffect = '23222';
        this.strBuffEffect = 'liliang';
        this.nBuffEffectY = 25;
        this.nFaXi = SkillConfig.EMagicType.Attack;
        this.nScale = SkillConfig.AffectType.Group;
        this.bEffectPos = SkillConfig.EffectPos.Stage;
        this.vecLevelExp = [200, 300, 1200, 1700, 4600, 5300, 11900, 22500];
        this.skillActOn = SkillConfig.ActOn.Self;
    }

    GetLevelData(nLevel) {
        return {
            nTargetCnt: Math.min(7, Math.floor(3 * (1 + Math.pow(nLevel, 0.3) * 8 / 100))),
            nRound: Math.floor(3 * (1 + Math.pow(nLevel, 0.35) * 5 / 100)),
            nAttackAdd: Math.round(25 * (Math.pow(nLevel, 0.35) * 3 / 100 + 1))
        }
    }

    GetDetail() {
        let stLevelData = this.GetLevelData(this.nCurExp);
        return `战斗魔神之力，强化己方战斗力。攻击力增加${stLevelData.nAttackAdd}%，命中增加15%。目标人数${stLevelData.nTargetCnt}人，持续${stLevelData.nRound}个回合。`;
    }
}

//--------------------- 销魂蚀骨  ---------------//
class CXiaoHunShiGu extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.XiaoHunShiGu;
        this.strName = '销魂蚀骨';
        this.strIcon = '1022';
        this.strDesc = '使单目标损失气血和法力。';
        this.strParticleEffect = '23123';
        this.nFaXi = SkillConfig.EMagicType.Frighten;
        this.vecLevelExp = [];
    }

    GetLevelData(nLevel) {
        return {
            nTargetCnt: 1,
            nBloodSub: Math.round(27 * (Math.pow(nLevel, 0.35) * 2 / 100 + 1)),
            nBlueSub: Math.round(38 * (Math.pow(nLevel, 0.33) * 2 / 100 + 1))
        }
    }

    GetDetail() {
        let stLevelData = this.GetLevelData(this.nCurExp);
        return `把对手的生命用鬼神的力量和魔法偷偷消耗,减少当前气血${stLevelData.nBloodSub}%和法力${stLevelData.nBlueSub}%。目标人数${stLevelData.nTargetCnt}人。`;
    }
}

//--------------------- 阎罗追命  ---------------//
class CYanLuoZhuiMing extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.YanLuoZhuiMing;
        this.strName = '阎罗追命';
        this.strIcon = '1026';
        this.strDesc = '使多目标损失气血和法力。';
        this.strParticleEffect = '23224';
        this.nFaXi = SkillConfig.EMagicType.Frighten;
        this.nScale = SkillConfig.AffectType.Group;
        this.bEffectPos = SkillConfig.EffectPos.Stage;
        this.vecLevelExp = [300, 1700, 5300, 11900];
    }

    GetLevelData(nLevel) {
        return {
            nTargetCnt: Math.min(7, Math.floor(3 * (1 + Math.pow(nLevel, 0.35) * 5 / 100))),
            nBloodSub: Math.round(25 * (Math.pow(nLevel, 0.35) * 2 / 100 + 1)),
            nBlueSub: Math.round(37 * (Math.pow(nLevel, 0.33) * 2 / 100 + 1))
        }
    }

    GetDetail() {
        let stLevelData = this.GetLevelData(this.nCurExp);
        return `利用阎王的力量降低对手生命和魔法的超级法门,减少当前气血${stLevelData.nBloodSub}%和法力${stLevelData.nBlueSub}%。目标人数${stLevelData.nTargetCnt}人。`;
    }
}

//--------------------- 秦思冰雾  ---------------//
class CQinSiBingWu extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.QinSiBingWu;
        this.strName = '秦思冰雾';
        this.strIcon = '1027';
        this.strDesc = '使单目标进入魅惑状态。';
        this.strParticleEffect = '24125';
        this.strBuffEffect = 'meihuo';
        this.nBuffEffectY = 50;
        this.nFaXi = SkillConfig.EMagicType.Charm;
        this.vecLevelExp = [300, 1700, 5300, 11900, 22500];
    }

    GetLevelData(nLevel) {
        return {
            nTargetCnt: 1,
            nRound: Math.floor(3 * (1 + Math.pow(nLevel, 0.35) * 5 / 100)),
            nDebuff: Math.floor(1.5 * (Math.pow(nLevel, 0.35) * 20 / 100 + 1))
        }
    }

    GetDetail() {
        let stLevelData = this.GetLevelData(this.nCurExp);
        return `冰雾怨何穷，秦丝娇未已。减少冰混睡忘抗性${stLevelData.nDebuff}%，目标人数${stLevelData.nTargetCnt}人，持续${stLevelData.nRound}个回合。`;
    }
}

//--------------------- 倩女幽魂  ---------------//
class CQianNvYouHun extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.QianNvYouHun;
        this.strName = '倩女幽魂';
        this.strIcon = '1031';
        this.strDesc = '使多目标进入魅惑状态。';
        this.strParticleEffect = '24226';
        this.strBuffEffect = 'meihuo';
        this.nBuffEffectY = 50;
        this.nFaXi = SkillConfig.EMagicType.Charm;
        this.nScale = SkillConfig.AffectType.Group;
        this.bEffectPos = SkillConfig.EffectPos.Stage;
        this.vecLevelExp = [200, 300, 1200, 1700, 4600, 5300, 11900, 22500];
    }

    GetLevelData(nLevel) {
        return {
            nTargetCnt: Math.min(7, Math.floor(3 * (1 + Math.pow(nLevel, 0.3) * 8 / 100))),
            nRound: Math.floor(3 * (1 + Math.pow(nLevel, 0.35) * 5 / 100)),
            nDebuff: Math.round(1.3 * (Math.pow(nLevel, 0.35) * 20 / 100 + 1))
        }
    }

    GetDetail() {
        let stLevelData = this.GetLevelData(this.nCurExp);
        return `小倩独家秘籍。减少冰混睡忘抗性${stLevelData.nDebuff}%，目标人数${stLevelData.nTargetCnt}人，持续${stLevelData.nRound}个回合。`;
    }
}

//--------------------- 血煞之蛊  ---------------//
class CXueShaZhiGu extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.XueShaZhiGu;
        this.strName = '血煞之蛊';
        this.strIcon = '1028';
        this.strDesc = '攻击单目标并恢复本方气血。';
        this.strParticleEffect = '24127';
        this.nFaXi = SkillConfig.EMagicType.ThreeCorpse;
        this.vecLevelExp = [];
    }

    GetLevelData(nLevel) {
        return {
            nTargetCnt: 1
        }
    }

    GetDetail() {
        return `极厉害的蛊虫，可以吸取对方的鲜血并传送给己方。可将造成伤害的300%化为己方所用，目标人数1人。`;
    }
}

//--------------------- 吸星大法  ---------------//
class CXiXingDaFa extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.XiXingDaFa;
        this.strName = '吸星大法';
        this.strIcon = '1032';
        this.strDesc = '攻击多目标并恢复本方气血。';
        this.strParticleEffect = '24228';
        this.nFaXi = SkillConfig.EMagicType.ThreeCorpse;
        this.nScale = SkillConfig.AffectType.Group;
        this.bEffectPos = SkillConfig.EffectPos.Stage;
        this.vecLevelExp = [600, 5700];
    }

    GetLevelData(nLevel) {
        return {
            nTargetCnt: Math.min(5, Math.floor(3 * (1 + Math.pow(nLevel, 0.3) * 5 / 100)))
        }
    }

    GetDetail() {
        let stLevelData = this.GetLevelData(this.nCurExp);
        return `放出尸蛊之虫，蚕食对方多个单位的生命，并化为己用。可将造成伤害的300%化为己方所用，目标人数${stLevelData.nTargetCnt}人。`;
    }
}

//--------------------- 落日熔金  ---------------//
class CLuoRiRongJin extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.LuoRiRongJin;
        this.strName = '落日熔金';
        this.strIcon = '1029';
        this.strDesc = '鬼火攻击单目标。';
        this.strParticleEffect = '24129';
        this.nFaXi = SkillConfig.EMagicType.GhostFire;
        this.vecLevelExp = [];
    }

    GetLevelData(nLevel) {
        let nCurGrade = this.GetCurGrade();
        return {
            nTargetCnt: 1,
            nHurt: Math.floor(65 * nCurGrade * (Math.pow(nLevel, 0.4) * 2.8853998118144273 / 100 + 1))
        }
    }

    GetDetail() {
        let stLevelData = this.GetLevelData(this.nCurExp);
        return `熊熊冥火，烈血残阳，映入耳目，所到之处，尽染血色，目标人数${stLevelData.nTargetCnt}人。(基础伤害${stLevelData.nHurt})`;
    }
}

//--------------------- 血海深仇  ---------------//
class CXueHaiShenChou extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.XueHaiShenChou;
        this.strName = '血海深仇';
        this.strIcon = '1033';
        this.strDesc = '鬼火攻击多目标。';
        this.strParticleEffect = '24230';
        this.nFaXi = SkillConfig.EMagicType.GhostFire;
        this.nScale = SkillConfig.AffectType.Group;
        this.bEffectPos = SkillConfig.EffectPos.Stage;
        this.vecLevelExp = [600, 5700];
    }

    GetLevelData(nLevel) {
        let nCurGrade = this.GetCurGrade();
        return {
            nTargetCnt: Math.min(5, Math.floor(3 * (1 + Math.pow(nLevel, 0.3) * 5 / 100))),
            nHurt: Math.floor(60 * nCurGrade * (Math.pow(nLevel, 0.4) * 2.8853998118144273 / 100 + 1))
        }
    }

    GetDetail() {
        let stLevelData = this.GetLevelData(this.nCurExp);
        return `血海深仇，引燃一切，对目标造成大量伤害，目标人数${stLevelData.nTargetCnt}人。(基础伤害${stLevelData.nHurt})`;
    }
}

//--------------------- 失心疯  ---------------//
class CShiXinFeng extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.ShiXinFeng;
        this.strName = '失心疯';
        this.strIcon = '1030';
        this.strDesc = '使单目标遗忘技能。';
        this.strParticleEffect = '24131';
        this.strBuffEffect = 'wang';
        this.nBuffEffectY = 100;
        this.nFaXi = SkillConfig.EMagicType.Forget;
        this.vecLevelExp = [200, 1900, 7100, 18500];
    }

    GetLevelData(nLevel) {
        return {
            nTargetCnt: 1,
            nRound: Math.floor(3 * (1 + Math.pow(nLevel, 0.3) * 7 / 100))
        }
    }

    GetDetail() {
        let stLevelData = this.GetLevelData(this.nCurExp);
        return `鬼上身，使敌方丧心病狂。每个技能都有85%几率被遗忘，无法使用物品，目标人数${stLevelData.nTargetCnt}人，持续${stLevelData.nRound}个回合。`;
    }
}

//--------------------- 孟婆汤  ---------------//
class CMengPoTang extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.MengPoTang;
        this.strName = '孟婆汤';
        this.strIcon = '1034';
        this.strDesc = '使多目标遗忘技能。';
        this.strParticleEffect = '24232';
        this.strBuffEffect = 'wang';
        this.nBuffEffectY = 100;
        this.nFaXi = SkillConfig.EMagicType.Forget;
        this.nScale = SkillConfig.AffectType.Group;
        this.bEffectPos = SkillConfig.EffectPos.Stage;
        this.vecLevelExp = [200, 300, 1700, 1900, 5300, 7100, 11900, 18500];
    }

    GetLevelData(nLevel) {
        return {
            nTargetCnt: Math.min(7, Math.floor(3 * (1 + Math.pow(nLevel, 0.35) * 5 / 100))),
            nRound: Math.floor(3 * (1 + Math.pow(nLevel, 0.3) * 7 / 100))
        }
    }

    GetDetail() {
        let stLevelData = this.GetLevelData(this.nCurExp);
        return `饮下孟婆汤，三生梦断，返生无路。每个技能都有75%几率被遗忘，无法使用物品，目标人数${stLevelData.nTargetCnt}人，持续${stLevelData.nRound}个回合。`;
    }
}
/********************************************************************* */

class ZhangYinDongDu extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.ZhangYinDongDu;
        this.strName = '帐饮东都';
        this.strIcon = SkillConfig.SkillIds.ZhangYinDongDu;;
        this.strDesc = '增加自身HP';
        this.action_type = SkillConfig.BtlActionType.Passive;
    }


    getEffect(level, qinmi, relive) {
        let ret = {}
        ret.add = Math.floor(4500 * (relive * 0.5 + 1) * (Math.pow(level, 0.5) / 10 + Math.pow(qinmi, 0.16666666666666666) * 10 / (100 + relive * 20)))
        return ret;
    }

    GetDetail() {
        return `增加召唤兽气血上限。`;
    }
}

class YuanQuanWanHu extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.YuanQuanWanHu;
        this.strName = '源泉万斛';
        this.strIcon = SkillConfig.SkillIds.YuanQuanWanHu;
        this.strDesc = '增加自身MP';
        this.action_type = SkillConfig.BtlActionType.Passive;
    }


    getEffect(level, qinmi, relive) {
        let ret = {}
        ret.add = Math.floor(4500 * (relive * 0.5 + 1) * (Math.pow(level, 0.5) / 10 + Math.pow(qinmi, 0.16666666666666666) * 10 / (100 + relive * 20)))
        return ret;
    }

    GetDetail() {
        return `增加召唤兽法力上限。`;
    }
}

class ShenGongGuiLi extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.ShenGongGuiLi;
        this.strName = '神工鬼力';
        this.strIcon = SkillConfig.SkillIds.ShenGongGuiLi;
        this.strDesc = '增加自身ATK';
        this.action_type = SkillConfig.BtlActionType.Passive;
    }


    getEffect(level, qinmi, relive) {
        let ret = {}
        ret.add = Math.floor(1875 * (relive * 0.5 + 1) * (Math.pow(level, 0.5) / 10 + Math.pow(qinmi, 0.16666666666666666) * 10 / (100 + relive * 20)))
        return ret;
    }

    GetDetail() {
        return `增加召唤兽攻击上限。`;
    }
}

class BeiDaoJianXing extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.BeiDaoJianXing;
        this.strName = '倍道兼行';
        this.strIcon = SkillConfig.SkillIds.BeiDaoJianXing;
        this.strDesc = '增加自身SPD';
        this.action_type = SkillConfig.BtlActionType.Passive;
    }


    getEffect(level, qinmi, relive) {
        let ret = {}
        ret.add = Math.floor(38 * (relive * 0.3 + 1) * (Math.pow(level, 0.5) / 10 + Math.pow(qinmi, 0.16666666666666666) * 10 / (100 + relive * 20)))
        return ret;
    }

    GetDetail() {
        return `增加召唤兽速度上限。`;
    }
}

class PanShan extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.PanShan;
        this.strName = '蹒跚';
        this.strIcon = SkillConfig.SkillIds.PanShan;
        this.strDesc = '减少自身SPD';
        this.action_type = SkillConfig.BtlActionType.Passive;
    }


    getEffect(level, qinmi, relive) {
        let ret = {}
        ret.reduce = Math.floor(38 * (relive * 0.3 + 1) * (Math.pow(level, 0.5) / 10 + Math.pow(qinmi, 0.16666666666666666) * 10 / (100 + relive * 20)))
        return ret;
    }

    GetDetail() {
        return `减少召唤兽速度上限。`;
    }
}


class HighZhangYinDongDu extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.HighZhangYinDongDu;
        this.strName = '高级帐饮东都';
        this.strIcon = SkillConfig.SkillIds.ZhangYinDongDu;;
        this.strDesc = '增加自身HP';
        this.action_type = SkillConfig.BtlActionType.Passive;
    }


    getEffect(level, qinmi, relive) {
        let ret = {}
        ret.add = Math.floor(6000 * (relive * 0.5 + 1) * (Math.pow(level, 0.5) / 10 + Math.pow(qinmi, 0.16666666666666666) * 10 / (100 + relive * 20)))
        return ret;
    }

    GetDetail() {
        return `增加召唤兽气血上限。`;
    }
}

class HighYuanQuanWanHu extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.HighYuanQuanWanHu;
        this.strName = '高级源泉万斛';
        this.strIcon = SkillConfig.SkillIds.YuanQuanWanHu;
        this.strDesc = '增加自身MP';
        this.action_type = SkillConfig.BtlActionType.Passive;
    }


    getEffect(level, qinmi, relive) {
        let ret = {}
        ret.add = Math.floor(6000 * (relive * 0.5 + 1) * (Math.pow(level, 0.5) / 10 + Math.pow(qinmi, 0.16666666666666666) * 10 / (100 + relive * 20)))
        return ret;
    }

    GetDetail() {
        return `增加召唤兽法力上限。`;
    }
}

class HighShenGongGuiLi extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.HighShenGongGuiLi;
        this.strName = '高级神工鬼力';
        this.strIcon = SkillConfig.SkillIds.ShenGongGuiLi;
        this.strDesc = '增加自身ATK';
        this.action_type = SkillConfig.BtlActionType.Passive;
    }


    getEffect(level, qinmi, relive) {
        let ret = {}
        ret.add = Math.floor(2500 * (relive * 0.5 + 1) * (Math.pow(level, 0.5) / 10 + Math.pow(qinmi, 0.16666666666666666) * 10 / (100 + relive * 20)))
        return ret;
    }

    GetDetail() {
        return `增加召唤兽攻击上限。`;
    }
}

class HighBeiDaoJianXing extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.HighBeiDaoJianXing;
        this.strName = '高级倍道兼行';
        this.strIcon = SkillConfig.SkillIds.BeiDaoJianXing;
        this.strDesc = '增加自身SPD';
        this.action_type = SkillConfig.BtlActionType.Passive;
    }


    getEffect(level, qinmi, relive) {
        let ret = {}
        ret.add = Math.floor(50 * (relive * 0.3 + 1) * (Math.pow(level, 0.5) / 10 + Math.pow(qinmi, 0.16666666666666666) * 10 / (100 + relive * 20)))
        return ret;
    }

    GetDetail() {
        return `增加召唤兽速度上限。`;
    }
}

class HighPanShan extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.HighPanShan;
        this.strName = '高级蹒跚';
        this.strIcon = SkillConfig.SkillIds.PanShan;
        this.strDesc = '减少自身SPD';
        this.action_type = SkillConfig.BtlActionType.Passive;
    }

    getEffect(level, qinmi, relive) {
        let ret = {}
        ret.reduce = Math.floor(50 * (relive * 0.3 + 1) * (Math.pow(level, 0.5) / 10 + Math.pow(qinmi, 0.16666666666666666) * 10 / (100 + relive * 20)))
        return ret;
    }

    GetDetail() {
        return `减少召唤兽速度上限。`;
    }
}

class GongXingTianFa extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.GongXingTianFa;
        this.strName = '恭行天罚';
        this.strIcon = SkillConfig.SkillIds.GongXingTianFa;
        this.strDesc = '用神威的景象激发自身的战意。召唤兽自身提高命中和狂暴率。';
        this.action_type = SkillConfig.BtlActionType.Passive;
    }


    getEffect(level, qinmi, relive) {
        let ret = {}
        ret.reduce = Math.floor(50 * (relive * 0.3 + 1) * (Math.pow(level, 0.5) / 10 + Math.pow(qinmi, 0.16666666666666666) * 10 / (100 + relive * 20)))
        return ret;
    }

    GetDetail() {
        return `召唤兽自身命中率和狂暴率。`;
    }
}

class TianGangZhanQi extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.TianGangZhanQi;
        this.strName = '天罡战气';
        this.strIcon = SkillConfig.SkillIds.TianGangZhanQi;
        this.strDesc = '处于混乱状态时必定攻击敌方目标，如果敌方无目标则待机。攻击敌方时，有几率清除目标正面状态。';
        this.action_type = SkillConfig.BtlActionType.Passive;
    }


    getEffect(level, qinmi, relive) {
        let ret = {}
        ret.add = Math.round(0.1 + 13 * (relive * 0.5 + 1) * (Math.pow(level, 0.5) / 10 + Math.pow(qinmi, 0.16666666666666666) * 10 / (100 + relive * 20)))
        return ret;
    }

    GetDetail() {
        return `处于混乱状态时必定攻击敌方目标，如果敌方无目标则待机。攻击敌方时，有几率清除目标正面状态。`;
    }
}

class BingLinChengXia extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.BingLinChengXia;
        this.strName = '兵临城下';
        this.strIcon = SkillConfig.SkillIds.BingLinChengXia;
        this.strDesc = '使用技能后的当回合物理攻击中，攻击力提高到2.5倍，需消耗50%最大生命值和20%最大法力值发动。';
        this.strParticleEffect = '31205';
        this.atkType = SkillConfig.AttackType.Melee;
    }

    GetDetail() {
        return `使用技能后的当回合物理攻击中，攻击力提高到2.5倍，需消耗50%最大生命值和20%最大法力值发动。`;
    }
}

class NiePan extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.NiePan;
        this.strName = '涅槃';
        this.strIcon = SkillConfig.SkillIds.NiePan;
        this.strDesc = '神兽死亡时在该回合有30%几率全血无异常状态复活1次。';
        this.action_type = SkillConfig.BtlActionType.Passive;
        this.strParticleEffect = '31206';
    }

    GetDetail() {
        return `神兽死亡时在该回合有30%几率全血无异常状态复活1次。`;
    }
}

class QiangHuaXuanRen extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.QiangHuaXuanRen;
        this.strName = '强化悬刃';
        this.strIcon = SkillConfig.SkillIds.QiangHuaXuanRen;
        this.strDesc = '悬刃加强版，敌方第一个施法单位施法前对其造成气血伤害，此技能只生效一次。';
        this.action_type = SkillConfig.BtlActionType.Passive;
    }

    GetDetail() {
        return `悬刃加强版，敌方第一个施法单位施法前对其造成气血伤害，此技能只生效一次。（仅PVP生效）`;
    }
}

class QiangHuaYiHuan extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.QiangHuaYiHuan;
        this.strName = '强化遗患';
        this.strIcon = SkillConfig.SkillIds.QiangHuaYiHuan;
        this.strDesc = '遗患加强版，敌方第一个施法单位施法前对其造成法力伤害，此技能只生效一次。';
        this.action_type = SkillConfig.BtlActionType.Passive;
    }

    GetDetail() {
        return `遗患加强版，敌方第一个施法单位施法前对其造成法力伤害，此技能只生效一次。（仅PVP生效）`;
    }
}


class XuanRen extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.XuanRen;
        this.strName = '悬刃';
        this.strIcon = SkillConfig.SkillIds.XuanRen;
        this.strDesc = '敌方第一个施法单位施法前对其造成气血伤害，此技能只生效一次。';
        this.action_type = SkillConfig.BtlActionType.Passive;
    }

    GetDetail() {
        return `敌方第一个施法单位施法前对其造成气血伤害，此技能只生效一次。（仅PVP生效）`;
    }
}

class YiHuan extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.YiHuan;
        this.strName = '遗患';
        this.strIcon = SkillConfig.SkillIds.YiHuan;
        this.strDesc = '敌方第一个施法单位施法前对其造成法力伤害，此技能只生效一次。';
        this.action_type = SkillConfig.BtlActionType.Passive;
    }

    GetDetail() {
        return `敌方第一个施法单位施法前对其造成法力伤害，此技能只生效一次。（仅PVP生效）`;
    }
}

class ChaoMingDianChe extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.ChaoMingDianChe;
        this.strName = '潮鸣电掣';
        this.strIcon = SkillConfig.SkillIds.ChaoMingDianChe;
        this.strDesc = '倍道兼行加强版，增加召唤兽速度。';
        this.action_type = SkillConfig.BtlActionType.Passive;
    }

    getEffect(level, qinmi, relive) {
        let ret = {}
        ret.add = Math.floor(75 * (relive * 0.3 + 1) * (Math.pow(level, 0.5) / 10 + Math.pow(qinmi, 0.16666666666666666) * 10 / (100 + relive * 20)))
        return ret;
    }

    GetDetail() {
        return `使召唤兽如潮鸣电掣一般，增加召唤兽速度`;
    }
}

class RuHuTianYi extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.RuHuTianYi;
        this.strName = '如虎添翼';
        this.strIcon = SkillConfig.SkillIds.RuHuTianYi;
        this.strDesc = '召唤兽出场瞬间，提高持有者和召唤兽本身5点人法抗性、15点仙法抗性和15点物理抗性，能和女魔盘叠加，有效2回合。';
        this.action_type = SkillConfig.BtlActionType.Passive;
        this.strBuffEffect = 'huyi';
        this.nBuffEffectY = 10;
    }

    GetDetail() {
        return `召唤兽出场瞬间，提高持有者和召唤兽本身5点人法抗性、15点仙法抗性和15点物理抗性，能和女魔盘叠加，有效2回合。`;
    }
}

class ShanXian extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.ShanXian;
        this.strName = '闪现';
        this.strIcon = SkillConfig.SkillIds.ShanXian;
        this.strDesc = '当召唤兽死亡离场时，有25%几率无需召唤自动加入战斗。';
        this.action_type = SkillConfig.BtlActionType.Passive;
    }

    GetDetail() {
        return `当前召唤兽死亡离场时，此召唤兽有25%几率无需召唤自动加入战斗。`;
    }
}

class HighShanXian extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.HighShanXian;
        this.strName = '高级闪现';
        this.strIcon = SkillConfig.SkillIds.ShanXian;
        this.strDesc = '当召唤兽死亡离场时，有35%几率无需召唤自动加入战斗。';
        this.action_type = SkillConfig.BtlActionType.Passive;
    }

    GetDetail() {
        return `当前召唤兽死亡离场时，此召唤兽有35%几率无需召唤自动加入战斗。`;
    }
}

class YinShen extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.YinShen;
        this.strName = '隐身';
        this.strIcon = SkillConfig.SkillIds.YinShen;
        this.strDesc = '进入战斗后立刻隐身3回合（第一回合除外），与击其不意冲突。';
        this.action_type = SkillConfig.BtlActionType.Passive;
        this.strBuffEffect = 'yinshen';
        this.nFaXi = SkillConfig.EMagicType.YinShen;
    }

    GetDetail() {
        return `进入战斗后立刻隐身3回合（第一回合除外），与击其不意冲突。`;
    }
}

class ZiXuWuYou extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.ZiXuWuYou;
        this.strName = '子虚乌有';
        this.strIcon = SkillConfig.SkillIds.ZiXuWuYou;
        this.strDesc = '隐身自己和某一友方单位。';
        this.strBuffEffect = 'yinshen';
        this.nFaXi = SkillConfig.EMagicType.YinShen;
        this.strParticleEffect = '2700';
        this.skillActOn = SkillConfig.ActOn.Self;
    }

    GetDetail() {
        return `自己与己方任一单位同时隐身，持续3回合，冷却时间5回合。`;
    }
}



class StealMoney extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.StealMoney;
        this.strName = '飞龙探云手';
        this.strIcon = SkillConfig.SkillIds.StealMoney;
        this.strDesc = '偷钱偷钱';
        this.strParticleEffect = '0';
        this.nFaXi = SkillConfig.EMagicType.Physics;
        this.atkType = SkillConfig.AttackType.Melee;
    }

    GetDetail() {
        return `偷钱偷钱`;
    }
}

class ChuiJinZhuanYu extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.ChuiJinZhuanYu;
        this.strName = '炊金馔玉';
        this.strIcon = SkillConfig.SkillIds.ChuiJinZhuanYu;
        this.strDesc = '提升敌方全体金五行50。';

        this.strParticleEffect = '32244'; //特效
        this.strBuffEffect = '32244_s'; // buff特效名字
        this.nSkillEffectY = -15;
        this.nBuffEffectY = -30; //buff特效 显示位置
        this.nScale = SkillConfig.AffectType.Group; //使用范围，单攻/群攻
        this.nBuffFront = false;
    }

    GetDetail() {
        return `使用后提高敌方全体金五行50，持续3回合，冷却时间5回合。`;
    }
}


class KuMuFengChun extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.KuMuFengChun;
        this.strName = '枯木逢春';
        this.strIcon = SkillConfig.SkillIds.KuMuFengChun;
        this.strDesc = '提升敌方全体木五行50。';

        this.strParticleEffect = '32245'; //特效
        this.strBuffEffect = '32245_s'; // buff特效名字
        this.nSkillEffectY = -15;
        this.nBuffEffectY = -30; //buff特效 显示位置
        this.nBuffFront = false;
    }

    GetDetail() {
        return `使用后提高敌方全体木五行50，持续3回合，冷却时间5回合。`;
    }
}


class XiTianJingTu extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.XiTianJingTu;
        this.strName = '西天净土';
        this.strIcon = SkillConfig.SkillIds.XiTianJingTu;
        this.strDesc = '提升敌方全体土五行50。';

        this.strParticleEffect = '32248'; //特效
        this.strBuffEffect = '32248_s'; // buff特效名字
        this.nSkillEffectY = -15;
        this.nBuffEffectY = -30; //buff特效 显示位置
        this.nScale = SkillConfig.AffectType.Group; //使用范围，单攻/群攻
        this.nBuffFront = false;
    }

    GetDetail() {
        return `使用后提高敌方全体土五行50，持续3回合，冷却时间5回合。`;
    }
}


class RuRenYinShui extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.RuRenYinShui;
        this.strName = '如人饮水';
        this.strIcon = SkillConfig.SkillIds.RuRenYinShui;
        this.strDesc = '提升敌方全体水五行50。';

        this.strParticleEffect = '32246'; //特效
        this.strBuffEffect = '32246_s'; // buff特效名字
        this.nSkillEffectY = -15;
        this.nBuffEffectY = -30; //buff特效 显示位置
        this.nScale = SkillConfig.AffectType.Group; //使用范围，单攻/群攻
        this.nBuffFront = false;
    }

    GetDetail() {
        return `使用后提高敌方全体水五行50，持续3回合，冷却时间5回合。`;
    }
}


class FengHuoLiaoYuan extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.FengHuoLiaoYuan;
        this.strName = '风火燎原';
        this.strIcon = SkillConfig.SkillIds.FengHuoLiaoYuan;
        this.strDesc = '提升敌方全体火五行50。';

        this.strParticleEffect = '32247'; //特效
        this.strBuffEffect = '32247_s'; // buff特效名字
        this.nSkillEffectY = -15;
        this.nBuffEffectY = -30; //buff特效 显示位置
        this.nBuffFront = false;
    }

    GetDetail() {
        return `使用后提高敌方全体火五行50，持续3回合，冷却时间5回合。`;
    }
}

class HuaWu extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.HuaWu;
        this.strName = '化无';
        this.strParticleEffect = '2701'; //特效
        this.strIcon = SkillConfig.SkillIds.HuaWu;
        this.strDesc = '取消第一个敌方法术，此技能只生效一次。';
        this.action_type = SkillConfig.BtlActionType.Passive;
    }

    GetDetail() {
        return `取消第一个敌方法术，此技能只生效一次。（仅PVP生效）`;
    }
}


class JueJingFengSheng extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.JueJingFengSheng;
        this.strName = '绝境逢生';
        this.strIcon = SkillConfig.SkillIds.JueJingFengSheng;
        this.strParticleEffect = '2702'; //特效
        this.strDesc = '恢复全体队友气血和法力。';
        this.skillActOn = SkillConfig.ActOn.Self;
    }

    GetDetail() {
        return `给己方所有单位回复60%的气血与60%的法力，每场战斗只能使用1次。（前5回合不可使用 ）`;
    }
}

class MiaoShouHuiChun extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.MiaoShouHuiChun;
        this.strName = '妙手回春';
        this.strIcon = SkillConfig.SkillIds.MiaoShouHuiChun;
        this.strParticleEffect = '2702'; //特效
        this.strDesc = '恢复3个队友气血和法力。';
        this.skillActOn = SkillConfig.ActOn.Self;
    }

    GetDetail() {
        return `给己方3个队友回复50%的气血与50%的法力，每场战斗只能使用1次。（前5回合不可使用 ）`;
    }
}

class FeiLongZaiTian extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.FeiLongZaiTian;
        this.strName = '飞龙在天';
        this.strIcon = SkillConfig.SkillIds.FeiLongZaiTian;
        this.strDesc = '法术攻击3个单位。';
        this.action_type = SkillConfig.BtlActionType.Passive;
    }

    GetDetail() {
        return `青龙指定使用风雷水火法术攻击3个目标单位，伤害效果与召唤兽等级、亲密以及法力上限有关。`;
    }
}

class FeiLongZaiTian_Feng extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.FeiLongZaiTian_Feng;
        this.strName = '飞龙在天-风';
        this.strIcon = SkillConfig.SkillIds.FengLeiYongDong;
        this.strDesc = '法术攻击3个单位。';
        this.strParticleEffect = '22111';
        this.nFaXi = SkillConfig.EMagicType.Wind;
    }

    GetDetail() {
        return `青龙指定使用风雷水火法术攻击3个目标单位，伤害效果与召唤兽等级、亲密以及法力上限有关。`;
    }
}

class FeiLongZaiTian_Huo extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.FeiLongZaiTian_Huo;
        this.strName = '飞龙在天-火';
        this.strIcon = SkillConfig.SkillIds.LieHuoJiaoYang;
        this.strDesc = '法术攻击3个单位。';
        this.strParticleEffect = '22109';
        this.nFaXi = SkillConfig.EMagicType.Fire;
    }

    GetDetail() {
        return `青龙指定使用风雷水火法术攻击3个目标单位，伤害效果与召唤兽等级、亲密以及法力上限有关。`;
    }
}

class FeiLongZaiTian_Shui extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.FeiLongZaiTian_Shui;
        this.strName = '飞龙在天-水';
        this.strIcon = SkillConfig.SkillIds.JiaoLongChuHai;
        this.strDesc = '法术攻击3个单位。';
        this.strParticleEffect = '22115';
        this.nFaXi = SkillConfig.EMagicType.Water;
    }

    GetDetail() {
        return `青龙指定使用风雷水火法术攻击3个目标单位，伤害效果与召唤兽等级、亲密以及法力上限有关。`;
    }
}

class FeiLongZaiTian_Lei extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.FeiLongZaiTian_Lei;
        this.strName = '飞龙在天-雷';
        this.strIcon = SkillConfig.SkillIds.DianShanLeiMing;
        this.strDesc = '法术攻击3个单位。';
        this.strParticleEffect = '22113';
        this.nSkillEffectY = 40;
        this.nFaXi = SkillConfig.EMagicType.Thunder;
    }

    GetDetail() {
        return `青龙指定使用风雷水火法术攻击3个目标单位，伤害效果与召唤兽等级、亲密以及法力上限有关。`;
    }
}


class YouFengLaiYi extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.YouFengLaiYi;
        this.strName = '有凤来仪';
        this.strIcon = SkillConfig.SkillIds.YouFengLaiYi;
        this.strDesc = '提升目标任意五行。';
        this.action_type = SkillConfig.BtlActionType.Passive;
    }

    GetDetail() {
        return `朱雀运用五行之力可使敌方全体目标任意五行大幅提升，持续3回合，冷却时间5回合。`;
    }
}

class YouFengLaiYi_Jin extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.YouFengLaiYi_Jin;
        this.strName = '高级炊金馔玉';
        this.strIcon = SkillConfig.SkillIds.ChuiJinZhuanYu;
        this.strDesc = '提升敌方全体金五行';

        this.strParticleEffect = '32244'; //特效
        this.strBuffEffect = '32244_s'; // buff特效名字
        this.nSkillEffectY = -15;
        this.nBuffEffectY = -30; //buff特效 显示位置
        this.nScale = SkillConfig.AffectType.Group; //使用范围，单攻/群攻
        this.nBuffFront = false;
    }

    GetDetail() {
        return `使用后提高敌方全体金五行，持续3回合，冷却时间5回合。`;
    }
}


class YouFengLaiYi_Mu extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.YouFengLaiYi_Mu;
        this.strName = '高级枯木逢春';
        this.strIcon = SkillConfig.SkillIds.KuMuFengChun;
        this.strDesc = '提升敌方全体木五行。';

        this.strParticleEffect = '32245'; //特效
        this.strBuffEffect = '32245_s'; // buff特效名字
        this.nSkillEffectY = -15;
        this.nBuffEffectY = -30; //buff特效 显示位置
        this.nBuffFront = false;
    }

    GetDetail() {
        return `使用后提高敌方全体木五行，持续3回合，冷却时间5回合。`;
    }
}


class YouFengLaiYi_Shui extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.YouFengLaiYi_Shui;
        this.strName = '高级如人饮水';
        this.strIcon = SkillConfig.SkillIds.RuRenYinShui;
        this.strDesc = '提升敌方全体水五行。';

        this.strParticleEffect = '32246'; //特效
        this.strBuffEffect = '32246_s'; // buff特效名字
        this.nSkillEffectY = -15;
        this.nBuffEffectY = -30; //buff特效 显示位置
        this.nScale = SkillConfig.AffectType.Group; //使用范围，单攻/群攻
        this.nBuffFront = false;
    }

    GetDetail() {
        return `使用后提高敌方全体水五行，持续3回合，冷却时间5回合。`;
    }
}

class YouFengLaiYi_Huo extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.YouFengLaiYi_Huo;
        this.strName = '高级风火燎原';
        this.strIcon = SkillConfig.SkillIds.FengHuoLiaoYuan;
        this.strDesc = '提升敌方全体火五行。';

        this.strParticleEffect = '32247'; //特效
        this.strBuffEffect = '32247_s'; // buff特效名字
        this.nSkillEffectY = -15;
        this.nBuffEffectY = -30; //buff特效 显示位置
        this.nBuffFront = false;
    }

    GetDetail() {
        return `使用后提高敌方全体火五行，持续3回合，冷却时间5回合。`;
    }
}

class YouFengLaiYi_Tu extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.YouFengLaiYi_Tu;
        this.strName = '高级西天净土';
        this.strIcon = SkillConfig.SkillIds.XiTianJingTu;
        this.strDesc = '提升敌方全体土五行。';

        this.strParticleEffect = '32248'; //特效
        this.strBuffEffect = '32248_s'; // buff特效名字
        this.nSkillEffectY = -15;
        this.nBuffEffectY = -30; //buff特效 显示位置
        this.nScale = SkillConfig.AffectType.Group; //使用范围，单攻/群攻
        this.nBuffFront = false;
    }

    GetDetail() {
        return `使用后提高敌方全体土五行，持续3回合，冷却时间5回合。`;
    }
}

class FenHuaFuLiu extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.FenHuaFuLiu;
        this.strName = '分花拂柳'; // 技能名字
        this.strIcon = SkillConfig.SkillIds.FenHuaFuLiu;
        this.strDesc = '物理攻击消灭一个单位时，有几率继续追击下个单位，最多追击3个单位。'; //技能介绍

        this.strParticleEffect = ''; //特效
        this.bEffectPos = SkillConfig.EffectPos.Body; // 
        // this.nSkillEffectX = 0; // 技能特效 显示修正位置
        // this.nSkillEffectY = 0; // 技能特效 显示修正位置
        this.action_type = SkillConfig.BtlActionType.Passive; // 主动 被动
    }

    GetDetail() {
        return `物理攻击消灭一个单位时，有几率继续追击下个单位，最多追击3个单位。`;
    }
}

class FenLieGongJi extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.FenLieGongJi;
        this.strName = '分裂攻击'; // 技能名字
        this.strIcon = SkillConfig.SkillIds.FenLieGongJi;
        this.strDesc = '物理攻击时有15%几率增加一个目标。'; //技能介绍
        this.action_type = SkillConfig.BtlActionType.Passive; // 主动 被动
    }

    GetDetail() {
        return `物理攻击时有15%几率增加一个目标。`;
    }
}

class HighFenLieGongJi extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.HighFenLieGongJi;
        this.strName = '高级分裂攻击'; // 技能名字
        this.strIcon = SkillConfig.SkillIds.HighFenLieGongJi;
        this.strDesc = '物理攻击时有30%几率增加一个目标。'; //技能介绍
        this.action_type = SkillConfig.BtlActionType.Passive; // 主动 被动
    }

    GetDetail() {
        return `物理攻击时有30%几率增加一个目标。`;
    }
}


class GeShanDaNiu extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.GeShanDaNiu;
        this.strName = '隔山打牛'; // 技能名字
        this.strIcon = SkillConfig.SkillIds.GeShanDaNiu;
        this.strDesc = '物理攻击时25%概率对周围的某个目标造成攻击伤害。'; //技能介绍
        this.action_type = SkillConfig.BtlActionType.Passive; // 主动 被动
    }

    GetDetail() {
        return `物理攻击时25%概率对周围的某个目标造成攻击伤害。`;
    }
}

class HighGeShanDaNiu extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.HighGeShanDaNiu;
        this.strName = '高级隔山打牛'; // 技能名字
        this.strIcon = SkillConfig.SkillIds.GeShanDaNiu;
        this.strDesc = '物理攻击时35%几率对周围的某个目标造成伤害。'; //技能介绍
        this.action_type = SkillConfig.BtlActionType.Passive; // 主动 被动
    }

    GetDetail() {
        return `物理攻击时35%几率对周围的某个目标造成伤害。`;
    }
}

class TianMoJieTi extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.TianMoJieTi;
        this.strName = '天魔解体'; // 技能名字
        this.strIcon = SkillConfig.SkillIds.TianMoJieTi;
        this.strDesc = '牺牲自身气血当前值的95%，对目标造成气血伤害。'; //技能介绍
        this.strParticleEffect = SkillConfig.SkillIds.TianMoJieTi;
        this.bEffectPos = SkillConfig.EffectPos.Body; // 
        this.atkType = SkillConfig.AttackType.Remote; //近身|远程
        this.action_type = SkillConfig.BtlActionType.Initiative; // 主动 被动
    }

    GetDetail() {
        return `牺牲自身气血当前值的95%，对目标造成法力伤害。`;
    }
}

class HighTianMoJieTi extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.HighTianMoJieTi;
        this.strName = '高级天魔解体'; // 技能名字
        this.strIcon = SkillConfig.SkillIds.TianMoJieTi;
        this.strDesc = '牺牲自身气血当前值的95%，对目标造成气血伤害。'; //技能介绍
        this.strParticleEffect = SkillConfig.SkillIds.TianMoJieTi;
        this.bEffectPos = SkillConfig.EffectPos.Body; // 
        this.atkType = SkillConfig.AttackType.Remote; //近身|远程
        this.action_type = SkillConfig.BtlActionType.Initiative; // 主动 被动
    }

    GetDetail() {
        return `牺牲自身气血当前值的95%，对目标造成法力伤害。`;
    }
}


class FenGuangHuaYing extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.FenGuangHuaYing;
        this.strName = '分光化影'; // 技能名字
        this.strIcon = SkillConfig.SkillIds.FenGuangHuaYing;
        this.strDesc = '牺牲自身气血当前值的95%，对目标造成法力伤害。'; //技能介绍
        this.strParticleEffect = SkillConfig.SkillIds.FenGuangHuaYing;
        this.bEffectPos = SkillConfig.EffectPos.Body; // 
        this.atkType = SkillConfig.AttackType.Remote; //近身|远程
        this.action_type = SkillConfig.BtlActionType.Initiative; // 主动 被动
    }

    GetDetail() {
        return `牺牲自身气血当前值的95%，对目标造成法力伤害。`;
    }
}

class HighFenGuangHuaYing extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.HighFenGuangHuaYing;
        this.strName = '高级分光化影'; // 技能名字
        this.strIcon = SkillConfig.SkillIds.FenGuangHuaYing;
        this.strDesc = '牺牲自身气血当前值的95%，对目标造成法力伤害。'; //技能介绍
        this.strParticleEffect = SkillConfig.SkillIds.FenGuangHuaYing;
        this.bEffectPos = SkillConfig.EffectPos.Body; // 
        this.atkType = SkillConfig.AttackType.Remote; //近身|远程
        this.action_type = SkillConfig.BtlActionType.Initiative; // 主动 被动
    }

    GetDetail() {
        return `牺牲自身气血当前值的95%，对目标造成法力伤害。`;
    }
}

class QingMianLiaoYa extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.QingMianLiaoYa;
        this.strName = '青面獠牙'; // 技能名字
        this.strIcon = SkillConfig.SkillIds.QingMianLiaoYa;
        this.strDesc = '牺牲自身法力当前值的95%，对目标造成气血伤害。'; //技能介绍
        this.strParticleEffect = SkillConfig.SkillIds.QingMianLiaoYa;
        this.bEffectPos = SkillConfig.EffectPos.Body; // 
        this.atkType = SkillConfig.AttackType.Remote; //近身|远程
        this.action_type = SkillConfig.BtlActionType.Initiative; // 主动 被动
    }

    GetDetail() {
        return `牺牲自身法力当前值的95%，对目标造成气血伤害。`;
    }
}

class HighQingMianLiaoYa extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.HighQingMianLiaoYa;
        this.strName = '高级青面獠牙'; // 技能名字
        this.strIcon = SkillConfig.SkillIds.QingMianLiaoYa;
        this.strDesc = '牺牲自身法力当前值的95%，对目标造成气血伤害。'; //技能介绍
        this.strParticleEffect = SkillConfig.SkillIds.QingMianLiaoYa;
        this.bEffectPos = SkillConfig.EffectPos.Body; // 
        this.atkType = SkillConfig.AttackType.Remote; //近身|远程
        this.action_type = SkillConfig.BtlActionType.Initiative; // 主动 被动
    }

    GetDetail() {
        return `牺牲自身法力当前值的95%，对目标造成气血伤害。`;
    }
}


class XiaoLouYeKu extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.XiaoLouYeKu;
        this.strName = '小楼夜哭'; // 技能名字
        this.strIcon = SkillConfig.SkillIds.XiaoLouYeKu;
        this.strDesc = '牺牲自身法力当前值的95%，对目标造成法力伤害。'; //技能介绍
        this.strParticleEffect = SkillConfig.SkillIds.XiaoLouYeKu;
        this.bEffectPos = SkillConfig.EffectPos.Body; // 
        this.atkType = SkillConfig.AttackType.Remote; //近身|远程
        this.action_type = SkillConfig.BtlActionType.Initiative; // 主动 被动
    }

    GetDetail() {
        return `牺牲自身法力当前值的95%，对目标造成法力伤害。`;
    }
}

class HighXiaoLouYeKu extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.HighXiaoLouYeKu;
        this.strName = '高级小楼夜哭'; // 技能名字
        this.strIcon = SkillConfig.SkillIds.XiaoLouYeKu;
        this.strDesc = '牺牲自身法力当前值的95%，对目标造成法力伤害。'; //技能介绍
        this.strParticleEffect = SkillConfig.SkillIds.XiaoLouYeKu;
        this.bEffectPos = SkillConfig.EffectPos.Body; // 
        this.atkType = SkillConfig.AttackType.Remote; //近身|远程
        this.action_type = SkillConfig.BtlActionType.Initiative; // 主动 被动
    }

    GetDetail() {
        return `牺牲自身法力当前值的95%，对目标造成法力伤害。`;
    }
}


class JiQiBuYi extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.JiQiBuYi;
        this.strName = '击其不意'; // 技能名字
        this.strIcon = SkillConfig.SkillIds.JiQiBuYi;
        this.strDesc = '进场时进行一次随机物理攻击。'; //技能介绍
        this.action_type = SkillConfig.BtlActionType.Passive; // 主动 被动
    }

    GetDetail() {
        return `进场时进行一次随机物理攻击。`;
    }
}

class HunLuan extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.HunLuan;
        this.strName = '混乱'; // 技能名字
        this.strIcon = SkillConfig.SkillIds.HunLuan;
        this.strDesc = '物理攻击时有几率释放单体混乱，持续1回合。'; //技能介绍
        this.strBuffEffect = 'luan';
        this.nFaXi = SkillConfig.EMagicType.Chaos;
        this.action_type = SkillConfig.BtlActionType.Passive; // 主动 被动
    }

    GetDetail() {
        return `物理攻击时有几率释放单体混乱，持续1回合。`;
    }
}

class FengYin extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.FengYin;
        this.strName = '封印'; // 技能名字
        this.strIcon = SkillConfig.SkillIds.FengYin;
        this.strDesc = '物理攻击时有几率释放单体封印，持续1回合。'; //技能介绍
        this.strBuffEffect = 'luan';
        this.nFaXi = SkillConfig.EMagicType.Chaos;
        this.action_type = SkillConfig.BtlActionType.Passive; // 主动 被动
    }

    GetDetail() {
        return `物理攻击时有几率释放单体封印，持续1回合。`;
    }
}
/**
技能模版

class xxx extends CBaseSkill {
    constructor() {
        super();
        this.Init();
    }

    Init() {
        this.nID = SkillConfig.SkillIds.YouFengLaiYi_Tu;
        this.strName = ''; // 技能名字
        this.strIcon = SkillConfig.SkillIds.XiTianJingTu;
        this.strDesc = ''; //技能介绍
        this.strParticleEffect = ''; //特效
        this.bEffectDir = false; // 特效是否区分方向
        this.bEffectPos = SkillConfig.EffectPos.Body; // 
        this.strBuffEffect = null; // buff特效名字
        this.nSkillEffectX = 0; // 技能特效 显示修正位置
        this.nSkillEffectY = 0; // 技能特效 显示修正位置
        this.nBuffEffectY = 0; //buff特效 显示位置
        this.nBuffFront = true; // buff显示在人物之前 还是之后
        this.nMaxExp = 10000; //最大熟练度
        this.nCurExp = 0; //当前熟练度
        this.atkType = SkillConfig.AttackType.Normal; //近身|远程
        this.nFaXi = 0; //法系
        this.nScale = SkillConfig.AffectType.Single; //使用范围，单攻/群攻
        this.vecLevelExp = []; //有序vecter，技能等级对应的熟练度
        this.action_type = SkillConfig.BtlActionType.Initiative; // 主动 被动
        this.skillActOn = SkillConfig.ActOn.Enemy;
    }

    GetDetail() {
        return `使用后提高敌方全体土五行，持续3回合，冷却时间5回合。`;
    }
}

 */

class CSkillMgr {
    constructor() {
        this.mapSkill = {};
        this.Init();
    }

    Init() {
        this.AddSkill(new CNormalAttack());
        this.AddSkill(new CNormalDefend());
        this.AddSkill(new CHeDingHong());
        this.AddSkill(new CWanDuGongXin());
        this.AddSkill(new CJieDaoShaRen());
        this.AddSkill(new CShiXinKuangLuan());
        this.AddSkill(new CMiHunZui());
        this.AddSkill(new CBaiRiMian());
        this.AddSkill(new CZuoBiShangGuan());
        this.AddSkill(new CSiMianChuGe());

        this.AddSkill(new CLieHuoJiaoYang());
        this.AddSkill(new CJiuYinChunHuo());
        this.AddSkill(new CFengLeiYongDong());
        this.AddSkill(new CXiuLiQianKun());
        this.AddSkill(new CDianShanLeiMing());
        this.AddSkill(new CTianZhuDiMie());
        this.AddSkill(new CJiaoLongChuHai());
        this.AddSkill(new CJiuLongBingFeng());

        this.AddSkill(new CMoShenHuTi());
        this.AddSkill(new CHanQingMoMo());
        this.AddSkill(new CTianWaiFeiMo());
        this.AddSkill(new CQianKunJieSu());
        this.AddSkill(new CShouWangShenLi());
        this.AddSkill(new CMoShenFuShen());
        this.AddSkill(new CXiaoHunShiGu());
        this.AddSkill(new CYanLuoZhuiMing());

        this.AddSkill(new CQinSiBingWu());
        this.AddSkill(new CQianNvYouHun());
        this.AddSkill(new CXueShaZhiGu());
        this.AddSkill(new CXiXingDaFa());
        this.AddSkill(new CLuoRiRongJin());
        this.AddSkill(new CXueHaiShenChou());
        this.AddSkill(new CShiXinFeng());
        this.AddSkill(new CMengPoTang());



        this.AddSkill(new ZhangYinDongDu());
        this.AddSkill(new YuanQuanWanHu());
        this.AddSkill(new ShenGongGuiLi());
        this.AddSkill(new BeiDaoJianXing());
        this.AddSkill(new PanShan());
        this.AddSkill(new HighZhangYinDongDu());
        this.AddSkill(new HighYuanQuanWanHu());
        this.AddSkill(new HighShenGongGuiLi());
        this.AddSkill(new HighBeiDaoJianXing());
        this.AddSkill(new HighPanShan());

        this.AddSkill(new ChuiJinZhuanYu());
        this.AddSkill(new KuMuFengChun());
        this.AddSkill(new XiTianJingTu());
        this.AddSkill(new RuRenYinShui());
        this.AddSkill(new FengHuoLiaoYuan());

        this.AddSkill(new GongXingTianFa());
        this.AddSkill(new TianGangZhanQi());

        this.AddSkill(new BingLinChengXia());
        this.AddSkill(new NiePan());
        this.AddSkill(new QiangHuaXuanRen());
        this.AddSkill(new QiangHuaYiHuan());
        this.AddSkill(new ChaoMingDianChe());
        this.AddSkill(new RuHuTianYi());

        this.AddSkill(new XuanRen());
        this.AddSkill(new YiHuan());

        this.AddSkill(new ShanXian());
        this.AddSkill(new HighShanXian());
        this.AddSkill(new ShanXian());
        this.AddSkill(new YinShen());
        this.AddSkill(new MiaoShouHuiChun());

        this.AddSkill(new StealMoney());

        this.AddSkill(new ZiXuWuYou());
        this.AddSkill(new HuaWu());
        this.AddSkill(new JueJingFengSheng());

        this.AddSkill(new FeiLongZaiTian());
        this.AddSkill(new FeiLongZaiTian_Feng());
        this.AddSkill(new FeiLongZaiTian_Huo());
        this.AddSkill(new FeiLongZaiTian_Shui());
        this.AddSkill(new FeiLongZaiTian_Lei());

        this.AddSkill(new YouFengLaiYi());
        this.AddSkill(new YouFengLaiYi_Jin());
        this.AddSkill(new YouFengLaiYi_Mu());
        this.AddSkill(new YouFengLaiYi_Shui());
        this.AddSkill(new YouFengLaiYi_Huo());
        this.AddSkill(new YouFengLaiYi_Tu());


        this.AddSkill(new FenHuaFuLiu());
        this.AddSkill(new FenLieGongJi());
        this.AddSkill(new HighFenLieGongJi());
        this.AddSkill(new GeShanDaNiu());
        this.AddSkill(new HighGeShanDaNiu());

        this.AddSkill(new TianMoJieTi());
        this.AddSkill(new FenGuangHuaYing());
        this.AddSkill(new QingMianLiaoYa());
        this.AddSkill(new XiaoLouYeKu());
        this.AddSkill(new HighTianMoJieTi());
        this.AddSkill(new HighFenGuangHuaYing());
        this.AddSkill(new HighQingMianLiaoYa());
        this.AddSkill(new HighXiaoLouYeKu());

        this.AddSkill(new JiQiBuYi());
    }

    AddSkill(stSkill) {
        this.mapSkill[stSkill.nID] = stSkill;
    }

    GetSkillInfo(nID) {
        if (this.mapSkill[nID] == null) {
            return null;
        }
        return this.mapSkill[nID];
    }

    getShenSkill() {
        return [
            this.mapSkill[SkillConfig.SkillIds.BingLinChengXia],
            this.mapSkill[SkillConfig.SkillIds.NiePan],
            this.mapSkill[SkillConfig.SkillIds.QiangHuaXuanRen],
            this.mapSkill[SkillConfig.SkillIds.QiangHuaYiHuan],
            this.mapSkill[SkillConfig.SkillIds.ChaoMingDianChe],
            this.mapSkill[SkillConfig.SkillIds.RuHuTianYi],
        ]
    }
}

let skillmgr = null;
module.exports = (() => {
    if (skillmgr == null) {
        skillmgr = new CSkillMgr();
    }
    return skillmgr;
})();