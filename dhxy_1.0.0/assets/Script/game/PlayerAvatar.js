﻿let GameRes = require('./GameRes');
let CPubFunction = require('./PubFunction');


cc.Class({

    extends: cc.Component,
    properties:{
        PlayerFunUI:cc.Prefab,
    },

    onLoad() {
    },

    PlayerAvatar_Init(info) {
        this.pInfo = info;
    },

    start() {
        cc.find('btnClose', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, 'PlayerAvatar', 'Close', 0));
        cc.find('btnHeadPic', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "PlayerAvatar", 'OpenPlayerFunUI', 0));
        cc.find('btnHeadPic', this.node).getComponent(cc.Sprite).spriteFrame = GameRes.getRoleHead(this.pInfo.resid);
        cc.find('btnHeadPic/level', this.node).getComponent(cc.Label).string = this.pInfo.level;
    },



    OpenPlayerFunUI() {
        let goFunUI = CPubFunction.CreateSubNode(cc.find('Canvas/MainUI'), {
            nX: 0,
            nY: 0
        }, this.PlayerFunUI, 'PlayerFunUI');

        goFunUI.getComponent('PlayerFunUI').PlayerFunUI_Init(this.pInfo);

    },



    Close() {
        this.node.destroy();
    },



});
