﻿class CScreenNoticeMgr {
    constructor() {
        this.vecMsg = [];
        this.goScreenNotice = null;
        this.goRichText = null;
        this.bInUsing = false;
    }

    Reset() {
        this.vecMsg = [];
        this.goScreenNotice = null;
        this.goRichText = null;
        this.bInUsing = false;
    }

    AddNotice(strRichText, bFront) {
        if (bFront == 1) {
            this.ShowOrHiden(false);
            this.vecMsg.splice(0, 0, strRichText);
        }
        else {
            this.vecMsg.push(strRichText);
        }
        this.CheckAndShowNotice();
    }

    OnUpdate(nIndex) {
        // if (!cc.ll || cc.ll.runningSceneName != 'MainScene') return;

        // if (nIndex % 10 == 0) {
        //     this.CheckAndShowNotice();
        // }

        // this.MoveAndHiden();
    }

    ShowOrHiden(bShow) {
        this.bInUsing = bShow;

        this.goScreenNotice.active = true;
        this.goScreenNotice.y = this.bInUsing ? 156 : 2000;
    }

    CheckAndShowNotice() {
        if (this.goScreenNotice == null) {
            this.goScreenNotice = cc.find('Canvas/MainUI/Notice');
            if (null == this.goScreenNotice)
                return;
        }

        if (this.goRichText == null) {
            this.goRichText = cc.find('Canvas/MainUI/Notice/Mask/notice_label');
            if (null == this.goRichText)
                return;
        }

        if (this.bInUsing == true)
            return;

        if (this.vecMsg.length <= 0)
            return;

        this.ShowOrHiden(true);

        this.goRichText.getComponent(cc.RichText).string = this.vecMsg.shift();
        this.goRichText.x = 100;
        this.goRichText.stopAllActions();
        let self = this;
        this.goRichText.runAction(cc.sequence(cc.moveTo(8, cc.v2(-1000, this.goRichText.y)), cc.callFunc(() => {
            self.bInUsing = false;
            if (self.vecMsg.length > 0) {
                self.CheckAndShowNotice();
            } else {
                self.ShowOrHiden(false);
            }
        })));
    }
}


//---------------------------


let pScreenNoticeMgr = null;

module.exports = (() => {
    if (pScreenNoticeMgr == null) {
        pScreenNoticeMgr = new CScreenNoticeMgr();
    }
    return pScreenNoticeMgr;
})();


