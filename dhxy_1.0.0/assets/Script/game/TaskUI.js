﻿let GameRes = require('./GameRes');
let CPubFunction = require('./PubFunction');
let CMainPlayerInfo = require('./MainPlayerInfo');

let pTaskConfigMgr = require('./task_config').g_pTaskConfigMgr;
let EState = require('./task_config').EState;

/**
 * 任务
 */
cc.Class({
    extends: cc.Component,

    properties: {
        btnPhKind: cc.Prefab,
        preItem: cc.Prefab,
    },

    start() {
        this.vecTaskBtn = [];
        this.vecTaskState = [];

        this.vecPrizeIcon = [];

        cc.find('btnClose', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "TaskUI", "Close", 0));
        cc.find('btnAbort', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "TaskUI", "Abort", 0));

        //---------------------
        this.nIndex = 0;
        this.CreateBtnByTaskList(CMainPlayerInfo.vecTaskState);
    },


    CreateBtnByTaskList(vecTask) {
        CPubFunction.DestroyVecNode(this.vecTaskBtn);
        this.vecTaskState = [];

        let stStart = { nX: 120, nY: -40 };
        let goContent = cc.find('svButton/view/content', this.node);

        for (const it in vecTask) {
            let stData = vecTask[it];
            this.vecTaskState.push(stData);

            let stTaskConfig = pTaskConfigMgr.GetTaskInfo(stData.nTaskID);

            let goBtn = CPubFunction.CreateSubNode(goContent, { nX: stStart.nX, nY: stStart.nY - it * 70 }, this.btnPhKind, 'btnPhKind');
            goBtn.getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "TaskUI", "OnClickTask", it));
            cc.find('Label', goBtn).getComponent(cc.Label).string = stTaskConfig.strTaskName;
            this.vecTaskBtn.push(goBtn);
        }
        this.OnClickTask(null, 0);
    },

    OnClickTask(stEvent, nIndex) {
        this.nIndex = nIndex;

        let stTaskState = this.vecTaskState[nIndex];
        if (null == stTaskState)
            return;

        let strTaskConfig = pTaskConfigMgr.GetTaskInfo(stTaskState.nTaskID);
        if (null == strTaskConfig)
            return;

        let strTip = '';
        let vecPrize = [];


        for (var it in stTaskState.vecStepState) {
            let stStepState = stTaskState.vecStepState[it];
            if (stStepState.nState != EState.EDoing)
                continue;

            strTip = strTaskConfig.vecEvent[it].strTip;
            vecPrize = strTaskConfig.vecEvent[it].vecPrize.slice(0)
            break;
        }

        cc.find('labTaskTip', this.node).getComponent(cc.Label).string = strTip;

        CPubFunction.DestroyVecNode(this.vecPrizeIcon);

        for (var nIndex in vecPrize) {
            let nodPrize = cc.find('nodPrize', this.node);

            let nX = -150 + (nIndex % 5) * 75;
            let nY = 30 - Math.floor(nIndex / 5) * 75;


            let key = vecPrize[nIndex].nKey;
            let nItemNum = vecPrize[nIndex].nValue;

            if (key == 'active')
                continue;

            let goPrizeIcon = CPubFunction.CreateSubNode(nodPrize, { nX: nX, nY: nY }, this.preItem, 'preItem');

            if (key == 'exp') {
                CPubFunction.SetSpriteFrame(cc.find('Icon', goPrizeIcon), 'Common/ui_common', 'ui_common_icon_exp');
                cc.find('txLap', goPrizeIcon).getComponent(cc.Label).string = nItemNum;
            }
            else if (key == 'petexp') {
                CPubFunction.SetSpriteFrame(cc.find('Icon', goPrizeIcon), 'Common/ui_common', 'ui_common_icon_exppet');
                cc.find('txLap', goPrizeIcon).getComponent(cc.Label).string = nItemNum;
            }
            else if (key == 'money') {
                CPubFunction.SetSpriteFrame(cc.find('Icon', goPrizeIcon), 'Common/ui_common', 'ui_common_icon_yinliang');
                cc.find('txLap', goPrizeIcon).getComponent(cc.Label).string = nItemNum;
            }
            else {
                let stConfigInfo = cc.ll.propData.item[key];
                cc.find('Icon', goPrizeIcon).getComponent(cc.Sprite).spriteFrame = GameRes.getItemIcon(stConfigInfo.icon);
                cc.find('txLap', goPrizeIcon).getComponent(cc.Label).string = nItemNum;
            }
            this.vecPrizeIcon.push(goPrizeIcon);
        }
    },

    Close() {
        this.node.destroy();
    },

    Abort() {
        let stTaskState = this.vecTaskState[this.nIndex];
        if (null == stTaskState)
            return;

        cc.ll.net.send('c2s_abort_task', { nTaskID: stTaskState.nTaskID });
        this.Close();
    },

});
