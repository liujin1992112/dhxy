﻿let GameRes = require('./GameRes');
let CPubFunction = require('./PubFunction');
let CSpeak = require('./Speak');

cc.Class({
    extends: cc.Component,

    properties: {},

    onLoad() { },

    start() { },

    Close() {
        this.node.destroy();
    },

    PlayerFunUI_Init(info) {
        this.pInfo = info;

        cc.find('btnClose', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "PlayerFunUI", 'Close', 0));

        cc.find('nodPlayerInfo/picPlayerAvater', this.node).getComponent(cc.Sprite).spriteFrame = GameRes.getRoleHead(info.resid);
        cc.find('nodPlayerInfo/picPlayerAvater/level', this.node).getComponent(cc.Label).string = info.level;
        cc.find('nodPlayerInfo/nodBianHao/labText', this.node).getComponent(cc.Label).string = info.roleid;
        cc.find('nodPlayerInfo/nodName/labText', this.node).color = CPubFunction.getReliveColor(info.relive);
        cc.find('nodPlayerInfo/nodName/labText', this.node).getComponent(cc.Label).string = info.name;
        // cc.find('nodPlayerInfo/nodBangPai/labText', this.node).getComponent(cc.Label).string = strBangName;


        cc.find('btnChat', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "PlayerFunUI", 'OnPlayerFunction', 'btnChat'));
        cc.find('btnAddFriend', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "PlayerFunUI", 'OnPlayerFunction', 'btnAddFriend'));
        cc.find('btnFight', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "PlayerFunUI", 'OnPlayerFunction', 'btnFight'));
        cc.find('btnTeam', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "PlayerFunUI", 'OnPlayerFunction', 'btnTeam'));
        if (cc.ll.player.teamid > 0 && cc.ll.player.teamid == info.teamid) {
            cc.find('btnFight', this.node).active = false;
        }
        cc.find('btnTeam', this.node).active = false;
        if (cc.ll.player.teamid == 0 && info.teamid > 0) {
            cc.find('btnTeam', this.node).active = true;
        }

    },

    PlayerFunUI_Init2(nResID, nLevel, nRoleID, nRelive, strName, strBangName) {
        this.pInfo = {
            resid: nResID,
            level: nLevel,
            relive: nLevel,
            name: strName
        };

        cc.find('btnClose', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "PlayerFunUI", 'Close', 0));

        cc.find('nodPlayerInfo/picPlayerAvater', this.node).getComponent(cc.Sprite).spriteFrame = GameRes.getRoleHead(nResID);
        cc.find('nodPlayerInfo/picPlayerAvater/level', this.node).getComponent(cc.Label).string = nLevel;
        cc.find('nodPlayerInfo/nodBianHao/labText', this.node).getComponent(cc.Label).string = nRoleID;
        cc.find('nodPlayerInfo/nodName/labText', this.node).color = CPubFunction.getReliveColor(nRelive);
        cc.find('nodPlayerInfo/nodName/labText', this.node).getComponent(cc.Label).string = strName;
        // cc.find('nodPlayerInfo/nodBangPai/labText', this.node).getComponent(cc.Label).string = strBangName;


        cc.find('btnChat', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "PlayerFunUI", 'OnPlayerFunction', 'btnChat'));
        cc.find('btnAddFriend', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "PlayerFunUI", 'OnPlayerFunction', 'btnAddFriend'));
        cc.find('btnFight', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "PlayerFunUI", 'OnPlayerFunction', 'btnFight'));
        cc.find('btnTeam', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "PlayerFunUI", 'OnPlayerFunction', 'btnTeam'));
        if (cc.ll.player.teamid > 0 && cc.ll.player.teamid == info.teamid) {
            cc.find('btnFight', this.node).active = false;
        }
        cc.find('btnTeam', this.node).active = false;
        if (cc.ll.player.teamid == 0 && info.teamid > 0) {
            cc.find('btnTeam', this.node).active = true;
        }
    },



    OnPlayerFunction(stEvent, stParam) {
        if (stParam == 'btnChat') {

        } else if (stParam == 'btnAddFriend') {
            cc.ll.net.send('c2s_add_friend', {
                roleid: this.pInfo.roleid
            });
        } else if (stParam == 'btnFight') {
            cc.ll.net.send('c2s_pk', {
                troleid: this.pInfo.roleid
            });
        } else if (stParam == 'btnTeam') {
            cc.ll.net.send('c2s_requst_team', {
                roleid: cc.ll.player.roleid,
                teamid: this.pInfo.teamid
            });
        }

        this.node.destroy();
    },



});