cc.Class({
	extends: cc.Component,
	properties: {
		level_label: cc.Label,
		reward_button: cc.Node,
		canotgain_sprite: cc.Sprite,
		hasgain_sprite: cc.Sprite,
		pet_atlas: cc.SpriteAtlas,
		bag_item: cc.Prefab,
	},

	init (data) {
		this.id = data.level;
		let reward_item_array = data.reward_item.split(';');
		/* let reward_pet_array = data.reward_pet.split(';');
		let reward_equip_array = data.reward_equip.split(';'); */
		let node_list = [];
		for (let item of reward_item_array) {
			let node = this.getBagItem(item.split(':')[0], item.split(':')[1]);
			node_list.push(node);
		}
		/* for (let item of reward_pet_array) {
			let node = this.getPetItem(item.split(':')[0], item.split(':')[1]);
			node_list.push(node);
		}
		for (let item of reward_equip_array) {
			console.log(item.split(':')[0]);
			let node = this.getBagItem(item.split(':')[0], item.split(':')[1]);
			node_list.push(node);
		} */
		for (let i = 0; i < node_list.length; ++i) {
			node_list[i].x = 34 + (i - (node_list.length-1)/2) * 80;
			node_list[i].y = 0;
		}

		this.level_label.string = data.des;
		if (data.level >= 110) {
			this.level_label.node.color = new cc.color(255, 161, 42);
		}
		let cur_level = cc.ll.player.level+'';
		let level_reward = cc.ll.player.level_reward.split(':');
		if (cur_level >= data.level) {
			this.reward_button.active = (level_reward.indexOf(this.id+'') == -1);
			this.canotgain_sprite.node.active = false;
			this.hasgain_sprite.node.active = (level_reward.indexOf(this.id+'') != -1);
		}
		else {
			this.reward_button.active = false;
			this.canotgain_sprite.node.active = true;
			this.hasgain_sprite.node.active = false;
		}
	},

	getBagItem (itemid, count) {
		let node = cc.instantiate(this.bag_item);
		let logic = node.getComponent('BagItem');
		node.parent = this.node;
		node.setContentSize(80, 80);
		node.getChildByName('itembg').setScale(0.8);
		logic.loadInfo({itemid: itemid, count: count});
		logic.selectedNode.opacity = 0;
		return node;
	},

	getPetItem (petid, count) {
		let item = cc.ll.propData.pet[petid];
		let resid = item.resid;
		let name = item.name;

		let node = cc.instantiate(this.bag_item);
		let logic = node.getComponent('BagItem');
		node.parent = this.node;
		node.setContentSize(80, 80);
		node.getChildByName('itembg').setScale(0.8);
		logic.selectedNode.opacity = 0;
		logic.itemIcon.spriteFrame = this.pet_atlas.getSpriteFrame(resid);
		logic.itemBg.active = true;
		logic.itemCount.active = false;
		logic.selected = ()=> { };
		return node;
	},

	onButtonClick (event, param) {
		cc.ll.net.send('c2s_level_reward', {
			roleid: cc.ll.player.roleid,
			level: parseInt(this.id),
		});
		if (cc.ll.player.level_reward == '')
			cc.ll.player.level_reward += this.id;
		else
			cc.ll.player.level_reward += ':'+this.id;
		this.reward_button.active = false;
		this.hasgain_sprite.node.active = true;
	},
});
