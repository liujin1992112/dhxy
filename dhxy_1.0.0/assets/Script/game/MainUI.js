﻿let GameRes = require('./GameRes');
let CPubFunction = require('./PubFunction');

cc.Class({
    extends: cc.Component,

    properties: {
        TimeLabel: cc.Label,
        LeftMaskBtn: cc.Node,
        LeftMaskLayer: cc.Node,

        ActBtnList1: cc.Node,
        ActBtnList2: cc.Node,
        MoreNode: cc.Node,

        /** 地图名称Label */
        mapNameLabel: cc.Label,
        pLabel: cc.Label,
        LevelLabel: cc.Label,
        petLevel: cc.Label,

        HPProgress: cc.ProgressBar,
        MPProgress: cc.ProgressBar,

        petHPProgress: cc.ProgressBar,
        petMPProgress: cc.ProgressBar,

        expProgress: cc.ProgressBar,

        headicon: cc.Sprite,

        petheadicon: cc.Sprite,

        notice: cc.Node,
        noticeLabel: cc.RichText,
        friendBtn: cc.Node,

        teamBtn: cc.Node,
        bangBtn: cc.Node,

        friendLayer: cc.Prefab,
        bagLayer: cc.Prefab,
        roleLayer: cc.Prefab,
        petLayer: cc.Prefab,
        preBang: cc.Prefab,
        preBattle: cc.Prefab,

        btlHide: [cc.Node],
        btmOper: cc.Node,

        /** 组队列表 */
        teamList: cc.Node,
        _isBattle: false,
        isBattle: {
            get() {
                return this._isBattle;
            },

            set(n) {
                this._isBattle = n;
                this.setBattleMod();
            }
        },
        hongBao: cc.Prefab,

        sideBtn: cc.Node,

        TeamUI: cc.Prefab,

        /** 任务UI界面 */
        TalkUI: cc.Prefab,
        ShopUI: cc.Prefab,
        DailyUI: cc.Prefab,
        LevelRewardUI: cc.Prefab,
        PaiHangBangUI: cc.Prefab,
        TaskUI: cc.Prefab,
        RelationApplyUI: cc.Prefab,
        RelationListUI: cc.Prefab,
        SetPanel: cc.Prefab,
    },

    // LIFE-CYCLE CALLBACKS:
    ctor() {
        this.maxHp = 0;
        this.hp = 0;
        this.maxMp = 0;
        this.mp = 0;

        this.petHp = 0;
        this.petMp = 0;
        this.petMaxHp = 0;
        this.petMaxMp = 0;

        this.timeTimer = 0;

        this.maxExp = 0;
        this.exp = 0;

        this.notices = [];
        this.loopNotices = [];

        this.m_nCurBangPage = 0;
        this.voice_time = 0;
        this.voice_type = -1;
    },

    onLoad() {
        this.chatLogic = cc.find('bottom_left/ui_common_bg_chat_main', this.node).getComponent('UIChatLogic');
        this.petheadicon.node.active = false;
        this.setHeadIcon(cc.ll.player.resid);
    },

    start() {
        this.gameLogic = this.node.parent.getComponent('GameLogic');
        this.expProgress.totalLength = this.expProgress.node.width;
        this.notice.active = false;
        this.sideBtn.active = false;

        let self = this;
        this.timeTimer = setInterval(() => {
            self.updateTime();
        }, 30 * 1000);
        this.updateTime();

        this.m_nodBang = null;
        this.m_nBnagShowType = false;
        this.isBattle = false;

        cc.find('top_left/duobao_btn', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "MainUI", "onOpenShop", 0));
        cc.find('top_left/Mask/layer/rank_btn', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "MainUI", "onOpenPaiHangBang", 0));
        cc.find('top_left/Mask/layer/active_btn', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "MainUI", "onDailyUI", 0));
        cc.find('top_left/Mask/layer/reward_btn', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "MainUI", "onLevelReward", 0));

        cc.find('right/TaskTip/btnOpenTaskUI', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "MainUI", "OpenTaskUI", 0));


        let worldspeak = cc.find('Canvas/MainUI/bottom_left/ui_common_bg_chat_main/speakbtn');
        worldspeak.on(cc.Node.EventType.TOUCH_START, this.onWorldVoiceRecord.bind(this));
        worldspeak.on(cc.Node.EventType.TOUCH_CANCEL, this.onWorldVoiceRecord.bind(this));
        worldspeak.on(cc.Node.EventType.TOUCH_END, this.onWorldVoiceRecord.bind(this));
        let teamspeak = cc.find('Canvas/MainUI/bottom_left/ui_common_bg_chat_main/speakteam');
        teamspeak.on(cc.Node.EventType.TOUCH_START, this.onTeamVoiceRecord.bind(this));
        teamspeak.on(cc.Node.EventType.TOUCH_CANCEL, this.onTeamVoiceRecord.bind(this));
        teamspeak.on(cc.Node.EventType.TOUCH_END, this.onTeamVoiceRecord.bind(this));
        let bangspeak = cc.find('Canvas/MainUI/bottom_left/ui_common_bg_chat_main/speakbang');
        bangspeak.on(cc.Node.EventType.TOUCH_START, this.onBangVoiceRecord.bind(this));
        bangspeak.on(cc.Node.EventType.TOUCH_CANCEL, this.onBangVoiceRecord.bind(this));
        bangspeak.on(cc.Node.EventType.TOUCH_END, this.onBangVoiceRecord.bind(this));
        let voicenode = cc.find('Canvas/MainUI/Voice');
        voicenode.active = false;

        setTimeout(() => {
            this.onLoadComplete();
        }, 1000);
    },

    onLoadComplete() {
        //向服务器查询任务
        cc.ll.net.send('c2s_ask_role_task', {});
    },

    setBattleMod() {
        for (const node of this.btlHide) {
            node.active = !this._isBattle;
        }
        let bui = cc.find('Canvas/BattleUILayer/BattleUI');
        if (bui) {
            bui.active = this._isBattle;
        }
        let blayer = cc.find('Canvas/BattleLayer');
        blayer.active = this._isBattle;
        if (!this._isBattle) {
            this.btmOper.x = 0;
        }
    },

    showBattleEnd(iswin) {
        this.isBattle = false;
        if (iswin == false) {
            CPubFunction.CreateNotice(cc.find('Canvas/MainUI'), '战斗失败，再接再厉', 2);

            let comTaskTip = cc.find('TaskTip', cc.find('Canvas/MainUI/right')).getComponent('TaskTip');
            comTaskTip.stAuto.nKind = 0;
            comTaskTip.stAuto.nTaskID = 0;

        }
        else {
            CPubFunction.CreateNotice(cc.find('Canvas/MainUI'), '战斗胜利', 2);
        }
    },

    OnTest() {
        cc.ll.net.send('c2s_create_test_npc', {});
    },

    onDestroy() {
        if (this.timeTimer != 0) {
            clearInterval(this.timeTimer);
        }
    },

    updateTime() {
        let time = new Date();
        let h = time.getHours();
        if (h < 10) {
            h = '0' + h;
        }
        let m = time.getMinutes();
        if (m < 10) {
            m = '0' + m;
        }
        let t = `${h}:${m}`;
        this.TimeLabel.string = t;

    },

    /**
     * 设置地图名称
     * @param {*} name 
     */
    setMapName(name) {
        this.mapNameLabel.string = name;
    },

    setPosition(x, y) {
        this.pLabel.string = `(${x}, ${y})`;
    },

    setMaxHp(maxhp) {
        this.maxHp = maxhp;
        this.updateHpMp();
    },

    setMaxMp(maxmp) {
        this.maxMp = maxmp;
        this.updateHpMp();
    },

    setHp(hp) {
        this.hp = hp;
        this.updateHpMp();
    },

    setMp(mp) {
        this.mp = mp;
        this.updateHpMp();
    },

    updateHpMp() {
        this.HPProgress.progress = this.hp / this.maxhp;
        this.MPProgress.progress = this.mp / this.maxMp;
    },

    setPetMaxHp(maxhp) {
        this.petMaxHp = maxhp;
        this.updatePetHpMp();
    },

    setPetMaxMp(maxmp) {
        this.petMaxMp = maxmp;
        this.updatePetHpMp();
    },

    setPetHp(hp) {
        this.petHp = hp;
        this.updatePetHpMp();
    },

    setPetMp(mp) {
        this.petMp = mp;
        this.updatePetHpMp();
    },

    updatePetHpMp() {
        this.petHPProgress.progress = this.petHp / this.petMaxhp;
        this.petMPProgress.progress = this.petMp / this.petMaxMp;
    },

    setHeadIcon(iconid) {
        let sp = GameRes.getRoleHead(iconid);
        if (sp) {
            this.headicon.spriteFrame = sp;
        }
    },

    setPetHeadIcon(iconid) {
        if (!iconid || iconid <= 0) {
            this.petheadicon.node.active = false;
            return;
        }
        let sp = GameRes.getPetHead(iconid);
        if (sp) {
            this.petheadicon.node.active = true;
            this.petheadicon.spriteFrame = sp;
        }
    },

    setMaxExp(exp) {
        this.maxExp = exp;
    },

    setExp(exp, maxexp = 0) {
        this.exp = exp;
        if (maxexp != 0) {
            this.maxExp = maxexp;
        }
        this.updateExp();
    },

    setRoleLevel(level) {
        this.LevelLabel.string = level;
    },

    setPetLevel(level) {
        if (this.petheadicon.node.active) {
            this.petLevel.node.active = true;
            this.petLevel.string = level;
        }
        else {
            this.petLevel.node.active = false;
        }
    },

    updateExp() {
        this.expProgress.progress = this.exp / this.maxExp;
    },

    /*{
        <color = #00ff00>Rich</c><color= #0fffff>Text</color>
    }*/
    addNotice(msg) {
        this.notices.push(msg);
        this.startNotice();
    },

    playNotice() {
        if (this.notices.length == 0) {
            this.isPlayNotice = false;
            this.notice.active = false;
            return;
        }

        this.isPlayNotice = true;
        this.notice.active = true;

        this.noticeLabel.node.x = 0;
        this.noticeLabel.string = this.notices.shift();
        this.scheduleOnce(() => {
            let time = this.noticeLabel.width / 500 * 5;
            this.noticeLabel.runAction(cc.sequence(
                cc.moveTo(time, cc.v2(-this.noticeLabel.width - 80, 0)),
                cc.callFunc(() => {
                    this.playNotice();
                }, this)
            ));
        }, 0);
    },

    startNotice() {
        if (this.isPlayNotice) {
            return;
        }
        this.playNotice();
    },


    // update (dt) {},

    onLeftMaskBtnClicked(e, d) {
        let action = this.LeftMaskLayer.getActionByTag(9874);
        if (action && !action.isDone()) {
            return;
        }
        let k = Math.abs(this.LeftMaskLayer.y);
        if (k < 10) {
            this.LeftMaskBtn.angle = 90;
            let act = cc.moveTo(0.1, cc.v2(0, this.LeftMaskLayer.height));
            act.setTag(9874);
            this.LeftMaskLayer.runAction(act);
        } else {
            this.LeftMaskBtn.angle = 270;
            let act = cc.moveTo(0.1, cc.v2(0, 0));
            act.setTag(9874);
            this.LeftMaskLayer.runAction(act);
        }
    },

    onMoreBtnClicked(e, d) {
        let action = this.ActBtnList1.getActionByTag(9874);
        if (action && !action.isDone()) {
            return;
        }
        let xlen = 500;
        let k = Math.abs(this.ActBtnList1.x);

        let action1 = cc.moveTo(0.1, cc.v2(xlen, 0));
        action1.setTag(9874);

        let act1 = cc.delayTime(0.1);
        let act2 = cc.moveTo(0.1, cc.v2(0, 0));
        let action2 = cc.sequence(act1, act2);
        action2.setTag(9874);
        if (k < 10) {
            this.ActBtnList1.runAction(action1);
            this.ActBtnList2.runAction(action2);
            this.MoreNode.runAction(cc.rotateTo(0.2, 45));
        } else {
            this.ActBtnList1.runAction(action2);
            this.ActBtnList2.runAction(action1);
            this.MoreNode.runAction(cc.rotateTo(0.2, 0));
        }
    },

    //zfy 加入队伍提示
    joinTeamWarn(data) {
        this.teamBtn.stopAllActions();
        this.teamBtn.runAction(cc.repeatForever(cc.sequence(cc.moveTo(0.3, cc.v2(-156, 30)), cc.moveTo(0.3, cc.v2(-156, -6)))));
    },

    onTeam(e, d) {
        this.teamBtn.stopAllActions();
        this.teamBtn.y = -6;

        CPubFunction.FindAndDeleteNode(this.node, "TeamUI");
        CPubFunction.CreateSubNode(this.node, {
            nX: 0,
            nY: 0
        }, this.TeamUI, 'TeamUI');
        //zfy --打开队伍音效
        cc.ll.AudioMgr.playOpenAudio();
    },

    //zfy 交换队长
    transferTeamDlg(data) {
        let node = cc.ll.notice.addMsg(1, '是否同意交换队长', () => {
            cc.ll.net.send('c2s_transfer_team', {
                roleid: data.roleid,
            });
        }, () => { }, 0);
        this.scheduleOnce(function () {
            node.destroy();
        }, 15);
    },

    onPackage(e, d) {
        /*
        CPubFunction.FindAndDeleteNode(this.node, "TalkUI");
        let goBattleUI = CPubFunction.CreateSubNode(this.node, {
            nX: 0,
            nY: -188
        }, this.TalkUI, 'TalkUI');
        */
        //cc.ll.net.send('c2s_btl_act', { action: 1, actionid: 0, targetid: 101, onlyid: this.gameLogic.GetMainPlayerID() });
    },

    onSetClicked(e, d) {
        let set = cc.instantiate(this.SetPanel);
        set.parent = cc.find('Canvas');
        //zfy --打开设置音效

        cc.ll.AudioMgr.playAudio('ui/ui_shezhi');
    },

    onRoleHeadClicked(e, d) {
        let role = cc.instantiate(this.roleLayer);
        role.name = 'RolePanel';
        role.parent = this.node;
        cc.ll.AudioMgr.playOpenAudio();
    },

    onPetHeadClicked(e, d) {
        if (this.node.getChildByName('PetPanel')) {
            this.node.getChildByName('PetPanel').destroy();
        }
        let pet = cc.instantiate(this.petLayer);
        pet.name = 'PetPanel';
        pet.parent = this.node;
        cc.ll.AudioMgr.playOpenAudio();
    },

    onBagBtnClicked(e, d) {
        if (this.node.getChildByName('BagPanel')) {
            this.node.getChildByName('BagPanel').destroy();
        }
        let bag = cc.instantiate(this.bagLayer);
        bag.name = 'BagPanel';
        bag.parent = this.node;
        //zfy --背包音效
        cc.ll.AudioMgr.playAudio('ui/ui_beibao');
    },

    friendsChat(info) {
        let friendsLayer = this.node.getChildByName('FriendPanel');
        if (friendsLayer != null) {
            friendsLayer.getComponent('FriendPanel').addChatListInfo(info);
        }
        else {
            this.friendBtn.stopAllActions();
            this.friendBtn.getChildByName('tip').active = true;
            this.friendBtn.runAction(cc.repeatForever(cc.sequence(cc.moveTo(0.3, cc.v2(-257, 30)), cc.moveTo(0.3, cc.v2(-257, -6)))));
        }
    },

    onFriendBtnClicked(e, d) {
        this.friendBtn.stopAllActions();
        this.friendBtn.y = -6;
        this.friendBtn.getChildByName('tip').active = false;
        if (this.node.getChildByName('FriendPanel')) {
            this.node.getChildByName('FriendPanel').destroy();
        }
        let friend = cc.instantiate(this.friendLayer);
        friend.name = 'FriendPanel';
        friend.parent = this.node;
        //zfy --打开好友音效
        cc.ll.AudioMgr.playOpenAudio();
    },

    //zfy 帮派
    joinBangWarn() {
        this.bangBtn.stopAllActions();
        this.bangBtn.runAction(cc.repeatForever(cc.sequence(cc.moveTo(0.3, cc.v2(-358, 30)), cc.moveTo(0.3, cc.v2(-358, -6)))));
    },

    onBangBtnClicked(e, d) {

        this.bangBtn.stopAllActions();
        this.bangBtn.y = -6;

        if (cc.find('Canvas').getChildByName('BangPanel')) {
            cc.find('Canvas').getChildByName('BangPanel').destroy();
        }
        let bang = cc.instantiate(this.preBang);
        bang.name = 'BangPanel';
        bang.parent = cc.find('Canvas');
        let bangid = cc.ll.player.bangid;
        if (bangid > 0) {
            bang.getComponent('BangPanel').showBangLayer();
        } else {
            bang.getComponent('BangPanel').showAddlayer();
        }
        //zfy --打开帮派音效
        cc.ll.AudioMgr.playOpenAudio();
    },

    onRightLowerBtnClicked(e, d) { },

    onMainMapBtnClicked(e, d) {
        cc.ll.AudioMgr.playOpenAudio();
        this.gameLogic.showMainMap();
    },

    onSmallMapBtnClicked(e, d) {
        cc.ll.AudioMgr.playOpenAudio();
        this.gameLogic.showSmallMap();
    },

    onOpenShop(e, d) {
        cc.ll.AudioMgr.playOpenAudio();
        CPubFunction.CreateSubNode(this.node, {
            nX: 0,
            nY: 0
        }, this.ShopUI, 'ShopUI');
    },

    onDailyUI(e, d) {
        cc.ll.AudioMgr.playOpenAudio();
        CPubFunction.CreateSubNode(this.node, {
            nX: 0,
            nY: 0
        }, this.DailyUI, 'DailyUI');
    },

    onLevelReward(e, d) {
        this.sideBtn.active = true;
    },

    onLevelBtnCicked() {
        cc.ll.AudioMgr.playOpenAudio();
        CPubFunction.CreateSubNode(this.node, {
            nX: 0,
            nY: 0
        }, this.LevelRewardUI, 'LevelRewardUI');
        this.onSideBtn_Close();
    },
    //--zfy  请求服务器获取补偿奖励
    onRewardBtnClicked() {
        cc.ll.net.send('c2s_remunerate');
        this.onSideBtn_Close();
    },
    //-zfy 世界红包界面
    onWorldRewardClick() {
        cc.ll.net.send('c2s_world_reward_list');
        this.onSideBtn_Close();
    },

    onSideBtn_Close() {
        this.sideBtn.active = false;
    },


    onOpenPaiHangBang(e, d) {
        cc.ll.AudioMgr.playOpenAudio();
        CPubFunction.CreateSubNode(this.node, {
            nX: 0,
            nY: 0
        }, this.PaiHangBangUI, 'PaiHangBangUI');
    },

    CloseUIByName(e, strName) {
        CPubFunction.FindAndDeleteNode(this.node, strName);
    },

    OpenTaskUI() {
        CPubFunction.FindAndDeleteNode(this.node, 'TaskUI');
        CPubFunction.CreateSubNode(this.node, {
            nX: 0,
            nY: 0
        }, this.TaskUI, 'TaskUI');
    },

    setTeamListInfo() {
        this.teamList.getComponent('MainUITeamList').setTeamListInfo();
    },


    changePrisonTime(n) {
        let prison = cc.find('prisonTime', this.node);
        if (prison) {
            if (n <= 0) {
                prison.active = false;
            }
            else {
                prison.active = true;
                let h = Math.floor(n / 3600) < 10 ? '0' + Math.floor(n / 3600) : Math.floor(n / 3600);
                let m = Math.floor((n / 60 % 60)) < 10 ? '0' + Math.floor((n / 60 % 60)) : Math.floor((n / 60 % 60));
                let s = Math.floor((n % 60)) < 10 ? '0' + Math.floor((n % 60)) : Math.floor((n % 60));
                let notice = `${h}:${m}:${s}后刑满释放`;
                prison.getChildByName('time').getComponent(cc.Label).string = notice;
                this.scheduleOnce(() => { this.changePrisonTime(n - 1); }, 1);
            }
        }
    },

    showHongBaoIcon() {
        let hongbaoicon = cc.find('top_left/hongbao', this.node);
        hongbaoicon.active = true;
        let action = cc.sequence(
            cc.rotateTo(0.05, 15),
            cc.rotateTo(0.05, -15),
            cc.rotateTo(0.05, 15),
            cc.rotateTo(0.05, -15),
            cc.rotateTo(0.05, 15),
            cc.rotateTo(0.05, -15),
            cc.delayTime(1)
        );
        hongbaoicon.runAction(cc.repeatForever(action));
    },

    hideHongBaoIcon() {
        let hongbaoicon = cc.find('top_left/hongbao', this.node);
        hongbaoicon.stopAllActions();
        hongbaoicon.active = false;
    },

    showHongBaoPanel() {
        this.hideHongBaoIcon();
        let node = this.node.getChildByName('WorldRedPocket');
        if (node != null) {
            return;
        }
        let hongbaopanel = cc.instantiate(this.hongBao);
        hongbaopanel.name = 'WorldRedPocket';
        hongbaopanel.parent = this.node;
    },

    hideHongBaoPanel() {
        let hongbaopanel = this.node.getChildByName('WorldRedPocket');
        if (hongbaopanel) {
            hongbaopanel.destroy();
        }
    },

    onOpenRelationApplyUI(data) {
        cc.ll.AudioMgr.playOpenAudio();
        let relationApplyUI = CPubFunction.CreateSubNode(this.node, {
            nX: 0,
            nY: 0
        }, this.RelationApplyUI, 'RelationApplyUI');
        let relationApply = relationApplyUI.getComponent('RelationApplyPanel');
        relationApply.initApplyData(data);
    },

    onOpenRelationListUI(data) {
        cc.ll.AudioMgr.playOpenAudio();
        let RelationListUI = CPubFunction.CreateSubNode(this.node, {
            nX: 0,
            nY: 0
        }, this.RelationListUI, 'RelationListUI');
        let relationListLogic = RelationListUI.getComponent('RelationListPanel');
        relationListLogic.initApplyData(data);
    },

    onWorldVoiceRecord(e, d) {
        if (!CC_JSB) {
            e.stopPropagation();
            return false;
        }
        if (e.type == cc.Node.EventType.TOUCH_START) {
            this.voiceRecord(0);

        } else if (e.type == cc.Node.EventType.TOUCH_END || e.type == cc.Node.EventType.TOUCH_CANCEL) {
            this.voiceRecordEnd(0);
        }
        e.stopPropagation();
    },

    onTeamVoiceRecord(e, d) {
        if (!CC_JSB) {
            e.stopPropagation();
            return false;
        }
        if (e.type == cc.Node.EventType.TOUCH_START) {
            this.voiceRecord(1);

        } else if (e.type == cc.Node.EventType.TOUCH_END || e.type == cc.Node.EventType.TOUCH_CANCEL) {
            this.voiceRecordEnd(1);
        }
        e.stopPropagation();
    },

    onBangVoiceRecord(e, d) {
        if (!CC_JSB) {
            e.stopPropagation();
            return false;
        }
        if (e.type == cc.Node.EventType.TOUCH_START) {
            this.voiceRecord(2);

        } else if (e.type == cc.Node.EventType.TOUCH_END || e.type == cc.Node.EventType.TOUCH_CANCEL) {
            this.voiceRecordEnd(2);
        }
        e.stopPropagation();
    },

    voiceRecord(vtype) {
        if (!CC_JSB) {
            return;
        }

        if (this.voice_type != -1) {
            return;
        }

        if (!cc.ll.voicemgr.startRecord()) {
            return;
        }

        this.voice_type = vtype;

        let voicenode = cc.find('Canvas/MainUI/Voice');
        voicenode.active = true;

        this.recored_timer = setTimeout(() => {
            this.voiceRecordEnd();
        }, 20 * 1000);
    },

    voiceRecordEnd() {
        if (!CC_JSB) {
            return;
        }
        let scaletype = this.voice_type;
        this.voice_type = -1;

        if (this.recored_timer != 0) {
            clearTimeout(this.recored_timer);
        }
        let voicenode = cc.find('Canvas/MainUI/Voice');
        voicenode.active = false;

        let data = cc.ll.voicemgr.endRecord();
        if (data == null) {
            return;
        }

        cc.ll.net.send('c2s_game_chat', {
            scale: scaletype,
            voice: data,
        });
    },
});
