
cc.Class({
    extends: cc.Component,
    properties: {
    },

    onLoad() {
        this.soldierIcons = [];
        for (let index = 0; ;index++) {
           let soldierIcon = cc.find('soldier_icon${index}', this.node);
           if (soldierIcon != null) {
               this.soldierIcons.push(soldierIcon);
           }
           else{
               return;
           }
        }
    },

    soldierClicked(e, d){

    },

    onCloseBtnClicked(e){
        this.node.destroy();
    },
});
