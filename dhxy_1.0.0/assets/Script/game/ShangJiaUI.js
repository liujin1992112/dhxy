﻿let GameRes = require('./GameRes');
let CPubFunction = require('./PubFunction');
let CMainPlayerInfo = require('./MainPlayerInfo');


let EShopAction =
{
    EAdd: 1,
    EDelete: 0
}

cc.Class({
    extends: cc.Component,


    properties: {
        BagItem: cc.Prefab,
        NumPad: cc.Prefab,
    },


    onLoad() {
        this.nSelect = 0;
        this.vecBtnItm = [];
    },

    start() {
        cc.find('btnClose', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "ShangJiaUI", "Close", 0));
        cc.find('bntSell', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "ShangJiaUI", "OnSell", 0));
        cc.find('btnTakeBack', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "ShangJiaUI", "OnTakeBack", 0));


        cc.find('nodSellNum/btnAdd', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "ShangJiaUI", "ChangeSellCnt", 1));
        cc.find('nodSellNum/btnCut', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "ShangJiaUI", "ChangeSellCnt", -1));
        cc.find('nodSellPrice/btnAdd', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "ShangJiaUI", "ChangePrice", 1));
        cc.find('nodSellPrice/btnCut', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "ShangJiaUI", "ChangePrice", -1));
        cc.find('nodSellNum/btnPad', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "ShangJiaUI", "OpenNumPadForCnt", 0));
        cc.find('nodSellPrice/btnPad', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "ShangJiaUI", "OpenNumPadForPrice", 0));

    },


    Init() {
    },


    Close() {
        this.node.destroy();
    },


    InitCtrState(nAct) {
        this.nAddOrDelete = nAct;
        cc.find('nodSellNum/btnAdd', this.node).active = (nAct == EShopAction.EAdd ? true : false);
        cc.find('nodSellNum/btnCut', this.node).active = (nAct == EShopAction.EAdd ? true : false);
        cc.find('nodSellPrice/btnAdd', this.node).active = (nAct == EShopAction.EAdd ? true : false);
        cc.find('nodSellPrice/btnCut', this.node).active = (nAct == EShopAction.EAdd ? true : false);
    },


    ShowBagItem() {
        this.InitCtrState(EShopAction.EAdd);

        CPubFunction.DestroyVecNode(this.vecBtnItm);
        let goContentBagItem = cc.find('scvGoods/view/content', this.node);

        let stStart = { nX: 40, nY: -40 };

        for (var it in CMainPlayerInfo.vecBagItem) {
            let stData = CMainPlayerInfo.vecBagItem[it];
            let btnBagItem = CPubFunction.CreateSubNode(goContentBagItem, { nX: stStart.nX + (it % 3) * 80, nY: stStart.nY - Math.floor(it / 3) * 80 }, this.BagItem, '');
            btnBagItem.getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "ShangJiaUI", "OnClickBagItm", it));
            btnBagItem.getComponent('BagItem').loadInfo(stData);
            this.vecBtnItm.push(btnBagItem);
        }

        goContentBagItem.height = Math.max(goContentBagItem.height, (CMainPlayerInfo.vecBagItem.length / 3 + 1) * 80);

        this.OnClickBagItm(null, 0);
    },

    OnClickBagItm(stEvent, nIndex) {
        this.nSelect = nIndex;

        let stBagItem = CMainPlayerInfo.vecBagItem[this.nSelect];

        let stInfo = cc.ll.propData.item[stBagItem.itemid];

        let goIcon = cc.find('svItemDetail/view/content/ItemIcon/icon', this.node);
        goIcon.getComponent(cc.Sprite).spriteFrame = GameRes.getItemIcon(stInfo.icon);
        cc.find('svItemDetail/view/content/txTitle', this.node).getComponent(cc.Label).string = stInfo.name;

        let strText = stInfo.description;
        strText += '\n\n' + '【用途】' + stInfo.usedetail;

        cc.find('svItemDetail/view/content/txDetal', this.node).getComponent(cc.Label).string = strText;
    },


    ShowSellingItem() {
        this.InitCtrState(EShopAction.EDelete);

        CPubFunction.DestroyVecNode(this.vecBtnItm);

        let goContentBagItem = cc.find('scvGoods/view/content', this.node);

        let stStart = { nX: 40, nY: -40 };

        for (var it in CMainPlayerInfo.vecMyGoods) {
            let stGoods = CMainPlayerInfo.vecMyGoods[it];

            let btnGoods = CPubFunction.CreateSubNode(goContentBagItem, { nX: stStart.nX + (it % 3) * 80, nY: stStart.nY - Math.floor(it / 3) * 80 }, this.BagItem, '');
            btnGoods.getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "ShangJiaUI", "OnClickGoods", it));
            btnGoods.getComponent('BagItem').loadInfo({ itemid: stGoods.nConfigID, count: stGoods.nCnt });
            this.vecBtnItm.push(btnGoods);
        }

        this.OnClickGoods(null, 0);
    },

    OnClickGoods(stEvent, nIndex) {
        this.nSelect = nIndex;

        let stGoods = CMainPlayerInfo.vecMyGoods[this.nSelect];

        let stInfo = cc.ll.propData.item[stGoods.nConfigID];

        let goIcon = cc.find('svItemDetail/view/content/ItemIcon/icon', this.node);

        goIcon.getComponent(cc.Sprite).spriteFrame = GameRes.getItemIcon(stInfo.icon);
        cc.find('svItemDetail/view/content/txTitle', this.node).getComponent(cc.Label).string = stInfo.name;

        let strText = stInfo.description;
        strText += '\n\n' + '【用途】' + stInfo.usedetail;

        cc.find('svItemDetail/view/content/txDetal', this.node).getComponent(cc.Label).string = strText;
        cc.find('nodSellNum/Label', this.node).getComponent(cc.Label).string = stGoods.nCnt;
        cc.find('nodSellPrice/Label', this.node).getComponent(cc.Label).string = stGoods.nPrice;
    },



    OpenNumPadForCnt(e, d) {
        if (this.nAddOrDelete == EShopAction.EDelete)
            return;

        let goNumPad = CPubFunction.CreateSubNode(cc.find('Canvas/MainUI'), { nX: 20, nY: 245 }, this.NumPad, 'NumPad');
        goNumPad.getComponent('NumPad').NumPad_Init((nNum) => {
            cc.find('nodSellNum/Label', this.node).getComponent(cc.Label).string = CPubFunction.ChangeNumToRange(nNum, 1, CMainPlayerInfo.vecBagItem[this.nSelect].count);
        });
    },

    OpenNumPadForPrice(e, d) {
        if (this.nAddOrDelete == EShopAction.EDelete)
            return;

        let goNumPad = CPubFunction.CreateSubNode(cc.find('Canvas/MainUI'), { nX: 20, nY: 245 }, this.NumPad, 'NumPad');
        goNumPad.getComponent('NumPad').NumPad_Init((nNum) => {
            cc.find('nodSellPrice/Label', this.node).getComponent(cc.Label).string = CPubFunction.ChangeNumToRange(nNum, 1, 9999999999);;
        });
    },


    ChangeSellCnt(stEvent, nAdd) {
        let nCur = parseInt(cc.find('nodSellNum/Label', this.node).getComponent(cc.Label).string);
        let nNewNum = CPubFunction.ChangeNumToRange(nCur + nAdd, 1, CMainPlayerInfo.vecBagItem[this.nSelect].count);
        cc.find('nodSellNum/Label', this.node).getComponent(cc.Label).string = nNewNum;
    },



    ChangePrice(stEvent, nAdd) {
        let nPrice = parseInt(cc.find('nodSellPrice/Label', this.node).getComponent(cc.Label).string);
        let nNewPrice = nPrice + 1;
        cc.find('nodSellPrice/Label', this.node).getComponent(cc.Label).string = nNewPrice;
    },

    OnSell() {
        if (this.nAddOrDelete != EShopAction.EAdd)
            return;

        let nCnt = parseInt(cc.find('nodSellNum/Label', this.node).getComponent(cc.Label).string);
        let nPrice = parseInt(cc.find('nodSellPrice/Label', this.node).getComponent(cc.Label).string);

        let stBagItem = CMainPlayerInfo.vecBagItem[this.nSelect];
        cc.ll.net.send('c2s_add_goods',
            {
                nSeller: cc.ll.player.roleid,
                nConfigID: stBagItem.itemid,
                nPrice: nPrice,
                nCnt: nCnt
            });

        this.Close();
    },

    OnTakeBack() {
        if (this.nAddOrDelete != EShopAction.EDelete)
            return;

        let stGoods = CMainPlayerInfo.vecMyGoods[this.nSelect];
        cc.ll.net.send('c2s_take_back_goods', { nID: stGoods.nID });
    },



});
