let resAnchor = {
    [1001]: { scale: 1.5, x: 0.55, y: 0.4 },
    [1002]: { scale: 2, x: 0.55, y: 0.4 },
    [1011]: { scale: 1.6, x: 0.3, y: 0.35 },
    [1012]: { scale: 1.5, x: 0.55, y: 0.35 },
    [1031]: { scale: 1.5, x: 0.55, y: 0.35 },
    [1032]: { scale: 1.5, x: 0.6, y: 0.35 },

    [2003]: { scale: 1.7, x: 0.52, y: 0.4 },
    [2004]: { scale: 1.5, x: 0.65, y: 0.3 },
    [2013]: { scale: 1.6, x: 0.65, y: 0.4 },
    [2014]: { scale: 1.6, x: 0.65, y: 0.35 },
    [2033]: { scale: 1.7, x: 0.55, y: 0.42 },
    [2034]: { scale: 1.7, x: 0.5, y: 0.4 },

    [3005]: { scale: 1.8, x: 0.52, y: 0.35 },
    [3006]: { scale: 1.7, x: 0.45, y: 0.35 },
    [3015]: { scale: 1.5, x: 0.55, y: 0.35 },
    [3016]: { scale: 1.7, x: 0.55, y: 0.35 },
    [3035]: { scale: 1.5, x: 0.45, y: 0.4 },
    [3036]: { scale: 1.7, x: 0.45, y: 0.45 },

    [4007]: { scale: 1.7, x: 0.45, y: 0.45 },
    [4008]: { scale: 1.7, x: 0.58, y: 0.35 },
    [4017]: { scale: 1.5, x: 0.5, y: 0.4 },
    [4018]: { scale: 1.7, x: 0.58, y: 0.4 },
    [4037]: { scale: 1.6, x: 0.55, y: 0.4 },
    [4038]: { scale: 1.5, x: 0.68, y: 0.4 },
}

cc.Class({
    extends: cc.Component,

    properties: {
        timeLabel: cc.Label,
        roleres: cc.SpriteAtlas,
    },

    ctor() {
        this.timer = 0;
        this.timer_n = 10;
    },

    onDestroy() {
        clearInterval(this.timer);
    },

    start() {
        this.timer = setInterval(() => {
            this.timeLabel.string = this.timer_n
            this.timer_n--;
            if (this.timer_n < 0) {
                this.timer_n = 0;
            }
        }, 1000);
    },

    setData(data) {
        for (let i = 0; i < 5; i++) {
            const t = data.teamS[i];
            if(t == null){
                break;
            }
            let tpinfo = resAnchor[t.resid];
            let roleresnode = cc.find('our_side_frame'+ (i+ 1) +'/mask/roleres', this.node);
            let sprite = roleresnode.getComponent(cc.Sprite);
            sprite.spriteFrame = this.roleres.getSpriteFrame(t.resid);
            roleresnode.scale = tpinfo.scale;
            roleresnode.anchorX = tpinfo.x;
            roleresnode.anchorY = tpinfo.y;

            let levelnode = cc.find('our_side_frame'+ (i+ 1) +'/levelbg/level', this.node);
            let levellabel = levelnode.getComponent(cc.Label);
            levellabel.string = t.level;

            let namenode = cc.find('our_side_frame'+ (i+ 1) +'/levelbg/name', this.node);
            let namelabel = namenode.getComponent(cc.Label);
            namelabel.string = t.name;
        }
        
        for (let i = 0; i < 5; i++) {
            const t = data.teamE[i];
            if(t == null){
                break;
            }
            let tpinfo = resAnchor[t.resid];
            let roleresnode = cc.find('enemy_frame'+ (i+ 1) +'/mask/roleres', this.node);
            let sprite = roleresnode.getComponent(cc.Sprite);
            sprite.spriteFrame = this.roleres.getSpriteFrame(t.resid);
            roleresnode.scale = tpinfo.scale;
            roleresnode.anchorX = tpinfo.x;
            roleresnode.anchorY = tpinfo.y;

            let levelnode = cc.find('enemy_frame'+ (i+ 1) +'/levelbg/level', this.node);
            let levellabel = levelnode.getComponent(cc.Label);
            levellabel.string = t.level;

            let namenode = cc.find('enemy_frame'+ (i+ 1) +'/levelbg/name', this.node);
            let namelabel = namenode.getComponent(cc.Label);
            namelabel.string = t.name;
        }
    }
});
