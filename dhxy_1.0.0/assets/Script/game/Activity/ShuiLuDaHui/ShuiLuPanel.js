let SLDHMgr = require('./sldh_mgr');
cc.Class({
    extends: cc.Component,

    properties: {
        lunciLabel: cc.Label,
        winTimesLabel: cc.Label,
        lossTimesLabel: cc.Label,
        scoreLabel: cc.Label,
        gongjiLabel: cc.Label,

        signLayer: cc.Node,
        fightLayer: cc.Node,
        infoRow: cc.Prefab,

        roleAtlas: cc.SpriteAtlas,
    },

    start() {

    },

    setData(data) {
        this.lunciLabel.string = '水陆大会 第' + data.lunci + '轮';
        this.scoreLabel.string = data.score;
        this.gongjiLabel.string = data.gongji;
        this.winTimesLabel.string = data.wtime;
        this.lossTimesLabel.string = data.ltime;

        this.setSelfTeamHead(data.selfteam);

        if (data.gamestate == SLDHMgr.SLDHState.Sign) {
            this.signLayer.active = true;
            this.fightLayer.active = false;
            this.setSignList(data.sign);
        } else if (data.gamestate >= SLDHMgr.SLDHState.CalTeam) {
            this.signLayer.active = false;
            this.fightLayer.active = true;
            this.setFightList(data.fight);
        }
    },

    setSelfTeamHead(selfteam) {
        for (let i = 0; i < 5; i++) {
            let selfteaminfo = selfteam[i];
            let item = cc.find('bgPanel/selfteam/selfteam/headicon' + (i + 1), this.node);
            let sprite = item.getComponent(cc.Sprite);
            if (selfteaminfo) {
                sprite.spriteFrame = this.roleAtlas.getSpriteFrame('role_' + selfteaminfo.resid);
            } else {
                sprite.spriteFrame = '';
            }
        }
    },

    setSignList(signlist) {
        let bg = cc.find('bgPanel/signLayer/ScrollView/view/content', this.node);
        bg.destroyAllChildren();
        let height = 0;
        for (let i = 0; i < signlist.length; i++) {
            const signinfo = signlist[i];

            let item = cc.instantiate(this.infoRow);
            item.x = 0;
            item.y = -24 - 50 * i;
            item.parent = bg;

            height = Math.abs(item.y);

            if (i % 2 == 0) {
                item.getComponent(cc.Sprite).spriteFrame = '';
            }

            cc.find('Index', item).getComponent(cc.Label).string = i + 1;
            cc.find('Name', item).getComponent(cc.Label).string = signinfo.name;
            cc.find('Race', item).getComponent(cc.Label).string = signinfo.rolenum;
            cc.find('EquipScpre', item).getComponent(cc.Label).string = signinfo.score;
        }
        bg.height = height;
    },

    setFightList(fightlist) {
        let hasfinish = true;
        for (let i = 0; i < 5; i++) {
            let finfo = fightlist[i];

            let winflag = cc.find('bgPanel/figitLayer/changci' + (i + 1) + '/icon_win', this.node);
            let lossflag = cc.find('bgPanel/figitLayer/changci' + (i + 1) + '/icon_lose', this.node);
            
            winflag.active = finfo.iswin == 1;
            lossflag.active = finfo.iswin == 0;

            if(finfo.iswin == 2){
                hasfinish = false;
            }

            for (let ei = 0; ei < 5; ei++) {
                let einfo = finfo.elist[ei];
                let item = cc.find('bgPanel/figitLayer/changci' + (i + 1) + '/team/headicon' + (ei + 1), this.node);
                let sprite = item.getComponent(cc.Sprite);
                if (einfo) {
                    sprite.spriteFrame = this.roleAtlas.getSpriteFrame('role_' + einfo.resid);
                } else {
                    sprite.spriteFrame = '';
                }
            }
        }
        if(hasfinish){
            cc.ll.msgbox.addMsg('五场战斗已完毕，积分功绩均已记录');
        }
    },

    onBtnClick(e, d) {
        if (d == 'unsign') {
            cc.ll.net.send('c2s_shuilu_unsign');
        }else if (d == 'gongji') {
        }
    }
});
