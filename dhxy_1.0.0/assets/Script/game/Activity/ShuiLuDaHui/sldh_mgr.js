let IconTag = 'IconTag';
let PanelTag = 'PanelTag';
let BtlTag = 'BtlTag';
let MallTag = 'MallTag';
let BtlEndTag = 'BtlEndTag';

let SLDHMgr = cc.Class({
    statics: {
        SLDHState: {
            Sign: 1,
            CalTeam: 2,
            Match: 3,
            Result: 4,
            Close: 5,
        },

        FightState: {
            Wait: 1,
            FightEnd: 2,
            Fighting: 3,
        }
    },

    ctor() {
        this.updateMainUI();
        this.sldhstate = 0;
    },

    updateMainUI() {
        this.mainUI = cc.find('Canvas/MainUI');
        this.mainUIlogic = this.mainUI.getComponent('MainUI');
    },

    checkMainUI() {
        if (!cc.isValid(this.mainUI)) {
            this.updateMainUI();
        }
    },

    changeState(state) {
        if (state == SLDHMgr.SLDHState.Sign) {
            this.showShuiLuIcon('已报名');
        } else if (state == SLDHMgr.SLDHState.CalTeam) {
            this.showShuiLuIcon('等待比赛开始');
        } else if (state == SLDHMgr.SLDHState.Match) {
            this.showShuiLuIcon('请准备对局马上开始');
        } else if (state == SLDHMgr.SLDHState.Close) {
            this.destroyShuiLuIcon();
        }
        this.sldhstate = state;
    },

    showShuiLuIcon(tips) {
        this.checkMainUI();
        let shuiluicon = this.mainUI.getChildByName(IconTag);
        if (shuiluicon) {
            shuiluicon.active = true;
            let shuiluiconlogic = shuiluicon.getComponent('MainUIShuiLuIcon');
            shuiluiconlogic.tips = tips;
        } else {
            cc.loader.loadRes("Prefabs/Activity/ShuiLuIcon", (err, prefab) => {
                let shuiluicon = cc.instantiate(prefab);
                if (shuiluicon) {
                    shuiluicon.name = IconTag;
                    shuiluicon.parent = this.mainUI;

                    let shuiluiconlogic = shuiluicon.getComponent('MainUIShuiLuIcon');
                    shuiluiconlogic.tips = tips;
                }
            });
        }
    },

    justShowShuiLuIcon() {
        this.checkMainUI();
        let shuiluicon = this.mainUI.getChildByName(IconTag);
        if (shuiluicon) {
            shuiluicon.active = true;
        }
    },

    hideShuiLuIcon() {
        this.checkMainUI();
        let shuiluicon = this.mainUI.getChildByName(IconTag);
        if (shuiluicon) {
            shuiluicon.active = false;
        }
    },

    destroyShuiLuIcon() {
        this.checkMainUI();
        let shuiluicon = this.mainUI.getChildByName(IconTag);
        if (shuiluicon) {
            shuiluicon.destroy();
        }
    },

    showShuiLuPanel(data) {
        this.checkMainUI();
        if (data.gamestate == 5) {
            this.destroyShuiLuIcon();
            this.destroyShuiLuPanel();
            return;
        }
        let shuilupanel = this.mainUI.getChildByName(PanelTag);
        if (shuilupanel == null) {
            cc.loader.loadRes("Prefabs/Activity/ShuiLuPanel", (err, prefab) => {
                shuilupanel = cc.instantiate(prefab);
                if (shuilupanel) {
                    shuilupanel.name = PanelTag;
                    shuilupanel.parent = this.mainUI;

                    let panellogic = shuilupanel.getComponent('ShuiLuPanel');
                    panellogic.setData(data);
                }
            });
        } else {
            let panellogic = shuilupanel.getComponent('ShuiLuPanel');
            panellogic.setData(data);
        }
    },

    destroyShuiLuPanel() {
        this.checkMainUI();
        let shuilupanel = this.mainUI.getChildByName(PanelTag);
        if (shuilupanel) {
            shuilupanel.destroy();
        }
    },


    showReadyBattle(data) {
        this.checkMainUI();
        let btlpanel = this.mainUI.getChildByName(BtlTag);
        if (btlpanel == null) {
            cc.loader.loadRes("Prefabs/Activity/ShuiLuBattle", (err, prefab) => {
                btlpanel = cc.instantiate(prefab);
                if (btlpanel) {
                    btlpanel.name = BtlTag;
                    btlpanel.parent = this.mainUI;

                    let btlpanellogic = btlpanel.getComponent('ShuiLuReadyBtl');
                    btlpanellogic.setData(data);
                    btlpanel.runAction(cc.sequence(
                        cc.delayTime(15),
                        cc.callFunc(() => {
                            this.destroyShuiLuReadyBtl();
                        }),
                    ));
                }
            });
        } else {
            let btlpanellogic = btlpanel.getComponent('ShuiLuReadyBtl');
            btlpanellogic.setData(data);
        }
    },

    destroyShuiLuReadyBtl() {
        this.checkMainUI();
        let btlpanel = this.mainUI.getChildByName(BtlTag);
        if (btlpanel) {
            btlpanel.destroy();
        }
    },

    showShuiLuMall() {
        this.checkMainUI();
        let mallpanel = this.mainUI.getChildByName(MallTag);
        if (mallpanel == null) {
            cc.loader.loadRes("Prefabs/Activity/ShuiLuMall", (err, prefab) => {
                let mallpanel = cc.instantiate(prefab);
                if (mallpanel) {
                    mallpanel.name = MallTag;
                    mallpanel.parent = this.mainUI;
                }
            });
        } else {

        }
    },

    showBtlEnd(data) {
        this.checkMainUI();

        let btlendpanel = this.mainUI.getChildByName(BtlEndTag);
        if (btlendpanel == null) {
            cc.loader.loadRes("Prefabs/Activity/ShuiLuBtlEnd", (err, prefab) => {
                btlendpanel = cc.instantiate(prefab);
                if (btlendpanel) {
                    btlendpanel.name = BtlEndTag;
                    btlendpanel.parent = this.mainUI;

                    let btlendpanellogic = btlendpanel.getComponent('ShuiLuBtlEnd');
                    btlendpanellogic.setData(data);
                }
            });
        } else {
            let btlendpanellogic = btlendpanel.getComponent('ShuiLuBtlEnd');
            btlendpanellogic.setData(data);
        }
    },

    destroyShuiLuBtlEnd() {
        this.checkMainUI();
        let btlendpanel = this.mainUI.getChildByName(BtlEndTag);
        if (btlendpanel) {
            btlendpanel.destroy();
        }
        this.changeState(this.sldhstate);
    },
});

SLDHMgr._instance = null;
SLDHMgr.getInstance = function () {
    if (SLDHMgr._instance == null) {
        SLDHMgr._instance = new SLDHMgr()
    }
    return SLDHMgr._instance;
}
module.exports = SLDHMgr;