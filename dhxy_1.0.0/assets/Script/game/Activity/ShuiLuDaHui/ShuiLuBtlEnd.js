cc.Class({
    extends: cc.Component,

    properties: {
        flabel:[cc.Label],
        winLayer:cc.Node,
        lossLayer:cc.Node,
    },

    start(){

    },

    setData(data){
        this.winLayer.active = data.iswin == 1;
        this.lossLayer.active = data.iswin == 0;
        this.flabel[0].string = data.exp;
        this.flabel[1].string = data.petexp;
        this.flabel[2].string = data.score;
        this.flabel[3].string = data.gongji;
    },

    clean(){
        this.node.destroy();
        let ShuiLuMgr = require('./sldh_mgr');
        ShuiLuMgr.justShowShuiLuIcon();
    }
})