
cc.Class({
    extends: cc.Component,

    properties: {
        tipsLab: cc.Label,

        tips: {
            get(){
                return this.tipsLab ? this.tipsLab.string : '';
            },
            set(n){
                this.tipsLab && (this.tipsLab.string = n);
            }
        }
    },

    
    ctor(){
        this.clicked = false;
    },
    start () {

    },

    sendGetInfo(){
        this.clicked = true;
        cc.ll.net.send('c2s_shuilu_info');
        setTimeout(() => {
            this.clicked = false;
        }, 2 * 1000);
    },
});
