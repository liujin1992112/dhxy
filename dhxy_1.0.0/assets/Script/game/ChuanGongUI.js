﻿let GameRes = require('./GameRes');
let CPubFunction = require('./PubFunction');



cc.Class({
    extends: cc.Component,
    properties: {
        svPartnerList: cc.Prefab,
        btnPartner:cc.Prefab,
        nodChuanGongSuccess:cc.Prefab,
    },


    onLoad() {
        this.vecInfo = [];
        this.nLeftSelect = -1;
        this.nRightSelect = -1;
        this.nodLeft = null;
        this.nodRight = null;
        this.nodAdd = null;

        this.nCostWhat = 0;
    },


    Init(vecInfo, nCurSelect) {
        this.vecInfo = vecInfo;

        this.nLeftSelect = nCurSelect;
    },

    start() {
        cc.find('btnClose', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "ChuanGongUI", "Close", 0));

        //-------------------

        this.btnUseMoney = cc.find('btnUseMoney', this.node);
        this.btnUseItem = cc.find('btnUseItem', this.node);

        this.nodLeft = cc.find('nodLeft', this.node);
        this.nodRight = cc.find('nodRight', this.node);
        this.nodAdd = cc.find('nodAdd', this.node);

        cc.find('nodAdd/btnAdd', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "ChuanGongUI", "OpenPartnerList", 0));

        for (let nIndex = 0; nIndex < 2; nIndex++) {
            let strPath = `ToggleContainer/toggle${nIndex}`;
            let goToggle = cc.find(strPath, this.node);

            goToggle.getComponent(cc.Toggle).checkEvents.push(CPubFunction.CreateEventHandler(this.node, "ChuanGongUI", "OnCheck", nIndex));
        }

        this.OnCheck(0, 0);

        cc.find('btnUseItem', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "ChuanGongUI", "OnOK", 0));
        cc.find('btnUseMoney', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "ChuanGongUI", "OnOK", 0));


        this.RefreshPartnerInfo();

    },

    OnCheck(e, nIndex) {
        this.nCostWhat = nIndex;
        CPubFunction.ChangeTeamNodeState([this.btnUseItem, this.btnUseMoney], nIndex);
    },

    RefreshPartnerInfo() {
        if (this.nLeftSelect < 0 || this.nLeftSelect >= this.vecInfo.length)
            return;

        this.SetPartnerInfo(this.nodLeft, this.vecInfo[this.nLeftSelect]);


        if (this.nRightSelect != -1) {
            this.nodRight.active = true;
            this.nodAdd.active = false;
            this.SetPartnerInfo(this.nodRight, this.vecInfo[this.nRightSelect]);
        }
        else {
            this.nodRight.active = false;
            this.nodAdd.active = true;
        }
    },


    OpenPartnerList() {
        let svPartnerList = CPubFunction.CreateSubNode(cc.find('Canvas/MainUI'), { nX: 0, nY: 0 }, this.svPartnerList, 'svPartnerList');

        cc.find('btnClose', svPartnerList).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "ChuanGongUI", "ClosePartnerList", svPartnerList));

        let content = cc.find('ScrollView/view/content', svPartnerList);

        let nY = 30;

        for (var it in this.vecInfo) {
            if (it == this.nLeftSelect)
                continue;

            let stInfo = this.vecInfo[it];

            nY -= 90;
            let goPartner = CPubFunction.CreateSubNode(content, { nX: 140, nY: nY }, this.btnPartner, 'btnPartner');
            goPartner.getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "ChuanGongUI", "SetRight", it));


            CPubFunction.SetSpriteFrame(cc.find('HeadPic/Icon', goPartner), 'Common/huoban', 'huoban_' + stInfo.resid);
            cc.find('Name', goPartner).getComponent(cc.Label).string = stInfo.name;
            cc.find('Level', goPartner).getComponent(cc.Label).string = `${stInfo.relive}转${stInfo.level}级`;
            cc.find('picState', goPartner).active = false;
        }

        content.height = Math.max(content.height, this.vecInfo.length * 90);

    },


    ClosePartnerList(stEvent, goItem) {
        goItem.destroy();
    },

    SetRight(stEvet, stData) {
        this.nRightSelect = stData;
        this.RefreshPartnerInfo();

        CPubFunction.FindAndDeleteNode(cc.find('Canvas/MainUI'), 'svPartnerList');
    },


    SetPartnerInfo(nodPartner, stInfo) {

        CPubFunction.SetSpriteFrame(cc.find('btnHeadPic', nodPartner), 'Common/huoban', 'huoban_' + stInfo.resid);

        cc.find('Name', nodPartner).getComponent(cc.Label).string = stInfo.name;
        cc.find('Level', nodPartner).getComponent(cc.Label).string = `${stInfo.relive}转${stInfo.level}级`;

        let nPercent = Math.floor(this.GetPercent(stInfo) * 100);

        let nNewLen = cc.find('picPecent', nodPartner).width * (nPercent / 100);

        cc.find('picPecent/picBar', nodPartner).width = nNewLen;
        cc.find('picPecent/Label', nodPartner).getComponent(cc.Label).string = `${nPercent}%`;

    },

    GetPercent(stInfo) {
        let stData = CPubFunction.GetLevel(stInfo.exp);
        return stData.nCur / stData.nMax;
    },


    Close() {
        this.node.destroy();
    },

    OnOK() {
        if (this.nLeftSelect < 0 || this.nLeftSelect >= this.vecInfo.length || this.nRightSelect < 0 || this.nRightSelect >= this.vecInfo.length)
            return;

        cc.ll.net.send('c2s_partner_exchange_exp', { nRoleID: cc.ll.player.roleid, nPartnerA: this.vecInfo[this.nLeftSelect].resid, nPartnerB: this.vecInfo[this.nRightSelect].resid, nCostWhat: this.nCostWhat });
    },

    GetInfoByID(nID) {
        for (var it in this.vecInfo) {
            if (this.vecInfo[it].resid == nID)
                return this.vecInfo[it];
        }

        return null;
    },

    OnChuanGongOK(vecInfo, data) {

        this.vecInfo = vecInfo; // 刷新vecInfo 
        let stAInfo = this.GetInfoByID(data.nPartnerA);
        stAInfo.exp = data.nAExp;

        let stBInfo = this.GetInfoByID(data.nPartnerB);
        stBInfo.exp = data.nBExp;


        let goSuccess = CPubFunction.CreateSubNode(cc.find('Canvas/MainUI'), { nX: 0, nY: 0 }, this.nodChuanGongSuccess, 'nodChuanGongSuccess');
        this.SetPartnerInfo(cc.find('Panel/nodLeft', goSuccess), stAInfo);
        this.SetPartnerInfo(cc.find('Panel/nodRight', goSuccess), stBInfo);
        cc.find('btnClose', goSuccess).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(cc.find('Canvas/MainUI'), "MainUI", "CloseUIByName", "nodChuanGongSuccess"));


        let comPartnerUI = cc.find('Canvas/MainUI/PartnerUI').getComponent('PartnerUI')
        comPartnerUI.ChangePartnerExp(data.nPartnerA, data.nAExp);
        comPartnerUI.ChangePartnerExp(data.nPartnerB, data.nBExp);
        comPartnerUI.ShowPartnerList();

        this.Close();

    },



});
