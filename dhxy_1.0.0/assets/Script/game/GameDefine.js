exports.pai = 3.14;

/**
 * 有生命的游戏对象类型
 */
let LivingType = {
	Unknow: 0,
	Player: 1,	//玩家
	NPC: 2,		//Npc
	Monster: 3,	//怪物
	Pet: 4,		//宠物
	Parent: 5,
}

exports.LivingType = LivingType;

let roleName = {
	1001: '逍遥生',
	1002: '俏千金',
	1011: '飞剑侠',
	1012: '燕山雪',
	1031: '神秀生',
	1032: '红拂女',
	2003: '神天兵',
	2004: '玄剑娥',
	2013: '武尊神',
	2014: '玄天姬',
	2033: '龙战将',
	2034: '云中君',
	3005: '虎头怪',
	3006: '狐美人',
	3015: '逆天魔',
	3016: '媚灵狐',
	3035: '混天魔',
	3036: '九尾狐',
	4007: '祭剑魂',
	4008: '夜溪灵',
	4017: '无崖子',
	4018: '幽梦影',
	4037: '南冠客',
	4038: '镜花影',
}
exports.roleName = roleName;


let AttrTypeL1 = {
	DHUNLUAN: 0, // 抗混乱
	DFENGYIN: 1, // 抗封印
	DHUNSHUI: 2, // 抗昏睡
	DDU: 3, // 抗毒
	DFENG: 4, // 抗风
	DHUO: 5, // 抗火
	DSHUI: 6, // 抗水
	DLEI: 7, // 抗雷
	DGUIHUO: 8, // 抗鬼火
	DYIWANG: 9, // 抗遗忘
	DSANSHI: 10, // 抗三尸
	DZHENSHE: 11, // 抗震慑
	DWULI: 12, // 抗物理

	PXISHOU: 13, // 物理吸收
	PMINGZHONG: 14, // 命中
	PSHANBI: 15, // 闪避

	HDHUNLUAN: 16, // 忽视抗混乱
	HDFENGYIN: 17, // 忽视抗封印
	HDHUNSHUI: 18, // 忽视抗昏睡
	HDDU: 19, // 忽视抗毒
	HDFENG: 20, // 忽视抗风
	HDHUO: 21, // 忽视抗火
	HDSHUI: 22, // 忽视抗水
	HDLEI: 23, // 忽视抗雷
	HDGUIHUO: 24, // 忽视抗鬼火
	HDYIWANG: 25, // 忽视抗遗忘
	HDSANSHI: 26, // 忽视抗三尸
	HDZHENSHE: 27, // 忽视抗震慑
	HDWULI: 28, // 忽视抗物理

	HP: 30,
	MAXHP: 31,
	MP: 32,
	MAXMP: 33,
	ATK: 34,
	SPD: 35,

	PLIANJI: 36, //连击
	PLIANJILV: 37, //连击率
	PKUANGBAO: 38, //狂暴
	PPOFANG: 39, //破防
	PPOFANGLV: 40, //破防率
	PFANZHEN: 41, //反震
	PFANZHENLV: 42, //反震率

	ADEFEND: 43, //加防
	ASPD: 44, //加速
	AATK: 45, //加攻
	AHP: 46, //加血
	AMP: 47, //加蓝
	AMEIHUO: 48, //加强魅惑

	PHP: 49, // 整体 百分比 血量
	PMP: 50, // 整体 百分比 法力
	PATK: 51, // 整体 百分比 攻击
	PSPD: 52, // 整体 百分比 速度

	FENGKBPRE: 53, // 风狂暴率
	HUOKBPRE: 54, // 火狂暴率
	SHUIKBPRE: 55, // 水狂暴率
	LEIKBPRE: 56, // 雷狂暴率
	SANSHIKBPRE: 57, // 三尸狂暴率
	GUIHUOKBPRE: 58, // 鬼火狂暴率
	FENGKB: 59, // 风狂暴
	HUOKB: 60, // 火狂暴
	SHUIKB: 61, // 水狂暴
	LEIKB: 62, // 雷狂暴
	SANSHIKB: 63,//三尸狂暴
	GUIHUOKB: 64,//鬼火狂暴

	GOLD: 70,	//金
	WOOD: 71,	//木
	WATER: 72,	//水
	FIRE: 73,	//火
	EARTH: 74,	//土
	KGOLD: 75,	//强力克金
	KWOOD: 76,	//强力克木
	KWATER: 77,	//强力克水
	KFIRE: 78,	//强力克火
	KEARTH: 79,	//强力克土

	GENGU: 100,
	LINGXING: 101,
	LILIANG: 102,
	MINJIE: 103,

	BFSWSHANGXIAN: 1000, //抗冰封睡忘上限
}
exports.AttrTypeL1 = AttrTypeL1;

let AttrTypeL2 = {
	GENGU: 100, // 根骨
	LINGXING: 101, // 灵性
	LILIANG: 102, // 力量
	MINJIE: 103, // 敏捷
}
exports.AttrTypeL2 = AttrTypeL2;

let EquipTypeNumerical = [
	AttrTypeL1.ATK,
	AttrTypeL1.SPD,
	AttrTypeL1.ADEFEND,
	AttrTypeL1.PLIANJI,
	AttrTypeL1.MAXHP,
	AttrTypeL1.MAXMP,
	AttrTypeL1.GENGU,
	AttrTypeL1.LINGXING,
	AttrTypeL1.LILIANG,
	AttrTypeL1.MINJIE,

	AttrTypeL1.GOLD,
	AttrTypeL1.WOOD,
	AttrTypeL1.WATER,
	AttrTypeL1.FIRE,
	AttrTypeL1.EARTH,
];
exports.EquipTypeNumerical = EquipTypeNumerical;


let AttrTypeL1Str = {
	[AttrTypeL1.DHUNLUAN]: '抗混乱',
	[AttrTypeL1.DFENGYIN]: '抗封印',
	[AttrTypeL1.DHUNSHUI]: '抗昏睡',
	[AttrTypeL1.DDU]: '抗毒',
	[AttrTypeL1.DFENG]: '抗风',
	[AttrTypeL1.DHUO]: '抗火',
	[AttrTypeL1.DSHUI]: '抗水',
	[AttrTypeL1.DLEI]: '抗雷',
	[AttrTypeL1.DGUIHUO]: '抗鬼火',
	[AttrTypeL1.DYIWANG]: '抗遗忘',
	[AttrTypeL1.DSANSHI]: '抗三尸',
	[AttrTypeL1.DZHENSHE]: '抗震慑',
	[AttrTypeL1.PXISHOU]: '物理吸收',
	[AttrTypeL1.PMINGZHONG]: '命中率',
	[AttrTypeL1.PSHANBI]: '闪躲率',
	[AttrTypeL1.HDHUNLUAN]: '忽视抗混乱',
	[AttrTypeL1.HDFENGYIN]: '忽视抗封印',
	[AttrTypeL1.HDHUNSHUI]: '忽视抗昏睡',
	[AttrTypeL1.HDDU]: '忽视抗毒',
	[AttrTypeL1.HDFENG]: '忽视抗风',
	[AttrTypeL1.HDHUO]: '忽视抗火',
	[AttrTypeL1.HDSHUI]: '忽视抗水',
	[AttrTypeL1.HDLEI]: '忽视抗雷',
	[AttrTypeL1.HDGUIHUO]: '忽视抗鬼火',
	[AttrTypeL1.HDYIWANG]: '忽视抗遗忘',
	[AttrTypeL1.HDSANSHI]: '忽视抗三尸',
	[AttrTypeL1.HDZHENSHE]: '忽视抗震慑',
	[AttrTypeL1.DWULI]: '抗物理',
	[AttrTypeL1.HDWULI]: '忽视抗物理',
	[AttrTypeL1.PLIANJI]: '连击',
	[AttrTypeL1.PLIANJILV]: '连击率',
	[AttrTypeL1.PKUANGBAO]: '狂暴',
	[AttrTypeL1.PPOFANG]: '破防程度',
	[AttrTypeL1.PPOFANGLV]: '破防率',
	[AttrTypeL1.PFANZHEN]: '反震程度',
	[AttrTypeL1.PFANZHENLV]: '反震率',
	[AttrTypeL1.ADEFEND]: '加强防御',
	[AttrTypeL1.ASPD]: '加速',
	[AttrTypeL1.AATK]: '加攻',
	[AttrTypeL1.AHP]: '加血',
	[AttrTypeL1.AMP]: '加法',
	[AttrTypeL1.AMEIHUO]: '加强魅惑',
	[AttrTypeL1.MAXHP]: '增加气血上限',
	[AttrTypeL1.MAXMP]: '增加法力上限',
	[AttrTypeL1.ATK]: '攻击',
	[AttrTypeL1.SPD]: '速度',
	[AttrTypeL1.HP]: '气血',
	[AttrTypeL1.MP]: '法力',

	[AttrTypeL1.PHP]: '气血改变',
	[AttrTypeL1.PMP]: '法力改变',
	[AttrTypeL1.PATK]: '攻击改变',
	[AttrTypeL1.PSPD]: '速度改变',

	[AttrTypeL1.SHUIKBPRE]: '水系狂暴率',
	[AttrTypeL1.LEIKBPRE]: '雷系狂暴率',
	[AttrTypeL1.HUOKBPRE]: '火系狂暴率',
	[AttrTypeL1.FENGKBPRE]: '风系狂暴率',
	[AttrTypeL1.SANSHIKBPRE]: '三尸狂暴率',
	[AttrTypeL1.GUIHUOKBPRE]: '鬼火狂暴率',
	[AttrTypeL1.SHUIKB]: '水狂暴',
	[AttrTypeL1.LEIKB]: '雷狂暴',
	[AttrTypeL1.HUOKB]: '火狂暴',
	[AttrTypeL1.FENGKB]: '风狂暴',
	[AttrTypeL1.SANSHIKB]: '三尸狂暴',
	[AttrTypeL1.GUIHUOKB]: '鬼火狂暴',

	[AttrTypeL1.GOLD]: '金',
	[AttrTypeL1.WOOD]: '木',
	[AttrTypeL1.WATER]: '水',
	[AttrTypeL1.FIRE]: '火',
	[AttrTypeL1.EARTH]: '土',
	[AttrTypeL1.KGOLD]: '强力克金',
	[AttrTypeL1.KWOOD]: '强力克木',
	[AttrTypeL1.KWATER]: '强力克水',
	[AttrTypeL1.KFIRE]: '强力克火',
	[AttrTypeL1.KEARTH]: '强力克土',

	[AttrTypeL1.GENGU]: '根骨',
	[AttrTypeL1.LINGXING]: '灵性',
	[AttrTypeL1.LILIANG]: '力量',
	[AttrTypeL1.MINJIE]: '敏捷',
}
exports.AttrTypeL1Str = AttrTypeL1Str;

let AttrTypeL2Str = {
	[AttrTypeL2.GENGU]: '根骨',
	[AttrTypeL2.LINGXING]: '灵性',
	[AttrTypeL2.LILIANG]: '力量',
	[AttrTypeL2.MINJIE]: '敏捷',
}
exports.AttrTypeL2Str = AttrTypeL2Str;

let AttrEquipTypeStr = {
	'FatalRate': AttrTypeL1.PKUANGBAO, // 致命（狂暴）%       
	'HitRate': AttrTypeL1.PMINGZHONG, // 命中%       
	'PhyDefNef': AttrTypeL1.PPOFANG, // 破防程度%     
	'PhyDefNefRate': AttrTypeL1.PPOFANGLV, // 破防概率%     
	'AdAtkEhan': AttrTypeL1.AATK, // 加强攻击%     
	'Atk': AttrTypeL1.ATK, //攻击（原数增加）
	'AdSpdEhan': AttrTypeL1.ASPD, // 加强速度%     
	'HpMax': AttrTypeL1.MAXHP, // 增加气血上限        
	'MpMax': AttrTypeL1.MAXMP, // 增加法力上限        
	'HpPercent': AttrTypeL1.AHP, // 气血%       
	'MpPercent': AttrTypeL1.AMP, // 法力%       
	'AtkPercent': AttrTypeL1.PATK,//攻击变为%
	'Speed': AttrTypeL1.SPD, // 速度 （原数增加）     
	'RainDef': AttrTypeL1.DSHUI, // 抗水法%      
	'ThunderDef': AttrTypeL1.DLEI, // 抗雷法%      
	'FireDef': AttrTypeL1.DHUO, // 抗火法%      
	'WindDef': AttrTypeL1.DFENG, // 抗风法%      
	'RainDefNeg': AttrTypeL1.HDSHUI, // 忽视水%      
	'ThunderDefNeg': AttrTypeL1.HDLEI, // 忽视雷%      
	'FireDefNeg': AttrTypeL1.HDHUO, // 忽视火%      
	'WindDefNeg': AttrTypeL1.HDFENG, // 忽视风%      
	'SealDef': AttrTypeL1.DFENGYIN, // 抗封印%      
	'DisorderDef': AttrTypeL1.DHUNLUAN, // 抗混乱%      
	'SleepDef': AttrTypeL1.DHUNSHUI, // 抗昏睡%      
	'PoisonDef': AttrTypeL1.DDU, // 抗中毒%      
	'SealDefNeg': AttrTypeL1.HDFENGYIN, // 忽视封印%     
	'DisorderDefNeg': AttrTypeL1.HDHUNLUAN, // 忽视混乱%     
	'SleepDefNeg': AttrTypeL1.HDHUNSHUI, // 忽视昏睡%     
	'PoisonDefNeg': AttrTypeL1.HDDU, // 忽视毒%      
	'ForgetDef': AttrTypeL1.DYIWANG, // 抗遗忘%      
	'GfireDef': AttrTypeL1.DGUIHUO, // 抗鬼火%      
	'SanshiDef': AttrTypeL1.DSANSHI, // 抗三尸%      
	'ForgetDefNeg': AttrTypeL1.HDYIWANG, // 忽视遗忘%     
	'GfireDefNeg': AttrTypeL1.HDGUIHUO, // 忽视鬼火%     
	'SanshiDefNeg': AttrTypeL1.HDSANSHI, // 忽视三尸%     
	'ShockDefNeg': AttrTypeL1.HDZHENSHE, // 忽视抗震慑%        
	'CharmEhan': AttrTypeL1.AMEIHUO, // 加强魅惑%     
	'PhyDef': AttrTypeL1.PXISHOU, // 物理吸收%     
	'AdDefEhan': AttrTypeL1.ADEFEND, //加强加防%
	'ShockDef': AttrTypeL1.DZHENSHE, //抗震慑%
	'HitCombo': AttrTypeL1.PLIANJI, //连击次数
	'VoidRate': AttrTypeL1.PSHANBI, //躲闪%

	'RainFatalRate': AttrTypeL1.SHUIKBPRE,
	'ThunderFatalRate': AttrTypeL1.LEIKBPRE,
	'FireFatalRate': AttrTypeL1.HUOKBPRE,
	'WindFatalRate': AttrTypeL1.FENGKBPRE,
	'SanshiFatalRate': AttrTypeL1.SANSHIKBPRE,
	'GfireFatalRate': AttrTypeL1.GUIHUOKBPRE,
	'RainFatalHurt': AttrTypeL1.SHUIKB,
	'ThunderFatalHurt': AttrTypeL1.LEIKB,
	'FireFatalHurt': AttrTypeL1.HUOKB,
	'WindFatalHurt': AttrTypeL1.FENGKB,
	'SanshiFatalHurt': AttrTypeL1.SANSHIKB,
	'GfireFatalHurt': AttrTypeL1.GUIHUOKB,

	'Kgold': AttrTypeL1.KGOLD,
	'Kwood': AttrTypeL1.KWOOD,
	'Kwater': AttrTypeL1.KWATER,
	'Kfire': AttrTypeL1.KFIRE,
	'Kearth': AttrTypeL1.KEARTH,


	'Basecon': AttrTypeL2.GENGU, // 根骨
	'Wakan': AttrTypeL2.LINGXING, // 灵性
	'Power': AttrTypeL2.LILIANG, // 力量
	'Agility': AttrTypeL2.MINJIE, // 敏捷
}
exports.AttrEquipTypeStr = AttrEquipTypeStr;

// 战斗回合动作
let ActType = {
	Skill: 1,	//技能
	Item: 2,	//道具
	Summon: 3,	//召唤
	RunAway: 4, //逃跑
	Protect: 5, //保护
	Catch: 6, 	//捕捉
	SummonBack: 7, //召还
}
exports.ActType = ActType;

// 战斗回合内 响应
let BtlRespone = {
	NoThing: 0, // 无响应
	Defense: 1, // 防御
	Dodge: 2, // 闪避
	CriticalHit: 3, // 暴击
	Catched: 4, // 被抓
	NoCatch: 5, // 不能被抓
	CatchFail: 6, // 捕捉失败
	Protect: 7,
	BeProtected: 8, //被保护
	SummonBack: 9,
	Summon: 10,
	SummonFaild: 11,
	PoFang: 12,
}
exports.BtlRespone = BtlRespone;


let RaceType = {
	Unknow: 0,
	Humen: 1,
	Sky: 2,
	Demon: 3,
	Ghost: 4,
}


let SexType = {
	Unknow: 0,
	Male: 1,
	Female: 2,
}

exports.SexType = SexType;



// 转生修正
let reliveFixAttr1 = {
	[RaceType.Humen]: {
		[SexType.Male]: {
			[AttrTypeL1.DHUNLUAN]: 10,
			[AttrTypeL1.DFENGYIN]: 10,
			[AttrTypeL1.DHUNSHUI]: 10,
		},
		[SexType.Female]: {
			[AttrTypeL1.DDU]: 10,
			[AttrTypeL1.DFENGYIN]: 10,
			[AttrTypeL1.DHUNSHUI]: 10,
		},
	},
	[RaceType.Demon]: {
		[SexType.Male]: {
			[AttrTypeL1.HP]: 8.2,
			[AttrTypeL1.MP]: 8.2,
			[AttrTypeL1.SPD]: 6.15,
		},
		[SexType.Female]: {
			[AttrTypeL1.HP]: 8.2,
			[AttrTypeL1.MP]: 8.2,
			[AttrTypeL1.DZHENSHE]: 9.2,
		},
	},
	[RaceType.Ghost]: {
		[SexType.Male]: {
			[AttrTypeL1.DGUIHUO]: 10,
			[AttrTypeL1.DYIWANG]: 10,
			[AttrTypeL1.DSANSHI]: 10,
		},
		[SexType.Female]: {
			[AttrTypeL1.DGUIHUO]: 10,
			[AttrTypeL1.DYIWANG]: 10,
			[AttrTypeL1.DWULI]: 15.3,
		},
	},
	[RaceType.Sky]: {
		[SexType.Male]: {
			[AttrTypeL1.DLEI]: 10,
			[AttrTypeL1.DSHUI]: 10,
			[AttrTypeL1.DFENG]: 10,
		},
		[SexType.Female]: {
			[AttrTypeL1.DLEI]: 10, // 抗混乱
			[AttrTypeL1.DSHUI]: 10, // 抗封印
			[AttrTypeL1.DHUO]: 10, // 抗昏睡
		},
	},
}
exports.reliveFixAttr1 = reliveFixAttr1;

// 转生修正
let reliveFixAttr2 = {
	[RaceType.Humen]: {
		[SexType.Male]: {
			[AttrTypeL1.DHUNLUAN]: 15,
			[AttrTypeL1.DFENGYIN]: 15,
			[AttrTypeL1.DHUNSHUI]: 15,
		},
		[SexType.Female]: {
			[AttrTypeL1.DDU]: 15,
			[AttrTypeL1.DFENGYIN]: 15,
			[AttrTypeL1.DHUNSHUI]: 15,
		},
	},
	[RaceType.Demon]: {
		[SexType.Male]: {
			[AttrTypeL1.HP]: 12.3,
			[AttrTypeL1.MP]: 12.3,
			[AttrTypeL1.SPD]: 9.23,
		},
		[SexType.Female]: {
			[AttrTypeL1.HP]: 12.3,
			[AttrTypeL1.MP]: 12.3,
			[AttrTypeL1.DZHENSHE]: 13.8,
		},
	},
	[RaceType.Ghost]: {
		[SexType.Male]: {
			[AttrTypeL1.DGUIHUO]: 15,
			[AttrTypeL1.DYIWANG]: 15,
			[AttrTypeL1.DSANSHI]: 15,
		},
		[SexType.Female]: {
			[AttrTypeL1.DGUIHUO]: 15,
			[AttrTypeL1.DYIWANG]: 15,
			[AttrTypeL1.DWULI]: 23,
		},
	},
	[RaceType.Sky]: {
		[SexType.Male]: {
			[AttrTypeL1.DLEI]: 15,
			[AttrTypeL1.DSHUI]: 15,
			[AttrTypeL1.DFENG]: 15,
		},
		[SexType.Female]: {
			[AttrTypeL1.DLEI]: 15, // 抗混乱
			[AttrTypeL1.DSHUI]: 15, // 抗封印
			[AttrTypeL1.DHUO]: 15, // 抗昏睡
		},
	},
}
exports.reliveFixAttr2 = reliveFixAttr2;

// 转生修正
let reliveFixAttr3 = {
	[RaceType.Humen]: {
		[SexType.Male]: {
			[AttrTypeL1.DHUNLUAN]: 20,
			[AttrTypeL1.DFENGYIN]: 20,
			[AttrTypeL1.DHUNSHUI]: 20,
		},
		[SexType.Female]: {
			[AttrTypeL1.DDU]: 20,
			[AttrTypeL1.DFENGYIN]: 20,
			[AttrTypeL1.DHUNSHUI]: 20,
		},
	},
	[RaceType.Demon]: {
		[SexType.Male]: {
			[AttrTypeL1.HP]: 16.4,
			[AttrTypeL1.MP]: 16.4,
			[AttrTypeL1.SPD]: 12.3,
		},
		[SexType.Female]: {
			[AttrTypeL1.HP]: 16.4,
			[AttrTypeL1.MP]: 16.4,
			[AttrTypeL1.DZHENSHE]: 18.5,
		},
	},
	[RaceType.Ghost]: {
		[SexType.Male]: {
			[AttrTypeL1.DGUIHUO]: 20,
			[AttrTypeL1.DYIWANG]: 20,
			[AttrTypeL1.DSANSHI]: 20,
		},
		[SexType.Female]: {
			[AttrTypeL1.DGUIHUO]: 20,
			[AttrTypeL1.DYIWANG]: 20,
			[AttrTypeL1.DWULI]: 30.6,
		},
	},
	[RaceType.Sky]: {
		[SexType.Male]: {
			[AttrTypeL1.DLEI]: 20,
			[AttrTypeL1.DSHUI]: 20,
			[AttrTypeL1.DFENG]: 20,
		},
		[SexType.Female]: {
			[AttrTypeL1.DLEI]: 20, // 抗混乱
			[AttrTypeL1.DSHUI]: 20, // 抗封印
			[AttrTypeL1.DHUO]: 20, // 抗昏睡
		},
	},
}
exports.reliveFixAttr3 = reliveFixAttr3;

/**获取地图对应的声音 */
let MapAudio = {
	[1010]: 'xinshou',
	[1011]: 'changan',
	[1201]: 'haorizi',
}
function getMapAudio(mapid) {
	let audio = MapAudio[mapid];
	if (audio == null) {
		audio = 'yewai';
	}
	return audio;
}
exports.getMapAudio = getMapAudio;



class CArriveCallBack {
	constructor(x, y, callback, data) {
		this.x = x;
		this.y = y;
		this.callback = callback;
		this.data = data;
	}
}

exports.CArriveCallBack = CArriveCallBack;

let EChuZhanState = {
	ERest: 0,
	EFight: 1
}

module.exports.EChuZhanState = EChuZhanState;



let EDir = {
	EDown: 0,
	ELeft: 1,
	EUp: 2,
	ERight: 4
}

module.exports.EDir = EDir;

let limitWordList = [
	'系统',
	'官方',
	'测试',
	'gm',
	'Gm',
	'GM',
	'管理',
	'Q群',
	'QQ',
	'微信',
	'内测',
	'内部',
	'技术',
	'公告',
	'公测',
	'垃圾',
	'裙',
	'毛泽东',
	'周恩来',
	'刘少奇',
	'习近平',
	'李克强',
	'群',
	'君羊',
	'朱德',
	'丁磊',
	'操',
	'日',
	'干',
	'你妈',
]
exports.limitWordList = limitWordList;



let TitleType = {
	IMGTitle: 0,		//图片类称谓
	CommonTitle: 1,		//普通文字称谓，如帮派类称谓
	BroTitle: 2,		//结拜类称谓	
	CoupleTitle: 3,		//夫妻类称谓
}

exports.TitleType = TitleType;

let RoleTitleInfoType = {
	Admin: 1,
	ShuiLuZhanShen: 86,
	SanCaiTongZi: 43,
	Couple: 200,
	Brother: 201,
	BangZhong: 221,
	TangZhu: 222,
	ZhangLao: 223,
	YouHuFa: 224,
	ZuoHuFa: 225,
	FuBangZhu: 226,
	BangZhu: 227,
}

exports.RoleTitleInfoType = RoleTitleInfoType;

let RoleTitleList = [

	{ id: 1, name: '游戏管理员', desc: '负责管理游戏内各种违规现象', type: 0 },

	{ id: 86, name: '水陆战神', desc: '水陆大会的不败战神', type: 0 },
	{ id: 43, name: '善财童子', desc: '有钱任性', type: 0 },

	{ id: 20, name: '天人合一', desc: '天与地合，人与天合', type: 0 },
	{ id: 29, name: '驰骋江湖', desc: '驰骋江湖，快意恩仇', type: 0 },
	{ id: 30, name: '马到成功', desc: '马都到了，事能不成吗？', type: 0 },
	{ id: 43, name: '财大气粗', desc: '有钱，说话就是硬气', type: 0 },
	{ id: 44, name: '超群绝伦', desc: '人群中你最亮眼', type: 0 },
	{ id: 46, name: '丰功伟绩', desc: '前无古人，后无来者', type: 0 },
	{ id: 47, name: '富甲天下', desc: '天底下最有钱的人', type: 0 },
	{ id: 56, name: '武林高手', desc: '武林中已经有你这号人了', type: 0 },
	{ id: 57, name: '武林奇才', desc: '骨骼惊奇，练武奇才', type: 0 },
	{ id: 58, name: '武林英杰', desc: '天下武林，英雄豪杰', type: 0 },
	{ id: 59, name: '武林至尊', desc: '武林上下，唯吾独尊', type: 0 },
	{ id: 60, name: '武林宗师', desc: '武林中，德高望重，人人敬仰', type: 0 },
	{ id: 65, name: '三界豪杰', desc: '三界之内，英雄豪杰', type: 0 },
	{ id: 78, name: '三界首善', desc: '三界之内的大善人', type: 0 },
	{ id: 96, name: '天地英雄', desc: '天地之间，汝为英雄', type: 0 },




	{ id: 200, name: '夫妻', desc: '夫妻搭配，干活不累', type: 3 },
	{ id: 201, name: '结拜', desc: '打架，拼的就是人多', type: 2 },


	{ id: 221, name: '帮众', desc: '的小虾米，只能打酱油', type: 1 },
	{ id: 222, name: '堂主', desc: '的堂主，大小也算是个干部了', type: 1 },
	{ id: 223, name: '长老', desc: '的长老，帮会中层，有望更上一层', type: 1 },
	{ id: 224, name: '右护法', desc: '的右护法，帮会高层，参与帮务管理', type: 1 },
	{ id: 225, name: '左护法', desc: '的左护法，帮会高层，参与帮务管理', type: 1 },
	{ id: 226, name: '副帮主', desc: '的副帮主，一人之上，万人之下', type: 1 },
	{ id: 227, name: '帮主', desc: '的帮主，帮会上下，唯吾独尊', type: 1 },

	/*
		20 29 30 43 44 46 47 56 
		57 58 59 60 65 78 1  96
	*/

];


exports.RoleTitleList = RoleTitleList;


let RelationType = {
	Brother: 0,	  	// 结拜
	Couple: 1,     	// 结婚
}
exports.RelationType = RelationType;

let RelationTypeMembersCount = {
	[RelationType.Brother]: 5,
	[RelationType.Couple]: 2,
}
exports.RelationTypeMembersCount = RelationTypeMembersCount;

let relationActionType = {
	Bro_Create: 0,
	Bro_Leave: 1,
	Bro_Destroy: 2,
}
exports.relationActionType = relationActionType;

//target:1 玩家，2 NPC
let relationMsg = [
	{ action: 0, msg1: '苍天在上，我与', msg2: '在此义结金兰，今后有福同享有难同当[32]', target: 1 },
	{ action: 0, msg1: '不求同年同月同日生，但求同年同月日死[32]', msg2: '', target: 1 },
	{ action: 0, msg1: '皇天在上，厚土在下，此心明鉴，永不负约[32]', msg2: '', target: 1 },
	{ action: 0, msg1: '[23]树有双生之宜，人有同心之交，恭喜', msg2: '人能遇见有缘的对方[23]', target: 2 },
	{ action: 0, msg1: '望', msg2: '人能够相伴终生，祝此情永存[23]', target: 2 },
]

exports.relationMsg = relationMsg;


exports.schemeUseMoney = 10000000;

exports.schemeActiveMoney = 100000;

exports.resetXiulian = 200000;
