
let CPubFunction = require('./PubFunction');
cc.Class({
    extends: cc.Component,
    properties: {
        mainLayer: cc.Node,
        aboveLayer: cc.Node,

        aboveMapNode: cc.Node,
        aboveCloseNode: cc.Node,

        mapBtnNode: [cc.Node],
        moveNode: cc.Node,
        endNode: cc.Node,
        roleNode: cc.Node,
    },

    onLoad() {
        this.aboveMapNode.on(cc.Node.EventType.TOUCH_START, this.touchBegan.bind(this));
        this.aboveMapNode.on(cc.Node.EventType.TOUCH_MOVE, this.touchMoved.bind(this));
        this.aboveMapNode.on(cc.Node.EventType.TOUCH_END, this.touchEnded.bind(this));
        this.aboveMapNode.on(cc.Node.EventType.TOUCH_CANCEL, this.touchCanceled.bind(this));

        // this.aboveMapNode.on(cc.Node.EventType.TOUCH_START, this.aboveTouchBegan.bind(this));
        this.moveNodes = [];
        this.gameLogic = this.node.parent.getComponent('GameLogic');
        this.roleNode.zIndex = 2;
    },

    init() {
        this.mainLayer.active = true;
        this.aboveLayer.active = false;
    },

    start() {
        console.log('mainmap start');
    },

    onDisable() {
        if (this.mapId) {
            // cc.loader.releaseRes(`Map/map/${this.mapId}`);
            cc.loader.releaseRes(`Map/map/${this.mapId}`, cc.Texture2D);
            cc.loader.releaseRes(`Map/map/${this.mapId}`, cc.SpriteFrame);
        }
    },

    onMapBtnClicked(e, d) {
        this.showAboveNode(d);
    },

    showAboveNode(mapid) {
        this.aboveMapNode.destroyAllChildren();
        this.mapId = mapid;
        let pPos = this.getPlayerPos();
        this.roleNode.setPosition(pPos);
        this.roleNode.active = false;
        this.unschedule(this.checkPlayerMove);
        for (let j = 0; j < this.moveNodes.length; j++) {
            this.moveNodes[j].active = false;
        }
        this.endNode.active = false;

        if (this.mapId == cc.ll.player.mapid) {
            this.roleNode.active = true;
            this.unschedule(this.checkPlayerMove);
            for (let j = 0; j < this.moveNodes.length; j++) {
                this.moveNodes[j].active = true;
            }
            if (this.moveNodes.length > 0) {
                this.endNode.active = true;
            }
            this.schedule(this.checkPlayerMove, 0.02, cc.macro.REPEAT_FOREVER);
        }
        let self = this;
        let mapres = cc.ll.propData.mapdata[this.mapId].mapid;
        cc.loader.loadRes(`Map/map/${mapres}`, cc.SpriteFrame, function (err, frame) {
            if (!err) {
                self.aboveMapNode.getComponent(cc.Sprite).spriteFrame = frame;
                self.aboveLayer.active = true;
                self.scheduleOnce(() => {
                    self.aboveCloseNode.x = self.aboveMapNode.width / 2;
                    self.aboveLayer.getChildByName('mapbg').width = self.aboveMapNode.width + 110;
                    self.aboveLayer.getChildByName('titleline').width = self.aboveMapNode.width + 120;

                    self.aboveLayer.getChildByName('pos').getComponent(cc.Label).string = cc.ll.propData.mapdata[mapid].map_name;
                    self.addMapNode();
                }, 0);

            }
            else {
                self.node.active = false;
            }
        });
    },

    addMapNode() {
        for (const key in cc.ll.propData.npc) {
            if (key == 'datatype') {
                continue;
            }
            let npc = cc.ll.propData.npc[key];
            let vecTmp = npc.auto_create.split(';');
            if (vecTmp.length < 3 || vecTmp[0] != this.mapId) {
                continue;
            }
            let npcNode = cc.instantiate(this.aboveLayer.getChildByName('npc'));
            npcNode.active = true;
            npcNode.parent = this.aboveMapNode;
            npcNode.getComponent(cc.Label).string = npc.name;
            npcNode.setPosition(this.getMapPosByGrid({ l: vecTmp[1], r: vecTmp[2] }));
        }
        for (const key in cc.ll.propData.transfer) {
            if (key == 'datatype') {
                continue;
            }
            let trans = cc.ll.propData.transfer[key];
            if (trans.mapid != this.mapId) {
                continue;
            }
            let transferNode = cc.instantiate(this.aboveLayer.getChildByName('transfer'));
            transferNode.active = true;
            transferNode.parent = this.aboveMapNode;
            transferNode.getComponent(cc.Label).string = '至' + cc.ll.propData.mapdata[trans.tomap].map_name;
            transferNode.setPosition(this.getMapPosByGrid({ l: trans.mapx, r: trans.mapy }));
        }
    },

    onCloseBtnClicked(e, d) {
        cc.ll.AudioMgr.playCloseAudio();
        this.node.active = false;
    },

    onSearchBtnClicked(e, d) {

    },

    onBackBtnClicked(e, d) {
        this.mainLayer.active = false;

        if (cc.ll.propData.mapdata[cc.ll.player.mapid].bmap == 0) {
            this.node.active = false;
            return;
        }
        this.showAboveNode(cc.ll.player.mapid);
    },

    onHomeBtnClicked(e, d) {
        if (cc.ll.player.mapid == 1201) {
            CPubFunction.CreateNotice(cc.find('Canvas'), '本场景禁止传送，请与天牢守卫对话离开。', 2);
            return;
        }
        this.gameLogic.mapLogic.changeMap(4001);
        this.node.active = false;
    },

    onAboveCloseBtnClicked(e, d) {
        cc.ll.AudioMgr.playCloseAudio();
        this.aboveLayer.active = false;
        if (this.mainLayer.active == false) {
            this.node.active = false;
        }
    },

    getPlayerPos() {
        let pPos = this.gameLogic.mapLogic.roleNode.getPosition();
        return this.getMapPos(pPos);
    },

    checkPlayerMove() {
        if (this.moveNodes.length == 0 && this.endNode.active == false) {
            this.unschedule(this.checkPlayerMove);
            this.gameLogic.autoNode.active = false;
        }

        let realpos = this.gameLogic.mapLogic.selfHeroLogic.netInfo;
        this.aboveLayer.getChildByName('pos').getComponent(cc.Label).string = cc.ll.propData.mapdata[this.mapId].map_name + `(${realpos.x},${realpos.y})`;
        let pPos = this.getPlayerPos();
        this.roleNode.setPosition(pPos);

        let curIndex = -1;
        for (let i = 0; i < this.moveNodes.length; i++) {
            const n = this.moveNodes[i];
            if (cc.ll.pDistance(pPos, n.getPosition()) < 20) {
                curIndex = i;
            }
        }
        if (cc.ll.pDistance(pPos, this.endNode.getPosition()) < 20) {
            this.endNode.active = false;
            curIndex = this.moveNodes.length - 1;
        }
        if (curIndex != -1) {
            for (let j = 0; j <= curIndex; j++) {
                this.moveNodes[j].destroy();
            }
            this.moveNodes.splice(0, curIndex + 1);
        }
    },

    removePathNodes() {
        this.unschedule(this.checkPlayerMove);
        for (let j = 0; j < this.moveNodes.length; j++) {
            this.moveNodes[j].destroy();
        }
        this.moveNodes = [];
        this.endNode.active = false;
    },

    showMovePoints(result) {
        for (const n of this.moveNodes) {
            n.destroy();
        }
        this.moveNodes = [];
        // cc.v2g()
        this.endNode.active = true;
        let showPoints = [];
        let MAP_RES_WIDTH = cc.ll.propData.mapdata[this.mapId].width;
        let dotdistance = (MAP_RES_WIDTH / this.aboveMapNode.width) * 1.5;
        let prepoint = result[0];
        for (let index = 0; index < result.length - 5; index++) {
            if (cc.ll.pDistance(cc.v2(prepoint.l, prepoint.r), cc.v2(result[index].l, result[index].r)) > dotdistance) {
                prepoint = result[index];
                showPoints.push(result[index]);
            }
        }
        // for (let index = dotdistance; index < result.length - dotdistance; index += dotdistance) {
        //     showPoints.push(result[index]);
        // }
        for (let index = 0; index < showPoints.length; index++) {
            let move = cc.instantiate(this.moveNode);
            move.active = true;
            move.setPosition(this.getMapPosByGrid(showPoints[index]));
            move.parent = this.aboveLayer;
            this.moveNodes.push(move);
        }
        this.endNode.setPosition(this.getMapPosByGrid(result[result.length - 1]));

        this.roleNode.active = true;
        this.schedule(this.checkPlayerMove, 0, cc.macro.REPEAT_FOREVER);
    },

    getMapPosByGrid(info) {
        let mapres = cc.ll.propData.mapdata[this.mapId].mapid;
        let mapData = cc.ll.propData.map[mapres];
        let gridwidth = mapData.baseInfo.grid_width;
        let gridheight = mapData.baseInfo.grid_height;
        let realPos = cc.v2(gridwidth * (0.5 + Number(info.l)), gridheight * (0.5 + Number(info.r)));
        return this.getMapPos(realPos);
    },

    getMapPos(realPos) {
        let MAP_RES_WIDTH = cc.ll.propData.mapdata[this.mapId].width;
        let MAP_RES_HEIGHT = cc.ll.propData.mapdata[this.mapId].height;
        let mapPosX = realPos.x * (this.aboveMapNode.width / MAP_RES_WIDTH) - this.aboveMapNode.width / 2;
        let mapPosY = realPos.y * (this.aboveMapNode.height / MAP_RES_HEIGHT) - this.aboveMapNode.height / 2;
        return cc.v2(mapPosX, mapPosY);
    },

    mapNodeClicked(e, d) {
        this.touchPos(cc.ll.pAdd(e.target.getPosition(), cc.v2(this.aboveMapNode.width / 2, this.aboveMapNode.height / 2)));
        this.gameLogic.autoNode.active = true;
    },

    touchPos(mapPos) {
        if (cc.ll.player.teamid > 0 && !cc.ll.player.isleader) {
            return;
        }

        if (this.mapId != cc.ll.player.mapid) {
            if (cc.ll.player.mapid == 1201) {
                if (cc.ll.player.shane > 0) {
                    CPubFunction.CreateNotice(cc.find('Canvas'), '天牢不是你想来就来想走就能走的。', 2);
                } else {
                    CPubFunction.CreateNotice(cc.find('Canvas'), '本场景禁止传送，请与天牢守卫对话离开。', 2);
                }
                this.node.active = false;
                return;
            }
            this.gameLogic.mapLogic.changeMap(this.mapId);
        }
        let realPosX = (mapPos.x / this.aboveMapNode.width) * this.gameLogic.mapLogic.MAP_RES_WIDTH;
        let realPosY = (mapPos.y / this.aboveMapNode.height) * this.gameLogic.mapLogic.MAP_RES_HEIGHT;
        let result = this.gameLogic.mapLogic.touchPos(cc.v2(realPosX, realPosY));
        if (result.length > 0) {
            this.showMovePoints(result);
            this.mainLayer.active = false;
        }
    },

    touchBegan(event) {
        if (this.node.active == false) {
            return;
        }
        if (this.aboveLayer.active == true) {
            let touchLoc = event.getLocation();
            if (!this.aboveLayer.active || !this.aboveMapNode.getBoundingBoxToWorld().contains(touchLoc)) {
                return;
            }
            let mapPos = this.aboveMapNode.convertToNodeSpace(touchLoc);
            this.touchPos(mapPos);
            return;
        }
        // for (const btn of this.mapBtnNode) {
        //     btn.getComponent('CustomButton').touchBegan(event);
        // }
    },

    touchMoved(event) {
        if (this.node.active == false || this.aboveLayer.active == true) {
            return;
        }
        // for (const btn of this.mapBtnNode) {
        //     btn.getComponent('CustomButton').touchMoved(event);
        // }
    },

    touchEnded(event) {
        if (this.node.active == false || this.aboveLayer.active == true) {
            return;
        }
        // for (const btn of this.mapBtnNode) {
        //     btn.getComponent('CustomButton').touchEnded(event);
        // }
    },

    touchCanceled(evene) {
        if (this.node.active == false || this.aboveLayer.active == true) {
            return;
        }
        // for (const btn of this.mapBtnNode) {
        //     btn.getComponent('CustomButton').touchCanceled(event);
        // }
    }
});
