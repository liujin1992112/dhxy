let PanelName = 'SetRoleTitlePanel';
class SetRoleTitleUIMgr{
    

    showTitleList(data){
        this.mainUI = cc.find('Canvas');
        let setRoleTitlePanel = this.mainUI.getChildByName(PanelName);
        if(setRoleTitlePanel){
            let panellogic = setRoleTitlePanel.getComponent('SetRoleTitleUI');
            panellogic.initScrollViewTitle(data);
        }else{
            cc.loader.loadRes("Prefabs/SetRoleTitleUI", (err, prefab) => {
                let setRoleTitlePanel = cc.instantiate(prefab);
                if (setRoleTitlePanel) {
                    setRoleTitlePanel.name = PanelName;
                    setRoleTitlePanel.parent = this.mainUI;
        
                    let panellogic = setRoleTitlePanel.getComponent('SetRoleTitleUI');
                    panellogic.initScrollViewTitle(data);
                }
            });
        }
    }


    onRoleTitleChanged(data){
        this.mainUI = cc.find('Canvas');
        let setRoleTitlePanel = this.mainUI.getChildByName(PanelName);
        if(setRoleTitlePanel){
            let panellogic = setRoleTitlePanel.getComponent('SetRoleTitleUI');
            panellogic.changeBtnLoadStatus(data);
        }else{
            cc.loader.loadRes("Prefabs/SetRoleTitleUI", (err, prefab) => {
                let setRoleTitlePanel = cc.instantiate(prefab);
                if (setRoleTitlePanel) {
                    setRoleTitlePanel.name = PanelName;
                    setRoleTitlePanel.parent = this.mainUI;
        
                    let panellogic = setRoleTitlePanel.getComponent('SetRoleTitleUI');
                    panellogic.changeBtnLoadStatus(data);
                }
            });
        }
    }
}

let instance = null;
module.exports = (()=>{
    if(instance==null){
        instance = new SetRoleTitleUIMgr();
    }
    return instance;
})();
