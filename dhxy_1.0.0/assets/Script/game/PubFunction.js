﻿let GameRes = require('./GameRes');
let GameDefine = require('./GameDefine');
let pScreenNoticeMgr = require('./ScreenNotice');
let MsgStr = require('../etc/msg_str');

class CPubFunction {

    constructor() {
        this.vecScreenCutDown = [];
        this.noticeNum = 0;
        this.vecRoleBroadCast = [];
        this.nTimeIndex = 0;

        this.btntimer = {
            timer: null,
            times: 0,
        };

        this.vecExpLevel = [0, 100, 500, 1000, 5000, 10000, 50000, 100000, 500000, 1000000, 5000000, 10000000, 50000000, 100000000]; //zzzErr
        this.mapAttribute = {};
        this.mapAttribute['AdSpdEhan'] = {
            Name: '加强加速',
            Kind: '物理属性'
        };
        this.mapAttribute['CharmEhan'] = {
            Name: '加强魅惑',
            Kind: '法术增强'
        };
        this.mapAttribute['AdAtkEhan'] = {
            Name: '加强加功',
            Kind: '物理属性'
        };
        this.mapAttribute['AdDefEhan'] = {
            Name: '加强防御',
            Kind: '物理属性'
        };
        this.mapAttribute['RecoverEhan'] = {
            Name: '加强遗忘',
            Kind: '法术增强'
        };
        this.mapAttribute['DisorderDef'] = {
            Name: '抗混乱',
            Kind: '法术抗性'
        };
        this.mapAttribute['DisorderDefNeg'] = {
            Name: '忽视抗混',
            Kind: '法术增强'
        };
        this.mapAttribute['DisorderEhan'] = {
            Name: '加强混乱',
            Kind: '法术增强'
        };
        this.mapAttribute['Atk'] = {
            Name: '攻击',
            Kind: '物理属性'
        };
        this.mapAttribute['Speed'] = {
            Name: '速度',
            Kind: '物理属性'
        };
        this.mapAttribute['HitCombo'] = {
            Name: '连击次数',
            Kind: '物理属性'
        };
        this.mapAttribute['HitComboRate'] = {
            Name: '连击率',
            Kind: '物理属性'
        };
        this.mapAttribute['HitRate'] = {
            Name: '命中率',
            Kind: '物理属性'
        };
        this.mapAttribute['FatalRate'] = {
            Name: '狂暴率',
            Kind: '物理属性'
        };
        this.mapAttribute['PhyDef'] = {
            Name: '物理吸收',
            Kind: '物理属性'
        };
        this.mapAttribute['PhyDefNef'] = {
            Name: '破防程度',
            Kind: '物理属性'
        };
        this.mapAttribute['PhyDefNefRate'] = {
            Name: '破防概率',
            Kind: '物理属性'
        };
        this.mapAttribute['VoidRate'] = {
            Name: '闪避率',
            Kind: '物理属性'
        };
        this.mapAttribute['HpMax'] = {
            Name: '气血',
            Kind: '物理属性'
        };
        this.mapAttribute['FireDef'] = {
            Name: '抗火',
            Kind: '法术抗性'
        };
        this.mapAttribute['ForgetDef'] = {
            Name: '抗遗忘',
            Kind: '法术抗性'
        };
        this.mapAttribute['GfireDef'] = {
            Name: '抗鬼火',
            Kind: '法术抗性'
        };
        this.mapAttribute['GfireDefNeg'] = {
            Name: '忽视抗鬼火',
            Kind: '法术增强'
        };
        this.mapAttribute['GfireRageOdds'] = {
            Name: '鬼火狂暴率',
            Kind: '法术增强'
        };
        this.mapAttribute['PoisonDef'] = {
            Name: '抗毒',
            Kind: '法术抗性'
        };
        this.mapAttribute['RainDef'] = {
            Name: '抗水',
            Kind: '法术抗性'
        };
        this.mapAttribute['RainDefNeg'] = {
            Name: '忽视抗水',
            Kind: '法术增强'
        };
        this.mapAttribute['RainEhan'] = {
            Name: '加强水系',
            Kind: '法术增强'
        };
        this.mapAttribute['RainRageOdds'] = {
            Name: '水系狂暴率',
            Kind: '法术增强'
        };
        this.mapAttribute['SanshiDef'] = {
            Name: '抗三尸',
            Kind: '法术抗性'
        };
        this.mapAttribute['SanshiDefNeg'] = {
            Name: '忽视抗三尸',
            Kind: '法术增强'
        };
        this.mapAttribute['ShanshiRageOdds'] = {
            Name: '三尸狂暴率',
            Kind: '法术增强'
        };
        this.mapAttribute['SealDef'] = {
            Name: '抗封印',
            Kind: '法术抗性'
        };
        this.mapAttribute['ShockDef'] = {
            Name: '抗震',
            Kind: '法术抗性'
        };
        this.mapAttribute['ShockDefNeg'] = {
            Name: '忽视抗震慑',
            Kind: '法术增强'
        };
        this.mapAttribute['ShockEhan'] = {
            Name: '加强震慑',
            Kind: '法术增强'
        };
        this.mapAttribute['SleepDef'] = {
            Name: '抗昏睡',
            Kind: '法术抗性'
        };
        this.mapAttribute['ThunderDef'] = {
            Name: '抗雷',
            Kind: '法术抗性'
        };
        this.mapAttribute['ThunderRageOdds'] = {
            Name: '雷系狂暴率',
            Kind: '法术增强'
        };
        this.mapAttribute['WindDef'] = {
            Name: '抗风',
            Kind: '法术抗性'
        };
        this.mapAttribute['WindDefNeg'] = {
            Name: '忽视抗风',
            Kind: '法术增强'
        };
        this.mapAttribute['WindEhan'] = {
            Name: '加强风系',
            Kind: '法术增强'
        };
        this.mapAttribute['WindRageOdds'] = {
            Name: '风系狂暴率',
            Kind: '法术增强'
        };
        this.mapAttribute['AtkPercent'] = {
            Name: '攻击改变',
            Kind: '物理属性'
        };
        //----------------------------------
    }

    initPrefab() {
        cc.loader.loadRes(`Prefabs/picPercent`, cc.Prefab, (err, prefab) => {
            this.picPercent = prefab;
        });
        cc.loader.loadRes(`Prefabs/picNotice`, cc.Prefab, (err, prefab) => {
            this.picNotice = prefab;
        });
        cc.loader.loadRes(`Prefabs/role`, cc.Prefab, (err, prefab) => {
            this.role = prefab;
        });
        cc.loader.loadRes(`Prefabs/Npc`, cc.Prefab, (err, prefab) => {
            this.Npc = prefab;
        });
        cc.loader.loadRes(`Prefabs/preRoleBrodcast`, cc.Prefab, (err, prefab) => {
            this.preRoleBrodcast = prefab;
        });
        cc.loader.loadRes(`Prefabs/TalkUI`, cc.Prefab, (err, prefab) => {
            this.TalkUI = prefab;
        });
    }

    // ResetTimer(nValue) {
    //     let pSelf = this;
    //     clearInterval(this.m_timer);
    //     this.m_timer = setInterval(function () {
    //         pSelf.OnTimer();
    //     }, nValue);
    // }

    // OnTimer() {  //50 毫秒一次
    //     this.nTimeIndex += 1;
    //     if (this.nTimeIndex % 20 == 0)  // 降频为 1 秒一次
    //     {
    //         // this.CheckAndDeleteNotice();
    //         //this.CheckAndDeleteRoleBroadCast();
    //     }

    //     // if (pScreenNoticeMgr) {
    //     //     pScreenNoticeMgr.OnUpdate(this.nTimeIndex);
    //     // }
    // }

    CreateScreenNotice(strRichText, bInstertFront) {
        pScreenNoticeMgr.AddNotice(strRichText, bInstertFront);
    }

    // CheckAndDeleteRoleBroadCast() {
    //     let nCurTime = this.GetTime();

    //     for (let nIndex in this.vecRoleBroadCast) {
    //         if (nCurTime >= this.vecRoleBroadCast[nIndex].nCreateTime + this.vecRoleBroadCast[nIndex].nLiveTime) {
    //             let goTmp = this.vecRoleBroadCast[nIndex];
    //             goTmp.goRoleBroadCast.destroy();
    //             this.vecRoleBroadCast.splice(nIndex, 1);
    //         }
    //     }
    // }

    GetItemName(nItemID) {
        if (cc.ll.propData.item[nItemID] == null)
            return '';

        return cc.ll.propData.item[nItemID].name;
    }

    CreateNotice(goPanel, strRichText, nLiveTime) {
        this.noticeNum++;
        let yn = (this.noticeNum % 5) * 40
        let picNotice = this.CreateSubNode(goPanel, {
            nX: -200,
            nY: 100 - yn
        }, this.picNotice, 'picNotice');
        if (this.isNumString(strRichText)) {
            let strcode = parseInt(strRichText);
            if (strcode && MsgStr[strcode]) {
                strRichText = MsgStr[strcode];
            }
        }
        cc.find('Label', picNotice).getComponent(cc.Label).string = strRichText;
        picNotice.runAction(cc.sequence(
            cc.delayTime(1.5 + (this.noticeNum * 0.3)),
            cc.moveTo(1, cc.v2(-200, 240)),
            cc.delayTime(0.5),
            cc.fadeOut(0.5),
            cc.callFunc(() => {
                this.noticeNum--;
                if (this.noticeNum < 0) {
                    this.noticeNum = 0;
                }
                picNotice.destroy();
            }),
        ));
    }


    CreateNpcNotice(nNpcConfigID, strText) {
        let pNpcMgr = require('../game/NpcMgr');
        let CSpeak = require('../game/Speak');

        let pConfigInfo = pNpcMgr.GetNpcConfigInfo(nNpcConfigID);
        if (null == pConfigInfo)
            return;

        let stSpeak = new CSpeak(pConfigInfo.nResID, strText, pConfigInfo.strName, 2);
        this.CreateNpcTalk([stSpeak], null);
    }



    GetLevel(nExp) {
        let nLen = this.vecExpLevel.length;

        for (let nIndex = nLen - 1; nIndex >= 0; nIndex--) {
            if (nExp >= this.vecExpLevel[nIndex]) {
                let nLevel = nIndex + 1;

                let nCur = 0;
                let nMax = 0;

                if (nIndex == nLen - 1) {
                    nCur = this.vecExpLevel[nIndex];
                    nMax = this.vecExpLevel[nIndex];
                } else {
                    nCur = nExp - this.vecExpLevel[nIndex];
                    nMax = this.vecExpLevel[nIndex + 1];
                }

                return {
                    nLevel: nIndex + 1,
                    nMax: nMax,
                    nCur: nCur
                };
            }
        }
        return {};
    }

    /**
     * 在父节点下创建子节点
     * @param {*} vParent   父节点
     * @param {*} stPos     子节点位置
     * @param {*} strPrefab 子节点预制体
     * @param {*} strName   子节点名称
     * @returns 
     */
    CreateSubNode(vParent, stPos, strPrefab, strName) {
        let goItem = cc.instantiate(strPrefab);
        goItem.parent = vParent;
        goItem.name = strName;
        goItem.setPosition(cc.v2(stPos.nX, stPos.nY));
        return goItem;
    }

    CreateEmojSubNode(vParent, stPos, strPrefab, strName, emojiAtlas) {
        let goItem = cc.instantiate(strPrefab);
        let chatItem = new cc.Node();
        chatItem.parent = goItem;
        let richText = chatItem.addComponent('CustomRichText');
        richText.maxWidth = goItem.contentNode.width - 10;;
        richText.fontSize = 18;
        richText.lineHeight = 20;
        richText.type = 1;
        //richText.rolename = info.name;
        richText.scale = 1;
        richText.emojiAtlas = emojiAtlas;
        richText.string = strMsg;
        //chatItem.x = -this.contentNode.width/2 + 3;
        chatItem.color = cc.color(200, 0, 0, 255);

        chatItem.parent = vParent;
        chatItem.name = strName;
        chatItem.setPosition(cc.v2(stPos.nX, stPos.nY));

        return chatItem;
    }

    CreateSubNodeByType(vParent, stPos, strPrefab, strName, type) {
        let goItem = cc.instantiate(strPrefab);
        goItem.parent = vParent;
        goItem.name = strName;
        goItem.opType = type;
        goItem.setPosition(cc.v2(stPos.nX, stPos.nY));

        return goItem;
    }

    isNumString(str) {
        var regPos = /^\d+$/; //非负浮点数
        if (regPos.test(str)) {
            return true;
        } else {
            return false;
        }
    }

    deepClone(obj) {
        let objClone = Array.isArray(obj) ? [] : {};
        if (obj && typeof obj === "object") {
            for (const key in obj) {
                if (obj.hasOwnProperty(key)) {
                    //判断ojb子元素是否为对象，如果是，递归复制
                    if (obj[key] && typeof obj[key] === "object") {
                        objClone[key] = deepClone(obj[key]);
                    } else {
                        //如果不是，简单复制
                        objClone[key] = obj[key];
                    }
                }
            }
        }
        return objClone;
    }

    getWeaponName(equip) {
        let weaponname = '';
        if (equip.type == 0) {
            weaponname = '0000';
        } //新手装备
        if (equip.type == 1) { //高级装备
            weaponname = '0100';
            if (equip.level == 100 && equip.gemcnt >= 10) { //100级打满宝石
                weaponname = '0101';
            } else if (equip.level == 120) { //120级
                weaponname = '0200';
                if (equip.gemcnt >= 12) { //打满宝石
                    weaponname = '0202';
                }
            }
        } else if (equip.type == 2) {
            weaponname = '0300';
            if (equip.gemcnt >= 13) {
                weaponname = '0302';
            } else if (equip.gemcnt >= 10) {
                weaponname = '0301';
            }
        } else if (equip.type == 3) {
            weaponname = '0400';
            if (equip.gemcnt >= 16) {
                weaponname = '0501';
            } else if (equip.gemcnt >= 13) {
                weaponname = '0403';
            } else if (equip.gemcnt >= 10) {
                weaponname = '0401';
            }
        }
        return weaponname;
    }

    /**
     * 添加武器动画信息
     * @param {*} weaponInfo        武器信息
     * @param {*} weaponNode        放置武器的节点
     * @param {*} animationNode     动画节点
     * @param {*} logic             逻辑脚本
     * @param {*} resid             角色资源id
     * @param {*} act               动名名称
     * @param {*} dir               动作方向
     * @returns 
     */
    addWeaponAnimation(weaponInfo, weaponNode, animationNode, logic, resid, act, dir) {
        if (!animationNode || !weaponNode || GameDefine.roleName[resid] == null || animationNode.active == false) {
            return;
        }
        let clips = weaponNode.getComponent(cc.Animation).getClips();
        for (const clip of clips) {
            if (clip.name == act) {
                weaponNode.getComponent(cc.Animation).stop(act);
                weaponNode.getComponent(cc.Animation).removeClip(clip, true);
            }
        }
        weaponNode.active = false;
        let weaponname = '0000';
        if (weaponInfo.length > 0) {
            weaponname = this.getWeaponName(JSON.parse(weaponInfo));
        }
        cc.loader.loadRes(`weapon/${resid}/${weaponname}_${act}_${dir}_wp`, cc.SpriteAtlas, function (err, atlas) {
            if (!animationNode || !animationNode.parent || animationNode.parent.parent == null) return;
            if (err) {
                console.log(err);
                return;
            }
            if (Array.isArray(atlas)) {
                return;
            }
            let curFrames = [];
            for (let i = 1; ; i++) {
                let frame = atlas.getSpriteFrame(i);
                if (frame) curFrames.push(frame);
                else break;
            }
            let curClip = cc.AnimationClip.createWithSpriteFrames(curFrames, 15);
            curClip.name = act;
            if (act == 'stand' || act == 'run') {
                curClip.wrapMode = cc.WrapMode.Loop;
            } else {
                curClip.wrapMode = cc.WrapMode.Normal;
            }
            let nodeAni = weaponNode.getComponent(cc.Animation);
            nodeAni.addClip(curClip);
            nodeAni.play(act);
            logic.scheduleOnce(() => {
                weaponNode.scaleX = animationNode.width / weaponNode.width;
                weaponNode.scaleY = animationNode.height / weaponNode.height;
            }, 0);
            weaponNode.active = true;
            if (cc.ll.propData.resanchor[resid]) {
                weaponNode.anchorY = cc.ll.propData.resanchor[resid].anchorY;
            }
        });

        //添加附加动画(某些角色不存在这些动画)
        if (!animationNode.getChildByName('addon')) {
            let addon = new cc.Node();
            addon.parent = animationNode;
            addon.addComponent(cc.Animation);
            let addonsp = addon.addComponent(cc.Sprite);
            addonsp.trim = false;
            addonsp.sizeMode = cc.Sprite.SizeMode.RAW;
            addon.name = 'addon';
        }
        let addonNode = animationNode.getChildByName('addon');
        let addonclips = addonNode.getComponent(cc.Animation).getClips();
        for (const clip of addonclips) {
            if (clip.name == act) {
                addonNode.getComponent(cc.Animation).stop(act);
                addonNode.getComponent(cc.Animation).removeClip(clip, true);
            }
        }
        addonNode.active = false;
        let addonname = weaponname.substr(0, 3) + '0';
        cc.loader.loadRes(`weapon/${resid}/${addonname}_${act}_${dir}_addon`, cc.SpriteAtlas, function (err, atlas) {
            if (!animationNode || !animationNode.parent || !animationNode.parent.parent == null) return;
            if (err) {
                console.log(err);
                return;
            }
            if (Array.isArray(atlas)) {
                return;
            }
            let addonani = addonNode.getComponent(cc.Animation);
            let addonsp = addonNode.getComponent(cc.Sprite);
            addonsp.spriteFrame = atlas.getSpriteFrame(0);
            let addonFrames = [];
            for (let i = 1; ; i++) {
                let frame = atlas.getSpriteFrame(i);
                if (frame) addonFrames.push(frame);
                else break;
            }
            let curClip = cc.AnimationClip.createWithSpriteFrames(addonFrames, 15);
            curClip.name = act;
            if (act == 'stand' || act == 'run') {
                curClip.wrapMode = cc.WrapMode.Loop;
            } else {
                curClip.wrapMode = cc.WrapMode.Normal;
            }
            addonani.addClip(curClip);
            addonani.play(act);
            logic.scheduleOnce(() => {
                addonNode.scaleX = animationNode.width / addonNode.width;
                addonNode.scaleY = animationNode.height / addonNode.height;
            }, 0);
            if (cc.ll.propData.resanchor[resid]) {
                addonNode.anchorY = cc.ll.propData.resanchor[resid].anchorY;
            }
            addonNode.active = true;
        });
    }

    /**
     * 获取颜色
     * @param {*} relive 
     * @param {*} type      type=0 人物和伙伴颜色，type=1召唤兽颜色
     * @returns 
     */
    getReliveColor(relive, type = 0) { //type=0 人物和伙伴颜色，type=1召唤兽颜色
        if (relive == 0) {
            if (type == 0) return new cc.Color(0x10, 0xdf, 0x08);
            if (type == 1) return new cc.Color(0xef, 0x65, 0x18);
        } else if (relive == 1) {
            if (type == 0) return new cc.Color(0xff, 0x7d, 0x00);
            if (type == 1) return new cc.Color(0xe7, 0x92, 0xde);
        } else if (relive == 2) {
            if (type == 0) return new cc.Color(0x7b, 0xfb, 0xff);
            if (type == 1) return new cc.Color(0xa5, 0x69, 0xf7);
        } else if (relive == 3) {
            if (type == 0) return new cc.Color(0xde, 0x04, 0x21);
            if (type == 1) return new cc.Color(0x29, 0x2c, 0xef);
        }
        return cc.Color.BLACK;
    }

    CreateRole(nOnlyID, nResID, strName, nHP, nMP, stPos, vParent) {

        let preRole = this.role;

        let goRole = this.CreateSubNode(vParent, stPos, preRole, 'BattleSoldier');
        cc.find('name', goRole).getComponent(cc.Label).string = strName;

        let comRole = goRole.getComponent('role');
        comRole.onlyid = nOnlyID;
        comRole.resid = nResID;
        comRole.hp = nHP;
        comRole.animationNode.anchorY = 0;
        if (cc.ll.propData.resanchor.hasOwnProperty(nResID))
            comRole.animationNode.anchorY = cc.ll.propData.resanchor[nResID].anchorY;

        comRole.resAtlas = GameRes.getRoleRes(nResID);
        comRole.initAnimation();

        return goRole;
    }

    CreateNpc(nOnlyID, nResID, strName, nHP, nMP, stPos, vParent) {

        let preRole = this.Npc;

        let goRole = this.CreateSubNode(vParent, stPos, preRole, 'BattleSoldier');
        cc.find('name', goRole).getComponent(cc.Label).string = strName;

        let comRole = goRole.getComponent('Npc');

        if (cc.ll.propData.resanchor.hasOwnProperty(nResID))
            comRole.animationNode.anchorY = cc.ll.propData.resanchor[nResID].anchorY;

        return goRole;
    }


    GetMapSum(mapTmp) {
        let nNum = 0;

        for (const it in mapTmp) {
            nNum += mapTmp[it];
        }

        return nNum;
    }


    GetMapLen(mapTmp) {
        let nCnt = 0;

        for (const it in mapTmp)
            nCnt++;

        return nCnt;
    }

    /**
     * 给节点添加事件处理器
     * @param {*} goNode        节点名称
     * @param {*} comName       脚本名称
     * @param {*} strFunction   事件处理器(函数名字)
     * @param {*} varParam      事件处理器参数
     * @returns 
     */
    CreateEventHandler(goNode, comName, strFunction, varParam) {
        let varHandler = new cc.Component.EventHandler();
        varHandler.target = goNode;
        varHandler.component = comName;
        varHandler.handler = strFunction;
        varHandler.customEventData = varParam;
        return varHandler;
    }

    FindAndDeleteNode(nodParent, strName) {
        let nodTmp = nodParent.getChildByName(strName);
        if (nodTmp != null)
            nodTmp.destroy();
    }

    FindAndHidenNode(nodParent, stPos) {
        let nodTmp = nodParent.getChildByName("TalkUI");
        if (nodTmp == null) {
            nodTmp = this.CreateSubNode(nodParent, {
                nX: 0,
                nY: 0
            }, this.TalkUI, "TalkUI");
        }
        nodTmp.setPosition(cc.v2(stPos.nX, stPos.nY));
        nodTmp.zIndex = 1;
        return nodTmp;
    }

    NumToString(nNum, nLen) {
        return (Array(nLen).join(0) + nNum).slice(-nLen);
    }

    CreateSubFrame(vParent, stPos, strPrefab, strClipName, callback) {
        let goEffectFrame = this.CreateSubNode(vParent, {
            nX: stPos.nX,
            nY: stPos.nY
        }, strPrefab, '');

        GameRes.loadClip(strClipName, clip => {
            clip.name = strClipName
            clip.wrapMode = cc.WrapMode.Normal; //循环播放 ？
            goEffectFrame.getComponent('Effect').CreateAndPlayAnimation(clip, callback);
        });
    }

    CreateSubFrameLoop(vParent, stPos, strPrefab, strClipName) {
        let goEffectFrame = this.CreateSubNode(vParent, {
            nX: stPos.nX,
            nY: stPos.nY
        }, strPrefab, '');

        GameRes.loadClip(strClipName, clip => {
            clip.name = strClipName
            clip.wrapMode = cc.WrapMode.Loop; //循环播放 ？
            goEffectFrame.getComponent('Effect').CreateAndPlayAnimation(clip);
        });
        return goEffectFrame;
    }

    /**
     * 显示主角与Npc对话框
     * @param {*} vecSpeak  内容
     * @param {*} pCallBack 对话完成的回调函数
     * @returns 
     */
    CreateNpcTalk(vecSpeak, pCallBack) {
        if (vecSpeak.length <= 0) {
            if (pCallBack)
                pCallBack();
            return;
        }

        let goTalkUI = this.FindAndHidenNode(cc.find('Canvas/MainUI'), {
            nX: 0,
            nY: -188
        });
        goTalkUI.getComponent('Talk').Init(vecSpeak, pCallBack);
    }

    /**
     * 设置龙骨和Avatar
     * @param {*} nodDragonBone 放置龙骨的位置点
     * @param {*} nodAvatar     放置图片的位置点
     * @param {*} nResID        资源ID
     */
    SetDragonBoneAvatar(nodDragonBone, nodAvatar, nResID) {
        nodDragonBone.active = false;
        if (nodAvatar) nodAvatar.active = false;
        cc.loader.loadResDir(`dragonbone/${nResID}`, function (err, assets) {
            if (err != null || nodDragonBone.isValid == false)
                return;

            if (assets.length > 0) {
                nodDragonBone.active = true;

                let dragonRes = {};
                for (let i = 0; i < assets.length; i++) {
                    if (assets[i] instanceof dragonBones.DragonBonesAsset) {
                        dragonRes.dragonAsset = assets[i];
                    }
                    if (assets[i] instanceof dragonBones.DragonBonesAtlasAsset) {
                        dragonRes.dragonAtlasAsset = assets[i];
                    }
                }

                //展现
                let t = nodDragonBone.getComponent(dragonBones.ArmatureDisplay);
                let oldAnimationName = null;
                if (t) {
                    oldAnimationName = t.animationName;
                }
                nodDragonBone.removeComponent(dragonBones.ArmatureDisplay);
                let comArmatureDisplay = nodDragonBone.addComponent(dragonBones.ArmatureDisplay);
                // cc.sys.isNative && comArmatureDisplay._factory.clear(false); //在手机上加载相同的资源会出现闪退
                comArmatureDisplay.dragonAsset = null;
                comArmatureDisplay.dragonAtlasAsset = null;
                comArmatureDisplay.dragonAsset = dragonRes.dragonAsset;
                comArmatureDisplay.dragonAtlasAsset = dragonRes.dragonAtlasAsset;
                comArmatureDisplay.armatureName = 'armatureName';
                if (oldAnimationName) {
                    comArmatureDisplay.playAnimation(oldAnimationName, 0);
                }
            } else {
                if (nodAvatar == null)
                    return;

                cc.loader.loadResDir(`ui/photo/${nResID}`, cc.SpriteFrame, (e, frame) => {
                    nodAvatar.active = true;
                    nodAvatar.getComponent(cc.Sprite).spriteFrame = frame[0];
                })
            }
        });
    }

    GetRaceFileName(nRace) {
        if (nRace == 1)
            return 'ui_common_icon_race1';

        if (nRace == 2)
            return 'ui_common_icon_race3';

        if (nRace == 3)
            return 'ui_common_icon_race2';

        if (nRace == 4)
            return 'ui_common_icon_race4';
    }

    /**
     * 设置结点下的cc.Sprite组件下的cc.SpriteFrame属性
     * @param {*} goSprite  结点
     * @param {*} strAtlas  图集资源路径
     * @param {*} strFrame  cc.SpriteFrame的名称
     */
    SetSpriteFrame(goSprite, strAtlas, strFrame) {
        cc.loader.loadRes(strAtlas, cc.SpriteAtlas, (err, atlas) => {
            if (goSprite && goSprite._components && goSprite.getComponent(cc.Sprite)) {
                goSprite.getComponent(cc.Sprite).spriteFrame = atlas.getSpriteFrame(strFrame);
            }
        });
    }

    /**
     * 销毁数组内节点
     * @param {*} vecNod 
     */
    DestroyVecNode(vecNod) {
        for (var it in vecNod) {
            vecNod[it].destroy();
        }
        vecNod.length = 0;
    }

    FindAndDoUIFunction(strUI, strFun, stData) {
        let strPath = `Canvas/MainUI/${strUI}`;
        let goUI = cc.find(strPath);
        if (null == goUI)
            return;

        let comUI = goUI.getComponent(strUI);
        if (null == comUI)
            return;

        comUI[strFun](stData);
    }

    CreateRoleBroadCast(goRole, strMsg) {
        if (null == strMsg || '' == strMsg)
            return;

        let goRoleBroadCast = this.CreateSubNode(goRole, {
            nX: -120,
            nY: 200
        }, this.preRoleBrodcast, 'preRoleBrodcast');
        cc.find('Label', goRoleBroadCast).getComponent(cc.Label).string = strMsg;

        // this.vecRoleBroadCast.push({ goRoleBroadCast: goRoleBroadCast, nCreateTime: this.GetTime(), nLiveTime: 3 });
        goRoleBroadCast.runAction(cc.sequence(cc.delayTime(3), cc.removeSelf()));
    }


    CreateRoleEmojBroadCast(goRole, strMsg, emojiAtlas) {
        if (null == strMsg || '' == strMsg)
            return;


        let goRoleBroadCast = this.CreateSubNode(goRole, {
            nX: -120,
            nY: 200
        }, this.preRoleBrodcast, 'preRoleBrodcast');
        //cc.find('Label', goRoleBroadCast).getComponent(cc.Label).string = strMsg;

        let chatItem = new cc.Node();
        chatItem.parent = goRoleBroadCast;
        let richText = chatItem.addComponent('CustomRichText');
        richText.maxWidth = 259;;
        richText.fontSize = 20;
        richText.lineHeight = 22;
        richText.type = 1;
        //richText.rolename = info.name;
        richText.scale = -1;
        richText.emojiAtlas = emojiAtlas;
        richText.string = strMsg;
        richText.strColor = cc.Color.WHITE;
        chatItem.x = 0;
        chatItem.color = cc.color(200, 0, 0, 255);

        chatItem.parent = goRoleBroadCast;
        chatItem.name = 'emojBroadCast';
        //chatItem.setPosition(cc.v2(stPos.nX, stPos.nY));
        chatItem.y = 0;


        // this.vecRoleBroadCast.push({ goRoleBroadCast: goRoleBroadCast, nCreateTime: this.GetTime(), nLiveTime: 3 });
        goRoleBroadCast.runAction(cc.sequence(cc.delayTime(3), cc.removeSelf()));
    }

    GetRole() {
        return cc.find('Canvas/MapUI').getComponent('GameMapLogic').selfHeroLogic;
    }

    /**
     * 主角到指定地图指定位置do
     * @param {*} nMap      地图ID
     * @param {*} nX        x位置
     * @param {*} nY        y位置
     * @param {*} pCallBack 完成回调
     * @returns 
     */
    MainPlayerToDo(nMap, nX, nY, pCallBack) {
        let comMapLogic = cc.find('Canvas/MapUI').getComponent('GameMapLogic');
        let pRole = comMapLogic.selfHeroLogic;

        if (cc.ll.player.mapid != nMap) {
            if (cc.ll.player.mapid == 1201) {
                this.CreateNotice(cc.find('Canvas'), '本场景禁止传送，请与天牢守卫对话离开。', 2);
                return;
            }
            //不同地图,则切换地图
            comMapLogic.changeMap(nMap);
        }

        this.stArriveCallBack = new GameDefine.CArriveCallBack(nX, nY, pCallBack);

        if (this.GetDistance({ x: nX, y: nY }, { x: pRole.netInfo.x, y: pRole.netInfo.y }) < 5) {
            pRole.OnArrive();//到达目的地
        }
        else {
            //移动到目的地
            comMapLogic.touchPos({
                x: nX * 20, y: nY * 20
            });
        }
    }

    CreateWaitTip(strNotice, time, pCallBack) {
        // clearInterval(this.stActionTimer);
        this.FindAndDeleteNode(cc.find('Canvas/MainUI'), 'picPercent');
        let picPercent = this.CreateSubNode(cc.find('Canvas/MainUI'), {
            nX: -220,
            nY: -100
        }, this.picPercent, 'picPercent');
        cc.find('Label', picPercent).getComponent(cc.Label).string = strNotice;
        let prog = picPercent.getComponent(cc.ProgressBar);
        prog.progress = 0;
        let n = 0;
        picPercent.runAction(cc.repeat(cc.sequence(cc.callFunc(() => {
            let p = prog.progress + 0.1 > 1 ? 1 : prog.progress + 0.1;
            prog.progress = p;
            n++;
            if (n >= 10) {
                if (pCallBack) {
                    pCallBack();
                }
                pCallBack = null;
                picPercent.destroy();
            }
        }), cc.delayTime(time / 10)), 10));
    }

    ChangeTeamButtonState(vecButton, strAtlas, strDarkFrame, strLightFrame, nCurBtn) {
        for (var it in vecButton) {
            if (it != nCurBtn) {
                this.SetSpriteFrame(vecButton[it], strAtlas, strDarkFrame);
            } else {
                this.SetSpriteFrame(vecButton[it], strAtlas, strLightFrame);
            }
        }
    }

    ChangeTeamNodeState(vecNode, nCurNode) {
        for (var it in vecNode) {
            vecNode[it].active = it == nCurNode ? true : false;
        }
    }

    GetTime() {
        let nTime = Math.floor(Date.parse(new Date()) / 1000);
        let nLocalTime = nTime + 28800;
        return nLocalTime;
    }

    GetAttributeName(strKey) {
        return this.mapAttribute.hasOwnProperty(strKey) ? this.mapAttribute[strKey].Name : '';
    }

    GetAttributeType(strKey) {
        return this.mapAttribute.hasOwnProperty(strKey) ? this.mapAttribute[strKey].Kind : '';
    }

    GetLevelString(nRelive, nLevel) {
        return `${nRelive}转${nLevel}级`;
    }

    ChangeNumToRange(fData, fMin, fMax) {
        fData = Math.max(fData, fMin);
        fData = Math.min(fData, fMax);

        return fData;
    }

    GetItemInfo(nItemID) {
        if (cc.ll.propData.item.hasOwnProperty(nItemID) == false)
            return null;

        return cc.ll.propData.item[nItemID];
    }

    GetDistance(stPosA, stPosB) {
        let nXDis = Math.abs(stPosA.x - stPosB.x);
        let nYDis = Math.abs(stPosA.y - stPosB.y);
        let nDis = Math.pow(nXDis * nXDis + nYDis * nYDis, 0.5);
        return nDis;
    }

    GetMapDataByIndex(mapData, nIndex) {
        if (nIndex < 0 || nIndex >= this.GetMapLen(mapData))
            return null;

        let nCnt = -1;
        for (let it in mapData) {
            nCnt++;
            if (nCnt == nIndex)
                return mapData[it];
        }

        return null;
    }

    IsDataInVecter(stData, vecData) {
        for (let it in vecData) {
            if (stData == vecData[it])
                return true;
        }
        return false;
    }


    GetDefault(stData, stDefault) {
        if (typeof (stData) == "undefined")
            return stDefault;

        return stData;
    }

    GetTaskTalkList(nTaskID, nStep) {
        let vecTmp = [];

        let CSpeak = require('./Speak');
        let pNpcMgr = require('./NpcMgr');

        let pTaskConfigMgr = require('./task_config').g_pTaskConfigMgr;
        let stStep = pTaskConfigMgr.GetTaskStepInfo(nTaskID, nStep);
        if (null == stStep)
            return vecTmp;


        for (var itSpeak in stStep.vecSpeak) {
            let stData = stStep.vecSpeak[itSpeak];

            let nSpeakerResID = 0;
            let strSpeaderName = ''
            let nPosType = 1;

            if (stData.nKey > 0) { //Npc说话

                let stConfigInfo = pNpcMgr.GetNpcConfigInfo(stData.nKey);
                nSpeakerResID = stConfigInfo.nResID;
                strSpeaderName = stConfigInfo.strName;
                nPosType = 2;
            } else { //主角说话
                nSpeakerResID = cc.ll.player.resid;
                strSpeaderName = cc.ll.player.name;
                nPosType = 1;
            }


            let stSpead = new CSpeak(nSpeakerResID, stData.nValue, strSpeaderName, nPosType);
            vecTmp.push(stSpead);
        }

        return vecTmp;
    }



    SetPrizeIcon(goPrizeIcon, strItem) {
        if (strItem == 'exp') {
            this.SetSpriteFrame(goPrizeIcon, 'Common/ui_common', 'ui_common_icon_exp');
        } else if (strItem == 'petexp') {
            this.SetSpriteFrame(goPrizeIcon, 'Common/ui_common', 'ui_common_icon_exppet');
        } else if (strItem == 'money') {
            goPrizeIcon.getComponent(cc.Sprite).spriteFrame = GameRes.getItemIcon(9001);
        } else {
            let stConfigInfo = cc.ll.propData.item[parseInt(strItem)];
            goPrizeIcon.getComponent(cc.Sprite).spriteFrame = GameRes.getItemIcon(stConfigInfo.icon);
        }

    }


    GetResID(nRace, nSex, nReliveCnt) {
        if (nReliveCnt == 2)
            nReliveCnt = 1;

        let strKey = `${nRace}${nSex}${nReliveCnt}`;

        let mapResID = {
            '110': 1001,
            '111': 1011,
            '113': 1031,
            '120': 1002,
            '121': 1012,
            '123': 1032,
            '210': 2003,
            '211': 2013,
            '213': 2033,
            '220': 2004,
            '221': 2014,
            '223': 2034,
            '310': 3005,
            '311': 3015,
            '313': 3035,
            '320': 3006,
            '321': 3016,
            '323': 3036,
            '410': 4007,
            '411': 4017,
            '413': 4037,
            '420': 4008,
            '421': 4018,
            '423': 4038,
        };

        if (mapResID.hasOwnProperty(strKey))
            return mapResID[strKey];

        return 0;
    }

    //zzzErr 需整理资源
    SetAnimation(goAnim, nResID, strClipName) {
        goAnim.active = false;

        cc.loader.loadRes(`shap/${nResID}/${strClipName}`, cc.SpriteAtlas, function (err, resActionAtlas) {
            goAnim.active = true;

            let curFrames = [];
            for (let i = 1; ; i++) {
                let frame = resActionAtlas.getSpriteFrame(i);
                if (null == frame)
                    break;
                curFrames.push(frame);
            }

            let curClip = cc.AnimationClip.createWithSpriteFrames(curFrames, curFrames.length);
            curClip.name = strClipName;
            curClip.wrapMode = cc.WrapMode.Loop;
            let nodeAni = goAnim.getComponent(cc.Animation);
            nodeAni.addClip(curClip);
            nodeAni.play(curClip.name);
            goAnim.anchorY = cc.ll.propData.resanchor.hasOwnProperty(nResID) ? cc.ll.propData.resanchor[nResID].anchorY : 0.4;
        });
    }

    initLongTouchBtn(btn, callback, param) {
        btn.on(cc.Node.EventType.TOUCH_START, () => {
            if (this.btntimer.timer != null) {
                return;
            }
            this.btntimer.timer = setInterval(() => {
                this.btntimer.times++;
                if (this.btntimer.times >= this.btntimer.step) {
                    this.btntimer.times = 0;
                    this.btntimer.step = this.btntimer.step / 2;
                    if (this.btntimer.step < 1) {
                        this.btntimer.step = 1;
                    }
                    callback(param);
                }
            }, 50);
            callback(param);
        });
        let cancelfunc = () => {
            clearInterval(this.btntimer.timer);
            this.btntimer.timer = null;
            this.btntimer.times = 0;
            this.btntimer.step = 20;
        };
        btn.on(cc.Node.EventType.TOUCH_CANCEL, cancelfunc);
        btn.on(cc.Node.EventType.TOUCH_END, cancelfunc);
    }

    safeJson(str) {
        if (typeof str == 'object') {
            return str;
        }
        let ret = null;
        if (typeof str != 'string' || str == null || str == '' || (str[0] != '{' && str[0] != '[')) {
            // console.log(`safeJson error, string: ${str}`);
            return ret;
        }
        try {
            ret = JSON.parse(str);
        } catch (error) {
            console.log(`safeJson error, string: ${str}`);
            ret = null;
        }
        return ret;
    }
}

let g_ctFunctionInstance = null;

module.exports = (() => {
    if (g_ctFunctionInstance == null) {
        g_ctFunctionInstance = new CPubFunction();
    }
    return g_ctFunctionInstance;
})();