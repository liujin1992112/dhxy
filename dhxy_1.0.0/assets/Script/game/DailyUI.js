﻿
let CPubFunction = require('./PubFunction');
let CNpcMgr = require('./NpcMgr');
let GameRes = require('./GameRes');

cc.Class({
    extends: cc.Component,


    properties: {
        ActiveDetailUI:cc.Prefab,
    },


    onLoad() {
        this.vecTitle =
            [
                "帮派任务",
                "师门任务",
                "五环任务",
                "钟馗抓鬼",
                "天庭降妖",
                "宝图任务",

                "大雁塔",
                "寻芳",
                "地宫",

                "野外封妖",
                "三界妖王",
                "地煞星",
            ];


        this.vecText =
            [
                "要想升级快~就得做帮派！\n\n【人数要求】单人 \n【等级要求】1级以上 \n【活动时间】每日刷新 \n【活动奖励】",

                "师傅每天为你量身打造的升级赚钱之路！\n\n【人数要求】单人 \n【等级要求】1级以上 \n【活动时间】每日刷新 \n【活动奖励】",

                "侠之大者，兼济天下苍生。路见不平拔刀相助者，云游大师将给与丰厚的金钱奖励。还可能有大侠陪你闯荡江湖！80级以上就轮到你去帮助别人了！\n\n【人数要求】单人 \n【等级要求】1级以上 \n【活动时间】每日刷新 \n【活动奖励】",

                "钟馗最近一直为泛滥于三界的野鬼烦得焦头烂额，去帮帮他吧！ \n组队人数越多，经验加成越高 \n队长每轮可以额外获得伙伴修炼册哦！  \n\n【人数要求】单人 \n【等级要求】1级以上 \n【活动时间】每日刷新 \n【活动奖励】",

                "李靖获知由于封印妖魔的降魔阵出现松动。导致天下妖魔纷纷逃跑，并且除了四个异常厉害的妖怪。目前在三界各地吸取天地精华。但是天庭目前无暇顾及，望给为大侠降服妖魔，拯救苍生。 \n组队人数越多，经验加成越高 \n队长每轮可以额外获得伙伴修炼册哦！  \n\n【人数要求】单人 \n【等级要求】1级以上 \n【活动时间】每日刷新 \n【活动奖励】",

                "据说长安酒馆的店小二是个大隐隐于市的高人，坐拥很多宝藏线索~  \n\n【人数要求】单人 \n【等级要求】1级以上 \n【活动时间】每日刷新 \n【活动奖励】",

                "大雁塔镇压妖魔无数，可是总有一些妖魔不安分。快去绑住慈恩寺方丈吧！   \n\n【人数要求】单人 \n【等级要求】1级以上 \n【活动时间】每日刷新 \n【活动奖励】",

                "寻找被封印的仙子，打败她们的守护兽解救她们吧！  \n\n【人数要求】单人 \n【等级要求】1级以上 \n【活动时间】每日刷新 \n【活动奖励】",


                "凌烟阁地宫为妖魔占据，吾皇以当世五神兽镇压其中，以遏制妖魔出世。但欲扫除妖魔永绝后患，仍需天下豪杰同心协力，希望英雄担此重任！ \n\n【人数要求】单人 \n【等级要求】1级以上 \n【活动时间】每日刷新 \n【活动奖励】",


                "挖宝会不小心挖出妖怪来！时刻准备灭掉他们！\n大唐边境（33级~65级），大唐境内（66级~85级），天宫（86级~105级），东海渔村（106级~125级），方寸山（146级~165级），万寿山（146级~165级），白骨山（166级~180级）   \n\n【人数要求】单人 \n【等级要求】1级以上 \n【活动时间】每日刷新 \n【活动奖励】",

                "三界妖王会不时下界作乱，击败他们可以获得丰厚奖励！！\n大唐边境（33级~65级），大唐境内（66级~85级），天宫（86级~105级），东海渔村（106级~125级），方寸山（146级~165级），万寿山（146级~165级），白骨山（166级~180级）   \n\n【人数要求】单人 \n【等级要求】1级以上 \n【活动时间】每日刷新 \n【活动奖励】",

                "地煞星迷失心智下界作乱，私造神兵利器，每逢30分钟和整点降临，三界侠士快快前往挑战！ \n\n【人数要求】单人 \n【等级要求】1级以上 \n【活动时间】每日刷新 \n【活动奖励】",
            ];

        this.vecPrize =
            [
                { exp: 0, petexp: 0 },
                { exp: 0, petexp: 0, 10402: 1 },
                { money: 0 },
                { exp: 0, petexp: 0, money: 0, 10001: 1, 10002: 1 },
                { exp: 0, petexp: 0, money: 0, 10001: 1, 10002: 1 },

                { 10114: 1, 10101: 1, money: 0, 10301: 1 },

                { exp: 0, 10114: 1, 10102: 1, 10405: 1, 10101: 1 },
                { exp: 0, petexp: 0, 10112: 1, 10111: 1 },
                { exp: 0, petexp: 0, 10101: 1, 10102: 1, 10103: 1, 10111: 1, 10118: 1 },

                { exp: 0, 10114: 1, 10113: 1, 10112: 1, 10202: 1, 10118: 1 },
                { exp: 0, petexp: 0, 10114: 1, 10113: 1, 10112: 1, 10202: 1, 10118: 1 },
                { exp: 0, 10301: 1 },
            ];



        this.vecBtnEvent = [];
        this.vecBtnPrize = [0, 0, 0, 0, 0, 0];
        this.mapDailyStart = {};
        this.mapFuBenCnt = {};

    },

    start() {
        cc.find('btnClose', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "DailyUI", "Close", 0));

        let goContent = cc.find(`ScrollView/view/content`, this.node);

        for (let i = 0; i <= 11; i++) {
            this.vecBtnEvent.push(goContent.children[i]);
            this.vecBtnEvent[i].getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "DailyUI", "OnDailyEvent", i));
        }

        for (let i = 0; i <= 10; i++)
            cc.find(`btnGo`, this.vecBtnEvent[i]).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "DailyUI", "OnGo", i));

        let nodPrize = cc.find('nodPrize', this.node);
        for (let i = 0; i < 6; i++) {
            this.vecBtnPrize[i] = nodPrize.children[i];
            this.vecBtnPrize[i].getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "DailyUI", "TakeActivePrize", i));
        }

        cc.ll.net.send('c2s_ask_daily_info', {});

        this.SetMoneyInfo();
    },


    TakeActivePrize(stEvetn, nIndex) {
        if (stEvetn.target.children[3].active == true)
            return;

        if (CPubFunction.GetMapSum(this.mapActiveScore) < (nIndex + 1) * 20)
            return;

        cc.ll.net.send('c2s_take_active_prize', { nIndex: nIndex });
    },


    OnReceiveDailyInfo(strJson) {
        let stData = JSON.parse(strJson);

        this.mapActiveScore = stData.mapActiveScore;

        cc.find('nodPrize/nodExp/picText/picBar', this.node).width = Math.min(CPubFunction.GetMapSum(this.mapActiveScore) / 140, 1) * 680;

        for (let i = 0; i < 6; i++) {
            this.vecBtnPrize[i].children[3].active = parseInt(stData.szBeenTake[i]);
        }

        for (let i = 0; i < 6; i++) {
            let vecTmp = stData.szActivePrize[i].split(",");
            this.SetPrizesInfo(this.vecBtnPrize[i], parseInt(vecTmp[0]), parseInt(vecTmp[1]));
        }

        this.mapDailyStart = stData.mapDailyStart;
        this.mapDailyCnt = stData.mapDailyCnt;
        this.mapFuBenCnt = stData.mapFuBenCnt;

        let vecKind = [{ group: 2, cnt: 20, active: 20 }, { group: 3, cnt: 20, active: 20 }, { group: 5, cnt: 5, active: 5 }, { group: 6, cnt: 120, active: 60 }, { group: 7, cnt: 40, active: 40 }, { group: 4, cnt: 15, active: 15 }];

        for (let nIndex = 0; nIndex <= 5; nIndex++) {
            let stDaily = vecKind[nIndex];

            let nCurCnt = CPubFunction.GetDefault(this.mapDailyCnt[stDaily.group], 0);
            cc.find(`labCnt`, this.vecBtnEvent[nIndex]).getComponent(cc.Label).string = `日次数：${nCurCnt}/${stDaily.cnt}`

            let nCurActive = CPubFunction.GetDefault(this.mapActiveScore[stDaily.group], 0)

            cc.find(`labActive`, this.vecBtnEvent[nIndex]).getComponent(cc.Label).string = `活跃度：${Math.floor(nCurActive)}/${stDaily.active}`
        }

        let vecFuBen = [{ taskid: 1001, active: 21 }, { taskid: 1002, active: 14 }, { taskid: 1004, active: 12 }];

        for (let nIndex2 = 0; nIndex2 <= 2; nIndex2++)  //zzzErr
        {
            let nBtnIndex = 6 + nIndex2;

            let stFuBen = vecFuBen[nIndex2];

            let nCurActive = 0;

            if (nBtnIndex == 8) //地宫
            {
                let nCur = this.IsAllReadyInceptFuBenTask(1004) + this.IsAllReadyInceptFuBenTask(1005) + this.IsAllReadyInceptFuBenTask(1006);
                cc.find(`labCnt`, this.vecBtnEvent[nBtnIndex]).getComponent(cc.Label).string = `日次数：${nCur}/3`;

                nCurActive = CPubFunction.GetDefault(this.mapActiveScore[1004], 0) + CPubFunction.GetDefault(this.mapActiveScore[1005], 0) + CPubFunction.GetDefault(this.mapActiveScore[1006], 0);
            }
            else {
                cc.find(`labCnt`, this.vecBtnEvent[nBtnIndex]).getComponent(cc.Label).string = `日次数：${this.IsAllReadyInceptFuBenTask(stFuBen.taskid)}/1`;
                nCurActive = CPubFunction.GetDefault(this.mapActiveScore[stFuBen.taskid], 0);
            }


            cc.find(`labActive`, this.vecBtnEvent[nBtnIndex]).getComponent(cc.Label).string = `活跃度：${nCurActive}/${stFuBen.active}`
        }

    },



    IsAllReadyInceptDailyTask(nTeam) {
        return this.mapDailyStart.hasOwnProperty(nTeam);
    },

    IsAllReadyInceptFuBenTask(nTask) {
        if (this.mapFuBenCnt.hasOwnProperty(nTask))
            return 1;

        return 0;
    },



    SetPrizesInfo(goItem, nItem, nNum) {
        let stConfigInfo = cc.ll.propData.item[nItem];
        cc.find('Icon', goItem).getComponent(cc.Sprite).spriteFrame = GameRes.getItemIcon(stConfigInfo.icon);
        cc.find('txLap', goItem).getComponent(cc.Label).string = nNum;
    },



    SetMoneyInfo() {
        // cc.find('layMoney/yinliang/ui_common_icon_yinliang/yinliangLabel', this.node).getComponent(cc.Label).string = cc.ll.player.gameData.money;
        // cc.find('layMoney/xianyu/ui_common_icon_xianyu/xianyuLabel', this.node).getComponent(cc.Label).string = cc.ll.player.gameData.jade;
        let topnode = cc.find('topInfo', this.node);
		let toplogic = topnode.getComponent('TopInfo');
		toplogic.updateGameMoney();
    },



    CloseAndClickNpc(nNpc) {
        let stPos = CNpcMgr.GetNpcPos(nNpc);
        CPubFunction.MainPlayerToDo(stPos.nMap, stPos.nX, stPos.nY, () => {
            CNpcMgr.FindNpcByConfigID(nNpc).getComponent('Npc').OnNpcClick();
        });
    },



    OnDailyEvent(e, i) {
        CPubFunction.FindAndDeleteNode(this.node, "ActiveDetailUI");
        let goActiveDetailUI = CPubFunction.CreateSubNode(this.node, { nX: -200, nY: 215 }, this.ActiveDetailUI, 'ActiveDetailUI');
        goActiveDetailUI.getComponent('ActiveDetailUI').ActiveDetailUI_Init(this.vecTitle[i], this.vecText[i], this.vecPrize[i]);
    },



    OnGo(e, d) {
        if (d == 0) //帮派任务
        {
            this.CloseAndClickNpc(10054);
        }

        if (d == 1) //师门任务
        {
            if (cc.ll.player.race == 1)
                this.CloseAndClickNpc(30062);

            if (cc.ll.player.race == 2)
                this.CloseAndClickNpc(10118);

            if (cc.ll.player.race == 3)
                this.CloseAndClickNpc(30063);

            if (cc.ll.player.race == 4)
                this.CloseAndClickNpc(30065);
        }

        if (d == 2) // 五环
        {
            this.CloseAndClickNpc(10080);
        }

        if (d == 3) //钟馗抓鬼
        {
            this.CloseAndClickNpc(10112);
        }

        if (d == 4) //天庭降妖
        {
            this.CloseAndClickNpc(10221);
        }

        if (d == 5) //宝图
        {
            this.CloseAndClickNpc(10013);
        }

        if (d == 6) //大雁塔
        {
            this.CloseAndClickNpc(10062);

        }

        if (d == 7) //寻芳
        {
            this.CloseAndClickNpc(10058);
        }

        if (d == 8) //地宫
        {
            this.CloseAndClickNpc(10051);
        }

        if (d == 9) //野外封妖
        {

        }

        if (d == 10) //三界妖王
        {

        }
    },

    Close() {
        cc.ll.AudioMgr.playCloseAudio();
        this.node.destroy();
    },





});
