﻿var ExpMgr = require('./ExpMgr');
let CPubFunction = require('../game/PubFunction');
var PracticeMgr = require('./PetPracticeMgr');
let GameDefine = require('./GameDefine');

let AttrTypeL1 = GameDefine.AttrTypeL1;

class PetLogic {
    constructor() {}

    init() {
        this.petid = 0; //数据库中的宠物id
        this.resid = 0; //资源id
        this.dataid = 0;
        this.intro = ''; //介绍
        this.name = '';
        this.relive = 0;
        this.level = 0;
        this.grade = 0;
        this.skill = [];
        this.shenSkill = 0;
        this.rate = 0;
        this.hp = 0;
        this.mp = 0;
        this.atk = 0;
        this.spd = 0;
        this.cur_hp = 0;
        this.cur_mp = 0;
        this.cur_atk = 0;
        this.cur_spd = 0;
        this.exp = 0;
        this.maxexp = 0;
        this.ppoint = {};
        this.dpoint = {};
        this.wuxing = {};
        this.xexp = 0;
        this.xlevel = 0;
        this.longgu = 0;

        this.qianneng = 0;
        this.ppointT = {};
        this.dpointT = {};

        this.xiulianfree = 0;
        this.xiulianleft = 0;
        this.xmaxexp = 0;
        this.maxskillcnt = 4;
        this.petcolor = -1;
        this.qinmi = 0;
        this.maxRate = 0
    }

    createPet(petid) {
        // this.setInfo(info);
        cc.ll.net.send('c2s_create_pet', {
            petid: petid,
        });
    }

    setInfo(info) {
        this.init();
        this.petinfo = info;
        if (this.petinfo == null) {
            return;
        }
        let skilllist = {};
        info.petid && (this.petid = info.petid);
        info.dataid && (this.dataid = info.dataid);
        info.resid && (this.resid = info.resid);
        info.intro && (this.intro = info.intro);
        info.name && (this.name = info.name);
        info.relive && (this.relive = info.relive);
        info.level && (this.level = info.level);
        info.grade && (this.grade = info.grade);
        info.skill && (skilllist = CPubFunction.safeJson(info.skill));
        info.shenskill && (this.shenSkill = info.shenskill);
        info.rate && (this.rate = info.rate / 10000);
        info.hp && (this.hp = info.hp);
        info.mp && (this.mp = info.mp);
        info.atk && (this.atk = info.atk);
        info.spd && (this.spd = info.spd);
        info.exp && (this.exp = info.exp);
        info.ppoint && (this.ppoint = JSON.parse(info.ppoint));
        info.dpoint && (this.dpoint = JSON.parse(info.dpoint));
        info.wuxing && (this.wuxing = JSON.parse(info.wuxing));
        info.xexp && (this.xexp = JSON.parse(info.xexp));
        info.xlevel && (this.xlevel = JSON.parse(info.xlevel));
        info.longgu && (this.longgu = info.longgu);
        info.maxskillcnt && (this.maxskillcnt = info.maxskillcnt);
        info.color && (this.petcolor = info.color);
        info.qinmi && (this.qinmi = info.qinmi);
        info.maxrate && (this.maxRate = info.maxrate / 10000);
        if (info.attr1) {
            this.attr1 = JSON.parse(info.attr1);
            this.cur_hp = this.attr1[AttrTypeL1.HP];
            this.cur_mp = this.attr1[AttrTypeL1.MP];
            this.cur_atk = this.attr1[AttrTypeL1.ATK];
            this.cur_spd = this.attr1[AttrTypeL1.SPD];
        }
        this.petinfo.skill = skilllist;
        for (const key in skilllist) {
            if (skilllist.hasOwnProperty(key)) {
                this.skill.push(key);
            }
        }
        this.maxexp = ExpMgr.GetSummonUpGradeExp(this.relive, this.level);
        this.xmaxexp = PracticeMgr.GetUpdateHunPo(info.xlevel);
        this.initPoint();
        this.calculateAttribute();
    }

    initPoint() {
        for (const key in GameDefine.AttrTypeL2) {
            if (GameDefine.AttrTypeL2.hasOwnProperty(key)) {
                const element = GameDefine.AttrTypeL2[key];
                (this.ppoint[GameDefine.AttrTypeL2[key]] == null) && (this.ppoint[GameDefine.AttrTypeL2[key]] = 0);
                this.ppoint[GameDefine.AttrTypeL2[key]] += this.petinfo.level;
                this.ppointT[GameDefine.AttrTypeL2[key]] = 0;
            }
        }

        (this.dpoint[AttrTypeL1.DFENGYIN] == null) && (this.dpoint[AttrTypeL1.DFENGYIN] = 0);
        (this.dpoint[AttrTypeL1.DHUNLUAN] == null) && (this.dpoint[AttrTypeL1.DHUNLUAN] = 0);
        (this.dpoint[AttrTypeL1.DHUNSHUI] == null) && (this.dpoint[AttrTypeL1.DHUNSHUI] = 0);
        (this.dpoint[AttrTypeL1.DYIWANG] == null) && (this.dpoint[AttrTypeL1.DYIWANG] = 0);

        this.dpointT[AttrTypeL1.DFENGYIN] = 0;
        this.dpointT[AttrTypeL1.DHUNLUAN] = 0;
        this.dpointT[AttrTypeL1.DHUNSHUI] = 0;
        this.dpointT[AttrTypeL1.DYIWANG] = 0;

        (this.dpoint[AttrTypeL1.DFENG] == null) && (this.dpoint[AttrTypeL1.DFENG] = 0);
        (this.dpoint[AttrTypeL1.DSHUI] == null) && (this.dpoint[AttrTypeL1.DSHUI] = 0);
        (this.dpoint[AttrTypeL1.DHUO] == null) && (this.dpoint[AttrTypeL1.DHUO] = 0);
        (this.dpoint[AttrTypeL1.DDU] == null) && (this.dpoint[AttrTypeL1.DDU] = 0);
        (this.dpoint[AttrTypeL1.DLEI] == null) && (this.dpoint[AttrTypeL1.DLEI] = 0);
        (this.dpoint[AttrTypeL1.DGUIHUO] == null) && (this.dpoint[AttrTypeL1.DGUIHUO] = 0);
        (this.dpoint[AttrTypeL1.DSANSHI] == null) && (this.dpoint[AttrTypeL1.DSANSHI] = 0);

        this.dpointT[AttrTypeL1.DFENG] = 0;
        this.dpointT[AttrTypeL1.DSHUI] = 0;
        this.dpointT[AttrTypeL1.DHUO] = 0;
        this.dpointT[AttrTypeL1.DDU] = 0;
        this.dpointT[AttrTypeL1.DLEI] = 0;
        this.dpointT[AttrTypeL1.DGUIHUO] = 0;
        this.dpointT[AttrTypeL1.DSANSHI] = 0;

        (this.dpoint[AttrTypeL1.PXISHOU] == null) && (this.dpoint[AttrTypeL1.PXISHOU] = 0);
        (this.dpoint[AttrTypeL1.PMINGZHONG] == null) && (this.dpoint[AttrTypeL1.PMINGZHONG] = 0);
        (this.dpoint[AttrTypeL1.PSHANBI] == null) && (this.dpoint[AttrTypeL1.PSHANBI] = 0);
        (this.dpoint[AttrTypeL1.PLIANJI] == null) && (this.dpoint[AttrTypeL1.PLIANJI] = 0);
        (this.dpoint[AttrTypeL1.PLIANJILV] == null) && (this.dpoint[AttrTypeL1.PLIANJILV] = 0);
        (this.dpoint[AttrTypeL1.PKUANGBAO] == null) && (this.dpoint[AttrTypeL1.PKUANGBAO] = 0);
        (this.dpoint[AttrTypeL1.PPOFANG] == null) && (this.dpoint[AttrTypeL1.PPOFANG] = 0);
        (this.dpoint[AttrTypeL1.PPOFANGLV] == null) && (this.dpoint[AttrTypeL1.PPOFANGLV] = 0);
        (this.dpoint[AttrTypeL1.PFANZHENLV] == null) && (this.dpoint[AttrTypeL1.PFANZHENLV] = 0);
        (this.dpoint[AttrTypeL1.PFANZHEN] == null) && (this.dpoint[AttrTypeL1.PFANZHEN] = 0);

        this.dpointT[AttrTypeL1.PXISHOU] = 0;
        this.dpointT[AttrTypeL1.PMINGZHONG] = 0;
        this.dpointT[AttrTypeL1.PSHANBI] = 0;
        this.dpointT[AttrTypeL1.PLIANJI] = 0;
        this.dpointT[AttrTypeL1.PLIANJILV] = 0;
        this.dpointT[AttrTypeL1.PKUANGBAO] = 0;
        this.dpointT[AttrTypeL1.PPOFANG] = 0;
        this.dpointT[AttrTypeL1.PPOFANGLV] = 0;
        this.dpointT[AttrTypeL1.PFANZHENLV] = 0;
        this.dpointT[AttrTypeL1.PFANZHEN] = 0;
    }

    calculateAttribute() {
        this.calculateProperty();
        this.calculateKang();
        // this.calPassiveSkillAttr();
    }

    calculateProperty() {
        // let cur_rate = this.rate + this.relive*0.1 + this.longgu * 0.01;
        // this.cur_hp = Math.round(this.level * cur_rate * this.ppoint[AttrTypeL1.GENGU] + 0.7 * this.hp * this.level * cur_rate + this.hp);
        // this.cur_mp = Math.round(this.level * cur_rate * this.ppoint[AttrTypeL1.LINGXING] + 0.7 * this.mp * this.level * cur_rate + this.mp);
        // this.cur_atk = Math.round(0.2 * this.level * cur_rate * this.ppoint[AttrTypeL1.LILIANG] + 0.2 * 0.7 * this.atk * this.level * cur_rate + this.atk);
        // this.cur_spd = Math.round((this.spd + this.ppoint[AttrTypeL1.MINJIE]) * cur_rate);
        this.qianneng = this.level * 8 - this.ppoint[AttrTypeL1.GENGU] - this.ppoint[AttrTypeL1.LINGXING] - this.ppoint[AttrTypeL1.LILIANG] - this.ppoint[AttrTypeL1.MINJIE];
    }

    calculateKang() {
        this.dfengyin = this.attr1[AttrTypeL1.DFENGYIN]; // this.dpoint[AttrTypeL1.DFENGYIN] * 4;
        this.dhunlun = this.attr1[AttrTypeL1.DHUNLUAN]; // this.dpoint[AttrTypeL1.DHUNLUAN] * 4;
        this.dhunshui = this.attr1[AttrTypeL1.DHUNSHUI]; // this.dpoint[AttrTypeL1.DHUNSHUI] * 4;
        this.dyiwang = this.attr1[AttrTypeL1.DYIWANG]; // this.dpoint[AttrTypeL1.DYIWANG] * 4;
        this.dfeng = this.attr1[AttrTypeL1.DFENG]; // this.dpoint[AttrTypeL1.DFENG] * 4;
        this.dshui = this.attr1[AttrTypeL1.DSHUI]; // this.dpoint[AttrTypeL1.DSHUI] * 4;
        this.dhuo = this.attr1[AttrTypeL1.DHUO]; // this.dpoint[AttrTypeL1.DHUO] * 4;
        this.ddu = this.attr1[AttrTypeL1.DDU]; // this.dpoint[AttrTypeL1.DDU] * 4;
        this.dlei = this.attr1[AttrTypeL1.DLEI]; // this.dpoint[AttrTypeL1.DLEI] * 4;
        this.dguihuo = this.attr1[AttrTypeL1.DGUIHUO]; // this.dpoint[AttrTypeL1.DGUIHUO] * 4;
        this.dsanshi = this.attr1[AttrTypeL1.DSANSHI]; // this.dpoint[AttrTypeL1.DSANSHI] * 4;
        this.dzhenshe = 0;
        this.pxishou = this.attr1[AttrTypeL1.PXISHOU]; // this.dpoint[AttrTypeL1.PXISHOU] * 3;
        this.pmingzhong = this.attr1[AttrTypeL1.PMINGZHONG]; // this.dpoint[AttrTypeL1.PMINGZHONG] * 1.5;
        this.pshanbi = this.attr1[AttrTypeL1.PSHANBI]; // this.dpoint[AttrTypeL1.PSHANBI] * 1.5;
        this.plianji = 3 + this.attr1[AttrTypeL1.PLIANJI]; // this.dpoint[AttrTypeL1.PLIANJI] * 1;
        this.plianjilv = this.attr1[AttrTypeL1.PLIANJILV]; // this.dpoint[AttrTypeL1.PLIANJILV] * 1.5;
        this.pkuangbao = this.attr1[AttrTypeL1.PKUANGBAO]; // this.dpoint[AttrTypeL1.PKUANGBAO] * 1.5;
        this.ppofang = this.attr1[AttrTypeL1.PPOFANG]; // this.dpoint[AttrTypeL1.PPOFANG] * 3;
        this.ppofanglv = this.attr1[AttrTypeL1.PPOFANGLV]; // this.dpoint[AttrTypeL1.PPOFANGLV] * 3;
        this.pfanzhenlv = this.attr1[AttrTypeL1.PFANZHENLV]; // this.dpoint[AttrTypeL1.PFANZHENLV] * 4;
        this.pfanzhen = this.attr1[AttrTypeL1.PFANZHEN]; // this.dpoint[AttrTypeL1.PFANZHEN] * 4;

        this.xiulianfree = PracticeMgr.GetLevelPoint(this.petinfo.relive, this.petinfo.xlevel)
        this.xiulianleft = this.xiulianfree;
        for (const key in this.dpoint) {
            this.xiulianleft -= this.dpoint[key];
        }
    }

    getMaxAddPoint(type) {
        return PracticeMgr.GetMaxAddPoint(this.relive, type);
    }

    getMaxLongGu() {
        if (this.relive == 0) return 2;
        if (this.relive == 1) return 4;
        if (this.relive == 2) return 7;
        if (this.relive == 3) return 12;
    }
}
module.exports = {
    PetLogic,
    AttrTypeL1
};