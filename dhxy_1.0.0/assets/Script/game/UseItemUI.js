let GameRes = require('./GameRes');
let CPubFunction = require('./PubFunction');
let GoodsMgr = require('./GoodsMgr');

cc.Class({
    extends: cc.Component,
    properties: {
        titleLab: cc.Label,
        listNode: cc.Node,
        itemBtn: cc.Node,
    },

    onLoad() {
        // this.node.on(cc.Node.EventType.TOUCH_START, this.touchBegan.bind(this));
        this.itemBtn.active = false;
        this.curindex = 0;
        this.operateid = 0;
        this.operatetype = 0;

        this.currentBtn = null;
        this.timecnt = 0;
        this.maxtiemcnt = 30;

        this.listInfo = {};
    },

    initBtn(btn, data) {
        btn.datainfo = data;
        btn.on(cc.Node.EventType.TOUCH_START, this.propertyBtnClick.bind(this));
        btn.on(cc.Node.EventType.TOUCH_CANCEL, this.propertyBtnClick.bind(this));
        btn.on(cc.Node.EventType.TOUCH_END, this.propertyBtnClick.bind(this));
    },

    propertyBtnClick(e) {
        if (e.type == cc.Node.EventType.TOUCH_START) {
            this.maxtiemcnt = 30;
            this.timecnt = 0;
            this.currentBtn = e.target;
        }
        else if (e.type == cc.Node.EventType.TOUCH_END || e.type == cc.Node.EventType.TOUCH_CANCEL) {
            this.timecnt = this.maxtiemcnt;
            this.update();
            this.currentBtn = null;
        }
    },

    update() {
        if (this.currentBtn == null) {
            return;
        }
        this.timecnt++;
        if (this.timecnt >= this.maxtiemcnt) {
            if (this.maxtiemcnt > 10) {
                this.maxtiemcnt -= 1;
            }
            this.timecnt = 0;
            this.itemClicked(null, this.currentBtn.datainfo);
        }
    },

    loadList(title, list, operateid = 0, operatetype = 0) {
        this.operatetype = operatetype;
        this.operateid = operateid;
        this.titleLab.string = title;
        this.listNode.height -= 100;
        for (const itemid of list) {
            this.addItem(GoodsMgr.getItem(itemid));
        }
    },

    setUIPos(x, y) {
        this.listNode.x = x;
        this.listNode.y = y + this.listNode.height;
    },

    addItem(info) {
        if (info == null) {
            return;
        }
        this.listNode.height += 100;
        let btn = cc.instantiate(this.itemBtn);
        btn.parent = this.listNode;
        btn.active = true;
        btn.y = -90 - 100 * this.curindex;
        btn.x = 0;
        if (this.operatetype == 0) {
            btn.getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, 'UseItemUI', 'itemClicked', info.id));
        }
        else {
            this.initBtn(btn, info.id);
        }

        btn.getChildByName('icon').getComponent(cc.Sprite).spriteFrame = GameRes.getItemIcon(info.icon);
        btn.getChildByName('name').getComponent(cc.Label).string = info.name;
        btn.getChildByName('detail').getComponent(cc.Label).string = info.detailshot;

        if (info.id < 90000) {//90000以上为特殊物品
            btn.getChildByName('num').getComponent(cc.Label).string = info.count;
            if (info.count == 0) {
                btn.getChildByName('num').color = cc.Color.RED;
                btn.getChildByName('get').active = true;
            }
        }
        else {
            btn.getChildByName('num').active = false;
            btn.getChildByName('detail').color = cc.Color.GREEN;
        }
        this.curindex++;
        this.listInfo[info.id] = btn;
    },

    refreshInfo(){
        for (const key in this.listInfo) {
            if (this.listInfo.hasOwnProperty(key)) {
                const btn = this.listInfo[key];
                let info = GoodsMgr.getItem(key);
                if (key < 90000) {//90000以上为特殊物品
                    btn.getChildByName('num').getComponent(cc.Label).string = info.count;
                    if (info.count == 0) {
                        btn.getChildByName('num').color = cc.Color.RED;
                        btn.getChildByName('get').active = true;
                    }
                }
            }
        }
    },

    itemClicked(e, d) {
        GoodsMgr.useItem(d, 1, this.operateid);
    },

    touchBegan(event) {
        // if (!this.listNode.getBoundingBoxToWorld().contains(event.getLocation()))
        //     this.node.destroy();
        // return true;
        this.node.destroy();
    },
});
