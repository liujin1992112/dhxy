
let Player = require('../player/player');
cc.Class({
    extends: cc.Component,

    properties: {
        listContent: cc.Node,
        listItem: cc.Node,
        noNode:cc.Node,
    },

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
    },

    loadList(data) {
        let curX = -220;
        let curY = -230;
        this.listContent.destroyAllChildren();
        //---zfy -->这个是从服务器获取到  红包数量
        this.noNode.active = data.list.length == 0;
        this.maxNum = data.list.length;
        
        for (let i = 0; i < this.maxNum; i++) {

            this.item = cc.instantiate(this.listItem);
            this.item.active = true;
            this.item.x = curX;
            this.item.y = curY;
            
            let itemBtn = cc.find('itemBtn',this.item); 
            let btnLabel = cc.find('btnLabel',this.item);
            btnLabel.active = true;
            let btnLabel_1 = cc.find('btnLabel_1',this.item);
            let btnLabel_2 = cc.find('btnLabel_2',this.item);

            let bone = this.item.getComponent(sp.Skeleton);
            bone.animation = 'idle'

            let name = cc.find('nameLabel',this.item);
            let nameStr = data.list[i].rolename;
            if(nameStr.length>6){
                nameStr = nameStr.substring(0,6);
            }
            name.getComponent(cc.Label).string = nameStr;


            if(data.list[i].state == 1){
                itemBtn.interactable = false;
                btnLabel.active = false;
                btnLabel_1.active = true;
                bone.animation = 'idle2';
            }else if(data.list[i].state == 2){
                itemBtn.interactable = false;
                btnLabel.active = false;
                btnLabel_2.active = true;
                bone.animation = 'idle2';
            }

            curX += 220;
            if (curX > 220) {
                curX = -220;
                curY -= 260;
            }
            let tag = data.list[i].tagid;
            itemBtn.tag = tag;
            this.item.parent = this.listContent;
            this.item.tag = tag;
            cc.log('--tag->' + tag);

        }
        this.listContent.height = -curY + 100;
        if (this.listContent.height < this.listContent.parent.height) {
            this.listContent.height = this.listContent.parent.height;
        }
    },

    start() {
        this.clickList = [];
        this.rewardList = [];
    },

    // 领取红包请求
    onItemClicked(e, d) {
        let target = e.target.tag;

        let item = this.listContent.getChildByTag(target);
        let btn = item.getChildByTag(target);
        let label = cc.find('btnLabel',item);
        label.active = false;

        if(btn.interactable == false){
            return;
        }
        
        let name = cc.find('nameLabel',item);
        name.active = false;
        let bone = item.getComponent(sp.Skeleton);
        bone.setAnimation(1,'bao',false);

        bone.setCompleteListener(function(){
            btn.interactable = false;
            cc.ll.net.send('c2s_world_reward_open',{
                tagID: target,
                roleid:cc.ll.player.roleid,
            });
        }.bind(item));
    },


    // upRewardUI(data){
    //     cc.log('---更新UI-->' );
    //     let curX = -220;
    //     let curY = -110;
    //     this.listContent.destroyAllChildren();
    //     for(let i =0;i<this.maxNum;i++){
    //         this.item = cc.instantiate(this.listItem);
    //         this.item.active = true;
    //         this.item.x = curX;
    //         this.item.y = curY;

    //         if(data.list[i].rState == 1){
    //             let listRo = data.list[i].listrole;
    //             let listNum = listRo.length;
    //             for(let a=0;a<listNum;a++){
    //                 if(listRo[a].roleid == cc.ll.player.roleid){
    //                     this.item.getComponent(cc.Button).interactable = false;
    //                     cc.find('Label', this.item).getComponent(cc.Label).string = '已领取';
    //                 }
    //             }
    //         }

    //         curX += 220;
    //         if (curX > 220) {
    //             curX = -220;
    //             curY -= 220;
    //         }
    //         this.item.parent = this.listContent;
    //         this.item.tag = i;
    //     }
    // },

    // 发红包
    onSendRewardBtn(){
        cc.loader.loadRes("Prefabs/SendWorldReward", (err, prefab) => {
            this.mainUI = cc.find('Canvas');
            let worldReward = cc.instantiate(prefab);
            if (worldReward) {
                worldReward.parent = this.mainUI;
            }
        });
        this.node.destroy();
    },

    // update (dt) {},
});