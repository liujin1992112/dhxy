let GameRes = require('./GameRes');
let CPubFunction = require('./PubFunction');
let roleTitleUtil = require('../utils/RoleTitlesUtil')
let GameDefine = require('./GameDefine');

cc.Class({
    extends: cc.Component,


    properties:
    {
        scrollViewContent: cc.Node,
        roleTitleItem: cc.Prefab,
        titleName: cc.Label,
        titleDesc: cc.Label,
        btnUnload: cc.Node,
        btnLoad: cc.Node,
    },


    onLoad() {
        cc.ll.net.send('c2s_title_info', {});   //请求用户所有的称谓数据
        this.selectRoleTitleId = -1;
        this.selectRoleTitleType = -1;
        this.selectRoleTitleName = '';
        this.titles = [];

    },

    //初始化用户称谓列表数据
    initScrollViewTitle(data){
        let roleTitleIdList = data;
        this.titles = JSON.parse(roleTitleIdList.titles);
        this.titles.forEach((titleItem,index) => {
            let pkgTitltItem = roleTitleUtil.pkgRoleTitle(titleItem,cc.ll.player.bangname);
            if(pkgTitltItem != null){
                let pre_roleTitle = cc.instantiate(this.roleTitleItem);
                var toggle = pre_roleTitle.getComponent(cc.Toggle);
                
                var toggleName = pre_roleTitle.getChildByName('LblTitleName').getComponent(cc.Label);
                toggleName.string = pkgTitltItem.name;

                var toggleName = pre_roleTitle.getChildByName('LblTitleType').getComponent(cc.Label);
                toggleName.string = pkgTitltItem.type;


                var checkEventHandler = new cc.Component.EventHandler();
                checkEventHandler.target = this; 
                checkEventHandler.component = "SetRoleTitleUI";
                checkEventHandler.handler = "onSelectRoleTitle";            
                checkEventHandler.customEventData = pkgTitltItem.id;                

                if(index == 0){
                    toggle.isChecked = true;
  
                    this.titleName.string = pkgTitltItem.name;
                    this.titleDesc.string =  pkgTitltItem.desc;        
                    this.selectRoleTitleId = pkgTitltItem.id;  
                    this.selectRoleTitleName = pkgTitltItem.name;
                    this.selectRoleTitleType = pkgTitltItem.type;

                    if(!pkgTitltItem.onLoad){
                        this.btnLoad.active = true;                  
                        this.btnUnload.active = false;
                    }else{
                        this.btnLoad.active = false;                  
                        this.btnUnload.active = true;
                    }
                    
                }else{
                    toggle.isChecked = false;
                }

                toggle.checkEvents.push(checkEventHandler);
                
                pre_roleTitle.parent = this.scrollViewContent;
            }
        });
        
       
    },

    
    onClickedBtnLoad(){
        if(this.selectRoleTitleId != -1){
            if(this.selectRoleTitleId == cc.ll.player.titleid && this.selectRoleTitleType == cc.ll.player.titletype){     
                //特殊称谓           
                if(this.selectRoleTitleType == GameDefine.TitleType.BroTitle || this.selectRoleTitleType == GameDefine.TitleType.CoupleTitle){
                    if(this.selectRoleTitleName == cc.ll.player.titleval){
                        cc.ll.net.send('c2s_title_change', {type: -1,titleid:0,value:'',operatetype:1});     //卸下当前称谓
                    }else{
                        cc.ll.net.send('c2s_title_change', {type:this.selectRoleTitleType,titleid:this.selectRoleTitleId,value:this.selectRoleTitleName,operatetype:2});        //装备新称谓
                    }
                }else{
                    cc.ll.net.send('c2s_title_change', {type: -1,titleid:0,value:'',operatetype:1});     //卸下当前称谓
                }
            }else{
                //图片或者普通称谓
                cc.ll.net.send('c2s_title_change', {type:this.selectRoleTitleType,titleid:this.selectRoleTitleId,value:this.selectRoleTitleName,operatetype:2});        //装备新称谓
            }
        }else{
            CPubFunction.CreateNotice(cc.find('Canvas'), '请先选择一个称谓', 2);
        }
    },

    //接收服务端传来的操作称谓结果
    changeBtnLoadStatus(data){
        if(data.ecode == 0){
            if(data.operatetype == 2){               
                this.btnUnload.active = true;
                this.btnLoad.active = false;                   
            }else{
                let childrens = this.scrollViewContent.getChildren();

                for (const child of childrens) {
                    var toggle = child.getComponent(cc.Toggle);
                    toggle.isChecked = false;
                }

                this.btnUnload.active = false;
                this.btnLoad.active = true;
            }

            cc.ll.player.titleid = data.titleid;
            cc.ll.player.titletype = data.type;
            cc.ll.player.titleval = data.value;
            let logic = cc.find('Canvas').getComponent('GameLogic');
            if (logic) {
                logic.resetSelfPlayerTitle();
            }

        }else{
            console.log('称谓操作失败');
        }
    },


    
    onSelectRoleTitle(toggle, roleTitleId){
        roleTitleId = Number(roleTitleId);

        let titleName = toggle.node.getChildByName('LblTitleName').getComponent(cc.Label).string;


        var roleTitle = roleTitleUtil.getRoleTitle(roleTitleId,titleName,cc.ll.player.bangname);


        this.titleName.string = roleTitle.name;
        this.titleDesc.string = roleTitle.desc;

        this.selectRoleTitleId = roleTitleId;
        this.selectRoleTitleName = roleTitle.name;
        this.selectRoleTitleType = roleTitle.type;

        /*
        let titleList = this.titles.filter(e => {
            if(e.titleid == roleTitleId)
                return true;
        });

        if(titleList.length > 0){
            let titleItem = titleList[0];
            this.selectRoleTitleType = titleItem.type;
        }
        */

        if(this.selectRoleTitleId != cc.ll.player.titleid){
            this.btnUnload.active = false;
            this.btnLoad.active = true;
        }else{
            this.btnUnload.active = true;
            this.btnLoad.active = false;
        }

    },


    onCloseBtnClicked(e, d) {
		//zfy --关闭音效
        cc.ll.AudioMgr.playAudio('ui/ui_guanbi');
		this.node.destroy();
	},

});
