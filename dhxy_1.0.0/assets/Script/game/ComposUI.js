﻿let GameRes = require('./GameRes');
let CPubFunction = require('./PubFunction');
let CMainPlayerInfo = require('./MainPlayerInfo');

cc.Class({
    extends: cc.Component,


    properties:
    {
        nodComposItem: cc.Prefab,
        NumPad: cc.Prefab,

        numBar: {
            default: null,
            type: cc.ProgressBar,
        },
    },


    onLoad() {
        this.mapExplan = {};
        this.mapExplan[30002] = ['30001:6'];
        this.mapExplan[30003] = ['30002:5'];
        this.mapExplan[30004] = ['30003:4'];
        this.mapExplan[30005] = ['30004:3'];
        this.mapExplan[30006] = ['30005:2'];

        this.mapExplan[30008] = ['30007:6'];
        this.mapExplan[30009] = ['30008:5'];
        this.mapExplan[30010] = ['30009:4'];
        this.mapExplan[30011] = ['30010:3'];
        this.mapExplan[30012] = ['30011:2'];

        this.mapExplan[30014] = ['30013:6'];
        this.mapExplan[30015] = ['30014:5'];
        this.mapExplan[30016] = ['30015:4'];
        this.mapExplan[30017] = ['30016:3'];
        this.mapExplan[30018] = ['30017:2'];

        this.mapExplan[30020] = ['30019:6'];
        this.mapExplan[30021] = ['30020:5'];
        this.mapExplan[30022] = ['30021:4'];
        this.mapExplan[30023] = ['30022:3'];
        this.mapExplan[30024] = ['30023:2'];

        this.mapExplan[30026] = ['30025:6'];
        this.mapExplan[30027] = ['30026:5'];
        this.mapExplan[30028] = ['30027:4'];
        this.mapExplan[30029] = ['30028:3'];
        this.mapExplan[30030] = ['30029:2'];

        this.mapExplan[50004] = ['10301:1', '10302:1', '10303:1'];

    },

    start() {
        this.btnFatherItem = cc.find('btnFatherItem', this.node);
        this.stFatherItemInfo = null;
        this.vecTempA = [];
        this.vecTempB = [];


        cc.find('btnClose', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "ComposUI", "Close", 0));
        cc.find('btnFatherItem', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "ComposUI", "OpenNumPad", 0));
        cc.find('btnOK', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "ComposUI", "OnOK", 0));


        this.InitFatherItemList();

    },

    Close() {
        this.node.destroy();
    },



    onSliderClicked(slider, customEventData) {
        this.numBar.progress = slider.progress;
        // let n = 1;
        // if(slider.progress == 1){
        //     cc.find('numStr', this.node).getComponent(cc.Label).string = Math.floor(this.cunt)+'/'+Math.floor(this.cunt);
        // }else if(slider.progress == 0){
        //     cc.find('numStr', this.node).getComponent(cc.Label).string = Math.floor(this.cunt)+'/1';
        // }else if(slider.progress>= 1/Math.floor(this.cunt)&&Math.floor(this.cunt)>=3){
        //     n = n+1;
        //     cc.find('numStr', this.node).getComponent(cc.Label).string = Math.floor(this.cunt)+'/'+n;
        // }
        let n = Math.floor(this.currNum / this.needNum);
        let label = cc.find('numStr', this.node).getComponent(cc.Label);
        let m = Math.floor(slider.progress * n);
        if (n > 0 && m <= 0) {
            m = 1;
        }
        this.numSend = m;
        label.string = m + '/' + n;
    },

    InitFatherItemList() {
        let goContent = cc.find('scvFather/view/content', this.node);

        CPubFunction.DestroyVecNode(this.vecTempA);

        let stStart = { nX: -90, nY: -58 };

        let nIndex = -1;

        for (var it in this.mapExplan) {
            nIndex++;

            let stPos = { nX: stStart.nX + (nIndex % 3) * 90, nY: stStart.nY - Math.floor(nIndex / 3) * 90 };
            let goItem = CPubFunction.CreateSubNode(goContent, stPos, this.nodComposItem, '');
            goItem.getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "ComposUI", 'SetFatherItem', { nItemID: it, nNum: 1 }));

            if (this.stFatherItemInfo == null) {
                this.stFatherItemInfo = { nItemID: it, nNum: 1 };
            }


            this.SetItemInfo(goItem, it, '')
            this.vecTempA.push(goItem);
        }

        this.SetFatherItem(0, this.stFatherItemInfo);
        goContent.height = Math.max(goContent.height, Math.ceil((CPubFunction.GetMapLen(this.mapExplan) + 1) / 3) * 90 + 60);
    },


    SetItemInfo(goItem, nID, strText) {
        let stConfigInfo = cc.ll.propData.item[nID];
        cc.find('Icon', goItem).getComponent(cc.Sprite).spriteFrame = GameRes.getItemIcon(stConfigInfo.icon);

        if (Math.floor(this.cunt) < 1 && strText.length >= 3) {
            cc.find('Label', goItem).getComponent(cc.Label).node.color = new cc.color(255, 0, 0, 255);
        }
        cc.find('Label', goItem).getComponent(cc.Label).string = strText;
    },


    SetFatherItem(e, stInfo) {
        this.stFatherItemInfo = stInfo;
        this.SetItemInfo(this.btnFatherItem, stInfo.nItemID, stInfo.nNum);

        CPubFunction.DestroyVecNode(this.vecTempB);
        let goContent = cc.find('scvSon/view/content', this.node);
        let vecSonItem = this.mapExplan[stInfo.nItemID];
        let nLen = vecSonItem.length;

        for (var it in vecSonItem) {
            let vecData = vecSonItem[it].split(':');
            let stSonInfo = { nItemID: parseInt(vecData[0]), nNum: parseInt(vecData[1]) };

            let stPos = { nX: - 90 * (nLen - 1) / 2 + it * 90, nY: 0 }

            let goSun = CPubFunction.CreateSubNode(goContent, stPos, this.nodComposItem, '');

            this.currNum = CMainPlayerInfo.GetBagItemCnt(stSonInfo.nItemID);
            this.needNum = this.stFatherItemInfo.nNum * stSonInfo.nNum;

            let n = 0;
            this.cunt = this.currNum / this.needNum;
            if (Math.floor(this.cunt) >= 1) {
                n = 1;
            }
            cc.find('numStr', this.node).getComponent(cc.Label).string = n + '/' + Math.floor(this.cunt);

            this.SetItemInfo(goSun, stSonInfo.nItemID, `${CMainPlayerInfo.GetBagItemCnt(stSonInfo.nItemID)}/${this.stFatherItemInfo.nNum * stSonInfo.nNum}`)
            this.vecTempB.push(goSun);
        }

        let stSonInfo = { nItemID: vecSonItem[0], nNum: vecSonItem[1] };
    },


    OpenNumPad(e, d) {
        if (null == this.stFatherItemInfo)
            return;


        CPubFunction.FindAndDeleteNode(this.node, 'NumPad');

        let goNumPad = CPubFunction.CreateSubNode(this.node, { nX: 177, nY: 188 }, this.NumPad, 'NumPad');
        goNumPad.getComponent('NumPad').NumPad_Init((nNum) => {
            let nCnt = CPubFunction.ChangeNumToRange(nNum, 1, 10000);
            this.SetComposCnt(nCnt);
        });
    },


    SetComposCnt(nCnt) {
        if (null == this.stFatherItemInfo)
            return;

        this.stFatherItemInfo.nNum = nCnt;

        this.SetFatherItem(this.btnFatherItem, this.stFatherItemInfo)
    },


    OnReceiveBagItem() {
        this.SetFatherItem(0, this.stFatherItemInfo);
    },



    OnOK() {
        let num = 0;
        if (null == this.stFatherItemInfo)
            return;
        if (Math.floor(this.cunt) == 0) {
            num = 1;
        } else {
            if (this.numSend == null || this.numSend == 0) {
                num = 1;
            } else {
                num = this.numSend;
            }
        }
        cc.ll.net.send('c2s_compose', { nFatherItem: this.stFatherItemInfo.nItemID, nNum: num });
    },

});
