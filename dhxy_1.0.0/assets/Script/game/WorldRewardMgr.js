
class WorldRewardMgr{
    

    showLoadList(data){
        this.mainUI = cc.find('Canvas');
        let worldRewardPanel = this.mainUI.getChildByName('WorldReward');
        if(worldRewardPanel){
            let panellogic = worldRewardPanel.getComponent('WorldReward');
            panellogic.loadList(data);
        }else{
            cc.loader.loadRes("Prefabs/WorldReward", (err, prefab) => {
                let worldRewardPanel = cc.instantiate(prefab);
                if (worldRewardPanel) {
                    worldRewardPanel.name = 'WorldReward';
                    worldRewardPanel.parent = this.mainUI;
        
                    let panellogic = worldRewardPanel.getComponent('WorldReward');
                    panellogic.loadList(data);
                }
            });
        }
    }
}

let instance = null;
module.exports = (()=>{
    if(instance==null){
        instance = new WorldRewardMgr();
    }
    return instance;
})();
