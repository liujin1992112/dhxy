﻿
let CPubFunction = require('./PubFunction');
let GameDefine = require('./GameDefine');

/**
 * Npc管理器
 */
class CNpcMgr {
    constructor() {
        this.mapNpcConfig = {};//地图上Npc配置
        this.mapNpcObj = {};//地图上Npc对象
    }

    /**
     * 加载npc配置
     */
    LoadNpcJson() {
        for (const it in cc.ll.propData.npc) {
            if (it == 'datatype')
                continue;

            let stData = cc.ll.propData.npc[it];
            this.mapNpcConfig[it] = { nConfigID: it, nResID: stData.resid, strName: stData.name, stTalk: stData.talk, mapButton: this.GetMapButton(stData.mapButton), nKind: stData.kind, stAutoCreate: this.GetCreate(stData.auto_create) };
        }
    }

    AddNpcObj(nOnlyID, goObj) {
        if (this.mapNpcObj.hasOwnProperty(nOnlyID)) {
            this.mapNpcObj[nOnlyID].destroy();
        }
        this.mapNpcObj[nOnlyID] = goObj;
    }

    CheckNpcRes(onlyid, resid) {
        for (const ponlyid in this.mapNpcObj) {
            if (ponlyid == onlyid) {
                continue;
            }
            const npcnode = this.mapNpcObj[ponlyid];
            if (npcnode.resid == resid) {
                return true;
            }
        }
        return false;
    }

    GetNpcDefaultDir(nConfigID) {
        if (this.mapNpcConfig.hasOwnProperty(nConfigID) == false)
            return 0;

        let pAutoCreate = this.mapNpcConfig[nConfigID].stAutoCreate;
        if (null == pAutoCreate)
            return 0;

        return pAutoCreate.dir;
    }

    GetMapButton(strJson) {
        if (strJson == "")
            return JSON.parse('{}');

        let mapButton = JSON.parse(strJson);
        return mapButton;
    }

    GetCreate(strData) {
        if (typeof (strData) == "undefined")
            return null;

        let vecData = [];
        let vecTmp = strData.split(";");
        if (vecTmp.length != 4)
            return null;

        return { map: parseInt(vecTmp[0]), x: parseInt(vecTmp[1]), y: parseInt(vecTmp[2]), dir: parseInt(vecTmp[3]) };
    }

    GetCurSceneNode() {
        let nodMap = cc.find('Canvas/MapUI');
        return nodMap;
    }

    GetCurGameMapID() {
        let goScene = this.GetCurSceneNode();
        if (null == goScene)
            return null;

        return goScene.getComponent('GameMapLogic').mapId;
    }


    ReloadNpcJson() {
        if (CPubFunction.GetMapLen(this.mapNpcConfig) == 0)
            this.LoadNpcJson();

        for (var it in this.mapNpcObj) {
            this.mapNpcObj[it].destroy();
        }

        this.mapNpcObj = {};
    }



    GetNpcConfigInfo(nConfigID) {
        if (this.mapNpcConfig.hasOwnProperty(nConfigID) == false)
            return null;

        return this.mapNpcConfig[nConfigID];
    }



    FindNpcByConfigID(nConfigID) {
        let mapPlayer = this.mapNpcObj;

        for (let it in mapPlayer) {
            if (mapPlayer[it].livingtype != GameDefine.LivingType.NPC)
                continue;

            let comNpc = mapPlayer[it].getComponent('Npc');
            if (comNpc.nConfigID != nConfigID)
                continue;

            return mapPlayer[it];
        }

        return null;
    }


    GetNpcPos(nConfigID) {
        for (let it in this.mapNpcConfig) {
            if (this.mapNpcConfig[it].nConfigID == nConfigID) {
                let stCreate = this.mapNpcConfig[it].stAutoCreate;
                if (null == stCreate)
                    continue;

                return { nNpc: nConfigID, nMap: stCreate.map, nX: stCreate.x, nY: stCreate.y, nDir: stCreate.dir };
            }
        }
        return null;
    }

    DestroyNpc(onlyid) {
        this.mapNpcObj[onlyid].getComponent('Npc').clear();
        this.mapNpcObj[onlyid].parent = null;
        this.mapNpcObj[onlyid].destroy();
        delete this.mapNpcObj[onlyid];
    }

}

let g_NpcMgr = null;
module.exports = (() => {
    if (g_NpcMgr == null) {
        g_NpcMgr = new CNpcMgr();
    }
    return g_NpcMgr;
})();



