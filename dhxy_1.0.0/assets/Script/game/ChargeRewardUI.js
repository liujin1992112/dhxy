cc.Class({
	extends: cc.Component,
	properties: {
		content: cc.Node,
		reward_item: cc.Prefab,
	},

	start () {
		/* cc.ll.http.send('/getChargeReward', {roleid: cc.ll.player.roleid}, ret => {
			let charge_sum = 0;
			let reward_record = [];
			if (ret.errcode == 0 && ret.data) {
				charge_sum = ret.data.charge_sum;
				if (ret.data.reward_record)
					reward_record = ret.data.reward_record.split(':');
			}
			this.initScroll(charge_sum, reward_record);
		});  */
		this.initScroll(cc.ll.player.chargesum, cc.ll.player.rewardrecord);
	},

	initScroll (charge_sum, reward_record) {
		let reward_list = cc.ll.propData.chargereward;
		let list = [];
		for (let i = 1; reward_list[i]; ++i) {
			let item = reward_list[i];
			list.push(item);
		}

		list.sort((a, b) => {
			return a.money - b.money;
		});

		for (let i = 1; list[i]; ++i) {
			let item = list[i];
			let node = cc.instantiate(this.reward_item);
			node.parent = this.content;
			let has_gain = ((reward_record&(1<<(item.id-1))) > 0);
			let logic = node.getComponent('ChargeRewardItem');
			let reward = [];
			if (item.gid1 && item.count1)
				reward.push({gid: item.gid1, count: item.count1});
			if (item.gid2 && item.count2)
				reward.push({gid: item.gid2, count: item.count2});
			logic.init(item.money, charge_sum, has_gain, reward, item.id);
		}
	},

	onButtonClick (event, param) {
		if (param == 'close') 
			this.node.destroy();
	},
});
