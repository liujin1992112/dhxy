/**
 * MainUI/buttom_left/ui_common_bg_chat_main上面的聊天界面逻辑处理
 */
cc.Class({
	extends: cc.Component,

	properties: {
		/** 滚动列表 */
		scrollNode: cc.Node,
		/** 滚动列表上的Content */
		contentNode: cc.Node,
		/** 聊天面板预制体 */
		chatPanel: cc.Prefab,
	},

	onLoad() {
		this.chatItemList = [];
		this.maxItems = 50;
		this.chatItemNodes = [];

		this.scrollNode.on('scroll-began', this.scrollBegan.bind(this));
		this.scrollNode.on('scroll-ended', this.scrollEnded.bind(this));
		this.scrollNode.on('touch-up', this.touchUp.bind(this));

		this.beganPos = this.contentNode.y;
	},

	scrollBegan(event) {
		this.beganPos = this.contentNode.y;
	},

	scrollEnded(event) {
		this.beganPos = this.contentNode.y;
	},

	touchUp(event) {
		if (Math.abs(this.contentNode.y - this.beganPos) < 4) {
			let parent = cc.find('Canvas');
			if (parent.getChildByName('ChatPanel') == null) {
				let panel = cc.instantiate(this.chatPanel);
				panel.parent = parent;
				panel.getComponent('ChatPanel').loadListInfo(this.chatItemList);
				this.scheduleOnce(() => {
					panel.x = panel.y = 0;
				}, 0);
			}
		}
	},

	onChatBtnClicked(e, d) {
		let parent = cc.find('Canvas');
		if (parent.getChildByName('ChatPanel') == null) {
			let panel = cc.instantiate(this.chatPanel);
			panel.x = panel.y = 0;
			panel.parent = parent;
			panel.getComponent('ChatPanel').loadListInfo(this.chatItemList);
			this.scheduleOnce(() => {
				panel.x = panel.y = 0;
			}, 0);
		}
	},

	addListInfo(info) {
		if (info.scale == 5) {
			this.fixChatItem(info.roleid);
			return;
		}
		this.chatItemList.push(info);
		this.addChatItem(info);

		if (this.chatItemList.length > this.maxItems) {
			this.delFirstChatItem();
			this.chatItemList.shift();
		}
	},

	delFirstChatItem() {
		let delItem = this.chatItemNodes.shift();
		if (delItem) {
			this.contentNode.height -= delItem.height;
			for (const item of this.chatItemNodes) {
				item.y += delItem.height;
			}
			delItem.destroy();
		}
	},

	addChatItem(info) {
		if (info.msg.length < 0 && info.voice < 0) {
			return;
		}

		let self = this;
		cc.loader.loadRes('Common/ChatEmoji', cc.SpriteAtlas, function (err, atlas) {
			let chatItem = new cc.Node();
			chatItem.parent = self.contentNode;
			let richText = chatItem.addComponent('CustomRichText');
			richText.maxWidth = self.contentNode.width - 6;
			richText.fontSize = 18;
			richText.lineHeight = 20;
			richText.type = 1;
			richText.scale = info.scale;
			richText.rolename = info.name;
			richText.emojiAtlas = atlas;
			if (info.msg.length > 0) {
				richText.string = info.msg;
			} else {
				if (info.voice >= 0) {
					richText.string = '[语音消息]';
				}
			}

			chatItem.roleid = info.roleid;
			chatItem.x = 3;

			let blankHeight = self.contentNode.height;
			if (self.chatItemNodes[0]) {
				blankHeight = -self.chatItemNodes[0].y;
			}
			if (chatItem.height > blankHeight) {
				self.contentNode.height += chatItem.height - blankHeight;
				for (const item of self.chatItemNodes) {
					item.y += blankHeight;
				}
			}
			else {
				for (const item of self.chatItemNodes) {
					item.y += chatItem.height;
				}
			}

			chatItem.y = -self.contentNode.height + chatItem.height;
			self.chatItemNodes.push(chatItem);

			if (self.contentNode.y > self.contentNode.height - self.contentNode.parent.height - chatItem.height - 20) {
				self.contentNode.y = self.contentNode.height - self.contentNode.parent.height;
				self.beganPos = self.contentNode.y;
			}
		});
	},

	fixChatItem(roleid) {
		for (const chatitem of this.chatItemNodes) {
			if (chatitem.roleid == roleid) {
				let richText = chatitem.getComponent('CustomRichText');
				richText.string = '给全体玩家拜年啦！！！';
			}
		}
	},

});
