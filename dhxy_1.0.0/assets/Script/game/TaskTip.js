﻿let CPubFunction = require('./PubFunction');
let CMainPlayerInfo = require('./MainPlayerInfo');
let pTaskConfigMgr = require('./task_config').g_pTaskConfigMgr;
let EState = require('./task_config').EState;
let EEventType = require('./task_config').EEventType;
let ETaskKind = require('./task_config').ETaskKind;
let CNpcMgr = require('./NpcMgr');

/**
 * 主角做任务提示小面板
 */
cc.Class({
    extends: cc.Component,
    properties: {
        /**任务提示Item预制体 */
        btnTaskTip: cc.Prefab,
        preTaskTalkFlag: cc.Prefab,
    },

    ctor() {
        this.nEventType = 0;
        this.vecTaskTip = [];
        this.vecGoFlag = [];
        this.bShow = false;

        this.stAuto = {
            nKind: 0,
            nTaskID: 0,
        }
    },

    start() {
        //给隐藏任务提示按钮添加点击事件处理器
        cc.find('btnHidenTip', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "TaskTip", "SwitchShowOrHiden", 0));
        this.SwitchShowOrHiden();
        this.ResetTimer(1000);
    },

    /**
     * 负责控制任务提示小面板的显示或隐藏
     */
    SwitchShowOrHiden() {
        this.bShow = !this.bShow;
        let stPos = this.bShow ? {
            nX: 20,
            nY: 20
        } : {
            nX: 270,
            nY: 19
        };
        this.node.setPosition(cc.v2(stPos.nX, stPos.nY));
        cc.find('btnHidenTip/picFlag', this.node).angle = this.bShow ? 90 : 270;
    },

    IniTip() {
        if (typeof (this.vecTaskTip) == "undefined")
            return;

        CPubFunction.FindAndHidenNode(cc.find('Canvas/MainUI'), {
            nX: 0,
            nY: -1000
        });

        //销毁数组内节点
        CPubFunction.DestroyVecNode(this.vecTaskTip);

        //遍历任务列表
        for (let it in CMainPlayerInfo.vecTaskState) {
            let stTaskState = CMainPlayerInfo.vecTaskState[it];
            let stTaskConfig = pTaskConfigMgr.GetTaskInfo(stTaskState.nTaskID);

            for (let nStep in stTaskState.vecStepState) {
                //查找正在进行的任务
                if (stTaskState.vecStepState[nStep].nState != EState.EDoing)
                    continue;

                let stStepConfig = stTaskConfig.vecEvent[nStep];
                let stStepState = stTaskState.vecStepState[nStep];

                this.nEventType = stStepConfig.nEventType;

                //处理任务描述
                let strTip = stStepConfig.strTip;
                if (stStepState.nEventType == EEventType.PlayerGiveNpcItem) {//给Npc道具,判断背包此物品的数量
                    if (CMainPlayerInfo.GetBagItemCnt(stStepConfig.nItemID) >= stStepConfig.nNum)
                        strTip = stStepConfig.strTip2;
                }

                if (stStepState.nEventType == EEventType.PlayerGatherNpc) {//获取Npc
                    strTip += ` ${stStepConfig.vecNpc.length - stStepState.vecRemainNpc.length}/${stStepConfig.vecNpc.length}`;
                }

                let stTaskName = stTaskConfig.strTaskName;
                if (stTaskConfig.nKind == ETaskKind.EDaily) {//日常任务
                    stTaskName += ` (${CMainPlayerInfo.GetDailyCnt(stTaskConfig.nTaskGrop) + 1}/ ${stTaskConfig.nDailyCnt})`;
                }

                this.AddTaskTip(stTaskState.nTaskID, nStep, stTaskName, strTip);

                // cc.log('----stStepState.nEventType->' + stStepState.nEventType + '----stStepConfig.bAutoTrigle->' + stStepConfig.bAutoTrigle);
                if (stStepState.nEventType == EEventType.PlayerTalkNpc && stStepConfig.bAutoTrigle == 1) {
                    let vecSpeak = CPubFunction.GetTaskTalkList(stTaskState.nTaskID, nStep);
                    CPubFunction.CreateNpcTalk(vecSpeak, () => {
                        //对话完成,通知服务器
                        cc.ll.net.send('c2s_task_talk_npc', {
                            nTaskID: stTaskState.nTaskID,
                            nStep: nStep,
                            nNpcConfigID: stStepState.vecRemainNpc[0].nNpcConfigID,
                            nNpcOnlyID: stStepState.vecRemainNpc[0].nNpcOnlyID
                        });
                    });
                }
            }
        }

        this.ResetSize();
        this.UpdateNpcTalkFlag();
    },

    /**
     * 计算滚动列表大小
     * @returns 
     */
    GetCurTipListSize() {
        let nSize = 0;
        for (var it in this.vecTaskTip) {
            nSize += this.vecTaskTip[it].height;
        }
        return nSize;
    },

    /**
     * 添加任务到提示面板中
     * @param {*} nTaskID       任务ID
     * @param {*} nStepIndex    第几步
     * @param {*} strTaskName   任务名称
     * @param {*} strStepDetail 任务描述
     */
    AddTaskTip(nTaskID, nStepIndex, strTaskName, strStepDetail) {
        // cc.log('----nTaskIDe->' + nTaskID + '----nStepIndex->' + nStepIndex + '----strTaskName->' + strTaskName + '-----strStepDetail->' + strStepDetail);
        let goContent = cc.find('ScrollView/view/content', this.node);

        let nSize = this.GetCurTipListSize();

        //在滚动面板添加任务提示Item,并为其绑定事件
        let btnTaskTip = CPubFunction.CreateSubNode(goContent, {
            nX: 127,
            nY: -nSize
        }, this.btnTaskTip, 'btnTaskTip');

        btnTaskTip.getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "TaskTip", "OnClickStepTip", {
            nTaskID: nTaskID,
            nStep: nStepIndex
        }));

        //初始化节点信息
        let picTaskTip = cc.find('picTaskTip', btnTaskTip);

        let goName = cc.find('txTaskName', picTaskTip);
        cc.find('txTaskName', picTaskTip).getComponent(cc.Label).string = strTaskName;

        let goDetail = cc.find('txStepDetail', picTaskTip);
        let comLabelDetail = goDetail.getComponent(cc.Label);
        comLabelDetail.string = strStepDetail;

        picTaskTip.height = Math.max(picTaskTip.height, goName.height + goDetail.height + 25);
        btnTaskTip.height = picTaskTip.height;

        btnTaskTip.y = -nSize - btnTaskTip.height / 2;
        picTaskTip.y = btnTaskTip.height / 2;

        //将item添加到列表中
        this.vecTaskTip.push(btnTaskTip);

        this.ResetSize();
    },

    /**
     * 重新计算列表高度
     */
    ResetSize() {
        let nSize2 = this.GetCurTipListSize();

        let nodScrollView = cc.find('ScrollView', this.node);
        nodScrollView.height = CPubFunction.ChangeNumToRange(nSize2, 20, 350);
        cc.find('ScrollView/view', this.node).height = CPubFunction.ChangeNumToRange(nSize2, 20, 350);
        cc.find('ScrollView/view/content', this.node).height = nSize2;

        let picLine = cc.find('picLine', this.node);
        picLine.y = 125 - nodScrollView.height;
    },


    ResetTimer(t) {
        // let pSelf = this;
        clearInterval(this.m_timer);
        this.m_timer = setInterval(() => {
            this.CheckAndAutoClick();
        }, t);
    },

    CheckAndAutoClick() {
        if (this.stAuto == null || this.stAuto.nKind == 0)
            return;

        if (null == CMainPlayerInfo.IsHasCurTask(this.stAuto.nTaskID))
            this.stAuto.nTaskID = CMainPlayerInfo.FindDailyTask(this.stAuto.nKind);

        if (this.stAuto.nTaskID == 0)
            return;

        let nStep = CMainPlayerInfo.GetTaskCurStep(this.stAuto.nTaskID);
        let stStepConfig = pTaskConfigMgr.GetTaskStepInfo(this.stAuto.nTaskID, nStep);

        let goWaitUI = cc.find('picPercent', cc.find('Canvas/MainUI'));
        if (goWaitUI)
            return;

        let goTalkUI = cc.find('TalkUI', cc.find('Canvas/MainUI'));
        if (goTalkUI && goTalkUI.y > -1000) {
            let comTalkUI = goTalkUI.getComponent('Talk');
            comTalkUI.NextSpeak();
            return;
        }

        if (stStepConfig.nEventType == EEventType.PlayerGiveNpcItem && CMainPlayerInfo.GetBagItemCnt(stStepConfig.nItemID) < stStepConfig.nNum && cc.find('NpcFunUI', cc.find('Canvas/MainUI'))) {
            cc.ll.net.send('c2s_buy_from_npc', {
                nConfigID: stStepConfig.nFromNpc,
                nItemID: stStepConfig.nItemID,
                nCnt: stStepConfig.nNum
            });
            CPubFunction.FindAndDeleteNode(cc.find('Canvas/MainUI'), 'NpcFunUI');
            return;
        }

        this.OnClickStepTip(null, {
            nTaskID: this.stAuto.nTaskID,
            nStep: nStep
        });
    },


    GetTaskNpcPos(nTaskID, nStep, nConfigID) {

        let stPos = CNpcMgr.GetNpcPos(nConfigID);
        if (stPos != null)
            return stPos;


        let stStepConfig = pTaskConfigMgr.GetTaskStepInfo(nTaskID, nStep);
        if (stStepConfig.hasOwnProperty('vecCreateNpc')) {
            for (var it in stStepConfig.vecCreateNpc) {
                if (stStepConfig.vecCreateNpc[it].nNpc == nConfigID)
                    return stStepConfig.vecCreateNpc[it];
            }
        }

        return null;
    },

    /**
     * 点击任务提示Item,则接受任务
     * @param {*} e 
     * @param {*} data 
     * @returns 
     */
    OnClickStepTip(e, data) {
        if (cc.ll.player.teamid > 0 && !cc.ll.player.isleader) {
            return;
        }
        let comGameLogic = cc.find('Canvas').getComponent('GameLogic');

        let stTaskConfig = pTaskConfigMgr.GetTaskInfo(data.nTaskID);
        let stStepConfig = stTaskConfig.vecEvent[data.nStep];
        let stStepState = CMainPlayerInfo.GetTaskStepState(data.nTaskID, data.nStep);

        if (stTaskConfig.nKind == ETaskKind.EDaily) {
            this.stAuto.nKind = stTaskConfig.nTaskGrop;
            this.stAuto.nTaskID = data.nTaskID;
        } else {
            this.stAuto.nKind = 0;
            this.stAuto.nTaskID = 0;
        }

        if (stStepConfig.nEventType == EEventType.PlayerTalkNpc) {
            let stPos = this.GetTaskNpcPos(data.nTaskID, data.nStep, stStepState.vecRemainNpc[0].nConfigID);
            CPubFunction.MainPlayerToDo(stPos.nMap, stPos.nX, stPos.nY, () => {
                let pNpc = CNpcMgr.FindNpcByConfigID(stPos.nNpc);
                if (pNpc) {
                    pNpc.getComponent('Npc').OnNpcClick();
                }
            });
        }

        if (stStepConfig.nEventType == EEventType.PlayerGatherNpc) {
            let stPos = this.GetTaskNpcPos(data.nTaskID, data.nStep, stStepState.vecRemainNpc[0].nConfigID);

            CPubFunction.MainPlayerToDo(stPos.nMap, stPos.nX, stPos.nY, () => {
                CNpcMgr.FindNpcByConfigID(stPos.nNpc).getComponent('Npc').OnNpcClick();
            });
        }

        if (stStepConfig.nEventType == EEventType.PlayerDoAction) {
            CPubFunction.MainPlayerToDo(stStepConfig.nMap, stStepConfig.nX, stStepConfig.nY, () => {
                let comRole = CPubFunction.GetRole();
                CPubFunction.CreateRoleBroadCast(comRole.node, stStepConfig.strTalk);
                CPubFunction.CreateWaitTip(CPubFunction.GetDefault(stStepConfig.strAction, ''), 1, () => {
                    cc.ll.net.send('c2s_role_action', {
                        nMapID: stStepConfig.nMap,
                        nX: stStepConfig.nX,
                        nY: stStepConfig.nY
                    });
                });
            });
        }

        if (stStepConfig.nEventType == EEventType.PlayerArriveArea) {
            //角色到达目标的,服务器定时检测此任务,不需要客户端通知
            CPubFunction.MainPlayerToDo(stStepConfig.nMap, stStepConfig.nX, stStepConfig.nY, () => { });
        }

        if (stStepConfig.nEventType == EEventType.PlayerGiveNpcItem) {
            this.IniTip();
            let nCur = CMainPlayerInfo.GetBagItemCnt(stStepConfig.nItemID);
            let nNpc = nCur >= stStepConfig.nNum ? stStepConfig.nNpcConfigID : stStepConfig.nFromNpc;
            let stData = CNpcMgr.GetNpcPos(nNpc);
            if (null == stData)
                return;

            CPubFunction.MainPlayerToDo(stData.nMap, stData.nX, stData.nY, () => {
                let goNpc = CNpcMgr.FindNpcByConfigID(nNpc);
                if (goNpc != null)
                    CNpcMgr.FindNpcByConfigID(nNpc).getComponent('Npc').OnNpcClick();
            });
        }

        if (stStepConfig.nEventType == EEventType.PlayerKillNpc) {
            let stPos = this.GetTaskNpcPos(data.nTaskID, data.nStep, stStepState.vecRemainNpc[0].nConfigID);

            CPubFunction.MainPlayerToDo(stPos.nMap, stPos.nX, stPos.nY, () => {
                let goNpc = CNpcMgr.FindNpcByConfigID(stPos.nNpc);
                if (goNpc)
                    goNpc.getComponent('Npc').OnNpcClick();
            });
        }
    },

    UpdateNpcTalkFlag() {
        CPubFunction.DestroyVecNode(this.vecGoFlag);

        for (let it in CMainPlayerInfo.vecTaskState) {
            let stTaskState = CMainPlayerInfo.vecTaskState[it];
            let stTaskConfig = pTaskConfigMgr.GetTaskInfo(stTaskState.nTaskID);

            for (let nStep in stTaskState.vecStepState) {
                if (stTaskState.vecStepState[nStep].nState != EState.EDoing)
                    continue;

                if (stTaskState.vecStepState[nStep].nEventType != EEventType.PlayerTalkNpc)
                    continue;

                let goNpc = CNpcMgr.FindNpcByConfigID(stTaskState.vecStepState[nStep].vecRemainNpc[0].nConfigID);

                let goFlag = CPubFunction.CreateSubNode(goNpc, {
                    nX: -5,
                    nY: 190
                }, this.preTaskTalkFlag, 'preTaskTalkFlag');

                //-----------------

                goFlag.stopAllActions();

                let ani = cc.sequence(cc.scaleTo(0.5, 1, 1), cc.scaleTo(0.5, 1, 0.9));

                goFlag.runAction(cc.repeatForever(ani));


                //-----------------

                this.vecGoFlag.push(goFlag);
            }
        }
    }









});