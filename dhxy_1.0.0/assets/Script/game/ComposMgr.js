﻿


class ComposMgr
{
    constructor( )
    {
        this.mapExplan = {};
        this.Init();

    }

    Init()
    {
        this.mapExplan[30002] = [30001, 30001, 30001, 30001, 30001, 30001];
        this.mapExplan[30003] = [30002, 30002, 30002, 30002, 30002];
        this.mapExplan[30004] = [30003, 30003, 30003, 30003];
        this.mapExplan[30005] = [30004, 30004, 30004];
        this.mapExplan[30006] = [30005, 30005];

        this.mapExplan[30008] = [30007, 30007, 30007, 30007, 30007, 30007];
        this.mapExplan[30009] = [30008, 30008, 30008, 30008, 30008];
        this.mapExplan[30010] = [30009, 30009, 30009, 30009];
        this.mapExplan[30011] = [30010, 30010, 30010];
        this.mapExplan[30012] = [30011, 30011];

        this.mapExplan[30014] = [30013, 30013, 30013, 30013, 30013, 30013];
        this.mapExplan[30015] = [30014, 30014, 30014, 30014, 30014];
        this.mapExplan[30016] = [30015, 30015, 30015, 30015];
        this.mapExplan[30017] = [30016, 30016, 30016];
        this.mapExplan[30018] = [30017, 30017];

        this.mapExplan[30020] = [30019, 30019, 30019, 30019, 30019, 30019];
        this.mapExplan[30021] = [30020, 30020, 30020, 30020, 30020];
        this.mapExplan[30022] = [30021, 30021, 30021, 30021];
        this.mapExplan[30023] = [30022, 30022, 30022];
        this.mapExplan[30024] = [30023, 30023];


        this.mapExplan[30026] = [30025, 30025, 30025, 30025, 30025, 30025];
        this.mapExplan[30027] = [30026, 30026, 30026, 30026, 30026];
        this.mapExplan[30028] = [30027, 30027, 30027, 30027];
        this.mapExplan[30029] = [30028, 30028, 30028];
        this.mapExplan[30030] = [30029, 30029];

        this.mapExplan[50004] = [10301, 10302, 10303];

        

    }


    GetFatherItem(nItem)
    {
        for (var it in this.mapExplan)
        {
            let vecItem = this.mapExplan[it];

            for (let itItem in vecItem)
            {
                if (vecItem[itItem] == nItem)
                    return it;
            }
        }

        return 0;
    }




}



let pComposMgr = null;

module.exports = (() =>
{
    if (pComposMgr == null)
    {
        pComposMgr = new ComposMgr();
    }
    return pComposMgr;
})();

