﻿let GameRes = require('./GameRes');
let CPubFunction = require('./PubFunction');



cc.Class({
    extends: cc.Component,
    properties: {
        btnPhKind:cc.Prefab,
        btnPhSubKind:cc.Prefab,
        rowPaiHangBang:cc.Prefab,
    },

    ctor() {
        this.subKind = -1;
    },

    start() {
        this.vecBtn = [];
        this.vecFatherBtn = [];
        this.vecSonBtn = [];
        this.bQueryBy = 0;

        this.vecRow = [];
        this.keyData = [];

        cc.find('btnClose', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "PaiHangBangUI", "Close", 0));

        //---------------------

        this.CreateBtnByJson();
    },


    CreateBtnByJson() {
        let stStart = { nX: 120, nY: -40 };

        let goContent = cc.find('svButton/view/content', this.node);

        for (const it in cc.ll.propData.paihang) {
            if (it == 'datatype')
                continue;

            let stData = cc.ll.propData.paihang[it];
            let goBtn = CPubFunction.CreateSubNode(goContent, { nX: stStart.nX, nY: stStart.nY - it * 70 }, this.btnPhKind, 'btnPhKind');
            goBtn.getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "PaiHangBangUI", "ChangeKind", it));
            cc.find('Label', goBtn).getComponent(cc.Label).string = stData.Name;

            this.vecFatherBtn.push(goBtn);
        }

        this.ChangeKind(null, 0);

    },

    ChangeKind(stEvent, strKey) {
        this.vecBtn = this.vecFatherBtn.slice(0);

        let goContent = cc.find('svButton/view/content', this.node);
        let stData = cc.ll.propData.paihang[strKey];

        if (this.vecSonBtn.length > 0) {
            CPubFunction.DestroyVecNode(this.vecSonBtn);
        } else {
            let nIndex = -1;
            for (var it in stData.SubList) {
                nIndex++;

                this.keyData[nIndex] = it;
                let info = stData.SubList[it];

                let goBtn = CPubFunction.CreateSubNode(goContent, { nX: 0, nY: 0 }, this.btnPhSubKind, 'btnPhSubKind');
                goBtn.getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "PaiHangBangUI", "ChangeSubKind", nIndex));
                cc.find('Label', goBtn).getComponent(cc.Label).string = info.name;

                

                this.vecSonBtn.push(goBtn);
                this.vecBtn.splice(parseInt(strKey) + nIndex + 1, 0, goBtn);
            }
        }

        this.ReplaceBtnList(goContent);
        this.ChangeSubKind(null, 0);
    },


    ChangeSubKind(stEvent, nIndex) {
        //zfy暂时不让点击
        if (nIndex >= 4 || this.subKind == nIndex) {
            return;
        }

        this.subKind = nIndex;
        let key = this.keyData[nIndex]
        let stData = cc.ll.propData.paihang[0];
        let info = stData.SubList[key];
        this.InitTitle(info.title);

        CPubFunction.ChangeTeamButtonState(this.vecSonBtn, 'Common/ui_common', 'ui_common_public_list0', 'ui_common_public_list2', nIndex);

        this.bQueryBy = nIndex;

        cc.ll.net.send('c2s_ask_paihang', { nByWhat: this.bQueryBy });

    },

    OnReceiveVecRow(kind, vecRow) {
        CPubFunction.DestroyVecNode(this.vecRow);

        let goContent = cc.find('nodDataView/ScrollView/view/content', this.node);

        let stStart = { nX: 250, nY: -20 };
        
        for (let i = 0; i < vecRow.length; i++) {
            let info = vecRow[i];
            let goRow = CPubFunction.CreateSubNode(goContent, { nX: stStart.nX, nY: stStart.nY - i * 40 }, this.rowPaiHangBang, '');
            if(kind == 0 || kind == 1){
                goRow.getComponent(cc.Button).clickEvents.push(
                    CPubFunction.CreateEventHandler(this.node, "PaiHangBangUI", "ClickRow", info[0])
                );
            }
            
            goRow.getComponent(cc.Sprite).enabled = i % 2 == 0;

            cc.find('Index', goRow).getComponent(cc.Label).string = i + 1;
            cc.find('Name', goRow).getComponent(cc.Label).string = info[1];
            cc.find('Level', goRow).getComponent(cc.Label).string = info[2]
            cc.find('Money', goRow).getComponent(cc.Label).string = info[3];

            this.vecRow.push(goRow);
        }

        goContent.height = Math.max(goContent.height, vecRow.length * 40);
    },

    ClickRow(e, nRoleID) {
        // cc.ll.net.send('c2s_ask_other_info', { nRoleID: nRoleID });
    },

    OnReceiveOtherRoleInfo(data) { },


    ReplaceBtnList(goContent) {
        let stStart = { nX: 120, nY: -40 };

        let nLen = this.vecBtn.length;

        for (const it in this.vecBtn) {
            this.vecBtn[it].setPosition(cc.v2(stStart.nX, stStart.nY - it * 70));
        }

        goContent.height = Math.max(goContent.height, nLen * 70);

    },

    InitTitle(vecTitle) {
        for (let i = 0; i < 4; i++) {
            let strName = `nodDataView/nodTitle/${i}/Label`;

            cc.find(`nodDataView/nodTitle/${i}/Label`, this.node).getComponent(cc.Label).string = vecTitle[i];
        }

    },

    Close() {
        cc.ll.AudioMgr.playCloseAudio();
        this.node.destroy();
    },




});
