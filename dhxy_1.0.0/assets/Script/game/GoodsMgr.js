﻿let CPubFunction = require('./PubFunction');

class GoodsMgr {
    constructor() {
        // this.Init();
    }

    Init() {
        cc.loader.loadRes(`Prefabs/WorkShopPanel`, cc.Prefab, (err, prefab) => {
            this.WorkShopPanel = prefab;
        });

        cc.loader.loadRes(`Prefabs/LotteryUI`, cc.Prefab, (err, prefab) => {
            this.LotteryUI = prefab;
        });
    }

    getItem(itemid) {
        let itemInfo = cc.ll.propData.item[itemid];
        if (itemInfo == null) {
            return null;
        }
        let count = cc.ll.player.itemList[itemid];
        if (count == null) {
            count = 0;
        }
        itemInfo.count = count;
        return itemInfo;
    }

    useItem(itemid, count = 1, operateid = 0) {
        let itemInfo = this.getItem(itemid);
        if (itemInfo.id >= 30001 && itemInfo.id <= 30030) {
            let workshop = cc.instantiate(this.WorkShopPanel);
            workshop.parent = cc.find('Canvas');
            workshop.getComponent('WorkShopPanel').setCurPanel(3);

            let bagLayer = cc.find('Canvas/MainUI/BagPanel');
            if (bagLayer != null) {
                bagLayer.destroy();
            }
            return;
        }
        if (itemInfo == null || (itemInfo.count <= 0 && itemInfo.effect == 1)) {
            return;
        }

        if (itemid == 50004) {
            let goMainUI = cc.find('Canvas/MainUI');
            CPubFunction.FindAndDeleteNode(goMainUI, 'LotteryUI');
            let goLotteryUI = CPubFunction.CreateSubNode(goMainUI, {
                nX: 0,
                nY: 0
            }, this.LotteryUI, 'LotteryUI');
        }

        if (CPubFunction.IsDataInVecter(itemid, [50001, 50002, 50003])) //藏宝图类型
        {
            let stTo = JSON.parse(itemInfo.json);

            CPubFunction.MainPlayerToDo(stTo.map, stTo.x, stTo.y, () => {
                CPubFunction.CreateWaitTip('挖宝', 1, () => {
                    cc.ll.net.send('c2s_use_bagitem', {
                        roleid: cc.ll.player.roleid,
                        itemid: itemid,
                        count: count,
                        operateid: operateid
                    });
                });

            });
            return;
        }

        cc.ll.net.send('c2s_use_bagitem', {
            roleid: cc.ll.player.roleid,
            itemid: itemid,
            count: count,
            operateid: operateid
        });
    }

    subItem(itemid) {
        let itemInfo = this.getItem(itemid);
        if (itemInfo == null || itemInfo.count <= 0) {
            return;
        }
        cc.ll.net.send('c2s_update_bagitem', {
            roleid: cc.ll.player.roleid,
            itemid: itemid,
            count: 1,
            operation: 0
        });
    }

    addItem(itemid) {
        let itemInfo = this.getItem(itemid);
        if (itemInfo == null) {
            return;
        }
        cc.ll.net.send('c2s_update_bagitem', {
            roleid: cc.ll.player.roleid,
            itemid: itemid,
            count: 1,
            operation: 1
        });
    }
}

let goodsmgr = null;
module.exports = (() => {
    if (goodsmgr == null) {
        goodsmgr = new GoodsMgr();
    }
    return goodsmgr;
})();