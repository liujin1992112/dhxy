let GameRes = require('./GameRes');
let GameDefine = require('./GameDefine');
let CNpcMgr = require('./NpcMgr');

/**
 * 游戏地图逻辑
 * 说明:挂在MainScene下的MapUI节点下,MapUI的锚点为(0,0),原点位置在左下角(-512,-288)或者地图层的位置在（-width/2, -height/2）,屏幕中心点为(0,0)
 */
cc.Class({
    extends: cc.Component,
    properties: {
        /** 角色结点 */
        roleNode: cc.Node,
        homePre: cc.Prefab,

        /** 传送点特效预制体 */
        transferPre: cc.Prefab,

        /**触摸地图特效预制体 */
        touchPre: cc.Prefab,
        rolepre: cc.Prefab,
    },

    ctor() {
        //标记地图是否加载完成
        this.loaded = false;

        //地图的宽度和高度
        this.MAP_RES_WIDTH = 5258;
        this.MAP_RES_HEIGHT = 2910;

        //地图格子的宽度和高度
        this.gridWidth = 20;
        this.gridHeight = 20;

        //地图的大小(每一个地图图块的大小为512*512)
        this.mapNodeSize = 512;

        //计算用于寻路的地图格子的行数和列数
        this.rowCount = Math.ceil(this.MAP_RES_HEIGHT / this.gridHeight);
        this.lineCount = Math.ceil(this.MAP_RES_WIDTH / this.gridWidth);
        this.borderCount = 1;//屏幕外的图片显示宽度
        this.transferList = [];//传送门列表

        //二维数组,存放地图的行走信息
        this.mapNodeArr = [];
        this.mapId = 0;//地图ID
        this.mapres = 0;

        this.lastpos = {
            x: 0,
            y: 0
        };
    },

    onLoad() {
        this.canvasWidht = cc.find('Canvas').width;
        this.canvasHeight = cc.find('Canvas').height;
        // this.canvasWidht = 1024;
        // this.canvasHeight = 576;
        this.gamelogic = (cc.find('Canvas')).getComponent('GameLogic');
    },

    start() {
        this.roleNode.zIndex = 3;
        this.selfHeroLogic = this.roleNode.getComponent('role');
        this.selfHeroLogic.setInfo(cc.ll.player, 1);
        cc.ll.player.setNode(this.roleNode);
        this.loadMap(false);
        if (!this.loaded) {
            this.loaded = true;
            this.gamelogic.loadComplete();
        }
    },

    /**
     * 切换地图
     * @param {*} mapid 
     * @param {*} pos 
     * @param {*} issend 是否通知服务器切换地图
     * @returns 
     */
    changeMap(mapid, pos, issend = true) {
        // if (cc.ll.player.shane > 0 && mapid == 1011) {
        //     if (cc.ll.player.teamid > 0) {   
        //         cc.ll.net.send('c2s_leave_team', {
        //             roleid: cc.ll.player.roleid,
        //             teamid: cc.ll.player.teamid
        //         });
        //         cc.ll.player.teamid = 0;     
        //     }
        //     this.changeMap(1201, cc.v2(59, 4));
        //     return;
        // }

        if (cc.ll.player.teamid > 0 && !cc.ll.player.isleader && issend) {
            return;
        }
        if (cc.ll.player.mapid == mapid) {
            return;
        }
        this.selfHeroLogic.actlist = [];
        this.roleNode.stopAllActions();
        this.selfHeroLogic.living_state = 0;

        cc.ll.player.mapid = mapid;
        // this.unschedule(this.checkRolePos);
        cc.sys.garbageCollect();

        this.loadMap(true, pos);
        if (issend) {
            //通知服务器切换地图
            cc.ll.net.send('c2s_change_map', {
                accountid: cc.ll.player.accountid,
                mapid: mapid,
                x: this.selfHeroLogic.netInfo.x,
                y: this.selfHeroLogic.netInfo.y
            });
        }
    },

    autoPatrol() {
        /* if (cc.ll.player.teamid > 0) {
            return;
        } */
        this.gamelogic.autoPatrolNode.active = true;

        let startX = Math.round((this.roleNode.getPosition().x - this.gridWidth / 2) / this.gridWidth);
        let startY = Math.round((this.roleNode.getPosition().y - this.gridHeight / 2) / this.gridHeight);
        let endX = Math.round((Math.floor(Math.random() * this.MAP_RES_WIDTH) - this.gridWidth / 2) / this.gridWidth);
        let endY = Math.round((Math.floor(Math.random() * this.MAP_RES_HEIGHT) - this.gridHeight / 2) / this.gridHeight);
        let result = this.searchMovePath(startX, startY, endX, endY);

        this.selfHeroLogic.actlist = [];
        for (const act of result) {
            this.selfHeroLogic.actlist.push({
                type: 2,
                info: act
            });
        }
    },

    RoleStop() {
        if (cc.ll.player.teamid > 0 && !cc.ll.player.isleader) {
            return;
        }
        if (this.gamelogic.autoPatrolNode.active) {
            this.autoPatrol();
            return;
        }
        for (const transfer of this.transferList) {
            if (cc.ll.pDistance(this.roleNode.getPosition(), transfer.getPosition()) < 90) {
                let toinfo = cc.ll.propData.transfer[transfer.__info_id__];
                this.changeMap(toinfo.tomap, cc.v2(toinfo.tox, toinfo.toy));
            }
        }
    },

    /**
     * 加载地图
     * @param {*} change 
     * @param {*} pos 
     */
    loadMap(change, pos) {
        // this.gamelogic.autoPatrolNode.active = true;

        /** 引妖香 面板 */
        if (this.gamelogic.node.getChildByName('YinYaoXiangPanel')) {
            this.gamelogic.node.getChildByName('YinYaoXiangPanel').getComponent('YinYaoXiangPanel').closePanel();
        }

        if (this.mapId != 0) {
            //说明已经在地图场景中
            for (let yTileNum = 0; yTileNum < this.mapNodeArr.length; yTileNum++) {
                let list = this.mapNodeArr[yTileNum];
                for (let xTileNum = 0; xTileNum < list.length; xTileNum++) {
                    if (list[xTileNum] == 1) {
                        this.mapNodeArr[yTileNum][xTileNum] = 0;
                    }

                    //销毁512*512的地图块
                    let amapnode = this.node.getChildByName(this.mapres + '_' + yTileNum + '_' + xTileNum);
                    if (amapnode) {
                        amapnode.destroy();//cocoscreator强化内存管理,destroy时引用计数为0,自动释放资源
                    }
                }
            }
        }
        this.selfHeroLogic.movePath = [];//重置玩家自己移动路径
        this.gamelogic.clearPlayerObjs();//清除地图上的玩家对象
        CNpcMgr.ReloadNpcJson();//重新加载Npc的配置

        // cc.loader.releaseResDir(`Map/jpg/${this.mapres}`, cc.Texture2D);
        // cc.loader.releaseResDir(`Map/jpg/${this.mapres}`, cc.SpriteFrame);
        // this.node.destroyAllChildren();
        // gamelogic.playerObjs = {};
        this.mapId = cc.ll.player.mapid;//玩家自己所在的地图ID
        this.mapres = cc.ll.propData.mapdata[this.mapId].mapid;//玩家自己所在的地图资源ID
        this.gamelogic.uiLogic.setMapName(cc.ll.propData.mapdata[this.mapId].map_name);//设置玩家所在地图名称

        //读取地图资源的宽度和高度
        this.MAP_RES_WIDTH = cc.ll.propData.mapdata[this.mapId].width;
        this.MAP_RES_HEIGHT = cc.ll.propData.mapdata[this.mapId].height;

        //计算用于寻路的地图格子的行数和列数
        this.rowCount = Math.ceil(this.MAP_RES_HEIGHT / this.gridHeight);
        this.lineCount = Math.ceil(this.MAP_RES_WIDTH / this.gridWidth);

        this.selfHeroLogic.gridHeight = this.gridHeight;
        this.selfHeroLogic.gridWidth = this.gridWidth;
        this.selfHeroLogic.canvasHeight = this.canvasHeight;
        this.selfHeroLogic.canvasWidht = this.canvasWidht;
        this.selfHeroLogic.onlyid = cc.ll.player.onlyid;
        this.selfHeroLogic.name = cc.ll.player.name;

        this.mapNodeArr = []; //先声明一维
        //大地图切格的小图块的大小为512*512,计算小图块的行数和列数
        let maprow = Math.ceil(this.MAP_RES_HEIGHT / this.mapNodeSize);
        let mapline = Math.ceil(this.MAP_RES_WIDTH / this.mapNodeSize);
        for (var i = 0; i < maprow; i++) { //一维长度为20
            this.mapNodeArr[i] = new Array(mapline); //在声明二维
            for (var j = 0; j < mapline; j++) { //二维长度为20
                this.mapNodeArr[i][j] = 0;
            }
        }

        //获取Canvas的宽和高
        this.width = cc.find('Canvas').width;
        this.height = cc.find('Canvas').height;
        this.mapScale = 0.8;//地图缩放比例
        this.node.scale = this.mapScale;

        this.npcInfo = cc.ll.propData.map[this.mapres].npcInfo;
        this.gridInfoArr = cc.ll.propData.map[this.mapres].mapInfo;//地图寻路网格信息
        this.selfHeroLogic.gridInfoArr = this.gridInfoArr;//地图寻路网格信息
        this.selfHeroLogic.actlist = [];
        this.gamelogic.autoNode.active = false;
        this.gamelogic.autoPatrolNode.active = false;
        this.gamelogic.mainMap.getComponent('MainMapLogic').removePathNodes();

        this.startPos = cc.ll.propData.map[this.mapres].startPos;//地图的起始位置
        this.assets = GameRes.mapResList[this.mapres];

        // this.node.x = -this.width / 2;
        // this.node.y = -this.height / 2;
        if (change) {
            if (!pos) {
                this.selfHeroLogic.setObjPos(this.startPos.x, this.startPos.y);
                // this.setTeamPos(this.startPos.x, this.startPos.y);
            } else {
                this.selfHeroLogic.setObjPos(pos.x, pos.y);
                // this.setTeamPos(pos.x, pos.y);
            }
        }
        // if (change) {
        //     cc.ll.net.send('c2s_aoi_move', { accountid: cc.ll.player.accountid, x: this.selfHeroLogic.netInfo.x, y: this.selfHeroLogic.netInfo.y });
        // }

        let rolePos = this.node.convertToWorldSpaceAR(this.roleNode.getPosition());
        this.updateMap(cc.v2(this.width / 2 - rolePos.x, this.height / 2 - rolePos.y));
        // this.scheduleOnce(() => {
        // // this.schedule(this.checkRolePos, 0.5, cc.macro.REPEAT_FOREVER);
        //     this.complete = true;
        // }, 3);

        //销毁传送点
        for (const transfer of this.transferList) {
            transfer.destroy();
        }
        this.transferList = [];

        for (const key in cc.ll.propData.transfer) {
            const info = cc.ll.propData.transfer[key];
            if (info.mapid == this.mapId) {
                let transfer = cc.instantiate(this.transferPre);
                transfer.x = info.mapx * 20;
                transfer.y = info.mapy * 20;
                // transfer.tag = info.id;
                transfer.__info_id__ = info.id;
                transfer.zIndex = 1;
                transfer.parent = this.node;
                this.transferList.push(transfer);
            }
        }
        if (this.mapId == 4001) {
            let ui = cc.instantiate(this.homePre);
            ui.parent = this.node;
            ui.name = 'homeUI';
            ui.zIndex = 1;
        } else if (this.node.getChildByName('homeUI') != null) {
            this.node.getChildByName('homeUI').destroy();
        }

        let audio = GameDefine.getMapAudio(this.mapId);
        cc.ll.AudioMgr.playBgm(audio);
    },

    /**
     * 刷新地图
     * @param {*} deltaPos 地图的偏移量
     * @returns 
     */
    updateMap(deltaPos) {
        this.node.x += deltaPos.x;
        this.node.y += deltaPos.y;

        //地图层的位置在（-width/2, -height/2）
        let maxX = -this.width / 2;
        let maxY = -this.height / 2;
        if (this.node.x > maxX) {
            this.node.x = maxX;
        }
        if (this.node.y > maxY) {
            this.node.y = maxY;
        }
        let minh = -(this.MAP_RES_HEIGHT * this.mapScale - this.height) + maxY;
        let minw = -(this.MAP_RES_WIDTH * this.mapScale - this.width) + maxX;

        if (this.node.y < minh) {
            this.node.y = minh;
        }
        if (this.node.x < minw) {
            this.node.x = minw;
        }

        //地图x和y轴的偏移量
        let tx = Math.abs(this.lastpos.x - this.node.x);
        let ty = Math.abs(this.lastpos.y - this.node.y);
        if (tx < 100 && ty < 100) {
            return;
        }

        this.lastpos = this.node.position;

        //当前MapUI节点大小
        let currentNodeSize = this.mapNodeSize * this.mapScale;
        let startx = this.node.x + currentNodeSize * this.borderCount - maxX;
        if (startx >= 0) {
            startx = 0;
        }
        let endx = this.node.x - currentNodeSize * this.borderCount - maxX - this.width;
        if (endx < minw - this.width / 2) {
            endx = minw - this.width / 2;
        }

        let starty = this.node.y + currentNodeSize * this.borderCount - maxY;
        if (starty >= 0) {
            starty = 0;
        }

        let endy = this.node.y - maxY - this.height;
        if (endy < minh - this.height / 2) {
            endy = minh - this.height / 2;
        }
        let begin_w_n = Math.floor((Math.abs(startx) / currentNodeSize));
        let end_w_n = Math.ceil((Math.abs(endx) / currentNodeSize));

        let begin_h_n = Math.floor((Math.abs(starty) / currentNodeSize));
        let end_h_n = Math.ceil((Math.abs(endy) / currentNodeSize));

        for (let yTileNum = 0; yTileNum < this.mapNodeArr.length; yTileNum++) {
            let list = this.mapNodeArr[yTileNum];
            for (let xTileNum = 0; xTileNum < list.length; xTileNum++) {
                const hadadd = list[xTileNum];
                if (xTileNum >= begin_w_n && xTileNum <= end_w_n && yTileNum >= begin_h_n && yTileNum <= end_h_n) {
                    if (hadadd == 0) {
                        let mapNode = new cc.Node();
                        let mapframe = mapNode.addComponent(cc.Sprite);
                        let self = this;
                        // console.log('path=',`Map/jpg/${this.mapres}/${this.mapres}_${yTileNum}_${xTileNum}`);
                        cc.loader.loadRes(`Map/jpg/${this.mapres}/${this.mapres}_${yTileNum}_${xTileNum}`, cc.SpriteFrame, function (err, frame) {
                            mapframe.spriteFrame = frame;
                            mapNode.parent = self.node;
                            mapNode.name = self.mapres + '_' + yTileNum + '_' + xTileNum;
                            mapNode.setAnchorPoint(cc.v2(0, 0));
                            mapNode.setPosition(xTileNum * self.mapNodeSize, yTileNum * self.mapNodeSize);
                            self.mapNodeArr[yTileNum][xTileNum] = 1;
                        });
                    }
                } else {
                    if (hadadd == 1) {
                        this.node.getChildByName(this.mapres + '_' + yTileNum + '_' + xTileNum).destroy();
                        cc.loader.releaseRes(`Map/jpg/${this.mapres}/${this.mapres}_${yTileNum}_${xTileNum}`, cc.Texture2D);
                        cc.loader.releaseRes(`Map/jpg/${this.mapres}/${this.mapres}_${yTileNum}_${xTileNum}`, cc.SpriteFrame);
                        this.mapNodeArr[yTileNum][xTileNum] = 0;
                    }
                }
            }
        }
    },

    /**
     * 定时根据角色的位置,动态更新地图的位置
     */
    update() {
        if (this.loaded) {
            this.checkRolePos();
        }
    },

    checkRolePos() {
        let rolePos = this.node.convertToWorldSpaceAR(this.roleNode.getPosition());
        let distance = cc.ll.pDistance(cc.v2(this.width / 2, this.height / 2), rolePos);
        if (distance > 5) {
            let deltaPos = cc.v2(0, 0);
            if (rolePos.x - this.width / 2 > 0 || rolePos.x - this.width / 2 < 0) {
                // deltaPos.x = (this.width / 2 - rolePos.x) / 10;
                let direction = rolePos.x > this.width / 2 ? -1 : 1;
                deltaPos.x = direction * Math.sqrt(Math.abs(this.width / 2 - rolePos.x)) / 2;
            }
            if (rolePos.y - this.height / 2 > 0 || rolePos.y - this.height / 2 < 0) {
                // deltaPos.y = (this.height / 2 - rolePos.y) / 10;
                let direction = rolePos.y > this.height / 2 ? -1 : 1;
                deltaPos.y = direction * Math.sqrt(Math.abs(this.height / 2 - rolePos.y)) / 2;
            }
            this.updateMap(deltaPos);
        }
    },

    /**
     * 触摸地图
     * @param {*} drawPos 触摸点,MapUI下的节点坐标
     * @returns 
     */
    touchPos(drawPos) {
        //玩家处于组队状态下,但是不是队长,无控制权
        if (cc.ll.player.teamid > 0 && !cc.ll.player.isleader) {
            return [];
        }
        this.cleanTeamPath();
        //计算主角在地图上的格子坐标,作为寻路的起始点坐标
        let startX = Math.round((this.roleNode.getPosition().x - this.gridWidth / 2) / this.gridWidth);
        let startY = Math.round((this.roleNode.getPosition().y - this.gridHeight / 2) / this.gridHeight);

        //计算触摸点在地图上的格子坐标,作为寻路的结束点坐标
        let endX = Math.round((drawPos.x - this.gridWidth / 2) / this.gridWidth);
        let endY = Math.round((drawPos.y - this.gridHeight / 2) / this.gridHeight);

        //主角进行寻路
        let result = this.searchMovePath(startX, startY, endX, endY);
        this.selfHeroLogic.actlist = [];
        this.roleNode.stopAllActions();
        if (result.length > 0) {
            for (const act of result) {
                this.selfHeroLogic.actlist.push({
                    type: 2,
                    info: act
                });
            }
            this.selfHeroLogic.living_state = 0;
        }
        //调用living_thing的objUpdate方法更新对象
        this.selfHeroLogic.objUpdate();
        this.gamelogic.autoNode.active = false;
        this.gamelogic.autoPatrolNode.active = false;
        this.gamelogic.mainMap.getComponent('MainMapLogic').removePathNodes();
        this.showTouchEffect(drawPos);
        return result;
    },

    cleanTeamPath() {
        if (cc.ll.player.teamid == 0 || !cc.ll.player.isleader) {
            return;
        }
        if (!cc.ll.player.teamInfo.objlist || !Array.isArray(cc.ll.player.teamInfo.objlist)) {
            return;
        }

        for (let index = 1; index < cc.ll.player.teamInfo.objlist.length; index++) {
            let obj = cc.ll.player.teamInfo.objlist[index];
            if (obj.livingtype != 1) {
                break;
            }
            let p = this.gamelogic.GetPlayerObjs(obj.onlyid);
            if (p) {
                let plogic = p.getComponent('role');
                plogic.actlist = [];
                p.stopAllActions();
                plogic.objUpdate();
            }
        }
    },

    setTeamPos(x, y) {
        if (cc.ll.player.teamid == 0 || !cc.ll.player.isleader) {
            return;
        }
        if (!cc.ll.player.teamInfo.objlist || !Array.isArray(cc.ll.player.teamInfo.objlist)) {
            return;
        }

        for (let index = 1; index < cc.ll.player.teamInfo.objlist.length; index++) {
            let obj = cc.ll.player.teamInfo.objlist[index];
            if (obj.livingtype != 1) {
                break;
            }
            let p = this.gamelogic.GetPlayerObjs(obj.onlyid);
            if (p) {
                let plogic = p.getComponent('role');
                plogic.setObjPos(x, y);
            }
        }
    },

    /**
     * 设置队伍路径
     * @returns 
     */
    setTeamPath() {
        if (cc.ll.player.teamid == 0 || !cc.ll.player.isleader) {
            return;
        }
        if (!cc.ll.player.teamInfo.objlist || !Array.isArray(cc.ll.player.teamInfo.objlist)) {
            return;
        }
        let path = this.selfHeroLogic.movePath;
        if (path.length < 5) {
            return;
        }
        if (path.length > 30) {
            path.splice(0, path.length - 30);
        }

        // this.addTeamPlayerToMap();
        for (let index = 1; index < cc.ll.player.teamInfo.objlist.length; index++) {
            let obj = cc.ll.player.teamInfo.objlist[index];
            if (obj.livingtype != 1) {
                break;
            }
            let p = this.gamelogic.GetPlayerObjs(obj.onlyid);
            if (p) {
                let plogic = p.getComponent('role');
                if (path.length > index * 5) {
                    let pos = path[path.length - 5 * index - 1];

                    plogic.actlist.push({
                        type: 2,
                        info: pos
                    });
                    plogic.objUpdate();
                }
            }
        }
    },

    addTeamPlayerToMap() {
        if (cc.ll.player.teamid == 0 || !cc.ll.player.isleader) {
            return;
        }
        for (let index = 1; index < cc.ll.player.teamInfo.objlist.length; index++) {
            let obj = cc.ll.player.teamInfo.objlist[index];
            if (obj.livingtype != 1) {
                break;
            }
            if (this.gamelogic.playerObjs[obj.onlyid] == null) {
                let curRole = cc.instantiate(this.rolepre);
                curRole.parent = this.node;
                curRole.zIndex = 3;
                curRole.livingtype = obj.livingtype;
                let comRole = curRole.getComponent('role');
                comRole.setObjPos(this.selfHeroLogic.netInfo.x, this.selfHeroLogic.netInfo.y);
                comRole.setInfo(obj);
                comRole.onlyid = obj.onlyid;
                this.gamelogic.playerObjs[obj.onlyid] = curRole;
            }
        }
    },

    /**
     * 显示触摸效果
     * @param {*} pos 
     */
    showTouchEffect(pos) {
        let teffect = cc.instantiate(this.touchPre);
        teffect.parent = this.node;
        teffect.x = pos.x;
        teffect.y = pos.y;
        teffect.runAction(cc.sequence(cc.delayTime(3), cc.removeSelf()));
    },

    /**
     * 搜索路径
     * @param {*} startX 
     * @param {*} startY 
     * @param {*} endX 
     * @param {*} endY 
     * @returns 
     */
    searchMovePath(startX, startY, endX, endY) {
        let availableStart = this.getAvailabelPoint(startY, startX, this.gridInfoArr, this.rowCount, this.lineCount);
        let availableEnd = this.getAvailabelPoint(endY, endX, this.gridInfoArr, this.rowCount, this.lineCount);
        let result = this.searchRoad(availableStart.r, availableStart.l, availableEnd.r, availableEnd.l, this.gridInfoArr, this.rowCount, this.lineCount);
        return result;
    },

    /**
     * 对位置寻路点位置进行预处理
     * @param {*} r      行
     * @param {*} l      列
     * @param {*} mapArr 寻路信息
     * @param {*} rows   用于寻路的地图格子的行数和列数
     * @param {*} lines  用于寻路的地图格子的行数和列数
     * @returns 返回可以行走点的行和列{r,l}
     */
    getAvailabelPoint(r, l, mapArr, rows, lines) {
        //对触摸点的进行地图边界处理
        if (r < 0) {
            r = 0;
        }
        if (l < 0) {
            l = 0;
        }
        if (r >= rows) {
            r = rows - 1;
        }
        if (l >= lines) {
            l = lines - 1;
        }

        if (mapArr[r][l] != 0) {
            return {
                r: r,
                l: l
            };
        }

        //8方向寻找触摸点周围可以到达的点    count:可以理解为以为中心点往外查找第几圈
        let count = 1;
        while (true) {
            if (count > lines && count > rows) {
                return {
                    r: -1,
                    l: -1
                };
            }
            if (r + count < rows && mapArr[r + count][l] != 0) {
                return {
                    r: r + count,
                    l: l
                };
            }
            if (l + count < lines && mapArr[r][l + count] != 0) {
                return {
                    r: r,
                    l: l + count
                };
            }
            if (r >= count && mapArr[r - count][l] != 0) {
                return {
                    r: r - count,
                    l: l
                };
            }
            if (l >= count && mapArr[r][l - count] != 0) {
                return {
                    r: r,
                    l: l - count
                };
            }
            if (r + count < rows && l + count < lines && mapArr[r + count][l + count] != 0) {
                return {
                    r: r + count,
                    l: l + count
                };
            }
            if (r >= count && l >= count && mapArr[r - count][l - count] != 0) {
                return {
                    r: r - count,
                    l: l - count
                };
            }
            if (r >= count && l + count < lines && mapArr[r - count][l + count] != 0) {
                return {
                    r: r - count,
                    l: l + count
                };
            }
            if (l >= count && r + count < rows && mapArr[r + count][l - count] != 0) {
                return {
                    r: r + count,
                    l: l - count
                };
            }
            count++;
        }
    },

    /**
     * 小顶堆上升算法
     * list 小顶堆列表
     * pos 起始计算位置，即从改点开始上升
     * indexlist 地图节点索引，节点地图坐标对应list中的位置
     * cols 地图列数
     */
    minheap_filterup(list, pos, indexlist, cols) {
        let c = pos; // 当前节点(current)的位置
        let p = Math.floor((c - 1) / 2); // 父(parent)结点的位置 
        let tmp = list[c]; // 当前节点(current)
        while (c > 0) { // c>0 还未上升到顶部
            if (list[p].F <= tmp.F) // 父节点比当前节点小，上升结束
                break;
            else {
                list[c] = list[p]; // 父节点放到当前位置
                indexlist[list[p].r * cols + list[p].l] = c; //设置父节点的索引位置
                c = p; // 当前位置上升到父节点位置
                p = Math.floor((p - 1) / 2); // 重新计算父节点位置
            }
        }
        list[c] = tmp; // 把传入节点放到上升位置
        indexlist[tmp.r * cols + tmp.l] = c; // 设置传入点的索引位置
    },

    /**
     * 小顶堆下沉算法
     * list 小顶堆列表
     * pos 起始计算位置，即从该点开始上升
     * indexlist 地图节点索引，节点地图坐标对应list中的位置
     * cols 地图列数
     */
    minheap_filterdown(list, pos, indexlist, cols) {
        let c = pos; // 当前(current)节点的位置
        let l = 2 * c + 1; // 左(left)孩子的位置
        let tmp = list[c]; // 当前(current)节点
        let end = list.length - 1; // 数组终点
        while (l <= end) {
            if (l < end && list[l].F > list[l + 1].F) // "l"是左孩子，"l+1"是右孩子
                l++; // 左右两孩子中选择较小者，即list[l+1]
            if (tmp.F <= list[l].F) // 当前节点比最小的子节点小，调整结束
                break;
            else {
                list[c] = list[l];
                indexlist[list[l].r * cols + list[l].l] = c;
                c = l;
                l = 2 * l + 1;
            }
        }
        list[c] = tmp;
        indexlist[tmp.r * cols + tmp.l] = c;
    },

    existInCloseList(point, list, cols) {
        if (list[point.r * cols + point.l]) return true;
        return false;
    },

    existInOpenList(point, list, cols) {
        if (list[point.r * cols + point.l]) return list[point.r * cols + point.l];
        return false;
    },

    //其中的MAP.arr是二维数组
    /**
     * A星寻路
     * @param {*} start_r   起始点的行
     * @param {*} start_l   起始点的列
     * @param {*} end_r     目标点的行   
     * @param {*} end_l     目标点的列
     * @param {*} mapArr    寻路数据
     * @param {*} rows      用于寻路的地图格子的行数和列数
     * @param {*} cols      用于寻路的地图格子的行数和列数
     * @returns 
     */
    searchRoad(start_r, start_l, end_r, end_l, mapArr, rows, cols) {
        if (end_r == -1 && end_l == -1) {
            return [];
        }

        var openList = [], //开启列表
            closeObjList = {}, //关闭列表索引
            openObjList = {}, //开启列表索引
            result = [], //结果数组
            result_index = 0; //终点位置

        if (start_r == end_r && start_l == end_l) {
            return result;
        }

        openList.push({
            r: start_r,
            l: start_l,
            G: 0
        }); //把当前点加入到开启列表中，并且G是0
        //数组索引=行号*列数+列号
        openObjList[start_r * cols + start_l] = start_r * cols + start_l;

        do {
            var currentPoint = openList[0];
            if (openList.length > 1) {
                openList[0] = openList[openList.length - 1];
                this.minheap_filterdown(openList, 0, openObjList, cols);
            }
            openList.splice(openList.length - 1, 1);
            closeObjList[currentPoint.r * cols + currentPoint.l] = currentPoint;
            delete openObjList[currentPoint.r * cols + currentPoint.l];

            var surroundPoint = this.SurroundPoint(currentPoint);
            for (var i in surroundPoint) {
                var item = surroundPoint[i]; //获得周围的八个点
                if (
                    item.r >= 0 && //判断是否在地图上
                    item.l >= 0 &&
                    item.r < rows &&
                    item.l < cols &&
                    mapArr[item.r][item.l] != 0 && //判断是否是障碍物
                    !this.existInCloseList(item, closeObjList, cols) && //判断是否在关闭列表中
                    mapArr[item.r][currentPoint.l] != 0 && //判断之间是否有障碍物，如果有障碍物是过不去的
                    mapArr[currentPoint.r][item.l] != 0) {
                    //g 到父节点的位置
                    //如果是上下左右位置的则g等于10，斜对角的就是14
                    var g = currentPoint.G + ((currentPoint.r - item.r) * (currentPoint.l - item.l) == 0 ? 10 : 14);
                    if (!this.existInOpenList(item, openObjList, cols)) { //如果不在开启列表中
                        //计算H，通过水平和垂直距离进行确定
                        item['H'] = Math.abs(end_r - item.r) * 10 + Math.abs(end_l - item.l) * 10;
                        item['G'] = g;
                        item['F'] = item.H + item.G;
                        item['parent'] = currentPoint;
                        openList.push(item);
                        openObjList[item.r * cols + item.l] = openList.length - 1;
                        if (item['H'] == 0) {
                            break;
                        }
                        this.minheap_filterup(openList, openList.length - 1, openObjList, cols);
                    } else { //存在在开启列表中，比较目前的g值和之前的g的大小
                        var index = this.existInOpenList(item, openObjList, cols);
                        //如果当前点的g更小
                        if (g < openList[index].G) {
                            openList[index].parent = currentPoint;
                            openList[index].G = g;
                            openList[index].F = g + openList[index].H;
                        }
                        this.minheap_filterup(openList, index, openObjList, cols);
                    }
                }
            }
            //如果开启列表空了，没有通路，结果为空
            if (openList.length == 0) {
                break;
            }
            // openList.sort(this.sortF);//这一步是为了循环回去的时候，找出 F 值最小的, 将它从 "开启列表" 中移掉
        } while (!(result_index = this.existInOpenList({
            r: end_r,
            l: end_l
        }, openObjList, cols)));

        //判断结果列表是否为空
        if (!result_index) {
            result = [];
        } else {
            var currentObj = openList[result_index];
            do {
                //把路劲节点添加到result当中
                result.unshift({
                    r: currentObj.r,
                    l: currentObj.l
                });
                currentObj = currentObj.parent;
            } while (currentObj.r != start_r || currentObj.l != start_l);
        }
        let alpha = 0.8;
        let beta = 0.2;
        let p = result.slice(0); //this.deepClone(result);
        for (let i = 1; i <= 8; i++) {
            for (let k = 2; k < p.length - 1; k++) {
                let t = p[k];
                let l = p[k - 1];
                let n = p[k + 1];
                t.l = t.l + alpha * (result[k].l - t.l) + beta * (l.l - 2 * t.l + n.l);
                t.r = t.r + alpha * (result[k].r - t.r) + beta * (l.r - 2 * t.r + n.r);
            }
        }
        return p;
        // return result;
    },

    //用F值对数组排序
    sortF(a, b) {
        return b.F - a.F;
    },

    //获取周围八个点的值
    SurroundPoint(curPoint) {
        var r = curPoint.r,
            l = curPoint.l;
        return [{
            r: r - 1,
            l: l - 1
        },
        {
            r: r,
            l: l - 1
        },
        {
            r: r + 1,
            l: l - 1
        },
        {
            r: r + 1,
            l: l
        },
        {
            r: r + 1,
            l: l + 1
        },
        {
            r: r,
            l: l + 1
        },
        {
            r: r - 1,
            l: l + 1
        },
        {
            r: r - 1,
            l: l
        }]
    },

    //判断点是否存在在列表中，是的话返回的是序列号
    existList(point, list) {
        for (var i in list) {
            if (point.r == list[i].r && point.l == list[i].l) {
                return i;
            }
        }
        return false;
    },
});