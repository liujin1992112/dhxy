cc.Class({
	extends: cc.Component,
	properties: {
		level_item: cc.Prefab,
		content: cc.Node,
	},

	start () {
		let a = cc.ll;
		let prop_level = cc.ll.propData.level;
		// for (let item = prop_level[level]; item; item = prop_level[level]) {
		for (let level = 10; level <= 200; level += 10) {
			let item = prop_level[level];
			if (item) {
				let node = cc.instantiate(this.level_item);
				node.parent = this.content;
				let logic = node.getComponent('LevelRewardItem');
				logic.init(item);
			}
		}
	},

	onButtonClick (event, param) {
		if (param == 'close') {
			cc.ll.AudioMgr.playCloseAudio();
			this.node.destroy();
		}
	},
});
