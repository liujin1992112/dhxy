let GameRes = require('./GameRes');
let CMainPlayerInfo = require('./MainPlayerInfo');
let GameDefine = require('./GameDefine');
let CNpcMgr = require('./NpcMgr');
let CPubFunction = require('./PubFunction');
let pScreenNoticeMgr = require('./ScreenNotice');

/**
 * 游戏逻辑脚本,挂载到Canvas上
 */
cc.Class({
	extends: cc.Component,
	properties: {
		/** MapUI(地图UI:显示角色、点击特效) */
		mapLayer: cc.Node,

		/** 总览地图界面预制体 */
		mainMapPre: cc.Prefab,

		/** MainUI(主场景的UI) */
		mainUI: cc.Node,

		/** 战斗关卡预制体 */
		battleStagePre: cc.Prefab,

		/** 战斗关卡UI部分预制体 */
		battleUIPre: cc.Prefab,

		/** 角色预制体 */
		rolepre: cc.Prefab,

		/** 宠物选择预制体 */
		petSelectedPre: cc.Prefab,

		/** `自动寻路中..` 节点 */
		autoNode: cc.Node,
		autoSearchNodes: [cc.Node],

		/** `自动逻辑中..` 节点 */
		autoPatrolNode: cc.Node,
		autoPatrolNodes: [cc.Node],

		/** 引妖香 面板 */
		YinYaoXiangPanel: cc.Prefab,
	},

	onLoad() {
		pScreenNoticeMgr.Reset();

		this.node.on(cc.Node.EventType.TOUCH_START, this.touchBegan.bind(this));
		this.node.on(cc.Node.EventType.TOUCH_MOVE, this.touchMoved.bind(this));
		this.node.on(cc.Node.EventType.TOUCH_END, this.touchEnded.bind(this));

		this.mapLogic = this.mapLayer.getComponent('GameMapLogic');//游戏地图逻辑脚本对象
		this.uiLogic = this.mainUI.getComponent('MainUI');//主场景的UI

		this.playerObjs = {};
		this.startAutoSearchAni();
		this.startAutoPatrolAni();

		//初始化地图总览界面,默认为隐藏状态
		this.mainMap = cc.instantiate(this.mainMapPre);
		this.mainMap.parent = this.node;
		this.mainMap.active = false;

		this.autoNode.active = false;
		this.autoPatrolNode.active = false;
		// cc.ll.net.send('c2s_get_petlist', {
		//     roleid: cc.ll.player.roleid
		// });
		cc.ll.net.send('c2s_get_bagitem', {
			roleid: cc.ll.player.roleid
		});
		cc.ll.net.send('c2s_equip_list', {
			roleid: cc.ll.player.roleid
		});

		this.role_need_sync = {};
	},

	start() {
		cc.ll.auto_world_voice = cc.sys.localStorage.getItem('auto_play_world');
		if (cc.ll.auto_world_voice == null) {
			cc.ll.auto_world_voice = 0;
			cc.sys.localStorage.setItem('auto_play_world', 0);
		}
	},


	setAllPlayerActive() {
		for (const key in this.playerObjs) {
			if (this.playerObjs.hasOwnProperty(key)) {
				var role = this.playerObjs[key].getComponent('role');
				if (cc.ll.player.onlyid == role.netInfo.onlyid || (cc.ll.player.teamid != 0 && cc.ll.player.teamid == role.netInfo.teamid)) {
					role.setRoleHidden(true);
				} else {
					role.setRoleHidden(!cc.ll.allowOtherHide);
				}
			}
		}
	},

	/**
	 * 资源加载完成后,通知服务器玩家进入游戏
	 */
	loadComplete() {
		cc.ll.net.send('c2s_enter_game', {
			accountid: cc.ll.player.accountid
		});

		// CPubFunction.ResetTimer(50);
	},

	GetMainPlayerID() {
		return this.mapLogic.selfHeroLogic.netInfo.onlyid;
	},

	GetPlayerObjs(onlyid) {
		return this.playerObjs[onlyid];
	},

	clearPlayer(onlyid) {
		this.playerObjs[onlyid].getComponent('role').clear();
		this.playerObjs[onlyid].parent = null;
		this.playerObjs[onlyid].destroy();
		delete this.playerObjs[onlyid];
	},

	/**
	 * 清除地图上的玩家对象
	 */
	clearPlayerObjs() {
		for (const key in this.playerObjs) {
			if (this.playerObjs.hasOwnProperty(key)) {
				this.clearPlayer(key);
			}
		}
		this.playerObjs = {};
	},

	synPlayerPos(data) {
		let curRole = null;
		if (data.onlyid == cc.ll.player.onlyid) {
			curRole = this.mapLogic.roleNode;
		}
		if (curRole == null) {
			curRole = this.playerObjs[data.onlyid];
		}
		if (curRole != null) {
			let plogic = curRole.getComponent('role');
			plogic.actlist = [];
			curRole.stopAllActions();
			plogic.setObjPos(data.x, data.y);
		}
	},


	resetSelfPlayerTitle() {
		this.mapLogic.selfHeroLogic.resetRoleTitle();
	},

	/*
	 * 按帧数同步玩家数据
	 */
	synPlayerByFrame(data) {
		for (let item of data) {
			this.role_need_sync[item.onlyid] = item;
		}
	},

	synPlayerInfo(info) {
		if (info.onlyid == cc.ll.player.onlyid && (cc.ll.player.teamid == 0 || (cc.ll.player.teamid > 0 && cc.ll.player.isleader))) {
			if (info.name != this.mapLogic.selfHeroLogic.netInfo.name || info.titleid != this.mapLogic.selfHeroLogic.netInfo.titleid || info.titleval != this.mapLogic.selfHeroLogic.netInfo.titleval) {
				this.mapLogic.selfHeroLogic.setInfo(info);
				this.mapLogic.selfHeroLogic.resetRoleTitle();
			}

			if (this.mainUI.getChildByName('RolePanel')) {
				this.mainUI.getChildByName('RolePanel').getComponent('RolePanel').resetRoleName(info.name);
			}
			return;
		}
		if (cc.ll.player.mapid != info.mapid) {
			return;
		}
		if (info.livingtype == GameDefine.LivingType.Player) {
			let curRole = null;
			if (info.onlyid == cc.ll.player.onlyid) {
				curRole = this.mapLogic.roleNode;
			}
			if (curRole == null) {
				curRole = this.playerObjs[info.onlyid];
			}
			if (curRole == null) {
				curRole = cc.instantiate(this.rolepre); //cc.instantiate(this.mapLogic.roleNode);
				curRole.parent = this.mapLayer;
				curRole.zIndex = 3;
				curRole.livingtype = info.livingtype;

				let comRole = curRole.getComponent('role');
				comRole.setObjPos(info.x, info.y);
				comRole.setInfo(info);
				comRole.onlyid = info.onlyid;
				this.playerObjs[info.onlyid] = curRole;
				this.mapLogic.setTeamPath();
			} else {
				let plogic = curRole.getComponent('role');
				if (!plogic.checkInTeam()) {
					let result = this.mapLogic.searchMovePath(plogic.netInfo.x, plogic.netInfo.y, info.x, info.y);

					plogic.actlist = [];
					curRole.stopAllActions();
					if (result.length > 0) {
						for (const act of result) {
							plogic.actlist.push({
								type: 2,
								info: act
							});
						}
						plogic.living_state = 0;
					}
					plogic.objUpdate();
				}
			}
			if (curRole != null) {
				let plogic = curRole.getComponent('role');
				plogic.netInfo.teamid = info.teamid;
				plogic.netInfo.isleader = info.isleader;

				if (info.name != plogic.netInfo.name || info.titleid != plogic.netInfo.titleid || info.titleval != plogic.netInfo.titleval) {
					plogic.netInfo.titleid = info.titleid;
					plogic.netInfo.titleval = info.titleval;
					plogic.netInfo.titletype = info.titletype;
					plogic.netInfo.bangname = info.bangname;
					//plogic.resetName(info.name);
					plogic.resetRoleTitle();
				}
				if (info.teamid > 0 && info.isleader) {
					plogic.addHeadStatus(info.teamcnt >= 5 ? 'zudui2' : 'zudui');
				} else {
					plogic.delHeadStatus('zudui');
					plogic.delHeadStatus('zudui2');
				}
				if (info.battleid > 0) {
					plogic.addHeadStatus('zhandou');
				} else {
					plogic.delHeadStatus('zhandou');
				}
			}
		}

		if (info.livingtype == GameDefine.LivingType.NPC) {
			let newp = CPubFunction.CreateNpc(info.onlyid, info.resid, info.name, 0, 0, {
				nX: info.x,
				nY: info.y
			}, this.mapLayer);
			newp.parent = this.mapLayer;
			newp.zIndex = 3;
			newp.livingtype = info.livingtype;
			newp.onlyid = info.onlyid;
			newp.resid = info.resid;


			let nDir = CNpcMgr.GetNpcDefaultDir(info.npcconfig);
			info.dir = nDir;

			let comNpc = newp.getComponent('Npc');
			comNpc.setInfo(info);

			CNpcMgr.AddNpcObj(info.onlyid, newp);
			let stConfig = CNpcMgr.mapNpcConfig[info.npcconfig];
			comNpc.Npc_Init(stConfig.stTalk, stConfig.mapButton);
		}
	},

	synPlayerStop(data) {
		let plogic = (this.playerObjs[data.onlyid]).getComponent('role');
		plogic.setObjPos(data.x, data.y);
		plogic.playStop();
	},

	synPlayerExit(data) {
		if (this.role_need_sync[data.onlyid]) {
			delete this.role_need_sync[data.onlyid];
		}
		if (this.playerObjs[data.onlyid] != null) {
			// this.playerObjs[data.onlyid].destroy();
			// delete this.playerObjs[data.onlyid];
			this.clearPlayer(data.onlyid);
		}

		if (CNpcMgr.mapNpcObj[data.onlyid] != null) {
			CNpcMgr.DestroyNpc(data.onlyid);
		}
	},

	synPlayerData(data) {
		cc.ll.player.setUserInfo(data);

		if (this.mainUI.getChildByName('RolePanel')) {
			this.mainUI.getChildByName('RolePanel').getComponent('RolePanel').loadGameData();
		}
		this.uiLogic.setRoleLevel(data.level);
		this.uiLogic.setExp(data.exp, data.maxexp);

		let bangPanel = this.node.getChildByName('BangPanel');
		if (bangPanel) {
			let banglogic = bangPanel.getComponent('BangPanel');
			if (banglogic.xiulianNode.active == true) {
				banglogic.xiulianNode.getComponent('BangXiulian').loadInfo();
			}
			if (banglogic.upgradeNode.active == true) {
				banglogic.upgradeNode.getComponent('BangUpgrade').loadInfo();
			}
		}

		let bagLayer = this.mainUI.getChildByName('BagPanel');
		if (bagLayer != null) {
			bagLayer.getComponent('BagPanel').setSchemeName();
		}
	},

	synChatInfo(info) {
		let voice_index = 0;
		if (info.voice.length > 0) {
			voice_index = cc.ll.voicemgr.addVoice(info.voice);
			info.voice = voice_index;
		} else {
			info.voice = -1;
		}

		this.uiLogic.chatLogic.addListInfo(info);

		if (this.node.getChildByName('ChatPanel')) {
			this.node.getChildByName('ChatPanel').getComponent('ChatPanel').addListInfo(info);
		}

		if (info.msg.length > 0) {
			if (info.scale == 1) {
				let stage = cc.find('Canvas/BattleLayer/BattleStage');
				if (stage) {
					stage.getComponent('BattleStage').objTalk(info);
				}
			} else if (info.scale == 3) {
				let node = cc.find('Canvas/PalaceNotice');
				if (node) {
					let logic = node.getComponent('PalaceNotice');
					if (logic) {
						logic.addStrToList(info.msg);
					}
				}
			}
		}

	},

	synGetFriendList(list) {
		let friendsLayer = this.mainUI.getChildByName('FriendPanel');
		if (this.mainUI.getChildByName('FriendAddLayer')) {
			this.mainUI.getChildByName('FriendAddLayer').destroy();
		}
		if (friendsLayer != null) {
			friendsLayer.getComponent('FriendPanel').loadFriendListInfo(list);
		}
		let pkPanel = this.mainUI.getChildByName('PKPanel');
		if (pkPanel) {
			pkPanel.getComponent('PKPanel').showScrollList(list);
		}
	},

	synFriendChatInfo(info) {
		cc.ll.player.addFriendChatInfo(info);
	},

	synSearchFriendList(list) {
		let addLayer = this.mainUI.getChildByName('FriendAddLayer');
		if (addLayer != null) {
			addLayer.getComponent('FriendAddLayer').showFriendList(list);
		}
		let pkPanel = this.mainUI.getChildByName('PKPanel');
		if (pkPanel) {
			pkPanel.getComponent('PKPanel').showSearch(list);
		}
	},

	synBagItemList(data) {
		let iteminfo = JSON.parse(data.info);
		let list = [];
		for (const itemid in iteminfo) {
			if (iteminfo.hasOwnProperty(itemid)) {
				const element = iteminfo[itemid];
				list.push({
					itemid: itemid,
					count: iteminfo[itemid]
				});
			}
		}
		cc.ll.player.itemList = iteminfo;

		CMainPlayerInfo.vecBagItem = list;

		let bagLayer = this.mainUI.getChildByName('BagPanel');
		if (bagLayer != null) {
			bagLayer.getComponent('BagPanel').loadBagList();
		}

		let schemePanel = cc.find('Canvas/MainUI/SchemePanel');
		if (schemePanel) {
			let equipsNode = schemePanel.getChildByName('schemeNode').getChildByName('EquipsNode');
			let equipsLogic = equipsNode.getComponent('SchemeEquipsPanel');
			if (equipsLogic) {
				equipsLogic.loadBagList();
			}

		}

		let nodPartnerUI = this.mainUI.getChildByName('PartnerUI');
		if (nodPartnerUI != null) {
			nodPartnerUI.getComponent('PartnerUI').OnReceiveBagItem();
		}

		let nodComposUI = this.mainUI.getChildByName('ComposUI');
		if (nodComposUI != null) {
			nodComposUI.getComponent('ComposUI').OnReceiveBagItem();
		}
	},

	synEquipList(data) {
		if (!data || !data.list) {
			cc.ll.player.equipdata = null;
		} else {
			try {
				cc.ll.player.equipdata = JSON.parse(data.list);
			} catch (err) {
				cc.ll.player.equipdata = null;
			}
		}
		let bagLayer = this.mainUI.getChildByName('BagPanel');
		if (bagLayer != null) {
			bagLayer.getComponent('BagPanel').loadBagList();
		}
		let shenbingup = this.node.getChildByName('ShenBingUpPanel');
		if (shenbingup != null) {
			shenbingup.getComponent('ShenBingUpPanel').loadEquipList();
		}
		let xianqi = this.node.getChildByName('XianQiPanel');
		if (xianqi != null) {
			if (xianqi.getComponent('XianQiPanel').upgradeNode.active) {
				xianqi.getComponent('XianQiPanel').upgradeNode.getComponent('XianQiUpPanel').loadEquipList();
			}
		}
	},

	synEquipInfo(data) {
		let info = JSON.parse(data.equip);
		if (cc.ll.player.equipdata) {
			cc.ll.player.equipdata.info[info.EquipID] = info;
		} else {
			return;
		}

		let detailNode = this.node.getChildByName('EquipItemDetail');
		if (detailNode != null && detailNode.getComponent('EquipItemDetail').iteminfo.EquipID == info.EquipID) {
			detailNode.getComponent('EquipItemDetail').loadInfo(info);
		}

		let schemeDetailNode = this.node.getChildByName('SchemeEquipItemDetail');
		if (schemeDetailNode != null && schemeDetailNode.getComponent('SchemeEquipItemDetail').iteminfo.EquipID == info.EquipID) {
			schemeDetailNode.getComponent('SchemeEquipItemDetail').loadInfo(info);
		}

		let workshop = this.node.getChildByName('WorkShopPanel');
		if (workshop != null) {
			workshop.getComponent('WorkShopPanel').reloadListData(info);
		}

		let shenbingCombine = this.node.getChildByName('ShenBingCombinePanel');
		if (shenbingCombine) {
			shenbingCombine.getComponent('ShenBingCombinePanel').finishedCombine(info);
		}
		let shenbingup = this.node.getChildByName('ShenBingUpPanel');
		if (shenbingup) {
			shenbingup.getComponent('ShenBingUpPanel').reloadListData(info);
		}

		let xianqi = this.node.getChildByName('XianQiPanel');
		if (xianqi) {
			if (xianqi.getComponent('XianQiPanel').upgradeNode.active) {
				xianqi.getComponent('XianQiPanel').upgradeNode.getComponent('XianQiUpPanel').loadEquipList();
			}
			if (xianqi.getComponent('XianQiPanel').combineNode.active) {
				xianqi.getComponent('XianQiPanel').combineNode.getComponent('XianQiCombinePanel').finishedCombine(info);
			}
		}
	},

	synNextEquip(data) {
		let info = JSON.parse(data.equip);
		if (!info || info == null) {
			return;
		}
		let workshop = this.node.getChildByName('WorkShopPanel');
		if (workshop != null) {
			workshop.getComponent('WorkShopPanel').nextEquipInfo = info;
			workshop.getComponent('WorkShopPanel').showInfo();
		}

		let shenbingup = this.node.getChildByName('ShenBingUpPanel');
		if (shenbingup != null) {
			shenbingup.getComponent('ShenBingUpPanel').showNextEquipInfo(info);
		}

		let xianqi = this.node.getChildByName('XianQiPanel');
		if (xianqi != null) {
			if (xianqi.getComponent('XianQiPanel').upgradeNode.active) {
				xianqi.getComponent('XianQiPanel').upgradeNode.getComponent('XianQiUpPanel').showNextEquipInfo(info);
			}
		}
	},

	synEquipProperty(data) {
		let workshop = this.node.getChildByName('WorkShopPanel');
		if (workshop != null) {
			workshop.getComponent('WorkShopPanel').showNewProperty(data.property);
		}
	},

	synChangeWeapon(weapon) {
		cc.ll.player.weapon = weapon;
		this.mapLogic.selfHeroLogic.changeWeapon(cc.ll.player.weapon);
	},

	synXianQiList(data) {
		let xianqi = this.node.getChildByName('XianQiPanel');
		if (xianqi != null) {
			xianqi.getChildByName('combine').getComponent('XianQiCombinePanel').loadTypeList(data);
		}
	},

	synLockerItemList(data) {
		let lockerLayer = this.node.getChildByName('LockerPanel');
		if (lockerLayer != null) {
			let baginfo = JSON.parse(data.bag);
			let lockerinfo = JSON.parse(data.locker);
			let equipinfo = JSON.parse(data.equip);
			lockerLayer.getComponent('LockerPanel').loadInfo(baginfo, lockerinfo, equipinfo);
			// lockerLayer.getComponent('LockerPanel').loadBagList(baginfo);
			// lockerLayer.getComponent('LockerPanel').loadLockerList(lockerinfo);
		}
	},

	synIncenseState(data) {
		if (data.ltime > 0) {
			if (this.node.getChildByName('YinYaoXiangPanel')) {
				this.node.getChildByName('YinYaoXiangPanel').destroy();
			}
			let yinyao = cc.instantiate(this.YinYaoXiangPanel);
			yinyao.name = 'YinYaoXiangPanel';
			yinyao.parent = this.node;
			let yinyaologic = yinyao.getComponent('YinYaoXiangPanel');
			yinyaologic.setTime(data.ltime);
		} else {
			if (this.node.getChildByName('YinYaoXiangPanel')) {
				this.node.getChildByName('YinYaoXiangPanel').getComponent('YinYaoXiangPanel').closePanel();
			}
			if (this.autoPatrolNode.active) {
				this.autoPatrolNode.active = false;
				this.mapLogic.selfHeroLogic.playStop();
			}
		}
	},

	synBangList(list) {
		let bangLayer = this.node.getChildByName('BangPanel');
		if (bangLayer != null) {
			bangLayer.getComponent('BangPanel').showAddlayer();
			bangLayer.getComponent('BangPanel').showBangList(list);
		}
	},

	closeBangList() {
		let bangLayer = this.node.getChildByName('BangPanel');
		if (bangLayer != null) {
			bangLayer.destroy();
		}
	},

	clearSelfRoleTitleInfo() {
		cc.ll.player.bangdata = null;
		cc.ll.player.bangid = 0;

		cc.ll.player.titleid = 0;
		cc.ll.player.bangname = '';

		this.resetSelfPlayerTitle();

	},


	synBangInfo(data) {
		cc.ll.player.bangdata = data;
		cc.ll.player.bangid = data.info.bangid;
		cc.ll.player.bangname = data.info.name;
		let bangLayer = this.node.getChildByName('BangPanel');
		if (bangLayer != null) {
			bangLayer.getComponent('BangPanel').showBangLayer();
			bangLayer.getComponent('BangPanel').showBangInfo(data);
		}
	},

	synBangRequest(data) {
		let bangLayer = this.node.getChildByName('BangPanel');
		if (bangLayer != null) {
			bangLayer.getComponent('BangPanel').requestNode.getComponent('BangList').showRequestList(data.requestlist);
		}
	},

	choosePet() {
		let choose = cc.instantiate(this.petSelectedPre);
		choose.parent = this.node;
		cc.ll.net.send('c2s_creat_equip', {
			type: 0,
			roleid: cc.ll.player.roleid,
			index: 1
		});

		cc.ll.net.send('c2s_creat_equip', {
			type: 0,
			roleid: cc.ll.player.roleid,
			index: 2
		});

		cc.ll.net.send('c2s_creat_equip', {
			type: 0,
			roleid: cc.ll.player.roleid,
			index: 3
		});
		cc.ll.net.send('c2s_creat_equip', {
			type: 0,
			roleid: cc.ll.player.roleid,
			index: 4
		});
		cc.ll.net.send('c2s_creat_equip', {
			type: 0,
			roleid: cc.ll.player.roleid,
			index: 5
		});
	},

	synPetInfo(data) {
		if (data == null) {
			return;
		}
		cc.ll.player.petInfo = data;
		let curInfo = null;
		for (let index = 0; index < data.list.length; index++) {
			if (data.list[index].petid == data.curid) {
				curInfo = data.list[index];
				break;
			}
		}
		if (curInfo) {
			this.uiLogic.setPetHeadIcon(curInfo.resid);
			this.uiLogic.setPetLevel(curInfo.level);
		}

		let petLayer = this.mainUI.getChildByName('PetPanel');
		if (petLayer != null) {
			petLayer.getComponent('PetPanel').loadPetList();
		}
	},

	synChangePet(data) {
		cc.ll.player.petInfo.curid = data.curid;

		let petLayer = this.mainUI.getChildByName('PetPanel');
		if (petLayer != null) {
			petLayer.getComponent('PetPanel').loadPetList();
		}
		// 重置宠物自动技能
		cc.ll.local.set('battle_p_skill', 0);

		let petlist = cc.ll.player.petInfo.list;
		for (let index = 0; index < petlist.length; index++) {
			if (petlist[index].petid == data.curid) {
				this.uiLogic.setPetHeadIcon(petlist[index].resid);
				this.uiLogic.setPetLevel(petlist[index].level);
				break;
			}
		}
	},

	synDelPet(data) {
		cc.ll.player.petInfo.curid = data.curid;
		let delpos = -1;
		for (let index = 0; index < cc.ll.player.petInfo.list.length; index++) {
			if (cc.ll.player.petInfo.list[index].petid == data.delid) {
				delpos = index;
			}
		}
		if (delpos != -1) {
			cc.ll.player.petInfo.list.splice(delpos, 1);
		}
		let petLayer = this.mainUI.getChildByName('PetPanel');
		if (petLayer != null) {
			petLayer.getComponent('PetPanel').loadPetList();
		}

		let curInfo = {};
		for (let index = 0; index < cc.ll.player.petInfo.list.length; index++) {
			if (cc.ll.player.petInfo.list[index].petid == data.curid) {
				curInfo = cc.ll.player.petInfo.list[index];
				break;
			}
		}
		this.uiLogic.setPetHeadIcon(curInfo.resid);
		this.uiLogic.setPetLevel(curInfo.level);
	},

	synUpdatePet(data) {
		let petinfo = data.info;
		for (let index = 0; index < cc.ll.player.petInfo.list.length; index++) {
			if (cc.ll.player.petInfo.list[index].petid == petinfo.petid) {
				cc.ll.player.petInfo.list[index] = petinfo;
				break;
			}
		}
		let MainUI = cc.find('Canvas/MainUI');
		let petLayer = MainUI.getChildByName('PetPanel');
		if (petLayer != null) {
			petLayer.getComponent('PetPanel').reloadInfo(petinfo);
		}

		if (cc.ll.player.petInfo.curid == petinfo.petid) {
			this.uiLogic.setPetHeadIcon(petinfo.resid);
			this.uiLogic.setPetLevel(petinfo.level);
		}
		let petFlyPanel = MainUI.getChildByName('PetFlyPanel');
		if (petFlyPanel) {
			petFlyPanel.getComponent('PetFlyPanel').refrush();
		}
	},

	updatePetSSkill(petid, skillid) {
		for (let index = 0; index < cc.ll.player.petInfo.list.length; index++) {
			let petdata = cc.ll.player.petInfo.list[index];
			if (petdata.petid == petid) {
				petdata.shenskill = skillid;
				break;
			}
		}
		let petPanel = cc.find('PetPanel', this.mainUI);
		if (petPanel) {
			let petlogic = petPanel.getComponent('PetPanel');
			petlogic.changeShenSkill(petid, skillid);
		}
		let petSSkillPanel = cc.find('PetPanel/PetShenSkill', this.mainUI);
		if (petSSkillPanel != null) {
			let logic = petSSkillPanel.getComponent('PetShenSkill');
			logic.destroySelf();
		}
	},


	OnClickEarth() {
		let comTaskTip = cc.find('TaskTip', cc.find('Canvas/MainUI/right')).getComponent('TaskTip');
		if (comTaskTip) {
			comTaskTip.stAuto.nKind = 0;
			comTaskTip.stAuto.nTaskID = 0;
		}
	},

	touchBegan(event) {
		//组队状态下,自己非队长,自己没有控制权限
		if (cc.ll.player.teamid > 0 && !cc.ll.player.isleader) {
			return;
		}
		this.OnClickEarth();

		//触摸地面,聊天表情面板处于打开状态,则关闭
		let touchPos = event.getLocation();
		if (this.node.getChildByName('ChatPanel') != null) {
			this.node.getChildByName('ChatPanel').getComponent('ChatPanel').hideEmoji();
		}
		if (cc.ll.player.prisonTime == 0) {
			for (const key in CNpcMgr.mapNpcObj) {
				if (CNpcMgr.mapNpcObj.hasOwnProperty(key)) {
					const npc = CNpcMgr.mapNpcObj[key];
					if (npc.getChildByName('Button').getBoundingBoxToWorld().contains(touchPos)) {
						npc.getComponent('Npc').OnNpcClick();
						return;
					}
				}
			}
			if (!cc.ll.allowOtherHide) {
				for (const key in this.playerObjs) {
					if (this.playerObjs.hasOwnProperty(key)) {
						const p = this.playerObjs[key];
						if (p.getChildByName('Button').getBoundingBoxToWorld().contains(touchPos)) {
							p.getComponent('role').OnClick();
							return;
						}
					}
				}
			}
		}
		let drawPos = this.mapLayer.convertToNodeSpaceAR(touchPos);
		this.mapLogic.touchPos(drawPos);
	},

	touchMoved(event) { },

	touchEnded(event) { },

	startAutoSearchAni() {
		for (let index = 0; index < this.autoSearchNodes.length; index++) {
			const n = this.autoSearchNodes[index];
			n.stopAllActions();
			let ani = cc.sequence(cc.scaleTo(0.35, 1.5), cc.scaleTo(0.35, 1), cc.delayTime(1.8));
			n.runAction(cc.sequence(cc.delayTime(index * 0.4), cc.callFunc(() => {
				n.runAction(cc.repeatForever(ani));
			})));
		}
	},

	startAutoPatrolAni() {
		for (let index = 0; index < this.autoPatrolNodes.length; index++) {
			const n = this.autoPatrolNodes[index];
			n.stopAllActions();
			let ani = cc.sequence(cc.scaleTo(0.35, 1.5), cc.scaleTo(0.35, 1), cc.delayTime(1.8));
			n.runAction(cc.sequence(cc.delayTime(index * 0.4), cc.callFunc(() => {
				n.runAction(cc.repeatForever(ani));
			})));
		}
	},

	showMainMap() {
		this.mainMap.active = true;
		this.mainMap.getComponent('MainMapLogic').init();
	},

	showSmallMap() {
		if (cc.ll.propData.mapdata[cc.ll.player.mapid].bmap == 0) {
			return;
		}
		this.mainMap.active = true;
		let mainmapLogic = this.mainMap.getComponent('MainMapLogic');
		mainmapLogic.init();
		mainmapLogic.showAboveNode(cc.ll.player.mapid);
		mainmapLogic.mainLayer.active = false;
	},

	/**
	 * 开始战斗
	 * @param {*} teamA 
	 * @param {*} teamB 
	 */
	enterBattle(teamA, teamB) {
		if (this.autoPatrolNode.active) {
			this.autoPatrolNode.active = false;
		}
		this.mapLogic.selfHeroLogic.playStop();
		// clean battle layer(清理战斗层)
		let battlelayer = cc.find('Canvas/BattleLayer');
		battlelayer.destroyAllChildren();

		// 初始化战斗关卡
		let battlestage = cc.instantiate(this.battleStagePre);
		battlestage.parent = battlelayer;
		battlestage.name = 'BattleStage';
		let battlelogic = battlestage.getComponent('BattleStage');

		// check battle ui(检测战斗UI层,不存在,则创建战斗UI层)
		let battleui = cc.find('Canvas/BattleUILayer/BattleUI');
		if (battleui == null) {
			let parent = cc.find('Canvas/BattleUILayer');
			battleui = cc.instantiate(this.battleUIPre);
			battleui.parent = parent;
		}
		let battleuilogic = battleui.getComponent('BattleUI');
		battleuilogic.initPetPanel(teamA.list);

		this.uiLogic.isBattle = true;
		cc.ll.notice.removeMsg();

		battlelogic.onBattle();
		battlelogic.setTeam(teamA.list, 1); //teamA.camp);
		battlelogic.setTeam(teamB.list, 2); //teamB.camp);
	},

	exitBattle(data) {
		let battlelayer = cc.find('Canvas/BattleLayer');

		let battlestage = battlelayer.getChildByName('BattleStage');
		if (battlestage) {
			// let deps = cc.loader.getDependsRecursively('Prefabs/Battle/BattleStage');
			// cc.loader.release(deps);
			let logic = battlestage.getComponent('BattleStage');
			logic.clear();
			battlestage.destroy();
		}

		battlelayer.destroyAllChildren();
		this.uiLogic.showBattleEnd(data.result > 0);
	},

	update() {
		let count = 0;
		for (let key in this.role_need_sync) {
			if (this.role_need_sync.hasOwnProperty(key)) {
				this.synPlayerInfo(this.role_need_sync[key]);
				delete this.role_need_sync[key];
				++count;
			}
			if (count == 1) {
				break;
			}
		}
	},
});