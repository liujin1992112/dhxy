﻿let GameRes = require('./GameRes');
let CPubFunction = require('./PubFunction');

cc.Class({
    extends: cc.Component,

    properties: {
        itemList: [cc.Node],
        openBtn: cc.Node,
    },

    onLoad() {
        this.memberCnt = 0;
        this.isOpen = true;
    },

    setTeamListInfo() {
        if (cc.ll.player.teamid == 0) {
            this.node.active = false;
            return;
        }
        this.memberCnt = 0;
        this.node.active = true;
        for (const obj of cc.ll.player.teamInfo.objlist) {
            this.itemList[this.memberCnt].getComponent('MainUITeamItem').setInfo(obj);
            if (this.isOpen) {
                this.itemList[this.memberCnt].x = -this.memberCnt * 67;
                this.itemList[this.memberCnt].opacity = 255;
            }
            this.memberCnt++;
        }
        if (this.isOpen) {
            this.openBtn.x = -(this.memberCnt - 1) * 67 - 40;
        }
        for (let index = this.memberCnt; index < this.itemList.length; index++) {
            this.itemList[index].x = 0;
            this.itemList[index].opacity = 0;
        }
    },

    openTeamList(e, d) {
        if (this.isOpen) {
            this.openBtn.angle = 180;
            this.isOpen = false;
            for (let index = 1; index < this.itemList.length; index++) {
                this.itemList[index].stopAllActions();
                this.itemList[index].runAction(cc.spawn(cc.moveTo(0.2, cc.v2(0, 0)), cc.fadeOut(0.2)));
            }
            this.openBtn.stopAllActions();
            this.openBtn.runAction(cc.moveTo(0.2, cc.v2(-40, 0)));
        }
        else {
            this.openBtn.angle = 0;
            this.isOpen = true;
            for (let index = 1; index < this.memberCnt; index++) {
                this.itemList[index].stopAllActions();
                this.itemList[index].runAction(cc.spawn(cc.moveTo(0.2, cc.v2(-index * 67, 0)), cc.fadeIn(0.2)));
            }
            this.openBtn.stopAllActions();
            this.openBtn.runAction(cc.moveTo(0.2, cc.v2(-(this.memberCnt - 1) * 67 - 40, 0)));
        }
    }
});
