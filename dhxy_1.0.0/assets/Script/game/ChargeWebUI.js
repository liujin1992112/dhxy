cc.Class({
    extends: cc.Component,
    properties: {
        money_label: cc.Label,
        name_label: cc.Label,
        web_view: cc.WebView,
    },

    setData(data) {
        this.data = data;
        let item = cc.ll.propData.charge[data.goodsid];
        if (item) {
            this.name_label.string = item.name;
            this.money_label.string = '￥' + item.money.toFixed(2);
        }
        else {
            this.name_label.string = 'XianYu' + data.money * 100;
            this.money_label.string = '￥' + data.money.toFixed(2);

        }
    },

    openWebView(pay_bankcode, webview) {
        this.data.pay_bankcode = pay_bankcode;
        /* this.data = {
            goodscount: 1,
            goodsid: 1,
            pay_bankcode: '903',
            roleid: 230,
        }; */
        cc.ll.http.send('/charge', this.data, ret => {
            if (ret.errcode != 0)
                return;
            let url = ret.data.url;
            if (webview) {
                this.web_view.node.active = true;
                this.web_view.url = encodeURI(url);
            } else {
                cc.sys.openURL(encodeURI(url));
            }
        });
    },

    onButtonClick(event, param) {
        if (param == 'close') {
            this.node.destroy();
        } else if (param == 'ali') {
            this.openWebView(220);
        } else if (param == 'ali_saoma') {
            this.openWebView(210, true);
        } else if (param == 'wx') {
            this.openWebView(330);
        } else if (param == 'wx_saoma') {
            this.openWebView(310, true);
        }
    },
});
