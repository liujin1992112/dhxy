let LivingThing = require('./living_thing');

cc.Class({
    extends: LivingThing,
    properties: {
    },

    onLoad() {
        this._super();
        this.obj_type = 2;
    },

    setSid(sid){
        let childrens = this.node.getChildren();
        for (const child of childrens) {
            child.active = false;
        }
        this.node.getChildByName('tower'+''+sid).active = true;
    },

    playRun() {
    },

    playStop() {
    },


    playAtk() {
        // this.dragonDisplay.playAnimation('attack1');
        // this.on_attack = true;
        // this.scheduleOnce(() => { this.on_attack = false }, 1);
    },

    playHit(hp) {
        if (this.hpBar != null) {
            this.hpBar.node.active = true;
            this.hpBar.progress = hp / this.hp;
        }
    },

    playDie() {
        this.node.runAction(cc.sequence(cc.fadeOut(1), cc.removeSelf()));
    },
});
