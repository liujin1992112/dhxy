var _vert = require('../../utils/HSL_Vert');
var _frag = require("../../utils/RGB_Frag");

/**
 * 负责战斗角色染色功能
 */
cc.Class({
    extends: cc.Component,

    properties: {
        _dh1: 0,
        _dh2: 0,

        dH1: {
            set(n) {
                this._dh1 = n;
                //this.render();
            }
        },
        dH2: {
            set(n) {
                this._dh2 = n;
                //this.render();
            }
        },
    },

    render() {
        // if (!this._program)
        //     this._program = new cc.GLProgram();
        // if (cc.sys.isNative) {
        //     this._program.initWithString(_vert, _frag);
        //     this._program.link();
        //     this._program.updateUniforms();
        //     var glProgram_state = cc.GLProgramState.getOrCreateWithGLProgram(this._program);
        //     glProgram_state.setUniformFloat("d_H1", this._dh1);
        //     glProgram_state.setUniformFloat("d_H2", this._dh2);
        // } else {
        //     this._program.initWithVertexShaderByteArray(_vert, _frag);
        //     this._program.addAttribute(cc.macro.ATTRIBUTE_NAME_POSITION, cc.macro.VERTEX_ATTRIB_POSITION);
        //     this._program.addAttribute(cc.macro.ATTRIBUTE_NAME_COLOR, cc.macro.VERTEX_ATTRIB_COLOR);
        //     this._program.addAttribute(cc.macro.ATTRIBUTE_NAME_TEX_COORD, cc.macro.VERTEX_ATTRIB_TEX_COORDS);
        //     this._program.link();
        //     this._program.updateUniforms();
        //     this._program.setUniformLocationWith1f(this._program.getUniformLocationForName("d_H1"), this._dh1);
        //     this._program.setUniformLocationWith1f(this._program.getUniformLocationForName("d_H2"), this._dh2);
        // }
        // this.setProgram(this.node._sgNode, this._program);
    },

    setProgram: function (node, program) {
        if (cc.sys.isNative) {
            var glProgram_state = cc.GLProgramState.getOrCreateWithGLProgram(program);
            node.setGLProgramState(glProgram_state);
        } else {
            node.setShaderProgram(program);
        }

        var children = node.children;
        if (!children)
            return;
        for (var i = 0; i < children.length; i++) {
            this.setProgram(children[i], program);
        }
    }
})