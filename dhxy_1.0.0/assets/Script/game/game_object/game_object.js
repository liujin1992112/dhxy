/**
 * 游戏物体基类
 */
cc.Class({
    extends: cc.Component,
    properties: {
    },

    onLoad() {
        this.onlyid = 0;
        this.obj_type = -1;//物体类型，0:role, 1:npc

        this.gridInfoArr = [];//地图二位网格信息数组
        this.gridWidth = 20;
        this.gridHeight = 20;
    },

    setInfo(objinfo) {
        this.onlyid = objinfo.onlyid;
    },

    /**
     * 获取物体在地图上的位置
     * @param {*} x 
     * @param {*} y 
     * @returns 
     */
    getMapPos(x, y) {
        return cc.v2(x * this.gridWidth, y * this.gridHeight);
    },

    setObjPos(x, y) {
        let mpos = this.getMapPos(x, y);
        this.node.setPosition(this.gridWidth / 2 + mpos.x, this.gridHeight / 2 + mpos.y);
    },

});
