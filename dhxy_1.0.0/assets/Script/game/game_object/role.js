let LivingThing = require('./living_thing');
let GameRes = require('../GameRes');
let CPubFunction = require('../PubFunction');
let roleTitleUtil = require('../../utils/RoleTitlesUtil');

/**
 * 抽象地图上角色逻辑脚本
 */
cc.Class({
    extends: LivingThing,
    properties: {
        weaponNode: cc.Node,
        btnPlayerAvatar: cc.Prefab,
    },

    onLoad() {
        this._super();
        this.obj_type = 0;
        this.resid = 0;
        this.run_type = 0;
        this.netInfo = {};
        this.move_speed = 250;
        this.move_tiems = 0;
        // this.teamid = 0;        
        // this.isSelfPlayer = false;

        // this.isShow = false;

        // 角色帧动画结点
        this.animationNode = this.node.getChildByName('frame');
        this.animationMaskNode = this.animationNode.getChildByName('maskframe');
        this.nameImgTitleNode = this.node.getChildByName('imgTitle');
        this.name1Node = this.node.getChildByName('name_1');
        this.name2Node = this.node.getChildByName('name_2');


        this.moveAngle = 0;
        this.weaponname = '';
        this.schedule(this.checkPos, 0.1, cc.macro.REPEAT_FOREVER);
        this.movePath = [];//角色的移动路径信息

        // this.setInfo(cc.ll.player);
        // cc.find('Button', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "role", "OnClick", 0));
    },

    clear() {
        let weaponNode = this.weaponNode;
        let animation = weaponNode.getComponent(cc.Animation);
        if (animation) {
            let clips = animation.getClips();
            for (const clip of clips) {
                weaponNode.getComponent(cc.Animation).stop(clip.name);
                weaponNode.getComponent(cc.Animation).removeClip(clip, true);
            }
        }

        let addonNode = this.node.getChildByName('frame').getChildByName('addon');
        if (addonNode != null) {
            let addonclips = addonNode.getComponent(cc.Animation).getClips();
            for (const clip of addonclips) {
                addonNode.getComponent(cc.Animation).stop(clip.name);
                addonNode.getComponent(cc.Animation).removeClip(clip, true);
            }
        }

        let animationNode = this.animationNode;
        if (animationNode) {
            let clips = animationNode.getComponent(cc.Animation).getClips();
            for (const clip of clips) {
                animationNode.getComponent(cc.Animation).stop(clip.name);
                animationNode.getComponent(cc.Animation).removeClip(clip, true);
            }
        }
    },

    showColorMask() {
        let color1 = this.netInfo.color1,
            color2 = this.netInfo.color2;
        if (this.netInfo.roleid == cc.ll.player.roleid) {
            color1 = cc.ll.player.color1;
            color2 = cc.ll.player.color2;
        }
        if (color1 == 0 && color2 == 0) {
            this.unshowColorMask();
            return;
        }
        this.animationMaskNode.active = true;
        let logic = this.animationMaskNode.getComponent('role_color');
        if (color1 != 0) {
            let r = Math.floor(color1 / 256 / 256),
                g = Math.floor(color1 / 256 % 256),
                b = Math.floor(color1 % 256);
            let t = Math.floor(r) * 1000000 + Math.floor(g) * 1000 + Math.floor(b);
            logic.dH1 = t;
        } else {
            logic.dH1 = 0;
        }

        if (color2 != 0) {
            let r = Math.floor(color2 / 256 / 256),
                g = Math.floor(color2 / 256 % 256),
                b = Math.floor(color2 % 256);
            let t = Math.floor(r) * 1000000 + Math.floor(g) * 1000 + Math.floor(b);
            logic.dH2 = t;
        } else {
            logic.dH2 = 0;
        }
        logic.render(false);
    },

    unshowColorMask() {
        this.animationMaskNode.active = false;
    },

    setRoleHidden(hiddenStatus) {
        let shadow = this.node.getChildByName('shadow');
        let button = this.node.getChildByName('Button');

        if (hiddenStatus != null) {
            this.animationNode.active = hiddenStatus;
            this.animationMaskNode.active = hiddenStatus;
            shadow.active = hiddenStatus;
            button.active = hiddenStatus;
        } else {
            this.animationNode.active = !this.animationNode.active;
            this.animationMaskNode.active = !this.animationNode.active;
            shadow.active = this.animationNode.active;
            button.active = this.animationNode.active;
        }
    },


    /**
     * 重置角色称号或者名字
     */
    resetRoleTitle() {
        if (this.netInfo.titleid == 0) {
            //不显示称谓
            if (this.nameImgTitleNode) {
                this.nameImgTitleNode.active = false;
            }
            if (this.name2Node) {
                this.name2Node.active = false;
            }
            if (this.name1Node) {
                this.name1Node.color = CPubFunction.getReliveColor(this.netInfo.relive);
                this.name1Node.getComponent(cc.Label).string = this.netInfo.name;
                this.name1Node.active = true;
            }
        } else if (this.netInfo.titleid <= 111) {
            //显示图片+名字
            this.name1Node && (this.name1Node.active = false);

            let titleId = this.netInfo.titleid;

            CPubFunction.SetSpriteFrame(this.nameImgTitleNode, 'Common/role_title', `roletitle_${titleId}`);//设置角色称号
            this.nameImgTitleNode && (this.nameImgTitleNode.active = true);
            if (this.name2Node) {
                this.name2Node.color = CPubFunction.getReliveColor(this.netInfo.relive);
                this.name2Node.getComponent(cc.Label).string = this.netInfo.name;
                this.name2Node.active = true;
            }
        } else {
            //显示文字称谓+名字
            this.nameImgTitleNode && (this.nameImgTitleNode.active = false);
            let titleShow = roleTitleUtil.getRoleTitle(this.netInfo.titleid, this.netInfo.titleval, this.netInfo.bangname);
            if (titleShow != null && this.name1Node) {
                this.name1Node.color = new cc.Color(30, 144, 255);
                this.name1Node.getComponent(cc.Label).string = titleShow.name;
                this.name1Node.active = true;
            }
            if (this.name2Node) {
                this.name2Node.color = CPubFunction.getReliveColor(this.netInfo.relive);
                this.name2Node.getComponent(cc.Label).string = this.netInfo.name;
                this.name2Node.active = true;
            }
        }
    },

    resetName(name) {
        this.netInfo.name = name;
        this.name1Node.getComponent(cc.Label).string = name;
    },

    /**
     * 设置角色信息
     * @param {*} objinfo 
     * @param {*} loadGame 
     */
    setInfo(objinfo, loadGame) {
        // console.log('objinfo', objinfo);
        this.netInfo = objinfo;
        if (this.netInfo.x < 0 || this.netInfo.y < 0) {
            this.netInfo.x = 0;
            this.netInfo.y = 0;
        }
        this.resAtlas = GameRes.getRoleRes(objinfo.resid);//获取角色资源图集
        this.resid = objinfo.resid;//角色资源id

        /*
        this.nameNode.color = CPubFunction.getReliveColor(objinfo.relive);
        this.nameNode.getComponent(cc.Label).string = objinfo.name;
        */

        this.resetRoleTitle();

        if (cc.ll.player.onlyid == objinfo.onlyid || (cc.ll.player.teamid != 0 && cc.ll.player.teamid == objinfo.teamid)) {
            //队长
            this.setRoleHidden(true);
        } else {
            this.setRoleHidden(!cc.ll.allowOtherHide);
        }

        // 加载角色外形,并播放动画
        // this.initAnimation();
        this.loadResThenPlay('stand_1');

        // 加载角色武器动画
        CPubFunction.addWeaponAnimation(this.netInfo.weapon, this.weaponNode, this.animationNode, this, this.resid, 'stand', 1);

        let nID = cc.ll.propData.mapdata[objinfo.mapid].mapid;
        let mapinfo = cc.ll.propData.map[nID];
        this.gridInfoArr = mapinfo.mapInfo;
        if (mapinfo.baseInfo.rows <= this.netInfo.y || mapinfo.baseInfo.lines <= this.netInfo.x) {
            this.netInfo.x = mapinfo.startPos.x;
            this.netInfo.y = mapinfo.startPos.y;
        }
        this.setObjPos(this.netInfo.x, this.netInfo.y);

        if (cc.ll.propData.resanchor[this.resid]) {
            this.animationNode.anchorY = cc.ll.propData.resanchor[this.resid].anchorY;
            this.animationMaskNode.anchorY = this.animationNode.anchorY;
        }

        if (this.gridInfoArr[this.netInfo.y][this.netInfo.x] == 2) {
            this.node.opacity = 150;
        } else {
            this.node.opacity = 255;
        }

        if (this.animationNode && this.animationNode.getComponent(cc.Animation)) {
            let clips = this.animationNode.getComponent(cc.Animation).getClips();
            for (const clip of clips) {
                this.animationNode.getComponent(cc.Animation).removeClip(clip, true);
            }
        }
        if (this.animationMaskNode && this.animationMaskNode.getComponent(cc.Animation)) {
            let clips = this.animationMaskNode.getComponent(cc.Animation).getClips();
            for (const clip of clips) {
                this.animationMaskNode.getComponent(cc.Animation).removeClip(clip, true);
            }
        }
        this.showColorMask();
    },


    // setSelfPlayer(isSelf){
    //     this.isSelfPlayer = isSelf;
    // },

    addHeadStatus(name) { //zhandou:战斗 zudui:队长未满员 zudui2:队长满员
        if (this.node.getChildByName(name) != null) {
            return;
        }
        let res = '';
        if (name == 'zhandou') {
            res = 'battle/zhandou';
        } else if (name == 'zudui' || name == 'zudui2') {
            this.delHeadStatus('zudui');
            this.delHeadStatus('zudui2');
            res = 'team/' + name;
        }
        if (res == '') {
            return;
        }
        let self = this;
        cc.loader.loadRes(res, cc.SpriteAtlas, function (err, atlas) {
            if (!self.node) return;
            let curFrames = atlas.getSpriteFrames();

            let curClip = cc.AnimationClip.createWithSpriteFrames(curFrames, 10);
            curClip.name = name;
            curClip.wrapMode = cc.WrapMode.Loop;

            let statusNode = new cc.Node();
            statusNode.parent = self.node;
            let statusAni = statusNode.addComponent(cc.Animation);
            let statusSp = statusNode.addComponent(cc.Sprite);
            statusSp.spriteFrame = curFrames[0];
            statusSp.trim = false;
            statusSp.sizeMode = cc.Sprite.SizeMode.RAW;
            statusNode.name = name;

            statusAni.addClip(curClip);
            statusAni.play(name);

            statusNode.anchorY = 0;
            statusNode.y = 170;
            self.scheduleOnce(() => {
                statusNode.scaleX = 50 / statusNode.width;
                statusNode.scaleY = 50 / statusNode.width;
            }, 0);
        });
    },

    delHeadStatus(name) {
        while (this.node.getChildByName(name)) {
            let node = this.node.getChildByName(name);
            node.parent = null;
            node.destroy();
        }
    },

    changeWeapon(weapon) {
        this.netInfo.weapon = weapon;
    },

    /**
     * 加载角色指定动作的外形资源,并播放对应的动画
     * @param {string} name 动作名称 
     */
    loadResThenPlay(name) {
        // console.log(this.resid, name);
        let index = 0;
        cc.loader.loadRes(`shap/${this.resid}/${name}`, cc.SpriteAtlas, (err, atlas) => {
            ++index;
            if (index == 2) {
                this.playAnimation(name);
            }
        });
        cc.loader.loadRes(`shape_mask/${this.resid}/${name}`, cc.SpriteAtlas, (err, atlas) => {
            ++index;
            if (index == 2) {
                this.playAnimation(name);
            }
        });
    },

    playAnimation(name) {
        if (!this.animationNode.active) {
            return;
        }

        let clips = this.animationNode.getComponent(cc.Animation).getClips();
        for (const clip of clips) {
            if (clip.name == name) {

                this.animationNode.getComponent(cc.Animation).play(name);
                this.playMaskAnimation(name);
                this.scheduleOnce(() => {
                    let preScaleX = this.animationNode.scaleX;
                    this.animationNode.scaleX = preScaleX > 0 ? 500 / this.animationNode.width : -500 / this.animationNode.width;
                    this.animationNode.scaleY = 500 / this.animationNode.height;
                    this.animationMaskNode.anchorY = this.animationNode.anchorY;
                    this.animationMaskNode.anchorX = this.animationNode.anchorX;
                }, 0);
                return;
            }
        }
        this.animationNode.active = false;
        cc.loader.loadRes(`shap/${this.resid}/${name}`, cc.SpriteAtlas, (err, atlas) => {
            if (!this.animationNode) return;
            let curFrames = atlas.getSpriteFrames();
            // for (let i = 1; ; i++) {
            //     let frame = atlas.getSpriteFrame(i);
            //     if (frame) curFrames.push(frame);
            //     else break;
            // }
            let curClip = cc.AnimationClip.createWithSpriteFrames(curFrames, 15);
            curClip.name = name;

            if (name.substr(0, 5) == 'stand' || name.substr(0, 3) == 'run') {
                curClip.wrapMode = cc.WrapMode.Loop;
            } else {
                curClip.wrapMode = cc.WrapMode.Normal;
            }

            let nodeAni = this.animationNode.getComponent(cc.Animation);
            nodeAni.addClip(curClip);
            nodeAni.play(name);
            this.playMaskAnimation(name);
            this.animationNode.active = true;

            this.scheduleOnce(() => {
                let preScaleX = this.animationNode.scaleX;
                this.animationNode.scaleX = preScaleX > 0 ? 500 / this.animationNode.width : -500 / this.animationNode.width;
                this.animationNode.scaleY = 500 / this.animationNode.height;
                this.animationMaskNode.anchorY = this.animationNode.anchorY;
                this.animationMaskNode.anchorX = this.animationNode.anchorX;
            }, 0);
        });
    },

    playMaskAnimation(name) {
        if (!this.animationMaskNode.active) {
            return;
        }

        name = name || this.getCurAnimateName();
        let clips = this.animationMaskNode.getComponent(cc.Animation).getClips();
        for (const clip of clips) {
            if (clip.name == name) {

                this.animationMaskNode.getComponent(cc.Animation).play(name);
                return;
            }
        }

        this.animationMaskNode.active = false;
        cc.loader.loadRes(`shape_mask/${this.resid}/${name}`, cc.SpriteAtlas, (err, atlas) => {
            if (err) {
                console.log(err);
                return;
            }
            let curFrames = atlas.getSpriteFrames();
            let curClip = cc.AnimationClip.createWithSpriteFrames(curFrames, 15);
            curClip.name = name;

            if (name.substr(0, 5) == 'stand' || name.substr(0, 3) == 'run') {
                curClip.wrapMode = cc.WrapMode.Loop;
            } else {
                curClip.wrapMode = cc.WrapMode.Normal;
            }

            let nodeAni = this.animationMaskNode.getComponent(cc.Animation);
            nodeAni.addClip(curClip);
            nodeAni.play(name);
            this.animationMaskNode.active = true;
        });
    },

    /**
     * 添加角色的移动路径点
     * @param {*} pos 
     */
    addMovePath(pos) {
        if (this.onlyid == cc.ll.player.onlyid && cc.ll.player.teamid > 0 && cc.ll.player.isleader) {
            this.movePath.push(pos);
            this.node.parent.getComponent('GameMapLogic').setTeamPath();
            // if (this.movePath.length > 30) {
            //     this.movePath.splice(0, this.movePath.length - 30);
            // }
        }
    },

    /**
     * 对象移动到指定的行号、列号对应的网格位置
     * @param {*} row   行号
     * @param {*} line  列号
     */
    objMove(row, line) {
        this._super();
        let trow = Math.ceil(row);
        let tline = Math.ceil(line);
        this.node.stopAllActions();
        if (this.actlist.length > 0) {
            //取出第一个行动点信息,并中数组中移除掉,添加到移动路径中
            let curpos = this.actlist.shift();
            let samerow = trow == Math.ceil(curpos.info.r);
            let sameline = tline == Math.ceil(curpos.info.l);
            let trowtmp = Math.ceil(curpos.info.r);
            let tlinetmp = Math.ceil(curpos.info.l);
            this.addMovePath({
                r: curpos.info.r,
                l: curpos.info.l
            });
            while (this.actlist.length > 1) {
                //必须保证行动列表中行动点信息数量大于等于2
                trowtmp = Math.ceil(curpos.info.r);
                tlinetmp = Math.ceil(curpos.info.l);
                let nextpos = this.actlist.shift();
                //判断相邻两个行动点状态,下一个行动点和当前行动点同行或者同列
                if ((samerow && Math.ceil(nextpos.info.r) == trowtmp) || (sameline && Math.ceil(nextpos.info.l) == tlinetmp)) {
                    curpos = nextpos;//切换到下一个点
                    this.addMovePath({
                        r: curpos.info.r,
                        l: curpos.info.l
                    });
                    continue;
                }

                //判断相邻两个行动点状态,下一个行动点和当前行动点不同行&&不同列
                if ((!samerow && !sameline && Math.ceil(nextpos.info.r) != trowtmp && Math.ceil(nextpos.info.l) != tlinetmp)) {
                    curpos = nextpos;//切换到下一个点
                    this.addMovePath({
                        r: curpos.info.r,
                        l: curpos.info.l
                    });
                    continue;
                }
                this.actlist.unshift(nextpos);
                break;
            }
            line = curpos.info.l;
            row = curpos.info.r;
        }
        let end_pos = cc.ll.pAdd(cc.v2(this.gridWidth / 2, this.gridHeight / 2), this.getMapPos(line, row));
        let addspd = 0;
        if (this.onlyid != cc.ll.player.onlyid && cc.ll.player.teamid > 0 && cc.ll.player.isleader && this.teamid == cc.ll.player.teamid) {
            // let dis = cc.ll.pDistance(this.node.getPosition(), cc.ll.player.getNode().getPosition());
            // if (dis > 120) {
            //     addspd = (dis - 120) / 120 * this.move_speed;
            // }

            if (this.actlist.length > 5)
                addspd = (this.actlist - 5) * 3;
        }
        let moveDelay = cc.ll.pDistance(cc.v2(this.node.x, this.node.y), end_pos) / (this.move_speed + addspd);
        let self = this;
        this.node.runAction(cc.sequence(cc.moveTo(moveDelay, end_pos), cc.callFunc(() => {
            if (this.actlist.length > 0 && this.actlist[0].type == 2) {
                let act = this.actlist.shift();
                self.addMovePath({
                    r: act.info.r,
                    l: act.info.l
                });
                self.objMove(act.info.r, act.info.l);
            }
        })));

        let temindex = 0;
        if (this.actlist.length > 3) {
            temindex = 2;
        } else {
            temindex = this.actlist.length - 1;
        }
        let tempos = end_pos;
        if (temindex != -1) {
            let act = this.actlist[temindex];
            tempos = cc.ll.pAdd(cc.v2(this.gridWidth / 2, this.gridHeight / 2), this.getMapPos(act.info.l, act.info.r));
        }
        let moveAngle = Math.atan2((tempos.y - this.node.y), (tempos.x - this.node.x)) * 180 / Math.PI;
        let deltaAngle = Math.abs(Math.abs(this.curangle) - Math.abs(moveAngle));
        // console.log(moveAngle, deltaAngle, this.run_type, tempos.x, tempos.y);
        if ((moveAngle > 5 && moveAngle < 175) || (moveAngle > 0 && (this.run_type == 0 || deltaAngle > 90))) {
            if ((moveAngle > 85 && this.animationNode.scaleX < 0) || (moveAngle < 85 && this.animationNode.scaleX > 0)) {
                this.animationNode.scaleX *= -1;
            }
            if (this.run_type != 1) {
                this.run_type = 1;
                this.loadResThenPlay('run_3');
                CPubFunction.addWeaponAnimation(this.netInfo.weapon, this.weaponNode, this.animationNode, this, this.resid, 'run', 3);
            }
            this.curangle = moveAngle;
        } else if ((moveAngle > -175 && moveAngle < -5) || (moveAngle <= 0 && (this.run_type == 0 || deltaAngle > 90))) {
            if ((moveAngle > -85 && this.animationNode.scaleX < 0) || (moveAngle < -85 && this.animationNode.scaleX > 0)) {
                this.animationNode.scaleX *= -1;
            }
            if (this.run_type != 2) {
                this.run_type = 2;
                // this.animationNode.getComponent(cc.Animation).play('rundown');
                this.loadResThenPlay('run_1');
                CPubFunction.addWeaponAnimation(this.netInfo.weapon, this.weaponNode, this.animationNode, this, this.resid, 'run', 1);
            }
            this.curangle = moveAngle;
        }
    },

    playStop() {
        if (cc.ll.player.teamid > 0 && cc.ll.player.isleader) {
            if (this.onlyid != cc.ll.player.onlyid && cc.ll.player.getLogic().living_state != 0) {
                this.living_state = 0;
                return;
            }
        }
        if (this.actlist.length > 0) {
            this.actlist = [];
            this.node.stopAllActions();
        }
        if (this.living_state == 0) {
            return;
        }
        this.move_tiems = 0;
        //
        this.living_state = 0;
        if (this.run_type == 1) {
            this.loadResThenPlay('stand_3');
            CPubFunction.addWeaponAnimation(this.netInfo.weapon, this.weaponNode, this.animationNode, this, this.resid, 'stand', 3);
        } else {
            this.loadResThenPlay('stand_1');
            CPubFunction.addWeaponAnimation(this.netInfo.weapon, this.weaponNode, this.animationNode, this, this.resid, 'stand', 1);
        }

        this.run_type = 0;
        if (this.onlyid != cc.ll.player.onlyid)
            return;

        if (cc.ll.player.teamid == 0 || cc.ll.player.isleader) {
            this.move_tiems = 0;
            let pathlist = [];
            for (let index = 0; this.movePath.length > index * 5 && pathlist.length < 5; index++) {
                let pos = this.movePath[this.movePath.length - 5 * index - 1];
                pathlist.push({
                    x: Math.round(pos.l),
                    y: Math.round(pos.r)
                });
            }

            cc.ll.net.send('c2s_aoi_stop', {
                accountid: this.netInfo.accountid,
                x: this.netInfo.x,
                y: this.netInfo.y,
                path: JSON.stringify(pathlist)
            });
            let gamelogic = (cc.find('Canvas')).getComponent('GameLogic');
            gamelogic.autoNode.active = false;
        }

        this.node.parent.getComponent('GameMapLogic').RoleStop(); //判断是否到传送点
        this.OnArrive();
    },

    OnArrive() {
        if (CPubFunction.stArriveCallBack == null)
            return;

        if (this.onlyid != cc.ll.player.onlyid) //回调函数仅供主角使用
            return;

        if (CPubFunction.GetDistance({
            x: CPubFunction.stArriveCallBack.x,
            y: CPubFunction.stArriveCallBack.y
        }, {
            x: this.netInfo.x,
            y: this.netInfo.y
        }) <= 2) {
            CPubFunction.stArriveCallBack.callback(CPubFunction.stArriveCallBack.data);
            CPubFunction.stArriveCallBack = null;
        }
    },

    checkPos() {
        let hadchange = false;
        let tline = Math.floor(this.node.x / this.gridWidth);
        let trow = Math.floor(this.node.y / this.gridHeight);


        if (this.netInfo.x != tline || this.netInfo.y != trow) {
            this.netInfo.x = tline;
            this.netInfo.y = trow;
            hadchange = true;
        }
        if (cc.ll.player.accountid == this.netInfo.accountid) {
            if (hadchange) {
                if (this.gridInfoArr[trow][tline] == 2) {
                    this.node.opacity = 150;
                } else {
                    this.node.opacity = 255;
                }
                this.move_tiems++;
                let gamelogic = (cc.find('Canvas')).getComponent('GameLogic');
                gamelogic.uiLogic.setPosition(tline, trow);
            }
            if (this.move_tiems >= 5) {
                this.move_tiems = 0;
                if (cc.ll.player.teamid == 0 || cc.ll.player.isleader) {
                    cc.ll.net.send('c2s_aoi_move', {
                        accountid: this.netInfo.accountid,
                        x: this.netInfo.x,
                        y: this.netInfo.y
                    });
                }
            }
        }
    },

    /**
     * 判断角色是否在Team中
     * @returns 
     */
    checkInTeam() {
        if (cc.ll.player.teamid == 0 || !cc.ll.player.isleader) {
            return false;
        }
        if (!cc.ll.player.teamInfo.objlist || !Array.isArray(cc.ll.player.teamInfo.objlist)) {
            return false;
        }
        for (let index = 1; index < cc.ll.player.teamInfo.objlist.length; index++) {
            let obj = cc.ll.player.teamInfo.objlist[index];
            if (obj.livingtype != 1) {
                return false;
            }
            if (obj.onlyid == this.onlyid) {
                return true;
            }
        }
        return false;
    },

    setObjPos(x, y) {
        this._super(x, y);
        this.netInfo.x = x;
        this.netInfo.y = y;
    },

    OnClick() {
        if (this.onlyid == cc.ll.player.onlyid)
            return;

        let goBtnAvater = CPubFunction.CreateSubNode(cc.find('Canvas/MainUI'), {
            nX: 230,
            nY: 120
        }, this.btnPlayerAvatar, 'btnPlayerAvatar');

        goBtnAvater.getComponent('PlayerAvatar').PlayerAvatar_Init(this.netInfo);

    },

    showLevelUp() {
        this.playEffectStr('levelup', 0, 0);
    },

    playEffectStr(strEffect, x, y) {
        let play = () => {
            CPubFunction.CreateSubFrame(this.animationNode, {
                nX: x,
                nY: y
            }, this.effect, strEffect);
        }
        if (this.effect == null) {
            let self = this;
            cc.loader.loadRes("Prefabs/EffectFrame", function (err, prefab) {
                self.effect = prefab;
                play();
            });
        } else {
            play();
        }
    },

    getCurAnimateName() {
        let animation = this.animationNode.getComponent(cc.Animation);
        if (animation) {
            return animation.currentClip.name;
        }
        return null;
    },
});