let gameobjcet = require('./game_object');

/**
 * 对地图上有生命的物品的抽象
 */
cc.Class({
    extends: gameobjcet,
    properties: {
        hpBar: cc.ProgressBar,
    },

    onLoad() {
        this._super();
        this.actlist = []; // 行动队列，{type:t, info:i}.type:(0 idle  1 attack 2 move 3 dead)
        this.living_state = 0;     // 状态  0 idle  1 attack 2 move 3 dead

        this.hp = 100;
        this.mp = 0;
        this.is_die = false;
        this.livingtype = 0;
        this.onlyid = 0;
    },

    playStop() {
        if (this.living_state == 0) {
            return;
        }
        //
        this.living_state = 0;
    },


    playAtk() {
        this.living_state = 1;
    },

    playHit(hp) {
        if (this.hpBar != null) {
            this.hpBar.node.active = true;
            this.hpBar.progress = hp / this.hp;
        }
        //播放被打动画
    },

    playDie() {
        this.living_state = 3;
        //播放死亡动画
        this.is_die = true;
        this.node.runAction(cc.sequence(cc.fadeOut(1), cc.removeSelf()));
    },

    start() {
        this.schedule(this.objUpdate, 0.1, cc.macro.REPEAT_FOREVER, 0.1);
    },

    /**
     * 对象更新
     */
    objUpdate() {
        this.node.zIndex = Math.round(576 - (this.node.parent.convertToWorldSpaceAR(cc.v2(this.node.x, this.node.y))).y);//更新层级
        if (this.actlist.length > 0) {
            if (this.actlist[0].type == 2 && this.living_state != 2) {
                let act = this.actlist.shift();
                this.objMove(act.info.r, act.info.l);
            }
        } else {
            this.playStop();
        }
    },

    setInfo(objinfo) {
        this.onlyid = objinfo.onlyid;
    },

    /**
     * 对象移动到指定的行号、列号对应的网格位置
     * @param {*} row   行号
     * @param {*} line  列号
     */
    objMove(row, line) {
        this.living_state = 2;
    },
});
