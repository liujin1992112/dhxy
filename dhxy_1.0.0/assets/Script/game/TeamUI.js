﻿let GameRes = require('./GameRes');
let CPubFunction = require('./PubFunction');
let CMainPlayerInfo = require('./MainPlayerInfo');

cc.Class({
    extends: cc.Component,

    properties: {
        typeList: cc.Node,
        typeContent: cc.Node,
        typeItem: cc.Node,
        titleLab: cc.Label,

        goTeamBtnAdd: cc.Prefab,
        PartnerUI: cc.Prefab,
        svPartnerList: cc.Prefab,
        btnPartner: cc.Prefab,
        goPartnerCard_1: cc.Prefab,
        goPartnerCard_2: cc.Prefab,
    },

    onLoad() {
        this.typeList.on(cc.Node.EventType.TOUCH_START, this.touchBegan.bind(this));
        this.initTypeList();
    },

    start() {
        cc.find('tabTeam/btnClose', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "TeamUI", "Close", 0));

        // let btnTeam = cc.find('btnTeam', this.node);

        // btnTeam.getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "TeamUI", "OnTeam", 0));
        // cc.find('btnFixedTeam', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "TeamUI", "OnFixedTeam", 0));

        cc.find('tabTeam/btnMyPartner', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "TeamUI", "OpenPartnerUI", 0));
        cc.find('tabTeam/btnCreateTeam', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "TeamUI", "CreateTeam", 0));
        cc.find('tabTeam/btnLeaveTeam', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "TeamUI", "LeaveTeam", 0));
        cc.find('tabTeam/btnMatch', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "TeamUI", "ShowTeamList", 0));
        cc.find('tabTeam/btnList', this.node).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "TeamUI", "ShowRequestList", 0));
        // this.vecTagTeamBtn = [cc.find('btnTeam', this.node), cc.find('btnFixedTeam', this.node)];
        // this.vecTabNode = [cc.find('tabTeam', this.node), cc.find('tabFixedTeam', this.node)];

        this.nCurSelect = 0;
        this.vecPartnerCard = [];

        //--------------------------

        cc.ll.net.send('c2s_ask_partner_list', { nRoleID: cc.ll.player.roleid });

    },

    CreateCardList() {
        CPubFunction.DestroyVecNode(this.vecPartnerCard);

        let goContent = cc.find('tabTeam/nodCardPanel', this.node);

        cc.find('tabTeam/btnMatch', this.node).active = false;
        cc.find('tabTeam/btnCreateTeam', this.node).active = false;
        cc.find('tabTeam/btnLeaveTeam', this.node).active = false;
        cc.find('tabTeam/btnList', this.node).active = false;
        let stStart = { nX: 90, nY: -170 };
        if (cc.ll.player.teamid == 0) {

            cc.find('tabTeam/btnMatch', this.node).active = true;
            cc.find('tabTeam/btnCreateTeam', this.node).active = true;

            //----------- 主角 -------------

            let goCard = this.CreateCard(cc.ll.player);
            cc.find('picPtFlag', goCard).active = false;
            this.vecPartnerCard.push(goCard);

            //------------ 伙伴 ------------

            let nCardIndex = 0;

            for (var it in CMainPlayerInfo.vecChuZhan) {
                let nID = CMainPlayerInfo.vecChuZhan[it];
                if (nID <= 0)
                    continue;

                let stInfo = CMainPlayerInfo.GetPartner(nID);

                nCardIndex++;

                let goCard = this.CreateCard(stInfo);
                goCard.getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "TeamUI", "OnClickCard", { nPos: it }));
                this.vecPartnerCard.push(goCard);
            }

            //------------ 加号 ------------

            if (this.vecPartnerCard.length < 5) {
                let goBtnAdd = CPubFunction.CreateSubNode(goContent, { nX: stStart.nX + this.vecPartnerCard.length * 150, nY: -170 }, this.goTeamBtnAdd, 'goTeamBtnAdd');
                goBtnAdd.getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "TeamUI", "OnClickCard", { nPos: CMainPlayerInfo.GetFreeChuZhanPos() }));
                this.vecPartnerCard.push(goBtnAdd);
            }
        }
        else {
            this.titleLab.string = cc.ll.player.teamInfo.type;
            cc.find('tabTeam/btnLeaveTeam', this.node).active = true;
            if (cc.ll.player.isleader) {
                cc.find('tabTeam/btnList', this.node).active = true;
            }
            if (cc.ll.player.teamInfo == null || cc.ll.player.teamInfo.objlist == null) {
                return;
            }
            let partnerPos = 0;
            for (const obj of cc.ll.player.teamInfo.objlist) {
                let goCard = this.CreateCard(obj);
                if (obj.livingtype == 1) {
                    cc.find('picPtFlag', goCard).active = false;
                }
                else {
                    goCard.getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "TeamUI", "OnClickCard", { nPos: partnerPos }));
                    partnerPos++;
                }
                this.vecPartnerCard.push(goCard);
            }
        }
    },

    CreateCard(objinfo) {
        // console.log(objinfo);
        let stStart = { nX: 90, nY: -170 };
        let goPanel = cc.find('tabTeam/nodCardPanel', this.node);

        let prefab = objinfo.livingtype == 1 ? this.goPartnerCard_1 : this.goPartnerCard_2;
        let strPrefab = objinfo.livingtype == 1 ? 'goPartnerCard_1' : 'goPartnerCard_2';
        let goCard = CPubFunction.CreateSubNode(goPanel, { nX: stStart.nX + this.vecPartnerCard.length * 150, nY: -170 }, prefab, strPrefab);
        CPubFunction.SetAnimation(cc.find('anmRole', goCard), objinfo.resid, 'stand_1'); //zzzErr 需整理资源

        let stData = cc.ll.propData.resanchor[objinfo.resid];

        let goAnmRole = cc.find('anmRole', goCard);
        if (stData) {
            goAnmRole.anchorY = stData.anchorY;
        }

        cc.find('labName', goCard).getComponent(cc.Label).string = objinfo.name;
        cc.find('labLevel', goCard).color = CPubFunction.getReliveColor(objinfo.relive);
        cc.find('labLevel', goCard).getComponent(cc.Label).string = `${objinfo.relive}转${objinfo.level}级`;
        CPubFunction.SetSpriteFrame(cc.find('picRace', goCard), 'Common/ui_common', CPubFunction.GetRaceFileName(objinfo.race));

        return goCard;
    },

    OpenPartnerUI() {
        CPubFunction.CreateSubNode(cc.find('Canvas/MainUI'), { nX: 0, nY: 0 }, this.PartnerUI, 'PartnerUI');
    },

    // OnTeam() {
    //     CPubFunction.ChangeTeamNodeState(this.vecTabNode, 0);
    //     CPubFunction.ChangeTeamButtonState(this.vecTagTeamBtn, 'Common/ui_common', 'ui_common_btn_tab1', 'ui_common_btn_tab2', 0);
    // },

    // OnFixedTeam()  //固定队
    // {
    //     CPubFunction.ChangeTeamNodeState(this.vecTabNode, 1);
    //     CPubFunction.ChangeTeamButtonState(this.vecTagTeamBtn, 'Common/ui_common', 'ui_common_btn_tab1', 'ui_common_btn_tab2', 1);
    // },

    ClosePartnerList() {
        CPubFunction.FindAndDeleteNode(this.node, 'svPartnerList');
    },

    GetReservePartnerCnt() {
        let nCnt = 0;

        for (var it in CMainPlayerInfo.vecPartnerInfo) {
            let stInfo = CMainPlayerInfo.vecPartnerInfo[it];
            if (CMainPlayerInfo.GetParterChuZhanPos(stInfo.id) == -1)
                nCnt++;
        }
        return nCnt;
    },

    TeamChangePartnerState(e, nIndex) {
        this.ClosePartnerList();

        CMainPlayerInfo.vecChuZhan[this.stOld.nPos] = CMainPlayerInfo.vecPartnerInfo[nIndex].id;
        cc.ll.net.send('c2s_change_partner_state', { nPartnerID: CMainPlayerInfo.vecPartnerInfo[nIndex].id, nPos: this.stOld.nPos });

        if (cc.ll.player.teamid == 0) {
            this.CreateCardList();
        }
    },

    ShowRemainPartnerList(e, data) {
        let svPartnerList = CPubFunction.CreateSubNode(this.node, { nX: data.nX, nY: data.nY }, this.svPartnerList, 'svPartnerList');

        cc.find('btnClose', svPartnerList).getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "TeamUI", "ClosePartnerList", 0));

        let content = cc.find('ScrollView/view/content', svPartnerList);

        let nY = 30;

        for (var it in CMainPlayerInfo.vecPartnerInfo) {
            let stInfo = CMainPlayerInfo.vecPartnerInfo[it];

            if (CMainPlayerInfo.GetParterChuZhanPos(stInfo.id) != -1)
                continue;

            nY -= 90;
            let goPartner = CPubFunction.CreateSubNode(content, { nX: 140, nY: nY }, this.btnPartner, 'btnPartner');

            CPubFunction.SetSpriteFrame(cc.find('HeadPic/Icon', goPartner), 'Common/huoban', 'huoban_' + stInfo.resid);
            cc.find('Name', goPartner).getComponent(cc.Label).string = stInfo.name;
            cc.find('Level', goPartner).getComponent(cc.Label).string = `${stInfo.relive}转${stInfo.level}级`;
            cc.find('picState', goPartner).active = false;

            goPartner.getComponent(cc.Button).clickEvents.push(CPubFunction.CreateEventHandler(this.node, "TeamUI", "TeamChangePartnerState", it));
        }

        content.height = Math.max(content.height, CMainPlayerInfo.vecPartnerInfo.length * 90);
    },

    OnClickCard(stEvetn, stData) {
        this.ShowRemainPartnerList(0, { nX: stEvetn.target.x - 275, nY: 0 });
        this.stOld = stData;
    },



    CreateTeam() {
        if (cc.ll.player.prisonTime > 0) {
            CPubFunction.CreateNotice(cc.find('Canvas'), '传说罪孽深重的人是创建不了队伍的！', 2);
            return;
        }
        if (cc.ll.player.teamid > 0) {
            return;
        }
        cc.ll.net.send('c2s_create_team', {
            roleid: cc.ll.player.roleid,
            type: this.titleLab.string
        });
    },

    LeaveTeam() {
        if (cc.ll.player.teamid == 0) {
            return;
        }
        cc.ll.net.send('c2s_leave_team', {
            roleid: cc.ll.player.roleid,
            teamid: cc.ll.player.teamid
        });

    },

    RequestTeam(teamid) {
        if (cc.ll.player.teamid > 0) {
            return;
        }
        cc.ll.net.send('c2s_requst_team', {
            roleid: cc.ll.player.roleid,
            teamid: teamid
        });
    },

    MatchTeam() {
        if (cc.ll.player.teamid > 0) {
            return;
        }
        cc.ll.net.send('c2s_match_team', {
            roleid: cc.ll.player.roleid
        });
    },

    ShowTeamList() {
        if (cc.ll.player.prisonTime > 0) {
            CPubFunction.CreateNotice(cc.find('Canvas'), '老实点，天王老子也救不了你。', 2);
            return;
        }
        if (cc.ll.player.teamid == 0) {
            cc.find('teamList', this.node).active = true;
            cc.find('teamList', this.node).getComponent('TeamList').getList(this.titleLab.string);
        }
    },

    ShowRequestList() {
        if (cc.ll.player.teamid > 0 && cc.ll.player.isleader) {
            cc.find('requestList', this.node).active = true;
            cc.find('requestList', this.node).getComponent('TeamRequestList').getList();
        }
    },

    ShowTaskList() {
        if (cc.ll.player.teamid > 0) {
            return;
        }
        this.typeList.active = true;
    },

    touchBegan(event) {
        let touchPos = event.getLocation();
        if (!this.typeList.getChildByName('listBg').getBoundingBoxToWorld().contains(touchPos)) {
            this.typeList.active = false;
        }
    },

    initTypeList() {
        let typeList = ['无目标', '钟馗抓鬼', '游荡妖魔', '三界妖王', '地煞星', '大雁塔', '寻芳', '地宫', '剧情任务'];
        let curListY = 0;
        let curListX = -240;
        this.typeContent.destroyAllChildren();
        for (const type of typeList) {
            let item = cc.instantiate(this.typeItem);
            item.active = true;
            item.name = type;

            item.getChildByName('name').getComponent(cc.Label).string = type;
            item.x = curListX;
            item.y = curListY;
            curListX += 240;
            if (curListX > 240) {
                curListX = -240;
                curListY -= 85;
            }
            item.parent = this.typeContent;
        }
        if (curListX > -240) {
            curListY -= 85;
        }
        this.typeContent.height = -curListY;
        if (this.typeContent.height < this.typeContent.parent.height) {
            this.typeContent.height = this.typeContent.parent.height;
        }
    },

    typeBtnClicked(e, d) {
        this.titleLab.string = e.target.name;
    },

    Close() {
        cc.ll.AudioMgr.playCloseAudio();
        this.node.destroy();
    },
});
