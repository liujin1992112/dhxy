
cc.Class({
    extends: cc.Component,
    properties: {
        soldierIcon: cc.Sprite,
        soldierName: cc.Label,
        soldierInfo: cc.Label,
        soldierAttack: cc.Label,
        soldierDefense: cc.Label,
    },

    onLoad() {
    },

    loadSoldierInfo(data){
        this.soldierName.string = data.name;
        this.soldierInfo.string = data.info;
        this.soldierAttack.string = data.attack;
        this.soldierDefense.string = data.defense;
    },

    onChangeBtnClicked(e){
        
    },

    onPathBtnClicked(e, d){

    },

    onCloseBtnClicked(e){
        this.node.destroy();
    },
});
