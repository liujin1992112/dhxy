cc.Class({
    extends: cc.Component,

    properties: {
        // WXName: cc.Label,
        WXLabel: cc.Label,
    },
    start() {

    },

    setInfo(WXAccount) {
        this.WXLabel.string = WXAccount;
        // this.WXName.string = WXName;
    },
    //点击复制客服微信  跳转到微信
    onPayClicked(e, d) {
        this.jumpToWechat(this.WXLabel.string);
    },

    jumpToWechat(weixin) {
        if (CC_JSB) {
            if (cc.sys.OS_IOS == cc.sys.os) {
                return jsb.reflection.callStaticMethod("IOSWeChatMgr", "jumpToWechat:", weixin);
            }
            else if (cc.sys.OS_ANDROID == cc.sys.os) {
                return jsb.reflection.callStaticMethod("org/cocos2dx/javascript/AppActivity", "jumpToWechat", "(Ljava/lang/String;)Z", weixin);
            }
        }
        return 0;
    }
    
});