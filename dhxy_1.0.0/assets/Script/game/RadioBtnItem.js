cc.Class({
	extends: cc.Component,
	properties: {
	},

    initCallFunc(target,checkCallFunc){
        this.checkCallFunc = checkCallFunc;
        this.target = target;
    },

	onRadioBtnClick (toggle, customEventData) {
		if(this.checkCallFunc != null && this.target != null && typeof(this.checkCallFunc) == 'function'){
            this.checkCallFunc&this.checkCallFunc.apply(this.target,customEventData);
        }
	},
});
