﻿
let GameRes = require('./GameRes');
let pTaskConfigMgr = require('./task_config').g_pTaskConfigMgr;
let ETaskKind = require('./task_config').ETaskKind;


class CMainPlayerInfo {
    constructor() {
        this.vecBagItem = [];
        this.vecMyGoods = [];
        this.vecTaskState = [];//任务状态信息
        this.mapDailyCnt = {};

        this.vecPartnerInfo = [];
        this.vecChuZhan = [0, 0, 0, 0];
    }

    IsHasPartner(nID) {
        for (var it in this.vecPartnerInfo) {
            if (this.vecPartnerInfo[it].id == nID)
                return it;
        }
        return -1;
    }

    GetPartner(nID) {
        for (var it in this.vecPartnerInfo) {
            if (this.vecPartnerInfo[it].id == nID)
                return this.vecPartnerInfo[it];
        }
        return null;
    }

    GetParterChuZhanPos(nPartnerID) {
        for (var it in this.vecChuZhan) {
            if (this.vecChuZhan[it] == nPartnerID)
                return it;
        }
        return -1;
    }

    GetFreeChuZhanPos() {
        for (var it in this.vecChuZhan) {
            if (this.vecChuZhan[it] == 0)
                return it;
        }
        return -1;
    }

    ChangeChuZhan(nPos, nPartnerID) {
        this.vecChuZhan[nPos] = nPartnerID;
        this.ReplaceChuZhanPos();
    }

    ReplaceChuZhanPos() {
        let vecTmp = this.vecChuZhan.slice(0);
        this.vecChuZhan = [0, 0, 0, 0];

        let nPos = -1;
        for (var it in vecTmp) {
            if (vecTmp[it] == 0)
                continue;
            nPos++;
            this.vecChuZhan[nPos] = vecTmp[it];
        }
    }

    FindDailyTask(nGrop) {
        for (let itTask in this.vecTaskState) {
            let pTaskInfo = pTaskConfigMgr.GetTaskInfo(this.vecTaskState[itTask].nTaskID);
            if (null == pTaskInfo)
                continue;

            if (pTaskInfo.nKind != ETaskKind.EDaily)
                continue;

            if (pTaskInfo.nTaskGrop == nGrop)
                return pTaskInfo.nTaskID;
        }
        return 0;
    }

    IsHasCurTask(nTaskID) {
        for (let itTask in this.vecTaskState) {
            if (this.vecTaskState[itTask].nTaskID == nTaskID)
                return this.vecTaskState[itTask];
        }
        return null;
    }

    GetTaskCurStep(nTaskID) {
        let pTaskState = this.IsHasCurTask(nTaskID);
        if (pTaskState == null)
            return -1;

        for (let nStep in pTaskState.vecStepState) {
            if (pTaskState.vecStepState[nStep].nState == 1)
                return nStep;
        }
        return -1;
    }

    GetTaskStepState(nTaskID, nStep) {
        for (let itTask in this.vecTaskState) {
            let stTaskState = this.vecTaskState[itTask];
            if (stTaskState.nTaskID != nTaskID)
                continue;
            return stTaskState.vecStepState[nStep];
        }
        return null;
    }

    IsAlreadyHasThisGropTask(nGroup) {
        for (let itTask in this.vecTaskState) {
            let stTaskState = this.vecTaskState[itTask];
            let pTaskInfo = pTaskConfigMgr.GetTaskInfo(stTaskState.nTaskID);
            if (pTaskInfo.nTaskGrop == nGroup)
                return true;
        }
        return false;
    }

    GetDailyCnt(nTaskGrop) {
        if (this.mapDailyCnt.hasOwnProperty(nTaskGrop))
            return this.mapDailyCnt[nTaskGrop];
        return 0;
    }

    GetBagItemCnt(nItemID) {
        for (var it in this.vecBagItem) {
            if (this.vecBagItem[it].itemid == nItemID)
                return this.vecBagItem[it].count;
        }
        return 0;
    }

    GetBagItem(nItemID) {
        for (var it in this.vecBagItem) {
            if (this.vecBagItem[it].itemid == nItemID)
                return this.vecBagItem[it];
        }
        return null;
    }

    /**
     * 接受任务数据
     * @param {*} stData 
     */
    OnReceiveRoleTask(stData) {
        let vecData = stData.vecTask;

        //遍历任务数据
        this.vecTaskState = [];
        for (let it in vecData) {
            let stData = vecData[it];

            var stTaskState = {};
            stTaskState.nTaskID = stData.nTaskID;
            stTaskState.vecStepState = [];
            //每一个任务下面每一个step的状态信息
            for (var itStep in stData.vecStep) {
                let stInfo = JSON.parse(stData.vecStep[itStep]);
                stTaskState.vecStepState.push(stInfo);
            }
            this.vecTaskState.push(stTaskState);
        }
        this.mapDailyCnt = JSON.parse(stData.strJsonDaily);
    }
}

let g_ctMainPlayerInfo = null;

module.exports = (() => {
    if (g_ctMainPlayerInfo == null) {
        g_ctMainPlayerInfo = new CMainPlayerInfo();
    }
    return g_ctMainPlayerInfo;
})();



