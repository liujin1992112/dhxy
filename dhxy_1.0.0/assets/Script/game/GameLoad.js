var gRes = require('./GameRes');
cc.Class({
    extends: cc.Component,

    properties: {
        loadingBar: cc.ProgressBar,

        /** 加载背景节点 */
        raceBgs: [cc.Node],
    },

    ctor() {
        this.loadPercent = 0;
    },

    start() {
        //随机加载背景图
        let t = Math.floor(Math.random() * 4);
        t = t == 4 ? 3 : t;
        this.raceBgs[t].active = true;
        // this.resids = [1001, 1002, 2003, 2004, 3005, 3006, 4007, 4008, 5051, 5052, 5011, 5021];

        //资源加载列表
        this.loadList = {

            // mapids: {
            //     list: [1000, 1001, 1002, 1003, 1004, 1005, 1006, 1007, 1008, 1009, 1010, 1011, 1012, 1013, 1014,
            //         1015, 1016, 1017, 1018, 1019, 1202, 1203, 1204, 1205, 1206, 1207, 1208, 1209, 2000, 2001, 2002, 3002, 4001],
            //     completed: 0,
            // },
            // effect: {
            //     list: [''],
            //     completed: 0,
            // },
            // Prefabs: {
            //     list: [''],
            //     completed: 0,
            // },
            ChatEmoji: {
                list: [''],
                completed: 0,
            },
            rolehead: {
                list: [''],
                completed: 0,
            },
            pethead: {
                list: [''],
                completed: 0,
            },
            huobanhead: {
                list: [''],
                completed: 0,
            },
            item: {
                list: [''],
                completed: 0,
            },
            // skill: {
            //     list: [''],
            //     completed: 0,
            // },
            // buff: {
            //     list: [''],
            //     completed: 0,
            // },
        }
        this.scheduleOnce(this.startLoad, 0);
    },

    loadComplete(ftype) {
        this.loadList[ftype].completed++;
        if (this.checkAllComplete()) {
            this.loadPercent = 1;
            this.loadingBar.progress = this.loadPercent;

            let CNpcMgr = require('./NpcMgr');
            CNpcMgr.LoadNpcJson();

            this.scheduleOnce(() => {
                cc.ll.sceneMgr.changeScene('MainScene');
            }, 0.1);
        }
    },

    checkAllComplete() {
        let allcomplete = true;
        for (const key in this.loadList) {
            if (this.loadList.hasOwnProperty(key)) {
                const item = this.loadList[key];
                if (item.list.length != item.completed) {
                    // console.log(key + '还没加载完！！！' + '总：'+item.list.length+'  已：' + item.completed);
                    allcomplete = false;
                    break;
                }
            }
        }
        return allcomplete;
    },

    addLoadingPer(n) {
        this.loadPercent += n;
        // console.log('this.loadPercent', this.loadPercent);
        this.loadingBar.progress = this.loadPercent;
    },

    startLoad() {
        let self = this;
        self.loadingBar.progress = self.loadPercent;

        let pubfunction = require('./PubFunction');
        pubfunction.initPrefab();

        //加载聊天表情图集
        cc.loader.loadRes('Common/ChatEmoji', cc.SpriteAtlas, function (err, atlas) {
            self.addLoadingPer(0.05);
            gRes.m_ChatEmojiAtlas = atlas;//将聊天表情图集保存到全局的GameRes对象中
            self.loadComplete('ChatEmoji');
        });

        //加载角色头像图集
        cc.loader.loadRes("Common/role", cc.SpriteAtlas, function (err, atlas) {
            self.addLoadingPer(0.05);
            gRes.setRoleHead(atlas);//保存角色头像图集到全局的GameRes对象中

            self.loadComplete('rolehead');
        });

        //加载宠物头像图集
        cc.loader.loadRes("Common/pethead", cc.SpriteAtlas, function (err, atlas) {
            self.addLoadingPer(0.05);
            gRes.setPetHead(atlas);//保存宠物头像图集到全局的GameRes对象中
            self.loadComplete('pethead');
        });

        //加载活动副本头像
        cc.loader.loadRes("Common/huoban", cc.SpriteAtlas, function (err, atlas) {
            self.addLoadingPer(0.05);
            gRes.setHuobanHead(atlas);
            self.loadComplete('huobanhead');
        });

        //加载物品图标
        cc.loader.loadRes("icon/item", cc.SpriteAtlas, function (err, atlas) {
            self.addLoadingPer(0.05);
            gRes.setItemIcon(atlas);
            self.loadComplete('item');
        });

    },

    loadUICommon() {
        //     let self = this;
        //     // 地图资源 60个点
        //     cc.loader.loadResDir('Common', cc.SpriteAtlas, function (completedCount, totalCount, item) {
        //         self.loadPercent += 0.4/totalCount;//0.6 + 0.4*completedCount / totalCount;
        //         self.loadingBar.progress = self.loadPercent;
        //     }, function (err, assets) {
        cc.ll.sceneMgr.changeScene('MainScene');
        //     });
    },
});