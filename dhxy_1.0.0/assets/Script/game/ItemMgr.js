﻿
var EItemType =
    {     
        Material: 0,
        Equip: 1,
        Pet: 2,            
        Medicine: 4,
        BestBook: 5,
        BetterBook: 6,
        CommonBook: 7,
        FiveElemeBook: 8,
        Treasure: 9,
        QualityCard:10,
        ChangeBody: 11,
        FeastDay: 12,
        TianCeFu: 13
    };




class CItemType
{
    constructor( nItemKind,nParam, strTypeName,strIcon )
    {
        this.nItemKind = nItemKind;
        this.nParam = nParam;    //当 nItemType 为装备时，nParam 为装备类型。当 nItemType 为 药品，材料，宠物，书时，nParam 为 ItemID
        this.strTypeName = strTypeName; 
        this.strIcon = strIcon; 
    }

}




/********************************************************************* */

let g_ctCItemMgr = null;


class CItemMgr
{
    constructor()
    {
        this.mapItemKind = {};
        this.mapEquipType = {};
        this.mapPet = {};
        this.mapMaterial = {};

        this.LoadDataFromJson();

    }


    LoadDataFromJson()
    {


        //一级分类：物品分类

        this.mapItemKind[EItemType.Equip] = '珍品装备';
        this.mapItemKind[EItemType.Pet] = '珍品宠物';
        this.mapItemKind[EItemType.Material] = '强化材料';
        this.mapItemKind[EItemType.Medicine] = '药品道具';
        this.mapItemKind[EItemType.BestBook] = '终极技能书';
        this.mapItemKind[EItemType.BetterBook] = '高级技能书';
        this.mapItemKind[EItemType.CommonBook] = '普通技能书';
        this.mapItemKind[EItemType.FiveElemeBook] = '五行天书';
        this.mapItemKind[EItemType.Treasure] = '奇珍异宝';
        this.mapItemKind[EItemType.QualityCard] = '属性卡';
        this.mapItemKind[EItemType.ChangeBody] = '变身卡';
        this.mapItemKind[EItemType.FeastDay] = '节日珍品';
        this.mapItemKind[EItemType.TianCeFu] = '天策符';


        //装备分类

        this.mapEquipType[0] = new CItemType(EItemType.Equip, 0, '全部', '');
        this.mapEquipType[1] = new CItemType(EItemType.Equip,1,  '祭剑魂', '');

        this.mapEquipType[2] = new CItemType(EItemType.Equip, 2, '男衣', '');
        this.mapEquipType[3] = new CItemType(EItemType.Equip, 3, '男帽', '');
        this.mapEquipType[4] = new CItemType(EItemType.Equip, 4, '项链', '');
        this.mapEquipType[5] = new CItemType(EItemType.Equip, 5, '鞋子', '');

        this.mapEquipType[6] = new CItemType(EItemType.Equip, 6, '男性挂件', '');
        this.mapEquipType[7] = new CItemType(EItemType.Equip,7, '男性腰带', '');
        this.mapEquipType[8] = new CItemType(EItemType.Equip, 8,'戒指', '');
        this.mapEquipType[9] = new CItemType(EItemType.Equip, 9,'披风', '');

        this.mapEquipType[10] = new CItemType(EItemType.Equip, 10,'逍遥生-扇', '');
        this.mapEquipType[11] = new CItemType(EItemType.Equip, 11,'俏千金-刀', '');
        this.mapEquipType[12] = new CItemType(EItemType.Equip, 12,'神天兵-枪', '');
        this.mapEquipType[13] = new CItemType(EItemType.Equip, 13,'玄剑娥-剑', '');

        this.mapEquipType[14] = new CItemType(EItemType.Equip, 14,'虎头怪-棍', '');
        this.mapEquipType[15] = new CItemType(EItemType.Equip, 15,'狐美人-鞭', '');
        this.mapEquipType[16] = new CItemType(EItemType.Equip, 16,'夜溪灵-灯', '');
        this.mapEquipType[17] = new CItemType(EItemType.Equip, 17,'飞剑侠-剑', '');

        this.mapEquipType[18] = new CItemType(EItemType.Equip, 18,'燕山雪-剑', '');
        this.mapEquipType[19] = new CItemType(EItemType.Equip, 19,'武神尊-枪', '');
        this.mapEquipType[20] = new CItemType(EItemType.Equip, 20,'媚灵狐-刀', '');
        this.mapEquipType[21] = new CItemType(EItemType.Equip, 21,'无崖子-书', '');

        this.mapEquipType[22] = new CItemType(EItemType.Equip, 22,'燕山雪-剑', '');
        this.mapEquipType[23] = new CItemType(EItemType.Equip, 23,'武神尊-枪', '');
        this.mapEquipType[24] = new CItemType(EItemType.Equip, 24,'媚灵狐-刀', '');
        this.mapEquipType[25] = new CItemType(EItemType.Equip, 25,'无崖子-书', '');

        this.mapEquipType[26] = new CItemType(EItemType.Equip, 26,'幽梦影-带', '');
        this.mapEquipType[27] = new CItemType(EItemType.Equip, 27,'神秀生-笔', '');
        this.mapEquipType[28] = new CItemType(EItemType.Equip, 28,'红拂女-剑', '');
        this.mapEquipType[29] = new CItemType(EItemType.Equip, 29,'龙战将-棍', '');

        this.mapEquipType[30] = new CItemType(EItemType.Equip, 30,'云中君-环', '');
        this.mapEquipType[31] = new CItemType(EItemType.Equip, 31,'混天魔-刀', '');
        this.mapEquipType[32] = new CItemType(EItemType.Equip, 32,'九尾狐-爪', '');
        this.mapEquipType[33] = new CItemType(EItemType.Equip, 33,'南冠客-刀', '');

        this.mapEquipType[34] = new CItemType(EItemType.Equip, 34,'镜花影-剑', '');
        this.mapEquipType[35] = new CItemType(EItemType.Equip, 35,'女衣', '');
        this.mapEquipType[36] = new CItemType(EItemType.Equip, 36,'女帽', '');
        this.mapEquipType[37] = new CItemType(EItemType.Equip, 37,'女性挂件', '');
        this.mapEquipType[38] = new CItemType(EItemType.Equip, 38,'女性腰带', '');



    }



    GetItem( nConfigID )
    {
        for (let it in this.mapSkill)
        {
            if ( it == nID)
                return this.mapSkill[it];
        }

        return null;
    }


}

module.exports = { g_ctCItemMgr: new CItemMgr(), EItemType, CItemType };

