
cc.Class({
    extends: cc.Component,

    properties: {
        skillAtlas:cc.SpriteAtlas,

        sprite:cc.Sprite,
        frame:cc.Node,

        _w:28,
        _h:28,
        width:{
            get(){
                return this._w;
            },
            set(n){
                this._w = n;
                this.node.width = n;
                this.sprite.node.width = n;
                this.frame.width = n;
            }
        },
        height: {
            get() {
                return this._h;
            },
            set(n) {
                this._h = n;
                this.node.height = n;
                this.sprite.node.height = n;
                this.frame.height = n;
            }
        },

        _icon_id:0,
        icon: {
            get(){
                return this._icon_id;
            },
            set(n){
                this._icon_id = n;
                this.setSprite(n);
            }, 
        }
    },


    start () {

    },

    setSprite(iconid){
        let frame = this.skillAtlas.getSpriteFrame(iconid);
        this.sprite.spriteFrame = frame;
    },
});
