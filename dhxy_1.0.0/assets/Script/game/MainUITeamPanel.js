﻿let GameRes = require('./GameRes');
let CPubFunction = require('./PubFunction');

cc.Class({
    extends: cc.Component,

    properties: {
        nameLab: cc.Label,
        headIcon: cc.Sprite,
        idLabel: cc.Label,
        leaveBtn: cc.Node,
        kickBtn: cc.Node,
        transferBtn: cc.Node,
    },

    onLoad() {
    },

    setInfo(info) {
        this.objInfo = info;
        this.leaveBtn.active = false;
        this.kickBtn.active = false;
        this.transferBtn.active = false;
        if (cc.ll.player.onlyid == info.onlyid) {
            this.leaveBtn.active = true;
        }else if (cc.ll.player.teamid > 0 && cc.ll.player.isleader) {
            this.kickBtn.active = true;
            this.transferBtn.active = true;
        }
        // else{
        //     this.node.destroy();
        //     return;
        // }
        this.nameLab.node.color = CPubFunction.getReliveColor(info.relive);
        this.nameLab.string = info.name;
        this.idLabel.string = `编号:${info.roleid}`;
    },

    leaveTeam(e, d){
        cc.ll.net.send('c2s_leave_team', {
            roleid: this.objInfo.roleid,
            teamid: this.objInfo.teamid
        });
        this.closeSelf();
    },

    transferTeamLeaderRequst(e, d){
        cc.ll.net.send('c2s_transfer_team_requst',{
            toid: this.objInfo.roleid,
        });
        this.closeSelf();
    },



    closeSelf(e, d){
        this.node.destroy();
    }
});
