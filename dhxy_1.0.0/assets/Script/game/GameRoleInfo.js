
cc.Class({
    extends: cc.Component,
    properties: {
        roleNode: cc.Node,
        soldierIcons: [cc.Node],
        addNode: cc.Node,
        skill1Node: cc.Node,
        skill2Node: cc.Node,
    },

    onLoad() {
        this.soldierList = [];
    },

    loadSoldierInfo(p){
        p = p == null ? -1 : p;
        let self = this;
        if (p != -1) {
            const sid = this.soldierList[p].sid;
            cc.loader.loadRes(`Dragonbones/soldiers/icons/${sid}`, cc.SpriteFrame, function (err, spriteframe) {
                self.soldierIcons[p].getChildByName('icon').getComponent(cc.Sprite).spriteframe = spriteframe;
            });
            return;
        }
        for (let index = 0; index < this.soldierList.length; index++) {
            const sid = this.soldierList[index].sid;
            cc.loader.loadRes(`Dragonbones/soldiers/icons/${sid}`, cc.SpriteFrame, function (err, spriteframe) {
                let icon = self.soldierIcons[index].getChildByName('icon');
                let sp = icon.getComponent(cc.Sprite);
                self.soldierIcons[index].getChildByName('icon').getComponent(cc.Sprite).spriteFrame = spriteframe;
            });
        }

        if (this.soldierList.length < 5) {
            this.addNode.active = true;
            this.addNode.active.x = this.soldierIcons[this.soldierList.length].x;
        }else{
            this.addNode.active = false;
        }
    },

    onChangeBtnClicked(e){
        
    },
});
