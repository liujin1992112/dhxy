module.exports =
	`
varying vec4 v_fragmentColor;
varying vec2 v_texCoord;

uniform float f_alpha;
uniform float d_H1;
uniform float d_H2;

void main()
{
	vec4 v_orColor = v_fragmentColor * texture2D(CC_Texture0, v_texCoord);
	v_orColor.a = (v_orColor.r >= 0.5) ? f_alpha : v_orColor.a;

	if(v_orColor.g >= 0.5){
		v_orColor.r = d_H1 / 1000000.0 / 255.0;
		v_orColor.g = mod(d_H1, 1000000.0) / 1000.0 / 255.0;
		v_orColor.b = mod(d_H1, 1000.0) / 255.0;
		v_orColor.a = 0.4;
		if (d_H1 == 0.0) {
			v_orColor.a = 0.0;
		}
	}
	else if(v_orColor.b >= 0.5){
		v_orColor.r = d_H2 / 1000000.0 / 255.0;
		v_orColor.g = mod(d_H2, 1000000.0) / 1000.0 / 255.0;
		v_orColor.b = mod(d_H2, 1000.0) / 255.0;
		v_orColor.a = 0.4;
		if (d_H2 == 0.0) {
			v_orColor.a = 0.0;
		}
	}
	else {
		v_orColor.a = 0.0;
	}
	gl_FragColor = v_orColor;
}
`
