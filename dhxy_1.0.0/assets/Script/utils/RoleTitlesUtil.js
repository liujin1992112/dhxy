let GameDefine = require('../game/GameDefine');

class RoleTitlesUtil{
    
    constructor() {
    }
    

    pkgRoleTitle(titleItem,bangName){
        let roleTitleInfo = null;
        let roleTitles = [];
        
        let tempList = JSON.parse(JSON.stringify(GameDefine.RoleTitleList));
        roleTitles = tempList.filter((rt) => {                
            return rt.id == titleItem.titleid;
        });                           
        
        if(roleTitles.length > 0){
            roleTitleInfo = roleTitles[0];
            roleTitleInfo.type = titleItem.type;
            if(roleTitleInfo.type == GameDefine.TitleType.CommonTitle){
                //帮会成员
                roleTitleInfo.name = bangName + roleTitleInfo.name;      
                roleTitleInfo.desc = bangName + roleTitleInfo.desc;          
            }else if(roleTitleInfo.type == GameDefine.TitleType.BroTitle || roleTitleInfo.type == GameDefine.TitleType.CoupleTitle){
                //结拜或夫妻，使用自定义称谓
                roleTitleInfo.name = titleItem.value;
            }
            roleTitleInfo.onLoad = titleItem.onload;
        }
        
        return roleTitleInfo;
       
    }


    getRoleTitle(titleId,titleVal,bangName){
        let tempList = JSON.parse(JSON.stringify(GameDefine.RoleTitleList));
        let roleTitles = tempList.filter((rt) => {                
            return rt.id == titleId;
        });                           
        
        if(roleTitles.length > 0){
            let roleTitleInfo = roleTitles[0];            
            if(roleTitleInfo.type == GameDefine.TitleType.CommonTitle){
                //帮会成员
                roleTitleInfo.name = bangName + roleTitleInfo.name;      
                roleTitleInfo.desc = bangName + roleTitleInfo.desc;       
            }else if(roleTitleInfo.type == GameDefine.TitleType.BroTitle || roleTitleInfo.type == GameDefine.TitleType.CoupleTitle){
                //结拜或夫妻，使用自定义称谓
                roleTitleInfo.name = titleVal;
            }
            
            return roleTitleInfo;
        }
        return '';
        
    }
  
}

let instance = null;
module.exports = (()=>{
    if(instance==null){
        instance = new RoleTitlesUtil();
    }
    return instance;
})();
