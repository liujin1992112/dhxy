const MVP = `
attribute vec4 a_position;
attribute vec2 a_texCoord;
attribute vec4 a_color;
varying vec4 v_fragmentColor; 
varying vec2 v_texCoord; 
void main()
{ 
	gl_Position = CC_PMatrix * a_position;
	v_fragmentColor = a_color;
	v_texCoord = a_texCoord;
}
`

const NOMVP = `
attribute vec4 a_position;
attribute vec2 a_texCoord;
attribute vec4 a_color;
varying vec4 v_fragmentColor;
varying vec2 v_texCoord;
void main()
{
    gl_Position = CC_PMatrix  * a_position;
    v_fragmentColor = a_color;
    v_texCoord = a_texCoord;
}
`

const Shaders = {
    Gray: {
        vert: MVP,
        frag: `
#ifdef GL_ES
precision lowp float;
#endif

varying vec4 v_fragmentColor;
varying vec2 v_texCoord;
void main()
{
    vec4 c = v_fragmentColor * texture2D(CC_Texture0, v_texCoord);
    gl_FragColor.xyz = vec3(0.2126*c.r + 0.7152*c.g + 0.0722*c.b);
    gl_FragColor.w = c.w;
}
`
    },
    GrayScaling: {
        vert: MVP,
        frag:
            `

varying vec4 v_fragmentColor;
varying vec2 v_texCoord;
void main () {
    vec4 c = v_fragmentColor * texture2D(CC_Texture0, v_texCoord);
    float gray = dot(c.rgb, vec3(0.299 * 0.5, 0.587 * 0.5, 0.114 * 0.5));
    gl_FragColor = vec4(gray, gray, gray, c.a * 0.5);
}
`
    },
    Stone: {
        vert: MVP,
        frag:
            `
#ifdef GL_ES
precision lowp float;
#endif

varying vec4 v_fragmentColor;
varying vec2 v_texCoord;
void main () {
    vec4 c = v_fragmentColor * texture2D(CC_Texture0, v_texCoord);
    float clrbright = (c.r + c.g + c.b) * (1. / 3.);
    float gray = (0.6) * clrbright;
    gl_FragColor = vec4(gray, gray, gray, c.a);
}
`
    },
    Ice: {
        vert: MVP,
        frag:
            `
#ifdef GL_ES
precision lowp float;
#endif

varying vec4 v_fragmentColor;
varying vec2 v_texCoord;
void main()
{
    vec4 c = v_fragmentColor * texture2D(CC_Texture0, v_texCoord);
    float brightness = (c.r + c.g + c.b) * (1. / 3.);
	float gray = (1.5)*brightness;
    c = vec4(gray, gray, gray, c.a)*vec4(0.8,1.2,1.5,1);
    gl_FragColor =c;
}
`
    },
    Frozen: {
        vert: MVP,
        frag:
            `
#ifdef GL_ES
precision lowp float;
#endif

varying vec4 v_fragmentColor;
varying vec2 v_texCoord;
void main () {
    vec4 c = v_fragmentColor * texture2D(CC_Texture0, v_texCoord);
    c *= vec4(0.8, 1, 0.8, 1);
	c.b += c.a * 0.2;
    gl_FragColor = c;
}
`
    },
    Mirror: {
        vert: MVP,
        frag:
            `
#ifdef GL_ES
precision lowp float;
#endif

varying vec4 v_fragmentColor;
varying vec2 v_texCoord;
void main () {
    vec4 c = v_fragmentColor * texture2D(CC_Texture0, v_texCoord);
    c.r *= 0.5;
    c.g *= 0.8;
    c.b += c.a * 0.2;
    gl_FragColor = c;
}
`
    },
    Poison: {
        vert: MVP,
        frag:
            `
#ifdef GL_ES
precision lowp float;
#endif

varying vec4 v_fragmentColor;
varying vec2 v_texCoord;
void main () {
    vec4 c = v_fragmentColor * texture2D(CC_Texture0, v_texCoord);
    c.r *= 0.8;
	c.r += 0.08 * c.a;
	c.g *= 0.8;
    c.g += 0.2 * c.a;
	c.b *= 0.8;
    gl_FragColor = c;
}
`
    },
    Banish: {
        vert: MVP,
        frag:
            `
#ifdef GL_ES
precision lowp float;
#endif

varying vec4 v_fragmentColor;
varying vec2 v_texCoord;
void main () {
    vec4 c = v_fragmentColor * texture2D(CC_Texture0, v_texCoord);
    float gg = (c.r + c.g + c.b) * (1.0 / 3.0);
    c.r = gg * 0.9;
    c.g = gg * 1.2;
    c.b = gg * 0.8;
    c.a *= (gg + 0.1);
    gl_FragColor = c;
}
`
    },
    Vanish: {
        vert: MVP,
        frag:
            `
#ifdef GL_ES
precision lowp float;
#endif

varying vec4 v_fragmentColor;
varying vec2 v_texCoord;
void main () {
    vec4 c = v_fragmentColor * texture2D(CC_Texture0, v_texCoord);
    float gray = (c.r + c.g + c.b) * (1. / 3.);
    float rgb = gray * 0.8;
    gl_FragColor = vec4(rgb, rgb, rgb, c.a * (gray + 0.1));
}
`
    },
    Invisible: {
        vert: MVP,
        frag:
            `
void main () {
    gl_FragColor = vec4(0,0,0,0);
}
`
    },

    Dissolve: {
        vert: MVP,
        frag:
            `
#ifdef GL_ES
precision lowp float;
#endif

varying vec4 v_fragmentColor;
uniform float time;
varying vec2 v_texCoord;

void main()
{
    vec4 c = color * texture2D(texture,uv0);
    float height = c.r;
    if(height < time)
    {
        discard;
    }
    if(height < time+0.04)
    {
        // 溶解颜色，可以自定义
        c = vec4(.9,.6,0.3,c.a);
    }
    gl_FragColor = c;
}
`
    },

    ZiTouMing: {
        vert: MVP,
        frag:
            `
varying vec4 v_fragmentColor;
varying vec2 v_texCoord;

uniform float f_alpha;

void main()
{
    vec4 v_orColor = v_fragmentColor * texture2D(CC_Texture0, v_texCoord);

    v_orColor.a = (v_orColor.r == 1.0 && v_orColor.g == 0.0 && v_orColor.b == 0.0) ? f_alpha : v_orColor.a;
    
    gl_FragColor = v_orColor;
}
`
    },
};

export default Shaders;