// ShaderMgr.js
// let sprite = node.getComponent(cc.Sprite);
// let shardermgr = require('../utils/ShaderMgr');
// shardermgr.setShader(sprite, 'Ice');


let Shaders = require('./Shaders');
var ShaderMgr = {
    shaderPrograms: {},

    setShader: function (sprite, shaderName) {
        var glProgram = this.shaderPrograms[shaderName];
        if (!glProgram) {
            glProgram = new cc.GLProgram();
            let sharder = Shaders[shaderName];
            glProgram.initWithString(sharder.vert, sharder.frag);
            if (!cc.sys.isNative) {
                glProgram.initWithVertexShaderByteArray(sharder.vert, sharder.frag);
                glProgram.addAttribute(cc.macro.ATTRIBUTE_NAME_POSITION, cc.macro.VERTEX_ATTRIB_POSITION);
                glProgram.addAttribute(cc.macro.ATTRIBUTE_NAME_COLOR, cc.macro.VERTEX_ATTRIB_COLOR);
                glProgram.addAttribute(cc.macro.ATTRIBUTE_NAME_TEX_COORD, cc.macro.VERTEX_ATTRIB_TEX_COORDS);
                glProgram.link();
                glProgram.updateUniforms();
            } else {
                // glProgram.initWithString(sharder.vert_nomvp, sharder.frag);
                // glProgram.addAttribute(cc.macro.ATTRIBUTE_NAME_POSITION, cc.macro.VERTEX_ATTRIB_POSITION);
                // glProgram.addAttribute(cc.macro.ATTRIBUTE_NAME_COLOR, cc.macro.VERTEX_ATTRIB_COLOR);
                // glProgram.addAttribute(cc.macro.ATTRIBUTE_NAME_TEX_COORD, cc.macro.VERTEX_ATTRIB_TEX_COORDS);

                // glProgram.link();
                // glProgram.updateUniforms();
                // this.updateGLParameters();
            }
            
            this.shaderPrograms[shaderName] = glProgram;
        }
        sprite._sgNode.setShaderProgram(glProgram);
        return glProgram;
    },
};

module.exports = ShaderMgr;