cc.Class({
    extends: cc.Component,
    properties: {
        mapdata: '',
    },

    onLoad() {
        this.node.on(cc.Node.EventType.TOUCH_START, this.touchBegan.bind(this));
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this.touchMoved.bind(this));
        this.node.on(cc.Node.EventType.TOUCH_END, this.touchEnded.bind(this));
        this.node.on(cc.Node.EventType.TOUCH_CANCEL, this.touchCanceled.bind(this));

        let src = this.node.getComponent(cc.Sprite).spriteFrame.getTexture().url;
        this.img = new Image();
        let self = this;
        this.img.onload = function () {
            let canvas = document.createElement("Canvas");
            self.ctx = canvas.getContext("2d");
            self.ctx.drawImage(self.img, 0, 0);
        };
        this.img.src = src;

        this.onTouched = false;
    },

    getAlphaByPos(pos) {
        let imgData = this.ctx.getImageData(pos.x, this.node.height - pos.y, 1, 1);
        let alpha = imgData.data[3];
        // console.log(alpha);
        return alpha;
    },

    touchBegan(event) {
        let touchPos = this.node.convertToNodeSpace(event.getLocation());
        if (this.getAlphaByPos(touchPos) > 150) {
            this.node.runAction(cc.scaleTo(0.1, 1.1));
            this.onTouched = true;
        }
    },

    touchMoved(event) {
        if (!this.onTouched) {
            return;
        }
        let touchPos = this.node.convertToNodeSpace(event.getLocation());
        if (this.getAlphaByPos(touchPos) < 150) {
            this.node.runAction(cc.scaleTo(0.1, 1));
            this.onTouched = false;
        }
    },

    touchEnded(event) {
        if (!this.onTouched) {
            return;
        }
        if (this.mapdata == '1010' ||this.mapdata == '1011') {
            this.parentLogic.onMapBtnClicked(this.mapdata);
        }
        this.node.runAction(cc.scaleTo(0.1, 1));
    },

    touchCanceled(evene) {
        this.node.runAction(cc.scaleTo(0.1, 1));
    }

});