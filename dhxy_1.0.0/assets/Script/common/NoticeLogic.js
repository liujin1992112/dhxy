var NoticeType = {
    ONLY_OK: 0,
    OK_CANCEL: 1,
}
cc.Class({
    extends: cc.Component,

    properties: {
        okBtn: cc.Node,
        cancelBtn: cc.Node,

        titleLabel: cc.Label,
        title: {
            get() { return this.titleLabel == null ? "" : this.titleLabel.string; },
            set(str) { if (this.titleLabel) { this.titleLabel.string = str; } },
        },

        msgLabel: cc.Label,
        msg: {
            get() { return this.msgLabel == null ? "" : this.msgLabel.string; },
            set(str) { if (this.msgLabel) { this.msgLabel.string = str; } },
        },

        _type: NoticeType.OK_CANCEL,
        type: {
            get() { return this._type; },
            set(t) {
                if (t == NoticeType.ONLY_OK) {
                    this.cancelBtn.active = false;
                    this.okBtn.x = 0;
                } else if (t == NoticeType.OK_CANCEL) {
                    this.cancelBtn.active = true;
                    this.okBtn.x = -120;
                    this.cancelBtn.x = 120;
                }
                this._type = t;
            }
        },

        blackBg: cc.Node,
        infoNode: cc.Node,
        okCallback: null,
        cancelCallback: null,
    },

    start() {
        this.infoNode.scale = 0.8;
        this.blackBg.opacity = 0;
        this.infoNode.runAction(cc.scaleTo(0.25, 1).easing(cc.easeBackOut()));
        this.blackBg.runAction(cc.fadeTo(0.25, 100));
    },

    deleteself() {
        this.infoNode.runAction(cc.sequence(cc.scaleTo(0.25, 0.5).easing(cc.easeBackIn()), cc.callFunc(() => { this.node.destroy(); })));
    },

    okClick(e, d) {
        if (this.okCallback) {
            this.okCallback();
        }
        cc.ll.AudioMgr.playOpenAudio();
        this.deleteself();
    },

    cancelClick(e, d) {
        if (this.cancelCallback) {
            this.cancelCallback();
        }
        cc.ll.AudioMgr.playOpenAudio();
        this.deleteself();
    }
});
