// var WeChatConfig = require('../etc/wechat_config');
var MsgCode = require('../etc/msg_code');
var StrCommon = require('../etc/str_common');
var Player = require('../player/player');

class WeChatMgr {
    // ReqAuth(account, password) {
    //     // if (cc.sys.OS_IOS == cc.sys.os) {
    //     //     jsb.reflection.callStaticMethod("IOSWeChatMgr", "sendAuthRequest");
    //     // }
    //     // else if (cc.sys.OS_ANDROID == cc.sys.os) {
    //     //     jsb.reflection.callStaticMethod("com/jianghu/shouyou/Native", "LoginWX", "()V");
    //     // }
    //     // else {
    //         if (account == "" || password == "") {
    //             cc.ll.msgbox.addMsg(StrCommon.AccPwdNotNull);
    //             cc.ll.loading.unshowLoading();
    //             return;
    //         }
    //         var data = {};
    //         data.password = password;
    //         data.openid = account;
    //         data.nickname = account;
    //         data.headimgurl = '';
    //         this.LoginGame(data);
    //     // }
    // }

    // ReqAccessToken(auth) {
    //     let tokenurl = WeChatConfig.AccessTokenUrl;
    //     tokenurl += 'appid=' + WeChatConfig.MXWechatAPPID;
    //     tokenurl += '&secret=' + WeChatConfig.MXWechatAPPSecret;
    //     tokenurl += '&code=' + auth;
    //     tokenurl += '&grant_type=authorization_code';
    //     cc.ll.http.sendurl(ret => {
    //         this.ReqUserInfo(ret);
    //     }, tokenurl);

    // }

    // ReqUserInfo(token) {
    //     let infourl = WeChatConfig.UserInfoUrl;
    //     infourl += 'access_token=' + token.access_token;
    //     infourl += '&openid=' + token.openid;
    //     cc.ll.http.sendurl(ret => {
    //         ret.password = 'dongxiangkutong';
    //         this.LoginGame(ret);
    //     }, infourl);
    // }


    // LoginGame(info) {
    //     var data = {};
    //     data.account = info.openid;
    //     data.password = info.password;
    //     data.name = info.nickname;
    //     data.headicon = info.headimgurl;
    //     cc.ll.http.send('/login', data, ret => {
    //         if (ret.errcode != MsgCode.SUCCESS) {
    //             cc.ll.msgbox.addMsg(ret.errcode);
    //             cc.ll.loading.unshowLoading();
    //             return;
    //         }
    //         var player = new Player();
    //         player.gold = ret.gold;
    //         player.name = ret.name;
    //         player.accountid = ret.accountid;
    //         player.roomid = ret.roomid;
    //         player.headicon = ret.headicon;
    //         cc.ll.player = player;
    //         cc.ll.sceneMgr.changeScene('HallScene');
    //     });
    // }

    // shareFinished() {
    //     if (this.shareType == 1) {
    //         cc.ll.http.send('/shared', { accountid: cc.ll.player.accountid }, ret => {
    //             if (ret.errcode == MsgCode.SUCCESS) {
    //                 cc.ll.player.gold = ret.gold;
    //                 if (cc.ll.runningSceneName == 'HallScene') {
    //                     let logic = cc.find('Canvas').getComponent('hallLogic');
    //                     logic.playerGoldLabel.string = cc.ll.player.gold;
    //                 }
    //             } else {
    //                 // if (ret.errcode != MsgCode.FAILED) {
    //                 //     cc.ll.msgbox.addMsg(ret.errcode);
    //                 // }
    //             }
    //         });
    //     }
    // }

    // shareWithFriendUrl(title, content, type) {
    //     if (!type) { type = 0; }
    //     this.shareType = type;
    //     if (cc.sys.OS_IOS == cc.sys.os) {
    //         jsb.reflection.callStaticMethod("IOSWeChatMgr", "shareWithWeixinFriendUrl:Title:Content:ImagePath:ShareType:", WeChatConfig.AppUrl, title, content, jsb.fileUtils.getWritablePath() + 'qy_share_icon.png', type);
    //     }
    //     else if (cc.sys.OS_ANDROID == cc.sys.os) {
    //         jsb.reflection.callStaticMethod("com/jianghu/shouyou/Native", "ShareUrlWX", "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V", WeChatConfig.AppUrl, title, content, jsb.fileUtils.getWritablePath() + 'qy_share_icon.png', type);
    //     }
    //     else {
    //         return;
    //     }
    // }

    // shareWithFriendImg(content) {
    //     if (CC_JSB) {
    //         console.log(jsb.fileUtils.getWritablePath());
    //         //如果待截图的场景中含有 mask，请使用下面注释的语句来创建 renderTexture
    //         // var renderTexture = cc.RenderTexture.create(1280,640, cc.Texture2D.PIXEL_FORMAT_RGBA8888, gl.DEPTH24_STENCIL8_OES);

    //         let size = cc.winSize;
    //         let renderTexture = cc.RenderTexture.create(size.width, size.height, cc.Texture2D.PIXEL_FORMAT_RGBA8888, gl.DEPTH24_STENCIL8_OES);
    //         renderTexture.begin();

    //         cc.director.getScene()._sgNode.visit();
    //         renderTexture.end();
    //         renderTexture.saveToFile('qy_screenshot.png', cc.ImageFormat.PNG, true, function () {
    //             if (cc.sys.OS_IOS == cc.sys.os) {
    //                 jsb.reflection.callStaticMethod("IOSWeChatMgr", "shareWithWeixinFriendImg:ImagePath:", content, jsb.fileUtils.getWritablePath() + 'qy_screenshot.png');
    //             }
    //             else if (cc.sys.OS_ANDROID == cc.sys.os) {
    //                 jsb.reflection.callStaticMethod("com/jianghu/shouyou/Native", "ShareImageWX", "(Ljava/lang/String;I)V", jsb.fileUtils.getWritablePath() + 'qy_screenshot.png', 0);
    //             }
    //             else {
    //                 return;
    //             }
    //         });
    //     }
    // }

    startRecord() {
        console.log(jsb.fileUtils.getWritablePath());
        if (CC_JSB) {
            var soundName = jsb.fileUtils.getWritablePath() + "SoundRecord.wav";
            if (cc.sys.OS_IOS == cc.sys.os) {
                jsb.reflection.callStaticMethod("IOSWeChatMgr", "beginRecordWithName:", soundName);
                return true;
            }
            else if (cc.sys.OS_ANDROID == cc.sys.os) {
                if(jsb.reflection.callStaticMethod("com/jianghu/shouyou/Native", "startSoundRecord", "()I") == 0){
                    cc.ll.msgbox.addMsg(StrCommon.RecordNotPermision);
                    return false;
                }
                return true;
            }
            else {
                return false;
            }
        }
    }

    // endRecord() {
    //     if (CC_JSB) {
    //         let soundName = '';
    //         if (cc.sys.OS_IOS == cc.sys.os) {
    //             soundName = jsb.fileUtils.getWritablePath() + "SoundRecord.wav";
    //             jsb.reflection.callStaticMethod("IOSWeChatMgr", "endRecord");
    //         }
    //         else if (cc.sys.OS_ANDROID == cc.sys.os) {
    //             var soundName = jsb.reflection.callStaticMethod("com/jianghu/shouyou/Native", "stopSoundRecord", "()Ljava/lang/String;");
    //         }
    //         if (soundName.length > 0) {
    //             var data = jsb.fileUtils.getDataFromFile(soundName);
    //             if (data.length < 8192) {
    //                 cc.ll.msgbox.addMsg(StrCommon.RecordTooShot);
    //                 return;
    //             }
    //             var rawarr = new Uint8Array(data);
    //             var rawstr = '';
    //             for (let index = 0; index < rawarr.length; index++) {
    //                 rawstr += rawarr[index] + ':';
    //             }
    //             cc.ll.net.send('talk', rawstr);
    //         }
    //     }
    // }

    getLocation() {
        if (CC_JSB) {
            if (cc.sys.OS_IOS == cc.sys.os) {
                return jsb.reflection.callStaticMethod("IOSWeChatMgr", "getLocation");
            }
            else if (cc.sys.OS_ANDROID == cc.sys.os) {
                return jsb.reflection.callStaticMethod("com/jianghu/shouyou/Native", "getLocation", "()Ljava/lang/String;");
            }
        }
        return "位置不可知";
    }
}

module.exports = new WeChatMgr();