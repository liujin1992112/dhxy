
cc.Class({
    extends: cc.Component,

    properties: {
        titleLabel: cc.Label,
        title: {
            get(){
                return this.titleLabel? this.titleLabel.string : '';
            },
            set(n){
                if(this.titleLabel){
                    this.titleLabel.string = n;
                }
            }
        },

        panel: cc.Node,
        width:{
            get(){
                return this.panel? this.panel.width : 0;
            },
            set(n){
                (this.panel) && (this.panel.width = n);
            }
        },

        height:{
            get(){
                return this.panel? this.panel.height : 0;
            },
            set(n){
                (this.panel) && (this.panel.height = n);
            }
        },
    },

    start () {

    },

    onCloseClicked(e, d){
        this.node.destroy();
    },
});
