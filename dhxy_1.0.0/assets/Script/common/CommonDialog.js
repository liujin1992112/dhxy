cc.Class({
    extends: cc.Component,

    properties: {
        titleLabel: cc.Label,
        title: {
            get() {
                return this.titleLabel == null ? '' : this.titleLabel.string;
            },
            set(n) {
                this.titleLabel && (this.titleLabel.string = n);
            },
        },

        frameBg: cc.Node,

        _isDestroy: false,
        isDestroy: {
            get() {
                return this._isDestroy;
            },
            set(n) {
                this._isDestroy = n;
            },
        },

        dialogWidth: {
            get() {
                return this.frameBg == null ? 0 : this.frameBg.width;
            },
            set(n) {
                this.frameBg && (this.frameBg.width = n);
            },
        },
        dialogHeight: {
            get() {
                return this.frameBg == null ? 0 : this.frameBg.height;
            },
            set(n) {
                this.frameBg && (this.frameBg.height = n);
            },
        }
    },

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {},

    start() {
        
    },

    onEnable() {
        if (this.frameBg == null) {
            return;
        }
        this.frameBg.scale = 0.5;
        this.frameBg.runAction(cc.scaleTo(0.25, 1).easing(cc.easeBackOut()));
    },

    selfClose() {
        this.frameBg.runAction(cc.sequence(cc.scaleTo(0.25, 0.5).easing(cc.easeBackIn()), cc.callFunc(() => {
            if (this._isDestroy) {
                this.node.destroy();
            } else {
                this.node.active = false;
            }
        })));
        cc.ll.AudioMgr.playDefaultBtn();
    }
});