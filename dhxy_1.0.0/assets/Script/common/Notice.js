var MsgStr = require('../etc/msg_str');
let noticeNormalName = 'SysNotice';
var NoticeBox = cc.Class({
    extends: cc.Component,
    properties: {

    },

    ctor() {
        this.pfab = null;
    },

    statics: {

        init: function () {
            var self = this;
            cc.loader.loadRes("Prefabs/Notice", function (err, prefab) {
                self.pfab = prefab;
            });
        },
        addMsg: function (type, msg, okfunc, cancelfunc) {
            if (this.pfab == null) {
                return;
            }
            var node = cc.instantiate(this.pfab);
            node.name = noticeNormalName;
            var logic = node.getComponent('NoticeLogic');
            if (typeof (msg) == "number") {
                msg = MsgStr[msg];
            }
            logic.type = type;
            logic.msg = msg;
            logic.okCallback = okfunc;
            logic.cancelCallback = cancelfunc;

            node.setTimeOut = function (t) {
                node.runAction(cc.sequence(cc.delayTime(t), cc.removeSelf()));
            };

            node.parent = cc.director.getScene();
            return node;
        },

        removeMsg: function () {
            let notice = cc.director.getScene().getChildByName(noticeNormalName);
            if (notice) {
                notice.destroy();
            }
        },
    },
});