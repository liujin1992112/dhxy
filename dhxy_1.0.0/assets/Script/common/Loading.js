cc.Class({
    extends: cc.Component,

    properties: {
        loadPrefab: cc.Prefab,
        _loading: cc.Node,
    },

    statics: {
        init() {
            let self = this;
            cc.loader.loadRes("Prefabs/Loading", function (err, prefab) {
                self.loadPrefab = prefab;
            });
        },

        showLoading(time) {
            if (this.loadPrefab == null) {
                return;
            }
            if (this._loading != null) {
                return;
            }
            if (time == null) {
                time = 10;
            }
            let bg = cc.director.getScene();
            // if (bg.getChildByName .getChildByTag(199871) != null){
            //     return;
            // }
            let loading = cc.instantiate(this.loadPrefab);
            loading.parent = bg;
            let self = this;
            let act1 = cc.delayTime(time);
            let act2 = cc.callFunc(() => {
                // loading.removeFromParent();
                self.unshowLoading();
            });
            loading.runAction(cc.sequence(act1, act2));
            this._loading = loading;
        },

        unshowLoading() {
            if (this._loading != null) {
                this._loading.destroy();
                this._loading = null;
            }
        }
    },
});