module.exports = {
    "LivingType": {
        Unknow: 0,
        Player: 1,
        NPC: 2,
        Monster: 3,
    },

    "RaceType": {
        Unknow: 0,
        Demon: 1,
        Sky: 2,
        Men: 3,
        Ghost: 4,
    },

    "SexType": {
        Unknow: 0,
        Male: 1,
        Female: 2,
    },

    "MapType": {
        Unknow: 0,
        Map: 1,
        Instance: 2,
    }
}