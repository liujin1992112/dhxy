let MsgStr = require('../etc/msg_str');

let Msgbox = cc.Class({
    // extends: cc.Component,
    properties: {

    },
    statics: {
        bg: null,
        init: function () {
            if (this.bg === null) {
                this.bg = new cc.Node();
                this.bg.setPosition(cc.v2(512, 100));
                cc.game.addPersistRootNode(this.bg);
                if (this.bg === null) {
                    return;
                }
            }
        },
        addMsg: function (msg) {
            if (typeof (msg) == "number") {
                msg = MsgStr[msg];
            }
            let msgnode = new cc.Node();
            msgnode.color = cc.color(255, 50, 68, 255);

            let msglabel = msgnode.addComponent(cc.Label);
            msglabel.string = msg;
            msglabel.fontSize = 30;
            msglabel.fontFamily = '黑体';

            let labeloutline = msgnode.addComponent(cc.LabelOutline);
            labeloutline.color = cc.color(0,0,0,255);
            labeloutline.width = 1;

            msgnode.setPosition(0, -50);
            msgnode.parent = this.bg;
            
            let act1 = cc.moveBy(2.5, cc.v2(0, 150));
            let act2l = [];
            act2l[act2l.length] = cc.fadeIn(0.5);
            act2l[act2l.length] = cc.delayTime(0.8);
            act2l[act2l.length] = cc.fadeOut(0.5);
            act2l[act2l.length] = cc.removeSelf();
            let act2 = cc.sequence(act2l);

            msgnode.runAction(act1);
            msgnode.runAction(act2);
        },
    },
});
module.exports = Msgbox;