class Lstorage {
    getItem(key) {
        let ackey = cc.ll.player.account + '_' + key;
        return cc.sys.localStorage.getItem(ackey);
    }

    setItem(key, value) {
        let ackey = cc.ll.player.account + '_' + key;
        cc.sys.localStorage.setItem(ackey, value);
    }
}

let lstorage = new Lstorage();

module.exports = lstorage;