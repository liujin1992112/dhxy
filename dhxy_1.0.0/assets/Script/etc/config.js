let ServerHttp = "43.249.194.76";
let ServerPort = 8560;
// let GameType = 'Release'; // Debug or Release
let GameType = 'Debug'; // Debug or Release

if (GameType == 'Debug') {
    ServerHttp = "127.0.0.1";
	// ServerHttp = "localhost";
}

module.exports = {
    GameType: GameType,
    ServerHttp: ServerHttp,
    ServerPort: ServerPort,
};
