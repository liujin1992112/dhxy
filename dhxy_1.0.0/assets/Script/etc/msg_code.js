var MsgCode = {
    SUCCESS: 0,
    FAILED: 1,

    SERVICE_HAS_REPEAT: -1,

    REGISTER_ACCOUNT_REPEAT: 100001, // 帐号重复
    REGISTER_DATABASE_ERROR: 100002, // 数据库插入错误

    LOGIN_ACCOUNT_PWD_ERROR: 100003, // 帐号或密码错误

    SERVICE_NOT_FOUND: 100004, //
    ACCOUNT_FREEZE: 100005, // 帐号被封，请联系客服查询
    NETWORK_ERROR: 100006, //
    NETWORK_DISCONNECT: 100007, // 网络断开连接
    CREATE_NAME_INVALID: 100008,//名字不合法
    INVITE_CODE_ERR: 100009, //邀请码错误
    VERSION_ERROR: 100010,//版本不正确
    NEED_RELOGIN: 100011,//版本不正确

    OPERATION_ERROR: 200001,//操作失败

    ROLE_NAME_EXIST: 200010,//角色名已存在
    ITEM_OPERATION_ERROR: 200011,//物品操作失败
    
    RELIVE_LEVEL_TOO_HIGH: 200012, // 更高转生等级暂未开放
    RELIVE_LEVEL_NOT_ENOUGH: 200013, // 转生等级不足
    BAG_ITEM_IS_FULL: 200014, // 背包已满

    GIFT_HAS_GOT : 300001,// 没有礼包或者已经领过了

    HONGBAO_GET_YET: 900001,//红包已经领过了
    SLDH_NOT_OPEN:900002, // 水陆大会没有开启
    SLDH_NOT_SIGN_TIME: 900003, // 不在水陆大会报名时间
    SLDH_SIGN_ALREADY:900004,// 已经报名过了。
    SLDH_SIGN_TEAM: 900005,// 水路大会必须3人以上组队参加
    SLDH_SIGN_TEAM_LEADER: 900006,// 只能队长报名
    SLDH_NOT_SIGN:900007,//水路大会 没有报名
    SLDH_SIGN_LEVEL_80: 900008,

    LINGHOU_NOT_TEAM:900100,// 不能组队 灵猴大吼一句，欺人太甚，还想以多欺少！
    LINGHOU_MONEY_ENOUGH:900101,// 银两不足 灵猴轻蔑的看了你一眼，便不再搭理你了！
    LINGHOU_FIGHT_TOO_MACH:900102, // 灵猴攻击次数太多 小猴子大喊一声，少侠饶命，小的再也不敢了！
    LINGHOU_TODAY_TOO_MACH:900103, // 今天攻击猴子次数太多 小猴子大喊一声，少侠饶命，小的再也不敢了！
};

module.exports = MsgCode;
