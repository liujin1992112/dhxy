var MsgCode = require("./msg_code");

var MsgStr = {
    [MsgCode.SUCCESS]: '成功',
    [MsgCode.FAILED]: '失败',

    [MsgCode.REGISTER_ACCOUNT_REPEAT]: '帐号重复',
    [MsgCode.REGISTER_DATABASE_ERROR]: '无法连接到服务器，请稍后再试',
    [MsgCode.LOGIN_ACCOUNT_PWD_ERROR]: '帐号或密码错误',
    [MsgCode.SERVICE_NOT_FOUND]: '网络错误',
    [MsgCode.ROOM_CREATE_FALIED]: '房间创建失败,请稍后重试',
    [MsgCode.ACCOUNT_FREEZE]: '帐号被封，请联系客服查询',
    [MsgCode.NETWORK_ERROR]: '网络连接失败，请稍后再试',
    [MsgCode.NETWORK_DISCONNECT]: '网络连接失败，点击重试',
    [MsgCode.INVITE_CODE_ERR]: '邀请码错误',
    [MsgCode.VERSION_ERROR]: '版本错误',
    [MsgCode.NEED_RELOGIN]: '登录超时，请重新登录',
    
    [MsgCode.OPERATION_ERROR]: '操作失败',

    [MsgCode.ITEM_OPERATION_ERROR]: '物品操作失败',
    [MsgCode.ROLE_NAME_EXIST]: '角色名已存在！',
    [MsgCode.CREATE_NAME_INVALID]: '名字不合法',
    
    [MsgCode.RELIVE_LEVEL_TOO_HIGH]: '更高转生等级暂未开放',
    [MsgCode.RELIVE_LEVEL_NOT_ENOUGH]: '转生等级不足',

    [MsgCode.BAG_ITEM_IS_FULL]:'背包已满',
    [MsgCode.GIFT_HAS_GOT] : '已经领取过了 或 暂时没有礼包',

    [MsgCode.HONGBAO_GET_YET]: '这个时间段的红包已经领过了，等下一次吧',
    [MsgCode.SLDH_NOT_OPEN]:'水陆大会没有开启',
    [MsgCode.SLDH_NOT_SIGN_TIME]:'不在水陆大会报名时间',
    [MsgCode.SLDH_SIGN_ALREADY]:'已经报名过了',
    [MsgCode.SLDH_SIGN_TEAM]:' 水路大会必须3人以上组队参加',
    [MsgCode.SLDH_SIGN_TEAM_LEADER]:'只能队长报名',
    [MsgCode.SLDH_NOT_SIGN]:'水路大会 没有报名',
    [MsgCode.SLDH_SIGN_LEVEL_80]:'水路大会必须所有队员在80级以上',

    [MsgCode.LINGHOU_NOT_TEAM]:'灵猴大吼一句，欺人太甚，还想以多欺少！',// 不能组队 
    [MsgCode.LINGHOU_MONEY_ENOUGH]:'灵猴轻蔑的看了你一眼，便不再搭理你了！',// 银两不足 
    [MsgCode.LINGHOU_FIGHT_TOO_MACH]:'灵猴大喊一声，少侠饶命，小的再也不敢了', // 灵猴攻击次数太多 
    [MsgCode.LINGHOU_TODAY_TOO_MACH]:'灵猴看到你就瑟瑟发抖', // 今天攻击猴子次数太多 
};

module.exports = MsgStr;
