let SkillConfig = require('./skill_config')

var SkillStr = {
    EMagicStr: {//法系名称
        [SkillConfig.EMagicType.Chaos]: '混乱',
        [SkillConfig.EMagicType.Toxin]: '毒',
        [SkillConfig.EMagicType.Sleep]: '昏睡',
        [SkillConfig.EMagicType.Seal]: '封印',
        [SkillConfig.EMagicType.Wind]: '风法',
        [SkillConfig.EMagicType.Fire]: '火法',
        [SkillConfig.EMagicType.Thunder]: '雷法',
        [SkillConfig.EMagicType.Water]: '水法',
        [SkillConfig.EMagicType.Speed]: '加速',
        [SkillConfig.EMagicType.Defense]: '加防',
        [SkillConfig.EMagicType.Attack]: '加攻',
        [SkillConfig.EMagicType.Frighten]: '震慑',
        [SkillConfig.EMagicType.ThreeCorpse]: '三尸',
        [SkillConfig.EMagicType.Charm]: '魅惑',
        [SkillConfig.EMagicType.GhostFire]: '鬼火',
        [SkillConfig.EMagicType.Forget]: '遗忘'
    },

    SkillIntros: {//技能名称
        [SkillConfig.SkillIds.NormalAtkSkill]: '',
        [SkillConfig.SkillIds.NormalDefSkill]: '',
        [SkillConfig.SkillIds.HeDingHongFen]: '利用无色无味的剧毒修炼出来的药粉，消灭敌人于无形。',
        [SkillConfig.SkillIds.JieDaoShaRen]: '非常巧妙的计谋，借他人之手而杀敌，使得他人进行混淆攻击。',
        [SkillConfig.SkillIds.MiHunZui]: '道家的不二法门，可令敌人如同醉酒，昏昏欲睡。',
        [SkillConfig.SkillIds.ZuoBiShangGuan]: '以强大的异能封住对手，使其完全无法动弹，而只能作壁上观。',
        [SkillConfig.SkillIds.WanDuGongXin]: '集天下最毒之物所修炼而成的法术，剧毒无比，中者不死即伤。',
        [SkillConfig.SkillIds.ShiXinKuangLuan]: '使用混乱法术，使多个目标变得丧心病狂，而自相残杀起来。',
        [SkillConfig.SkillIds.BaiRiMian]: '菩提祖师的修炼法门之一，据说使用可以让多个目标百日不醒。',
        [SkillConfig.SkillIds.SiMianChuGe]: '发动四面八方的能力，隔绝对手与外界的接触，使其只能坐以待毙。',
        [SkillConfig.SkillIds.LieHuoJiaoYang]: '吸收天上太阳的热力才凝聚起来的强烈火焰，让人受到严重伤害。',
        [SkillConfig.SkillIds.FengLeiYongDong]: '利用自己身体中的能量产生强烈的有毒的风进行攻击。',
        [SkillConfig.SkillIds.DianShanLeiMing]: '让掌管雷电的神仙帮助施法，强大的力量和声音伤害他人。',
        [SkillConfig.SkillIds.JiaoLongChuHai]: '传说中蛟龙飞升的时候天会降大雨，利用这个能量进行攻击伤害别人。',
        [SkillConfig.SkillIds.JiuYinChunHuo]: '天下最厉害的火，不是阳刚之火，而是至柔至阴的以柔克刚的阴火，杀伤力不可想象。',
        [SkillConfig.SkillIds.XiuLiQianKun]: '镇元大仙的绝技，乾坤袖一抖就令天地动容，风云变色，伤人于无形。',
        [SkillConfig.SkillIds.TianZhuDiMie]: '缭绕的仙气，唤起天界异人之雷力，施与对手无与伦比的伤害。',
        [SkillConfig.SkillIds.JiuLongBingFeng]: '利用巨大的水流转化而成的冰块进行袭击敌人，在攻击的过程中把人冰封了。',
        [SkillConfig.SkillIds.MoShenHuTi]: '召唤鬼神依附到自己身上。',
        [SkillConfig.SkillIds.TianWaiFeiMo]: '天外魔神处学来的法术，激发自身和队友潜能。',
        [SkillConfig.SkillIds.ShouWangShenLi]: '共工法术，激发自己和队友的魔性。',
        [SkillConfig.SkillIds.XiaoHunShiGu]: '把对手的生命用鬼神的力量和魔法偷偷消耗,减少当前气血和法力',
        [SkillConfig.SkillIds.HanQingMoMo]: '脉脉含情的眼神让目标忘却痛苦，大大增加各项抗性。',
        [SkillConfig.SkillIds.QianKunJieSu]: '借助战斗魔神的力量，增加自己和队友的速度。',
        [SkillConfig.SkillIds.MoShenFuShen]: '战斗魔神之力，强化己方的攻击力和命中。',
        [SkillConfig.SkillIds.YanLuoZhuiMing]: '利用阎王的力量按比例消减对手生命和魔法。',
        [SkillConfig.SkillIds.QinSiBingWu]: '冰雾怨何穷，秦丝娇未已。减少冰混睡忘抗性。',
        [SkillConfig.SkillIds.XueShaZhiGu]: '极厉害的蛊虫，可以吸取对方的鲜血并传送给己方。',
        [SkillConfig.SkillIds.LuoRiRongJin]: '熊熊冥火，烈血残阳，映入耳目，所到之处，尽染血色。',
        [SkillConfig.SkillIds.ShiXinFeng]: '鬼上身，使敌方丧心病狂。每个技能都有几率被遗忘，无法使用物品。',
        [SkillConfig.SkillIds.QianNvYouHun]: '小倩独家秘笈。减少对手各项抗性，使其虚弱。',
        [SkillConfig.SkillIds.XiXingDaFa]: '放出尸蛊之虫，蚕食对方多个单位的生命，并化为己用。',
        [SkillConfig.SkillIds.XueHaiShenChou]: '血海深仇，引燃一切，对目标造成大量伤害。',
        [SkillConfig.SkillIds.MengPoTang]: '饮下孟婆汤，三生梦断，返生无路。每个技能都有几率被遗忘，无法使用物品。'
    }
}


module.exports = SkillStr;