var StrCommon = {
    StartLoading: '正在载入',
    AccPwdNotNull: '帐号或密码不能为空',
    NetWorkError: '网络连接失败，请稍后再试',
    RoomCreateError: '参数错误',
    NetDisConnect: '网络断开链接！',
    NetOtherLogin: '您的帐号在其他地方登录',
    IsExitRoom: '是否要离开房间',
    IsDisbanRoom: '是否要解散房间',
    RoomHasDisband: '房间被解散',
    RecordTooShot: '录音时间过短！',
    RecordNotPermision: '未开启录音权限',
    PhoneNumberError: '手机号格式错误',
    RegistError:'注册失败',
    RegistSuccess:'注册成功',
    ResetPsdError:'修改密码失败',
    ResetPsdSuccess:'修改密码注册成功',
    VerifyCodeError:'验证码错误',
    PsdNotMatch:'密码不一致',
    AccountNotNull:'账号不能为空',
    PsdNotNull:'密码不能为空',

    ChooseRefineItem: '请先选择炼化材料',
    SuiPianNotEnough: '神兵碎片不足',
    BaHuangNotEnough: '八荒遗风不足',
    HeChengNotEnough: '合成材料不足',
    YinYaoNotEnough: '引妖香不足',


    AddExp: '获得 %s 经验',
    AddMoney: '获得 %s 银两',

    NotDeveloped: '功能暂未开发，敬请期待',
}

module.exports = StrCommon;