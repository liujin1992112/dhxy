let skillIds = { //技能ID
    NormalAtkSkill: 1001, //普通攻击
	NormalDefSkill: 1002, //防御
	HeDingHongFen: 1003, //鹤顶红粉
	JieDaoShaRen: 1004, //借刀杀人
	MiHunZui: 1005, //迷魂醉
	ZuoBiShangGuan: 1006, //作壁上观
	WanDuGongXin: 1007, //万毒攻心
	ShiXinKuangLuan: 1008, //失心狂乱
	BaiRiMian: 1009, //百日眠
	SiMianChuGe: 1010, //四面楚歌
	LieHuoJiaoYang: 1011, //烈火骄阳
	FengLeiYongDong: 1012, //风雷涌动
	DianShanLeiMing: 1013, //电闪雷鸣
	JiaoLongChuHai: 1014, //蛟龙出海
	JiuYinChunHuo: 1015, //九阴纯火
	XiuLiQianKun: 1016, //袖里乾坤
	TianZhuDiMie: 1017, //天诛地灭
	JiuLongBingFeng: 1018, //九龙冰封
	MoShenHuTi: 1019, //魔神护体
	TianWaiFeiMo: 1020, //天外飞魔
	ShouWangShenLi: 1021, //兽王神力
	XiaoHunShiGu: 1022, //销魂蚀骨
	HanQingMoMo: 1023, //含情脉脉
	QianKunJieSu: 1024, //乾坤借速
	MoShenFuShen: 1025, //魔神附身
	YanLuoZhuiMing: 1026, //阎罗追命
	QinSiBingWu: 1027, //秦思冰雾
	XueShaZhiGu: 1028, //血煞之蛊
	LuoRiRongJin: 1029, //落日熔金
	ShiXinFeng: 1030, //失心疯
	QianNvYouHun: 1031, //倩女幽魂
	XiXingDaFa: 1032, //吸星大法
	XueHaiShenChou: 1033, //血海深仇
	MengPoTang: 1034, //孟婆汤


	ZhangYinDongDu: 2001, //帐饮东都
	YuanQuanWanHu: 2002, //源泉万斛
	ShenGongGuiLi: 2003, //神工鬼力
	BeiDaoJianXing: 2004, //倍道兼行
	PanShan: 2005, //蹒跚
	GongXingTianFa: 2006, // 恭行天罚
	TianGangZhanQi: 2007, // 天罡战气
	XuanRen: 2008, //悬刃
	YiHuan: 2009, //遗患
	ShanXian: 2010, // 闪现
	FeiLongZaiTian: 2011,// 飞龙在天
	FeiLongZaiTian_Feng: 2012, // 飞龙在天 风
	FeiLongZaiTian_Huo: 2013, // 飞龙在天 火
	FeiLongZaiTian_Shui: 2014, // 飞龙在天 水
	FeiLongZaiTian_Lei: 2015, // 飞龙在天 雷

	YouFengLaiYi: 2016,// 有凤来仪
	LiSheDaChuan: 2017, // 利涉大川

	YouFengLaiYi_Jin: 2017,// 有凤来仪 金
	YouFengLaiYi_Mu: 2018,// 有凤来仪 木
	YouFengLaiYi_Shui: 2019,// 有凤来仪 水
	YouFengLaiYi_Huo: 2020,// 有凤来仪 火
	YouFengLaiYi_Tu: 2021,// 有凤来仪 土

	FenLieGongJi: 2030, // 分裂攻击
	TianMoJieTi: 2031,// 天魔解体
	FenGuangHuaYing: 2032, // 分光化影
	QingMianLiaoYa: 2033, // 青面獠牙
	XiaoLouYeKu: 2034, // 小楼夜哭
	GeShanDaNiu: 2035, // 隔山打牛

    HunLuan: 2036, // 混乱
	FengYin: 2037, // 封印


	HighZhangYinDongDu: 2101, //高级帐饮东都
	HighYuanQuanWanHu: 2102, //高级源泉万斛
	HighShenGongGuiLi: 2103, //高级神工鬼力
	HighBeiDaoJianXing: 2104, //高级倍道兼行
	HighPanShan: 2105, //高级蹒跚
	HighShanXian: 2110, // 高级闪现
	YinShen: 2111, // 隐身
	MiaoShouHuiChun: 2112,//妙手回春

	FenHuaFuLiu: 2113,// 分花拂柳


	WuXingXiangKe: 2120, // 五行相克 -- 用来标记五行技能
	ChuiJinZhuanYu: 2121, //炊金馔玉
	KuMuFengChun: 2122, //枯木逢春
	RuRenYinShui: 2123, //如人饮水
	FengHuoLiaoYuan: 2124, //烽火燎原
	XiTianJingTu: 2125, //西天净土

	HighFenLieGongJi: 2130, // 高级分裂攻击
	HighTianMoJieTi: 2131,// 高级天魔解体
	HighFenGuangHuaYing: 2132, // 高级分光化影
	HighQingMianLiaoYa: 2133, // 高级青面獠牙
	HighXiaoLouYeKu: 2134, // 高级小楼夜哭
    HighGeShanDaNiu: 2135, // 高级隔山打牛
    
    JiQiBuYi: 2136, // 击其不意

	ZiXuWuYou: 2700, // 子虚乌有
	HuaWu: 2701, // 化无
	JueJingFengSheng: 2702, // 绝境逢生
	DangTouBangHe: 2703, //当头棒喝
	JiangSi: 2704, //将死
	ZuoNiaoShouSan: 2705, // 作鸟兽散
	ChunHuiDaDi: 2706, // 春回大地
	ShuangGuanQiXia: 2707, // 双管齐下
	MingChaQiuHao: 2708, // 明察秋毫


	BingLinChengXia: 3001, //兵临城下
	NiePan: 3002, //涅槃
	QiangHuaXuanRen: 3003, //强化悬刃
	QiangHuaYiHuan: 3004, //强化遗患
	ChaoMingDianChe: 3005, //潮鸣电掣
	RuHuTianYi: 3006, //如虎添翼

	StealMoney: 8000, // 偷钱，天降灵猴
}

var SkillConfig = {
    EMagicType: {//法系id
        Physics: 0, //物理
        Chaos: 1, //混乱
        Toxin: 2, //毒
        Sleep: 3, //昏睡
        Seal: 4, //封印
        Wind: 5, //风法
        Fire: 6, //火法
        Thunder: 7, //雷法
        Water: 8, //水法
        AtkSkillMax: 9, // 攻击类法术最大值
        Speed: 10, //加速
        Defense: 11, //加防
        Attack: 12, //加攻
        Frighten: 13, //震慑
        ThreeCorpse: 14, //三尸
        Charm: 15, //魅惑
        GhostFire: 16, //鬼火
        Forget: 17, //遗忘
        SubDefense: 18, // 减防
        YinShen: 19,// 隐身
    },



    AffectType: {//技能攻击范围
        Single: 0,           //单攻
        Group: 1,            //群攻
    },

    SkillIds: skillIds,

    // 特效播放位置
    EffectPos: {
        Body: 0,           // 敌人身上
        Stage: 1,           // 场景上
    },

    // 战斗回合类型
    BtlActionType: {
        Initiative: 1, // 主动
        Passive: 2, // 被动
    },

    AttackType: {
        Normal:0, // 默认看技能属性
        Melee: 1,//近身
        Remote: 2,// 远程
    },

    ActOn: {
        All: 0,
        Enemy:1,
        Self:2,
    },

    roleSkillIds: {
        [0]: [skillIds.JieDaoShaRen, skillIds.ShiXinKuangLuan, skillIds.MiHunZui, skillIds.BaiRiMian, skillIds.ZuoBiShangGuan, skillIds.SiMianChuGe],
        [1]: [skillIds.HeDingHongFen, skillIds.WanDuGongXin, skillIds.MiHunZui, skillIds.BaiRiMian, skillIds.ZuoBiShangGuan, skillIds.SiMianChuGe],
        [2]: [skillIds.FengLeiYongDong, skillIds.XiuLiQianKun, skillIds.DianShanLeiMing, skillIds.TianZhuDiMie, skillIds.JiaoLongChuHai, skillIds.JiuLongBingFeng],
        [3]: [skillIds.LieHuoJiaoYang, skillIds.JiuYinChunHuo, skillIds.DianShanLeiMing, skillIds.TianZhuDiMie, skillIds.JiaoLongChuHai, skillIds.JiuLongBingFeng],
        [4]: [skillIds.TianWaiFeiMo, skillIds.QianKunJieSu, skillIds.ShouWangShenLi, skillIds.MoShenFuShen, skillIds.XiaoHunShiGu, skillIds.YanLuoZhuiMing],
        [5]: [skillIds.MoShenHuTi, skillIds.HanQingMoMo, skillIds.ShouWangShenLi, skillIds.MoShenFuShen, skillIds.XiaoHunShiGu, skillIds.YanLuoZhuiMing],
        [6]: [skillIds.XueShaZhiGu, skillIds.XiXingDaFa, skillIds.LuoRiRongJin, skillIds.XueHaiShenChou, skillIds.ShiXinFeng, skillIds.MengPoTang],
        [7]: [skillIds.QinSiBingWu, skillIds.QianNvYouHun, skillIds.LuoRiRongJin, skillIds.XueHaiShenChou, skillIds.ShiXinFeng, skillIds.MengPoTang],
    },

    roleSkillTypes: {
        [0]: [1002, 1003, 1004],
        [1]: [1001, 1003, 1004],
        [2]: [1006, 1007, 1008],
        [3]: [1005, 1007, 1008],
        [4]: [1010, 1011, 1012],
        [5]: [1009, 1011, 1012],
        [6]: [1014, 1015, 1016],
        [7]: [1013, 1015, 1016]

    },

    //升级消耗
    SkillConsume: [
        [1422, 6434],
        [1725, 6740],
        [2031, 7050],
        [2338, 7365],
        [2648, 7684],
        [2961, 8007],
        [3275, 8334],
        [3592, 8665],
        [3911, 9000],
        [4232, 9340],
        [4555, 9684],
        [4881, 10032],
        [5208, 10384],
        [5538, 10740],
        [5871, 11100],
        [6205, 11465],
        [6542, 11834],
        [6881, 12207],
        [7222, 12584],
        [7565, 12965],
        [7911, 13350],
        [8258, 13740],
        [8608, 14134],
        [8961, 14532],
        [9315, 14934],
        [9672, 15340],
        [10031, 15750],
        [10392, 16165],
        [10755, 16584],
        [11121, 17007],
        [11488, 17434],
        [11858, 17865],
        [12231, 18300],
        [12605, 18740],
        [12982, 19184],
        [13361, 19632],
        [13742, 20084],
        [14125, 20540],
        [14511, 21000],
        [14898, 21465],
        [15288, 21934],
        [15681, 22407],
        [16075, 22884],
        [16472, 23365],
        [16871, 23850],
        [17272, 24340],
        [17675, 24834],
        [18081, 25332],
        [18488, 25834],
        [18898, 26340],
        [32358, 51314],
        [33353, 52918],
        [34365, 54553],
        [35393, 56218],
        [36438, 57914],
        [37499, 59640],
        [38576, 61398],
        [39670, 63187],
        [40780, 65006],
        [41908, 66857],
        [43052, 68740],
        [44212, 70653],
        [45390, 72599],
        [46584, 74576],
        [47795, 76584],
        [49024, 78625],
        [50269, 80697],
        [51531, 82802],
        [52811, 84938],
        [54107, 87107],
        [55421, 89308],
        [56753, 91542],
        [58101, 93808],
        [59467, 96106],
        [60850, 98438],
        [62251, 100802],
        [63670, 103199],
        [65106, 105628],
        [66559, 108091],
        [68030, 110587],
        [69519, 113116],
        [71026, 115679],
        [72550, 118274],
        [74092, 120904],
        [75652, 123566],
        [77230, 126263],
        [78826, 128993],
        [80440, 131756],
        [82072, 134554],
        [83723, 137385],
        [85391, 140251],
        [87077, 143150],
        [88782, 146084],
        [90505, 149052],
        [92246, 152054],
        [94005, 155090],
        [95783, 158161],
        [97579, 161266],
        [99394, 164406],
        [101227, 167581],
        [317122, 572122],
        [324114, 584970],
        [331195, 597984],
        [338366, 611167],
        [345626, 624517],
        [352977, 638037],
        [360418, 651726],
        [367949, 665585],
        [375571, 679615],
        [383285, 693815],
        [391090, 708188],
        [398988, 722732],
        [406977, 737449],
        [415058, 752340],
        [423233, 767404],
        [431500, 782643],
        [439861, 798057],
        [448315, 813646],
        [456863, 829410],
        [465505, 845352],
        [474241, 861470],
        [483072, 877766],
        [491998, 894240],
        [501019, 910892],
        [510136, 927723],
        [519348, 944734],
        [528657, 961925],
        [538062, 979296],
        [547563, 996848],
        [557161, 1014582],
        [566856, 1032498],
        [576648, 1050596],
        [586538, 1068877],
        [596526, 1087342],
        [606612, 1105990],
        [616796, 1124823],
        [627079, 1143841],
        [637461, 1163044],
        [647942, 1182433],
        [658522, 1202009],
        [669202, 1221771],
        [679981, 1241720],
        [690861, 1261857],
        [701841, 1282183],
        [712922, 1302696],
        [724103, 1323399],
        [735386, 1344292],
        [746770, 1365374],
        [758255, 1386647],
        [769843, 1408111],
        [781532, 1429766],
        [793324, 1451613],
        [805218, 1473652],
        [817215, 1495884],
        [829315, 1518308],
        [841518, 1540927],
        [853825, 1563739],
        [866235, 1586746],
        [878749, 1609948],
        [891367, 1633345],
        [904090, 1656937],
        [916918, 1680726],
        [929850, 1704711],
        [942887, 1728893],
        [956029, 1753273],
        [969277, 1777851],
        [982631, 1802626],
        [996091, 1827601],
        [1009657, 1852774],
        [1023329, 1878147],
        [1037108, 1903720],
        [1050994, 1929493],
        [1064986, 1955467],
        [1079086, 1981642],
        [1093294, 2008019],
        [1107609, 2034598],
        [1122032, 2061378],
        [1136563, 2088362],
        [1151203, 2115549],
        [1165951, 2142939],
        [1180808, 2170533],
        [1195774, 2198332],
        [1210849, 2226335],
        [1226034, 2254544],
        [1241328, 2282958],
        [1256732, 2311577],
        [1272246, 2340404],
        [1287870, 2369436],
        [1303604, 2398676],
        [1319450, 2428124],
        [1335406, 2457779],
        [1351473, 2487642],
        [1367651, 2517714],
        [1383941, 2547995],
        [1400343, 2578486],
        [1416856, 2609186],
        [1433482, 2640096],
        [1450219, 2671217],
        [1467069, 2702548],
        [1484032, 2734091],
        [1501108, 2765846],
        [1518297, 2797812],
        [1535599, 2829991],
        [1553014, 2862382],
        [1570543, 2894986],
        [1588186, 2927804],
        [1605943, 2960836],
        [1623814, 2994082],
        [1641800, 3027542],
        [1659900, 3061218],
        [1678115, 3095108],
        [1696445, 3129214],
        [1714890, 3163537],
        [1733450, 3198075],
        [1752126, 3232830],
        [1770918, 3267802],
        [1789826, 3302992],
        [1808850, 3338399],
        [1827990, 3374024],
        [1847247, 3409868],
        [1866620, 3445931],
        [1886110, 3482212],
        [1905718, 3518713],
        [1925442, 3555434],
        [1945284, 3592376],
        [1965244, 3629537],
        [1985321, 3666920],
        [2005516, 3704523],
        [2025830, 3742349],
        [2046261, 3780396],
        [2066812, 3818665],
        [2087481, 3857157],
        [2108269, 3895872],
        [2129175, 3934809],
        [2150202, 3973971],
        [2171347, 4013356],
        [2192612, 4052966],
        [2213997, 4092800],
        [2235502, 4132859],
        [2257127, 4173144],
        [2278872, 4213653],
        [2300738, 4254389],
        [2322724, 4295351],
        [2344831, 4336539],
        [2367060, 4377955],
        [2389409, 4419597],
        [2411879, 4461467],
        [2434472, 4503565],
        [2457185, 4545891],
        [2480021, 4588445]
    ]
}


module.exports = SkillConfig;