let skill_same_audio = {
    [2121]: 2121,
    [2122]: 2121,
    [2123]: 2121,
    [2124]: 2121,
    [2125]: 2121,
}

cc.Class({
    extends: cc.Component,

    properties: {
        _bgm: 0,
        _music_volume: 1,
        _effect_volume: 1,
        current: 0,
        _btnClick: null,
    },

    statics: {
        init() {
            this.current = 0;
            let mv = cc.sys.localStorage.getItem('music_volume');
            mv = mv == null ? 1 : mv;
            this._music_volume = parseFloat(mv);
            let ev = cc.sys.localStorage.getItem('effect_volume');
            ev = ev == null ? 1 : ev;
            this._effect_volume = parseFloat(ev);
            let vv = cc.sys.localStorage.getItem('voice_volume');
            vv = vv == null ? 1 : vv;
            this._voice_volume = parseFloat(vv);

            this._audioCache = {};
        },

        play(audio_clip, isbgm) {
            // if (CC_JSB) {
            //     if (isbgm) {
            //         jsb.AudioEngine.stop(Number(this.current));
            //         this.current = jsb.AudioEngine.play2d(url, true, this._music_volume);
            //         return this.current;
            //     } else {
            //         let audioId = jsb.AudioEngine.play2d(url, false, this._effect_volume);
            //         return audioId;
            //     }
            // } else {
            //     if (isbgm) {
            //         cc.audioEngine.stop(this.current);
            //         this.current = cc.audioEngine.play(url, isbgm, this._music_volume);
            //         return this.current;
            //     } else {
            //         return cc.audioEngine.play(url, isbgm, this._effect_volume);
            //     }
            // }3
            if (isbgm) {
                cc.audioEngine.stop(this.current);
                this.current = cc.audioEngine.play(audio_clip, isbgm, this._music_volume);
                return this.current;
            } else {
                return cc.audioEngine.play(audio_clip, isbgm, this._effect_volume);
            }
        },

        stopPlay(effect_id) {
            // if (CC_JSB) {
            //     jsb.AudioEngine.stop(effect_id);
            // } else {
            cc.audioEngine.stop(effect_id);
            // }
        },

        playBgm(bgmpath) {
            if (this._bgm == bgmpath) {
                return;
            }

            let audio = this._audioCache[bgmpath];
            if (audio) {
                this._bgm = bgmpath;
                this.play(audio, true);
            } else {
                let self = this;
                cc.loader.loadRes("Audio/" + bgmpath, cc.AudioClip, function (err, clip) {
                    if (err) {
                        cc.error(err.message || err);
                        return;
                    }
                    self._bgm = bgmpath;
                    self._audioCache[bgmpath] = clip;
                    self.play(clip, true);
                });
            }
        },

        playAudio(path) {
            let audio = this._audioCache[path];
            if (audio) {
                return this.play(audio, false);
            } else {
                let self = this;
                cc.loader.loadRes("Audio/" + path, cc.AudioClip, function (err, clip) {
                    if (err) {
                        cc.error(err.message || err);
                        return;
                    }
                    self._audioCache[path] = clip;
                    return self.play(clip, false);
                });
            }
        },

        setMusicVolume(n) {
            this._music_volume = Math.floor(n * 100) / 100;
            // if (CC_JSB) {
            //     jsb.AudioEngine.setVolume(Number(this.current), Number(this._music_volume));
            // } else {
            cc.audioEngine.setVolume(this.current, this._music_volume);
            // }
            cc.sys.localStorage.setItem('music_volume', this._music_volume);
        },

        getMusicVolume() {
            return this._music_volume;
        },

        setEffectVolume(n) {
            this._effect_volume = Math.floor(n * 100) / 100;
            cc.sys.localStorage.setItem('effect_volume', this._effect_volume);
        },

        getEffectVolume() {
            return this._effect_volume;
        },

        setVoiceVolume(n) {
            this._voice_volume = Math.floor(n * 100) / 100;
            cc.sys.localStorage.setItem('voice_volume', this._voice_volume);
        },

        getVoiceVolume() {
            return this._voice_volume;
        },

        playDefaultBtn() {
            this.playAudio('Common/BtnClick');
        },

        playOutCard() {
            this.playAudio('Common/OutPokers');
        },

        playGetCard() {
            this.playAudio('Common/sendcard');
        },

        playOpenAudio() {
            this.playAudio('ui/ui_dakai');
        },

        playCloseAudio() {
            this.playAudio('ui/ui_guanbi');
        },

        playFenyeAudio() {
            this.playAudio('ui/ui_fenye');
        },

        playSkillAudio(skillId) {
            let audioid = skillId;
            if (skill_same_audio.hasOwnProperty(skillId)) {
                audioid = skill_same_audio[skillId];
            }
            this.playAudio('skill/' + audioid);
        },

        playActAudio() {
            this.playAudio('skill/magic');
        },

        playMagicAudio(resid) {
            let idNum = {
                [1]: [1001],
                [2]: [1002],
                [3]: [2003],
                [4]: [2004],
                [5]: [3005],
                [6]: [3006],
                [7]: [4007],
                [8]: [4008],
            };
            if (resid > 4038) return;
            let idPath = idNum[resid % 10];
            if (null == idPath) return;
            cc.ll.AudioMgr.playAudio('skill/' + idPath + '/magicvo');
        },

        playMapAidio(mapid) {
            if (mapid == 1010) {
                cc.ll.AudioMgr.playBgm('xinshou');
            } else if (mapid == 1011) {
                cc.ll.AudioMgr.playBgm('xinnian');
            } else if (mapid == 1201) {
                cc.ll.AudioMgr.playBgm('haorizi');
            }
        },

        playLocalAudio(path, idcallback, endCallback) {
            if (!CC_JSB) {
                return false;
            }
            let self = this;
            cc.audioEngine.uncache(path);
            cc.loader.load(path, function (err, clip) {
                if (err) {
                    cc.error(err.message || err);
                    idcallback(-1);
                }
                let audio_id = self.play(clip, false);
                if (audio_id <= 0) {
                    idcallback(-1);
                }
                jsb.AudioEngine.setFinishCallback(audio_id, function (id, clip) {
                    if (endCallback) {
                        endCallback()
                    }
                });
                idcallback(audio_id);
            });
            return true;
            // cc.audioEngine.play(path, false);
        },

    },
})