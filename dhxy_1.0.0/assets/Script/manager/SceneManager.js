// SceneManager.js
var SceneMgr = cc.Class({
	statics: {
		changeScene: function (scene) {
			cc.director.preloadScene(scene, function () {
				// scene.sceneName = scene;
				cc.director.loadScene(scene, () => {
					cc.ll.runningSceneName = scene;
					cc.sys.garbageCollect();
				});
			});
		}
	}
});

module.exports = SceneMgr;