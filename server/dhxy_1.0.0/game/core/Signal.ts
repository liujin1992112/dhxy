import Global from "../../game/core/Global";
import PlayerMgr from "../object/PlayerMgr";
import Http from "../../utils/Http";

export default class Signal {
    static shared=new Signal();
    registed:boolean;
    token_list:any;
    constructor() {
        this.registed = false;
        this.token_list = {};
    }

    sendToGate(event:any, data:any, callback:any) {
        if (callback == null){
            callback = ()=>{}
        }
        Http.sendget(Global.serverConfig.GameConfig.GATE_IP, Global.serverConfig.GameConfig.GATE_PORT, event, data, callback);
    }

    ServerRegister() {
        let self = this;
        this.sendToGate('/S_Register', {
            id: Global.serverID,
            name: Global.serverName,
            ip: Global.serverConfig.GameConfig.IP,
            fake: Global.serverConfig.GameConfig.FAKE,
            port: Global.serverConfig.GameConfig.PORT,
            http_port: Global.serverConfig.HTTP.PORT,
        }, (isconnect:any, data:any) => {
            if (isconnect) {
                if (data.result == Global.msgCode.SUCCESS) {
                    self.registed = true;
                    console.log(`游戏服[${Global.serverName}(${Global.serverID})], 已经连接`);

                    for (const accountid in data.tokens) {
                        if (data.tokens.hasOwnProperty(accountid)) {
                            const token = data.tokens[accountid];
                            self.addLoginToken(accountid, token);
                        }
                    }
                }
            }
        });
    }

    update(dt:number) {
        if (dt % (1000 * 15) == 0) {
            if (this.registed == false) {
                this.ServerRegister();
                return;
            }

            this.checkToken();

            this.sendToGate('/S_Ping', {
                id: Global.serverID,
                num: PlayerMgr.shared.getPlayerNum(),
            }, (isconnect:any, data:any) => {
                if (!isconnect) {
                    this.registed = false;
                }
                if (data.result != Global.msgCode.SUCCESS){
                    this.registed = false;
                }
            });
        }
    }

    addLoginToken(accountid:any, token:any){
        let time = new Date();
        let pToken = {
            accountid: accountid,
            token: token,
            islogin: false,
            time: time.getTime(),
        };
        this.token_list[accountid] = pToken;
    }

    checkToken(){
        let time = new Date();
        for (const accountid in this.token_list) {
            if (this.token_list.hasOwnProperty(accountid)) {
                const tokeninfo = this.token_list[accountid];
                if (tokeninfo.islogin == false && time.getTime() - tokeninfo.time > 5 * 60 * 1000){
                    delete this.token_list[accountid];
                }
            }
        }
    }

    DeleteTocken(nAccountID:any)
    {
        if (this.token_list.hasOwnProperty(nAccountID) == false)
            return;

        delete this.token_list[nAccountID];
    }

    getLoginToken(accountid:any){
        if (this.token_list[accountid]){
            return this.token_list[accountid].token
        }
        return 'notoken';
    }
}