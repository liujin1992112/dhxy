/*
 * 开服读取记录玩家充值总额
 */

import PlayerMgr from "../object/PlayerMgr";
import DB from "../../utils/DB";
import Launch from "./launch";

export default class ChargeSum {
	static shared=new ChargeSum();
	rolelist:any;
	constructor() {
		this.rolelist = {};
	}
	/*
	 * 从数据库读取role的总充值金额
	 */
	init() {
		DB.query('SELECT roleid, sum(realmoney) AS rmb FROM charge_record WHERE status = 1 GROUP BY roleid;', (err:any, rows:any) => {
			if (err) {
				throw err;
			}
			if (rows.length != 0) {
				for (let item of rows) {
					if (item.roleid && item.rmb) {
						// this.playerCharge(item.roleid, item.rmb);
						if (this.rolelist[item.roleid]) {
							this.rolelist[item.roleid] += item.rmb;
						} else {
							this.rolelist[item.roleid] = item.rmb;
						}
					}
				}
			}
			console.log('充值模块加载完毕！');
			Launch.shared.complete('chargemgr');
		});
	}

	/*
	 * 玩家充值
	 * @param roleid 玩家角色id
	 * @param charge 充值金额
	 * @param jade 充值仙玉
	 */
	playerCharge(roleid:any, charge:any, jade:any) {
		if (this.rolelist[roleid]) {
			this.rolelist[roleid] += charge;
		} else {
			this.rolelist[roleid] = charge;
		}
		let player = PlayerMgr.shared.getPlayerByRoleId(roleid);
		if (player) {
			player.ChargeSuccess(jade, charge);
		}
	}

	/*
	 * 获取玩家充值总额
	 * @param roleid 玩家角色id
	 */
	getPlayerChargeSum(roleid:any):any{
		return this.rolelist[roleid] || 0;
	}
}