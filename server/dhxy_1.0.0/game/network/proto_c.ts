let c2s = {
    ['test']: (agent:any,data:any) => {
        console.log('socket disconnect');
    },

    // ['ping']: (agent:any,data:any) => {
    //     agent.ping && agent.ping(data);
    // },

    // ['disconnect']: (agent:any,data:any) => {
    //     agent.disconnect(data);
    // },

    ['gm_command']: (agent:any,data:any) => {
        agent.gm_command(data);
    },
    /////////////////////////////////////////////////
    ['c2s_login']: (agent:any,data:any) => {
        agent.c2s_login && agent.c2s_login(data);
    },
    ['c2s_relogin']: (agent:any,data:any) => {
        agent.c2s_relogin && agent.c2s_relogin(data);
    },
    ['c2s_enter_game']: (agent:any,data:any) => {
        agent.c2s_enter_game && agent.c2s_enter_game(data);
    },
    ['c2s_change_map']: (agent:any,data:any) => {
        agent.c2s_change_map && agent.c2s_change_map(data);
    },
    ['c2s_create_team']: (agent:any,data:any) => {
        agent.c2s_create_team && agent.c2s_create_team(data);
    },
    ['c2s_match_team']: (agent:any,data:any) => {
        agent.c2s_match_team && agent.c2s_match_team(data);
    },
    ['c2s_requst_team']: (agent:any,data:any) => {
        agent.c2s_requst_team && agent.c2s_requst_team(data);
    },
    ['c2s_leave_team']: (agent:any,data:any) => {
        agent.c2s_leave_team && agent.c2s_leave_team(data);
    },
    ['c2s_transfer_team']: (agent:any,data:any) => {
        agent.c2s_transfer_team && agent.c2s_transfer_team(data);
    },
    ['c2s_transfer_team_requst']: (agent:any,data:any) => {
        agent.c2s_transfer_team_requst && agent.c2s_transfer_team_requst(data);
    },
    ['c2s_team_list']: (agent:any,data:any) => {
        agent.c2s_team_list && agent.c2s_team_list(data);
    },
    ['c2s_team_requeslist']: (agent:any,data:any) => {
        agent.c2s_team_requeslist && agent.c2s_team_requeslist(data);
    },
    ['c2s_operteam']: (agent:any,data:any) => {
        agent.c2s_operteam && agent.c2s_operteam(data);
    },

    ['c2s_aoi_move']: (agent:any,data:any) => {
        agent.c2s_aoi_move && agent.c2s_aoi_move(data);
    },
    ['c2s_aoi_stop']: (agent:any,data:any) => {
        agent.c2s_aoi_stop && agent.c2s_aoi_stop(data);
    },
    ['c2s_player_upskill']: (agent:any,data:any) => {
        agent.c2s_player_upskill && agent.c2s_player_upskill(data);
    },
    ['c2s_player_addpoint']: (agent:any,data:any) => {
        agent.c2s_player_addpoint && agent.c2s_player_addpoint(data);
    },
    ['c2s_xiulian_point']: (agent:any,data:any) => {
        agent.c2s_xiulian_point && agent.c2s_xiulian_point(data);
    },
    ['c2s_xiulian_upgrade']: (agent:any,data:any) => {
        agent.c2s_xiulian_upgrade && agent.c2s_xiulian_upgrade(data);
    },
    ['c2s_game_chat']: (agent:any,data:any) => {
        agent.c2s_game_chat && agent.c2s_game_chat(data);
    },
    ['c2s_get_friends']: (agent:any,data:any) => {
        agent.c2s_get_friends && agent.c2s_get_friends(data);
    },
    ['c2s_update_friends']: (agent:any,data:any) => {
        agent.c2s_update_friends && agent.c2s_update_friends(data);
    },
    ['c2s_search_friends']: (agent:any,data:any) => {
        agent.c2s_search_friends && agent.c2s_search_friends(data);
    },
    ['c2s_add_friend']: (agent:any,data:any) => {
        agent.c2s_add_friend && agent.c2s_add_friend(data);
    },
    ['c2s_friend_chat']: (agent:any,data:any) => {
        agent.c2s_friend_chat && agent.c2s_friend_chat(data);
    },

    ['c2s_ask_partner_list']: (agent:any,data:any) => {
        agent.QueryPartner && agent.QueryPartner(data.nRoleID);
    },

    ['c2s_change_partner_state']: (agent:any,data:any) => {
        agent.ChangePartnerState && agent.ChangePartnerState(data);
    },

    ['c2s_partner_relive']: (agent:any,data:any) => {
        agent.PartnerRelive && agent.PartnerRelive(data);
    },

    ['c2s_partner_exchange_exp']: (agent:any,data:any) => {
        agent.PartnerExchangeExp && agent.PartnerExchangeExp(data);
    },

    ['c2s_ask_other_info']: (agent:any,data:any) => {
        if (agent.QueryOther != null)
            agent.QueryOther(data.nRoleID);
    },

    ['c2s_ask_role_task']: (agent:any,data:any) => {
        if (agent.QueryRoleTask != null)
            agent.QueryRoleTask();
    },

    ['c2s_create_test_npc']: (agent:any,data:any) => {
    },

    ['c2s_start_grop_task']: (agent:any,data:any) => {
        if (agent.StartGropTask != null)
            agent.StartGropTask(data.nNpcOnlyID, data.nTaskGrop);
    },


    ['c2s_incept_fuben_task']: (agent:any,data:any) => {

        if (agent.InceptFuBenTask != null)
            agent.InceptFuBenTask(data.nNpcOnlyID, data.nTaskID);
    },

    ['c2s_player_shutup']: (agent:any,data:any) => {
        agent.c2s_player_shutup && agent.c2s_player_shutup(data.nRoleID);
    },

    ['c2s_player_speak']: (agent:any,data:any) => {
        agent.c2s_player_speak && agent.c2s_player_speak(data.nRoleID);
    },

    ['c2s_kick_off']: (agent:any,data:any) => {
        agent.KickOffPlayer && agent.KickOffPlayer(data.nRoleID);
    },

    ['c2s_freeze_ip']: (agent:any,data:any) => {
        agent.FreezePlayerIP && agent.FreezePlayerIP(data.nRoleID);
    },

    ['c2s_freeze_mac']: (agent:any,data:any) => {
        agent.FreezePlayerMAC && agent.FreezePlayerMAC(data.nRoleID);
    },


    ['c2s_task_reset']: (agent:any,data:any) => {
        if (agent.TaskReset != null)
            agent.TaskReset();
    },


    ['c2s_abort_task']: (agent:any,data:any) => {
        if (agent.AbortTask != null)
            agent.AbortTask(data);
    },




    ['c2s_task_talk_npc']: (agent:any,data:any) => {
        if (agent.OnTaskTalkNpc != null)
            agent.OnTaskTalkNpc(data.nTaskID, data.nStep, data.nNpcConfigID, data.nNpcOnlyID);
    },

    ['c2s_trigle_npc_bomb']: (agent:any,data:any) => {
        if (agent.TrigleNpcBomb != null)
            agent.TrigleNpcBomb(data.nNpcConfigID, data.nNpcOnlyID);
    },

    ['c2s_ask_daily_info']: (agent:any,data:any) => {
        agent.AskDailyInfo && agent.AskDailyInfo();
    },

    ['c2s_take_active_prize']: (agent:any,data:any) => {
        agent.TakeActivePrize && agent.TakeActivePrize(data.nIndex);
    },


    ['c2s_enter_battle']: (agent:any,data:any) => {
        if (agent.PlayerEnterBattle != null)
            agent.PlayerEnterBattle(data.nGroupID);
    },

    ['c2s_challenge_npc']: (agent:any,data:any) => {
        if (agent.PlayerChallengeNpc != null)
            agent.PlayerChallengeNpc(data.nOnlyID, data.nConfigID);
    },

    ['c2s_act_npc']: (agent:any,data:any) => {
        if (agent.OnRoleActNpc != null)
            agent.OnRoleActNpc(data.nOnlyID, data.nNpcConfigID);
    },

    ['c2s_role_action']: (agent:any,data:any) => {
        if (agent.OnRoleAction != null)
            agent.OnRoleAction(data);
    },



    ['c2s_ask_paihang']: (agent:any,data:any) => {
        if (agent.QueryPaiHang != null)
            agent.QueryPaiHang(data.nByWhat);
    },

    ['c2s_get_shop_items']: (agent:any,data:any) => {
        agent.QueryItemGoods && agent.QueryItemGoods(data);
    },

    ['c2s_buymall_items']: (agent:any,data:any) => {
        agent.BuyMall && agent.BuyMall(data);
    },

    ['c2s_get_shop_equips']: (agent:any,data:any) => {
        agent.QueryEquipGoods && agent.QueryEquipGoods(data);
    },


    ['c2s_ask_roles_goods']: (agent:any,data:any) => {
        agent.QueryAndSendRolsGoods && agent.QueryAndSendRolsGoods(data.nRoleID);
    },

    ['c2s_add_goods']: (agent:any,data:any) => {
        agent.AddGoods && agent.AddGoods(data);
    },

    ['c2s_take_back_goods']: (agent:any,data:any) => {
        agent.TakeBackGoods && agent.TakeBackGoods(data);
    },

    ['c2s_buy_goods']: (agent:any,data:any) => {
        agent.BuyGoods && agent.BuyGoods(data);
    },

    ['c2s_buy_from_npc']: (agent:any,data:any) => {
        agent.BuyFromNpc && agent.BuyFromNpc(data.nConfigID, data.nItemID, data.nCnt);
    },



    ['c2s_get_bagitem']: (agent:any,data:any) => {
        agent.c2s_get_bagitem && agent.c2s_get_bagitem(data);
    },
    ['c2s_get_mall']: (agent:any,data:any) => {
        agent.c2s_get_mall && agent.c2s_get_mall(data);
    },

    ['c2s_ask_relive_list']: (agent:any,data:any) => {
        agent.c2s_ask_relive_list && agent.c2s_ask_relive_list(data);
    },

    ['c2s_change_relive_list']: (agent:any,data:any) => {
        agent.c2s_change_relive_list && agent.c2s_change_relive_list(data);
    },


    ['c2s_compose']: (agent:any,data:any) => {
        agent.c2s_compose && agent.c2s_compose(data);
    },


    ['c2s_ask_lottery_info']: (agent:any,data:any) => {
        agent.c2s_ask_lottery_info && agent.c2s_ask_lottery_info(data);
    },


    ['c2s_lottery_go']: (agent:any,data:any) => {
        agent.c2s_lottery_go && agent.c2s_lottery_go(data);
    },


    ['c2s_ask_npc_shop_item']: (agent:any,data:any) => {
        agent.c2s_ask_npc_shop_item && agent.c2s_ask_npc_shop_item(data);
    },
    ['c2s_use_bagitem']: (agent:any,data:any) => {
        agent.c2s_use_bagitem && agent.c2s_use_bagitem(data);
    },
    ['c2s_stop_incense']: (agent:any,data:any) => {
        agent.c2s_stop_incense && agent.c2s_stop_incense(data);
    },
    ['c2s_get_lockeritem']: (agent:any,data:any) => {
        agent.c2s_get_lockeritem && agent.c2s_get_lockeritem(data);
    },
    ['c2s_update_bagitem']: (agent:any,data:any) => {
        agent.c2s_update_bagitem && agent.c2s_update_bagitem(data);
    },
    ['c2s_update_lockeritem']: (agent:any,data:any) => {
        agent.c2s_update_lockeritem && agent.c2s_update_lockeritem(data);
    },
    ['c2s_createbang']: (agent:any,data:any) => {
        agent.c2s_createbang && agent.c2s_createbang(data);
    },
    ['c2s_operbang']: (agent:any,data:any) => {
        agent.c2s_operbang && agent.c2s_operbang(data);
    },
    ['c2s_requestbang']: (agent:any,data:any) => {
        agent.c2s_requestbang && agent.c2s_requestbang(data);
    },
    ['c2s_joinbang']: (agent:any,data:any) => {
        agent.c2s_joinbang && agent.c2s_joinbang(data);
    },
    ['c2s_leavebang']: (agent:any,data:any) => {
        agent.c2s_leavebang && agent.c2s_leavebang(data);
    },
    ['c2s_getbanglist']: (agent:any,data:any) => {
        agent.c2s_getbanglist && agent.c2s_getbanglist(data);
    },
    ['c2s_getbangrequest']: (agent:any,data:any) => {
        agent.c2s_getbangrequest && agent.c2s_getbangrequest(data);
    },
    ['c2s_getbanginfo']: (agent:any,data:any) => {
        agent.c2s_getbanginfo && agent.c2s_getbanginfo(data);
    },
    ['c2s_searchbang']: (agent:any,data:any) => {
        agent.c2s_searchbang && agent.c2s_searchbang(data);
    },
    ['c2s_bang_bid']: (agent:any,data:any) => {
        agent.c2s_bang_bid && agent.c2s_bang_bid(data);
    },
    ['c2s_relive_pet']: (agent:any,data:any) => {
        agent.c2s_relive_pet && agent.c2s_relive_pet(data);
    },
    ['c2s_wash_petproperty']: (agent:any,data:any) => {
        agent.c2s_wash_petproperty && agent.c2s_wash_petproperty(data);
    },
    ['c2s_save_petproperty']: (agent:any,data:any) => {
        agent.c2s_save_petproperty && agent.c2s_save_petproperty(data);
    },
    ['c2s_charge_reward']: (agent:any,data:any) => {
        agent.c2s_charge_reward && agent.c2s_charge_reward(data);
    },
    ['c2s_hecheng_pet']: (agent:any,data:any) => {
        agent.c2s_hecheng_pet && agent.c2s_hecheng_pet(data);
    },
    ['c2s_create_pet']: (agent:any,data:any) => {
        agent.c2s_create_pet && agent.c2s_create_pet(data);
    },
    ['c2s_get_petlist']: (agent:any,data:any) => {
        agent.c2s_get_petlist && agent.c2s_get_petlist(data);
    },
    ['c2s_change_pet']: (agent:any,data:any) => {
        agent.c2s_change_pet && agent.c2s_change_pet(data);
    },
    ['c2s_update_pet']: (agent:any,data:any) => {
        agent.c2s_update_pet && agent.c2s_update_pet(data);
    },
    ['c2s_level_reward']: (agent:any,data:any) => {
        agent.c2s_level_reward && agent.c2s_level_reward(data);
    },

    ['c2s_del_pet']: (agent:any,data:any) => {
        agent.c2s_del_pet && agent.c2s_del_pet(data);
    },
    ['c2s_pet_forgetskill']: (agent:any,data:any) => {
        agent.c2s_pet_forgetskill(data);
    },
    ['c2s_pet_lockskill']: (agent:any,data:any) => {
        agent.c2s_pet_lockskill(data);
    },
    
    ['c2s_pet_changeSskill']: (agent:any,data:any) => {
        agent.c2s_pet_changeSskill && agent.c2s_pet_changeSskill(data);
    },

    ['c2s_creat_equip']: (agent:any,data:any) => {
        agent.c2s_creat_equip && agent.c2s_creat_equip(data);
    },
    ['c2s_equip_list']: (agent:any,data:any) => {
        agent.c2s_equip_list && agent.c2s_equip_list(data);
    },
    ['c2s_equip_info']: (agent:any,data:any) => {
        agent.c2s_equip_info && agent.c2s_equip_info(data);
    },
    ['c2s_next_equip']: (agent:any,data:any) => {
        agent.c2s_next_equip && agent.c2s_next_equip(data);
    },
    ['c2s_equip_update']: (agent:any,data:any) => {
        agent.c2s_equip_update && agent.c2s_equip_update(data);
    },
    ['c2s_equip_upgrade']: (agent:any,data:any) => {
        agent.c2s_equip_upgrade && agent.c2s_equip_upgrade(data);
    },
    ['c2s_equip_inlay']: (agent:any,data:any) => {
        agent.c2s_equip_inlay && agent.c2s_equip_inlay(data);
    },
    ['c2s_equip_refine']: (agent:any,data:any) => {
        agent.c2s_equip_refine && agent.c2s_equip_refine(data);
    },
    ['c2s_equip_recast']: (agent:any,data:any) => {
        agent.c2s_equip_recast && agent.c2s_equip_recast(data);
    },
    ['c2s_xianqi_list']: (agent:any,data:any) => {
        agent.c2s_xianqi_list && agent.c2s_xianqi_list(data);
    },
    ['c2s_shenbing_upgrade']: (agent:any,data:any) => {
        agent.c2s_shenbing_upgrade && agent.c2s_shenbing_upgrade(data);
    },
    ['c2s_xianqi_upgrade']: (agent:any,data:any) => {
        agent.c2s_xianqi_upgrade && agent.c2s_xianqi_upgrade(data);
    },

    ['c2s_btl']: (agent:any,data:any) => {
        agent.c2s_btl && agent.c2s_btl(data);
    },

    ['c2s_btl_auto']: (agent:any,data:any) => {
        agent.c2s_btl_auto && agent.c2s_btl_auto(data);
    },

    ['c2s_btl_act']: (agent:any,data:any) => {
        agent.c2s_btl_act && agent.c2s_btl_act(data);
    },

    ['c2s_mall_buy']: (agent:any,data:any) => {
        agent.c2s_mall_buy(data);
    },
    // ['c2s_Get_WX']: (agent) => {
    //     agent.c2s_Get_WX();
    // },

    ['c2s_relive']: (agent:any,data:any) => {
        agent.c2s_relive(data);
    },

    ['c2s_changerace']: (agent:any,data:any) => {
        agent.c2s_changerace(data);
    },

    ['c2s_changename']: (agent:any,data:any) => {
        agent.c2s_changename(data);
    },

    ['c2s_pk']: (agent:any,data:any) => {
        agent.c2s_pk(data);
    },

    ['c2s_hongbao_open']: (agent:any,data:any) => {
        agent.c2s_hongbao_open(data);
    },
    ['c2s_shuilu_sign']: (agent:any,data:any) => {
        agent.c2s_shuilu_sign(data);
    },

    ['c2s_shuilu_unsign']: (agent:any,data:any) => {
        agent.c2s_shuilu_unsign(data);
    },

    ['c2s_resetgift']: (agent:any,data:any) => {
        agent.c2s_resetgift(data);
    },
    ['c2s_remunerate']: (agent:any,data:any) => {
        agent.c2s_remunerate(data);
    },

    ['c2s_getgift_info']: (agent:any,data:any) => {
        agent.c2s_getgift_info(data);
    },
    ['c2s_shuilu_info']: (agent:any,data:any) => {
        agent.c2s_shuilu_info(data);
    },

    ['c2s_world_reward']:(agent:any,data:any)=>{
        agent.c2s_world_reward(data);
    },
    ['c2s_world_reward_list']:(agent:any,data:any)=>{
        agent.c2s_world_reward_list(data);
    },
    ['c2s_world_reward_open']:(agent:any,data:any)=>{
        agent.c2s_world_reward_open(data);
    },

    ['c2s_title_change']:(agent:any,data:any)=>{
        agent.c2s_title_change(data);
    },

    ['c2s_title_info']:(agent:any,data:any)=>{
        agent.c2s_title_info(data);
    },

    ['c2s_title_custom']:(agent:any,data:any)=>{
        agent.c2s_titls_custom(data);
    },

    ['c2s_linghou_fight']:(agent:any,data:any)=>{
        agent.c2s_linghou_fight(data);
    },

    ['c2s_palace_fight']:(agent:any,data:any)=>{
        agent.c2s_palace_fight(data);
    },

    ['c2s_palace_agree']:(agent:any,data:any)=>{
        agent.c2s_palace_agree(data);
    },

    ['c2s_palace_rolelist']:(agent:any,data:any)=>{
        agent.c2s_palace_rolelist(data);
    },
    
    ['c2s_relation_new']:(agent:any,data:any)=>{
        agent.c2s_relation_new(data);
    },

    ['c2s_relation_agree']:(agent:any,data:any)=>{
        agent.c2s_relation_agree(data);
    },

    ['c2s_relation_List']:(agent:any,data:any)=>{
        agent.c2s_relation_List(data);
    },
    
    ['c2s_relation_leave']:(agent:any,data:any)=>{
        agent.c2s_relation_leave(data);
    },
    
    ['c2s_relation_add']:(agent:any,data:any)=>{
        agent.c2s_relation_add(data);
    },

    ['c2s_relation_reject']:(agent:any,data:any)=>{
        agent.c2s_relation_reject(data);
    },

	['c2s_change_role_color']:(agent:any,data:any)=>{
		agent.c2s_change_role_color(data);
	},

    ['c2s_scheme_create']:(agent:any,data:any)=>{
        agent.c2s_scheme_create(data);
    },
    
    ['c2s_scheme_List']:(agent:any,data:any)=>{
        agent.c2s_scheme_List(data);
    },

    ['c2s_scheme_info']:(agent:any,data:any)=>{
        agent.c2s_scheme_info(data);
    },
   
    ['c2s_scheme_updateEquip']:(agent:any,data:any)=>{
        agent.c2s_scheme_updateEquip(data);
    },

    ['c2s_scheme_addCustomPoint']:(agent:any,data:any)=>{
        agent.c2s_scheme_addCustomPoint(data);
    },

    ['c2s_scheme_addXiulianPoint']:(agent:any,data:any)=>{
        agent.c2s_scheme_addXiulianPoint(data);
    },

    ['c2s_scheme_resetXiulianPoint']:(agent:any,data:any)=>{
        agent.c2s_scheme_resetXiulianPoint(data);
    },

    ['c2s_scheme_changePartner']:(agent:any,data:any)=>{
        agent.c2s_scheme_changePartner(data);
    }, 

    ['c2s_scheme_activate']:(agent:any,data:any)=>{
        agent.c2s_scheme_activate(data);
    },

    ['c2s_scheme_changeName']:(agent:any,data:any)=>{
        agent.c2s_scheme_changeName(data);
    },

    ['c2s_scheme_use']:(agent:any,data:any)=>{
        agent.c2s_scheme_use(data);
    },

    ['c2s_bell_msg']:(agent:any,data:any)=>{
        agent.c2s_bell_msg(data);
    },

    ['c2s_safepass_msg']:(agent:any,data:any)=>{
        agent.c2s_safepass_msg(data);
    },

    ['c2s_petfly_msg']:(agent:any,data:any)=>{
        agent.c2s_petfly_msg(data);
    },

}

module.exports = c2s;
// module.exports = (agent) => {
// 	for (const event of Object.keys(c2s)) {
// 		let func = c2s[event];
// 		agent.socket.on(event, buffer => {
// 			let data = null;
// 			if(c2s[event]){
// 				let pack = new packet(event);
// 				data = pack.todata(buffer);
// 			}
// 			try {
// 				func(agent:any,data:any);
// 				agent.ping(false);
// 			} catch (error) {
// 				console.error('proto_c func Error Catch!');
// 				console.error(error.stack);
// 			}
// 		});
// 	}
// }
