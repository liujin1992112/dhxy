// 游戏配置表

exports.INFO={
    SERVER_TYPE:"game",
    AREA_ID:1,
    SERVER_ID:1000,
    SERVER_NAME:"测试区",
}

exports.DB = {
    DB_PORT: 8806,
};

exports.CLI = {
    PORT: 8812,
};

exports.GameConfig = {
    HOST: "127.0.0.1",
    IP: "127.0.0.1",
    FAKE: "127.0.0.1",
    PORT: 8810,
    GATE_IP: "127.0.0.1",
    GATE_PORT: 8560,
};

exports.HTTP = {
    HOST: "127.0.0.1",
    IP: "127.0.0.1",
    PORT: 8811,
};

