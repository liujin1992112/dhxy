客户端修改
	配置文件/assets/resources/config/config.json
	修改ip字段为服务器ip

服务器配置
	安装mysql 5.7 版本
		mysql导入服务器目录下 xy_game.sql文件
	安装nodejs 10.x 版本
	
	修改配置文件 /etc/config.js 
		修改 NET_IP 字段为服务器公网ip字段（如果存在内网配置内网ip字段）
		如需分布式配置，修改GATE_IP为网关ip

	修改数据库配置文件 /etc/db_config.js
		修改mysql对应的服务器ip 以及 数据库账号密码

	修改网关配置文件 /gate/gate_config.js
		修改GATE_IP 为网关服所在的服务器ip

	
