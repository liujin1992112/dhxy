/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50726
Source Host           : localhost:3306
Source Database       : xy_game

Target Server Type    : MYSQL
Target Server Version : 50726
File Encoding         : 65001

Date: 2020-05-31 13:04:12
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for charge_record
-- ----------------------------
DROP TABLE IF EXISTS `charge_record`;
CREATE TABLE `charge_record` (
  `orderid` varchar(20) DEFAULT NULL COMMENT '订单id',
  `roleid` int(10) NOT NULL DEFAULT '0' COMMENT '角色id',
  `money` int(11) DEFAULT '0' COMMENT '充值金额',
  `realmoney` int(11) DEFAULT '0' COMMENT '真实充值金额',
  `jade` int(11) DEFAULT '0' COMMENT '仙玉',
  `goodsid` int(11) DEFAULT '0' COMMENT '物品id',
  `goodscount` int(11) DEFAULT '0' COMMENT '数量',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `finish_time` datetime DEFAULT NULL COMMENT '完成时间',
  `serverid` int(10) NOT NULL DEFAULT '0',
  `status` int(11) DEFAULT '0' COMMENT '状态'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for ip_frozen
-- ----------------------------
DROP TABLE IF EXISTS `ip_frozen`;
CREATE TABLE `ip_frozen` (
  `frozenid` int(10) NOT NULL AUTO_INCREMENT COMMENT '订单id',
  `accountid` int(10) NOT NULL,
  `frozenip` varchar(20) NOT NULL DEFAULT '' COMMENT 'ip地址',
  `frozentime` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`frozenid`,`frozenip`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for mac_frozen
-- ----------------------------
DROP TABLE IF EXISTS `mac_frozen`;
CREATE TABLE `mac_frozen` (
  `frozenid` int(10) NOT NULL AUTO_INCREMENT COMMENT '订单id',
  `accountid` int(10) NOT NULL,
  `mac` varchar(64) NOT NULL DEFAULT '' COMMENT 'ip地址',
  `frozentime` datetime NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`frozenid`,`mac`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for qy_account
-- ----------------------------
DROP TABLE IF EXISTS `qy_account`;
CREATE TABLE `qy_account` (
  `accountid` int(10) NOT NULL AUTO_INCREMENT,
  `account` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT '',
  `invite` varchar(255) NOT NULL DEFAULT '' COMMENT '邀请码',
  `phone` varchar(255) DEFAULT '0',
  `location` varchar(255) DEFAULT '0',
  `last_login_time` datetime DEFAULT NULL,
  `login_ip` varchar(255) DEFAULT '',
  `register_time` datetime DEFAULT NULL,
  `state` int(10) DEFAULT '0',
  `safecode` varchar(255) DEFAULT NULL COMMENT '安全码',
  `mac` varchar(255) DEFAULT '' COMMENT '计算地址',
  PRIMARY KEY (`accountid`,`account`)
) ENGINE=InnoDB AUTO_INCREMENT=39375 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for qy_agent
-- ----------------------------
DROP TABLE IF EXISTS `qy_agent`;
CREATE TABLE `qy_agent` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT '递增id',
  `account` varchar(255) DEFAULT NULL COMMENT '代理帐号',
  `password` varchar(255) DEFAULT NULL COMMENT '代理密码',
  `name` varchar(255) DEFAULT NULL COMMENT '代理名字',
  `addtime` int(11) DEFAULT '0' COMMENT '添加时间戳',
  `invitecode` varchar(255) DEFAULT NULL COMMENT '邀请码',
  `state` int(1) DEFAULT '0' COMMENT '状态 0正常 1关闭',
  `is_auth` tinyint(1) DEFAULT '0' COMMENT '是否授权',
  `pid` int(11) DEFAULT '0' COMMENT '父级ID',
  PRIMARY KEY (`id`),
  KEY `is_auth` (`is_auth`,`pid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for qy_bang
-- ----------------------------
DROP TABLE IF EXISTS `qy_bang`;
CREATE TABLE `qy_bang` (
  `bangid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '帮派名字',
  `aim` varchar(255) DEFAULT NULL COMMENT '帮派宗旨',
  `rolenum` int(10) DEFAULT '0' COMMENT '人数',
  `masterid` int(11) DEFAULT NULL COMMENT '帮主id',
  `mastername` varchar(255) DEFAULT NULL COMMENT '帮主名字',
  `createtime` datetime DEFAULT NULL COMMENT '创建时间',
  `state` int(10) DEFAULT '1' COMMENT '1正常帮派，0解散帮派',
  `serverid` int(10) DEFAULT '0' COMMENT '服务器id',
  `bidding` int(10) DEFAULT '0' COMMENT '帮派权重值',
  PRIMARY KEY (`bangid`)
) ENGINE=InnoDB AUTO_INCREMENT=110530002 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for qy_equip_1000
-- ----------------------------
DROP TABLE IF EXISTS `qy_equip_1000`;
CREATE TABLE `qy_equip_1000` (
  `EquipID` int(10) NOT NULL AUTO_INCREMENT,
  `EquipType` int(10) DEFAULT '0' COMMENT '装备类型，0:新手装备，1:高级装备，2:神兵，3:仙器',
  `RoleID` int(10) NOT NULL DEFAULT '0' COMMENT '装备拥有者id',
  `BaseAttr` varchar(255) DEFAULT NULL COMMENT '基础属性',
  `Grade` int(10) DEFAULT '0' COMMENT '装备等级',
  `EIndex` int(10) DEFAULT '0' COMMENT '装备位置',
  `Shuxingxuqiu` varchar(255) DEFAULT NULL COMMENT '属性需求',
  `Type` int(10) DEFAULT '0' COMMENT '装备编号',
  `GemCnt` int(10) DEFAULT '0' COMMENT '宝石镶嵌数量',
  `LianhuaAttr` varchar(255) DEFAULT NULL COMMENT '炼化属性',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `delete_time` datetime DEFAULT NULL COMMENT '删除时间',
  `state` int(10) DEFAULT '1' COMMENT '状态0删除',
  `name` varchar(255) DEFAULT NULL COMMENT '装备名字',
  `pos` int(255) DEFAULT NULL COMMENT '装备存放位置',
  PRIMARY KEY (`EquipID`,`RoleID`)
) ENGINE=InnoDB AUTO_INCREMENT=1560 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for qy_equip_1001
-- ----------------------------
DROP TABLE IF EXISTS `qy_equip_1001`;
CREATE TABLE `qy_equip_1001` (
  `EquipID` int(10) NOT NULL AUTO_INCREMENT,
  `EquipType` int(10) DEFAULT '0' COMMENT '装备类型，0:新手装备，1:高级装备，2:神兵，3:仙器',
  `RoleID` int(10) NOT NULL DEFAULT '0' COMMENT '装备拥有者id',
  `BaseAttr` varchar(255) DEFAULT NULL COMMENT '基础属性',
  `Grade` int(10) DEFAULT '0' COMMENT '装备等级',
  `EIndex` int(10) DEFAULT '0' COMMENT '装备位置',
  `Shuxingxuqiu` varchar(255) DEFAULT NULL COMMENT '属性需求',
  `Type` int(10) DEFAULT '0' COMMENT '装备编号',
  `GemCnt` int(10) DEFAULT '0' COMMENT '宝石镶嵌数量',
  `LianhuaAttr` varchar(255) DEFAULT NULL COMMENT '炼化属性',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `delete_time` datetime DEFAULT NULL COMMENT '删除时间',
  `state` int(10) DEFAULT '1' COMMENT '状态0删除',
  PRIMARY KEY (`EquipID`,`RoleID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for qy_equip_1002
-- ----------------------------
DROP TABLE IF EXISTS `qy_equip_1002`;
CREATE TABLE `qy_equip_1002` (
  `EquipID` int(10) NOT NULL AUTO_INCREMENT,
  `EquipType` int(10) DEFAULT '0' COMMENT '装备类型，0:新手装备，1:高级装备，2:神兵，3:仙器',
  `RoleID` int(10) NOT NULL DEFAULT '0' COMMENT '装备拥有者id',
  `BaseAttr` varchar(255) DEFAULT NULL COMMENT '基础属性',
  `Grade` int(10) DEFAULT '0' COMMENT '装备等级',
  `EIndex` int(10) DEFAULT '0' COMMENT '装备位置',
  `Shuxingxuqiu` varchar(255) DEFAULT NULL COMMENT '属性需求',
  `Type` int(10) DEFAULT '0' COMMENT '装备编号',
  `GemCnt` int(10) DEFAULT '0' COMMENT '宝石镶嵌数量',
  `LianhuaAttr` varchar(255) DEFAULT NULL COMMENT '炼化属性',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `delete_time` datetime DEFAULT NULL COMMENT '删除时间',
  `state` int(10) DEFAULT '1' COMMENT '状态0删除',
  PRIMARY KEY (`EquipID`,`RoleID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for qy_equip_1003
-- ----------------------------
DROP TABLE IF EXISTS `qy_equip_1003`;
CREATE TABLE `qy_equip_1003` (
  `EquipID` int(10) NOT NULL AUTO_INCREMENT,
  `EquipType` int(10) DEFAULT '0' COMMENT '装备类型，0:新手装备，1:高级装备，2:神兵，3:仙器',
  `RoleID` int(10) NOT NULL DEFAULT '0' COMMENT '装备拥有者id',
  `BaseAttr` varchar(255) DEFAULT NULL COMMENT '基础属性',
  `Grade` int(10) DEFAULT '0' COMMENT '装备等级',
  `EIndex` int(10) DEFAULT '0' COMMENT '装备位置',
  `Shuxingxuqiu` varchar(255) DEFAULT NULL COMMENT '属性需求',
  `Type` int(10) DEFAULT '0' COMMENT '装备编号',
  `GemCnt` int(10) DEFAULT '0' COMMENT '宝石镶嵌数量',
  `LianhuaAttr` varchar(255) DEFAULT NULL COMMENT '炼化属性',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `delete_time` datetime DEFAULT NULL COMMENT '删除时间',
  `state` int(10) DEFAULT '1' COMMENT '状态0删除',
  PRIMARY KEY (`EquipID`,`RoleID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for qy_equip_1004
-- ----------------------------
DROP TABLE IF EXISTS `qy_equip_1004`;
CREATE TABLE `qy_equip_1004` (
  `EquipID` int(10) NOT NULL AUTO_INCREMENT,
  `EquipType` int(10) DEFAULT '0' COMMENT '装备类型，0:新手装备，1:高级装备，2:神兵，3:仙器',
  `RoleID` int(10) NOT NULL DEFAULT '0' COMMENT '装备拥有者id',
  `BaseAttr` varchar(255) DEFAULT NULL COMMENT '基础属性',
  `Grade` int(10) DEFAULT '0' COMMENT '装备等级',
  `EIndex` int(10) DEFAULT '0' COMMENT '装备位置',
  `Shuxingxuqiu` varchar(255) DEFAULT NULL COMMENT '属性需求',
  `Type` int(10) DEFAULT '0' COMMENT '装备编号',
  `GemCnt` int(10) DEFAULT '0' COMMENT '宝石镶嵌数量',
  `LianhuaAttr` varchar(255) DEFAULT NULL COMMENT '炼化属性',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `delete_time` datetime DEFAULT NULL COMMENT '删除时间',
  `state` int(10) DEFAULT '1' COMMENT '状态0删除',
  PRIMARY KEY (`EquipID`,`RoleID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for qy_friends
-- ----------------------------
DROP TABLE IF EXISTS `qy_friends`;
CREATE TABLE `qy_friends` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `roleidA` int(10) NOT NULL,
  `nameA` varchar(255) NOT NULL,
  `residA` int(10) NOT NULL DEFAULT '0',
  `reliveA` int(10) NOT NULL DEFAULT '0',
  `levelA` int(10) NOT NULL DEFAULT '0',
  `raceA` int(10) NOT NULL,
  `sexA` int(10) NOT NULL,
  `accountidA` int(10) NOT NULL,
  `roleidB` int(10) NOT NULL,
  `nameB` varchar(255) NOT NULL,
  `residB` int(10) NOT NULL DEFAULT '0',
  `reliveB` int(10) NOT NULL DEFAULT '0',
  `levelB` int(10) NOT NULL DEFAULT '0',
  `raceB` int(10) NOT NULL,
  `sexB` int(10) NOT NULL,
  `accountidB` int(10) NOT NULL,
  `state` int(10) NOT NULL DEFAULT '0',
  `time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for qy_info
-- ----------------------------
DROP TABLE IF EXISTS `qy_info`;
CREATE TABLE `qy_info` (
  `comment` varchar(2048) CHARACTER SET utf8mb4 DEFAULT '' COMMENT '公告内容',
  `guideid` int(10) DEFAULT '0' COMMENT '向导服',
  `shuilusid` int(11) DEFAULT '1' COMMENT '水路大会赛季'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for qy_notice
-- ----------------------------
DROP TABLE IF EXISTS `qy_notice`;
CREATE TABLE `qy_notice` (
  `text` varchar(255) DEFAULT '' COMMENT '广播信息',
  `type` int(10) DEFAULT '0' COMMENT '1走马灯 2聊天框 3走马灯+聊天框',
  `serverid` int(10) DEFAULT '0' COMMENT '服务器id',
  `time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for qy_pet_1000
-- ----------------------------
DROP TABLE IF EXISTS `qy_pet_1000`;
CREATE TABLE `qy_pet_1000` (
  `petid` int(11) NOT NULL AUTO_INCREMENT COMMENT '唯一id',
  `name` varchar(255) DEFAULT '',
  `dataid` int(11) DEFAULT '0',
  `relive` int(10) DEFAULT '0' COMMENT '转生',
  `level` int(10) DEFAULT '0' COMMENT '等级',
  `resid` int(10) NOT NULL DEFAULT '0' COMMENT '资源id',
  `color` int(10) DEFAULT '0' COMMENT '变色',
  `grade` int(10) DEFAULT '0' COMMENT '召唤兽品级',
  `roleid` int(10) NOT NULL DEFAULT '0' COMMENT '角色id',
  `fly` int(10) DEFAULT '0' COMMENT '飞升等级',
  `qinmi` int(10) DEFAULT '0' COMMENT '亲密',
  `shenskill` int(10) DEFAULT '0' COMMENT '神兽技能',
  `skill` varchar(255) DEFAULT NULL,
  `ppoint` varchar(255) DEFAULT NULL,
  `dpoint` varchar(255) DEFAULT NULL,
  `rate` int(10) DEFAULT '0' COMMENT '成长率',
  `hp` int(10) DEFAULT '0' COMMENT '气血',
  `mp` int(10) DEFAULT '0' COMMENT '法力',
  `atk` int(10) DEFAULT '0' COMMENT '攻击力',
  `spd` int(10) DEFAULT '0' COMMENT '速度',
  `wuxing` varchar(255) DEFAULT NULL COMMENT '五行',
  `exp` int(10) DEFAULT '0' COMMENT '经验',
  `xexp` int(10) DEFAULT '0' COMMENT '修炼经验',
  `xlevel` int(10) DEFAULT '0' COMMENT '修炼等级',
  `longgu` int(10) DEFAULT '0',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `delete_time` datetime DEFAULT NULL COMMENT '删除时间',
  `state` int(10) DEFAULT '1' COMMENT '状态1正常，0删除',
  PRIMARY KEY (`petid`),
  KEY `roleid` (`roleid`) USING BTREE,
  KEY `dataid` (`dataid`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=175 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for qy_relation
-- ----------------------------
DROP TABLE IF EXISTS `qy_relation`;
CREATE TABLE `qy_relation` (
  `relationId` int(11) NOT NULL COMMENT '关系id',
  `members` json DEFAULT NULL COMMENT '成员列表',
  `relationType` int(10) DEFAULT NULL COMMENT '关系类型',
  `relationName` varchar(255) DEFAULT NULL COMMENT '关系名字',
  `createTime` datetime DEFAULT NULL COMMENT '创建时间',
  `status` int(10) DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`relationId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for qy_role
-- ----------------------------
DROP TABLE IF EXISTS `qy_role`;
CREATE TABLE `qy_role` (
  `roleid` int(11) NOT NULL AUTO_INCREMENT COMMENT '唯一id',
  `accountid` int(10) NOT NULL DEFAULT '0' COMMENT '账户id',
  `serverid` int(10) DEFAULT '0' COMMENT '所在服务器id',
  `name` varchar(255) DEFAULT '',
  `resid` int(10) DEFAULT '0' COMMENT '角色的资源id',
  `race` int(10) DEFAULT '0' COMMENT '种族',
  `sex` int(10) DEFAULT '1' COMMENT '性别 1 男 2 女',
  `relive` int(10) DEFAULT '0' COMMENT '转生',
  `relivelist` varchar(255) DEFAULT '[[0,0],[0,0],[0,0]]' COMMENT '转生记录',
  `level` int(10) DEFAULT '1' COMMENT '等级',
  `level_reward` varchar(255) DEFAULT '' COMMENT '等级奖励',
  `exp` int(11) DEFAULT '0',
  `money` int(11) DEFAULT '200000',
  `jade` int(11) DEFAULT '0',
  `mapid` int(10) DEFAULT '0',
  `x` int(10) DEFAULT '0',
  `y` int(10) DEFAULT '0',
  `bangid` int(10) DEFAULT '0' COMMENT '帮派id',
  `color` varchar(255) DEFAULT '' COMMENT '染色信息',
  `star` int(10) DEFAULT '0' COMMENT '地煞星级',
  `shane` int(10) DEFAULT '0' COMMENT '善恶',
  `addpoint` varchar(255) DEFAULT '',
  `xiupoint` varchar(2000) DEFAULT '' COMMENT '修炼加点',
  `xiulevel` int(10) DEFAULT '0' COMMENT '修炼等级',
  `title` varchar(255) DEFAULT '' COMMENT '称号',
  `skill` varchar(255) DEFAULT '',
  `bagitem` varchar(2000) DEFAULT NULL COMMENT '背包物品',
  `lockeritem` varchar(2000) DEFAULT NULL COMMENT '储物柜物品',
  `partner` varchar(255) DEFAULT NULL,
  `pet` int(10) DEFAULT '0',
  `getpet` int(10) DEFAULT '0',
  `equiplist` varchar(2000) DEFAULT NULL COMMENT '装备列表，所有装备',
  `taskstate` varchar(4000) DEFAULT NULL,
  `partnerlist` varchar(2000) DEFAULT NULL,
  `chargesum` int(10) DEFAULT '0',
  `rewardrecord` int(10) DEFAULT '0',
  `getgift` int(10) DEFAULT NULL COMMENT '是否领取礼包',
  `shuilu` varchar(1000) DEFAULT NULL COMMENT '水路大会信息',
  `active_scheme_name` varchar(255) DEFAULT NULL COMMENT '套装名称',
  `friendlist` json DEFAULT NULL COMMENT '好友列表',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `lastonline` datetime DEFAULT NULL COMMENT '最后在线时间',
  `gmlevel` int(10) DEFAULT '0',
  `state` int(10) DEFAULT '0',
  PRIMARY KEY (`roleid`,`accountid`)
) ENGINE=InnoDB AUTO_INCREMENT=157838 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for shop_goods
-- ----------------------------
DROP TABLE IF EXISTS `shop_goods`;
CREATE TABLE `shop_goods` (
  `nID` int(11) NOT NULL AUTO_INCREMENT,
  `nConfigID` int(11) DEFAULT NULL,
  `nKind` int(11) DEFAULT NULL,
  `nSubKind` int(11) DEFAULT NULL,
  `strJson` varchar(255) DEFAULT NULL,
  `nSeller` int(11) DEFAULT NULL,
  `nAddTime` int(11) DEFAULT NULL,
  `nPrice` int(11) DEFAULT NULL,
  `nCnt` int(11) DEFAULT NULL,
  `nSellCnt` int(11) DEFAULT NULL,
  PRIMARY KEY (`nID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
