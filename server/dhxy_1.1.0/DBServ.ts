import Global from "./game/core/Global";
import mysql from "mysql";

//启动日志管理
let loginfo = require('./utils/logconfig');
loginfo('xydb');

// 加载配置表
let config_file = process.argv[2];
let config = require(config_file);

Global.serverConfig = config;
console.log('系统配置表加载完毕');

// 未知异常捕获
process.on('uncaughtException', function (err: any) {
	console.error('An uncaught error occurred!');
	console.error(err.stack);
});

let pool: any = null;

function nop(a: any, b: any, c: any, d: any, e: any, f: any, g: any) {
}

/**
 * 从数据库连接池取出连接执行sql操作
 * @param sql 
 * @param callback 
 */
function query(sql: any, callback: any) {
	console.log("SQL:", sql);
	pool.getConnection(function (err: any, conn: any) {
		if (err) {
			callback(err, null, null);
		} else {
			console.log(sql);
			conn.query(sql, function (qerr: any, vals: any, fields: any) {
				//释放连接  
				// pool.releaseConnection(conn)
				conn.release();
				//事件驱动回调  
				callback(qerr, vals, fields);
			});
		}
	});
};

/**
 * 初始化数据库连接池
 * @param config 数据库配置
 */
function init(config: any) {
	pool = mysql.createPool({
		host: config.HOST,
		user: config.USER,
		password: config.PWD,
		database: config.DB,
		port: config.PORT,
		timeout: 60 * 60 * 1000,
		multipleStatements: true,
	});
};

init(config.DB);
console.log('数据库初始化完毕');

class agent {
	socket: any;
	name: string;
	id: number;
	constructor(socket: any) {
		this.socket = socket;
		this.name = '';
		this.id = 0;
	}
	send(event: any, data: any) {
		this.socket.emit(event, data);
	}
}

let agent_seed_id = 0;
let socket_pool: any = {};
let io = require('socket.io')(config.PORT, {
	"transports": ['websocket', 'polling']
});

/**
 * 执行sql操作
 * @param agent 
 * @param data 
 */
function fsql(agent: any, data: any) {
	let sql_id = data.id;
	query(data.sql, (err: any, rows: any, fields: any) => {
		if (err) {
			//sql操作完成后异常,回复
			agent.send('sqled', {
				errorcode: Global.msgCode.FAILED,
			});
			throw (err);
		}
		//sql操作正常,回复
		agent.send('sqled', {
			errorcode: Global.msgCode.SUCCESS,
			id: sql_id,
			data: rows,
		});
	});
}

/**
 * 向数据库服务器注册
 * @param agent 
 * @param data 
 */
function freg(agent: any, data: any) {
	agent.name = data.name;
	console.log(`[${agent.name}]完成注册`);
}

function fclose(agent: any) {
	delete socket_pool[agent.id];
}

io.sockets.on('connection', (socket: any) => {
	let sAgent = new agent(socket);
	sAgent.id = agent_seed_id;

	//sql操作默认列表,相当于类似拦截器的功能
	let list = require('./game/network/default_slist');
	for (let event in list) {
		if (list.hasOwnProperty(event)) {
			let func = list[event];
			sAgent.socket.on(event, (data: any) => {
				if (event != "ping" && event != "pong") console.log("<receive data>", event, data);
				func(sAgent, data);
			});
		}
	}

	//监听sql操作
	socket.on('sql', (data: any) => {
		fsql(sAgent, data);
	});

	//监听向数据库服务器注册服务器信息
	socket.on('reg', (data: any) => {
		freg(sAgent, data);
	});

	socket.on('close', (data: any) => {
		fclose(sAgent);
	});

	socket.on('disconnect', (data: any) => {
		fclose(sAgent);
	});
	socket_pool[agent_seed_id] = sAgent;
});

console.log("数据库模块启动完毕，正在监听本地:8807");
