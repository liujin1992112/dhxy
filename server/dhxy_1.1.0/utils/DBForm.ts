import Global from "../game/core/Global";

let socketio = require('socket.io-client');
let sql_seed = 0;//sql操作id

/**
 * 负责与DB数据库服务器交互
 */
export default class DBForm {
	static shared = new DBForm();
	sqlPool: any;
	socket: any;

	constructor() {
		let uri = "http://localhost:8807";
		let socket = socketio.connect(uri, {
			reconnect: true
		});
		let self = this;
		socket.on('connect', function () { //绑定连接上服务器之后触发的数据
			self.init();
			self.restart();
		});
		socket.on("disconnection", function () {
			console.log("断开连接");
		});

		//保存sql操作信息
		this.sqlPool = {};

		this.socket = socket;
	}

	init() {
		//向数据库服务器提交注册服务器信息
		let self = this;
		self.socket.emit('reg', {
			name: Global.serverName,
		});

		//监听sql操作完成的消息
		this.socket.on('sqled', (data: any) => {
			let id = data.id;
			let sqlinfo = self.sqlPool[id];
			if (sqlinfo) {
				try {
					if (sqlinfo.func) {
						if (data.errorcode == Global.msgCode.SUCCESS) {
							sqlinfo.func(null, data.data);
						} else {
							sqlinfo.func(data.errorcode)
						}
					}
				} catch (error) {
					console.error('DB Error Catch!');
					console.error(sqlinfo.sql);
					console.error(error.stack);
				}
			}
			//sql操作完成,将sql信息从sql池中删除掉
			delete this.sqlPool[id];
		});
	}

	/**
	 * 处理socket断开后sql缓存池中缓存的sql操作
	 */
	restart() {
		for (const sql_seed_id in this.sqlPool) {
			const sqlinfo = this.sqlPool[sql_seed_id];
			this.socket.emit('sql', {
				id: sql_seed_id,
				sql: sqlinfo.sql,
			});
		}
	}

	checkDate() {
		// let ctime = Date.now();
		// for (const sql in this.dbData) {
		// 	const datainfo = this.dbData[sql];
		// 	if(ctime - datainfo.time > 3 * 60 * 1000){
		// 		delete this.dbData[sql];
		// 	}
		// }
	}

	query(sql: any, callback: any) {
		console.log("SQL:", sql);
		sql_seed++;
		if (sql_seed > 5000) {
			sql_seed = 0;
		}

		//保存sql信息
		this.sqlPool[sql_seed] = {
			id: sql_seed,
			sql: sql,
			func: callback,
		}

		//向数据库服务器提交sql操作
		let b = this.socket.emit("sql", {
			id: sql_seed,
			sql: sql,
		});
	}
}