module.exports = (gametype: any) => {
    var log4js = require('log4js');
    log4js.configure(
        {
            appenders: {
                console:
                {
                    type: 'console',
                },
                datalog:
                {
                    type: "dateFile",
                    filename: __dirname + '/../logs/' + gametype,
                    alwaysIncludePattern: true,
                    pattern: "-yyyy-MM-dd-hh.log",
                    category: "console"
                }
            },
            replaceConsole: true,
            categories: {
                default:
                {
                    appenders: ['console'],
                    level: 'debug',
                },
                datalog:
                {
                    // 指定为上面定义的appender，如果不指定，无法写入
                    appenders: ['console', 'datalog'],
                    level: 'debug', // 指定等级
                },
            }
        });
}