import Global from "./game/core/Global";
import DB from "./utils/DB";
import ServerMgr from "./gate/ServerMgr";
import FrozenIPMgr from "./gate/FrozenIPMgr";
import FrozenMacMgr from "./gate/FrozenMacMgr";

export default class Gate {
    mod_list: any = {};

    private complete() {
        console.log('网关服务器启动完毕，等待命令');
    }

    init(mod: any) {
        this.mod_list[mod] = 0;
        return () => {
            this.mod_list[mod] = 1;
            let allcomplete = true;
            for (const mkey in this.mod_list) {
                if (this.mod_list.hasOwnProperty(mkey)) {
                    const value = this.mod_list[mkey];
                    if (value == 0) {
                        allcomplete = false;
                        break;
                    }
                }
            }
            if (allcomplete) {
                this.complete();
            }
        }
    }

    lanuch() {
        Global.serverType = 'gate';
        Global.localIP = Global.getIPAdress();
        Global.serverID = 1000;

        //启动日志管理
        let loginfo = require('./utils/logconfig');
        loginfo('gate');

        // 启动命令行管理
        require('./common/command');
        console.log('命令行模块启动完毕');

        // 加载配置表
        let config = require("./etc/gate_config");
        Global.serverConfig = config;

        // 启动数据库
        // DB.init(config.DB);
        // console.log('数据库初始化完毕');

        // 启动监控系统
        // let cli = require('./common/cli');
        // cli.start(config.CLI.PORT, init('cli'));

        //启动http模块
        let http_service = require('./common/http_service');
        let funlist = require('./gate/http_gate');
        http_service.start({
            ip: config.HTTP.LOCAL,
            port: config.HTTP.PORT,
            funclist: funlist,
        });
        console.log(`HTTP模块启动完毕，开始监听${config.HTTP.LOCAL}:${config.HTTP.PORT}`);

        //启动服务器管理模块
        ServerMgr.shared.init();
        console.log(`服务器管理模块启动完毕`);

        //启动封禁IP管理模块
        FrozenIPMgr.shared.init();
        console.log(`封禁IP管理模块启动完毕`);

        //启动封禁设备管理模块
        FrozenMacMgr.shared.init();
        console.log(`封禁设备管理模块启动完毕`);
    }
}

// 未知异常捕获
process.on('uncaughtException', function (err: any) {
    console.error('An uncaught error occurred!');
    console.error(err.stack);
});

new Gate().lanuch();