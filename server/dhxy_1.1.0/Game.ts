import Global from "./game/core/Global";
import Launch from "./game/core/launch";

// 未知异常捕获
process.on('uncaughtException', function (err: any) {
    console.error('An uncaught error occurred!');
    console.error(err.stack);
})

function complete() {
    console.log('启动游戏模块...');
    Launch.shared.start();
}

let mod_list: any = {};

function init(mod: any) {
    mod_list[mod] = 0;
    return () => {
        mod_list[mod] = 1;
        let allcomplete = true;
        for (const mkey in mod_list) {
            if (mod_list.hasOwnProperty(mkey)) {
                const value = mod_list[mkey];
                if (value == 0) {
                    allcomplete = false;
                    break;
                }
            }
        }
        if (allcomplete) {
            complete();
        }
    }
}

function main() {
    Global.localIP = Global.getIPAdress();
    let config_file = process.argv[2];
    // 加载配置表
    let config = require(config_file);
    Global.serverType = config.INFO.SERVER_TYPE;
    Global.serverName = config.INFO.SERVER_NAME;
    Global.serverID = config.INFO.SERVER_ID;
    Global.serverConfig = config;
    console.log("系统配置表加载完毕");
    //启动日志管理
    let loginfo = require('./utils/logconfig');
    loginfo('dhxy_' + Global.serverID);

    // 启动命令行管理
    require("./common/command");
    console.log('命令行模块启动完毕');

    // 启动监控系统
    let cli = require("./common/cli");
    cli.start(config.CLI.PORT, init('cli'));

    //启动http模块
    let http_service = require('./common/http_service');
    let funlist = require("./game/network/http/http_game");
    http_service.start({
        ip: config.HTTP.IP,
        port: config.HTTP.PORT,
        funclist: funlist,
    });
    console.log(`HTTP模块启动完毕，开始监听${config.HTTP.IP}:${config.HTTP.PORT}`);
}

main();