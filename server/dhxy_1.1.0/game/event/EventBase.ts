/**
 * 任务事件基类
 */
export default class EventBase {
    nEventType: number;//事件类型
    strTip: string;//事件名称
    vecPrize: [];//事件奖励列表

    constructor() {
        this.nEventType = 0;
        this.strTip = '';
        this.vecPrize = [];
    }
}