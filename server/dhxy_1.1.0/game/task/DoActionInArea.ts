import EventBase from "../event/EventBase";

/**
 * 角色到某一个区域执行操作事件
 */
export default class DoActionInArea extends EventBase {
    nMap: number;//地图ID
    nX: number;//地图的x轴坐标
    nY: number;//地图的y轴坐标
    strAction: any;//执行操作名称
    strTalk: string;
    constructor() {
        super();
        this.nMap = 0;
        this.nX = 0;
        this.nY = 0;
        this.strAction;
        this.strTalk = '';
    }
}