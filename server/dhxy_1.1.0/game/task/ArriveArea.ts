import EventBase from "../event/EventBase";

/**
 * 角色到某一个区域事件
 */
export default class ArriveArea extends EventBase {
    nMap: number;//地图ID
    nX: number;//地图的x轴坐标
    nY: number;//地图的y轴坐标
    constructor() {
        super();
        this.nMap = 0;
        this.nX = 0;
        this.nY = 0;
    }
}