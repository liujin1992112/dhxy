import EventBase from "../event/EventBase";

/**
 * 角色击杀Npc事件
 */
export default class KillDynamicNpc extends EventBase {
    vecCreateNpc: any[];
    vecNpc: any[];
    bAutoTrigle: number;
    constructor() {
        super();
        this.vecCreateNpc = [];
        this.vecNpc = [];
        this.bAutoTrigle = 0;
    }
}