
/**
 * 任务模型
 */
export default class Task {
    nTaskID: number;//任务ID
    nKind: number;//任务类型
    nTaskGrop: number;
    nDailyCnt: number;
    strTaskName: string;//任务名称
    vecLimit: any[];//做任务需要达到的条件
    vecEvent: any[];//任务下面一系列事件
    vecFailEvent: any[];//任务下面一失败事件列表
    constructor() {
        this.nTaskID = 0;
        this.nKind = 0;
        this.nTaskGrop = 0;
        this.nDailyCnt = 0;
        this.strTaskName = '';
        this.vecLimit = [];
        this.vecEvent = [];  //事件列表
        this.vecFailEvent = [];
    }
}