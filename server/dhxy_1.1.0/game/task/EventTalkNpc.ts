import EventBase from "../event/EventBase";

/**
 * 角色与Npc对话事件
 */
export default class EventTalkNpc extends EventBase {
    vecCreateNpc: any[];//此次事件需要创建的Npc列表
    nNpcConfigID: number;
    vecSpeak: any[];
    bAutoTrigle: number;
    vecNpc: any;

    constructor() {
        super();
        this.vecCreateNpc = [];
        this.nNpcConfigID = 0;
        this.vecSpeak = [];
        this.bAutoTrigle = 0;
    }

}
