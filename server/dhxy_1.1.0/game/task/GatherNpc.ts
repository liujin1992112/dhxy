import EventBase from "../event/EventBase";

/**
 * 角色采集事件
 */
export default class GatherNpc extends EventBase {
    vecCreateNpc: [];
    vecNpc: [];
    constructor() {
        super();
        this.vecCreateNpc = [];
        this.vecNpc = [];
    }
}